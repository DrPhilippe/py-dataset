@echo off
setlocal
cls
set PYTHONPATH=%cd%\libraries
for /L %%i in (0, 1, 239) do py binaries/datasets/nema/render.py %* --scene 0 --sequence %%i
endlocal