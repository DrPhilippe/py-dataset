@echo off
setlocal
cls
set PYTHONPATH=%cd%\libraries
for /L %%i in (0, 1, 100) do py binaries/train_xp.py %* --initial_epoch %%i --number_of_epochs 1
endlocal