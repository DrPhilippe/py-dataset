import numpy

def length ( a, b ):
	return b - a

def line_length ( l ):
	return length( l[0], l[1] )

def width ( r ):
	return length( r[1,0], r[0,0] )

def height ( r ):
	return length( r[1,1], r[0,1] )

def area ( r ):
	return width( r ) * height( r )

def line_intersection ( l1, l2 ):
	return numpy.asarray([
			max( l1[0], l2[0] ),
			min( l1[1], l2[1] )
		],
		dtype=numpy.float32
		)

def rect_intersection ( r1, r2 ):
	return numpy.asarray([
		[ max( r1[0,0], r2[0,0] ), max( r1[0,1], r2[0,1] ) ],
		[ min( r1[1,0], r2[1,0] ), min( r1[1,1], r2[1,1] ) ]
		],
		dtype=numpy.float32
		)

def line_iou ( l1, l2 ):
	intersection = line_intersection( l1, l2 )
	return line_length(intersection) / ( line_length(l1) + line_length(l2) - line_length(intersection) )

def rect_iou ( r1, r2 ):
	intersection = rect_intersection( r1, r2 )
	return area(intersection) / ( area(r1) + area(r2) - area(intersection) )

if __name__ == '__main__':
	r1 = numpy.asarray( [[0.,0.],[1.,1.]], dtype=numpy.float32 )
	l1 = numpy.asarray( [0., 1.], dtype=numpy.float32 )

	offsets = numpy.arange( 1., -0.01, -0.01, dtype=numpy.float32 )
	ious    = numpy.zeros( [offsets.size, 3], dtype=numpy.float32 )

	for i in range(offsets.size):
		offset = offsets[ i ]
		r2     = numpy.add( r1, [offset, offset] )
		l2     = numpy.add( l1, [offset, offset] )

		ious[ i, 0 ] = 1.-offset
		ious[ i, 1 ] = line_iou( l1, l2 )
		ious[ i, 2 ] = rect_iou( r1, r2 )

	numpy.savetxt( 'ious.csv', ious, fmt='%06.4f', header='offset, line, rect', comments='', delimiter=', ' )