import time
import threading

import pydataset.dataset
import pydataset.manipulators
import pytools.tasks

count = 0
lock  = threading.Lock()

def counter ( example ):
	global count, lock
	lock.acquire()
	count += 1
	lock.release()
	return example

mapper = pydataset.manipulators.CallableMapper.create(
	          callable = counter,
	url_search_pattern = 'groups/training/groups/0/examples/*',
	              name = 'Counter'
	)

# Method 1
t = time.time()
manager = pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = 'file://D:/datasets/mnist',
	         progress_tracker = pytools.tasks.console_progress_logger( 'Parallel mapping' ),
	     wait_for_application = False
	)
manager.wait_for_stop()
manager.join_tasks()
t = time.time()-t
t = time.gmtime(t)
print( 'Parallel mapping took:', time.strftime('%H:%M:%S', t), 'count:', count )
count = 0

# Method 2
t = time.time()
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = 'file://D:/datasets/mnist',
	         progress_tracker = pytools.tasks.console_progress_logger( 'Parallel mapping' )
	)
t = time.time()-t
t = time.gmtime(t)
print( 'Parallel mapping took:', time.strftime('%H:%M:%S', t), 'count:', count )
count = 0

# Method 3
t = time.time()
pydataset.manipulators.apply(
	              manipulator = mapper,
	url_search_pattern_prefix = 'file://D:/datasets/mnist',
	         progress_tracker = pytools.tasks.console_progress_logger( 'Sequential mapping' )
	)
t = time.time()-t
t = time.gmtime(t)
print( 'Sequential mapping took:', time.strftime('%H:%M:%S', t), 'count:', count )
count = 0