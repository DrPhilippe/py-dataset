
import pydataset.aruco
import pydataset.dataset
import pydataset.manipulators
import pydataset.video
import pytools.tasks

# --------------------------------------------------

charuco_settings = pydataset.aruco.CharucoSettings(
	dictionary_name = pydataset.aruco.DictionaryName.DICT_6X6_250,
	    board_width = 5,
	   board_height = 5,
	  square_length = 0.025,
	  marker_length = 0.020
	)

# --------------------------------------------------

pydataset.aruco.plotting.save_charuco_board( charuco_settings, 'charuco.pdf' )

# --------------------------------------------------

# dataset = pydataset.dataset.get_or_create( 'file://D:/datasets/charuco' )
# dataset.get_or_create_group( 'calibration' )

# --------------------------------------------------

# importer = pydataset.video.VideoImporter.create(
# 	   dst_group_url = 'file://D:/datasets/charuco/groups/calibration',
# 	          logger = pytools.tasks.file_logger( 'log.txt' ),
# 	progress_tracker = pytools.tasks.console_progress_logger( 'Importing video' ),
# 	        filepath = 'assets/video.mp4',
# 	      frame_size = pydataset.data.ShapeData( 1080, 720 )
# 	)
# importer.execute()

# --------------------------------------------------

# markers_corners_detector = pydataset.aruco.MarkersCornersDetector.create(
# 	                board_config = board_config,
# 	          image_feature_name = 'frame',
# 	markers_corners_feature_name = 'markers_corners',
# 	          url_search_pattern = 'charuco/groups/calibration/examples/*'
# 	)

# pydataset.manipulators.apply(
# 	              manipulator = markers_corners_detector,
# 	url_search_pattern_prefix = 'file://D:/datasets/',
# 	        number_of_threads = 8,
# 	         progress_tracker = pytools.tasks.console_progress_logger( 'Detecting Markers Corners' )
# 	)

# --------------------------------------------------

# charuco_corners_interpolator = pydataset.aruco.CharucoCornersInterpolator.create(
# 	                board_config = board_config,
# 	          image_feature_name = 'frame',
# 	markers_corners_feature_name = 'markers_corners',
# 	charuco_corners_feature_name = 'charuco_corners',
# 	          url_search_pattern = 'charuco/groups/calibration/examples/*'
# 	)

# pydataset.manipulators.apply(
# 	              manipulator = charuco_corners_interpolator,
# 	url_search_pattern_prefix = 'file://D:/datasets/',
# 	        number_of_threads = 8,
# 	         progress_tracker = pytools.tasks.console_progress_logger( 'Interpolating Charuco Corners' )
# 	)

# --------------------------------------------------

# camera_calibrator = pydataset.aruco.CharucoCameraCalibrator.create(
# 	                board_config = board_config,
# 	charuco_corners_feature_name = 'charuco_corners',
# 	          image_feature_name = 'frame',
# 	     url_calibration_example = 'charuco',
# 	  camera_matrix_feature_name = 'K',
# 	    dist_coeffs_feature_name = 'dist_coeff',
# 	          url_search_pattern = 'charuco/groups/calibration/examples/*'
# 	)

# pydataset.manipulators.apply(
# 	              manipulator = camera_calibrator,
# 	url_search_pattern_prefix = 'file://D:/datasets/',
# 	        number_of_threads = 8,
# 	         progress_tracker = pytools.tasks.console_progress_logger( 'Calibrating Camera' )
# 	)

# --------------------------------------------------

# pose_estimator = pydataset.aruco.CharucoPoseEstimator.create(
# 	                   board_config = board_config,
# 	   charuco_corners_feature_name = 'charuco_corners',
# 	     camera_matrix_feature_name = 'K',
# 	       dist_coeffs_feature_name = 'dist_coeff',
# 	   rotation_vector_feature_name = 'charuco_r',
# 	translation_vector_feature_name = 'charuco_t',
# 	             url_search_pattern = 'charuco/groups/calibration/examples/*'
# 	)

# pydataset.manipulators.apply(
# 	              manipulator = pose_estimator,
# 	url_search_pattern_prefix = 'file://D:/datasets/',
# 	        number_of_threads = 8,
# 	         progress_tracker = pytools.tasks.console_progress_logger( 'Estimating Charuco Pose' )
# 	)
