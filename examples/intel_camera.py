# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import threading
import time

# INTERNALS
import pycameras.intel
import pytools.tasks

# ##################################################
# ###                    MAIN                    ###
# ##################################################

camera = pycameras.intel.IntelCamera.create(
	logger = pytools.tasks.file_logger( 'logs/intel_camera.log' ),
	  name = 'Intel RealSense D435I'
	)

controller = pytools.tasks.Manager( camera )
controller.start_tasks( wait_for_start=True )

print( camera.get_description() )

while camera.is_running():	
	frames = camera.frames	

	if 'color' in frames:
		frame = frames[ 'color' ]
		cv2.imshow( 'Color', frame )

	if 'depth' in frames:
		frame = frames[ 'depth' ]
		cv2.imshow( 'Depth', frame )	

	if cv2.waitKey(20) == ord('q'):
		controller.stop_tasks( wait_for_stop=True )

controller.join_tasks()

