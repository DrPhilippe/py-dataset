import pyrealsense2
import cv2
import numpy

config = pyrealsense2.config()
config.enable_stream( pyrealsense2.stream.color, 1280, 720, pyrealsense2.format.bgr8, 30 )
config.enable_stream( pyrealsense2.stream.depth, 1280, 720, pyrealsense2.format.z16, 30 )
# config.enable_stream( pyrealsense2.stream.infrared, 1, 1280, 720, pyrealsense2.format.y8, 30 )
# config.enable_stream( pyrealsense2.stream.infrared, 2, 1280, 720, pyrealsense2.format.y8, 30 )

pipeline = pyrealsense2.pipeline()
profile = pipeline.start( config )

while ( True ):

	# Get the frame set
	frames = pipeline.wait_for_frames()
	if ( frames is None ):
		break
	
	# Get metadata
	actual_fps = frames.get_frame_metadata( pyrealsense2.frame_metadata_value.actual_fps )
	
	# Get color frame
	color_frame  = frames.get_color_frame()
	color_width  = color_frame.get_width()
	color_height = color_frame.get_height()
	color_bpp    = color_frame.get_bytes_per_pixel()

	# Display color
	color = numpy.asanyarray( color_frame.get_data() )
	text = '{}x{}x{} (fps={})'.format( color_width, color_height, color_bpp, actual_fps )
	color = cv2.putText( color, text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0) , 1, cv2.LINE_AA )	
	cv2.imshow( 'color', color )
	
	# # Get depth frame
	# depth_frame  = frames.get_depth_frame()
	# depth_width  = color_frame.get_width()
	# depth_height = color_frame.get_height()
	# depth_bpp    = color_frame.get_bytes_per_pixel()

	# # Display depth
	# depth = numpy.asanyarray( depth_frame.get_data() ).astype( numpy.uint8 )
	# cv2.imshow( 'depth', depth )

	# # Get infrared frames
	# infra_frame_1 = frames.get_infrared_frame( 1 )
	# infra_frame_2 = frames.get_infrared_frame( 2 )

	# # Display infrared
	# infra_1 = numpy.asanyarray( infra_frame_1.get_data() )
	# infra_2 = numpy.asanyarray( infra_frame_2.get_data() )
	# cv2.imshow( 'infra #1', infra_1 )
	# cv2.imshow( 'infra #2', infra_2 )

	# exit ?
	if cv2.waitKey(1) == ord('q'):
		break

# while ( True )

pipeline.stop()