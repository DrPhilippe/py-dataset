import pydataset
import pykeras
import tensorflow
import cv2
import numpy

example = pydataset.dataset.get( 'file://D:/Datasets/linemod/groups/raw/groups/testing/groups/cat/groups/00001/examples/0' )
image   = example.get_feature( 'image' ).value
rect    = example.get_feature( 'bounding_rectangle' ).value.astype(numpy.float32).ravel()



x1 = rect[0]
y1 = rect[1]
x2 = rect[2]
y2 = rect[3]
w  = x2 - x1
h  = y2 - y1

rect_1 = numpy.asarray( [x1, y1, x2, y2, 1.], dtype=numpy.float32 )
rect_1 = tensorflow.reshape( rect_1, [1, 1, 1, 7] )

yolo_1 = numpy.asarray([
	(x1 + w / 2.) / 640.,
	(y1 + w / 2.) / 480.,
	numpy.sqrt( w / 640. ),
	numpy.sqrt( h / 480. ),
	1.,
	], dtype=numpy.float32)

yolo_2 = pykeras.yolo_utils.xyxy_rectangles_to_yolo( rect_1, 1, 1, (640., 480.) )
rect_2 = pykeras.yolo_utils.predicted_rectangles_to_xyxy( yolo_2, 1, 1, (1., 1.) )

rect_1 = rect_1.numpy().ravel()[0:4]
yolo_2 = yolo_2.numpy().ravel()[0:4]
rect_2 = rect_2.numpy().ravel()[0:4]
rect_3 = [x1/640., y1/480., x2/640, y2/480.]

print( 'rect_1:', rect_1 )
print( 'yolo_1:', yolo_1 )
print( 'yolo_2:', yolo_2 )
print( 'rect_2:', rect_2 )
print( 'rect_3:', rect_3 )

image = pydataset.cv2_drawing.draw_bounding_rectangle( image, numpy.reshape(rect_1, [2,2]).astype(numpy.int32), (255,0,0), 2, (255,0,0), 3, 3 )
image = pydataset.cv2_drawing.draw_bounding_rectangle( image, numpy.reshape(rect_2, [2,2]).astype(numpy.int32), (0,0,255), 1, (0,0,255), 2, 2 )
cv2.imshow( '00001', image )
cv2.waitKey(0)