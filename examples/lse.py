import numpy
import sys
import pydataset.render

mesh    = pydataset.render.MeshData.load_ply( sys.argv[1] )
points  = mesh.points.copy()
normals = mesh.normals.copy()
nb      = points.shape[0]
print( 'points:', points.shape )



def LSE ( point, neighboors, i, j, k ):
	# Compute distances
	distances = numpy.linalg.norm( neighboors - point, axis=-1 ) 
	# Compute weights
	weights = numpy.exp( -distances**2.0 / 5.0**5.0 )
	# Compute LSE
	return numpy.sum( weights * neighboors[:,0]**i * neighboors[:,1]**j * neighboors[:,2]**k )


def constrain ( R, normal ):
	r1 = R[0,:]
	r2 = R[1,:]
	r3 = R[2,:]
	o = numpy.dot( r1, normal )
	if o >= 0.0:
		r3_ = r3
	else:
		r3_ = -r3
	r1_ = r1
	r2_ = numpy.cross( -r1_, r3_ )

	R[0,:] = r1_
	R[1,:] = r2_
	R[2,:] = r3_
	return R

colors = numpy.ones( (nb, 4), dtype=numpy.float32 )
for i in range(nb):

	# Get current point and its neighboors
	p          = points[i]
	n          = normals[i]
	distances  = numpy.linalg.norm( points - p, axis=-1 ) 
	neighboors = points[ distances < 30.0 ]
	# print( 'p:', p )
	# print( 'neighboors:', neighboors.shape )

	# Center neigboorhood
	neighboors = neighboors - p

	# Compute covariance
	# C = numpy.cov( neighboors )
	C = numpy.dot( numpy.transpose(neighboors), neighboors )
	# print( 'C:', C.shape )

	# Singula Value Decomposition
	R, v, vh = numpy.linalg.svd( C )
	# print( 'U:', u.shape )
	# print( 'V:', v.shape )
	# print( 'R:', R.shape )

	# Constrain R as a rotation matrix
	R = constrain( R, n )

	# Apply rotation invariance
	neighboors = numpy.dot( neighboors, R )
	
	lse = LSE( p, neighboors, 0, 2, 1 )
	colors[ i, 0:3 ] = [lse, lse, lse]

mesh.colors = colors
mesh.save_ply( "out_lse.ply" )

