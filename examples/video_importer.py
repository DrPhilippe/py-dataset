import pytools.tasks
import pydataset.io

# backend_url   = 'file://Users/Philippe/Projet/PyDataset/datasets'
backend_url   = 'file://D:/datasets'
dataset_url   = backend_url + '/video'
dst_group_url = dataset_url

importer = pydataset.io.VideoImporter.create(
	   dst_group_url = dst_group_url,
	          logger = pytools.tasks.file_logger( 'logs/video-importer.txt' ),
	progress_tracker = pytools.tasks.console_progress_logger( 'Importing video' ),
	        filepath = 'resources/video.mp4'
	)
importer.execute()