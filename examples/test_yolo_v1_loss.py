import cv2
import numpy
import sys
import tensorflow
import pydataset
import pykeras

W = 1.
H = 1.
S = 1
B = 2
C = 2

example = pydataset.dataset.get( 'file://D:/Datasets/linemod/groups/raw/groups/all/groups/duck/groups/00066/examples/0' )
image   = example.get_feature( 'image' ).value
rect    = example.get_feature( 'bounding_rectangle' ).value.ravel().astype(numpy.float32)
yolo    = example.get_feature( 'yolo_v1' ).value
x1      = rect[ 0 ]
y1      = rect[ 1 ]
x2      = rect[ 2 ]
y2      = rect[ 3 ]


def disp_f ( name, array ):
	sys.stdout.write( '{:20s}: ['.format(name) )
	for v in array:
		sys.stdout.write( '{:10.6f}, '.format(v) )
	sys.stdout.write( ']\n' )

def disp_b ( name, array ):
	print( '{:20s}: {}'.format(name, array) )

def to_yolo ( x1, y1, x2, y2, ci=0., cj=0. ):
	global S, B, C, W, H
	cw = W/S
	ch = H/S
	cx = (x1+x2) / 2.
	cy = (y1+y2) / 2.
	w  = x2-x1
	h  = y2-y1
	cx = numpy.clip( (cx - ci*cw)/cw, 0., 1. )
	cy = numpy.clip( (cy - cj*ch)/ch, 0., 1. )
	w  = numpy.sqrt( w / 1. )
	h  = numpy.sqrt( h / 1. )
	return cx, cy, w, h

yolo = to_yolo( x1, y1, x2, y2 )

y_true = numpy.asarray([
	# raw 1
	# 0., 0., 0., 0., 0., 0., 0.,
	# 0., 0., 0., 0., 0., 0., 0.,
	# 0., 0., 0., 0., 0., 0., 0.,
	# raw 2
	# 0., 0., 0., 0., 0., 0., 0.,
	x1/640., y1/480., x2/640., y2/480., 1., 0., 1.,
	# 0., 0., 0., 0., 0., 0., 0.,
	# raw 3
	# 0., 0., 0., 0., 0., 0., 0.,
	# 0., 0., 0., 0., 0., 0., 0.,
	# 0., 0., 0., 0., 0., 0., 0.
	], dtype=numpy.float32 )

y_pred = numpy.asarray([
	# raw 1
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# raw 2
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	*to_yolo(x1/640, y1/480, x2/640, y2/480), 1., *to_yolo(0., 0., 0., 0.), 0.5, 0., 1.,
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# raw 3
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.,
	# *to_yolo(0., 0., 0., 0.), 0., *to_yolo(0., 0., 0., 0.), 0., 0., 0.
	], dtype=numpy.float32 )

y_true                           = tensorflow.reshape( y_true, [-1, S, S, 5+C] )
y_pred                           = tensorflow.reshape( y_pred, [-1, S, S, B*5+C] )
not_empty                        = pykeras.yolo_utils.identify_non_empty_cells( y_true )
y_true_rects_xyxy, y_true_logits = pykeras.yolo_utils.slice_labels( y_true )
y_pred_rects_yolo, y_pred_logits = pykeras.yolo_utils.slice_predictions( y_pred, B )
y_true_rects_xyxy                = pykeras.yolo_utils.repeat_labels_rectangles_to_match_predictions( y_true_rects_xyxy, B )
y_true_rects_yolo                = pykeras.yolo_utils.xyxy_rectangles_to_yolo( y_true_rects_xyxy, S, B, (W, H) )
y_pred_rects_xyxy                = pykeras.yolo_utils.predicted_rectangles_to_xyxy( y_pred_rects_yolo, S, B, (W, H) )
ious = tensorflow.reshape( pykeras.yolo_utils.intersection_over_union(
	tensorflow.reshape( y_true_rects_xyxy, [-1, 5] ),
	tensorflow.reshape( y_pred_rects_xyxy, [-1, 5] )
	), [-1, S, S, B] )
bests, ious2 = pykeras.yolo_utils.best_rectangles_matches_mask( y_true_rects_xyxy, y_pred_rects_xyxy, S, B )
not_empty = tensorflow.reshape( not_empty, [-1, S, S, 1] )
not_empty = tensorflow.tile( not_empty, [1, 1, 1, B*5] )

i_obj   = tensorflow.math.logical_and( not_empty, bests )
i_noobj = tensorflow.math.logical_not( i_obj )

# Replace label confidence by iou
y_true_rects_xyxy = tensorflow.where( ious2 > 0, ious2, y_true_rects_xyxy )

true_obj   = tensorflow.multiply( y_true_rects_xyxy, tensorflow.cast( i_obj, tensorflow.float32 ) )
pred_obj   = tensorflow.multiply( y_pred_rects_xyxy, tensorflow.cast( i_obj, tensorflow.float32 ) )
true_noobj = tensorflow.multiply( y_true_rects_xyxy, tensorflow.cast( i_noobj, tensorflow.float32 ) )
pred_noobj = tensorflow.multiply( y_pred_rects_xyxy, tensorflow.cast( i_noobj, tensorflow.float32 ) )

print( 'rect:', rect )
print( 'rect:', [x1/640., y1/480., x2/640., y2/480.] )
print( 'yolo', yolo )
# image = cv2.rectangle( image, (x1,y1), (x2,y2), (0,255,0), 2 )
# cv2.imshow( 'image', image )
# cv2.waitKey( 0 )

for i in range( S ):
	for j in range( S ):
		print( '-------------------------------------------------------------------------' )
		print( 'CELL ', (i, j) )
		disp_b( 'not_empty',         not_empty.numpy()[0, i, j].ravel() )
		disp_b( 'bests',             bests.numpy()[0, i, j].ravel() )
		disp_b( 'i_obj',             i_obj.numpy()[0, i, j].ravel() )
		disp_b( 'i_noobj',           i_noobj.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'y_true',            y_true.numpy()[0, i, j].ravel() )
		disp_f( 'y_pred',            y_pred.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'y_true_logits',     y_true_logits.numpy()[0, i, j].ravel() )
		disp_f( 'y_pred_logits',     y_pred_logits.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'y_true_rects_xyxy', y_true_rects_xyxy.numpy()[0, i, j].ravel() )
		disp_f( 'y_pred_rects_xyxy', y_pred_rects_xyxy.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'y_true_rects_yolo', y_true_rects_yolo.numpy()[0, i, j].ravel() )
		disp_f( 'y_pred_rects_yolo', y_pred_rects_yolo.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'ious',              ious.numpy()[0, i, j].ravel() )
		disp_f( 'ious2',             ious2.numpy()[0, i, j].ravel() )
		print( '' )
		disp_f( 'true_obj',          true_obj.numpy()[0, i, j].ravel() )
		disp_f( 'pred_obj',          pred_obj.numpy()[0, i, j].ravel() )
		disp_f( 'true_noobj',        true_noobj.numpy()[0, i, j].ravel() )
		disp_f( 'pred_noobj',        pred_noobj.numpy()[0, i, j].ravel() )
		print( '' )

loss = pykeras.losses.yolo_v1.YoloLoss( (W, H), S, B, C )
true_obj_rects, pred_obj_rects, true_noobj_rects, pred_noobj_rects, true_logits, pred_logits = loss.prepare_data( y_true, y_pred )

true_obj_rects   = tensorflow.reshape( true_obj_rects, [-1, S, S, 5*B] )
pred_obj_rects   = tensorflow.reshape( pred_obj_rects, [-1, S, S, 5*B] )
true_noobj_rects = tensorflow.reshape( true_noobj_rects, [-1, S, S, 5*B] )
pred_noobj_rects = tensorflow.reshape( pred_noobj_rects, [-1, S, S, 5*B] )
true_logits = tensorflow.reshape( true_logits, [-1, S, S, C] )
pred_logits = tensorflow.reshape( pred_logits, [-1, S, S, C] )

for i in range( S ):
	for j in range( S ):
		print( '-------------------------------------------------------------------------' )
		print( 'CELL ', (i, j) )
		disp_f( 'true_obj_rects',     true_obj_rects.numpy()[0, i, j].ravel() )
		disp_f( 'pred_obj_rects',     pred_obj_rects.numpy()[0, i, j].ravel() )
		disp_f( 'true_noobj_rects',  true_noobj_rects.numpy()[0, i, j].ravel() )
		disp_f( 'pred_noobj_rects',  pred_noobj_rects.numpy()[0, i, j].ravel() )
		disp_f( 'true_logits',       true_logits.numpy()[0, i, j].ravel() )
		disp_f( 'pred_logits',       pred_logits.numpy()[0, i, j].ravel() )
		print( '' )

loss_value = loss( y_true, y_pred )
print( 'LOSS:', loss_value.numpy() )