import pydataset
import cv2
import numpy

example = pydataset.dataset.get( 'file://E:/Datasets/linemod/groups/raw/groups/all/groups/duck/groups/00066/examples/0' )
image   = example.get_feature( 'image' ).value
mesh    = example.get_feature( 'mesh_ply' ).value
K       = example.get_feature( 'K' ).value
R       = example.get_feature( 'R' ).value
t       = example.get_feature( 't' ).value

r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )

bb = mesh.get_bounding_box()
bb,ret = cv2.projectPoints( bb, r, t, K, numpy.zeros([5]) )
bb = numpy.reshape( bb, [8, 2] )
drawing = pydataset.cv2_drawing.draw_bounding_box( image.copy(), bb, edges_color=(0,0,255), edges_thickness=1, points_color=(0,0,255), points_radius=1, points_thickness=2 )

# cv2.imshow( 'duck 66', image )
# cv2.waitKey( 0 )
cv2.imwrite( 'duck_66_color.png', image )
cv2.imwrite( 'duck_66_bb.png', drawing )