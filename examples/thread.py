from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import sys

class Worker ( QObject ):

	started = pyqtSignal( int )
	finished = pyqtSignal( int )

	def __init__ ( self, index ):
		super( Worker, self ).__init__()
		self.index     = index
		self.counter   = 0
		self.must_stop = True

	def execute ( self ):
		self.started.emit( self.index )
		while not self.must_stop:
			self.counter += 1
			QCoreApplication.processEvents()
			QThread.yieldCurrentThread()
		return 0

	@pyqtSlot()
	def on_start ( self ):
		self.counter   = 0
		self.must_stop = False
		self.execute()
		self.finished.emit( self.index )
		self.thread().quit()

	@pyqtSlot()
	def on_stop ( self ):
		self.must_stop = True
		QCoreApplication.processEvents()
		QThread.yieldCurrentThread()

	def __repr__ ( self ):
		return '[{}.Worker#{}: {}]'.format(self.thread(), self.index, self.counter)

	def __str__ ( self ):
		return '[{}.Worker#{}: {}]'.format(self.thread(), self.index, self.counter)

class Manager ( QObject ):
	NB = 10

	start_workers = pyqtSignal()
	stop_workers = pyqtSignal()

	def __init__ ( self ):
		super( Manager, self ).__init__()
		print( '{}.__init__():'.format(self) )
		self.workers = []
		self.threads = []
		for index in range(Manager.NB):
			thread = QThread()
			self.threads.append( thread )
			
			worker = Worker( index )
			worker.moveToThread( thread )
			self.start_workers.connect( worker.on_start )
			self.stop_workers.connect( worker.on_stop )
			worker.started.connect( self.on_worker_started )
			worker.finished.connect( self.on_worker_finished )
			self.workers.append( worker )
			print( '    {}'.format(worker) )

	@pyqtSlot()
	def start_work ( self ):
		print( '{}.start_work():'.format(self) )
		for index in range(Manager.NB):
			thread = self.threads[ index ]
			thread.start()
		for index in range(Manager.NB):
			worker = self.workers[ index ]
			print( '    {}'.format(worker) )
		self.start_workers.emit()
		
	
	@pyqtSlot()
	def stop_work ( self ):
		print( '{}.stop_work():'.format(self) )		
		self.stop_workers.emit()
		QThread.yieldCurrentThread()
		QCoreApplication.processEvents()
		for index in range(Manager.NB):
			thread = self.threads[ index ]
			worker = self.workers[ index ]
			thread.wait()
			QThread.yieldCurrentThread()
			QCoreApplication.processEvents()
			print( '    {}'.format(worker) )	

	@pyqtSlot( int )
	def on_worker_started ( self, index ):
		print( 'Worker#{} started'.format(index) )

	@pyqtSlot( int )
	def on_worker_finished ( self, index ):
		print( 'Worker#{} finished'.format(index) )

	def __repr__ ( self ):
		return '[{}.Manager]'.format(self.thread())

	def __str__ ( self ):
		return '[{}.Manager]'.format(self.thread())

if __name__ == '__main__':
	app = QApplication( sys.argv )
	manager = Manager()
	window = QWidget()
	layout = QHBoxLayout( window )
	window.setLayout( layout )
	button = QPushButton( 'Start', window )
	button.clicked.connect( manager.start_work )
	layout.addWidget( button )
	button = QPushButton( 'Stop', window )
	button.clicked.connect( manager.stop_work )
	layout.addWidget( button )
	window.show()
	app.exec()