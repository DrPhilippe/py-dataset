# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import threading
import time

# INTERNALS
import pycameras
import pycameras.opencv
import pytools.tasks

# ##################################################
# ###                    MAIN                    ###
# ##################################################

camera = pycameras.opencv.OpenCVCamera( logger=pytools.tasks.file_logger( 'logs/opencv_camera.log' ) )

manager = pytools.tasks.Manager( camera )
manager.start_tasks( wait_for_start=True )

print( camera.get_description() )

while camera.is_running():	
	frames = camera.frames	

	if 'color' in frames:
		frame = frames[ 'color' ]
		cv2.imshow( 'Color', frame )

	if cv2.waitKey(20) == ord('q'):
		manager.stop_tasks( wait_for_stop=True )

manager.join_tasks()

