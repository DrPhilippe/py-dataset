# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset.dataset
import pytools.tasks

parser = argparse.ArgumentParser()
parser.add_argument( 'src_url', type=str, help='URL of the source item' )
parser.add_argument( 'dst_url', type=str, help='URL of the destination item' )
arguments = parser.parse_args()

pt = pytools.tasks.console_progress_logger( 'Duplicating:' )
src = pydataset.dataset.get( arguments.src_url )
dst = pydataset.dataset.get_or_create( arguments.dst_url )
dst.copy( src, True, pt )