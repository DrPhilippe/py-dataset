import pytools
import pykeras

logger = pytools.tasks.console_logger()
dataset = pykeras.inputs_v2.PyDataset.open(
	    url_search_pattern = 'file://G:/Datasets/linemod/groups/raw/groups/training/groups/duck/groups/*/examples/*',
	                  name = 'training-duck',
	initial_features_names = [ 'image', 'category_index' ],
	               shuffle = True,
	            batch_size = 32,
	   drop_batch_reminder = False,
	         prefetch_size = 0
	   )
dataset.summary( logger )

i=0
for batch in dataset.use(logger):
	logger.log( 'Batch N°{}:', i )
	for key, value in batch.items():
		logger.log( '    {:20s}: {}', key, value.shape )
	i+=1
	logger.end_line()
	logger.flush()
	if i == 10:
		break

