import cv2
import numpy


def draw_circle ( mask, center, scale ):
	center = ( int(center[0]), int(center[1]) )
	scale  = int( scale/2. )
	return cv2.circle( mask, center, scale, (255,), cv2.FILLED, cv2.LINE_8 )

def draw_square ( mask, center, scale ):
	p1 = numpy.add( center, [-scale/2., -scale/2.] ).astype(int)
	p2 = numpy.add( center, [+scale/2., +scale/2.] ).astype(int)
	return  cv2.rectangle( mask, (p1[0], p1[1]), (p2[0], p2[1]), (255,), cv2.FILLED, cv2.LINE_8 )

def draw_triangle( mask, center, scale, angle=180. ):
	p1 = scale * numpy.asarray([
		numpy.cos( (angle+90.)*numpy.pi/180. ),
		numpy.sin( (angle+90.)*numpy.pi/180. )
		]) + center
	p2 = scale * numpy.asarray([
		numpy.cos( (angle+90.+120.)*numpy.pi/180. ),
		numpy.sin( (angle+90.+120.)*numpy.pi/180. )
		]) + center
	p3 = scale * numpy.asarray([
		numpy.cos( (angle+90.+240.)*numpy.pi/180. ),
		numpy.sin( (angle+90.+240.)*numpy.pi/180. )
		]) + center

	pts = numpy.asarray( [p1,p2,p3], dtype=int )
	pts = numpy.reshape( pts, [-1, 1, 2] )
	return cv2.fillPoly( mask, [pts], (255,), cv2.LINE_8 )


# Empty mask
mask = numpy.zeros( [224, 224], dtype=numpy.uint8 )

# Center and scale of the shape
center = numpy.asarray( [112.,112.], dtype=numpy.float32 )
scale  = 64.

# Circular shape
# mask = draw_circle( mask, center, scale )
# mask = draw_square( mask, center, scale )
mask = draw_triangle( mask, center, scale )

# Rectangular shape
# shape = cv2.rectangle( shape, (112.+scale,32), (224-32,224-32), (255,), cv2.FILLED, cv2.LINE_8 )
# shape = cv2.polylines( shape, [[]])
cv2.imshow( 'mask', mask )
cv2.waitKey(0)