import pydataset.data
import pydataset.dataset
import numpy

nb_digit = 2
nb_examples = 10

dataset = pydataset.dataset.create( 'mongodb://localhost:27017/mnist' )
print( dataset.url )
for group_name in ['training', 'testing']:
	group = dataset.create_group( group_name )
	print( group.url )
	for i in range(nb_digit):
		g = group.create_group( '{:1d}'.format(i) )
		print( g.url )
		f = g.create_feature( pydataset.data.IntFeatureData, 'digit', shape=pydataset.data.ShapeData(), value=i )
		print( f.url )
		for j in range(nb_examples):
			e = g.create_example()
			print( e.url )
			f = e.create_feature( pydataset.data.ImageFeatureData, 'image',
				    shape = pydataset.data.ShapeData(32, 32),
				    dtype = 'uint8',
				    value = numpy.zeros( (32, 32), dtype=numpy.uint8 ),
				extension = '.jpeg',
				    param = 50
				)
			print( f.url, f.data.shape, f.data.dtype )