
import pydataset.dataset
import cv2

def show( example, wait=1000 ):
	features = example.get_features()
	image = features[ 'image' ].value
	digit = features[ 'digit' ].value
	image = cv2.resize( image, (128, 128) )
	image = cv2.cvtColor( image, cv2.COLOR_GRAY2BGR )
	image = cv2.putText( image, str(digit), (2, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1, cv2.LINE_AA ) 
	cv2.imshow( 'mnist', image )
	if cv2.waitKey(wait) == ord('q'):
		exit()

# Iterate over the first example of each group and each digit
iterator = pydataset.dataset.Iterator(
	'file://D:/datasets/mnist/groups/*/groups/*/examples/example_0'
	)
for example in iterator:
	show( example, 0 )
# Pick one
example = iterator.at( 9 )
show( example, 0 )
# Pick an other
example = iterator.at( 10 )
show( example, 0 )

# Iterate can work on single examples
iterator = pydataset.dataset.Iterator(
	'file://D:/datasets/mnist/groups/training/groups/0/examples/example_0'
	)
# show the example
for example in iterator:
	show( example, 0 )
# Same thing
example = iterator.at( 0 )
show( example, 0 )