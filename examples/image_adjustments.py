import cv2
import numpy
import pytools
import tensorflow

def read_image ( path='resources/lm/duck_66.png' ):
	image = cv2.imread( path )
	return numpy.divide( image, 255. ).astype( numpy.float32 )

def save_image ( image, path ):
	image = numpy.multiply( image, 255. ).astype( numpy.uint8 )
	cv2.imwrite( path, image )

out_dir = pytools.path.DirectoryPath( 'resources/lm/adj/' )
if out_dir.is_not_a_directory():
	out_dir.create()

image = read_image()

brightnesses = numpy.arange( -1.0, 1.2, 0.2, dtype=numpy.float32 )
for brightness in brightnesses:
	adj_image = tensorflow.image.adjust_brightness( image, brightness )
	adj_image = tensorflow.clip_by_value( adj_image, 0., 1. ).numpy()
	save_image( adj_image, 'resources/lm/adj/brightness_{:+03.1f}.png'.format(brightness) )

contrasts = numpy.arange( 0.2, 2.2, 0.2, dtype=numpy.float32 )
for contrast in contrasts:
	adj_image = tensorflow.image.adjust_contrast( image, contrast )
	adj_image = tensorflow.clip_by_value( adj_image, 0., 1. ).numpy()
	save_image( adj_image, 'resources/lm/adj/contrast_{:+03.1f}.png'.format(contrast) )

hues = numpy.arange( -0.1, 0.11, 0.01, dtype=numpy.float32 )
for hue in hues:
	adj_image = tensorflow.image.adjust_hue( image, hue )
	adj_image = tensorflow.clip_by_value( adj_image, 0., 1. ).numpy()
	save_image( adj_image, 'resources/lm/adj/hue_{:+05.3f}.png'.format(hue) )

saturations = numpy.arange( 0.2, 2.2, 0.2, dtype=numpy.float32 )
for saturation in saturations:
	adj_image = tensorflow.image.adjust_saturation( image, saturation )
	adj_image = tensorflow.clip_by_value( adj_image, 0., 1. ).numpy()
	save_image( adj_image, 'resources/lm/adj/saturation_{:+03.1f}.png'.format(saturation) )

qualities = numpy.arange( 10, 110, 10, dtype=numpy.int32 )
for quality in qualities:
	adj_image = tensorflow.image.adjust_jpeg_quality( image, quality )
	adj_image = tensorflow.clip_by_value( adj_image, 0., 1. ).numpy()
	save_image( adj_image, 'resources/lm/adj/quality_{:+04d}.png'.format(quality) )

