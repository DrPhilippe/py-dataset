import numpy
import cv2

image = numpy.arange( 0, 255, 1, dtype=numpy.uint8 )
image = numpy.reshape( image, [1, 255, 1] )
image = numpy.tile( image, [255, 1, 1] )

image = numpy.reshape( image, [255*255, 1] )
print( image.shape )
image = cv2.applyColorMap( image, cv2.COLORMAP_JET )
image = numpy.reshape( image, [255, 255, 3] )
print( image.shape )

cv2.imshow( 'image', image )
cv2.waitKey(0)
