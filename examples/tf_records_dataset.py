import cv2
import tensorflow
import pykeras
import pytools

logger = pytools.tasks.console_logger()
dataset = pykeras.inputs_v2.TFRecordsDataset.open(
	        directory_path = 'D:\\Datasets\\t_less',
	                  name = 'training',
	initial_features_names = [ 'image', 'visible_mask', 'bounding_rectangle', 'one_hot_category' ],
	                repeat = False,
	               shuffle = False,
	   shuffle_buffer_size = 0,
	            batch_size = 1,
	   drop_batch_reminder = False,
	         prefetch_size = 0
	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesToFloat,
# 	images_features_names = [ 'image' ]
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomBrightness,
# 	images_features_names = [ 'image' ],
# 	            max_delta = 0.1
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomContrast,
# 	images_features_names = [ 'image' ],
# 	                lower = 0.9,
# 	                upper = 1.1
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomHue,
# 	images_features_names = [ 'image' ],
# 	            max_delta = 0.05
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomSaturation,
# 	images_features_names = [ 'image' ],
# 	                lower = 0.8,
# 	                upper = 1.0
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomJpegQuality,
# 	images_features_names = [ 'image' ],
# 	                lower = 80.,
# 	                upper = 100.
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesRandomGaussianBlur,
# 	images_features_names = [ 'image' ],
# 	    lower_kernel_size = 1,
# 	    upper_kernel_size = 5,
# 	          lower_sigma = 1.,
# 	          upper_sigma = 2.
# 	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.CenterCropper,
# 	          images_features_names = [ 'image', 'visible_mask' ],
# 	                      crop_size = (300, 300),
# 		    affected_features_names = [ 'bounding_rectangle' ]
# 	)
dataset.add_mapper(
	pykeras.inputs_v2.mappers.BoundingRectangleCropper,
	              images_features_names = [ 'image', 'visible_mask' ],
		bounding_rectangle_feature_name = 'bounding_rectangle',
			                  crop_size = (0, 0),
			                   pad_size = (16, 16),
		        affected_features_names = [ 'bounding_rectangle' ]
	)
# dataset.add_mapper(
# 	pykeras.inputs_v2.mappers.ImagesResizer,
# 	  images_features_names = [ 'image', 'visible_mask' ],
# 	                   size = (300, 300),
# 	                 method = 'nearest',
# 	affected_features_names = [ 'bounding_rectangle' ]
# 	)
dataset.add_mapper(
	pykeras.inputs_v2.mappers.BackgroundExchanger,
	backgrounds_directory_path = 'resources/sun2012',
	        image_feature_name = 'image',
	         mask_feature_name = 'visible_mask',
	                 crop_size = (540, 720),
	                 past_mode = 'random',
	   affected_features_names = [ 'bounding_rectangle' ]
	)
# # dataset.add_mapper( # very slow
# 	pykeras.inputs_v2.mappers.ImagesSummary,
# 	   log_directory_path = 'tensorboard',
# 	             log_name = 'images',
# 	images_features_names = [ 'image' ],
# 	)
dataset.add_mapper(
	pykeras.inputs_v2.mappers.FeaturesMapper,
	inputs = { 'image': 'image' },
	labels = {
		'visible_mask': 'visible_mask',
		'bounding_rectangle': 'bounding_rectangle',
		'one_hot_category': 'one_hot_category'
		}
	)
dataset.summary( logger )

for inputs, labels, samples_weights in dataset.use(logger):

	images = inputs[ 'image' ]
	masks  = labels[ 'visible_mask'  ]
	rects  = labels[ 'bounding_rectangle' ]
	H = tensorflow.cast( tensorflow.shape( images )[ 1 ], tensorflow.float32 )
	W = tensorflow.cast( tensorflow.shape( images )[ 2 ], tensorflow.float32 )

	rects = tensorflow.cast( rects, tensorflow.float32 )
	rects = tensorflow.reshape( rects, [-1, 2] )
	rects = tensorflow.math.divide( rects, [W, H] )
	rects = tensorflow.reshape( rects, [-1, 4] )
	rects = tensorflow.unstack( rects, axis=1 )
	rects = tensorflow.stack([
		rects[ 1 ], # y1
		rects[ 0 ], # x1
		rects[ 3 ], # y2
		rects[ 2 ]  # x2
		],
		axis=1
		)
	rects = tensorflow.reshape( rects, [-1, 1, 4] )

	images = tensorflow.cast( images, tensorflow.float32 )
	images = tensorflow.math.divide( images, 255. )
	images = tensorflow.image.draw_bounding_boxes(
		images,
		rects,
		[[1., 0., 0., 1.]]
		)

	images = tensorflow.multiply( images, 255. )
	images = tensorflow.cast( images, tensorflow.uint8 )
	images = images.numpy()

	masks = tensorflow.cast( masks, tensorflow.uint8 )
	masks = tensorflow.multiply( masks, 255 )
	masks = masks.numpy()

	batch_size = images.shape[ 0 ]
	for i in range( batch_size ):
		cv2.imshow( 'image', images[i] )
		cv2.imshow( 'mask', masks[i] )
		if cv2.waitKey( 1000 ) == ord('q'):
			exit()

	# for i in range( batch_size )

# for inputs, labels, samples_weights in dataset.use(logger)
