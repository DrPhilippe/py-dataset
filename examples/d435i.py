import json
import pyrealsense2

# Check if the capera is plugged in
context = pyrealsense2.context()
if context.devices.size() == 0:
	raise RuntimeError( '0 camera plugged in!' )

divices_data = []
for device in context.devices:
	device_data = {
		        'name': device.get_info( pyrealsense2.camera_info.name ),
		  'product_id': device.get_info( pyrealsense2.camera_info.product_id ),
		'product_line': device.get_info( pyrealsense2.camera_info.product_line ),
		      'serial': device.get_info( pyrealsense2.camera_info.serial_number )
	}
	depth_sensor = device.first_depth_sensor()
	if depth_sensor is not None:
		device_data[ 'depth_scale' ] = depth_sensor.get_depth_scale()
	divices_data.append( device_data )

# Configure camera
config = pyrealsense2.config()
# config.enable_device( serial )
config.enable_stream( pyrealsense2.stream.color, 1280, 720, pyrealsense2.format.bgr8, 30 )
config.enable_stream( pyrealsense2.stream.depth, 1280, 720, pyrealsense2.format.z16, 30 )

# Run camera
pipeline = pyrealsense2.pipeline()
profile = pipeline.start( config )

# Get intrinsics
stream_profile = profile.get_stream( pyrealsense2.stream.color ).as_video_stream_profile()
intrinsics = stream_profile.get_intrinsics()
camera_data = {
	 'width': intrinsics.width,
	'height': intrinsics.height,
	    'fx': intrinsics.fx,
	    'fy': intrinsics.fy,
	   'ppx': intrinsics.ppx,
	   'ppy': intrinsics.ppy,
	'coeffs': intrinsics.coeffs
}

# Get runnuing divice
device = profile.get_device()
if device:
	camera_data[ 'device' ] = {
		        'name': device.get_info( pyrealsense2.camera_info.name ),
		  'product_id': device.get_info( pyrealsense2.camera_info.product_id ),
		'product_line': device.get_info( pyrealsense2.camera_info.product_line ),
		      'serial': device.get_info( pyrealsense2.camera_info.serial_number )
		}
	depth_sensor = device.first_depth_sensor()
	if depth_sensor is not None:
		depth_scale = depth_sensor.get_depth_scale()
		camera_data[ 'depth_scale' ] = depth_scale
pipeline.stop()


data = {
	'devices': divices_data,
	'camera':  camera_data
}
with open( 'data.json', 'wt' ) as file:
	json.dump( data, file, 
		      skipkeys = False,
		  ensure_ascii = True,
		check_circular = True,
		     allow_nan = False,
		           cls = None,
		        indent = '\t',
		    separators = (',', ': '),
		       default = None,
		     sort_keys = False
		)