

import numpy
import sys
import pytools.serialization
import pyui.inspector
import pyui.widgets

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MyEnum',
	   namespace = 'examples.inspector',
	fields_names = []
	)
class MyEnum ( pytools.serialization.Enum ):

	A = None
	B = None

	def __init__ ( self, value ):
		super( MyEnum, self ).__init__( value )

	@classmethod
	def get_possible_values ( cls ):
		return [ 'A', 'B' ]

MyEnum.A = MyEnum( 'A' )
MyEnum.B = MyEnum( 'B' )


@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MyData',
	   namespace = 'examples.inspector',
	fields_names = [
		'bool',
		'bytes',
		'dir',
		'dtype',
		'enum',
		'file',
		'float',
		'int',
		'list',
		'ndarray',
		'shape',
		'text'
		]
	)
class MyData ( pytools.serialization.Serializable ):
	def __init__ ( self ):
		super( MyData, self ).__init__()
		self.bool = True
		self.bytes = b'Bity world'
		self.dir = pytools.path.FilePath( __file__ ).parent()
		self.dtype = numpy.dtype( 'float32' )
		self.enum = MyEnum.A
		self.file = pytools.path.FilePath( __file__ )
		self.float = 3.14
		self.int = 42
		self.list = [1, 2, 3, 4]
		self.ndarray = numpy.arange( 4 )
		self.shape = (1, 2)
		self.text = 'hello text'

data = MyData()

app = pyui.widgets.Application( sys.argv, 'ITECA', application_name='Inspector Example' )
inspector = pyui.inspector.InspectorWidget( data, is_editable=True, is_root=True )
inspector.show()
app.main_window = inspector
app.exec()