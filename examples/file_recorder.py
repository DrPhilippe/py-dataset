# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import threading
import time

# INTERNALS
import pycameras
import pycameras.intel
import pytools.tasks
import pytools.path

# ##################################################
# ###                    MAIN                    ###
# ##################################################

# Create the camera
camera = pycameras.intel.IntelCamera.create(
	logger = pytools.tasks.file_logger( 'logs/intel_camera.log' ),
	  name = 'Intel RealSense D435I'
	)

# Create the recorder
recorder = pycameras.FileRecorder.create(
	                   camera = camera,
	                   logger = pytools.tasks.file_logger( 'logs/file_recorder.log' ),
	         progress_tracker = pytools.tasks.console_progress_logger( 'Recording' ),
	                directory = 'D:/records',
	maximum_number_of_records = 200
	)

# Create a single manager to manage both
manager = pytools.tasks.Manager( camera, recorder )

# Run the two
manager.start_tasks( wait_for_start=True )

# Log camera info
print( camera.get_description() )

# Main loop
while recorder.is_running():
	
	frames = recorder.current_frames

	if 'color' in frames:
		frame = frames[ 'color' ]
		cv2.imshow( 'Color', frame )

	if cv2.waitKey(20) == ord('q'):
		recorder.stop()

# Clean tasks
manager.stop_tasks( wait_for_stop=True )
manager.join_tasks()