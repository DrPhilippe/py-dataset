import pydataset
import cv2
import numpy
import pandas

# ##################################################
# LOAD DATA

e      = pydataset.dataset.get( 'file://D:/Datasets/linemod/groups/raw/groups/duck/examples/example_66' )
# e      = pydataset.dataset.get( 'file://D:/Datasets/linemod/groups/cropped/groups/all/groups/duck/examples/example_33' )
mesh   = e.get_feature( 'mesh_ply' ).value.points
K      = e.get_feature( 'K' ).value
image  = e.get_feature( 'image' ).value
R      = e.get_feature( 'R' ).value
t      = e.get_feature( 't' ).value
render = e.get_feature( 'render_image' ).value
r, j   = cv2.Rodrigues( R )
height = image.shape[0]
width  = image.shape[1]

print( 'K.shape:', K.shape )
print( 'mesh.shape:', mesh.shape )
print( 'image.shape:', image.shape )
print( 'r.shape:', r.shape )
print( 't.shape:', t.shape )
print( 'render.shape:', render.shape )

# ##################################################
# BOUDING RECTANGLE FROM POINTS

# Project point cloud into image
points, j = cv2.projectPoints( mesh, r, t, K, numpy.zeros(5,dtype=numpy.float32) )
points = numpy.reshape( points, (-1,2) ).astype( numpy.float32 )

# Compute bounding rect
br00 = numpy.min( points[:, 0] )
br01 = numpy.min( points[:, 1] )
br10 = numpy.max( points[:, 0] )
br11 = numpy.max( points[:, 1] )
br = numpy.asarray( [[br00, br01], [br10, br11]], dtype=numpy.float32 )
print( 'br:' )
print( '  ({:06.4f}, {:06.4f}) = ({:06.4f}, {:06.4f})'.format( br00, br01, br00/float(width), br01/float(height) ) )
print( '  ({:06.4f}, {:06.4f}) = ({:06.4f}, {:06.4f})'.format( br01, br11, br01/float(width), br11/float(height) ) )

# Draw point cloud
image = pydataset.cv2_drawing.draw_points( image, points[0:-1:8, :], (128,255,128), radius=0, thickness=1 )

# Draw bounding rect
image = cv2.rectangle( image, (br00, br01), (br10, br11), (64,64,255), 1 )
image = pydataset.cv2_drawing.draw_points( image, br.astype(numpy.float32), (64,64,255), radius=1, thickness=2 )

# Show image
cv2.imshow( 'image', image )
cv2.waitKey( 0 )

# Save image
cv2.imwrite( 'examples/bounding-rectangle.jpg', image )

# Save bounding rectangle in pixels coordinates
data = pandas.DataFrame.from_dict({
	'U': [br00, br10],
	'V': [br01, br11]
	})
data.to_csv( str('examples/bounding-rectangle.txt'), float_format='%06.4f', decimal=',', sep=';', index=False )

# Save points and bounding rectangle to csv using normaized coordinates
data_u = numpy.append( points[:, 0], [br00, br10] )
data_v = numpy.append( points[:, 1], [br01, br11] )
data   = pandas.DataFrame.from_dict({
	'U': data_u / width,
	'V': data_v / height
	})
data.to_csv( str('examples/bounding-rectangle.csv'), float_format='%06.4f', decimal=',', sep=';', index=True, index_label='K' )

# ##################################################
# BOUDING RECTANGLE FROM MASK

alpha = render[ :, : ,3 ]
mask  = alpha > 0

# vertical projection, this gives a horizontal line of cells
v_proj = numpy.any( mask, axis=0 )
print( 'v_proj:', v_proj.shape, v_proj.dtype )

# horizontal projection, this gives a vertical line of cells
h_proj = numpy.any( mask, axis=1 )
print( 'h_proj:', h_proj.shape, h_proj.dtype )

# Find non-zeros raws and columns
h_proj_indexes = numpy.nonzero( h_proj )[ 0 ]
v_proj_indexes = numpy.nonzero( v_proj )[ 0 ]
print( 'h_proj_indexes:', h_proj_indexes.shape, h_proj_indexes.dtype )
print( 'v_proj_indexes:', v_proj_indexes.shape, v_proj_indexes.dtype )

# Compute bounding rect
br00 = v_proj_indexes[  0 ] # first true in h
br01 = h_proj_indexes[  0 ] # first true in v
br10 = v_proj_indexes[ -1 ] # last true in h
br11 = h_proj_indexes[ -1 ] # last true in v
br   = numpy.asarray( [[br00, br01], [br10, br11]], dtype=numpy.int32 )
print( 'br:' )
print( '  ({:06.4f}, {:06.4f}) = ({:06.4f}, {:06.4f})'.format( br00, br01, br00/float(width), br01/float(height) ) )
print( '  ({:06.4f}, {:06.4f}) = ({:06.4f}, {:06.4f})'.format( br10, br11, br10/float(width), br11/float(height) ) )

# Change dynamic range of tha mask, h, v
mask   =   mask.astype(numpy.uint8)*225
h_proj = h_proj.astype(numpy.uint8)*225
v_proj = v_proj.astype(numpy.uint8)*225

# Create display image containg the mask
display = numpy.zeros( (height+10, width+10), dtype=numpy.uint8 )
display[ 0:height, 0:width ] = mask

# add the horintal projection
v_proj = numpy.reshape( v_proj, (1, width) )
v_proj = numpy.tile( v_proj, 10 )
v_proj = numpy.reshape( v_proj, (10, width) )
display[ height:height+10, 0:width ] = v_proj

# add the vertical projection
h_proj = numpy.reshape( h_proj, (height, 1) )
h_proj = numpy.repeat( h_proj, 10, axis=0 )
h_proj = numpy.reshape( h_proj, (height, 10) )
display[ 0:height, width:width+10 ] = h_proj


# Lines surronding the blob
display = cv2.cvtColor( display, cv2.COLOR_GRAY2BGR )
display = cv2.line( display, (br00, br01), (width, br01), (255,0,0), 1 )
display = cv2.line( display, (br00, br01), (br00, height), (255,0,0), 1 )
display = cv2.line( display, (br00, br11), (width, br11), (0,0,255), 1 )
display = cv2.line( display, (br10, br01), (br10, height), (0,0,255), 1 )
display = cv2.rectangle( display, (br00, br01), (br10, br11), (0,255,0) )

# Display images
cv2.imshow( 'render', render )
cv2.imshow( 'alpha', alpha )
cv2.imshow( 'mask', mask )
cv2.imshow( 'display', display )
cv2.waitKey( 0 )

# Save images
cv2.imwrite( 'examples/bounding-rectangle.render.jpg', render )
cv2.imwrite( 'examples/bounding-rectangle.alpha.jpg', alpha )
cv2.imwrite( 'examples/bounding-rectangle.mask.jpg', mask )
cv2.imwrite( 'examples/bounding-rectangle.display.jpg', display )
