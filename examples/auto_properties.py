
class Getter:
	def __init__ ( self, name, default ):
		self._name    = name
		self._default = default
	def __call__ ( self, instance ):
		return getattr( instance, '_'+self._name, self._default )

class Setter:
	def __init__ ( self, name ):
		self._name = name
	def __call__ ( self, instance, value ):
		return setattr( instance, '_'+self._name, value )

class Deleter:
	def __init__ ( self, name ):
		self._name = name
	def __call__ ( self, instance ):
		return delattr( instance, '_'+self._name )

class SettingsAttribute:
	def __init__( self, properties ):
		self._properties = properties
	def __call__ ( self, cls ):
		names = []
		for prop in self._properties:
			name    = prop[ 0 ]
			default = prop[ 1 ]
			doc     = prop[ 2 ]
			p = property( Getter(name, default), Setter(name), Deleter(name), doc )
			setattr( cls, name, p )
			names.append( name )
		cls.__properties__ = self._properties
		cls.__properties_names__ = names
		return cls


class Settings:
	def __init__ ( self, **kwargs ):
		for prop in type(self).__properties__:
			name = prop[0]
			default = prop[1]
			setattr( self, '_'+name, kwargs.get(name, default) )

	def log ( self ):
		print( '{}:'.format( type(self).__name__ ) )
		for name in type(self).__properties_names__:
			value = getattr( self, '_'+name )
			print( '    {} ({}): {}'.format(name, type(value).__name__, value) )

@SettingsAttribute([
	('foo', 1, "foo property"),
	('bar', False, "bar property")
	])
class TestSettings( Settings ):
	def __init__( self, **kwargs ):
		super( TestSettings, self ).__init__( **kwargs )


if __name__ == '__main__':
	t = TestSettings( foo=42, bar=True )
	t.log()
	

