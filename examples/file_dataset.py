import numpy
import pydataset.dataset
import pytools.path

dataset = pydataset.dataset.get_or_create( 'file://D:/datasets/file-dataset' )

for group_index in range( 2 ):
	
	group = dataset.get_or_create_group( 'group_{}'.format(group_index) )
	group.create_or_update_feature(
		     feature_name = 'group_index',
		feature_data_type = pydataset.dataset.IntFeatureData,
		            value = group_index
		)

	for example_index in range( 2 ):

		example = group.get_or_create_example( 'example_{}'.format(example_index) )

		# predefined features
		example.create_or_update_feature(
			     feature_name = 'bool_feature',
			feature_data_type = pydataset.dataset.BoolFeatureData,
			            value = True
			)
		example.create_or_update_feature(
			     feature_name = 'bytes_feature',
			feature_data_type = pydataset.dataset.BytesFeatureData,
			            value = b'hello bytes world'
			)
		example.create_or_update_feature(
			     feature_name = 'float_feature',
			feature_data_type = pydataset.dataset.FloatFeatureData,
			            value = 3.14
			)
		example.create_or_update_feature(
			     feature_name = 'image_feature',
			feature_data_type = pydataset.dataset.ImageFeatureData,
			            value = numpy.zeros( [256, 256], dtype=numpy.uint8 )
			)
		example.create_or_update_feature(
			     feature_name = 'int_feature',
			feature_data_type = pydataset.dataset.IntFeatureData,
			            value = 42
			)
		example.create_or_update_feature(
			     feature_name = 'ndarray_feature',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = numpy.eye(3, dtype=numpy.float32)
			)

		# putools.path
		example.create_or_update_feature(
			     feature_name = 'directory_path',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pytools.path.DirectoryPath( 'C:\\Program Files' )
			)
		example.create_or_update_feature(
			     feature_name = 'file_path',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pytools.path.FilePath( 'log.txt' )
			)

		# pydataset.data
		example.create_or_update_feature(
			     feature_name = 'color',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pydataset.data.ColorData( 255, 255, 255, 255 )
			)
		example.create_or_update_feature(
			     feature_name = 'point_2d',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pydataset.data.Point2DData( 1, 1 )
			)
		example.create_or_update_feature(
			     feature_name = 'point_3d',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pydataset.data.Point3DData( 1, 1, 1 )
			)
		example.create_or_update_feature(
			     feature_name = 'rectangle',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pydataset.data.RectangleData( 0, 0, 10, 10 )
			)
		example.create_or_update_feature(
			     feature_name = 'shape',
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			            value = pydataset.data.ShapeData( 1, 2, 3 )
			)

	# for example_index in range( 2 )
	print( group )

# for group_index in range( 2 )
print( dataset )