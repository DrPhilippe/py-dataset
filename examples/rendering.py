import PyQt5.QtCore
import PyQt5.QtWidgets
import pydataset.data
import pydataset.render
import sys
import numpy

def print_mat( m ):
	m = numpy.asarray( m.data() )
	m = numpy.reshape( m, [4,4] )
	print( m )

app = PyQt5.QtWidgets.QApplication( sys.argv )

example   = pydataset.dataset.get( 'file://D:/datasets/linemod/groups/ape/examples/example_0' )
mesh_data = example.get_feature( 'mesh_ply' ).value
K = example.get_feature( 'K' ).value
R = example.get_feature( 'R' ).value
t = example.get_feature( 't' ).value

# Scene
scene = pydataset.render.Scene.create( name='Scene' )
# Scene / Camera
scene.camera = pydataset.render.cameras.OpenCVCamera.create(
	                x1 = 0.0, # left
	                y1 = 0.0, # bottom
	                x2 = 640.0, # right
	                y2 = 480.0, # top
	         near_clip = 0.01,
	          far_clip = 1000.0,
	                 K = K.ravel().tolist(),
	window_coordinates = 'y_down',
	              name = 'Camera',
	            parent = scene
	)
# Scene / Ape
ape = pydataset.render.Entity.create( name='Ape', parent=scene )
ape.local_transform = pydataset.render.numpy_utils.invert_xy().transposed()
# Scene / Ape / Mesh
mesh = pydataset.render.Mesh( mesh_data, parent=ape )
mesh.local_transform = pydataset.render.numpy_utils.affine_transform( R, t ).transposed()

# Viewport
viewport = pydataset.render.Viewport( scene, window_flags=PyQt5.QtCore.Qt.Window )
viewport.resize( 640, 480 )
viewport.show()

app.exec()