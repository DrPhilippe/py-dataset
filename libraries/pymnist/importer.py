# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pydataset.io
import pytools.assertions
import pytools.tasks

# LOCALS
from .importer_settings import ImporterSettings

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pydataset.io.Importer ):
	"""
	MNIST raw dataset importer.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		Arguments:
			self                                 (`pymnist.Importer`): Importer to initialize.
			settings                     (`pymnist.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_group ( self, name, images, labels, count, progress_ratio, progress_offset=0.0 ):
		"""
		Creates the examples of a group (training or testing) using the given images and labels.

		Arguments:
			self (`pydataset.mnist.Importer`): Importer importing the examples.
			name                      (`str`): Name of the group.
			images          (`numpy.ndarray`): Array containing the images (dtype: numpy.uint8, shape: count x 28 x 28)
			labels          (`numpy.ndarray`): Array containing the labels (dtype: numpy.int8, shape: count)
			count                     (`int`): Number of examples.
			progress_ration         (`float`): Progression ration multiplied to the group importation progress.
			progress_offset         (`float`): Progression offset added to the group importation progress.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.value_in( name, ['training', 'testing'] )
		assert pytools.assertions.type_is_instance_of( images, numpy.ndarray )
		assert pytools.assertions.equal( images.dtype, numpy.uint8 )
		assert pytools.assertions.equal( images.shape, (count,28,28) )
		assert pytools.assertions.type_is_instance_of( labels, numpy.ndarray )
		assert pytools.assertions.equal( labels.dtype, numpy.uint8 )
		assert pytools.assertions.equal( labels.shape, (count,) )
		assert pytools.assertions.type_is( count, int )
		assert pytools.assertions.value_in( count, [60000,10000] )
		assert pytools.assertions.type_is( progress_ratio, float )
		assert pytools.assertions.type_is( progress_offset, float )

		# Log
		if ( self.logger ):
			self.logger.log( 'Importing group {}:', name )

		# Create the group
		split_group = self.dst_group.create_group( name )
		
		# Log
		if ( self.logger ):
			self.logger.log( '    {}', split_group.url )

		# Create a sub group for each digit
		categories_groups = []
		for digit in range( 10 ):
			
			# Create the group for the digit
			category_group = split_group.create_group(
				 group_name = str(digit),
				auto_update = False
				)

			# Create the digit feature
			category_group.create_feature(
				feature_data_type = pydataset.dataset.IntFeatureData,
				     feature_name = self.settings.digit_feature_name,
				      auto_update = False,
				            value = digit
				)
			categories_groups.append( category_group )
			
			# Log
			if ( self.logger ):
				self.logger.log( '    {}', category_group.url )

		# for digit in range( 10 )

		# Update the group for the split
		split_group.update()
			
		# Log
		if ( self.logger ):
			self.logger.end_line()
			self.logger.log( 'Creating training examples:' )
			self.logger.flush()

		# Create the example for each image
		for example_index in range( count ):

			image = images[ example_index, :, : ]
			label = labels[ example_index ]

			example = categories_groups[ label ].create_example( auto_update=False )

			example.create_feature(
				feature_data_type = pydataset.dataset.ImageFeatureData,
				     feature_name = self.settings.image_feature_name,
				      auto_update = True,
				            shape = pydataset.data.ShapeData(28, 28),
				            value = image,
				        extension = '.jpg',
				            param = 100
				)

			# Log
			if ( self.logger ):
				self.logger.log( '    {}', example.url )
			
			# Progression
			if ( self.progress_tracker ):
				self.progress_tracker.update( (float(example_index)/float(count))*progress_ratio + progress_offset )

		# for example_index in range( count )
		
		# Update the groups
		for category_group in categories_groups:
			category_group.update()

		# Log
		if ( self.logger ):
			self.logger.log( '    Done' )
			self.logger.end_line()
			self.logger.flush()

	# def create_group ( self, images, labels, count, progress_ratio, progress_offset )
	
	# --------------------------------------------------

	def read_images_file ( self, filepath ):
		"""
		Reads a binary files containing images of the mnist dataset.

		Arguments:
			self        (`pydataset.mnist.Importer`): Importer importing the examples.
			filepath (`str`/`pytools.path.FilePath`): Path of the file.

		Returns:
			magic            (`int`): Magic number
			count            (`int`): Number of images.
			height           (`int`): Height of the images.
			width            (`int`): Width of the images.
			images (`numpy.ndarray`): Array containing the images (dtype: numpy.uint8, shape: count x height x width).
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath(filepath)

		with filepath.open('rb') as file:
				
			# Header
			magic  = int.from_bytes( file.read( 4 ), 'big' )
			count  = int.from_bytes( file.read( 4 ), 'big' )
			height = int.from_bytes( file.read( 4 ), 'big' )
			width  = int.from_bytes( file.read( 4 ), 'big' )

			# Images
			images = file.read()
			images = numpy.frombuffer( images, dtype=numpy.uint8 )
			images = numpy.reshape( images, (count, width, height) )

			return magic, count, height, width, images
		
		# with filepath.open('rb') as file

	# def read_images_file ( filepath )

	# --------------------------------------------------

	def read_labels_file ( self, filepath ):
		"""
		Reads a binary files containing labels of the mnist dataset.

		Arguments:
			self        (`pydataset.mnist.Importer`): Importer importing the examples.
			filepath (`str`/`pytools.path.FilePath`): Path of the file.

		Returns:
			magic    (`numpy.int32`): Magic number
			count    (`numpy.int32`): Number of labels.
			labels (`numpy.ndarray`): Array containing the labels (dtype: numpy.uint8, shape: count).
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( filepath, pytools.path.FilePath )
		
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath(filepath)

		with filepath.open('rb') as file:
			
			# Header
			magic  = int.from_bytes( file.read( 4 ), 'big' )
			count  = int.from_bytes( file.read( 4 ), 'big' )

			# Labels
			labels = file.read()
			labels = numpy.frombuffer( labels, dtype=numpy.uint8 )
			labels = numpy.reshape( labels, (count,) )

			return magic, count, labels
		
		# with filepath.open('rb') as file

	# def read_labels_file ( self, filepath )

	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the mnist dataset.

		Arguments:
			self (`pydataset.mnist.Importer`): Importer used to import the mnist dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		
		# Log
		if ( self.logger ):
			self.logger.log( 'MNIST Importer:' )
			self.logger.log( '    dst_group_url:              {}', self.settings.dst_group_url )
			self.logger.log( '    raw_dataset_directory_path: {}', self.settings.raw_dataset_directory_path )
			self.logger.log( '    training_images_filename:   {}', self.settings.training_images_filename )
			self.logger.log( '    training_labels_filename:   {}', self.settings.training_labels_filename )
			self.logger.log( '    testing_images_filename:    {}', self.settings.testing_images_filename )
			self.logger.log( '    testing_labels_filename:    {}', self.settings.testing_labels_filename )
			self.logger.log( '    training_group_name:        {}', self.settings.training_group_name )
			self.logger.log( '    testing_group_name:         {}', self.settings.testing_group_name )
			self.logger.log( '    image_feature_name:         {}', self.settings.image_feature_name )
			self.logger.log( '    digit_feature_name:         {}', self.settings.digit_feature_name )
			self.logger.log( '    name:                       {}', self.settings.name )
			self.logger.end_line()
			self.logger.log( 'Checking raw dataset directory and files:' )
			self.logger.flush()

		# Check the raw dataset directory
		if self.settings.raw_dataset_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(self.settings.raw_dataset_directory_path) )

		# Log
		if ( self.logger ):
			self.logger.log( '    raw_dataset_directory_path: OK' )
			self.logger.flush()
		
		training_images_filepath = self.settings.raw_dataset_directory_path + pytools.path.FilePath( self.settings.training_images_filename )
		training_labels_filepath = self.settings.raw_dataset_directory_path + pytools.path.FilePath( self.settings.training_labels_filename )
		testing_images_filepath  = self.settings.raw_dataset_directory_path + pytools.path.FilePath( self.settings.testing_images_filename )
		testing_labels_filepath  = self.settings.raw_dataset_directory_path + pytools.path.FilePath( self.settings.testing_labels_filename )

		# Log
		if ( self.logger ):
			self.logger.log( '    training_images_filepath: {}', training_images_filepath )
			self.logger.log( '    training_labels_filepath: {}', training_labels_filepath )
			self.logger.log( '    testing_images_filepath:  {}', testing_images_filepath )
			self.logger.log( '    testing_labels_filepath:  {}', testing_labels_filepath )
			self.logger.flush()

		# Check the raw dataset files
		if training_images_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist.".format(training_images_filepath) )
		if training_labels_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist.".format(training_labels_filepath) )
		if testing_images_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist.".format(testing_images_filepath) )
		if testing_labels_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist.".format(testing_labels_filepath) )

		# Log
		if ( self.logger ):
			self.logger.log( '    training_images_filepath:   OK' )
			self.logger.log( '    training_labels_filepath:   OK' )
			self.logger.log( '    testing_images_filepath:    OK' )
			self.logger.log( '    testing_labels_filepath:    OK' )
			self.logger.end_line()
			self.logger.log( 'Loading training images:' )
			self.logger.flush()

		# Load the training images
		magic, count, height, width, images = self.read_images_file( training_images_filepath )

		# Log
		if ( self.logger ):
			self.logger.log( '    magic:  {}', magic  )
			self.logger.log( '    count:  {}', count  )
			self.logger.log( '    height: {}', height )
			self.logger.log( '    width:  {}', width  )
			self.logger.log( '    images: dtype={}, shape={}', images.dtype, images.shape )
			self.logger.end_line()
			self.logger.log( 'Loading training labels:' )
			self.logger.flush()

		# Check the training images:
		if magic != 2051:
			raise RuntimeError( 'Unexpected training images magic: {} instead of {}'.format(magic, 2051) )
		if count != 60000:
			raise RuntimeError( 'Unexpected training images count: {} instead of {}'.format(count, 60000) )
		if height != 28:
			raise RuntimeError( 'Unexpected training images height: {} instead of {}'.format(height, 28) )
		if width != 28:
			raise RuntimeError( 'Unexpected training images width: {} instead of {}'.format(width, 28) )

		# Load the training labels
		magic, count, labels = self.read_labels_file( training_labels_filepath )
		
		# Check the training labels:
		if magic != 2049:
			raise RuntimeError( 'Unexpected training images magic: {} instead of {}'.format(magic, 2049) )
		if count != 60000:
			raise RuntimeError( 'Unexpected training images count: {} instead of {}'.format(count, 60000) )
		
		# Log
		if ( self.logger ):
			self.logger.log( '    magic:  {}', magic  )
			self.logger.log( '    count:  {}', count  )
			self.logger.log( '    labels: dtype={}, shape={}', labels.dtype, labels.shape )
			self.logger.end_line()
			self.logger.flush()	

		# Create training examples
		self.create_group( 'training', images, labels, 60000, progress_ratio=6.0/7.0 )

		# Log
		if ( self.logger ):
			self.logger.log( 'Loading testing images:' )
			self.logger.flush()

		# Load the testing images
		magic, count, height, width, images = self.read_images_file( testing_images_filepath )

		# Log
		if ( self.logger ):
			self.logger.log( '    magic:  {}', magic  )
			self.logger.log( '    count:  {}', count  )
			self.logger.log( '    height: {}', height )
			self.logger.log( '    width:  {}', width  )
			self.logger.log( '    images: dtype={}, shape={}', images.dtype, images.shape )
			self.logger.end_line()
			self.logger.log( 'Loading testing labels:' )
			self.logger.flush()

		# Check the training images:
		if magic != 2051:
			raise RuntimeError( 'Unexpected testing images magic: {} instead of {}'.format(magic, 2051) )
		if count != 10000:
			raise RuntimeError( 'Unexpected testing images count: {} instead of {}'.format(count, 10000) )
		if height != 28:
			raise RuntimeError( 'Unexpected testing images height: {} instead of {}'.format(height, 28) )
		if width != 28:
			raise RuntimeError( 'Unexpected testing images width: {} instead of {}'.format(width, 28) )

		# Load the training labels
		magic, count, labels = self.read_labels_file( testing_labels_filepath )
		
		# Check the training labels:
		if magic != 2049:
			raise RuntimeError( 'Unexpected testing images magic: {} instead of {}'.format(magic, 2049) )
		if count != 10000:
			raise RuntimeError( 'Unexpected testing images count: {} instead of {}'.format(count, 10000) )
		
		# Log
		if ( self.logger ):
			self.logger.log( '    magic:  {}', magic  )
			self.logger.log( '    count:  {}', count  )
			self.logger.log( '    labels: dtype={}, shape={}', labels.dtype, labels.shape )
			self.logger.end_line()
			self.logger.flush()	

		# Create training examples
		self.create_group( 'testing', images, labels, 10000, progress_ratio=1.0/7.0, progress_offset=6.0/7.0 )

		if ( self.progress_tracker ):
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a mnist improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.mnist.ImporterSettings`: The settings.
		"""
		return ImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.dataset.Importer )