# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.io
import pytools.assertions
import pytools.path
import pytools.serialization

# ##################################################
# ###          CLASS IMPORTER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImporterSettings',
	   namespace = 'pymnist',
	fields_names = [
		'raw_dataset_directory_path',
		'training_images_filename',
		'training_labels_filename',
		'testing_images_filename',
		'testing_labels_filename',
		'training_group_name',
		'testing_group_name',
		'image_feature_name',
		'digit_feature_name'
		]
	)
class ImporterSettings ( pydataset.io.ImporterSettings ):
	"""
	Settings of the MNIST dataset importer.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dst_group_url, raw_dataset_directory_path,
		training_images_filename = 'train-images.idx3-ubyte',
		training_labels_filename = 'train-labels.idx1-ubyte',
		 testing_images_filename = 't10k-images.idx3-ubyte',
		 testing_labels_filename = 't10k-labels.idx1-ubyte',
		     training_group_name = 'training',
		      testing_group_name = 'testing',
		      image_feature_name = 'image',
		      digit_feature_name = 'digit',
		                    name = 'mnist importer'
		):
		"""
		Initializes a new instance of the `pydataset.mnist.ImporterSettings` class.

		Arguments:
			self                       (`pydataset.mnist.ImporterSettings`): Instance to initialize.
			dst_group_url                                           (`str`): URL of the group where to import data to.
			raw_dataset_directory_path (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the raw mnist dataset.
			training_images_filename                                (`str`): Name of the binary file containing the raw training images.
			training_labels_filename                                (`str`): Name of the binary file containing the raw training labels.
			testing_images_filename                                 (`str`): Name of the binary file containing the raw testing images.
			testing_labels_filename                                 (`str`): Name of the binary file containing the raw testing labels.
			training_group_name                                     (`str`): Name of the group where to save training examples.
			testing_group_name                                      (`str`): Name of the group where to save testing examples.
			image_feature_name                                      (`str`): Name to give to the image features.
			digit_feature_name                                      (`str`): Name to give to the digit features.
			name                                                    (`str`): Name of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is_instance_of( raw_dataset_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( name, str )

		super( ImporterSettings, self ).__init__( dst_group_url, name )

		if isinstance( raw_dataset_directory_path, str ):
			raw_dataset_directory_path = pytools.path.DirectoryPath( raw_dataset_directory_path )

		self.raw_dataset_directory_path = raw_dataset_directory_path
		self.training_images_filename = training_images_filename
		self.training_labels_filename = training_labels_filename
		self.testing_images_filename = testing_images_filename
		self.testing_labels_filename = testing_labels_filename
		self.training_group_name = training_group_name
		self.testing_group_name  = testing_group_name
		self.image_feature_name  = image_feature_name
		self.digit_feature_name  = digit_feature_name

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def raw_dataset_directory_path ( self ):
		"""
		Path to the directory containing the raw mnist dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._raw_dataset_directory_path

	# def raw_dataset_directory_path ( self )
	
	# --------------------------------------------------

	@raw_dataset_directory_path.setter
	def raw_dataset_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, pytools.path.DirectoryPath )

		self._raw_dataset_directory_path = value
		
	# def raw_dataset_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def training_images_filename ( self ):
		"""
		Name of the binary file containing the raw training images (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._training_images_filename

	# def training_images_filename ( self )
	
	# --------------------------------------------------

	@training_images_filename.setter
	def training_images_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._training_images_filename = value
		
	# def training_images_filename ( self, value )

	# --------------------------------------------------

	@property
	def training_labels_filename ( self ):
		"""
		Name of the binary file containing the raw training labels (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._training_labels_filename

	# def training_labels_filename ( self )
	
	# --------------------------------------------------

	@training_labels_filename.setter
	def training_labels_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._training_labels_filename = value
		
	# def training_labels_filename ( self, value )

	# --------------------------------------------------

	@property
	def testing_images_filename ( self ):
		"""
		Name of the binary file containing the raw testing images (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._testing_images_filename

	# def testing_images_filename ( self )
	
	# --------------------------------------------------

	@testing_images_filename.setter
	def testing_images_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._testing_images_filename = value
		
	# def testing_images_filename ( self, value )

	# --------------------------------------------------

	@property
	def testing_labels_filename ( self ):
		"""
		Name of the binary file containing the raw testing labels (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._testing_labels_filename

	# def testing_labels_filename ( self )
	
	# --------------------------------------------------

	@testing_labels_filename.setter
	def testing_labels_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._testing_labels_filename = value
		
	# def testing_labels_filename ( self, value )

	# --------------------------------------------------

	@property
	def training_group_name ( self ):
		"""
		Name of the group where to save training examples (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._training_group_name

	# def training_group_name ( self )
	
	# --------------------------------------------------

	@training_group_name.setter
	def training_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._training_group_name = value
		
	# def training_group_name ( self, value )
	
	# --------------------------------------------------

	@property
	def testing_group_name ( self ):
		"""
		Name of the group where to save testing examples (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._testing_group_name

	# def testing_group_name ( self )
	
	# --------------------------------------------------

	@testing_group_name.setter
	def testing_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._testing_group_name = value
		
	# def testing_group_name ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name to give to the image features (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._image_feature_name

	# def image_feature_name ( self )
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
		
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def digit_feature_name ( self ):
		"""
		Name to give to the image features (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._digit_feature_name

	# def digit_feature_name ( self )
	
	# --------------------------------------------------

	@digit_feature_name.setter
	def digit_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._digit_feature_name = value
		
	# def digit_feature_name ( self, value )

# class ImporterSettings ( pydataset.dataset.ImporterSettings )