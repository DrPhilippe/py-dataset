# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .importer_settings import ImporterSettings
from .scene_importer    import SceneImporter

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pydataset.io.Importer ):
	"""
	Importer that imports raw tless dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		Arguments:
			self                                 (`pytless.Importer`): Importer to initialize.
			settings                     (`pytless.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			       dst_group = dst_group,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the tless dataset.

		Arguments:
			self (`pystless.Importer`): Importer used to import the tless dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )

		# Create progression trackers
		if self.progress_tracker:
			models_tracker  = self.progress_tracker.create_child( 0.02 )
			sensors_tracker = self.progress_tracker.create_child( 0.98 )
		else:
			models_tracker  = None
			sensors_tracker = None

		self.import_models( models_tracker )
		self.import_sensors( sensors_tracker )

	# def run ( self )

	# --------------------------------------------------

	def import_models ( self, progress_tracker=None ):
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING MODELS:' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			self.logger.log( '    models_group_name: {}', self.settings.models_group_name )
			self.logger.end_line()
			self.logger.flush()

		# Models directories pathes
		model_cad_dir             = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'models_cad' )
		models_cad_subdivided_dir = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'models_cad_subdivided' )
		models_reconst_dir        = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'models_reconst' )

		# Log
		if self.logger:
			self.logger.log( 'DIRECTORES:' )
			self.logger.log( '     model_cad_dir:             {}', model_cad_dir )
			self.logger.log( '     models_cad_subdivided_dir: {}', models_cad_subdivided_dir )
			self.logger.log( '     models_reconst_dir:        {}', models_reconst_dir )
			self.logger.end_line()
			self.logger.flush()

		# Check
		if model_cad_dir.is_not_a_directory():
			raise IOError( 'The directory {} does not exist'.format(model_cad_dir) )
		if models_cad_subdivided_dir.is_not_a_directory():
			raise IOError( 'The directory {} does not exist'.format(models_cad_subdivided_dir) )
		if models_reconst_dir.is_not_a_directory():
			raise IOError( 'The directory {} does not exist'.format(models_reconst_dir) )

		# Group where to import the models to
		models_group = self.dst_group.create_group( self.settings.models_group_name )

		# Log
		if self.logger:
			self.logger.log( 'GROUP:' )
			self.logger.log( '    models_group: {}', models_group.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Go through each models
		for model_index in range( 1, 31 ):

			# Compose model name
			model_name = self.settings.models_examples_name_format.format( model_index )

			# Compose model filename
			model_filename = model_name + '.ply'

			# Compose model filepath
			model_cad_filepath            = model_cad_dir             + pytools.path.FilePath( model_filename )
			model_cad_subdivided_filepath = models_cad_subdivided_dir + pytools.path.FilePath( model_filename )
			model_reconst_filepath        = models_reconst_dir        + pytools.path.FilePath( model_filename )

			# Log
			if self.logger:
				self.logger.log( 'MODEL {}:', model_index )
				self.logger.log( '    model_index:                   {}', model_index )
				self.logger.log( '    model_name:                    {}', model_name )
				self.logger.log( '    model_filename:                {}', model_filename )
				self.logger.log( '    model_cad_filepath:            {} ({})', model_cad_filepath, model_cad_filepath.is_a_file() )
				self.logger.log( '    model_cad_subdivided_filepath: {} ({})', model_cad_subdivided_filepath, model_cad_subdivided_filepath.is_a_file() )
				self.logger.log( '    model_reconst_filepath:        {} ({})', model_reconst_filepath, model_reconst_filepath.is_a_file() )
				self.logger.flush()

			# Check
			if not model_cad_filepath.is_a_file():
				raise IOError( 'The file "{}" does not exist'.format(model_cad_filepath) )
			if not model_cad_subdivided_filepath.is_a_file():
				raise IOError( 'The file "{}" does not exist'.format(model_cad_subdivided_filepath) )
			if not model_reconst_filepath.is_a_file():
				raise IOError( 'The file "{}" does not exist'.format(model_reconst_filepath) )

			# Load model mesh
			model_cad            = pydataset.render.MeshData.load_ply_v2( model_cad_filepath )
			model_cad_subdivided = pydataset.render.MeshData.load_ply_v2( model_cad_subdivided_filepath )
			model_reconst        = pydataset.render.MeshData.load_ply_v2( model_reconst_filepath )

			# Scale models
			model_cad.points            /= 1000.
			model_cad_subdivided.points /= 1000.
			model_reconst.points        /= 1000.

			# Create an example for the model
			example = models_group.create_example( model_name )

			# Log
			if self.logger:
				self.logger.log( '    example: {}', example.absolute_url )

			# Create category index feature
			if self.settings.category_index_feature_name:
				index_feature = example.create_or_update_feature(
					     feature_name = self.settings.category_index_feature_name,
					feature_data_type = pydataset.dataset.IntFeatureData,
					      auto_update = True,
					            value = model_index-1,
					)
				if self.logger:
					self.logger.log( '    - category index: {}', index_feature.absolute_url )

			# Create one-hot category feature
			if self.settings.one_hot_category_feature_name:
				one_hot_feature = example.create_or_update_feature(
					     feature_name = self.settings.one_hot_category_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					            value = pydataset.classification_utils.index_to_one_hot( model_index-1, 30 ),
					)
				if self.logger:
					self.logger.log( '    - one-hot category: {}', one_hot_feature.absolute_url )

			# Create mesh feature
			if self.settings.mesh_feature_name:
				mesh_feature = example.create_or_update_feature(
					     feature_name = self.settings.mesh_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = model_cad,
					)
				if self.logger:
					self.logger.log( '    - cad: {}', mesh_feature.absolute_url )

			# Create subdivided mesh feature
			if self.settings.mesh_subdivided_feature_name:
				mesh_subdivided_feature = example.create_or_update_feature(
					     feature_name = self.settings.mesh_subdivided_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = model_cad_subdivided,
					)
				if self.logger:
					self.logger.log( '    - cad subdivided: {}', mesh_subdivided_feature.absolute_url )

			# Create reconstructed mesh feature
			if self.settings.mesh_reconstructed_feature_name:
				mesh_reconst_feature = example.create_or_update_feature(
					     feature_name = self.settings.mesh_reconstructed_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = model_reconst,
					)
				if self.logger:
					self.logger.log( '    - cad reconstructed: {}', mesh_reconst_feature.absolute_url )

			# Log
			if self.logger:
				self.logger.log( 'MODEL {} DONE', model_index )
				self.logger.end_line()
				self.logger.flush()
		
			# Update progression
			if progress_tracker:
				progress_tracker.update( (model_index-1.)/30. )

		# for model_index in range( 1, 31 )

		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING MODELS' )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_models ( self )

	# --------------------------------------------------

	def import_sensors ( self, progress_tracker=None ):
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING SENSORS:' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			self.logger.log( '    sensors_group_name: {}', self.settings.sensors_group_name )
			self.logger.end_line()
			self.logger.flush()

		# Create the scenes groups
		sensors_dst_group = self.dst_group.create_group( self.settings.sensors_group_name )

		# Create progress trackers
		progress_trackers=[]
		count = len(self.settings.sensors_names)
		for index in range( count ):
			progress_trackers.append( progress_tracker.create_child() if progress_tracker else None )

		# Import each sensor
		for index in range( count ):
			self.import_sensor(
				self.settings.sensors_names[ index ],
				sensors_dst_group,
				progress_trackers[index]
				)
		
		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING SENSORS' )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_sensors ( self, progress_tracker=None )

	# --------------------------------------------------

	def import_sensor ( self, sensor_name, sensors_dst_group, progress_tracker ):
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is( sensor_name, str )
		assert pytools.assertions.type_is_instance_of( sensors_dst_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING SENSOR "{}":', sensor_name )
			self.logger.end_line()
			self.logger.flush()

		# Create dst group name
		sensor_dst_group = sensors_dst_group.create_group( sensor_name )
		
		# Log
		if self.logger:
			self.logger.log( 'GROUP:' )
			self.logger.log( '    sensor_dst_group: {}', sensor_dst_group.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Create progess trackers
		if progress_tracker:
			train_tracker = progress_tracker.create_child()
			test_tracker  = progress_tracker.create_child()
		else:
			train_tracker = None
			test_tracker  = None

		# import splits
		self.import_split( sensor_name, 'train', sensor_dst_group, train_tracker )
		self.import_split( sensor_name, 'test', sensor_dst_group, test_tracker )

		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING SENSOR "{}"', sensor_name )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_sensor ( self, sensor_name, sensors_dst_group, progress_tracker )
	
	# --------------------------------------------------
	
	def import_split ( self, sensor_name, split_name, sensor_dst_group, progress_tracker ):
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is( sensor_name, str )
		assert pytools.assertions.type_is( split_name, str )
		assert pytools.assertions.type_is_instance_of( sensor_dst_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING SPLIT "{}/{}":', sensor_name, split_name )
			self.logger.end_line()
			self.logger.flush()

		# Directories
		split_dir = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath.format( '{}_{}', split_name, sensor_name )

		# Log
		if self.logger:
			self.logger.log( 'DIRECTORY:' )
			self.logger.log( '    split_dir: {} ({})', split_dir, split_dir.is_a_directory() )
			self.logger.end_line()
			self.logger.flush()

		# Check
		if split_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(split_dir) )

		# Create dst group name
		split_dst_group = sensor_dst_group.create_group( split_name )
				
		# Log
		if self.logger:
			self.logger.log( 'GROUP:' )
			self.logger.log( '    split_dst_group: {}', split_dst_group.absolute_url )
			self.logger.end_line()
			self.logger.log( 'SEARCHING FOR SCENES:' )
			self.logger.flush()
		
		# Seach for scenes
		scenes_importers = []
		scenes_count = 0
		for content in split_dir.list_contents():
			if content.is_a_directory():
				dst_group = split_dst_group.create_group( '{:02d}'.format(scenes_count) )
				scene_importer  = SceneImporter.create(
					              dst_group_url = dst_group.absolute_url,
					   raw_scene_directory_path = content,
					                  dst_group = dst_group,
					                     logger = pytools.tasks.file_logger( 'logs/tless_importer_scene_{:02d}.log'.format(scenes_count) ),
					           progress_tracker = progress_tracker.create_child(),
		                                   name = 'tless_importer_scene_{:02d}.log'.format(scenes_count),
				# models
				              models_group_name = self.settings.models_group_name,
				    models_examples_name_format = self.settings.models_examples_name_format,
				              mesh_feature_name = self.settings.mesh_feature_name,
				   mesh_subdivided_feature_name = self.settings.mesh_subdivided_feature_name,
				mesh_reconstructed_feature_name = self.settings.mesh_reconstructed_feature_name,
				# scenes
				             sensors_group_name = self.settings.sensors_group_name,
				# per image
				     camera_matrix_feature_name = self.settings.camera_matrix_feature_name,
				       depth_image_feature_name = self.settings.depth_image_feature_name,
				       depth_scale_feature_name = self.settings.depth_scale_feature_name,
				       color_image_feature_name = self.settings.color_image_feature_name,
				              mask_feature_name = self.settings.mask_feature_name,
				      visible_mask_feature_name = self.settings.visible_mask_feature_name,
				render_depth_image_feature_name = self.settings.render_depth_image_feature_name,
				render_color_image_feature_name = self.settings.render_color_image_feature_name,
				# per object instance
				    instance_index_feature_name = self.settings.instance_index_feature_name,
				    category_index_feature_name = self.settings.category_index_feature_name,
				  one_hot_category_feature_name = self.settings.one_hot_category_feature_name,
				       translation_feature_name = self.settings.translation_feature_name,
				          rotation_feature_name = self.settings.rotation_feature_name,
				bounding_rectangle_feature_name = self.settings.bounding_rectangle_feature_name,
				        visibility_feature_name = self.settings.visibility_feature_name,
				)
				scenes_importers.append( scene_importer )
				scenes_count += 1
				if self.logger:
					self.logger.log( '    found: {}', content )
			else:
				raise IOError( 'The path item "{}" should not exist'.format(content) )
		
		# Log
		if self.logger:
			self.logger.log( 'DONE SEARCHING FOR SCENES' )
			self.logger.end_line()
			self.logger.flush()

		# Import scenes
		self.manage( scenes_importers )

		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING SPLIT "{}/{}"', sensor_name, split_name )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_split ( self, sensor_name, split_name, sensor_dst_group, progress_tracker )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a tless improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the tless importer settings constructor.

		Returns:
			`pytless.ImporterSettings`: The settings.
		"""
		return ImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.io.Importer )