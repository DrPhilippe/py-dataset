# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###          CLASS IMPORTER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SceneImporterSettings',
	   namespace = 'pytless',
	fields_names = [
		# directory
		'raw_dataset_directory_path',
		# sensors
		'sensors',
		# models
		'models_group_name',
		'models_examples_name_format',
		'mesh_feature_name',
		# scenes
		'sensors_group_name',
		# per image
		'camera_matrix_feature_name',
		'depth_scale_feature_name',
		'depth_image_feature_name',
		'color_image_feature_name',
		'mask_feature_name',
		'visible_mask_feature_name',
		'render_depth_image_feature_name',
		'render_color_image_feature_name',
		# per object instance
		'instance_index_feature_name',
		'category_index_feature_name',
		'one_hot_category_feature_name',
		'translation_feature_name',
		'rotation_feature_name',
		'bounding_rectangle_feature_name',
		'visibility_feature_name'
		]
	)
class SceneImporterSettings ( pydataset.io.ImporterSettings ):
	"""
	Dataset importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dst_group_url, raw_scene_directory_path,
		# models
		              models_group_name = 'objects',
		    models_examples_name_format = 'obj_{:02d}',
		              mesh_feature_name = 'mesh',
		   mesh_subdivided_feature_name = 'mesh_subdivided',
		mesh_reconstructed_feature_name = 'mesh_reconst',
		# scenes
		             sensors_group_name = 'sensors',
		# per image
		     camera_matrix_feature_name = 'K',
		       depth_image_feature_name = 'depth',
		       depth_scale_feature_name = 'depth_scale',
		       color_image_feature_name = 'image',
		              mask_feature_name = 'mask',
		      visible_mask_feature_name = 'mask',
		render_depth_image_feature_name = 'render_depth',
		render_color_image_feature_name = 'render_image',
		# per object instance
		    instance_index_feature_name = 'instance_index',
		    category_index_feature_name = 'category_index',
		  one_hot_category_feature_name = 'one_hot_category',
		       translation_feature_name = 't',
		          rotation_feature_name = 'R',
		bounding_rectangle_feature_name = 'bounding_rectangle',
		        visibility_feature_name = 'visibility',
		# other
		                           name = 'tless-scene_importer'
		):
		"""
		Initializes a new instance of the tless importer settings class.

		Arguments:
			self                               (`pytless.ImporterSettings`): Instance to initialize.
			dst_group_url                                           (`str`): URL of the group where to import data to.
			raw_scene_directory_path   (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the raw linemod dataset.
			models_group_name                                       (`str`): Name of the group in which to import the model.
			models_examples_name_format                             (`str`): Name format for the example where to save the model data and feautres.
			mesh_feature_name                                       (`str`): Name of the feature where to save the mesh.
			mesh_subdivided_feature_name                            (`str`): Name of the feature where to save the subdividedmesh.
			mesh_reconstructed_feature_name                         (`str`): Name of the feature where to save the reconstructed mesh.
			sensors_group_name                                      (`str`): Name of the group in which to import the scensors data.
			camera_matrix_feature_name                              (`str`): Name of the feature where to save the camera parameters
			depth_image_feature_name                                (`str`): Name of the feature where to save the depth image.
			depth_scale_feature_name                                (`str`): Name of the feature where to save the depth scale.
			color_image_feature_name                                (`str`): Name of the feature where to save the color image.
			mask_feature_name                                       (`str`): Name of the feature where to save the mask.
			visible_mask_feature_name                               (`str`): Name of the feature where to save the visible mask.
			render_depth_image_feature_name                         (`str`): Name of the feature where to save the rendered depth image.
			render_color_image_feature_name                         (`str`): Name of the feature where to save the rendered color image.
			instance_index_feature_name                             (`str`): Name of the feature where to save the instance index.
			category_index_feature_name                             (`str`): Name of the feature where to save the category index.
			one_hot_category_feature_name                           (`str`): Name of the feature where to save the one hot category.
			translation_feature_name                                (`str`): Name of the feature where to save the translation vector.
			rotation_feature_name                                   (`str`): Name of the feature where to save the rotation matrix.
			bounding_rectangle_feature_name                         (`str`): Name of the feature where to save the bounding rectangle.
			visibility_feature_name                                 (`str`): Name of the feature where to save the visibility proportion.
			name                                                    (`str`): Name of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is_instance_of( raw_scene_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( models_group_name, str )
		assert pytools.assertions.type_is( models_examples_name_format, str )
		assert pytools.assertions.type_is( mesh_feature_name, str )
		assert pytools.assertions.type_is( mesh_subdivided_feature_name, str )
		assert pytools.assertions.type_is( mesh_reconstructed_feature_name, str )
		assert pytools.assertions.type_is( sensors_group_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( depth_image_feature_name, str )
		assert pytools.assertions.type_is( depth_scale_feature_name, str )
		assert pytools.assertions.type_is( color_image_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( visible_mask_feature_name, str )
		assert pytools.assertions.type_is( render_depth_image_feature_name, str )
		assert pytools.assertions.type_is( render_color_image_feature_name, str )
		assert pytools.assertions.type_is( instance_index_feature_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( visibility_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( SceneImporterSettings, self ).__init__( dst_group_url, name )

		# directory
		self._raw_scene_directory_path        = pytools.path.DirectoryPath.ensure( raw_scene_directory_path )
		# models
		self._models_group_name               = models_group_name
		self._models_examples_name_format     = models_examples_name_format
		self._mesh_feature_name               = mesh_feature_name
		self._mesh_subdivided_feature_name    = mesh_subdivided_feature_name
		self._mesh_reconstructed_feature_name = mesh_reconstructed_feature_name
		# scenes
		self._sensors_group_name              = sensors_group_name
		# per-images
		self._camera_matrix_feature_name      = camera_matrix_feature_name
		self._depth_image_feature_name        = depth_image_feature_name
		self._depth_scale_feature_name        = depth_scale_feature_name
		self._color_image_feature_name        = color_image_feature_name
		self._mask_feature_name               = mask_feature_name
		self._visible_mask_feature_name       = visible_mask_feature_name
		self._render_depth_image_feature_name = render_depth_image_feature_name
		self._render_color_image_feature_name = render_color_image_feature_name
		# per object instance
		self._instance_index_feature_name     = instance_index_feature_name
		self._category_index_feature_name     = category_index_feature_name
		self._one_hot_category_feature_name   = one_hot_category_feature_name
		self._translation_feature_name        = translation_feature_name
		self._rotation_feature_name           = rotation_feature_name
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self._visibility_feature_name         = visibility_feature_name

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def raw_scene_directory_path ( self ):
		"""
		Path to the directory containing the raw tless scene (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )

		return self._raw_scene_directory_path

	# def raw_scene_directory_path ( self )
	
	# --------------------------------------------------

	@raw_scene_directory_path.setter
	def raw_scene_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, pytools.path.DirectoryPath )

		self._raw_scene_directory_path = value
		
	# def raw_scene_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def sensors_names ( self ):
		"""
		Names of the sensors to import (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._sensors_names
	
	# def sensors_names ( self )

	# --------------------------------------------------

	@sensors_names.setter
	def sensors_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
		for name in value:
			assert pytools.assertions.value_in( name, ['canon', 'kinect', 'primesense'] )
	
		self._sensors_names = value
	
	# def sensors_names ( self, value )

	# --------------------------------------------------

	@property
	def models_group_name ( self ):
		"""
		Name of the group in which to import the model (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._models_group_name
	
	# def models_group_name ( self )

	# --------------------------------------------------

	@models_group_name.setter
	def models_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._models_group_name = value
	
	# def models_group_name ( self, value )

	# --------------------------------------------------

	@property
	def models_examples_name_format ( self ):
		"""
		Name format for the example where to save the model data and feautres (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._models_examples_name_format
	
	# def models_examples_name_format ( self )

	# --------------------------------------------------

	@models_examples_name_format.setter
	def models_examples_name_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._models_examples_name_format = value
	
	# def models_examples_name_format ( self, value )
	
	# --------------------------------------------------

	@property
	def mesh_feature_name ( self ):
		"""
		Name of the feature where to save the objects mesh (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._mesh_feature_name
	
	# def mesh_feature_name ( self )

	# --------------------------------------------------

	@mesh_feature_name.setter
	def mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_feature_name = value
	
	# def mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_subdivided_feature_name ( self ):
		"""
		Name of the feature where to save the objects subdivided mesh (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._mesh_subdivided_feature_name
	
	# def mesh_subdivided_feature_name ( self )

	# --------------------------------------------------

	@mesh_subdivided_feature_name.setter
	def mesh_subdivided_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_subdivided_feature_name = value
	
	# def mesh_subdivided_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_reconstructed_feature_name ( self ):
		"""
		Name of the feature where to save the objects reconstructed mesh (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._mesh_reconstructed_feature_name
	
	# def mesh_reconstructed_feature_name ( self )

	# --------------------------------------------------

	@mesh_reconstructed_feature_name.setter
	def mesh_reconstructed_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_reconstructed_feature_name = value
	
	# def mesh_reconstructed_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def sensors_group_name ( self ):
		"""
		Name of the group in which to import the sensors data and scenes (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._sensors_group_name
	
	# def sensors_group_name ( self )

	# --------------------------------------------------

	@sensors_group_name.setter
	def sensors_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._sensors_group_name = value
	
	# def sensors_group_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_image_feature_name ( self ):
		"""
		Name of the feature containing the depth image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._depth_image_feature_name
	
	# def depth_image_feature_name ( self )

	# --------------------------------------------------

	@depth_image_feature_name.setter
	def depth_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_image_feature_name = value
	
	# def depth_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_scale_feature_name ( self ):
		"""
		Name of the feature containing the depth scale (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._depth_scale_feature_name
	
	# def depth_scale_feature_name ( self )

	# --------------------------------------------------

	@depth_scale_feature_name.setter
	def depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_scale_feature_name = value
	
	# def depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def color_image_feature_name ( self ):
		"""
		Name of the feature containing the color image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._color_image_feature_name
	
	# def color_image_feature_name ( self )

	# --------------------------------------------------

	@color_image_feature_name.setter
	def color_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._color_image_feature_name = value
	
	# def color_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._mask_feature_name
	
	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visible_mask_feature_name ( self ):
		"""
		Name of the feature containing the visible mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._visible_mask_feature_name
	
	# def visible_mask_feature_name ( self )

	# --------------------------------------------------

	@visible_mask_feature_name.setter
	def visible_mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visible_mask_feature_name = value
	
	# def visible_mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_depth_image_feature_name ( self ):
		"""
		Name of the feature containing the rendered color image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._render_depth_image_feature_name
	
	# def render_depth_image_feature_name ( self )

	# --------------------------------------------------

	@render_depth_image_feature_name.setter
	def render_depth_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._render_depth_image_feature_name = value
	
	# def render_depth_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_color_image_feature_name ( self ):
		"""
		Name of the feature containing the color image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._render_color_image_feature_name
	
	# def render_color_image_feature_name ( self )

	# --------------------------------------------------

	@render_color_image_feature_name.setter
	def render_color_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._render_color_image_feature_name = value
	
	# def render_color_image_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def instance_index_feature_name ( self ):
		"""
		Name of the feature containing the instance index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._instance_index_feature_name
	
	# def instance_index_feature_name ( self )

	# --------------------------------------------------

	@instance_index_feature_name.setter
	def instance_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._instance_index_feature_name = value
	
	# def instance_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature containing the category index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._category_index_feature_name
	
	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )

	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._translation_feature_name
	
	# def translation_feature_name ( self )

	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_feature_name = value
	
	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._rotation_feature_name
	
	# def rotation_feature_name ( self )

	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_feature_name = value
	
	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature containing the bounding rectangle (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._bounding_rectangle_feature_name
	
	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visibility_feature_name ( self ):
		"""
		Name of the feature containing the visibility proportion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
	
		return self._visibility_feature_name
	
	# def visibility_feature_name ( self )

	# --------------------------------------------------

	@visibility_feature_name.setter
	def visibility_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visibility_feature_name = value
	
	# def visibility_feature_name ( self, value )

# class SceneImporterSettings ( pydataset.io.ImporterSettings )