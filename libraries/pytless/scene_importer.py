# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .scene_importer_settings import SceneImporterSettings

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class SceneImporter ( pydataset.io.Importer ):
	"""
	Importer that imports raw tless scene.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		Arguments:
			self                                 (`pytless.Importer`): Importer to initialize.
			settings                (`pytless.SceneImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )
		assert pytools.assertions.type_is_instance_of( settings, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( SceneImporter, self ).__init__(
			        settings = settings,
			       dst_group = dst_group,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the tless dataset.

		Arguments:
			self (`pystless.SceneImporter`): Importer used to import the tless dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		self.import_scene()

	# def run ( self )

	# --------------------------------------------------

	def import_scene ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		scene_directory  = self.settings.raw_scene_directory_path
		scene_dst_group  = self.dst_group
		progress_tracker = self.progress_tracker

		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING SCENE "{}"', scene_directory )
			self.logger.end_line()
			self.logger.flush()

		# Directories and files
		color_dir          = scene_directory + pytools.path.DirectoryPath( 'rgb' )
		depth_dir          = scene_directory + pytools.path.DirectoryPath( 'depth' )
		rendered_color_dir = scene_directory + pytools.path.DirectoryPath( 'rendered_color' )
		rendered_depth_dir = scene_directory + pytools.path.DirectoryPath( 'rendered_depth' )
		mask_dir           = scene_directory + pytools.path.DirectoryPath( 'mask' )
		gt_file            = scene_directory + pytools.path.FilePath( 'gt.yml' )
		info_file          = scene_directory + pytools.path.FilePath( 'info.yml' )

		# Log
		if self.logger:
			self.logger.log( 'DIRECTORIES AND FILES:' )
			self.logger.log( '    color_dir:          {} ({})', color_dir, color_dir.is_a_directory() )
			self.logger.log( '    depth_dir:          {} ({})', depth_dir, depth_dir.is_a_directory() )
			self.logger.log( '    rendered_color_dir: {} ({})', rendered_color_dir, rendered_color_dir.is_a_directory() )
			self.logger.log( '    rendered_depth_dir: {} ({})', rendered_depth_dir, rendered_depth_dir.is_a_directory() )
			self.logger.log( '    mask_dir:           {} ({})', mask_dir, mask_dir.is_a_directory() )
			self.logger.log( '    gt_file:            {} ({})', gt_file, gt_file.is_a_file() )
			self.logger.log( '    info_file:          {} ({})', info_file, info_file.is_a_file() )
			self.logger.end_line()
			self.logger.flush()

		# Check
		if color_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(color_dir) )
		if depth_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(depth_dir) )
		if rendered_color_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(rendered_color_dir) )
		if rendered_depth_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(rendered_depth_dir) )
		if mask_dir.is_not_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(mask_dir) )
		if gt_file.is_not_a_file():
			raise IOError( 'The file "{}" does not exist'.format(gt_file) )
		if info_file.is_not_a_file():
			raise IOError( 'The file "{}" does not exist'.format(info_file) )

		# Read GT and infos
		gt_data          = gt_file.read_yml()
		info_data        = info_file.read_yml()
		number_of_images = len(gt_data)

		# Log
		if self.logger:
			self.logger.log( 'INFOS:' )
			self.logger.log( '    number_of_images: {}', number_of_images )
			self.logger.end_line()
			self.logger.flush()

		# Import images
		for image_index in range( number_of_images ):
			
			# Log
			if self.logger:
				self.logger.log( 'IMAGE N°{}:', image_index )
				self.logger.flush()

			# Create group for the image
			image_group = scene_dst_group.create_group( '{:04d}'.format(image_index) )

			# Files
			color_file          = color_dir          + pytools.path.FilePath.format( '{:04d}.png', image_index )
			depth_file          = depth_dir          + pytools.path.FilePath.format( '{:04d}.png', image_index )
			rendered_color_file = rendered_color_dir + pytools.path.FilePath.format( '{:04d}.all.png', image_index )
			rendered_depth_file = rendered_depth_dir + pytools.path.FilePath.format( '{:04d}.all.npy', image_index )
			
			# Check
			if color_file.is_not_a_file():
				raise IOError( 'The file "{}" does not exist'.format(color_file) )
			if depth_file.is_not_a_file():
				raise IOError( 'The file "{}" does not exist'.format(depth_file) )
			if rendered_color_file.is_not_a_file():
				raise IOError( 'The file "{}" does not exist'.format(rendered_color_file) )
			if rendered_depth_file.is_not_a_file():
				raise IOError( 'The file "{}" does not exist'.format(rendered_depth_file) )

			# Read images / depth
			color          = color_file.read_image( cv2.IMREAD_COLOR )
			depth          = depth_file.read_image( cv2.IMREAD_ANYDEPTH ).astype( numpy.float16 )
			rendered_color = rendered_color_file.read_image( cv2.IMREAD_COLOR )
			rendered_depth = rendered_depth_file.read_ndarray().astype( numpy.float16 )

			# Read infos:
			cam_K       = numpy.asarray( info_data[ image_index ][ 'cam_K'     ] ).astype( numpy.float32 )
			# cam_R_w2c   = numpy.asarray( info_data[ image_index ][ 'cam_R_w2c' ] ).astype( numpy.float32 )
			# cam_t_w2c   = numpy.asarray( info_data[ image_index ][ 'cam_t_w2c' ] ).astype( numpy.float32 )
			depth_scale = float( info_data[ image_index ][ 'depth_scale' ] )
			# elev        = int( info_data[ image_index ][ 'elev' ] )
			# mode        = int( info_data[ image_index ][ 'mode' ] )
			
			cam_K     = numpy.reshape( cam_K, [3, 3] )
			# cam_R_w2c = numpy.reshape( cam_R_w2c, [3, 3] )
			# cam_t_w2c = numpy.reshape( cam_t_w2c, [3] ) / 1000.

			if self.settings.camera_matrix_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.camera_matrix_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					        value     = cam_K
					)

			if self.settings.depth_scale_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.depth_scale_feature_name,
					feature_data_type = pydataset.dataset.FloatFeatureData,
					      auto_update = True,
					        value     = depth_scale
					)

			# if self.settings.translation_feature_name:
			# 	image_group.create_feature(
			# 		     feature_name = self.settings.translation_feature_name,
			# 		feature_data_type = pydataset.dataset.NDArrayFeatureData,
			# 		      auto_update = True,
			# 		        value     = cam_t_w2c
			# 		)

			# if self.settings.rotation_feature_name:
			# 	image_group.create_feature(
			# 		     feature_name = self.settings.rotation_feature_name,
			# 		feature_data_type = pydataset.dataset.NDArrayFeatureData,
			# 		      auto_update = True,
			# 		        value     = cam_R_w2c
			# 		)

			if self.settings.color_image_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.color_image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					        value     = color,
					        extension = '.png',
					        param     = 0
					)

			if self.settings.depth_image_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.depth_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					        value     = depth
					)

			if self.settings.render_color_image_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.render_color_image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					        value     = rendered_color,
					        extension = '.png',
					        param     = 0
					)

			if self.settings.render_depth_image_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.render_depth_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					        value     = rendered_depth
					)

			# Import instances
			number_of_instances = len( gt_data[ image_index ] )
			for instance_index in range(number_of_instances):

				# Create example
				instance_example = image_group.create_example( '{:04d}'.format(instance_index) )

				# Files
				visible_mask_file   = mask_dir           + pytools.path.FilePath.format( '{:04d}.{:04d}.npy', image_index, instance_index )
				rendered_color_file = rendered_color_dir + pytools.path.FilePath.format( '{:04d}.{:04d}.png', image_index, instance_index )
				rendered_depth_file = rendered_depth_dir + pytools.path.FilePath.format( '{:04d}.{:04d}.npy', image_index, instance_index )
				
				# Check
				if visible_mask_file.is_not_a_file():
					raise IOError( 'The file "{}" does not exist'.format(visible_mask_file) )
				if rendered_color_file.is_not_a_file():
					raise IOError( 'The file "{}" does not exist'.format(rendered_color_file) )
				if rendered_depth_file.is_not_a_file():
					raise IOError( 'The file "{}" does not exist'.format(rendered_depth_file) )

				# Read images / depth
				visible_mask   = visible_mask_file.read_ndarray().astype( numpy.bool )
				rendered_color = rendered_color_file.read_image( cv2.IMREAD_UNCHANGED )
				rendered_depth = rendered_depth_file.read_ndarray().astype( numpy.float16 )
				mask           = numpy.greater( numpy.sum(rendered_color, axis=-1), 0 )
				visiblity      = numpy.sum( visible_mask.astype(numpy.float32), axis=None ) / numpy.sum( mask.astype(numpy.float32), axis=None )
				
				# Read gt:
				cam_R_m2c          = numpy.asarray( gt_data[ image_index ][ instance_index ][ 'cam_R_m2c' ] ).astype( numpy.float32 )
				cam_t_m2c          = numpy.asarray( gt_data[ image_index ][ instance_index ][ 'cam_t_m2c' ] ).astype( numpy.float32 )
				bounding_rectangle = numpy.asarray( gt_data[ image_index ][ instance_index ][ 'obj_bb' ] ).astype( numpy.float32 )
				obj_id             = int(           gt_data[ image_index ][ instance_index ][ 'obj_id' ] )

				# Reshape
				cam_R_m2c          = numpy.reshape( cam_R_m2c, [3, 3] )
				cam_t_m2c          = numpy.reshape( cam_t_m2c, [3] ) / 1000.
				bounding_rectangle = numpy.reshape( bounding_rectangle, [2, 2] )
				bounding_rectangle[ 1, 0 ] += bounding_rectangle[ 0, 0 ]
				bounding_rectangle[ 1, 1 ] += bounding_rectangle[ 0, 1 ]

				# Mesh ref
				model_ref_prefix = 'groups/{}/examples/{}/features/'.format(
					self.settings.models_group_name,
					self.settings.models_examples_name_format.format( obj_id )
					)

				if self.settings.visible_mask_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.visible_mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = visible_mask
						)

				if self.settings.mask_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = mask
						)

				if self.settings.visibility_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.visibility_feature_name,
						feature_data_type = pydataset.dataset.FloatFeatureData,
						      auto_update = False,
						            value = float(visiblity)
						)

				if self.settings.render_color_image_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.render_color_image_feature_name,
						feature_data_type = pydataset.dataset.ImageFeatureData,
						      auto_update = False,
						        value     = rendered_color,
						        extension = '.png',
						        param     = 0
						)

				if self.settings.render_depth_image_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.render_depth_image_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						        value     = rendered_depth
						)

				if self.settings.translation_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.translation_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						        value     = cam_t_m2c
						)

				if self.settings.rotation_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.rotation_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						        value     = cam_R_m2c
						)

				if self.settings.bounding_rectangle_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.bounding_rectangle_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						        value     = bounding_rectangle
						)

				if self.settings.category_index_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.category_index_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						        value     = model_ref_prefix + self.settings.category_index_feature_name,
						)

				if self.settings.one_hot_category_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.one_hot_category_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						        value     = model_ref_prefix + self.settings.one_hot_category_feature_name,
						)

				if self.settings.mesh_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.mesh_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						        value     = model_ref_prefix + self.settings.mesh_feature_name,
						)

				if self.settings.mesh_subdivided_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.mesh_subdivided_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						        value     = model_ref_prefix + self.settings.mesh_subdivided_feature_name,
						)

				if self.settings.mesh_reconstructed_feature_name:
					instance_example.create_feature(
						     feature_name = self.settings.mesh_reconstructed_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						        value     = model_ref_prefix + self.settings.mesh_reconstructed_feature_name,
						)

				instance_example.update()

			# for instance_index in range(number_of_instances)

			# Update progression
			if progress_tracker:
				progress_tracker.update( float(image_index)/float(number_of_images) )

		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING SCENE "{}"', scene_directory )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_scene ( self, scene_directory, split_dst_group, progress_tracker )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a tless scene improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the tless importer settings constructor.

		Returns:
			`pytless.SceneImporterSettings`: The settings.
		"""
		return SceneImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SceneImporter ( pydataset.io.Importer )