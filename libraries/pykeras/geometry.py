# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras.logits
import pytools.assertions

# ##################################################
# ###                  COSTANTS                  ###
# ##################################################

RANDIANS = 2.0 * numpy.pi
DEGREES  = 360.0

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def axis_name_to_index ( axis ):
	"""
	Returns the index corresponding to the given axis name.

	Arguments:
		axis (`str`): One of 'x', 'y', 'z'.

	Returns:
		`int`: The index of the axis.
	"""
	assert pytools.assertions.type_is( axis, str )
	assert pytools.assertions.value_in( axis, ('x', 'y', 'z', 'w') )

	if axis == 'x':
		return 0
	elif axis == 'y':
		return 1
	elif axis == 'z':
		return 2
	elif axis == 'w':
		return 3

# def axis_name_to_index ( axis )

# --------------------------------------------------

def count_correct_distances ( distances, threshold, axis=-1, dtype='int32' ):
	"""
	Compares distances to the given threshold.
	
	Arguments:
		distances (`tensorflow.Tensor`): Batch of ditances.
		threshold             (`float`): Distance threshold.

	Returns:
		(`tensorflow.Tensor`): Thresholded distances
	"""
	correct = compare_distances( distances, threshold )
	return pykeras.logits.count_positives( correct, axis=axis, dtype=dtype )

# def count_correct_distances ( distances, threshold, axis, dtype )

# --------------------------------------------------

def cross_product ( x, y, axis=-1 ):
	"""
	Computes the cross product of x and y (<x, y>).
	"""
	return tensorflow.math.reduce_sum( tensorflow.math.multiply( x, y ), axis=axis )

# --------------------------------------------------

def compare_distances ( distances, threshold ):
	"""
	Compares distances to the given threshold.
	
	Arguments:
		distances (`tensorflow.Tensor`): Batch of ditances.
		threshold             (`float`): Distance threshold.

	Returns:
		(`tensorflow.Tensor`): Thresholded distances
	"""
	
	return tensorflow.math.less_equal( distances, threshold )

# def compare_distances ( distances, threshold )

# --------------------------------------------------

def distances ( x, y, norm_order='euclidean', axis=-1 ):
	"""
	Computes the distance between vectors or matrices.

	Arguments:
		x          (`tensorflow.Tensor`): Batch of vectors or matrices.
		y          (`tensorflow.Tensor`): Batch of vectors or matrices.
		norm_order (`str`/`int`/`float`): Order of the distance norm.
		axis                     (`int`): Axis on which to compute the difference.

	Returns:
		`tensorflow.Tensor`: Batch of distances.
	"""
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_in( norm_order, (float, int, str) )
	if isinstance( norm_order, str ):
		assert pytools.assertions.value_in( norm_order, ('euclidean', 'fro') )

	return tensorflow.math.abs( tensorflow.norm( tensorflow.subtract( x, y ), norm_order, axis ) )

# def distances ( x, y, norm_order, axis )

# --------------------------------------------------

def euler_differences ( e1, e2, axis=-1, euler_units=RANDIANS ):
	"""
	Compute the angle differences between two euler angles.

	Arguments:
		e1 (`tensorflow.Tensor`): Batch of euler angles.
		e2 (`tensorflow.Tensor`): Batch of euler angles.
		axis             (`int`): Axis on which to compute the difference.
		euler_units    (`float`): Euler max range (RANDIANS for radians, DEGREES for degrees).

	Returns:
		`tensorflow.Tensor`: Batch of differences.
	"""
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_is( euler_units, float )

	# Make sure both angles are the smallest positives euler angles
	e1 = tensorflow.math.floormod( e1, euler_units )
	e2 = tensorflow.math.floormod( e2, euler_units )

	# distance in both direction
	ddir1 = tensorflow.math.floormod( tensorflow.math.subtract( e1, e2 ), euler_units )
	ddir2 = tensorflow.math.floormod( tensorflow.math.subtract( e2, e1 ), euler_units )

	# Distance on each coordinates
	return tensorflow.math.minimum( ddir1, ddir2 )

# def euler_differences ( e1, e2, axis, euler_units )

# --------------------------------------------------

def euler_distances ( e1, e2, norm_order='euclidean', axis=-1, euler_units=RANDIANS ):
	"""
	Computes the distance between two euler angles.

	Arguments:
		e1         (`tensorflow.Tensor`): Batch of euler angles.
		e2         (`tensorflow.Tensor`): Batch of euler angles.
		norm_order (`str`/`int`/`float`): Order of the distance norm.
		axis                     (`int`): Axis on which to compute the difference.
		euler_units              (`float`): Euler max range (RANDIANS for radians, DEGREES for degrees).

	Returns:
		`tensorflow.Tensor`: Batch of distances.
	"""
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_in( norm_order, (float, int, str) )
	if isinstance( norm_order, str ):
		assert pytools.assertions.value_in( norm_order, ('euclidean', 'fro') )
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_is( euler_units, float )

	# Euler coordinates differences
	difference = euler_differences( e1, e2, axis, euler_units )

	# Euler distances
	dist = tensorflow.linalg.norm( difference, ord=norm_order, axis=-1 )

	return dist

# def euler_distances ( e1, e2, norm_order, axis )

# --------------------------------------------------

def degrees_to_radians ( angle ):
	"""
	Converts the given angle from degrees to radians.
	
	angle must be a numeric value supporting multiplication and division.

	Arguments:
		angle (`any`): Angle in degree.
	
	Returns:
		`any`: Angle in radians (same type as angle).
	"""
	return angle * numpy.pi / 180.0

# def degrees_to_radians ( angle )

# --------------------------------------------------

def quaternion_distances ( q1, q2, norm_order='euclidean', axis=-1 ):
	"""
	Computes the distance between two quaternion angles.
	
	theta = cos-1( 2 <q1, q2>^2 - 1)

	Links:
		https://math.stackexchange.com/questions/90081/quaternion-distance

	Arguments:
		q1         (`tensorflow.Tensor`): Batch of quaternion angles.
		q2         (`tensorflow.Tensor`): Batch of quaternion angles.
		norm_order (`str`/`int`/`float`): Order of the distance norm.
		axis                     (`int`): Axis on which to compute the difference.

	Returns:
		`tensorflow.Tensor`: Batch of distances in radians.
	"""
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_in( norm_order, (float, int, str) )
	if isinstance( norm_order, str ):
		assert pytools.assertions.value_in( norm_order, ('euclidean', 'fro') )
	assert pytools.assertions.type_is( axis, int )

	return tensorflow.math.acos(
		tensorflow.math.subtract(
			tensorflow.multiply(
				2.0,
				tensorflow.math.square( cross_product( q1, q2 ) )
				),
			1.0
			)
		)

# def quaternion_distances ( e1, e2, norm_order, axis )

# --------------------------------------------------

def quaternion_rough_distances ( q1, q2, norm_order='euclidean', axis=-1 ):
	"""
	Computes the rough distance between two quaternion angles.
	
	d = 1 - cos( theta )/1 = 1 - <q1, q2>^2
	where theta is the angle between the two quaternions.

	It gives 0. whenever the quaternions represent the same orientation,
	and it gives 1. whenever the two orientations are 180 degrees apart.

	Links:
		https://math.stackexchange.com/questions/90081/quaternion-distance

	Arguments:
		q1         (`tensorflow.Tensor`): Batch of quaternion angles.
		q2         (`tensorflow.Tensor`): Batch of quaternion angles.
		norm_order (`str`/`int`/`float`): Order of the distance norm.
		axis                     (`int`): Axis on which to compute the difference.

	Returns:
		`tensorflow.Tensor`: Batch of distances in the range [0., 1.].
	"""
	assert pytools.assertions.type_is( axis, int )
	assert pytools.assertions.type_in( norm_order, (float, int, str) )
	if isinstance( norm_order, str ):
		assert pytools.assertions.value_in( norm_order, ('euclidean', 'fro') )
	assert pytools.assertions.type_is( axis, int )

	return tensorflow.math.acos(
		tensorflow.math.subtract(
			1.0,
			tensorflow.math.square( cross_product( q1, q2 ) )
			)
		)

# def quaternion_rough_distances ( e1, e2, norm_order, axis )

# --------------------------------------------------

def radians_to_degrees ( angle ):
	"""
	Converts the given angle from radians to degree.
	
	angle must be a numeric value supporting multiplication and division.

	Arguments:
		angle (`any`): Angle in radians.
	
	Returns:
		`any`: Angle in degree (same type as angle).
	"""
	return angle * 180.0 / numpy.pi

# def radians_to_degrees ( angle )

# --------------------------------------------------

def select_vector_axes ( tensor, axes ):
	"""
	Selects axes of vectors contained in a batched tensor.
	
	Arguments:
		tensor (`tensorflow.Tensor`): Batch of vectors.

	Returns:
		`tensorflow.Tensor`: Batch of vectors.
	"""
	assert pytools.assertions.type_in( axes, (tuple, str) )
	if isinstance( axes, tuple ):
		assert pytools.assertions.tuple_items_type_is( axes, int )
	else:
		for axis in axes:
			assert pytools.assertions.value_in( axis, 'xyzw' )
		# convert axes to int
		axes = map( axis_name_to_index, list(axes) )

	# Separate each axes of the tensor into separate tensors (batched)
	tensor = tensorflow.unstack( tensor, axis=-1 )
	
	# Select the ones to keep
	vector_axes = []
	for axis in axes:
		vector_axes.append( tensor[ axis ] )
	
	# Stack them together
	return tensorflow.stack( vector_axes, axis=1 )

# def select_vector_axes ( vector, axes )
