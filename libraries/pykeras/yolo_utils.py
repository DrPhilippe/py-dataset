# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow

# INTERNALS
import pytools.assertions

# LOCALS

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def identify_non_empty_cells ( y_true ):
	"""
	Identifies non-empty cells in the labels.

	Non-empty cell have either a non-zero bounding rectangle with confidence 1 or a 1 in the one hot category label part.
	So we can sum over the last dimension (5+C values) and compare the result to 0.

	Arguments:
		y_true (`tensorflow.Tensor`): Labels with shape [BATCH, S, S, 5+C].

	Returns:
		`tensorflow.Tensor`: Non-empty cell mask with shape [BATCH, S, S] where non-empty cells are `True` and empty cells are `False`.
	"""
	cell_sum  = tensorflow.math.reduce_sum( y_true, axis=-1 ) #[BATCH, S, S]
	return      tensorflow.math.greater( cell_sum, 0. ) #[BATCH, S, S]

# def identify_non_empty_cells ( y_true )

# --------------------------------------------------

def slice_labels ( y_true ):
	"""
	Slice the bounding rectangle and the category probabilities from the labels.

	Arguments:
		y_true  (`tensorflow.Tensor`): Labels with shape [BATCH, S, S, 5+C].

	Returns:
		y_true_rect   (`tensorflow.Tensor`): Label bounding rectangle [BATCH, S, S, 5].
		y_true_logits (`tensorflow.Tensor`): Label class probabilities [BATCH, S, S, C].
	"""
	# Usefull values
	DEPTH = 5

	# Slice labels
	y_true_rect   = y_true[ :, :, :, 0:DEPTH ] #[BATCH, S, S, 5]
	y_true_logits = y_true[ :, :, :, DEPTH:  ] #[BATCH, S, S, C]
	
	return y_true_rect, y_true_logits

# def slice_labels ( self, y_true )

# --------------------------------------------------

def slice_predictions ( y_pred, number_of_detectors_per_cell=2 ):
	"""
	Slice the bounding rectangle and the category probabilities from the labels.

	Arguments:
		y_pred         (`tensorflow.Tensor`): Predictions with shape [BATCH, S, S, 5*B+C].
		number_of_detectors_per_cell (`int`): Number of detector in each cell (aka B).
	
	Returns:
		y_pred_rects  (`tensorflow.Tensor`): Predicted bounding rectangle [BATCH, S, S, 5*B].
		y_pred_logits (`tensorflow.Tensor`): Predicted class probabilities [BATCH, S, S, C].
	"""
	# Usefull values
	B     = number_of_detectors_per_cell
	DEPTH = 5

	# Slice predictions
	y_pred_rects  = y_pred[ :, :, :, 0:DEPTH*B ] # [BATCH, S, S, 5*B]
	y_pred_logits = y_pred[ :, :, :, DEPTH*B:  ] # [BATCH, S, S, C]
	
	return y_pred_rects, y_pred_logits

# def slice_predictions ( self, ... )

# --------------------------------------------------

def repeat_labels_rectangles_to_match_predictions ( y_true_rect, number_of_detectors_per_cell=2 ):
	"""
	Repeats the label rectangle as many times as predictions rectangles.

	Arguments:
		y_true         (`tensorflow.Tensor`): Labels rectangles with shape [BATCH, S, S, 5].
		number_of_detectors_per_cell (`int`): Number of detector in each cell (aka B).

	Returns:
		`tensorflow.Tensor`: Repeated labels rectangles with shape [BATCH, S, S, 5*B].
	"""
	
	# Usefull values
	B = number_of_detectors_per_cell

	# Repeat rectangle
	return tensorflow.tile( y_true_rect, [1, 1, 1, B] ) # [BATCH, S, S, B*D]

# def repeat_labels_rectangles_to_match_predictions ( ... )

# --------------------------------------------------

def predicted_rectangles_to_xyxy (
	rectangles,
	number_of_cells=7,
	number_of_detectors_per_cell=2,
	image_size=(1., 1.),
	X=0,
	Y=1
	):
	"""
	Converts yolo predicted bounding rectangles to bounding rectangles with format x_min, y_min, x_max, y_max.
	
	Arguments:
		rectangles     (`tensorflow.Tensor`): Rectangles predicted by yolo [BATCH, S, S, 5*B].
		number_of_cells              (`int`): Number of cells on the width and height of the image (aka. S).
		number_of_detectors_per_cell (`int`): Number of bounding rectangles detector per cell (aka. B).
		image_size (`tuple` of `2` `float`s): Size of the image.
		X                            (`int`): Index of the X coordinate of the rectangles.
		Y                            (`int`): Index of the Y coordinate of the rectangles.

	Returns:
		`tensorflow.Tensor`: The bounding rectangles in x_min, y_min, x_max, y_max format.
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.true( number_of_cells > 0 )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.true( number_of_detectors_per_cell > 0 )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, float )
	assert pytools.assertions.type_is( X, int )
	assert pytools.assertions.type_is( Y, int )

	# Usefull values
	W, H           = X+2, Y+2
	S              = number_of_cells
	B              = number_of_detectors_per_cell
	DEPTH          = 5
	image_width    = image_size[X]
	image_height   = image_size[Y]

	# Check shape / dtype
	if not tensorflow.TensorSpec( [None, S, S, B*DEPTH], tensorflow.dtypes.float32 ).is_compatible_with( rectangles ):
		raise ValueError( "`rectangles` is not compatible with yolo v1 predictions: {}/[None, {}, {}, {}], {}/float32".format(
			list(rectangles.shape),
			S, S,
			B*DEPTH,
			rectangles.dtype
			))

	# Compute other usefull values
	shape_backup      = tensorflow.shape( rectangles )
	BATCH             = shape_backup[ 0 ]
	cells_width       = tensorflow.math.divide( image_width,  float(S) )
	cells_height      = tensorflow.math.divide( image_height, float(S) )
	
	# First reshape the rectangles tensor so that all the rectangles are on axis 0
	rectangles  = tensorflow.reshape( rectangles, [BATCH*S*S*B, DEPTH] )
	coordinates = tensorflow.unstack( rectangles, axis=-1 )

	# The bounding rectangle are localized using their center relatively within the the cell
	# So first we need to express them in pixel within the cell
	coordinates[ X ] = tensorflow.math.multiply( coordinates[ X ], cells_width  )
	coordinates[ Y ] = tensorflow.math.multiply( coordinates[ Y ], cells_height )

	# The bounding rectangle are sized using square root of image relative size.
	# We need to square the rectangle width and height and multiply them by the image width and height.
	coordinates[ W ] = tensorflow.math.multiply( tensorflow.math.square(coordinates[ W ]), image_width  )
	coordinates[ H ] = tensorflow.math.multiply( tensorflow.math.square(coordinates[ H ]), image_height )

	# We need to add the localtion the top-left corner of each cell to the rectangle they contain.
	# This will give us the position of the bounding rectangle center withing the image.
	# We do this using a mask containing the coordinates of the top-left corner of a cell.
	cells_positions_mask = create_cells_origines_mask( S, B, image_size, X, Y ) # [S, S, B*DEPTH]

	# We repeat it for each batch 
	cells_positions_mask = tensorflow.reshape( cells_positions_mask, [1, S, S, B*DEPTH] )
	cells_positions_mask = tensorflow.tile( cells_positions_mask, [BATCH, 1, 1, 1] )
	
	# The mask has the shape of yolo output,
	# so we need to reshape the rectangles back to their original shape
	rectangles = tensorflow.stack( coordinates, axis=-1 )
	rectangles = tensorflow.reshape( rectangles, shape_backup )

	# We can now add the mask to the image to get the rectangles
	# where their location is defined using image pixel coordinates
	rectangles = tensorflow.add( rectangles, cells_positions_mask )

	# Again we reshape the rectangle so that all rectangle are on axis 1
	rectangles  = tensorflow.reshape( rectangles, [BATCH*S*S*B, 5] )
	coordinates = tensorflow.unstack( rectangles, axis=-1 )

	# We remove half the width and height of the bounding rectangle to X and Y to get the top left corner X1 and Y1
	coordinates[ X ] = tensorflow.math.subtract( coordinates[ X ], tensorflow.math.divide( coordinates[ W ], 2. ) )
	coordinates[ Y ] = tensorflow.math.subtract( coordinates[ Y ], tensorflow.math.divide( coordinates[ H ], 2. ) )

	# We also need to clamp by 0 for rectangle that where already 0
	coordinates[ X ] = tensorflow.clip_by_value( coordinates[ X ], 0., image_width )
	coordinates[ Y ] = tensorflow.clip_by_value( coordinates[ Y ], 0., image_height )

	# Now because we want to compute IoUs, it is better to have
	# rectangle in the format x_min, y_min, x_max, y_max.

	# We now add X1 to W and Y1 to H to get X2 and Y2
	coordinates[ W ] = tensorflow.math.add( coordinates[ W ], coordinates[ X ] )
	coordinates[ H ] = tensorflow.math.add( coordinates[ H ], coordinates[ Y ] )

	# We can now revert definetly the shape of the cells rectangles
	rectangles = tensorflow.stack( coordinates, axis=-1 )
	rectangles = tensorflow.reshape( rectangles, shape_backup )

	# And return the result
	return rectangles

# def predicted_rectangles_to_xyxy ( ... )

# -------------------------------------------------

def create_cells_origines_mask (
	number_of_cells=7,
	number_of_detectors_per_cell=2,
	image_size=(1., 1.),
	X=0,
	Y=1
	):
	"""
	Creates an array that can be used to convert from yolo bounding rectangle format to x_min, y_min, x_max, y_max rectangles.
	
	The result is an array of shape [S, S, B*5] containing for each cell the x and y coordinates of the cell's top-left corners.
	The 3 remaining coordinate are set to zeros.

	Arguments:
		number_of_cells              (`int`): Number of cells on the width and height of the image (aka. S).
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell (aka. B).
		image_size (`tuple` of `2` `float`s): Size of the image.
		X                            (`int`): Index of the X coordinate of the rectangles.
		Y                            (`int`): Index of the Y coordinate of the rectangles.
	
	Returns:
		`numpy.ndarray`: Array of shape containing the cells top-left corners coordinates.
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.true( number_of_cells>0 )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.true( number_of_detectors_per_cell>0 )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, float )
	assert pytools.assertions.type_is( X, int )
	assert pytools.assertions.type_is( Y, int )

	# Usefull values
	S            = number_of_cells
	B            = number_of_detectors_per_cell
	DEPTH        = 5 
	cells_width  = image_size[X] / float(S)
	cells_height = image_size[Y] / float(S)

	mask = numpy.zeros( [S, S, 1, DEPTH], dtype=numpy.float32 )

	for y in range(S):
		for x in range(S):
			mask[y, x, 0, X] = cells_width  * float(x)
			mask[y, x, 0, Y] = cells_height * float(y)

	mask = numpy.tile( mask, [1, 1, B, 1] )
	mask = numpy.reshape( mask, [S, S, B*DEPTH] )
	return mask

# def create_cells_origines_mask ( ... )

# -------------------------------------------------

def intersection_over_union ( lhs, rhs, X1=0, Y1=1, X2=2, Y2=3 ):
	"""
	Computes the intersection over union of rectangles.

	Arguments:
		lhs (`tensorflow.Tensor`): Rectangles with shape [BATCH, 5]
		rhs (`tensorflow.Tensor`): Rectangles with shape [BATCH, 5]

	Returns:
		`tensorflow.Tensor`: Intersections over unions with shape [BATCH,]
	"""

	# Check shape / dtype
	if not tensorflow.TensorSpec( [None, 5], tensorflow.dtypes.float32 ).is_compatible_with( lhs ):
		raise ValueError( "`lhs` arguments is not compatible with yolo v1 rectangles: {}/[None, 5], {}/float32".format(
			list(lhs.shape),
			lhs.dtype
			))
	if not tensorflow.TensorSpec( [None, 5], tensorflow.dtypes.float32 ).is_compatible_with( rhs ):
		raise ValueError( "`rhs` arguments is not compatible with yolo v1 rectangles: {}/[None, 5], {}/float32".format(
			list(rhs.shape),
			rhs.dtype
			))

	# Unstack their coordinates
	lhs = tensorflow.unstack( lhs, axis=-1 )
	rhs = tensorflow.unstack( rhs, axis=-1 )

	# compute intersection area
	i_x1     = tensorflow.math.maximum( lhs[ X1 ], rhs[ X1 ] )
	i_y1     = tensorflow.math.maximum( lhs[ Y1 ], rhs[ Y1 ] )
	i_x2     = tensorflow.math.minimum( lhs[ X2 ], rhs[ X2 ] )
	i_y2     = tensorflow.math.minimum( lhs[ Y2 ], rhs[ Y2 ] )
	i_width  = tensorflow.math.subtract( i_x2, i_x1 )
	i_height = tensorflow.math.subtract( i_y2, i_y1 )
	i_width  = tensorflow.where( i_width < 0, tensorflow.zeros_like(i_width), i_width ) # Clamp min width to 0
	i_width  = tensorflow.where( i_height < 0, tensorflow.zeros_like(i_height), i_height ) # Clamp min height to 0
	i_area   = tensorflow.math.multiply( i_width, i_height )
	i_area   = tensorflow.math.abs( i_area )

	# LHS rect area
	lhs_width  = tensorflow.math.subtract( lhs[ X2 ], lhs[ X1 ] )
	lhs_height = tensorflow.math.subtract( lhs[ Y2 ], lhs[ Y1 ] )
	lhs_width  = tensorflow.where( lhs_width < 0, tensorflow.zeros_like(lhs_width), lhs_width ) # Clamp min width to 0
	lhs_height = tensorflow.where( lhs_height < 0, tensorflow.zeros_like(lhs_height), lhs_height ) # Clamp min height to 0
	lhs_area   = tensorflow.math.multiply( lhs_width, lhs_height )
	lhs_area   = tensorflow.math.abs( lhs_area )

	# RHS rect area
	rhs_width  = tensorflow.math.subtract( rhs[ X2 ], rhs[ X1 ] )
	rhs_height = tensorflow.math.subtract( rhs[ Y2 ], rhs[ Y1 ] )
	rhs_width  = tensorflow.where( rhs_width < 0, tensorflow.zeros_like(rhs_width), rhs_width ) # Clamp min width to 0
	rhs_height = tensorflow.where( rhs_height < 0, tensorflow.zeros_like(rhs_height), rhs_height ) # Clamp min height to 0
	rhs_area   = tensorflow.math.multiply( rhs_width, rhs_height )
	rhs_area   = tensorflow.math.abs( rhs_area )

	# Union area
	u_area = tensorflow.math.add( lhs_area, rhs_area )
	u_area = tensorflow.math.subtract( u_area, i_area )
	u_area = tensorflow.math.add( u_area, tensorflow.keras.backend.epsilon() )

	# Compute IoU
	ious = tensorflow.divide( i_area, u_area )
	return tensorflow.clip_by_value( ious, 0., 1. )

# def intersection_over_union ( lhs, rhs )

# --------------------------------------------------

def best_rectangles_matches_mask ( y_true_rects, y_pred_rects, number_of_cells=7, number_of_detectors_per_cell=2 ):
	"""
	Computes a mask that when multiplied to rectangles, only keeps the best rectangles match within cells.

	Arguments:
		y_true         (`tensorflow.Tensor`): Labels rectangles, same size as yolo output rectangles [BATCH, S, S, 5*B].
		y_pred         (`tensorflow.Tensor`): Yolo output rectangles [BATCH, S, S, 5*B].
		number_of_cells              (`int`): Number of cells on the width and height of the image (aka. S).
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell (aka. B).
	
	Returns:
		mask (`tensorflow.Tensor`): Mask that multiplies best match by 1, the rest by 0. [BATCH, S, S, B*DEPTH]
		ious (`tensorflow.Tensor`): IoUs same shape as rectangle [BATCH, S, S, B*DEPTH]
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.true( number_of_cells > 0 )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.true( number_of_detectors_per_cell > 0 )

	# Usefull values
	S     = number_of_cells
	B     = number_of_detectors_per_cell
	DEPTH = 5

	# Check shape / dtype
	if not tensorflow.TensorSpec( [None, S, S, B*DEPTH], tensorflow.dtypes.float32 ).is_compatible_with( y_true_rects ):
		raise ValueError( "`y_true_rects` is not compatible with yolo v1 predictions: {}/[None, {}, {}, {}], {}/float32".format(
			list(y_true_rects.shape),
			S, S,
			B*DEPTH,
			y_true_rects.dtype
			))
	if not tensorflow.TensorSpec( [None, S, S, B*DEPTH], tensorflow.dtypes.float32 ).is_compatible_with( y_pred_rects ):
		raise ValueError( "`y_pred_rects` is not compatible with yolo v1 predictions: {}/[None, {}, {}, {}], {}/float32".format(
			list(y_pred_rects.shape),
			S, S,
			B*DEPTH,
			y_pred_rects.dtype
			))

	# Get batch size
	BATCH = tensorflow.shape( y_true_rects )[ 0 ]

	# Have all rectangles along axis 0
	flat_shape        = [BATCH*S*S*B, DEPTH]
	flat_y_true_rects = tensorflow.reshape( y_true_rects, flat_shape ) # [BATCH*S*S*B, DEPTH]
	flat_y_pred_rects = tensorflow.reshape( y_pred_rects, flat_shape ) # [BATCH*S*S*B, DEPTH]

	# Compute IoUs of all rectangles in all cells
	ious = intersection_over_union( flat_y_true_rects, flat_y_pred_rects ) # [BATCH*S*S*B]
	ious = tensorflow.reshape( ious, [BATCH, S, S, B] ) # [BATCH, S, S, B]
	
	# Get the maximum IoU in each cell
	max_iou = tensorflow.math.reduce_max( ious, axis=-1, keepdims=True ) # [BATCH, S, S, 1]

	# Repeat the maximum IoU for each rectangle of a cell
	max_iou = tensorflow.tile( max_iou, [1, 1, 1, B]) # [BATCH, S, S, B]
	
	# Compute a mask indicating which IoU was the maximum
	max_loc = tensorflow.math.equal( max_iou, ious ) # [BATCH, S, S, B]
	
	# Repeat this mask for all the coordinates of the rectangles
	mask = tensorflow.reshape( max_loc, [BATCH, S, S, B, 1] ) # [BATCH, S, S, B, 1]
	mask = tensorflow.tile( mask, [1, 1, 1, 1, DEPTH] ) # [BATCH, S, S, B, DEPTH]
	mask = tensorflow.reshape( mask, [BATCH, S, S, B*DEPTH] ) # [BATCH, S, S, B*DEPTH]
	
	# # Multiply the labels with the mask to zero out rectangles
	# # that aren the best match in each cell
	# y_true_rects = tensorflow.multiply( y_true_rects, mask )

	# # Multiply the labels with the mask to zero out rectangles
	# # that aren the best match in each cell
	# y_pred_rects = tensorflow.multiply( y_pred_rects, mask )

	# Reshape and add 0 in front of IoUs to have them aligned with the rectangles confidence
	ious = tensorflow.reshape( ious, [BATCH*S*S*B, 1] )
	z    = tensorflow.zeros_like( ious )
	ious = tensorflow.concat( [z,z,z,z,ious], axis=-1 )
	ious = tensorflow.reshape( ious, [BATCH,S,S, B*DEPTH] )

	# Return them
	return mask, ious

# def best_rectangles_matches_mask ( ... )

# --------------------------------------------------

def xyxy_rectangles_to_yolo (
	rectangles,
	number_of_cells=7,
	number_of_detectors_per_cell=2,
	image_size=(1., 1.),
	X=0,
	Y=1
	):
	"""
	Converts rectangles in the format x_min, y_min, x_max, y_max to yolo target rectangle.

	Arguments:
		rectangles     (`tensorflow.Tensor`): Rectangles with shape [BATCH, S, S, 5*B]
		number_of_cells              (`int`): Number of cells on the width and height of the image (aka. S).
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell (aka. B).
		image_size (`tuple` of `2` `float`s): Size of the image.
		X                            (`int`): Index of the X coordinate of the rectangles.
		Y                            (`int`): Index of the Y coordinate of the rectangles.

	Returns:
		`tensorflow.Tensor`: Converted rectangles with shape [BATCH, S, S, 5*B].
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.true( number_of_cells > 0 )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.true( number_of_detectors_per_cell > 0 )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, float )
	assert pytools.assertions.type_is( X, int )
	assert pytools.assertions.type_is( Y, int )
	
	# usefull Values
	X1, Y1, X2, Y2 = X, Y, X+2, Y+2
	S              = number_of_cells
	B              = number_of_detectors_per_cell
	DEPTH          = 5
	image_width    = image_size[X]
	image_height   = image_size[Y]
	cells_height   = image_height / float(number_of_cells)
	cells_width    = image_width  / float(number_of_cells)

	# Check shape
	if not tensorflow.TensorSpec( [None, S, S, B*DEPTH], tensorflow.dtypes.float32 ).is_compatible_with( rectangles ):
		raise ValueError( "`rectangles` is not compatible with yolo v1 predictions: {}/[None, {}, {}, {}], {}/float32".format(
			list(rectangles.shape),
			S, S,
			B*DEPTH,
			rectangles.dtype
			))

	# Save input shape
	shape_backup = tensorflow.shape( rectangles )
	BATCH        = shape_backup[  0 ]

	# Split each coordinates
	rectangles = tensorflow.reshape( rectangles, [ BATCH*S*S*B, DEPTH] )
	coordinates = tensorflow.unstack( rectangles, axis=-1 )

	# Rename coordinates
	X1s = coordinates[ X1 ]
	Y1s = coordinates[ Y1 ]
	X2s = coordinates[ X2 ]
	Y2s = coordinates[ Y2 ]

	# Compute rectangle width and height in pixel coordinates within the image
	WIDTHs  = tensorflow.subtract( X2s, X1s )
	HEIGHTs = tensorflow.subtract( Y2s, Y1s )

	# Compute the coordinates of the bounding rectangles centers in pixel coordinates within the image
	CXs = tensorflow.math.add( X1s, tensorflow.math.divide( WIDTHs, 2.  ) )
	CYs = tensorflow.math.add( Y1s, tensorflow.math.divide( HEIGHTs, 2. ) )
	
	# Compute the coorinates of the cells each rectangle belongs to
	# in pixel coordinates within the image
	CELL_IXs = tensorflow.math.floor( tensorflow.math.divide( CXs, cells_width  ) )
	CELL_IYs = tensorflow.math.floor( tensorflow.math.divide( CYs, cells_height ) )
	CELL_Xs  = tensorflow.math.multiply( CELL_IXs, cells_width  )
	CELL_Ys  = tensorflow.math.multiply( CELL_IYs, cells_height )
	
	# bounding rectangles centers with respect to the cell they belong to
	CXs = tensorflow.math.divide( tensorflow.math.subtract( CXs, CELL_Xs ), cells_width  )
	CYs = tensorflow.math.divide( tensorflow.math.subtract( CYs, CELL_Ys ), cells_height )
	
	# Square root of the bounding rectangle width and height with respect to the image size
	WIDTHs  = tensorflow.math.sqrt( tensorflow.math.divide( WIDTHs,  image_width  ) )
	HEIGHTs = tensorflow.math.sqrt( tensorflow.math.divide( HEIGHTs, image_height ) )

	# Put back the tensor
	coordinates[ X1 ] = CXs
	coordinates[ Y1 ] = CYs
	coordinates[ X2 ] = WIDTHs
	coordinates[ Y2 ] = HEIGHTs
	result = tensorflow.stack( coordinates, axis=-1 )
	return tensorflow.reshape( result, shape_backup )

# def xyxy_rectangles_to_yolo ( ... )