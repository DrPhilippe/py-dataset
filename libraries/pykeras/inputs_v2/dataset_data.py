# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .mappers import MapperSettings

# ##################################################
# ###             CLASS DATASET-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'batch_size',
		'drop_batch_reminder',
		'shuffle',
		'repeat',
		'mappers',
		'name'
		]
	)
class DatasetData ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		initial_features_names=[],
		batch_size=32,
		drop_batch_reminder=False,
		shuffle=True,
		repeat=False,
		mappers=[],
		prefetch_size=8,
		name='dataset'
		):
		"""
		Initialize a new instance of the `pykeras.inputs.DatasetData` class.

		Arguments:
			self                 (`pykeras.inputs.DatasetData`): Instance to initialize.
			initial_features_names           (`list` of `str`s): Name of the feautre to load from the dataset (before batching and mapping).
			batch_size                                  (`int`): Number of examples included in each batch.
			drop_batch_reminder                        (`bool`): Should the remaining examples be dropped when batching.
			shuffle                                    (`bool`): Should examples be shuffled.
			repeat                               (`bool`/`int`): Number of repetition of the dataset.
			mappers (`list` of `pykeras.inputs.MapperSettings`): Settings of the online mappers applied to this dataset.
			prefetch_size                               (`int`): Number of batch to prefetch.
			name                                        (`str`): Name of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( initial_features_names, list )
		assert pytools.assertions.list_items_type_is( initial_features_names, str )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( drop_batch_reminder, bool )
		assert pytools.assertions.type_is( shuffle, bool )
		assert pytools.assertions.type_in( repeat, (bool, int) )
		assert pytools.assertions.type_is( mappers, list )
		assert pytools.assertions.list_items_type_is( mappers, MapperSettings )
		assert pytools.assertions.type_is( name, str )

		self._initial_features_names = copy.deepcopy( initial_features_names )
		self._batch_size             = batch_size
		self._drop_batch_reminder    = drop_batch_reminder
		self._mappers                = copy.deepcopy( mappers )
		self._name                   = name
		self._repeat                 = repeat
		self._prefetch_size          = prefetch_size
		self._shuffle                = shuffle

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def initial_features_names ( self ):
		"""
		Name of the feautre to load from the dataset (before batching and mapping) (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		
		return self._initial_features_names

	# def initial_features_names ( self )

	# --------------------------------------------------

	@property
	def batch_size ( self ):
		"""
		Number of examples included in each batch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._batch_size
	
	# def batch_size ( self )

	# --------------------------------------------------

	@batch_size.setter
	def batch_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		
		self._batch_size = value
	
	# def batch_size ( self, value )
	
	# --------------------------------------------------

	@property
	def drop_batch_reminder ( self ):
		"""
		Should the remaining examples be dropped when batching (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._drop_batch_reminder
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@drop_batch_reminder.setter
	def drop_batch_reminder ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, bool )

		self._drop_batch_reminder = value
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@property
	def shuffle ( self ):
		"""
		Should examples be shuffled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._shuffle
	
	# def shuffle ( self )

	# --------------------------------------------------

	@shuffle.setter
	def shuffle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, bool )

		self._shuffle = value
	
	# def shuffle ( self, value )

	# --------------------------------------------------

	@property
	def repeat ( self ):
		"""
		Number of repetition of the dataset (`bool`/`int`).

		If its a boolean: `False` equals to 0 repetitions (1 epoch), `True` equal to `1` repetition (2 epochs).
		Otherwise it is the number of repetitions and the number of epochs is `repeat+1`.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._repeat

	# def repeat ( self )
	
	# --------------------------------------------------

	@repeat.setter
	def repeat ( self, value ):		
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_in( value, (int, bool) )
		
		self._repeat = value

	# def repeat ( self, value )

	# --------------------------------------------------

	@property
	def mappers ( self ):
		"""
		Settings of the mappers applied to this dataset (`list` of `pykeras.inputs.MapperSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._mappers
	
	# def mappers ( self )

	# --------------------------------------------------

	@mappers.setter
	def mappers ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, MapperSettings )

		self._mappers = copy.deepcopy( value )
	
	# def mappers ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this dataset sequence (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._name
	
	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def prefetch_size ( self ):
		"""
		Number of batch to prefetch while the model is consuming the current one (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._prefetch_size
	
	# def prefetch_size ( self )

	# --------------------------------------------------

	@prefetch_size.setter
	def prefetch_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, int )

		self._prefetch_size = value
	
	# def prefetch_size ( self )

# class DatasetData ( pytools.serialization.Serializable )