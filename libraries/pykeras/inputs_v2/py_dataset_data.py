# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .dataset_data import DatasetData

# ##################################################
# ###           CLASS PY-DATASET-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PyDatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'url_search_pattern'
		]
	)
class PyDatasetData ( DatasetData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		url_search_pattern,
		**kwargs
		):
		"""
		Initialize a new instance of the `pykeras.inputs.PyDatasetData` class.

		Arguments:
			self        (`pykeras.inputs.PyDatasetData`): Instance to initialize.
			url_search_pattern                   (`str`): URL used to find examples of this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDatasetData )
		assert pytools.assertions.type_is( url_search_pattern, str )

		super( PyDatasetData, self ).__init__( **kwargs )

		self._url_search_pattern = url_search_pattern

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL used to find examples of this dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDatasetData )

		return self._url_search_pattern
	
	# def url_search_pattern ( self )

	# --------------------------------------------------

	@url_search_pattern.setter
	def url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PyDatasetData )
		assert pytools.assertions.type_is( value, str )

		self.url_search_pattern = value
	
	# def url_search_pattern ( self )

# class PyDatasetData ( DatasetData )