# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .              import mappers
from .dataset_data  import DatasetData

# ##################################################
# ###               CLASS DATASET                ###
# ##################################################

class Dataset:
	"""
	Base class for datasets used as model inputs.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, data ):
		"""
		Initialize a new instance of the `pykeras.inputs.Dataset` class.

		Arguments:
			self     (`pykeras.inputs.Dataset`): Instance to initialize.
			data (`pykeras.inputs.DatasetData`): Data describing the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( data, DatasetData )

		# Initialize fields
		self._data    = data
		self._mappers = []

		# Reload mappers
		# self._load_online_mappers()

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing the dataset (`pykeras.inputs.DatasetData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self._data
	
	# def data ( self )
	
	# --------------------------------------------------

	@property
	def mappers ( self ):
		"""
		Mappers applied to the dataset (`list` of `pykeras.inputs.OnlineMapper`).

		Mappers are applied in the order of appearance in this list.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		
		return self._mappers
	
	# def mappers ( self )
	
	# --------------------------------------------------

	@property
	def mappers_names ( self ):
		"""
		Names of the mappers applied to the dataset (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		
		return [mapper.name for mapper in self._mappers]
	
	# def mappers_names ( self )

	# --------------------------------------------------

	@property
	def raw_features_names ( self ):
		"""
		Names of features before application of the mappers (`list` of `str`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		raise NotImplementedError()
	
	# def raw_features_names ( self )

	# --------------------------------------------------

	@property
	def initial_features_names ( self ):
		"""
		Name of the feautre to load from the dataset (before batching and mapping) (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		
		return self.data.initial_features_names

	# def initial_features_names ( self )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of features after application of the mappers (`list` of `str`s/`tuple` of `list`s of `str`s).

		If the features are not grouped, then is is the list of their names (`list` of `str`s).
		If they are grouped then it is a tuple with `3` entries: `(inputs, labels, sample_weights)`
		Each of them is then a list of features names (`list` of `str`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		if self.initial_features_names:
			features_names = list(self.initial_features_names)
		else:
			features_names = list(self.raw_features_names)

		for mapper in self.mappers:
			features_names = mapper.map_features_names( features_names )
		
		return features_names

	# def features_names ( self )

	# --------------------------------------------------

	@property
	def batch_size ( self ):
		"""
		Number of examples included in each batch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.batch_size
	
	# def batch_size ( self )
	
	# --------------------------------------------------

	@property
	def drop_batch_reminder ( self ):
		"""
		Should the remaining examples be dropped when batching (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.drop_batch_reminder
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@property
	def shuffle ( self ):
		"""
		Should examples be shuffled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.shuffle
	
	# def shuffle ( self )
	
	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.name
	
	# def name ( self )
		
	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		raise NotImplementedError()
	
	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def steps_per_epoch ( self ):
		"""
		Number of steps per epoch (`int`).

		This is actually the number of batch required to complete one epoch.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		if self.batch_size == 0:
			return self.number_of_examples
			
		# Number of complete steps
		steps_per_epoch = self.number_of_examples // self.batch_size

		# Are batch reminders kept ? Is there a batch reminder ?
		if not self.drop_batch_reminder and (self.number_of_examples % self.batch_size) != 0:
			
			# Then one more batch must be done
			steps_per_epoch += 1

		# Result
		return steps_per_epoch

	# def steps_per_epoch ( self )
	
	# --------------------------------------------------

	@property
	def prefetch_size ( self ):
		"""
		Number of batch to prefetch in parallel (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.prefetch_size
	
	# def prefetch_size ( self )
	
	# --------------------------------------------------

	@property
	def repeat ( self ):
		"""
		Number of repetition of the dataset (`bool`/`int`).

		If it is a boolean:
			* `False` equals to 0 repetitions (1 epoch)
			* `True` equal to inifinit repetition (2 epochs).
		
		If it is an integere:
			* -1 is infinite repetitions
			* 0 is 1 epoch (no-repetitions)
			* Otherwise the number of epochs is `repeat`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.repeat

	# def repeat ( self )

	# --------------------------------------------------
	
	@property
	def number_of_repetitions ( self ):
		"""
		Number of repetitions (`int`).
		"""
		if self.repeat:
			return self.repeat if isinstance( self.repeat, int ) else -1
		else:
			return 1

	# def number_of_repetitions ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_mapper ( self, mapper_or_mapper_type, **kwargs ):
		"""
		Adds the given mapper to the list of mappers applied to this dataset.
	
		Mappers are applied only when the dataset is used.

		If a mapper type is passed instead of a mapper instance,
		a new instance of that mapper type is intantiated
		using any named arguments given to this method after the mapper type.

		Arguments:
			self                        (`pykeras.inputs.Dataset`): Dataset to map.
			mapper_or_mapper_type (`type`/`pykeras.inputs.Mapper`): Mapper to add.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( mapper_or_mapper_type, (type, mappers.Mapper) )

		if isinstance( mapper_or_mapper_type, type ):
			mapper = mapper_or_mapper_type.create( **kwargs )
		else:
			mapper = mapper_or_mapper_type

		self._mappers.append( mapper )
		self._data._mappers.append( mapper.settings )
	
	# def add_mapper ( self, mapper_or_mapper_type, **kwargs )

	# --------------------------------------------------

	def summary ( self, logger ):
		"""
		Prints a short description of this dataset.

		Arguments:
			self (`pykeras.inputs.Dataset`): Dataset to summarize.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( logger, pytools.tasks.Logger )

		logger.log( 'Dataset "{}" ({}):', self.name, type(self).__name__ )
		logger.log( '    steps_per_epoch     : {}', self.steps_per_epoch )
		logger.log( '    number_of_examples  : {}', self.number_of_examples )
		logger.log( '    repeat              : {}', self.repeat )
		logger.log( '    batch_size          : {}', self.batch_size )
		logger.log( '    drop_batch_reminder : {}', self.drop_batch_reminder )
		logger.log( '    shuffle             : {}', self.shuffle )
		logger.log( '    prefetch_size       : {}', self.prefetch_size )
		logger.log( '    mappers             : {}', self.mappers_names )

		features_names = self.features_names
		if isinstance( features_names, list ):
			logger.log( '    features_names      : {}', self.features_names )
		else:	
			logger.log( '    inputs_names        : {}', features_names[0] )
			logger.log( '    labels_names        : {}', features_names[1] )
			logger.log( '    sample_weights_names: {}', features_names[2] )

		logger.log( '' )
		logger.end_line()
		logger.flush()

	# def summary ( self, logger )

	# --------------------------------------------------

	def use ( self, logger ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self        (`pykeras.inputs.Dataset`): Dataset to use.
			logger (`None`/`pytools.tasks.Logger`): logger used to log.

		Returns:
			`object`: A type accepted by `tensorflow.keras.Model.fit()`
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		raise NotImplementedError()

	# def use ( self )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __len__ ( self ):
		"""
		Number of steps to complete one epoch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.steps_per_epoch

	# def __len__ ( self )