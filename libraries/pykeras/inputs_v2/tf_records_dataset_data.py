# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .dataset_data import DatasetData

# ##################################################
# ###       CLASS TF-RECORDS-DATASET-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TFRecordsDatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'directory_path',
		'tf_record_filename',
		'blueprint_filename',
		'shuffle_buffer_size',
		]
	)
class TFRecordsDatasetData ( DatasetData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		directory_path,
		tf_record_filename='{}.tfrecords',
		blueprint_filename='{}.json',
		shuffle_buffer_size=512,
		**kwargs
		):
		"""
		Initialize a new instance of the `pykeras.inputs.TFRecordsDatasetData` class.

		Arguments:
			self        (`pykeras.inputs.TFRecordsDatasetData`): Instance to initialize.
			directory_path (`str`/`pytools.path.DirectoryPath`): Location of the tfrecord and blueprint files.
			tf_record_filename                          (`str`): TFRecord filename.
			blueprint_filename                          (`str`): Blueprint filename.
			shuffle_buffer_size                         (`int`): Size of the shuffling buffer.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )
		assert pytools.assertions.type_is_instance_of( directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( tf_record_filename, str )
		assert pytools.assertions.type_is( blueprint_filename, str )
		assert pytools.assertions.type_is( shuffle_buffer_size, int )

		super( TFRecordsDatasetData, self ).__init__( **kwargs )

		self._directory_path      = pytools.path.DirectoryPath( directory_path ) if isinstance( directory_path, str ) else copy.deepcopy( directory_path )
		self._tf_record_filename  = tf_record_filename
		self._blueprint_filename  = blueprint_filename
		self._shuffle_buffer_size = shuffle_buffer_size

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def directory_path ( self ):
		"""
		Location of directory containing the tfrecord and blueprint files (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )

		return self._directory_path
	
	# def directory_path ( self )

	# --------------------------------------------------

	@directory_path.setter
	def directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._directory_path = pytools.path.DirectoryPath( value ) if isinstance( value, str ) else copy.deepcopy( value )
	
	# def directory_path ( self )

	# --------------------------------------------------

	@property
	def tf_record_filename ( self ):
		"""
		TFRecord filename (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )

		return self._tf_record_filename
	
	# def tf_record_filename ( self )

	# --------------------------------------------------

	@tf_record_filename.setter
	def tf_record_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )
		assert pytools.assertions.type_is( value, str )

		self._tf_record_filename = value
	
	# def tf_record_filename ( self )

	# --------------------------------------------------

	@property
	def blueprint_filename ( self ):
		"""
		Blueprint filename (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )

		return self._blueprint_filename
	
	# def blueprint_filename ( self )

	# --------------------------------------------------

	@blueprint_filename.setter
	def blueprint_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )
		assert pytools.assertions.type_is( value, str )

		self._blueprint_filename = value
	
	# def blueprint_filename ( self )

	# --------------------------------------------------

	@property
	def shuffle_buffer_size ( self ):
		"""
		Size of the shuffling buffer (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )

		return self._shuffle_buffer_size
	
	# def shuffle_buffer_size ( self )

	# --------------------------------------------------

	@shuffle_buffer_size.setter
	def shuffle_buffer_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDatasetData )
		assert pytools.assertions.type_is( value, int )

		self._shuffle_buffer_size = value
	
	# def shuffle_buffer_size ( self )

# class TFRecordsDatasetData ( DatasetData )