# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_mapper                        import ImagesMapper
from .images_random_gaussian_blur_settings import ImagesRandomGaussianBlurSettings

# ##################################################
# ###     CLASS IMAGES-RANDOM-GAUSSIAN-BLUR      ###
# ##################################################

class ImagesRandomGaussianBlur ( ImagesMapper ):
	"""
	Mapper used to randomly blur images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomGaussianBlur` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomGaussianBlur`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomGaussianBlurSettings`): Mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlur )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomGaussianBlurSettings )

		super( ImagesRandomGaussianBlur, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def blur_image ( self, image, random_kernel_size, random_sigma ):
		"""
		Blur the image using the given random paramters.

		Arguments:
			self (`pykeras.inputs.ImagesRandomGaussianBlur`): Mapper instance.
			image                      (`tensorflow.Tensor`): Image to blue.
			random_kernel_size         (`tensorflow.Tensor`): Random kernel size.
			random_sigma               (`tensorflow.Tensor`): Random standard deviation.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlur )
		assert pytools.assertions.type_is_instance_of( image, tensorflow.Tensor )
		assert pytools.assertions.type_is_instance_of( random_kernel_size, tensorflow.Tensor )
		assert pytools.assertions.type_is_instance_of( random_sigma, tensorflow.Tensor )

		def _gaussian_kernel( kernel_size, sigma, n_channels, dtype ):
			"""
			https://stackoverflow.com/questions/59286171/gaussian-blur-image-in-dataset-pipeline-in-tensorflow
			"""
			x = tensorflow.range(-kernel_size // 2 + 1, kernel_size // 2 + 1, dtype=dtype)
			g = tensorflow.math.exp(-(tensorflow.pow(x, 2) / (2 * tensorflow.pow(tensorflow.cast(sigma, dtype), 2))))
			g_norm2d = tensorflow.pow(tensorflow.reduce_sum(g), 2)
			g_kernel = tensorflow.tensordot(g, g, axes=0) / g_norm2d
			g_kernel = tensorflow.expand_dims(g_kernel, axis=-1)
			return tensorflow.expand_dims(tensorflow.tile(g_kernel, (1, 1, n_channels)), axis=-1)


		kernel = _gaussian_kernel( random_kernel_size, random_sigma, tensorflow.shape(image)[-1], image.dtype )
		image = tensorflow.nn.depthwise_conv2d( image[None], kernel, [1,1,1,1], 'SAME' )
		return image[0]

	# def blur_image ( self, image, random_kernel_size, random_sigma )

	# --------------------------------------------------

	def get_random_value ( self, lower, upper ):
		"""
		Picks a random value in [lower, upper].

		Arguments:
			self (`pykeras.inputs.ImagesRandomGaussianBlur`): Mapper instance.

		Returns:
			`tensorflow.Tensor`: Random value.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlur )

		# Is the seed specified ?
		if self.settings.seed is None:
			
			# Get a random brightness deltat without seeding
			return tensorflow.random.uniform(
				 shape = [],
				minval = lower,
				maxval = upper,
				 dtype = tensorflow.float32
				)

		else:
			# Get a random brightness deltat with a seed
			return tensorflow.random.stateless_uniform(
				 shape = [],
				  seed = self.settings.seed,
				minval = lower,
				maxval = upper,
				 dtype = tensorflow.float32
				)
		
		# if self.settings.seed is None:

	# def get_random_value ( self, lower, upper )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self  (`pykeras.inputs.ImagesRandomGaussianBlur`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlur )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Number of images to resize
		number_of_images = len(self.settings.images_features_names)
		
		# Get a random adjustment value
		random_kernel_size = self.get_random_value( self.settings.lower_kernel_size, self.settings.upper_kernel_size )
		random_sigma       = self.get_random_value( self.settings.lower_sigma, self.settings.upper_sigma )

		# Resize all named images
		for image_index in range( number_of_images ):

			# Get image
			image_feature_name = self.settings.images_features_names[ image_index ]
			image              = features[ image_feature_name ]	
			
			# Adjust image
			image = self.blur_image( image, random_kernel_size, random_sigma )

			# Clip
			image = tensorflow.clip_by_value( image, 0., 1. )

			# Replace the images in the batch
			features[ image_feature_name ] = image

		# for i in range(count)

		return features
		
	# def map_features ( self, features )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomGaussianBlurSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomGaussianBlurSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomGaussianBlur ( ImagesMapper )