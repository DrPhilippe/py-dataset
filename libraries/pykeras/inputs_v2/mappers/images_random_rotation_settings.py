# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ###  CLASS IMAGES-RANDOM-TRANSLATION-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomTranslationSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'min_angle',
		'max_angle',
		'image_width',
		'image_height',
		'seed'
		]
	)
class ImagesRandomRotationSettings ( ImagesTransformerSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		affected_features_names=[],
		transform_matrix_feature_name='',
		min_angle = -22.,
		max_angle = 22.,
		image_width = 224,
		image_height = 224,
		seed = None,
		name = 'images-random-rotation'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomTranslationSettings` class.
		
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			images_features_names                      (`list` of `str`s): Name of the images to translate.
			affected_features_names                    (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name                         (`str`): If set the rotation matrix is outputed with the given name.
			min_angle                                           (`float`): Minimum rotation angle.
			max_angle                                           (`float`): Maximum rotation angle.
			image_width                                           (`int`): Image width.
			image_height                                          (`int`): Image theight.
			seed                           (`tuple` of `2` `int`s/`None`): Random seed, or None.
			mode                                   ('roll' or 'pad+crop'): Mode defining how translation is implemented.
			name                                                  (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( min_angle, float )
		assert pytools.assertions.type_is( max_angle, int )
		assert pytools.assertions.type_is( image_width, int )
		assert pytools.assertions.type_is( image_height, int )
		assert pytools.assertions.type_in( seed, (tuple, type(None)) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomTranslationSettings, self ).__init__(
			images_features_names,
			affected_features_names,
			transform_matrix_feature_name,
			name
			)

		self._min_angle    = min_angle
		self._max_angle    = max_angle
		self._image_width  = image_width
		self._image_height = image_height
		self._seed         = copy.deepcopy( seed )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def min_delta_x ( self ):
		"""
		Minimum translation on X (`int`).
		"""
		return self._min_delta_x

	# --------------------------------------------------

	@min_delta_x.setter
	def min_delta_x ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._min_delta_x = value

	# --------------------------------------------------
	
	@property
	def max_delta_x ( self ):
		"""
		Maximum translation on X (`int`).
		"""
		return self._max_delta_x

	# --------------------------------------------------

	@max_delta_x.setter
	def max_delta_x ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._max_delta_x = value

	# --------------------------------------------------
	
	@property
	def min_delta_y ( self ):
		"""
		Minimum translation on Y (`int`).
		"""
		return self._min_delta_y

	# --------------------------------------------------

	@min_delta_y.setter
	def min_delta_y ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._min_delta_y = value

	# --------------------------------------------------
	
	@property
	def max_delta_y ( self ):
		"""
		Maximum translation on Y (`int`).
		"""
		return self._max_delta_y

	# --------------------------------------------------

	@max_delta_y.setter
	def max_delta_y ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._max_delta_y = value

	# --------------------------------------------------

	@property
	def seed ( self ):
		"""
		Random seed, or None (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )

		return self._seed

	# def seed ( self )

	# --------------------------------------------------

	@seed.setter
	def seed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )
		assert pytools.assertions.type_in( value, (tuple, type(None)) )
		if isinstance( value, tuple ):
			assert pytools.assertions.equal( len(value), 2 )
			assert pytools.assertions.tuple_items_type_is( value, int )

		self._seed = copy.deepcopy( value )

	# def seed ( self )

# class ImagesRandomTranslationSettings ( ImagesTransformerSettings )