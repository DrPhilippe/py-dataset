# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .mapper import Mapper
from .real_background_exchanger_settings import RealBackgroundExchangerSettings

# ##################################################
# ###      CLASS REAL-BACKGROUND-EXCHANGER       ###
# ##################################################

class RealBackgroundExchanger( Mapper ):
	"""
	Exchanges the background of an image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.RealBackgroundExchanger` class.

		Arguments:
			self             (`pykeras.inputs.RealBackgroundExchanger`): Instance to initialize.
			settings (`pykeras.inputs.RealBackgroundExchangerSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchanger )
		assert pytools.assertions.type_is_instance_of( settings, RealBackgroundExchangerSettings )

		super( RealBackgroundExchanger, self ).__init__( settings )

	# def __init__ ( ... )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self                    (`pykeras.inputs.RealBackgroundExchanger`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`/`tuple` of `3` `dict`s of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchanger )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		image = features[ self.settings.image_feature_name ]
		mask  = features[ self.settings.mask_feature_name ]
		back  = features[ self.settings.background_feature_name ]

		mask = tensorflow.cast( mask, tensorflow.bool )
		image = tensorflow.where( mask, image, back )
		
		features[ self.settings.image_feature_name ] = image
		return features

	# def map_features ( self, features )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Named Arguments:
			Forwarded to the real background exchanger settings constructor.

		Returns:
			`pykeras.inputs.RealBackgroundExchancgerSettings`: The new mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return RealBackgroundExchangerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RealBackgroundExchanger( Mapper )