# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment          import ImagesRandomAdjustment
from .images_random_saturation_settings import ImagesRandomSaturationSettings

# ##################################################
# ###       CLASS IMAGES-RANDOM-SATURATION       ###
# ##################################################

class ImagesRandomSaturation ( ImagesRandomAdjustment ):
	"""
	Mapper used randomly adjust the saturation of RGB images.

	All images are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_contrast`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomSaturation` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomSaturation`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomSaturationSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomSaturation )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomSaturationSettings )

		super( ImagesRandomSaturation, self ).__init__(
			tensorflow.image.adjust_saturation,
			settings
			)

	# def __init__ ( self, settings )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomSaturationSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomSaturationSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomSaturation ( ImagesRandomAdjustment )