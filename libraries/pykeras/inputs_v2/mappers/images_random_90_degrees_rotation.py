# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_90_degrees_rotation_settings import ImagesRandom90DegreesRotationSettings
from .images_transformer                         import ImagesTransformer

# ##################################################
# ###            CLASS CENTER-CROPPER            ###
# ##################################################

class ImagesRandom90DegreesRotation ( ImagesTransformer ):
	"""
	Base class for mappers that resize, rotate, mirror, shift, and crop images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandom90DegreesRotation` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandom90DegreesRotation`): Images transformer instance to initialize.
			settings (`pykeras.inputs.ImagesRandom90DegreesRotationSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotation )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandom90DegreesRotationSettings )

		super( ImagesRandom90DegreesRotation, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	@tensorflow.function
	def get_random_k ( self ):
		"""
		Picks a random number of rotation in [0, 3].

		Returns:
			`tensorflow.Tensor`: Random number of rotation in [0, 3].
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotation )

		# Is the seed specified ?
		if self.settings.seed is None:
			
			# Get a random brightness deltat without seeding
			return tensorflow.random.uniform(
				 shape = [],
				minval = 0,
				maxval = 4,
				 dtype = tensorflow.int32
				)

		else:
			# Get a random brightness deltat with a seed
			return tensorflow.random.stateless_uniform(
				 shape = [],
				  seed = self.settings.seed,
				minval = 0,
				maxval = 4,
				 dtype = tensorflow.int32
				)
		
		# if self.settings.seed is None:

	# def get_random_k ( self )

	# --------------------------------------------------
	
	@tensorflow.function
	def get_rotation_matrix ( k ):
		"""
		Creates a rotation matrix for the given number of rotations by 90 degrees

		Arguments:
			k (`tensorflow.Tensor`): Number of rotations by 90 degrees.

		Returns:
			`tensorflow.Tensor`: 2D rotation matrix.
		"""
		k         = tensorflow.cast( k, tensorflow.float32 )
		pi_sur_2  = tensorflow.constant( numpy.pi/2.0, shape=[], dtype=numpy.float32 )
		angle     = tensorflow.multiply( k, pi_sur_2 )
		cos_angle = tensorflow.cos( angle )
		sin_angle = tensorflow.sin( angle )
		matrix    = tensorflow.stack( (cos_angle, -sin_angle, sin_angle, cos_angle),  axis=-1 )
		return      tensorflow.reshape( matrix, (2, 2) )

	# def get_rotation_matrix ( k )

	# --------------------------------------------------

	@tensorflow.function
	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotation )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Pick a random number of rotations
		k = self.get_random_k()

		# Compute rotation matrix
		R = self.get_rotation_matrix( k )

		# Rotate all images by the same number of 90 degrees rotations
		for image_feature_name in self.settings.images_features_names:
			image = features[ image_feature_name ]
			image = tensorflow.image.rot90( image, k=k, name='rot_'+image_feature_name )
			features[ image_feature_name ] = image

		# Update affected features (2D image points, K matrix)
		# And maybe export rotation matrix
		features = self.map_affected_features( features, R )
		return features

	# def map_features ( self, features )

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		return ImagesRandom90DegreesRotationSettings( **kwargs )

# class ImagesRandom90DegreesRotation ( ImagesTransformer )