# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
import tensorflow_addons

# INTERNALS
import pytools

# LOCALS
from .images_random_rotation_settings import ImagesRandomRotationSettings
from .images_transformer              import ImagesTransformer

# ##################################################
# ###        CLASS IMAGES-RANDOM-ROTATION        ###
# ##################################################

class ImagesRandomRotation ( ImagesTransformer ):
	"""
	Base class for mappers that resize, rotate, mirror, shift, and crop images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomRotation` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomRotation`): Images transformer instance to initialize.
			settings (`pykeras.inputs.ImagesRandomRotationSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomRotationSettings )

		super( ImagesRandomRotation, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def get_random_angle ( self, min_value, max_value ):
		
		# Is the seed specified ?
		if self.settings.seed is None:
			
			# Get a random brightness deltat without seeding
			angle = tensorflow.random.uniform(
				 shape = [],
				minval = tensorflow.constant( min_value, shape=[], dtype=tensorflow.float32 ),
				maxval = tensorflow.constant( max_value, shape=[], dtype=tensorflow.float32 ),
				 dtype = tensorflow.float32
				)

		else:
			# Get a random brightness deltat with a seed
			angle = tensorflow.random.stateless_uniform(
				 shape = [],
				  seed = self.settings.seed,
				minval = tensorflow.constant( min_value, shape=[], dtype=tensorflow.float32 ),
				maxval = tensorflow.constant( max_value, shape=[], dtype=tensorflow.float32 ),
				 dtype = tensorflow.float32
				)
		
		# if self.settings.seed is None:

		pi_sur_2  = tensorflow.constant( numpy.pi/2.0, shape=[], dtype=numpy.float32 )
		return tensorflow.multiply( angle, pi_sur_2 )
	
	# def get_random_angle ( self )

	# --------------------------------------------------

	def get_rotation_matrix_around ( self, angle, x, y ):

		angle = tensorflow.cast( angle, tensorflow.float32 )
		x     = tensorflow.cast( x, tensorflow.float32 )
		y     = tensorflow.cast( y, tensorflow.float32 )

		cos_angle = tensorflow.cos( angle )
		sin_angle = tensorflow.sin( angle )
		matrix    = tensorflow.stack([
			cos_angle, -sin_angle, -x*cos_angle+y*sin_angle+x,
			sin_angle,  cos_angle, -x*sin_angle+y*cos_angle+y,
			       0.,         0.,                          1.
			])
		return  tensorflow.reshape( matrix, (3, 3) )

	# def get_rotation_matrix_around ( self, angle, x, y )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Pick a random angle
		angle = self.get_random_angle( self.settings.min_delta_x, self.settings.max_delta_x )

		# Compute translation matrix
		T = self.get_rotation_matrix_around( angle, self.settings.image_width//2, self.settings.image_height//2 )

		# Rotate all images by the same number of 90 degrees rotations
		for image_feature_name in self.settings.images_features_names:
			
			# Fetch image
			image = features[ image_feature_name ]

			# cast image
			dtype = image.dtype
			image = tensorflow.cast( image, tensorflow.float32 )

			# Pad image with zeros
			image = tensorflow_addons.image.translate( image, [delta_x, delta_y] )

			# Export image
			image = tensorflow.cast( image, dtype )
			features[ image_feature_name ] = image

		# Update affected features (2D image points, K matrix)
		# And maybe export translation matrix
		features = self.map_affected_features( features, T )
		return features

	# def map_features ( self, features )

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		return ImagesRandomTranslationSettings( **kwargs )

# class ImagesRandomTranslation ( ImagesTransformer )