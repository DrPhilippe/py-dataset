# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .center_cropper_settings import CenterCropperSettings
from .images_transformer      import ImagesTransformer

# ##################################################
# ###            CLASS CENTER-CROPPER            ###
# ##################################################

class CenterCropper ( ImagesTransformer ):
	"""
	Crops images in the center.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.CenterCropper` class.

		Arguments:
			self             (`pykeras.inputs.CenterCropper`): Instance to initialize.
			settings (`pykeras.inputs.CenterCropperSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropper )
		assert pytools.assertions.type_is_instance_of( settings, CenterCropperSettings )

		super( CenterCropper, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_crop_rectangle ( self, features, image_width, image_height ):
		"""
		Create the crop rectangle for an image with the given size and with the given features.

		Arguments:
			self             (`pykeras.inputs.CenterCropper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features.
			image_width                               (`int`): Image width.
			image_height                              (`int`): Image height.

		Returns:
			crop_x      (`tensorflow.Tensor`): Crop rectangle top-left X coordinate (single value, int32).
			crop_y      (`tensorflow.Tensor`): Crop rectangle top-left Y coordinate (single value, int32).
			crop_width  (`tensorflow.Tensor`): Crop rectangle width (single value, int32).
			crop_height (`tensorflow.Tensor`): Crop rectangle height (single value, int32).
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Crop rectangle width and height
		crop_width       = tensorflow.constant( self.settings.crop_width , shape=(), dtype=tensorflow.float32 )
		crop_height      = tensorflow.constant( self.settings.crop_height, shape=(), dtype=tensorflow.float32 )
		half_crop_width  = tensorflow.divide( crop_width,  2. )
		half_crop_height = tensorflow.divide( crop_height, 2. )

		# Compute crop rectangle x and y coordinates
		image_center_x = tensorflow.math.divide( tensorflow.cast(image_width, tensorflow.float32),  2. )
		image_center_y = tensorflow.math.divide( tensorflow.cast(image_height, tensorflow.float32), 2. )
		crop_x         = tensorflow.math.floor( tensorflow.math.subtract( image_center_x, half_crop_width  ) )
		crop_y         = tensorflow.math.floor( tensorflow.math.subtract( image_center_y, half_crop_height ) )

		# Clamp the crop rectangle in the image
		crop_x = tensorflow.clip_by_value( crop_x, 0., tensorflow.subtract( float(image_width),  crop_width  ) )
		crop_y = tensorflow.clip_by_value( crop_y, 0., tensorflow.subtract( float(image_height), crop_height ) )

		# Cast values
		crop_x      = tensorflow.cast( crop_x, tensorflow.int32 )
		crop_y      = tensorflow.cast( crop_y, tensorflow.int32 )
		crop_width  = tensorflow.cast( crop_width, tensorflow.int32 )
		crop_height = tensorflow.cast( crop_height, tensorflow.int32 )
		
		# Return values
		return crop_x, crop_y, crop_width, crop_height

	# def create_crop_rectangle ( self, features, image_width, image_height )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self             (`pykeras.inputs.CenterCropper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Rectangle variables, undefined yet
		crop_width  = tensorflow.constant(0)
		crop_height = tensorflow.constant(0)
		crop_x      = tensorflow.constant(0)
		crop_y      = tensorflow.constant(0)
		crop_init   = tensorflow.constant(False)

		# Go through images to crop
		for image_feature_name in self.settings.images_features_names:

			# Get the image
			image = features[ image_feature_name ]

			# Get its size
			image_height = tensorflow.shape( image )[ 0 ]
			image_width  = tensorflow.shape( image )[ 1 ]

			def init_crop ():
				# Create cop rectangle
				crop_x, crop_y, crop_width, crop_height = self.create_crop_rectangle( features, image_width, image_height )
				# Flag the crop rectangle as initialized
				crop_init = tensorflow.constant(True)
				# return the 5 variables
				return crop_x, crop_y, crop_width, crop_height, crop_init

			def do_nothing ():
				return crop_x, crop_y, crop_width, crop_height, crop_init

			# Is the crop rectangle initialized ?
			crop_x, crop_y, crop_width, crop_height, crop_init = tensorflow.cond( tensorflow.logical_not(crop_init), init_crop, do_nothing )

			# Crop the image
			image = tensorflow.reshape( image, [image_height, image_width, -1] )
			image = tensorflow.image.crop_to_bounding_box(
				image,
				crop_y,
				crop_x,
				crop_height,
				crop_width
				)

			# Update the image
			features[ image_feature_name ] = image

		# for image_feature_name in self.settings.images_features_names

		# Compute the crop translation matrix to update affected features
		crop_x      = tensorflow.cast( crop_x, tensorflow.float32 )
		crop_y      = tensorflow.cast( crop_y, tensorflow.float32 )
		crop_width  = tensorflow.cast( crop_width, tensorflow.float32 )
		crop_height = tensorflow.cast( crop_height, tensorflow.float32 )
		crop_matrix = tensorflow.stack([
			1., 0., -crop_x,
			0., 1., -crop_y,
			0., 1., 1.
			])
		crop_matrix = tensorflow.reshape( crop_matrix, [3, 3] )
		crop_rectangle = tensorflow.stack([
			crop_x, crop_y,
			crop_x+crop_width, crop_y+crop_height
			])
		crop_rectangle = tensorflow.reshape( crop_rectangle, [2, 2] )

		# Output crop rectangle ?
		if self.settings.crop_rectangle_feature_name:
			features[ self.settings.crop_rectangle_feature_name ] = crop_rectangle

		# Modify features affected by the cropping operation
		# and maybe save it
		features = self.map_affected_features( features, crop_matrix )

		# Return updated features
		return features

	# def map_features ( self, features )
	
	# --------------------------------------------------

	def map_features_names ( self, features_names ):
		"""
		Map the names of the features.

		Arguments:
			self (`pykeras.inputs.CenterCropper`): Center cropper instance.
			features_names                (`str`): Names of the features before cropping.

		Returns:
			`list` of `str`s/`tuple` of `3` `list`s of `str`s: Names of the features after cropping.
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropper )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		features_names = super( CenterCropper, self ).map_features_names( features_names )
		
		if self.settings.crop_rectangle_feature_name:
			features_names.append( self.settings.crop_rectangle_feature_name )

		return features_names

	# def map_features_names ( self, features_names )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the center cropper of type `cls`.
		
		Arguments:
			cls (`type`): Type of center cropper for which to create settings.

		Named Arguments:
			Forwarded to the center cropper settings constructor.

		Returns:
			`pykeras.inputs.CenterCropperSettings`: The new center cropper settings instance.
		"""
		return CenterCropperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class CenterCropper ( ImagesTransformer )