# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings

# ##################################################
# ###  CLASS IMAGES-RANDOM-ADJUSTMENT-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomAdjustmentSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'lower',
		'upper',
		'seed'
		]
	)
class ImagesRandomAdjustmentSettings ( ImagesMapperSettings ):
	"""
	Settings used to randomly adjust images.
	
	All images of one example are adjusted with the same random value.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names = [],
		lower = 0.,
		upper = 1.,
		seed = None,
		name = 'images-random-adjustment'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomAdjustmentSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesRandomAdjustmentSettings`): Instance to initialize.
			images_features_names               (`list` of `str`s): Name of the images to map.
			lower                                        (`float`): Lower bound of the random value adjustment.
			upper                                        (`float`): Upper bound of the random value adjustment.
			seed                    (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                           (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( lower, float )
		assert pytools.assertions.type_is( upper, float )
		assert pytools.assertions.type_in( seed, (tuple, type(None)) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomAdjustmentSettings, self ).__init__( images_features_names, name )

		self._lower = lower
		self._upper = upper
		self._seed  = copy.deepcopy( seed )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def lower ( self ):
		"""
		Lower bound of the random value adjustment (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )

		return self._lower

	# def lower ( self )

	# --------------------------------------------------

	@lower.setter
	def lower ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )
		assert pytools.assertions.type_is( value, float )

		self._lower = value

	# def lower ( self )

	# --------------------------------------------------

	@property
	def upper ( self ):
		"""
		Upper bound of the random value adjustment (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )

		return self._upper

	# def upper ( self )

	# --------------------------------------------------

	@upper.setter
	def upper ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )
		assert pytools.assertions.type_is( value, float )

		self._upper = value

	# def upper ( self )

	# --------------------------------------------------

	@property
	def seed ( self ):
		"""
		Random seed, or None (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )

		return self._seed

	# def seed ( self )

	# --------------------------------------------------

	@seed.setter
	def seed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustmentSettings )
		assert pytools.assertions.type_in( value, (tuple, type(None)) )
		if isinstance( value, tuple ):
			assert pytools.assertions.equal( len(value), 2 )
			assert pytools.assertions.tuple_items_type_is( value, int )

		self._seed = copy.deepcopy( value )

	# def seed ( self )

# class ImagesRandomAdjustmentSettings ( MapperSettings )