# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .bounding_rectangle_cropper import BoundingRectangleCropper
from .random_scale_bounding_rectangle_cropper_settings import RandomScaleBoundingRectangleCropperSettings

# ##################################################
# ###      CLASS BOUNDING-RECTANGLE-CROPPER      ###
# ##################################################

class RandomScaleBoundingRectangleCropper ( BoundingRectangleCropper ):
	"""
	Crops images at the center of objects bounding rectangle.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.RandomScaleBoundingRectangleCropper` class.

		Arguments:
			self             (`pykeras.inputs.RandomScaleBoundingRectangleCropper`): Instance to initialize.
			settings (`pykeras.inputs.RandomScaleBoundingRectangleCropperSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, RandomScaleBoundingRectangleCropper )
		assert pytools.assertions.type_is_instance_of( settings, RandomScaleBoundingRectangleCropperSettings )

		super( RandomScaleBoundingRectangleCropper, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def create_crop_rectangle ( self, features, image_width, image_height ):
		"""
		Create the crop rectangle for an image with the given size and with the given features.

		Arguments:
			self (`pykeras.inputs.RandomScaleBoundingRectangleCropper`): Mapper instance.
			features           (`dict` of `str` to `tensorflow.Tensor`): Batch of features.
			image_width                                         (`int`): Image width.
			image_height                                        (`int`): Image height.

		Returns:
			crop_x      (`tensorflow.Tensor`): Crop rectangle top-left X coordinate (single value, int32).
			crop_y      (`tensorflow.Tensor`): Crop rectangle top-left Y coordinate (single value, int32).
			crop_width  (`tensorflow.Tensor`): Crop rectangle width (single value, int32).
			crop_height (`tensorflow.Tensor`): Crop rectangle height (single value, int32).
		"""
		assert pytools.assertions.type_is_instance_of( self, RandomScaleBoundingRectangleCropper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Get the bounding rectangle of the object
		bounding_rectangle = features[ self.settings.bounding_rectangle_feature_name ] # [2, 2]
		bounding_rectangle = tensorflow.cast( bounding_rectangle, tensorflow.int32 )

		# Check bounding rectangle specifications
		if not tensorflow.TensorSpec( shape=(2, 2), dtype=tensorflow.int32 ).is_compatible_with( bounding_rectangle ):
			raise ValueError(
				"The bounding rectangle Tensor does not have the require specifications: shape={}/[2, 2], dtype={}/int32".format(
				list(bounding_rectangle.shape),
				str(bounding_rectangle.dtype)
				))

		# Cast bounding rectangle
		bounding_rectangle = tensorflow.cast( bounding_rectangle, tensorflow.int32 )

		# Compute the center of the bounding rectangle
		center_x = tensorflow.math.divide(
			tensorflow.cast( tensorflow.math.add( bounding_rectangle[0, 0], bounding_rectangle[1, 0] ), tensorflow.float32 ),
			2.
			)
		center_y = tensorflow.math.divide(
			tensorflow.cast( tensorflow.math.add( bounding_rectangle[0, 1], bounding_rectangle[1, 1] ),	tensorflow.float32 ),
			2.
			)

		# Size of the bounding rectangle
		br_width  = tensorflow.math.subtract( bounding_rectangle[1, 0], bounding_rectangle[0, 0] )
		br_height = tensorflow.math.subtract( bounding_rectangle[1, 1], bounding_rectangle[0, 1] )

		# Create the crop rectangle (its size)
		if self.settings.crop_width > 0 and self.settings.crop_height > 0:
			# Use the size defined in the settings
			crop_width  = tensorflow.constant( self.settings.crop_width  + self.settings.pad_width*2,  shape=(), dtype=tensorflow.int32 )
			crop_height = tensorflow.constant( self.settings.crop_height + self.settings.pad_height*2, shape=(), dtype=tensorflow.int32 )
		else:
			# Use the actual bounding rectangle, with padding
			crop_width  = tensorflow.add( br_width,  tensorflow.constant(self.settings.pad_width*2,  shape=(), dtype=tensorflow.int32) )
			crop_height = tensorflow.add( br_height, tensorflow.constant(self.settings.pad_height*2, shape=(), dtype=tensorflow.int32) )

		# Randomly scale it
		random_scale = tensorflow.random.uniform(
			 shape = [],
			minval = self.settings.min_scale,
			maxval = self.settings.max_scale,
			 dtype = tensorflow.float32
			)
		crop_width  = tensorflow.math.multiply( tensorflow.cast( crop_width,  tensorflow.float32 ), random_scale )
		crop_height = tensorflow.math.multiply( tensorflow.cast( crop_height, tensorflow.float32 ), random_scale )
		crop_width  = tensorflow.cast( tensorflow.math.floor( crop_width  ), tensorflow.int32 )
		crop_height = tensorflow.cast( tensorflow.math.floor( crop_height ), tensorflow.int32 )

		# Cannot crop bigger than the image
		crop_width  = tensorflow.minimum( crop_width,  image_width  )
		crop_height = tensorflow.minimum( crop_height, image_height )

		# Center it on the the bounding rectangle
		half_width  = tensorflow.math.divide( tensorflow.cast(crop_width,  tensorflow.float32), 2. )
		half_height = tensorflow.math.divide( tensorflow.cast(crop_height, tensorflow.float32), 2. )
		crop_x      = tensorflow.math.subtract( center_x, half_width )
		crop_y      = tensorflow.math.subtract( center_y, half_height )
		crop_x      = tensorflow.cast( tensorflow.floor( crop_x ), tensorflow.int32 )
		crop_y      = tensorflow.cast( tensorflow.floor( crop_y ), tensorflow.int32 )

		# Clamp the crop rectangle in the image
		crop_x = tensorflow.clip_by_value( crop_x, 0, tensorflow.subtract( image_width,  crop_width  )-1 )
		crop_y = tensorflow.clip_by_value( crop_y, 0, tensorflow.subtract( image_height, crop_height )-1 )

		# Return values
		return crop_x, crop_y, crop_width, crop_height

	# def create_crop_rectangle ( self, features, image_width, image_height )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the bounding rectangle cropper of type `cls`.
		
		Arguments:
			cls (`type`): Type of bounding rectangle cropper for which to create settings.

		Named Arguments:
			Forwarded to the bounding rectangle cropper settings constructor.

		Returns:
			`pykeras.inputs.RandomScaleBoundingRectangleCropperSettings`: The new bounding rectangle cropper settings instance.
		"""
		return RandomScaleBoundingRectangleCropperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RandomScaleBoundingRectangleCropper ( BoundingRectangleCropper )