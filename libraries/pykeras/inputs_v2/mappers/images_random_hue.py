# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment   import ImagesRandomAdjustment
from .images_random_hue_settings import ImagesRandomHueSettings

# ##################################################
# ###          CLASS IMAGES-RANDOM-HUE           ###
# ##################################################

class ImagesRandomHue ( ImagesRandomAdjustment ):
	"""
	Mapper used randomly adjust the hue of RGB images.

	All images are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_hue`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomHue` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomHue`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomHueSettings`): Mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomHue )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomHueSettings )

		super( ImagesRandomHue, self ).__init__(
			tensorflow.image.adjust_hue,
			settings
			)

	# def __init__ ( self, settings )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomHueSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomHueSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomHue ( ImagesRandomAdjustment )