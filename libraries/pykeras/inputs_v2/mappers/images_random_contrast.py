# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment        import ImagesRandomAdjustment
from .images_random_contrast_settings import ImagesRandomContrastSettings

# ##################################################
# ###        CLASS IMAGES-RANDOM-CONTRAST        ###
# ##################################################

class ImagesRandomContrast ( ImagesRandomAdjustment ):
	"""
	Mapper used randomly adjust the contrast of images.

	All images of one example are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_contrast`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomContrast` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomContrast`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomContrastSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomContrast )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomContrastSettings )

		super( ImagesRandomContrast, self ).__init__(
			tensorflow.image.adjust_contrast,
			settings
			)

	# def __init__ ( self, settings )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomContrastSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomContrastSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomContrast ( ImagesRandomAdjustment )