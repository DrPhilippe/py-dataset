# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .center_cropper_settings import CenterCropperSettings

# ##################################################
# ### CLASS BOUNDING-RECTANGLE-CROPPER-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BoundingRectangleCropperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'bounding_rectangle_feature_name',
		]
	)
class BoundingRectangleCropperSettings ( CenterCropperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		bounding_rectangle_feature_name='bounding_rectangle',
		crop_size=(224, 224),
		pad_size=(0, 0),
		affected_features_names=[],
		transform_matrix_feature_name='',
		crop_rectangle_feature_name='',
		name='bounding-rectangle-cropper'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.BoundingRectangleCropperSettings` class.
	
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			self (`pykeras.inputs.BoundingRectangleCropperSettings`): Instance to initialize.
			images_features_names                 (`list` of `str`s): Name of the images to crop.
			bounding_rectangle_feature_name                  (`str`): Bounding rectangle of the object to crop.
			crop_size                        (`tuple` of `2` `int`s): Crop size as `(height, width)`.
			affected_features_names               (`list` of `str`s): Names of features affected by the crop.
			transform_matrix_feature_name                    (`str`): If set the crop translation matrix is outputed with the given name.
			crop_rectangle_feature_name                      (`str`): If set the crop rectangle is outputed with the given name.
			name                                             (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( crop_size, tuple )
		assert pytools.assertions.tuple_items_type_is( crop_size, int )
		assert pytools.assertions.equal( len(crop_size), 2 )
		assert pytools.assertions.type_is( pad_size, tuple )
		assert pytools.assertions.tuple_items_type_is( pad_size, int )
		assert pytools.assertions.equal( len(pad_size), 2 )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( crop_rectangle_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( BoundingRectangleCropperSettings, self ).__init__(
			images_features_names,
			crop_size,
			affected_features_names,
			transform_matrix_feature_name,
			crop_rectangle_feature_name,
			name
			)

		self._pad_size = pad_size
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------
	
	@property
	def pad_size ( self ):
		"""
		Pad size as `(height, width)` (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )

		return self._pad_size

	# def pad_size ( self )
	
	# --------------------------------------------------
	
	@pad_size.setter
	def pad_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )
		assert pytools.assertions.type_is( value, tuple )
		assert pytools.assertions.equal( len(value), 2 )
		assert pytools.assertions.tuple_items_type_is( value, int )

		self._pad_size = value

	# def pad_size ( self, value )

	# --------------------------------------------------
	
	@property
	def pad_height ( self ):
		"""
		Pad height (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )

		return self._pad_size[ 0 ]

	# def pad_height ( self )

	# --------------------------------------------------
	
	@property
	def pad_width ( self ):
		"""
		Pad width (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )

		return self._pad_size[ 1 ]

	# def pad_width ( self )

	# --------------------------------------------------
	
	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Bounding rectangle of the object to crop (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )
	
	# --------------------------------------------------
	
	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectangleCropperSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value

	# def bounding_rectangle_feature_name ( self, value )

# class BoundingRectangleCropperSettings ( CenterCropperSettings )