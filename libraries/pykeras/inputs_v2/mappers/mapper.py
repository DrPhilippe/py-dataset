# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###                CLASS MAPPER                ###
# ##################################################

class Mapper:
	"""
	Mapper applied to features of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.Mapper` class.

		Arguments:
			self             (`pykeras.inputs.Mapper`): Instance to initialize.
			settings (`pykeras.inputs.MapperSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is_instance_of( settings, MapperSettings )

		self._settings = copy.deepcopy( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def settings ( self ):
		"""
		Settings of this mapper (`pykeras.inputs.MapperSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )

		return self._settings
	
	# def settings ( self )
	
	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the mapper (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )

		return self.settings.name
	
	# def name ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self                    (`pykeras.inputs.Mapper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`/`tuple` of `3` `dict`s of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		return features

	# def map_features ( self, features )

	# --------------------------------------------------

	def map_features_names ( self, features_names ):
		"""
		Map the names of the features.

		Arguments:
			self (`pykeras.inputs.Mapper`): Mapper instance.
			features_names         (`str`): Names of the features before mapping.

		Returns:
			`list` of `str`s/`tuple` of `3` `list`s of `str`s: Names of the features after mapping.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		return features_names

	# def map_features_names ( self, features_names )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __call__ ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self                    (`pykeras.inputs.Mapper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is( features, dict )

		with tensorflow.name_scope( self.settings.name ):
			return self.map_features( features )

	# def __call__ ( self, features )

	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self ):
		"""
		Callback used to notify this mapper that one epoch has passed.

		Arguments:
			self (`pykeras.inputs.Mapper`): Mapper instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		
		pass

	# def on_epoch_end ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, **kwargs ):
		"""
		Creates a new instance of the mapper class `cls`.

		Arguments:
			cls (`type`): Type of mapper to create.

		Named Arguments:
			Forwarded to the mapper's settings constructor.

		Return:
			`pykeras.inputs.Mapper`: The new mapper instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Create the mapper settings
		settings = cls.create_settings( **kwargs )

		# Create the mapper
		return cls( settings )

	# def create ( cls, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Named Arguments:
			Forwarded to the mapper's settings constructor.

		Returns:
			`pykeras.inputs.OnlineMapperSettings`: The new mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )

# class Mapper