# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###      CLASS POINTS-NORMALIZER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PointsNormalizerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'image_feature_name',
		'points_feature_name',
		'new_min',
		'new_max'
		]
	)
class PointsNormalizerSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, image_feature_name='', points_feature_name='', new_min=0., new_max=0., name='points-normalizer' ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( points_feature_name, str )
		assert pytools.assertions.type_is( new_min, float )
		assert pytools.assertions.type_is( new_max, float )

		super( PointsNormalizerSettings, self ).__init__( name )

		self._image_feature_name  = image_feature_name
		self._points_feature_name = points_feature_name
		self._new_min             = new_min
		self._new_max             = new_max

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def points_feature_name ( self ):
		"""
		Name of the feature containing the control points sets to normalize (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )

		return self._points_feature_name
	
	# def points_feature_name ( self )

	# --------------------------------------------------

	@points_feature_name.setter
	def points_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._points_feature_name = value
	
	# def points_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def new_min ( self ):
		"""
		New minimum point value on x and y (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )

		return self._new_min
	
	# def new_min ( self )

	# --------------------------------------------------

	@new_min.setter
	def new_min ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )
		assert pytools.assertions.type_is( value, float )

		self._new_min = value
	
	# def new_min ( self, value )

	# --------------------------------------------------

	@property
	def new_max ( self ):
		"""
		New minimum point value on x and y (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )

		return self._new_max
	
	# def new_max ( self )

	# --------------------------------------------------

	@new_max.setter
	def new_max ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizerSettings )
		assert pytools.assertions.type_is( value, float )

		self._new_max = value
	
	# def new_max ( self, value )

# class PointsNormalizerSettings ( MapperSettings )