# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment            import ImagesRandomAdjustment
from .images_random_jpeg_quality_settings import ImagesRandomJpegQualitySettings

# ##################################################
# ###      CLASS IMAGES-RANDOM-JPEG-QUALITY      ###
# ##################################################

class ImagesRandomJpegQuality ( ImagesRandomAdjustment ):
	"""
	Mapper used randomly adjust the JPEG quality of RGB images.

	All images of one example are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_jpeg_quality`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomJpegQuality` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomJpegQuality`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomJpegQualitySettings`): Mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomJpegQuality )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomJpegQualitySettings )

		super( ImagesRandomJpegQuality, self ).__init__(
			tensorflow.image.adjust_jpeg_quality,
			settings
			)

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_random_adjustment_value ( self ):
		"""
		Picks a random adjustment value.

		Arguments:
			self (`pykeras.inputs.ImagesRandomAdjustment`): Mapper instance.

		Returns:
			`tensorflow.Tensor`: Random adjustment value.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )

		value = super( ImagesRandomJpegQuality, self ).get_random_adjustment_value()
		return  tensorflow.cast( value, tensorflow.int32 )
		
		# if self.settings.seed is None:

	# def get_random_adjustment_value ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomJpegQualitySettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomJpegQualitySettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomJpegQuality ( ImagesRandomAdjustment )