# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
import tensorflow_addons

# INTERNALS
import pytools

# LOCALS
from .images_random_translation_settings import ImagesRandomTranslationSettings
from .images_transformer                 import ImagesTransformer

# ##################################################
# ###      CLASS IMAGES-RANDOM-TRANSLATION       ###
# ##################################################

class ImagesRandomTranslation ( ImagesTransformer ):
	"""
	Base class for mappers that resize, rotate, mirror, shift, and crop images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomTranslation` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomTranslation`): Images transformer instance to initialize.
			settings (`pykeras.inputs.ImagesRandom90DegreesRotationSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomTranslationSettings )

		super( ImagesRandomTranslation, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def get_random_delta ( self, min_value, max_value ):
		"""
		Picks a random translation delta in [min_value, max_value]

		Returns:
			`tensorflow.Tensor`: Random translation delta in [min_value, max_value]
		"""

		# Is the seed specified ?
		if self.settings.seed is None:
			
			# Get a random brightness deltat without seeding
			return tensorflow.random.uniform(
				 shape = [],
				minval = tensorflow.constant( min_value, shape=[], dtype=tensorflow.int32 ),
				maxval = tensorflow.constant( max_value, shape=[], dtype=tensorflow.int32 ),
				 dtype = tensorflow.int32
				)

		else:
			# Get a random brightness deltat with a seed
			return tensorflow.random.stateless_uniform(
				 shape = [],
				  seed = self.settings.seed,
				minval = tensorflow.constant( min_value, shape=[], dtype=tensorflow.int32 ),
				maxval = tensorflow.constant( max_value, shape=[], dtype=tensorflow.int32 ),
				 dtype = tensorflow.int32
				)
		
		# if self.settings.seed is None:

	# def get_random_delta ( self )

	# --------------------------------------------------
	
	def get_translation_matrix ( self, delta_x, delta_y ):
		"""
		Creates a rotation matrix for the given number of rotations by 90 degrees

		Arguments:
			delta_x (`tensorflow.Tensor`): Translation on X.
			delta_y (`tensorflow.Tensor`): Translation on Y.

		Returns:
			`tensorflow.Tensor`: 2D rotation matrix.
		"""
		matrix = tensorflow.stack([
			1., 0., tensorflow.cast( delta_x, tensorflow.float32 ),
			0., 1., tensorflow.cast( delta_y, tensorflow.float32 ),
			0., 1., 1.
			])
		return tensorflow.reshape( matrix, [3, 3] )

	# def get_translation_matrix ( ... )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Pick a random translation
		delta_x = self.get_random_delta( self.settings.min_delta_x, self.settings.max_delta_x )
		delta_y = self.get_random_delta( self.settings.min_delta_y, self.settings.max_delta_y )

		# Compute translation matrix
		T = self.get_translation_matrix( delta_x, delta_y )

		# Rotate all images by the same number of 90 degrees rotations
		for image_feature_name in self.settings.images_features_names:
			
			# Fetch image
			image = features[ image_feature_name ]

			# cast image
			dtype = image.dtype
			image = tensorflow.cast( image, tensorflow.float32 )

			# Pad image with zeros
			image = tensorflow_addons.image.translate( image, [delta_x, delta_y] )

			# Export image
			image = tensorflow.cast( image, dtype )
			features[ image_feature_name ] = image

		# Update affected features (2D image points, K matrix)
		# And maybe export translation matrix
		features = self.map_affected_features( features, T )
		return features

	# def map_features ( self, features )

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		return ImagesRandomTranslationSettings( **kwargs )

# class ImagesRandomTranslation ( ImagesTransformer )