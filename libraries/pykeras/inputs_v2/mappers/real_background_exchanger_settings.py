# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###  CLASS REAL-BACKGROUND-EXCHANGER-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RealBackgroundExchangerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'image_feature_name',
		'mask_feature_name',
		'background_feature_name'
		]
	)
class RealBackgroundExchangerSettings ( MapperSettings ):
	"""
	Used to exchange the background of an image with the pictured background.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self,
		image_feature_name='',
		mask_feature_name='',
		background_feature_name='',
		name='real-background-exchanger'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.RealBackgroundExchangerSettings` class.

		Arguments:
			self (`pykeras.inputs.RealBackgroundExchangerSettings`): Instance to initialize.
			image_feature_name                              (`str`): Name of the image feature of which to replace the background.
			mask_feature_name                               (`str`): Name of the feature containing the mask.
			background_feature_name                         (`str`): Name of the feature containing the background.
			name                                            (`str`): Name of the background exchanger.
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( background_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( RealBackgroundExchangerSettings, self ).__init__( name )

		self._image_feature_name      = image_feature_name
		self._mask_feature_name       = mask_feature_name
		self._background_feature_name = background_feature_name

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the image feature of which to replace the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def background_feature_name ( self ):
		"""
		Name of the feature containing the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )

		return self._background_feature_name

	# def background_feature_name ( self )

	# --------------------------------------------------

	@background_feature_name.setter
	def background_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RealBackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._background_feature_name = value

	# def background_feature_name ( self, value )

# class RealBackgroundExchangerSettings ( MapperSettings )