# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment_settings import ImagesRandomAdjustmentSettings
from .images_mapper                     import ImagesMapper

# ##################################################
# ###       CLASS IMAGES-RANDOM-ADJUSTMENT       ###
# ##################################################

class ImagesRandomAdjustment ( ImagesMapper ):
	"""
	Mapper used randomly adjust images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, adjustment_fn, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomAdjustment` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomAdjustment`): Instance to initialize.
			adjustment_fn                          (`function`/`None`): Tensorflow adjustment function.
			settings (`pykeras.inputs.ImagesRandomAdjustmentSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomAdjustmentSettings )

		super( ImagesRandomAdjustment, self ).__init__( settings )

		self._adjustment_fn = adjustment_fn

	# def __init__ ( self, adjustment_fn, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------
	
	@property
	def adjustment_fn ( self ):
		"""
		Tensorflow adjustment function (`function`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )

		return self._adjustment_fn

	# def adjustment_fn ( self )

	# --------------------------------------------------

	@adjustment_fn.setter
	def adjustment_fn ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )

		self._adjustment_fn = value

	# def adjustment_fn ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def adjust_image ( self, image, random_adjustment_value ):
		"""
		Adjust the image with the given random adjustment value.

		Arguments:
			self (`pykeras.inputs.ImagesRandomAdjustment`): Mapper instance.
			image                    (`tensorflow.Tensor`): Image to adjust.
			random_adjustment_value  (`tensorflow.Tensor`): Random adjustment value.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )
		assert pytools.assertions.type_is_instance_of( image, tensorflow.Tensor )
		assert pytools.assertions.type_is_instance_of( random_adjustment_value, tensorflow.Tensor )

		# If the adjustment function is not provided
		if self.adjustment_fn is None:

			# Raise not implemented error
			raise NotImplementedError()

		# Adjust the image using the adjustment function
		return self.adjustment_fn( image, random_adjustment_value )

	# def adjust_image ( self, image, random_adjustment_value )

	# --------------------------------------------------

	def get_random_adjustment_value ( self ):
		"""
		Picks a random adjustment value.

		Arguments:
			self (`pykeras.inputs.ImagesRandomAdjustment`): Mapper instance.

		Returns:
			`tensorflow.Tensor`: Random adjustment value.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )

		# Is the seed specified ?
		if self.settings.seed is None:
			
			# Get a random brightness deltat without seeding
			return tensorflow.random.uniform(
				 shape = [],
				minval = self.settings.lower,
				maxval = self.settings.upper,
				 dtype = tensorflow.float32
				)

		else:
			# Get a random brightness deltat with a seed
			return tensorflow.random.stateless_uniform(
				 shape = [],
				  seed = self.settings.seed,
				minval = self.settings.lower,
				maxval = self.settings.upper,
				 dtype = tensorflow.float32
				)
		
		# if self.settings.seed is None:

	# def get_random_adjustment_value ( self )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self             (`pykeras.inputs.ImagesResizer`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomAdjustment )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Number of images to resize
		number_of_images = len(self.settings.images_features_names)
		
		# Get a random adjustment value
		random_adjustment_value = self.get_random_adjustment_value()

		# Resize all named images
		for image_index in range( number_of_images ):

			# Get image
			image_feature_name = self.settings.images_features_names[ image_index ]
			image              = features[ image_feature_name ]	
			
			# Adjust image
			image = self.adjust_image( image, random_adjustment_value )

			# Clip
			# image = tensorflow.clip_by_value( image, 0., 1. )

			# Replace the images in the batch
			features[ image_feature_name ] = image

		# for i in range(count)

		return features
		
	# def map_features ( self, features )

# class ImagesRandomAdjustment ( ImagesMapper )