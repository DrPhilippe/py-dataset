# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# ##################################################
# ###           CLASS MAPPER-SETTINGS            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MapperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'name'
		]
	)
class MapperSettings ( pytools.serialization.Serializable ):
	"""
	Settings of a mapper.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='mapper' ):
		"""
		Initializes a new instance of the `pykeras.mappers.MapperSettings` class.

		Arguments:
			self (`pykeras.inputs.MapperSettings`): Instance to initialize.
			name                           (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MapperSettings )
		assert pytools.assertions.type_is( name, str )

		self._name = name

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the mapper (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MapperSettings )

		return self._name
	
	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

# class MapperSettings ( pytools.serialization.Serializable )