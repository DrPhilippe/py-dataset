# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment_settings import ImagesRandomAdjustmentSettings

# ##################################################
# ###      CLASS IMAGES-RANDOM-HUE-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomHueSettings',
	   namespace = 'pykeras.inputs',
	fields_names = []
	)
class ImagesRandomHueSettings ( ImagesRandomAdjustmentSettings ):
	"""
	Settings used to randomly adjust the brightness of the images.

	All images of one example are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_brightness`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names = [],
		max_delta = 1.,
		seed = None,
		name = 'images-random-hue'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomHueSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesRandomHueSettings`): Instance to initialize.
			images_features_names        (`list` of `str`s): Name of the images to map.
			max_delta                             (`float`): Hue offset will be chosen in tha range `[-max_deltat, max_deltat]` and `max_deltat` must be in the range `[0, 0.5]`.
			seed             (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                    (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomHueSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( max_delta, float )
		assert pytools.assertions.true( max_delta >= 0. )
		assert pytools.assertions.true( max_delta <= 0.5 )
		assert pytools.assertions.type_in( seed, (type(None), tuple) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomHueSettings, self ).__init__(
			images_features_names,
			-max_delta,
			max_delta,
			seed,
			name
			)

	# def __init__ ( self, ... )

# class ImagesRandomHueSettings ( ImagesRandomAdjustmentSettings )