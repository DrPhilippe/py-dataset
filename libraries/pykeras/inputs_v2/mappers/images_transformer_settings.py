# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings

# ##################################################
# ###     CLASS IMAGES-TRANSFORMER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesTransformerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'affected_features_names',
		'transform_matrix_feature_name'
		]
	)
class ImagesTransformerSettings ( ImagesMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		affected_features_names=[],
		transform_matrix_feature_name='',
		name='images-transformer'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesTransformerSettings` class.
		
		This class is the base class for mappers that resize, rotate, mirror, shift, and crop images.

		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			self (`pykeras.inputs.ImagesTransformerSettings`): Instance to initialize.
			images_features_names          (`list` of `str`s): Name of the images to transform.
			affected_features_names        (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name             (`str`): If set the transformation matrix is outputed with the given name.
			name                                      (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformerSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( ImagesTransformerSettings, self ).__init__( images_features_names, name )
		
		self._affected_features_names       = copy.deepcopy( affected_features_names )
		self._transform_matrix_feature_name = transform_matrix_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def affected_features_names ( self ):
		"""
		Names of features affected by the crop (`list` of `str`s).

		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D transforms with dtype `float32` and shape compatible with `(-1, 3, 3)`.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformerSettings )

		return self._affected_features_names

	# def affected_features_names ( self )
	
	# --------------------------------------------------
	
	@affected_features_names.setter
	def affected_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._affected_features_names = value

	# def affected_features_names ( self, value )

	# --------------------------------------------------
	
	@property
	def transform_matrix_feature_name ( self ):
		"""
		If set the transformation matrix is outputed with the given name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformerSettings )

		return self._transform_matrix_feature_name

	# def transform_matrix_feature_name ( self )
	
	# --------------------------------------------------
	
	@transform_matrix_feature_name.setter
	def transform_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformerSettings )
		assert pytools.assertions.type_is( value, str )

		self._transform_matrix_feature_name = value

	# def transform_matrix_feature_name ( self, value )

# class ImagesTransformerSettings ( ImagesMapperSettings )