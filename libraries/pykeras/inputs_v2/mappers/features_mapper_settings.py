# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###      CLASS FEATURES-MAPPERS-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FeaturesMapperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'inputs',
		'labels',
		'samples_weights'
		]
	)
class FeaturesMapperSettings ( MapperSettings ):
	"""
	This mapper is used to rename, deplicate, or remove features from a batch.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, inputs={}, labels={}, samples_weights={}, name='features-mapper' ):
		"""
		Initializes a new instance of the `pykeras.inputs.FeaturesMapperSettings` class.

		Arguments:
			self (`pykeras.inputs.FeaturesMapperSettings`): Instance to initialize.
			inputs              (`dict` of `str` to `str`): New inputs features names mapped to the old features names.
			labels              (`dict` of `str` to `str`): New labels features names mapped to the old features names.
			samples_weights     (`dict` of `str` to `str`): New samples weights feature name mapped to the old features names.
			name                                   (`str`): Name of the features mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, str )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, str )
		assert pytools.assertions.type_is( samples_weights, dict )
		assert pytools.assertions.dictionary_types_are( samples_weights, str, str )

		super( FeaturesMapperSettings, self ).__init__( name )

		self._inputs          = copy.deepcopy( inputs )
		self._labels          = copy.deepcopy( labels )
		self._samples_weights = copy.deepcopy( samples_weights )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def inputs ( self ):
		"""
		New inputs features names mapped to the old features names (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )

		return self._inputs
	
	# def inputs ( self )

	# --------------------------------------------------

	@inputs.setter
	def inputs ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )

		self._inputs = copy.deepcopy( value )
	
	# def inputs ( self, value )

	# --------------------------------------------------

	@property
	def labels ( self ):
		"""
		New labels features names mapped to the old features names (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )

		return self._labels
	
	# def labels ( self )

	# --------------------------------------------------

	@labels.setter
	def labels ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )

		self._labels = copy.deepcopy( value )
	
	# def labels ( self, value )

	# --------------------------------------------------

	@property
	def samples_weights ( self ):
		"""
		New samples weights feature name mapped to the old features names (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )

		return self._samples_weights
	
	# def samples_weights ( self )

	# --------------------------------------------------

	@samples_weights.setter
	def samples_weights ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapperSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )

		self._samples_weights = copy.deepcopy( value )
	
	# def samples_weights ( self, value )

# class FeaturesMapperSettings ( MapperSettings )