# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings

# ##################################################
# ###       CLASS IMAGES-TO-FLOAT-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesToFloatSettings',
	   namespace = 'pykeras.inputs',
	fields_names = []
	)
class ImagesToFloatSettings ( ImagesMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names=[],
		name='images-to-float'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesToFloatSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesToFloatSettings`): Instance to initialize.
			images_features_names      (`list` of `str`s): Name of the images to map.
			name                                  (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesToFloatSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( name, str )

		super( ImagesToFloatSettings, self ).__init__( images_features_names, name )

	# def __init__ ( self, ... )

# class ImagesToFloatSettings ( ImagesMapperSettings )