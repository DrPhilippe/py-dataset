# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ###       CLASS IMAGES-RESIZER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesResizerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'size',
		'method',
		'preserve_aspect_ratio',
		'antialias'
		]
	)
class ImagesResizerSettings ( ImagesTransformerSettings ):
	"""
	Mapper used to resize images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		size=(224,224),
		method='bilinear',
		preserve_aspect_ratio=False,
		antialias=False,
		affected_features_names=[],
		transform_matrix_feature_name='',
		name='images-resizer'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesResizerSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesResizerSettings`): Instance to initialize.
			images_features_names       (`list` of `str`): Names of the images features to resize.
			size                  (`tuple` of `2` `int`s): New size of the images `(height, width)`.
			method                                (`str`): Resize method.
			preserve_aspect_ratio                (`bool`): Whether to preserve the aspect ratio.
			antialias                            (`bool`): Whether to use an anti-aliasing filter when downsampling an image.
			affected_features_names        (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name             (`str`): If set the transformation matrix is outputed with the given name.
			name                                  (`str`): Name of the images resizer.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( size, tuple )
		assert pytools.assertions.tuple_items_type_is( size, int )
		assert pytools.assertions.equal( len(size), 2 )
		assert pytools.assertions.type_is( method, str )
		assert pytools.assertions.value_in( method, ['bilinear', 'lanczos3', 'lanczos5', 'bicubic', 'gaussian', 'nearest', 'area', 'mitchellcubic'] )
		assert pytools.assertions.type_is( preserve_aspect_ratio, bool )
		assert pytools.assertions.type_is( antialias, bool )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( ImagesResizerSettings, self ).__init__( images_features_names, affected_features_names, transform_matrix_feature_name, name )

		self._size                  = copy.deepcopy( size )
		self._method                = method
		self._preserve_aspect_ratio = preserve_aspect_ratio
		self._antialias             = antialias

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def size ( self ):
		"""
		New size of the images (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )

		return self._size

	# def size ( self )

	# --------------------------------------------------

	@size.setter
	def size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._size = value

	# def size ( self, value )

	# --------------------------------------------------

	@property
	def method ( self ):
		"""
		Resize method (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )

		return self._method

	# def method ( self )

	# --------------------------------------------------

	@method.setter
	def method ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['bilinear', 'lanczos3', 'lanczos5', 'bicubic', 'gaussian', 'nearest', 'area', 'mitchellcubic'] )

		self._method = value

	# def method ( self, value )

	# --------------------------------------------------

	@property
	def preserve_aspect_ratio ( self ):
		"""
		Whether to preserve the aspect ratio (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )

		return self._preserve_aspect_ratio

	# def preserve_aspect_ratio ( self )

	# --------------------------------------------------

	@preserve_aspect_ratio.setter
	def preserve_aspect_ratio ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )
		assert pytools.assertions.type_is( value, bool )

		self._preserve_aspect_ratio = value

	# def preserve_aspect_ratio ( self, value )

	# --------------------------------------------------

	@property
	def antialias ( self ):
		"""
		Whether to use an anti-aliasing filter when downsampling an image (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )

		return self._antialias

	# def antialias ( self )

	# --------------------------------------------------

	@antialias.setter
	def antialias ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesResizerSettings )
		assert pytools.assertions.type_is( value, bool )

		self._antialias = value

	# def antialias ( self, value )

# class ImagesResizerSettings ( ImagesTransformerSettings )