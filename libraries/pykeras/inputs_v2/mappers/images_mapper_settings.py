# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###        CLASS IMAGES-MAPPER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesMapperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'images_features_names'
		]
	)
class ImagesMapperSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names=[],
		name='images-mapper'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesMapperSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesMapperSettings`): Instance to initialize.
			images_features_names     (`list` of `str`s): Name of the images to map.
			name                                 (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesMapperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( name, str )

		super( ImagesMapperSettings, self ).__init__( name )

		self._images_features_names = copy.deepcopy( images_features_names )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def images_features_names ( self ):
		"""
		Name of the images to map (`list` of `str`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesMapperSettings )

		return self._images_features_names

	# def images_features_names ( self )

	# --------------------------------------------------
	
	@images_features_names.setter
	def images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesMapperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._images_features_names = value

	# def images_features_names ( self, value )

# class ImagesMapperSettings ( MapperSettings )