# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings

# ##################################################
# ###       CLASS IMAGES-SUMMARY-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesSummarySettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'log_directory_path',
		'log_name',
		'max_outputs'
		]
	)
class ImagesSummarySettings ( ImagesMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		log_directory_path=pytools.path.DirectoryPath(),
		log_name='images',
		images_features_names=[],
		max_outputs=3,
		name='images-summary'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesSummarySettings` class.

		Arguments:
			self           (`pykeras.inputs.ImagesSummarySettings`): Instance to initialize.
			log_directory_path (`str`/`pytools.path.DirectoryPath`): Path of the log directory.
			log_name                                        (`str`): Name of the log writer, i.e. the tensorboard group.
			images_features_names                (`list` of `str`s): Name of the images to summarize.
			max_outputs                                     (`int`): Maximum number of summaries.
			name                                            (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )
		assert pytools.assertions.type_is_instance_of( log_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is( log_name, str )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( max_outputs, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesSummarySettings, self ).__init__( images_features_names, name )

		self._log_directory_path = pytools.path.DirectoryPath.ensure( log_directory_path )
		self._log_name           = log_name
		self._max_outputs        = max_outputs

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def log_directory_path ( self ):
		"""
		Path of the log directory (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )

		return self._log_directory_path

	# def log_directory_path ( self )
	
	# --------------------------------------------------
	
	@log_directory_path.setter
	def log_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._log_directory_path = pytools.path.DirectoryPath.ensure( value )

	# def log_directory_path ( self, value )

	# --------------------------------------------------
	
	@property
	def log_name ( self ):
		"""
		Name of the log writer, i.e. the tensorboard group (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )

		return self._log_name

	# def log_name ( self )
	
	# --------------------------------------------------
	
	@log_name.setter
	def log_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._log_name = value

	# def log_name ( self, value )

	# --------------------------------------------------
	
	@property
	def max_outputs ( self ):
		"""
		Maximum number of summaries (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )

		return self._max_outputs

	# def max_outputs ( self )

	# --------------------------------------------------
	
	@max_outputs.setter
	def max_outputs ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesSummarySettings )
		assert pytools.assertions.type_is( value, int )

		self._max_outputs = value

	# def max_outputs ( self, value )

# class ImagesSummarySettings ( MapperSettings )