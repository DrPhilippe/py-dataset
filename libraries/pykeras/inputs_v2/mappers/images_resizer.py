# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_resizer_settings import ImagesResizerSettings
from .images_transformer      import ImagesTransformer

# ##################################################
# ###          CLASS IMAGES-TRANSFORMER          ###
# ##################################################

class ImagesResizer ( ImagesTransformer ):
	"""
	Mapper used to resize images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesResizer` class.

		Arguments:
			self             (`pykeras.inputs.ImagesResizer`): Instance to initialize.
			settings (`pykeras.inputs.ImagesResizerSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizer )
		assert pytools.assertions.type_is_instance_of( settings, ImagesResizerSettings )

		super( ImagesResizer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self             (`pykeras.inputs.ImagesResizer`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizer )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Number of images to resize
		number_of_images = len(self.settings.images_features_names)

		# Resize all named images
		for image_index in range(number_of_images):

			# Get image
			image_feature_name = self.settings.images_features_names[ image_index ]
			image              = features[ image_feature_name ]	
			image_dtype        = image.dtype
			image_shape        = tensorflow.shape( image )
			image_rank         = len(image_shape)
			image_height       = image_shape[0]
			image_width        = image_shape[1]
			new_height = self.settings.size[ 0 ]
			new_width  = self.settings.size[ 1 ]

			# Resize images
			image = tensorflow.cast( image, tensorflow.float32 )
			image = tensorflow.image.resize( image,
				                 size = self.settings.size,
				               method = self.settings.method,
				preserve_aspect_ratio = self.settings.preserve_aspect_ratio,
				            antialias = self.settings.antialias,
				                 name = 'resize-image'
				)
			# Restore the type of the image
			image = tensorflow.cast( image, image_dtype )
			
			# Replace the images in the batch
			features[ image_feature_name ] = image
		
		# for i in range(count)
		
		# Compute the scaling factors
		fy = tensorflow.divide( tensorflow.cast(new_height, tensorflow.float32), tensorflow.cast(image_height, tensorflow.float32) )
		fx = tensorflow.divide( tensorflow.cast(new_width,  tensorflow.float32), tensorflow.cast(image_width,  tensorflow.float32) )

		# Compose the transformation matrix
		resize_matrix = tensorflow.stack([
			fx, 0., 0.,
			0., fy, 0.,
			0., 1., 1.
			])
		resize_matrix = tensorflow.reshape( resize_matrix, [3, 3] )

		# Modify features affected by the cropping operation
		# and maybe save it
		features = self.map_affected_features( features, resize_matrix )

		return features
		
	# def map_features ( self, features )

	# --------------------------------------------------

	def map_features_names ( self, features_names ):
		"""
		Map the names of the features.

		Arguments:
			self (`pykeras.inputs.ImagesResizer`): Mapper instance.
			features_names                (`str`): Names of the features before mapping.

		Returns:
			`tuple` of `3` `list`s of `str`s: Names of the features after mapping.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesResizer )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		return features_names

	# def map_features_names ( self, features_names )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesResizerSettings`: The new features mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesResizerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesResizer ( ImagesTransformer )