# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings
from .mapper                 import Mapper

# ##################################################
# ###            CLASS IMAGES-MAPPER             ###
# ##################################################

class ImagesMapper ( Mapper ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.ImagesMapper` class.

		Arguments:
			self             (`pykeras.inputs.ImagesMapper`): Instance to initialize.
			settings (`pykeras.inputs.ImagesMapperSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesMapper )
		assert pytools.assertions.type_is_instance_of( settings, ImagesMapperSettings )

		super( ImagesMapper, self ).__init__( settings )

	# def __init__ ( self, ... )

# class ImagesMapper ( Mapper )