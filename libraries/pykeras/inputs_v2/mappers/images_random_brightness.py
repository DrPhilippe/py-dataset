# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_random_brightness_settings import ImagesRandomBrightnessSettings
from .images_random_adjustment          import ImagesRandomAdjustment

# ##################################################
# ###       CLASS IMAGES-RANDOM-BRIGHTNESS       ###
# ##################################################

class ImagesRandomBrightness ( ImagesRandomAdjustment ):
	"""
	Mapper used randomly adjust the brightness of images.

	All images are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_brightness`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomBrightness` class.

		Arguments:
			self             (`pykeras.inputs.ImagesRandomBrightness`): Instance to initialize.
			settings (`pykeras.inputs.ImagesRandomBrightnessSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomBrightness )
		assert pytools.assertions.type_is_instance_of( settings, ImagesRandomBrightnessSettings )

		super( ImagesRandomBrightness, self ).__init__(
			tensorflow.image.adjust_brightness,
			settings
			)

	# def __init__ ( self, settings )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesRandomBrightnessSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesRandomBrightnessSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesRandomBrightness ( ImagesRandomAdjustment )