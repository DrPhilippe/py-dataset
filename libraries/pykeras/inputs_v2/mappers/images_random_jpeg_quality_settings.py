# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment_settings import ImagesRandomAdjustmentSettings

# ##################################################
# ### CLASS IMAGES-RANDOM-JPEG-QUALITY-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomJpegQualitySettings',
	   namespace = 'pykeras.inputs',
	fields_names = []
	)
class ImagesRandomJpegQualitySettings ( ImagesRandomAdjustmentSettings ):
	"""
	Settings used to randomly adjust the JPEG quality of the images.

	All images of one example are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_jpeg_quality`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names = [],
		lower = 0.,
		upper = 1.,
		seed = None,
		name = 'images-random-jpeg-quality'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomJpegQualitySettings` class.
			
		Arguments:
			self (`pykeras.inputs.ImagesRandomJpegQualitySettings`): Instance to initialize.
			images_features_names                (`list` of `str`s): Name of the images to map.
			lower                                         (`float`): Lower bound of the random quality.
			upper                                         (`float`): Upper bound of the random quality.
			seed                     (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                            (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomJpegQualitySettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( lower, float )
		assert pytools.assertions.type_is( upper, float )
		assert pytools.assertions.true( upper >= 0. )
		assert pytools.assertions.true( upper <= 100. )
		assert pytools.assertions.true( lower >= 0. )
		assert pytools.assertions.true( lower <= upper )
		assert pytools.assertions.type_in( seed, (type(None), tuple) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomJpegQualitySettings, self ).__init__(
			images_features_names,
			lower,
			upper,
			seed,
			name
			)

	# def __init__ ( self, ... )

# class ImagesRandomJpegQualitySettings ( ImagesRandomAdjustmentSettings )