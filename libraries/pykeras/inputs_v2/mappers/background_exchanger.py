# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import random
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .background_exchanger_settings import BackgroundExchangerSettings
from .images_transformer            import ImagesTransformer

# ##################################################
# ###         CLASS BACKGROUND-EXCHANGER         ###
# ##################################################

class BackgroundExchanger ( ImagesTransformer ):
	"""
	Used to exchange the background of an image with an other.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.BackgroundExchanger` class.

		Arguments:
			self             (`pykeras.inputs.BackgroundExchanger`): Instance to initialize.
			settings (`pykeras.inputs.BackgroundExchangerSettings`): Background exchanger settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchanger )
		assert pytools.assertions.type_is_instance_of( settings, BackgroundExchangerSettings )

		super( BackgroundExchanger, self ).__init__( settings )
		
		self._backgrounds = None
		self._sizes = None
		self._number_of_backgrounds = 0

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def backgrounds ( self ):
		"""
		List of background images (`tensorflow.TensorArray`).
		"""
		return self._backgrounds

	# def backgrounds ( self )

	# --------------------------------------------------

	@property
	def number_of_backgrounds ( self ):
		"""
		Number of background images (`int`).
		"""
		return self._number_of_backgrounds

	# def number_of_backgrounds ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def load_backgrounds ( self ):
		"""
		Loads the background images.

		Arguments:
			self (`pykeras.inputs.BackgroundExchanger`): Instance of which to loaf backgrounds.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchanger )

		if self._backgrounds is not None:
			return

		# Check background directory
		if self.settings.backgrounds_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exists".format(self.settings.backgrounds_directory_path) )

		# Go through each content of the directory
		raw_backgrouns = []
		for content_path in self.settings.backgrounds_directory_path.list_contents():

			# Is it an image ?
			if content_path.is_a_file() and content_path.extension() in ('.jpg', '.jpeg', '.png'):

				background = cv2.imread( str(content_path), cv2.IMREAD_COLOR )
				raw_backgrouns.append( background )
			
			# if content_path.is_a_file() and content_path.extension() in ('.jpg', '.jpeg', '.png')

		# for content_path in self.settings.backgrounds_directory_path.list_contents()

		# number of backgrounds
		self._number_of_backgrounds = len( raw_backgrouns )

		# Allocate tensor array
		self._backgrounds = tensorflow.TensorArray(
			           dtype = tensorflow.dtypes.uint8,
			            size = self.number_of_backgrounds,
			    dynamic_size = False,
			     infer_shape = False,
			clear_after_read = False
			)

		# Set backgrounds in the tensor array
		for i in range(self.number_of_backgrounds):
			self._backgrounds = self._backgrounds.write( i, raw_backgrouns[i] )

	# def load_backgrounds ( self )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self       (`pykeras.inputs.BackgroundExchanger`): Background exchanger instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict`s of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchanger )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Load the backgrounds
		self.load_backgrounds()

		# Get the image and its number of channels
		image = features[ self.settings.image_feature_name ] # [H, W, C], uint8
		H = tensorflow.cast( tensorflow.shape( image )[ 0 ], tensorflow.int32 )
		W = tensorflow.cast( tensorflow.shape( image )[ 1 ], tensorflow.int32 )
		C = tensorflow.cast( tensorflow.shape( image )[ 2 ], tensorflow.int32 )

		# Use either user set size or image size
		if self.settings.crop_size is not None:
			CH = tensorflow.constant( self.settings.crop_size[0], dtype=tensorflow.int32 )
			CW = tensorflow.constant( self.settings.crop_size[1], dtype=tensorflow.int32 )
		else:
			CH = tensorflow.cast( tensorflow.shape( image )[ 0 ], tensorflow.int32 )
			CW = tensorflow.cast( tensorflow.shape( image )[ 1 ], tensorflow.int32 )

		# Get a radom backgrounds
		background_index = tensorflow.random.get_global_generator().uniform( (), 0, self.number_of_backgrounds, dtype=tensorflow.int32 ) # [1], int32
		background       = self.backgrounds.read( background_index ) # [BH, BW, C], uint8

		# Take a random crop of the background
		background = tensorflow.image.random_crop( background, size=(CH, CW, 3), name='crop-background' ) # [CH, CW, 3], uint8

		# Get the foreground mask
		mask = features[ self.settings.mask_feature_name ] # [H, W], bool
		mask = tensorflow.cast( mask, tensorflow.bool )
		
		# Repeat the mask to match the number of channels of the image
		mask = tensorflow.reshape( mask, [H, W, 1] ) # [H, W, 1], bool
		mask = tensorflow.tile( mask, [1, 1, C] ) # [H, W, C], bool

		if self.settings.past_mode == 'center':
			two = tensorflow.constant( 2, dtype=tensorflow.int32 )
			y  = tensorflow.cast( tensorflow.subtract( tensorflow.divide( CH, two ), tensorflow.divide( H, two ) ), tensorflow.int32 )
			x  = tensorflow.cast( tensorflow.subtract( tensorflow.divide( CW, two ), tensorflow.divide( W, two ) ), tensorflow.int32 )
		elif self.settings.past_mode == 'random':
			y = tensorflow.random.get_global_generator().uniform( (), 0, CH-H, dtype=tensorflow.int32 ) # [1], int32
			x = tensorflow.random.get_global_generator().uniform( (), 0, CW-W, dtype=tensorflow.int32 ) # [1], int32
		else:
			y = tensorflow.constant( 0, dtype=tensorflow.int32 )
			x = tensorflow.constant( 0, dtype=tensorflow.int32 )

		# Pad image and mask
		image = tensorflow.image.pad_to_bounding_box( image, y, x, CH, CW )
		mask  = tensorflow.image.pad_to_bounding_box( mask, y, x, CH, CW )
		
		# Do exchange
		image = tensorflow.where( mask, image, background ) # [CH, CW, C], uint8

		# Save the modified image
		features[ self.settings.image_feature_name ] = image
		features[ self.settings.mask_feature_name  ] = mask

		# Apply crop matrix to affected features
		past_matrix = tensorflow.stack([
			1., 0., tensorflow.cast( x, tensorflow.float32 ),
			0., 1., tensorflow.cast( y, tensorflow.float32 ),
			0., 1., 1.
			])
		past_matrix = tensorflow.reshape( past_matrix, [3, 3] )
		features = self.map_affected_features( features, past_matrix )

		# Return the modified features
		return features
		
	# def map_features ( self, features )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.BackgroundExchangerSettings`: The new background exchanger settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return BackgroundExchangerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class BackgroundExchanger ( Mapper )