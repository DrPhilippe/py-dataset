# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_mapper_settings import ImagesMapperSettings

# ##################################################
# ### CLASS IMAGES-RANDOM-GAUSSIAN-BLUR-SETTINGS ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomGaussianBlurSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'lower_kernel_size',
		'upper_kernel_size',
		'lower_sigma',
		'upper_sigma'
		'seed'
		]
	)
class ImagesRandomGaussianBlurSettings ( ImagesMapperSettings ):
	"""
	Settings used to randomly blur images.
	
	All images of one example are blurred with the same kernel.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names = [],
		lower_kernel_size = 1,
		upper_kernel_size = 5,
		lower_sigma = 1.,
		upper_sigma = 5.,
		seed = None,
		name = 'images-random-blur'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomGaussianBlurSettings` class.

		Arguments:
			self (`pykeras.inputs.ImagesRandomGaussianBlurSettings`): Instance to initialize.
			images_features_names                 (`list` of `str`s): Name of the images to map.
			lower_kernel_size                                (`int`): Lower bound of the kernel size.
			upper_kernel_size                                (`int`): Upper bound of the kernel size.
			lower_sigma                                    (`float`): Lower bound of the standard deviation.
			upper_sigma                                    (`float`): Upper bound of the standard deviation.
			seed                      (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                             (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( lower_kernel_size, int )
		assert pytools.assertions.type_is( upper_kernel_size, int )
		assert pytools.assertions.type_is( lower_sigma, float )
		assert pytools.assertions.type_is( upper_sigma, float )
		assert pytools.assertions.type_in( seed, (tuple, type(None)) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )	
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomGaussianBlurSettings, self ).__init__( images_features_names, name )

		self._lower_kernel_size = lower_kernel_size
		self._upper_kernel_size = upper_kernel_size
		self._lower_sigma       = lower_sigma
		self._upper_sigma       = upper_sigma
		self._seed              = copy.deepcopy( seed )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def lower_kernel_size ( self ):
		"""
		Lower bound of the kernel size (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )

		return self._lower_kernel_size

	# def lower_kernel_size ( self )

	# --------------------------------------------------

	@lower_kernel_size.setter
	def lower_kernel_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_is( value, int )

		self._lower_kernel_size = value

	# def lower_kernel_size ( self )

	# --------------------------------------------------

	@property
	def upper_kernel_size ( self ):
		"""
		Upper bound of the kernel size (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )

		return self._upper_kernel_size

	# def upper_kernel_size ( self )

	# --------------------------------------------------

	@upper_kernel_size.setter
	def upper_kernel_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_is( value, int )

		self._upper_kernel_size = value

	# def upper_kernel_size ( self )

	# --------------------------------------------------

	@property
	def lower_sigma ( self ):
		"""
		Lower bound of the standard deviation (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )

		return self._lower_sigma

	# def lower_sigma ( self )

	# --------------------------------------------------

	@lower_sigma.setter
	def lower_sigma ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_is( value, int )

		self._lower_sigma = value

	# def lower_sigma ( self )

	# --------------------------------------------------

	@property
	def upper_sigma ( self ):
		"""
		Upper bound of the standard deviation (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )

		return self._upper_sigma

	# def upper_sigma ( self )

	# --------------------------------------------------

	@upper_sigma.setter
	def upper_sigma ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_is( value, float )

		self._upper_sigma = value

	# def upper_sigma ( self )

	# --------------------------------------------------

	@property
	def seed ( self ):
		"""
		Random seed, or None (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		return self._seed

	# def seed ( self )

	# --------------------------------------------------

	@seed.setter
	def seed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomGaussianBlurSettings )
		assert pytools.assertions.type_in( value, (tuple, type(None)) )
		if isinstance( value, tuple ):
			assert pytools.assertions.equal( len(value), 2 )
			assert pytools.assertions.tuple_items_type_is( value, int )

		self._seed = copy.deepcopy( value )

	# def seed ( self )

# class ImagesRandomGaussianBlurSettings ( MapperSettings )