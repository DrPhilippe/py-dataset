# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_to_float_settings import ImagesToFloatSettings
from .images_mapper            import ImagesMapper

# ##################################################
# ###            CLASS IMAGES-RESIZER            ###
# ##################################################

class ImagesToFloat ( ImagesMapper ):
	"""
	Mapper used to convert images to floats with pixels value ranging from 0 to 1.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesToFloat` class.

		Arguments:
			self             (`pykeras.inputs.ImagesToFloat`): Instance to initialize.
			settings (`pykeras.inputs.ImagesToFloatSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesToFloat )
		assert pytools.assertions.type_is_instance_of( settings, ImagesToFloatSettings )

		super( ImagesToFloat, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self             (`pykeras.inputs.ImagesResizer`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesToFloat )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Number of images to resize
		number_of_images = len(self.settings.images_features_names)
	
		# Resize all named images
		for image_index in range(number_of_images):

			# Get image
			image_feature_name = self.settings.images_features_names[ image_index ]
			image              = features[ image_feature_name ]	
			
			# Convert image
			image = tensorflow.cast( image, tensorflow.float32 )
			image = tensorflow.math.divide( image, 255. )
			
			# Replace the images in the batch
			features[ image_feature_name ] = image

		# for i in range(count)

		return features
		
	# def map_features ( self, features )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.ImagesToFloatSettings`: The new settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return ImagesToFloatSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesToFloat ( ImagesMapper )