# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings
from .images_mapper               import ImagesMapper

# ##################################################
# ###            CLASS CENTER-CROPPER            ###
# ##################################################

class ImagesTransformer ( ImagesMapper ):
	"""
	Base class for mappers that resize, rotate, mirror, shift, and crop images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesTransformer` class.

		Arguments:
			self             (`pykeras.inputs.ImagesTransformer`): Images transformer instance to initialize.
			settings (`pykeras.inputs.ImagesTransformerSettings`): Center cropper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformer )
		assert pytools.assertions.type_is_instance_of( settings, ImagesTransformerSettings )

		super( ImagesTransformer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map_affected_features ( self, features, transform_matrix ):
		"""
		Maps the affected features in the given batch of features.

		Arguments:
			self         (`pykeras.inputs.ImagesTransformer`): Images transformer instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.
			transform_matrix            (`tensorflow.Tensor`): Transformation matrix.
		
		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformer )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Output transormation matrix ?
		if self.settings.transform_matrix_feature_name:
			features[ self.settings.transform_matrix_feature_name ] = transform_matrix

		# Create specifications for 2D sets of points
		# and sets of 3D matrices
		points_spec   = tensorflow.TensorShape( [None, 2] )
		matrices_spec = tensorflow.TensorShape( [None, 3, 3] )

		# Go though features to update
		for affected_feature_name in self.settings.affected_features_names:

			# Get the affected feature, save its dtype and shape
			feature = features[ affected_feature_name ]
			shape   = tensorflow.shape( feature )
			dtype   = feature.dtype

			# Convert it to a float32 tensor
			feature = tensorflow.cast( feature, tensorflow.float32 )

			# Decifer if it is a set of points ?
			if feature.shape[-1] == 2: #points_spec.is_compatible_with( feature.shape ):

				# Reshape the feature as a set of points
				feature = tensorflow.reshape( feature, [-1, 2] )

				# Convert them to homogenous coordinates
				ones    = tensorflow.ones( [tensorflow.shape(feature)[0], 1], dtype=tensorflow.float32 )
				feature = tensorflow.concat( [feature, ones], axis=-1 )
				
				# Transform the 2D points
				feature = tensorflow.linalg.matrix_transpose( feature )
				feature = tensorflow.linalg.matmul( transform_matrix, feature ) # 3x3 times 3xN
				feature = tensorflow.linalg.matrix_transpose( feature )
				
				# Discard the homogeneous coordinate
				feature = feature[ :, 0:2 ]

			# A set of matrices ?
			elif feature.shape[-1] == 3 and feature.shape[-2] == 3:
				
				# Reshape the feature as a set of 3x3 matrices
				feature = tensorflow.reshape( feature, [-1, 3, 3] )

				# Compose the two matrices
				feature = tensorflow.linalg.matmul( transform_matrix, feature )

			# Or an error
			else:
				raise ValueError(
					"The affected feature '{}' with shape {} is nor a set of 2D points nor a set of 3x3 matrices".format(
					affected_feature_name,
					feature.shape
					))
				
			# if points_spec.is_compatible_with( feature )

			# Restore the shape and dtype of the feature
			feature = tensorflow.reshape( feature, shape )
			feature = tensorflow.cast( feature, dtype )

			# Update affected feature
			features[ affected_feature_name ] = feature

		# for affected_feature_name in self.settings.affected_features_names

		# Return updated features
		return features

	# def map_affected_features ( self, features )
	
	# --------------------------------------------------

	def map_features_names ( self, features_names ):
		"""
		Map the names of the features.

		Arguments:
			self (`pykeras.inputs.ImagesTransformer`): Images transformer instance.
			features_names                    (`str`): Names of the features before transformation.

		Returns:
			`list` of `str`s/`tuple` of `3` `list`s of `str`s: Names of the features after transformation.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesTransformer )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		if self.settings.transform_matrix_feature_name:
			features_names.append( self.settings.transform_matrix_feature_name )

		return features_names

	# def map_features_names ( self, features_names )

# class ImagesTransformer ( ImagesMapper )