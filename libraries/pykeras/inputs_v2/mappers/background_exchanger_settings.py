# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ###    CLASS BACKGROUND-EXCHANGER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BackgroundExchangerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'backgrounds_directory_path',
		'image_feature_name',
		'mask_feature_name',
		'crop_size',
		'past_mode'
		]
	)
class BackgroundExchangerSettings ( ImagesTransformerSettings ):
	"""
	Used to exchange the background of an image with an other.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self,
		backgrounds_directory_path='',
		image_feature_name='',
		mask_feature_name='',
		crop_size=None,
		past_mode=None,
		affected_features_names=[],
		transform_matrix_feature_name='',
		name='background-exchanger'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.BackgroundExchangerSettings` class.

		Arguments:
			self            (`pykeras.inputs.BackgroundExchangerSettings`): Instance to initialize.
			backgrounds_directory_path (`pytools.path.DirecoryPath`/`str`): Path of the directory containing the background image.
			image_feature_name                                     (`str`): Name of the image feature of which to replace the background.
			mask_feature_name                                      (`str`): Name of the feature containing the mask.
			crop_size                       (`None`/`tuple` of `2` `int`s): Crop size as `(height, width)`.
			past_mode                                       (`None`/`str`): Crop mode either `None` same size, `center` or `random`.
			affected_features_names                     (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name                          (`str`): If set the transformation matrix is outputed with the given name.
			name                                                   (`str`): Name of the background exchanger.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_in( backgrounds_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_in( crop_size, (type(None), tuple) )
		if type(crop_size) == tuple:
			assert pytools.assertions.tuple_items_type_is( crop_size, int )
		assert pytools.assertions.type_in( past_mode, (type(None), str) )
		if type(past_mode) == str:
			assert pytools.assertions.value_in( past_mode, ['center', 'random'] )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( BackgroundExchangerSettings, self ).__init__( [], affected_features_names, transform_matrix_feature_name, name )

		self._backgrounds_directory_path = pytools.path.DirectoryPath.ensure( backgrounds_directory_path )
		self._image_feature_name         = image_feature_name
		self._mask_feature_name          = mask_feature_name
		self._crop_size                  = crop_size
		self._past_mode                  = past_mode
		self._affected_features_names    = affected_features_names

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def backgrounds_directory_path ( self ):
		"""
		Path of the directory containing the background image (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._backgrounds_directory_path
	
	# def backgrounds_directory_path ( self )

	# --------------------------------------------------

	@backgrounds_directory_path.setter
	def backgrounds_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is_instance_of( value, (pytools.path.DirectoryPath, str) )

		self._backgrounds_directory_path = pytools.path.DirectoryPath.ensure( value )
	
	# def backgrounds_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the image feature of which to replace the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

	# --------------------------------------------------
	
	@property
	def crop_size ( self ):
		return self._crop_size

	@crop_size.setter
	def crop_size ( self, value ):
		self._crop_size = value

	# --------------------------------------------------
	
	@property
	def past_mode ( self ):
		return self._past_mode

	@past_mode.setter
	def past_mode ( self, value ):
		self._past_mode = value

# class BackgroundExchangerSettings ( MapperSettings )