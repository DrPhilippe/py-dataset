# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .bounding_rectangle_cropper_settings import BoundingRectangleCropperSettings

# ##################################################
# ### CLASS BOUNDING-RECTANGLE-CROPPER-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RandomScaleBoundingRectangleCropperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'min_scale',
		'max_scale'
		]
	)
class RandomScaleBoundingRectangleCropperSettings ( BoundingRectangleCropperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		bounding_rectangle_feature_name='bounding_rectangle',
		crop_size=(224, 224),
		pad_size=(0, 0),
		min_scale=0.8,
		max_scale=1.2,
		affected_features_names=[],
		transform_matrix_feature_name='',
		crop_rectangle_feature_name='',
		name='random-scale-bounding-rectangle-cropper'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.RandomScaleBoundingRectangleCropperSettings` class.
	
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			self (`pykeras.inputs.RandomScaleBoundingRectangleCropperSettings`): Instance to initialize.
			images_features_names                 (`list` of `str`s): Name of the images to crop.
			bounding_rectangle_feature_name                  (`str`): Bounding rectangle of the object to crop.
			crop_size                        (`tuple` of `2` `int`s): Crop size as `(height, width)`.
			affected_features_names               (`list` of `str`s): Names of features affected by the crop.
			transform_matrix_feature_name                    (`str`): If set the crop translation matrix is outputed with the given name.
			crop_rectangle_feature_name                      (`str`): If set the crop rectangle is outputed with the given name.
			name                                             (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, RandomScaleBoundingRectangleCropperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( crop_size, tuple )
		assert pytools.assertions.type_is( min_scale, float )
		assert pytools.assertions.type_is( max_scale, float )
		assert pytools.assertions.tuple_items_type_is( crop_size, int )
		assert pytools.assertions.equal( len(crop_size), 2 )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( crop_rectangle_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( RandomScaleBoundingRectangleCropperSettings, self ).__init__(
			images_features_names,
			bounding_rectangle_feature_name,
			crop_size,
			pad_size,
			affected_features_names,
			transform_matrix_feature_name,
			crop_rectangle_feature_name,
			name
			)

		self._min_scale = min_scale
		self._max_scale = max_scale

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	@property
	def min_scale ( self ):
		return self._min_scale

	@min_scale.setter
	def min_scale( self, value ):
		assert pytools.assertions.type_is( value, float )
		self._min_scale = value

	@property
	def max_scale ( self ):
		return self._max_scale

	@max_scale.setter
	def max_scale( self, value ):
		assert pytools.assertions.type_is( value, float )
		self._max_scale = value

# class RandomScaleBoundingRectangleCropperSettings ( BoundingRectangleCropperSettings )