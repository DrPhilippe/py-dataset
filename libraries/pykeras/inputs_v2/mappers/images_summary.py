# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .images_summary_settings import ImagesSummarySettings
from .images_mapper           import ImagesMapper

# ##################################################
# ###            CLASS IMAGES-SUMMARY            ###
# ##################################################

class ImagesSummary ( ImagesMapper ):
	"""
	Summarize images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesSummary` class.

		Arguments:
			self             (`pykeras.inputs.ImagesResizer`): Instance to initialize.
			settings (`pykeras.inputs.ImagesResizerSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummary )
		assert pytools.assertions.type_is_instance_of( settings, ImagesSummarySettings )

		super( ImagesSummary, self ).__init__( settings )

		self._file_writer = None
		self._step        = 0

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self             (`pykeras.inputs.ImagesSummary`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummary )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Open the file writer if it is not
		if self._file_writer is None:
			logdir            = self.settings.log_directory_path + pytools.path.DirectoryPath( self.settings.log_name )
			self._file_writer = tensorflow.summary.create_file_writer( str(logdir) )

		# Summarize each image
		for image_feature_name in self.settings.images_features_names:
			
			image = features[ image_feature_name ]
			
			# Reshape the image so that it is [BATCH, HEIGHT, WIDTH, CHANNELS]
			shape = list(image.shape)
			rank = len(shape)
			if ( rank == 3 ):
				height = shape[0]
				width  = shape[1]
				depth  = shape[2]
			elif ( rank == 2 ):
				height = shape[0]
				width  = shape[1]
				depth  = 1
			else:
				raise RuntimeError( "Image '{}' has an unsupported shape '{}'".format(image_feature_name, shape) )
			
			# Reshape
			image = tensorflow.reshape( image, [1, height, width, depth], name='resize-image-summary' )

			# Summarize
			with self._file_writer.as_default():
				image = tensorflow.reverse( image, axis=[-1] )
				tensorflow.summary.image( image_feature_name, image, step=self._step, max_outputs=self.settings.max_outputs )

		# for image_name in images_names

		# Next step
		self._step += 1

		# Return untouched features
		return features

	# def map_features ( self, features )
	
	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self ):
		"""
		Callback used to notify this mapper that one epoch has passed.

		Arguments:
			self (`pykeras.inputs.ImagesSummary`): Images summary instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesSummary )
		
		# Reset step
		self._step = 0

	# def on_epoch_end ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the images summary of type `cls`.
		
		Arguments:
			cls (`type`): Type of images summary for which to create settings.

		Named Arguments:
			Forwarded to the images summary's settings constructor.

		Returns:
			`pykeras.inputs.ImagesSummarySettings`: The new images summary settings instance.
		"""
		return ImagesSummarySettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImagesSummary ( ImagesMapper )