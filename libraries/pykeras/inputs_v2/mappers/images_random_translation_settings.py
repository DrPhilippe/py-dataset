# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ###  CLASS IMAGES-RANDOM-TRANSLATION-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomTranslationSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'min_delta_x',
		'max_delta_x',
		'min_delta_y',
		'max_delta_y',
		'seed'
		]
	)
class ImagesRandomTranslationSettings ( ImagesTransformerSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		affected_features_names=[],
		transform_matrix_feature_name='',
		min_delta_x = -16,
		max_delta_x = 16,
		min_delta_y = -16,
		max_delta_y = 16,
		seed = None,
		name = 'images-random-translation'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomTranslationSettings` class.
		
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			images_features_names                      (`list` of `str`s): Name of the images to translate.
			affected_features_names                    (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name                         (`str`): If set the rotation matrix is outputed with the given name.
			min_delta_x                                           (`int`): Minimum translation on X.
			max_delta_x                                           (`int`): Maximum translation on X.
			min_delta_y                                           (`int`): Minimum translation on Y.
			max_delta_y                                           (`int`): Maximum translation on Y.
			seed                           (`tuple` of `2` `int`s/`None`): Random seed, or None.
			mode                                   ('roll' or 'pad+crop'): Mode defining how translation is implemented.
			name                                                  (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( min_delta_x, int )
		assert pytools.assertions.type_is( max_delta_x, int )
		assert pytools.assertions.type_is( min_delta_y, int )
		assert pytools.assertions.type_is( max_delta_y, int )
		assert pytools.assertions.type_in( seed, (tuple, type(None)) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomTranslationSettings, self ).__init__(
			images_features_names,
			affected_features_names,
			transform_matrix_feature_name,
			name
			)

		self._min_delta_x = min_delta_x
		self._max_delta_x = max_delta_x
		self._min_delta_y = min_delta_y
		self._max_delta_y = max_delta_y
		self._seed        = copy.deepcopy( seed )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def min_delta_x ( self ):
		"""
		Minimum translation on X (`int`).
		"""
		return self._min_delta_x

	# --------------------------------------------------

	@min_delta_x.setter
	def min_delta_x ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._min_delta_x = value

	# --------------------------------------------------
	
	@property
	def max_delta_x ( self ):
		"""
		Maximum translation on X (`int`).
		"""
		return self._max_delta_x

	# --------------------------------------------------

	@max_delta_x.setter
	def max_delta_x ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._max_delta_x = value

	# --------------------------------------------------
	
	@property
	def min_delta_y ( self ):
		"""
		Minimum translation on Y (`int`).
		"""
		return self._min_delta_y

	# --------------------------------------------------

	@min_delta_y.setter
	def min_delta_y ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._min_delta_y = value

	# --------------------------------------------------
	
	@property
	def max_delta_y ( self ):
		"""
		Maximum translation on Y (`int`).
		"""
		return self._max_delta_y

	# --------------------------------------------------

	@max_delta_y.setter
	def max_delta_y ( self, value ):
		assert pytools.assertions.type_is( value, int )
		self._max_delta_y = value

	# --------------------------------------------------

	@property
	def seed ( self ):
		"""
		Random seed, or None (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )

		return self._seed

	# def seed ( self )

	# --------------------------------------------------

	@seed.setter
	def seed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomTranslationSettings )
		assert pytools.assertions.type_in( value, (tuple, type(None)) )
		if isinstance( value, tuple ):
			assert pytools.assertions.equal( len(value), 2 )
			assert pytools.assertions.tuple_items_type_is( value, int )

		self._seed = copy.deepcopy( value )

	# def seed ( self )

# class ImagesRandomTranslationSettings ( ImagesTransformerSettings )