# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow

# INTERNALS
import pykeras
import pytools

# LOCALS
from .mapper import Mapper
from .points_normalizer_settings import PointsNormalizerSettings

# ##################################################
# ###          CLASS POINTS-NORMALIZER           ###
# ##################################################

class PointsNormalizer ( Mapper ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, PointsNormalizer )
		assert pytools.assertions.type_is_instance_of( settings, PointsNormalizerSettings )

		super( PointsNormalizer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map_features ( self, features ):
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		# Get the image, points, and points weights features
		image_batch   = features[ self.settings.image_feature_name  ] # [width, height, channels]
		points_batch  = features[ self.settings.points_feature_name ] # [CLASSES, 8, 2]
		
		# Get the image(s) width and height
		image_width  = tensorflow.shape( image_batch )[ 0 ]
		image_height = tensorflow.shape( image_batch )[ 1 ]
		image_width  = tensorflow.cast( image_width,  tensorflow.float32 )
		image_height = tensorflow.cast( image_height, tensorflow.float32 )

		# New range
		new_min = tensorflow.constant( self.settings.new_min, dtype=tensorflow.float32 )
		new_max = tensorflow.constant( self.settings.new_max, dtype=tensorflow.float32 )

		# Normalize points
		points_batch = pykeras.pose_utils.normalize_images_points( points_batch, image_width, image_height, new_min, new_max )

		# Set points
		features[ self.settings.points_feature_name ] = points_batch

		# Return mapped batch
		return features

	# def map_features ( ... )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		assert pytools.assertions.type_is( cls, type )

		return PointsNormalizerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class PointsNormalizer( Mapper )