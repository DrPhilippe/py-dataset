# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ### CLASS IMAGES-RANDOM-90-DEGREES-ROTATION-SETTINGS ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandom90DegreesRotationSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'seed'
		]
	)
class ImagesRandom90DegreesRotationSettings ( ImagesTransformerSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		affected_features_names=[],
		transform_matrix_feature_name='',
		seed = None,
		name='images-random-90-degrees-rotation'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandom90DegreesRotationSettings` class.
		
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			self (`pykeras.inputs.ImagesRandom90DegreesRotationSettings`): Instance to initialize.
			images_features_names                      (`list` of `str`s): Name of the images to rotate.
			affected_features_names                    (`list` of `str`s): Names of features affected by the transformation.
			transform_matrix_feature_name                         (`str`): If set the rotation matrix is outputed with the given name.
			seed                           (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                                  (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotationSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_in( seed, (tuple, type(None)) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandom90DegreesRotationSettings, self ).__init__(
			images_features_names,
			affected_features_names,
			transform_matrix_feature_name,
			name
			)

		self._seed = copy.deepcopy( seed )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def seed ( self ):
		"""
		Random seed, or None (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotationSettings )

		return self._seed

	# def seed ( self )

	# --------------------------------------------------

	@seed.setter
	def seed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagesRandom90DegreesRotationSettings )
		assert pytools.assertions.type_in( value, (tuple, type(None)) )
		if isinstance( value, tuple ):
			assert pytools.assertions.equal( len(value), 2 )
			assert pytools.assertions.tuple_items_type_is( value, int )

		self._seed = copy.deepcopy( value )

	# def seed ( self )

# class ImagesRandom90DegreesRotationSettings ( ImagesTransformerSettings )