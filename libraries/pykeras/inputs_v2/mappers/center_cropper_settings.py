# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
from .images_transformer_settings import ImagesTransformerSettings

# ##################################################
# ###       CLASS CENTER-CROPPER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CenterCropperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'crop_size',
		'crop_rectangle_feature_name',
		]
	)
class CenterCropperSettings ( ImagesTransformerSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		images_features_names=[],
		crop_size=(224, 224),
		affected_features_names=[],
		transform_matrix_feature_name='',
		crop_rectangle_feature_name='',
		name='images-cropper'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.CenterCropperSettings` class.
	
		Affected features can be:
			* sets of 2D points on the image with dtype `float32` and shape compatible with `(-1, 2)`.
			* sets of 3D projection matrices or 2D affine transformation matrices with dtype `float32` and shape compatible with `(-1, 3, 3)`.

		Arguments:
			self (`pykeras.inputs.CenterCropperSettings`): Instance to initialize.
			images_features_names      (`list` of `str`s): Name of the images to crop.
			crop_size             (`tuple` of `2` `int`s): Crop size as `(height, width)`.
			affected_features_names    (`list` of `str`s): Names of features affected by the crop.
			transform_matrix_feature_name         (`str`): If set the crop translation matrix is outputed with the given name.
			crop_rectangle_feature_name           (`str`): If set the crop rectangle is outputed with the given name.
			name                                  (`str`): Name of the cropper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( crop_size, tuple )
		assert pytools.assertions.tuple_items_type_is( crop_size, int )
		assert pytools.assertions.equal( len(crop_size), 2 )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )
		assert pytools.assertions.type_is( transform_matrix_feature_name, str )
		assert pytools.assertions.type_is( crop_rectangle_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( CenterCropperSettings, self ).__init__(
			images_features_names,
			affected_features_names,
			transform_matrix_feature_name,
			name
			)

		self._crop_size                   = copy.deepcopy( crop_size )
		self._crop_rectangle_feature_name = crop_rectangle_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def crop_size ( self ):
		"""
		Crop size as `(height, width)` (`tuple` of `2` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )

		return self._crop_size

	# def crop_size ( self )
	
	# --------------------------------------------------
	
	@crop_size.setter
	def crop_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )
		assert pytools.assertions.type_is( value, tuple )
		assert pytools.assertions.equal( len(value), 2 )
		assert pytools.assertions.tuple_items_type_is( value, int )

		self._crop_size = value

	# def crop_size ( self, value )

	# --------------------------------------------------
	
	@property
	def crop_height ( self ):
		"""
		Crop height (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )

		return self._crop_size[ 0 ]

	# def crop_height ( self )

	# --------------------------------------------------
	
	@property
	def crop_width ( self ):
		"""
		Crop width (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )

		return self._crop_size[ 1 ]

	# def crop_width ( self )

	# --------------------------------------------------
	
	@property
	def crop_rectangle_feature_name ( self ):
		"""
		If set the crop rectangle is outputed with the given name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )

		return self._crop_rectangle_feature_name

	# def crop_rectangle_feature_name ( self )
	
	# --------------------------------------------------
	
	@crop_rectangle_feature_name.setter
	def crop_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CenterCropperSettings )
		assert pytools.assertions.type_is( value, str )

		self._crop_rectangle_feature_name = value

	# def crop_rectangle_feature_name ( self, value )

# class CenterCropperSettings ( ImagesTransformerSettings )