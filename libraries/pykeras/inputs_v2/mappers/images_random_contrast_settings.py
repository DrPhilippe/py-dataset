# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .images_random_adjustment_settings import ImagesRandomAdjustmentSettings

# ##################################################
# ###  CLASS IMAGES-RANDOM-BRIGHTNESS-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagesRandomContrastSettings',
	   namespace = 'pykeras.inputs',
	fields_names = []
	)
class ImagesRandomContrastSettings ( ImagesRandomAdjustmentSettings ):
	"""
	Settings used to randomly adjust the contrast of images.

	All images of one example are adjusted with the same random value.

	See:
		`tensorflow.image.adjust_contrast`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,		
		images_features_names = [],
		lower = 0.,
		upper = 1.,
		seed = None,
		name = 'images-random-contrast'
		):
		"""
		Initializes a new instance of the `pykeras.inputs.ImagesRandomContrastSettings` class.
	
		0 mean no contrast.
		1 mean no changes in contrast.
		
		Arguments:
			self (`pykeras.inputs.ImagesRandomContrastSettings`): Instance to initialize.
			images_features_names             (`list` of `str`s): Name of the images to map.
			lower                                      (`float`): Lower bound of the random contrast offset.
			upper                                      (`float`): Upper bound of the random contrast offset.
			seed                  (`tuple` of `2` `int`s/`None`): Random seed, or None.
			name                                         (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagesRandomContrastSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( lower, float )
		assert pytools.assertions.type_is( upper, float )
		assert pytools.assertions.type_in( seed, (type(None), tuple) )
		if isinstance( seed, tuple ):
			assert pytools.assertions.equal( len(seed), 2 )
			assert pytools.assertions.tuple_items_type_is( seed, int )
		assert pytools.assertions.type_is( name, str )

		super( ImagesRandomContrastSettings, self ).__init__(
			images_features_names,
			lower,
			upper,
			seed,
			name
			)

	# def __init__ ( self, ... )

# class ImagesRandomContrastSettings ( ImagesRandomAdjustmentSettings )