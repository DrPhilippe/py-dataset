# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from .features_mapper_settings import FeaturesMapperSettings
from .mapper                   import Mapper

# ##################################################
# ###           CLASS FEATURES-MAPPER            ###
# ##################################################

class FeaturesMapper ( Mapper ):
	"""
	Used to rename, duplicate or remove features from a batch.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pykeras.inputs.FeaturesMapper` class.

		Arguments:
			self             (`pykeras.inputs.FeaturesMapper`): Instance to initialize.
			settings (`pykeras.inputs.FeaturesMapperSettings`): Features mapper settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapper )
		assert pytools.assertions.type_is_instance_of( settings, FeaturesMapperSettings )

		super( FeaturesMapper, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Maps the given batch of features.

		Arguments:
			self            (`pykeras.inputs.FeaturesMapper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.

		Returns:
			`tuple` of `3` `dict`s of `str` to `tensorflow.Tensor`: Mapped batch of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )

		inputs          = self.map_features_susbet( features, self.settings.inputs )
		labels          = self.map_features_susbet( features, self.settings.labels )
		samples_weights = self.map_features_susbet( features, self.settings.samples_weights )

		return (inputs, labels, samples_weights)
		
	# def map_features ( self, features )

	# --------------------------------------------------

	def map_features_names ( self, features_names ):
		"""
		Map the names of the features.

		Arguments:
			self (`pykeras.inputs.FeaturesMapper`): Mapper instance.
			features_names                 (`str`): Names of the features before mapping.

		Returns:
			`tuple` of `3` `list`s of `str`s: Names of the features after mapping.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapper )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		return (
			list( self.settings.inputs.keys() ),
			list( self.settings.labels.keys() ),
			list( self.settings.samples_weights.keys() )
			)

	# def map_features_names ( self, features_names )

	# --------------------------------------------------

	def map_features_susbet ( self, features, mapping ):
		"""
		Perform the mapping of feaures for the given mapping subset.
		
		Arguments:
			self            (`pykeras.inputs.FeaturesMapper`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Batch of features to map.
			mapping                (`dict` of `str` to `str`): Mapping settings.
		
		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Batch of mapped features.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeaturesMapper )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		assert pytools.assertions.type_is( mapping, dict )
		assert pytools.assertions.dictionary_types_are( mapping, str, str )

		result = {}
		for new_name, old_name in mapping.items():
			
			# Check
			if old_name not in features:
				raise RuntimeError( "Trying to map feature '{}'' to feature '{}', but '{}'' does not exist in {}".format(
					old_name,
					new_name,
					old_name,
					list(features.keys())
					))

			result[ new_name ] = features[ old_name ]

		# for new_name, old_name in mapping.items()

		return result

	# def map_features_susbet ( self, features, mapping )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of mapper for which to create settings.

		Returns:
			`pykeras.inputs.FeaturesMapperSettings`: The new features mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return FeaturesMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class FeaturesMapper ( Mapper )