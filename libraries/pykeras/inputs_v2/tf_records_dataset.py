# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools

# LOCALS
from ..                       import records
from .dataset                 import Dataset
from .tf_records_dataset_data import TFRecordsDatasetData

# ##################################################
# ###          CLASS TF-RECORDS-DATASET          ###
# ##################################################

class TFRecordsDataset ( Dataset ):
	"""
	This dataset is used to load tf-records datasets created using `pykeras.records`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data ):
		"""
		Initializes a new instance of the `pykeras.inputs.TFRecordsDataset` class.

		Arguments:
			self     (`pykeras.inputs.TFRecordsDataset`): Instance to initialze.
			data (`pykeras.inputs.TFRecordsDatasetData`): Data of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )
		assert pytools.assertions.type_is_instance_of( data, TFRecordsDatasetData )

		# Path of the blueprint file
		blueprint_filename = data.blueprint_filename
		if '{}' in blueprint_filename:
			blueprint_filename = blueprint_filename.format( data.name )
		data.blueprint_filename = blueprint_filename
		blueprint_filepath = data.directory_path + pytools.path.FilePath( data.blueprint_filename )

		# Path of the tf records file
		tf_record_filename = data.tf_record_filename
		if '{}' in tf_record_filename:
			tf_record_filename = tf_record_filename.format( data.name )
		data.tf_record_filename = tf_record_filename
		
		super( TFRecordsDataset, self ).__init__( data )

		# Load the blueprint
		self._blueprint  = records.DatasetBlueprint( blueprint_filepath.deserialize_json() )

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def blueprint ( self ):
		"""
		Blueprint of the dataset (`pykeras.records.DatasetBlueprint`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )

		return self._blueprint

	# def blueprint ( self )

	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )

		return self.blueprint.number_of_examples
	
	# def number_of_examples ( self )
	
	# --------------------------------------------------

	@property
	def raw_features_names ( self ):
		"""
		Names of features (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )

		return self.blueprint.features_names
	
	# def raw_features_names ( self )

	# --------------------------------------------------

	@property
	def shuffle_buffer_size ( self ):
		"""
		Size of the shuffling buffer (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )

		return self.data.shuffle_buffer_size
	
	# def shuffle_buffer_size ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def summary ( self, logger ):
		"""
		Prints a short description of this dataset.

		Arguments:
			self (`pykeras.inputs.Dataset`): Dataset to summarize.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( logger, pytools.tasks.Logger )

		logger.log( 'TF-Records Dataset "{}":', self.name )
		logger.log( '    directory_path         : {}', self.data.directory_path )
		logger.log( '    blueprint_filename     : {}', self.data.blueprint_filename )
		logger.log( '    tf_record_filename     : {}', self.data.tf_record_filename )
		logger.log( '    initial_features_names : {}', self.initial_features_names )
		logger.log( '    steps_per_epoch        : {}', self.steps_per_epoch )
		logger.log( '    number_of_examples     : {}', self.number_of_examples )
		logger.log( '    repeat                 : {}', self.repeat )
		logger.log( '    batch_size             : {}', self.batch_size )
		logger.log( '    drop_batch_reminder    : {}', self.drop_batch_reminder )
		logger.log( '    shuffle                : {}', self.shuffle )
		logger.log( '    prefetch_size          : {}', self.prefetch_size )
		logger.log( '    mappers                : {}', self.mappers_names )

		features_names = self.features_names
		if isinstance( features_names, list ):
			logger.log( '    features_names         : {}', self.features_names )
		else:	
			logger.log( '    inputs_names           : {}', features_names[0] )
			logger.log( '    labels_names           : {}', features_names[1] )
			logger.log( '    sample_weights_names   : {}', features_names[2] )

		logger.log( '' )
		logger.end_line()
		logger.flush()

	# def summary ( self, logger )

	# --------------------------------------------------

	def use ( self, logger=None ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self (`pykeras.inputs.TFRecordsDataset`): Dataset to use.
			logger   (`None`/`pytools.tasks.Logger`): logger used to log.

		Returns:
			`tensorflow.data.TFRecordDataset`: This dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFRecordsDataset )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		# TFRecord filepath
		tf_recod_filepath = self.data.directory_path + pytools.path.FilePath( self.data.tf_record_filename )

		# Mampping functions used to prepate the dataset
		def parser_fn ( example ):
			with tensorflow.name_scope( 'parser' ):
				return self._blueprint.parse_example_record( example )

		# Function used to only keep initial features
		def keep_fn ( features ):
			with tensorflow.name_scope( 'keep' ):
				keys = list(features.keys())
				for key in keys:
					if key not in self.initial_features_names:
						features[ key ] = None
						del features[ key ]
				return features

		# Open the dataset
		dataset = tensorflow.data.TFRecordDataset( str(tf_recod_filepath), self._blueprint.data.compression_type )
		if logger:
			logger.log( 'Pipeline Of "{}":', self.name )
			logger.log( '    load( {} )', str(tf_recod_filepath) )

		# Parse examples
		if logger:
			logger.log( '    parse( {} )', self.raw_features_names )
		dataset = dataset.map( parser_fn, tensorflow.data.AUTOTUNE )

		# Only keep initial features names
		if logger:
			logger.log( '    keep( {} )', self.initial_features_names )
		dataset = dataset.map( keep_fn )
		
		# Repeat dataset
		if self.repeat: # -1 is True
			count = self.number_of_repetitions
			if logger:
				logger.log( '    repeat( {} )', count )
			with tensorflow.name_scope( 'repeat' ):
				dataset = dataset.repeat( count )

		# Shuffle examples
		if self.shuffle:
			if logger:
				logger.log( '    shuffle( {} )', self.shuffle_buffer_size )
			with tensorflow.name_scope( 'shuffle' ):
				dataset = dataset.shuffle( self.shuffle_buffer_size )

		# Map examples
		with tensorflow.name_scope( 'map' ):
			for mapper in self.mappers:
				if logger:
					logger.log( '    map( {} )', mapper.name )
				dataset = dataset.map( mapper, tensorflow.data.AUTOTUNE )

		# Batch examples
		if self.batch_size > 0:
			if logger:
				logger.log( '    batch( {}, {} )', self.batch_size, self.drop_batch_reminder )
			with tensorflow.name_scope( 'batch' ):
				dataset = dataset.batch( self.batch_size, self.drop_batch_reminder )
	
		# Prefetch examples
		if self.prefetch_size != 0:
			if logger:
				logger.log( '    prefetch( {} )', self.prefetch_size )
			with tensorflow.name_scope( 'prefetch' ):
				dataset = dataset.prefetch( self.prefetch_size )

		# Log
		if logger:
			logger.end_line()
			logger.flush()

		# Returned prepared dataset
		return dataset

	# def use ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def open ( cls, **kwargs ):
		"""
		Creates a new instance of the `pykeras.inputs.TFRecordsDataset` class.

		Named Arguments:
			directory_path (`str`/`pytools.path.DirectoryPath`): Location of the tfrecord and blueprint files.
			tf_record_filename                          (`str`): TFRecord filename.
			blueprint_filename                          (`str`): Blueprint filename.
			name                                        (`str`): Name of the dataset.
			shuffle                                    (`bool`): Should examples be shuffled.
			shuffle_buffer_size                         (`int`): Size of the shuffling buffer.
			batch_size                                  (`int`): Number of examples included in each batch.
			drop_batch_reminder                        (`bool`): Should the remaining examples be dropped when batching.
			mappers (`list` of `pykeras.inputs.MapperSettings`): Settings of the online mappers applied to this dataset.
			prefetch_size                               (`int`): Number of batch to prefetch while the model is consuming the current one.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Create the dataset sequence data
		data = TFRecordsDatasetData( **kwargs )

		# Create the dataset sequence
		return cls( data )

	# def open ( cls, **kwargs )

# class TFDataset ( Dataset )