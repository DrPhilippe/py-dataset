# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import random
import tensorflow

# INTERNALS
import pydataset
import pytools

# LOCALS
from .dataset         import Dataset
from .py_dataset_data import PyDatasetData

# ##################################################
# ###          CLASS TF-RECORDS-DATASET          ###
# ##################################################

class PyDataset ( Dataset ):
	"""
	This dataset is used to load tf-records datasets created using `pykeras.records`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data ):
		"""
		Initializes a new instance of the `pykeras.inputs.PyDataset` class.

		Arguments:
			self     (`pykeras.inputs.PyDataset`): Instance to initialze.
			data (`pykeras.inputs.PyDatasetData`): Data of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )
		assert pytools.assertions.type_is_instance_of( data, PyDatasetData )

		# Init parent
		super( PyDataset, self ).__init__( data )

		# Load iterator
		self._iterator = pydataset.dataset.Iterator( self.data.url_search_pattern )

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL used to find examples of this dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )

		return self.data.url_search_pattern
	
	# def url_search_pattern ( self )

	# --------------------------------------------------

	@property
	def iterator ( self ):
		"""
		Iterator used to iterate example (`pydataset.dataset.Iterator`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )

		return self._iterator

	# def iterator ( self )

	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )

		return self.iterator.count
	
	# def number_of_examples ( self )
	
	# --------------------------------------------------

	@property
	def raw_features_names ( self ):
		"""
		Names of features (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )

		return self.initial_features_names
	
	# def raw_features_names ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def summary ( self, logger ):
		"""
		Prints a short description of this dataset.

		Arguments:
			self (`pykeras.inputs.PyDataset`): Dataset to summarize.
			logger   (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )
		assert pytools.assertions.type_is_instance_of( logger, pytools.tasks.Logger )

		logger.log( 'PyDataset "{}":', self.name )
		logger.log( '    steps_per_epoch     : {}', self.steps_per_epoch )
		logger.log( '    number_of_examples  : {}', self.number_of_examples )
		logger.log( '    repeat              : {}', self.repeat )
		logger.log( '    batch_size          : {}', self.batch_size )
		logger.log( '    drop_batch_reminder : {}', self.drop_batch_reminder )
		logger.log( '    shuffle             : {}', self.shuffle )
		logger.log( '    prefetch_size       : {}', self.prefetch_size )
		logger.log( '    mappers             : {}', self.mappers_names )

		features_names = self.features_names
		if isinstance( features_names, list ):
			logger.log( '    features_names      : {}', self.features_names )
		else:	
			logger.log( '    inputs_names        : {}', features_names[0] )
			logger.log( '    labels_names        : {}', features_names[1] )
			logger.log( '    sample_weights_names: {}', features_names[2] )

		logger.log( '' )
		logger.end_line()
		logger.flush()

	# def summary ( self, logger )

	# --------------------------------------------------

	def use ( self, logger ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self      (`pykeras.inputs.PyDataset`): Dataset to use.
			logger (`None`/`pytools.tasks.Logger`): logger used to log.

		Returns:
			`tensorflow.data.Dataset`: Dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		# Generator function
		def generator_fn ():
			# Examples indexes
			indexes = list( range(self.iterator.count) )

			# Repeat dataset
			number_of_repetitions = self.number_of_repetitions
			epoch = 0
			while ( number_of_repetitions == -1 or epoch < number_of_repetitions ):
				
				# Shuffle examples
				if self.shuffle:
					random.shuffle( indexes )

				# Provide one epoch worse of examples
				for i in indexes:
					features = self.iterator.at( i ).get_features( self.initial_features_names )
					data = {}
					for name, feature in features.items():
						data[ name ] = feature.value
					yield data

				# Notify mappers that one epoch passed
				for mapper in mappers:
					mapper.on_epoch_end()

			# End of generation
			raise StopIteration()

		def get_signature ():
			features  = self.iterator.at(0).get_features( self.initial_features_names )
			signature = {}
			for feature_name, feature in features.items():				
				if isinstance( feature.data, pydataset.dataset.BoolFeatureData ):
					spec = tensorflow.TensorSpec( shape=(), dtype=tensorflow.dtypes.bool )
				
				elif isinstance( feature.data, pydataset.dataset.BytesFeatureData ):
					spec = tensorflow.TensorSpec( shape=(), dtype=tensorflow.dtypes.string )
				
				elif isinstance( feature.data, pydataset.dataset.IntFeatureData ):
					spec = tensorflow.TensorSpec( shape=(), dtype=tensorflow.dtypes.int32 )
				
				elif isinstance( feature.data, pydataset.dataset.FloatFeatureData ):
					spec = tensorflow.TensorSpec( shape=(), dtype=tensorflow.dtypes.float32 )
				
				elif isinstance( feature.data, pydataset.dataset.ImageFeatureData ):
					spec = tensorflow.TensorSpec( shape=feature.data.shape, dtype=tensorflow.dtypes.uint8 )
				
				elif isinstance( feature.data, pydataset.dataset.NDArrayFeatureData ):
					spec = tensorflow.TensorSpec( shape=feature.data.shape, dtype=feature.data.dtype )

				elif isinstance( feature.data, pydataset.dataset.TextFeatureData ):
					spec = tensorflow.TensorSpec( shape=(), dtype=tensorflow.dtypes.string )

				else:
					raise RuntimeError( "Unsupported feature data type: {}".format(feature.data) )
				
				signature[ feature_name ] = spec
	
			return signature

		# Function used to only keep initial features
		def keep_fn ( features ):
			keys = list(features.keys())
			for key in keys:
				if key not in self.initial_features_names:
					features[ key ] = None
					del features[ key ]
			return features

		# Create the dataset using a generator that uses the iterator
		if logger:
			logger.log( 'Pipeline Of "{}":', self.name )
			logger.log( '    load( "{}" )', self.url_search_pattern )
		dataset = tensorflow.data.Dataset.from_generator( generator_fn, output_signature=get_signature() )
		
		# Only keep initial features names
		if logger:
			logger.log( '    keep( {} )', self.initial_features_names )
		dataset = dataset.map( keep_fn )

		# Batch examples
		if self.batch_size > 0:
			if logger:
				logger.log( '    batch( {}, {} )', self.batch_size, self.drop_batch_reminder )
			dataset = dataset.batch( self.batch_size, self.drop_batch_reminder )
		
		# Map examples
		for mapper in self.mappers:
			if logger:
				logger.log( '    map( {} )', mapper.name )
			dataset.map( mapper, tensorflow.data.AUTOTUNE )

		# Prefetch examples
		if self.prefetch_size > 0:
			if logger:
				logger.log( '    prefetch( {} )', self.prefetch_size )
			dataset = dataset.prefetch( self.prefetch_size )

		# Log
		if logger:
			logger.end_line()
			logger.flush()

		# Returned prepared dataset
		return dataset

	# def use ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def open ( cls, **kwargs ):
		"""
		Creates a new instance of the `pykeras.inputs.PyDataset` class.

		Named Arguments:
			url_search_pattern                          (`str`): URL used to find examples of this dataset
			name                                        (`str`): Name of the dataset.
			shuffle                                    (`bool`): Should examples be shuffled.
			shuffle_buffer_size                         (`int`): Size of the shuffling buffer.
			batch_size                                  (`int`): Number of examples included in each batch.
			drop_batch_reminder                        (`bool`): Should the remaining examples be dropped when batching.
			mappers (`list` of `pykeras.inputs.MapperSettings`): Settings of the online mappers applied to this dataset.
			prefetch_size                               (`int`): Number of batch to prefetch while the model is consuming the current one.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Create the dataset sequence data
		data = PyDatasetData( **kwargs )

		# Create the dataset sequence
		return cls( data )

	# def open ( cls, **kwargs )

# class PyDataset ( Dataset )