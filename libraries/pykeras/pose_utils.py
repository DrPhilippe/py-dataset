# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def normalize_images_points ( points, image_width, image_height, target_min, target_max ):

	target_len = tensorflow.subtract( target_max, target_min )

	points = tensorflow.math.divide( points, [image_width, image_height] )
	points = tensorflow.math.multiply( points, [target_len, target_len] )
	points = tensorflow.math.add( points, [target_min, target_min] )
	
	return points
	
# def normalize_images_points ( ... )

# --------------------------------------------------

def rescale_image_points ( points, image_width, image_height, target_min, target_max ):

	target_len = tensorflow.subtract( target_max, target_min )

	points = tensorflow.math.subtract( points, [target_min, target_min] )
	points = tensorflow.math.divide( points, [target_len, target_len] )
	points = tensorflow.math.multiply( points, [image_width, image_height] )

	return points

# def rescale_image_points ( points, image_shape )

# --------------------------------------------------

def normalize_control_points ( points, image_width, image_height ):
	
	points = tensorflow.math.divide( points, [image_width, image_height] )
	points = tensorflow.math.multiply( points, [2.0, 2.0] )
	points = tensorflow.math.subtract( points, [1.0, 1.0] )
	return points
	
# def normalize_control_points ( points, image_width, image_height )

# --------------------------------------------------

def rescale_control_points ( points, image_width, image_height ):

	points = tensorflow.math.add( points, [1.0, 1.0] )
	points = tensorflow.math.divide( points, [2.0, 2.0] )
	points = tensorflow.math.multiply( points, [image_width, image_height] )
	return points

# def rescale_control_points ( points, image_width, image_height )