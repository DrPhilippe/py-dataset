# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###            CLASS ONLINE-MAPPER             ###
# ##################################################

class OnlineMapper:
	"""
	Mapper applied to input batch durring accesing batched example of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.OnlineMapper` class.

		Arguments:
			self             (`pykeras.inputs.OnlineMapper`): Instance to initialize.
			settings (`pykeras.inputs.OnlineMapperSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )
		assert pytools.assertions.type_is_instance_of( settings, OnlineMapperSettings )

		self._settings = settings

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def settings ( self ):
		"""
		Settings of the online mapper (`pykeras.inputs.OnlineMapperSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )

		return self._settings
	
	# def settings ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch ):
		"""
		Maps the given batch of inputs and labels features.

		Arguments:
			self    (`pykeras.inputs.OnlineMapper`): Instance to initialize.
			inputs_batch (`dict` of `str` to `any`): Batch of inputs to map.
			labels_batch (`dict` of `str` to `any`): Batch of labels to map.

		Returns:
			inputs_batch (`dict` of `str` to `any`): Mapped batch of inputs.
			labels_batch (`dict` of `str` to `any`): Mapped batch of labels.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )

		return inputs_batch, labels_batch

	# def map ( self, inputs_batch, labels_batch )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __call__ ( self, inputs_batch, labels_batch ):
		"""
		Maps the given batch.

		Arguments:
			self    (`pykeras.inputs.OnlineMapper`): Mapper.
			inputs_batch (`dict` of `str` to `any`): Batch of inputs to map.
			labels_batch (`dict` of `str` to `any`): Batch of labels to map.

		Returns:
			inputs_batch (`dict` of `str` to `any`): Mapped batch of inputs.
			labels_batch (`dict` of `str` to `any`): Mapped batch of labels.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )

		return self.map( inputs_batch, labels_batch )

	# def __call__ ( self, inputs_batch, labels_batch )

	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self ):
		"""
		Callback used to notify this mapper that one epoch has passed.

		Arguments:
			self (`pykeras.inputs.OnlineMapper`): Mapper to notify.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )
		
		pass

	# def on_epoch_end ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, **kwargs ):
		"""
		Creates a new instance of the online mapper class `cls`.

		Arguments:
			cls (`type`): Type of online mapper to create.

		Named Arguments:
			Forwarded to the online mapper's settings constructor.

		Return:
			`pykeras.inputs.OnlineMapper`: The new online mapper instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Create the online mapper settings
		settings = cls.create_settings( **kwargs )

		# Create the online mapper
		return cls( settings )

	# def create ( cls, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the online mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of online mapper for which to create settings.

		Named Arguments:
			Forwarded to the online mapper's settings constructor.

		Returns:
			`pykeras.inputs.OnlineMapperSettings`: The new online mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )

# class OnlineMapper