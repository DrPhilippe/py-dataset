# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS

# INTERNALS
import pydataset.data
import pytools.assertions

# LOCALS
from .online_feature_adapter_settings import OnlineFeatureAdapterSettings
from .online_mapper import OnlineMapper

# ##################################################
# ###        CLASS ONLINE-FEATURE-ADAPTER        ###
# ##################################################

class OnlineFeatureAdapter ( OnlineMapper ):
	"""
	Used to rename, duplicate or remove features from a batch.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapter )
		assert pytools.assertions.type_is_instance_of( settings, OnlineFeatureAdapterSettings )

		super( OnlineFeatureAdapter, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapter )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )
		
		if len(self.settings.inputs_adaptations.keys()) > 0:
			inputs_batch = self.map_features( inputs_batch, self.settings.inputs_adaptations )

		if len(self.settings.labels_adaptations.keys()) > 0:
			labels_batch = self.map_features( labels_batch, self.settings.labels_adaptations )

		return inputs_batch, labels_batch

	# def map ( self, inputs_batch, labels_batch )

	# --------------------------------------------------

	def map_features ( self, features_batch, adaptations ):
		"""
		Reorganize the given batch of features using the given adaptation settings.

		Arguments:
		
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapter )
		assert pytools.assertions.type_is( features_batch, dict )
		assert pytools.assertions.type_is( adaptations, dict )
		assert pytools.assertions.dictionary_types_are( adaptations, str, str )

		# Names of feature to produce
		adapted_names = list(adaptations.keys())

		# Initialize a new batch container
		adapted_batch = {}

		# Populate the new batch
		for adapted_name in adapted_names:

			# Get the name of the feature in the raw batch
			feature_name = adaptations[ adapted_name ]

			# Copy the feature in the new batch and rename it
			adapted_batch[ adapted_name ] = features_batch[ feature_name ]

		# for feature_name in out_names

		return adapted_batch

	# def map_features ( self, features_batch, adaptions )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the feature adapter of type `cls`.
		
		Arguments:
			cls (`type`): Type of feature adapter for which to create settings.

		Named Arguments:
			inputs_adaptations (`dict` of `str` to `str`): New input feature name mapped to the old input feature name
			labels_adaptations (`dict` of `str` to `str`): New label feature name mapped to the old label feature name
			name                                  (`str`): Name of the online feature adapter.

		Returns:
			`pykeras.inputs.OnlineFeatureAdapterSettings`: The new online feature adapter settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return OnlineFeatureAdapterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class OnlineFeatureAdapter ( OnlineMapper )