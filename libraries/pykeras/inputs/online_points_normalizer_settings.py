# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###  CLASS ONLINE-POINTS-NORMALIZER-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlinePointsNormalizerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'image_feature_name',
		'points_feature_name'
		]
	)
class OnlinePointsNormalizerSettings ( OnlineMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, image_feature_name='', points_feature_name='', name='online-points-normalizer' ):
		"""
		Initializes a new instance of the `pykeras.inputs.OnlinePointsNormalizerSettings` class.

		Arguments:
			self (`pykeras.inputs.OnlinePointsNormalizerSettings`): Instance to initialize.
			image_feature_name                             (`str`): Name of the input feature containing the image.
			points_feature_name                            (`str`): Name of the label feature containing the control points sets to normalize.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizerSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( points_feature_name, str )

		super( OnlinePointsNormalizerSettings, self ).__init__( name )

		self._image_feature_name  = image_feature_name
		self._points_feature_name = points_feature_name

	# def __init__ ( self, image_feature_name, points_feature_name, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the input feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizerSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def points_feature_name ( self ):
		"""
		Name of the label feature containing the control points sets to normalize (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizerSettings )

		return self._points_feature_name
	
	# def points_feature_name ( self )

	# --------------------------------------------------

	@points_feature_name.setter
	def points_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._points_feature_name = value
	
	# def points_feature_name ( self, value )

# class OnlinePointsNormalizerSettings ( OnlineMapperSettings )