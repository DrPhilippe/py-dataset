# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .dataset_data import DatasetData

# ##################################################
# ###           CLASS TF-DATASET-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TFDatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'directory_path',
		'tf_record_filename',
		'blueprint_filename',
		'drop_batch_reminder',
		'shuffle_buffer_size',
		'prefetch_size'
		]
	)
class TFDatasetData ( DatasetData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		directory_path,
		tf_record_filename='{}.tfrecords',
		blueprint_filename='{}.json',
		drop_batch_reminder=False,
		shuffle_buffer_size=512,
		prefetch_size=8,
		**kwargs
		):
		"""
		Initialize a new instance of the `pykeras.inputs.TFDatasetData` class.

		Arguments:
			self               (`pykeras.inputs.TFDatasetData`): Instance to initialize.
			directory_path (`str`/`pytools.path.DirectoryPath`): Location of the tfrecord and blueprint files.
			tf_record_filename                          (`str`): TFRecord filename.
			blueprint_filename                          (`str`): Blueprint filename.
			drop_batch_reminder                        (`bool`): Should the remaining examples be dropped when batching.
			shuffle_buffer_size                         (`int`): Size of the shuffling buffer.
			prefetch_size                               (`int`): Number of batch to prefetch while the model is consuming the current one.

		Named Arguments:
			inputs_features_names                                   (`list` of `str`): Names of features considered as inputs.
			labels_features_names                                   (`list` of `str`): Names of features considered as labels.
			batch_size                                                        (`int`): Number of examples included in each batch.
			shuffle                                                          (`bool`): Should examples be shuffled.
			online_mappers_settings (`list` of `pykeras.inputs.OnlineMapperSettings`): Settings of the online mappers applied to this dataset.
			name                                                              (`str`): Name of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is_instance_of( directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( tf_record_filename, str )
		assert pytools.assertions.type_is( blueprint_filename, str )
		assert pytools.assertions.type_is( drop_batch_reminder, bool )
		assert pytools.assertions.type_is( shuffle_buffer_size, int )
		assert pytools.assertions.type_is( prefetch_size, int )

		super( TFDatasetData, self ).__init__( **kwargs )

		self._directory_path      = pytools.path.DirectoryPath( directory_path ) if isinstance( directory_path, str ) else directory_path
		self._tf_record_filename  = tf_record_filename
		self._blueprint_filename  = blueprint_filename
		self._drop_batch_reminder = drop_batch_reminder
		self._shuffle_buffer_size = shuffle_buffer_size
		self._prefetch_size       = prefetch_size

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def directory_path ( self ):
		"""
		Location of the tfrecord and blueprint files (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._directory_path
	
	# def directory_path ( self )

	# --------------------------------------------------

	@directory_path.setter
	def directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._directory_path = pytools.path.DirectoryPath( value ) if isinstance( value, str ) else value
	
	# def directory_path ( self )

	# --------------------------------------------------

	@property
	def tf_record_filename ( self ):
		"""
		TFRecord filename (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._tf_record_filename
	
	# def tf_record_filename ( self )

	# --------------------------------------------------

	@tf_record_filename.setter
	def tf_record_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is( value, str )

		self._tf_record_filename = value
	
	# def tf_record_filename ( self )

	# --------------------------------------------------

	@property
	def blueprint_filename ( self ):
		"""
		Blueprint filename (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._blueprint_filename
	
	# def blueprint_filename ( self )

	# --------------------------------------------------

	@blueprint_filename.setter
	def blueprint_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is( value, str )

		self._blueprint_filename = value
	
	# def blueprint_filename ( self )

	# --------------------------------------------------

	@property
	def drop_batch_reminder ( self ):
		"""
		Should the remaining examples be dropped when batching (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._drop_batch_reminder
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@drop_batch_reminder.setter
	def drop_batch_reminder ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is( value, bool )

		self._drop_batch_reminder = value
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@property
	def shuffle_buffer_size ( self ):
		"""
		Size of the shuffling buffer (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._shuffle_buffer_size
	
	# def shuffle_buffer_size ( self )

	# --------------------------------------------------

	@shuffle_buffer_size.setter
	def shuffle_buffer_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is( value, int )

		self._shuffle_buffer_size = value
	
	# def shuffle_buffer_size ( self )

	# --------------------------------------------------

	@property
	def prefetch_size ( self ):
		"""
		Number of batch to prefetch while the model is consuming the current one (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )

		return self._prefetch_size
	
	# def prefetch_size ( self )

	# --------------------------------------------------

	@prefetch_size.setter
	def prefetch_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TFDatasetData )
		assert pytools.assertions.type_is( value, int )

		self._prefetch_size = value
	
	# def prefetch_size ( self )

# class TFDatasetData ( DatasetData )