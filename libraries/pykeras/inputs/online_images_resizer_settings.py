# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###    CLASS ONLINE-IMAGES-RESIZER-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlineImagesResizerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'inputs_images_features_names',
		'inputs_new_sizes',
		'labels_images_features_names',
		'labels_new_sizes'
		]
	)
class OnlineImagesResizerSettings ( OnlineMapperSettings ):
	"""
	Resize the images
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		inputs_images_features_names=[], inputs_new_sizes=[],
		labels_images_features_names=[], labels_new_sizes=[],
		name='online-image-resizer'
		):
		if isinstance( inputs_images_features_names, str ):
			inputs_images_features_names = [inputs_images_features_names]
		if isinstance( inputs_new_sizes, pydataset.data.ShapeData ):
			inputs_new_sizes = [inputs_new_sizes]
		if isinstance( labels_images_features_names, str ):
			labels_images_features_names = [labels_images_features_names]
		if isinstance( labels_new_sizes, pydataset.data.ShapeData ):
			labels_new_sizes = [labels_new_sizes]

		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )
		assert pytools.assertions.type_is( inputs_images_features_names, list )
		assert pytools.assertions.list_items_type_is( inputs_images_features_names, str )
		assert pytools.assertions.type_is( inputs_new_sizes, list )
		assert pytools.assertions.list_items_type_is( inputs_new_sizes, pydataset.data.ShapeData )
		assert pytools.assertions.true( len(inputs_images_features_names)==len(inputs_new_sizes) or len(inputs_new_sizes)==1 )
		assert pytools.assertions.type_is( labels_images_features_names, list )
		assert pytools.assertions.list_items_type_is( labels_images_features_names, str )
		assert pytools.assertions.type_is( labels_new_sizes, list )
		assert pytools.assertions.list_items_type_is( labels_new_sizes, pydataset.data.ShapeData )
		assert pytools.assertions.true( len(labels_images_features_names)==len(labels_new_sizes) or len(labels_new_sizes)==1 )
		assert pytools.assertions.type_is( name, str )

		super( OnlineImagesResizerSettings, self ).__init__( name )

		self._inputs_images_features_names = copy.deepcopy( inputs_images_features_names )
		self._inputs_new_sizes             = copy.deepcopy( inputs_new_sizes )
		self._labels_images_features_names = copy.deepcopy( labels_images_features_names )
		self._labels_new_sizes             = copy.deepcopy( labels_new_sizes )

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def inputs_images_features_names ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )

		return self._inputs_images_features_names
	
	# def inputs_images_features_names ( self )

	# --------------------------------------------------

	@inputs_images_features_names.setter
	def inputs_images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._inputs_images_features_names = value
	
	# def inputs_images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def inputs_new_sizes ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )

		return self._inputs_new_sizes
	
	# def inputs_new_sizes ( self )

	# --------------------------------------------------

	@inputs_new_sizes.setter
	def inputs_new_sizes ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, pydataset.data.ShapeData )

		self._inputs_new_sizes = value
	
	# def inputs_new_sizes ( self, value )

	# --------------------------------------------------

	@property
	def labels_images_features_names ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )

		return self._labels_images_features_names
	
	# def labels_images_features_names ( self )

	# --------------------------------------------------

	@labels_images_features_names.setter
	def labels_images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._labels_images_features_names = value
	
	# def labels_images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def labels_new_sizes ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )

		return self._labels_new_sizes
	
	# def labels_new_sizes ( self )

	# --------------------------------------------------

	@labels_new_sizes.setter
	def labels_new_sizes ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, pydataset.data.ShapeData )

		self._labels_new_sizes = value
	
	# def labels_new_sizes ( self, value )

# class OnlineImagesResizerSettings ( OnlineMapperSettings )