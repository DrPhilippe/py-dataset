# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###    CLASS ONLINE-IMAGES-RESIZER-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlineFeatureAdapterSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'inputs_adaptations',
		'labels_adaptations'
		]
	)
class OnlineFeatureAdapterSettings ( OnlineMapperSettings ):
	"""
	This online mapper is used to rename, deplicate, or remove features from a batch.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, inputs_adaptations={}, labels_adaptations={}, name='online-feature-adapter' ):
		"""
		Initializes a new instance of the `pykeras.inputs.OnlineFeatureAdapterSettings` class.

		Arguments:
			self (`pykeras.inputs.OnlineFeatureAdapterSettings`): Instance to initialize.
			inputs_adaptations        (`dict` of `str` to `str`): New input feature name mapped to the old input feature name
			labels_adaptations        (`dict` of `str` to `str`): New label feature name mapped to the old label feature name
			name                                         (`str`): Name of the online feature adapter.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapterSettings )
		assert pytools.assertions.type_is( inputs_adaptations, dict )
		assert pytools.assertions.dictionary_types_are( inputs_adaptations, str, str )
		assert pytools.assertions.type_is( labels_adaptations, dict )
		assert pytools.assertions.dictionary_types_are( labels_adaptations, str, str )

		super( OnlineFeatureAdapterSettings, self ).__init__( name )

		self._inputs_adaptations = copy.deepcopy( inputs_adaptations )
		self._labels_adaptations = copy.deepcopy( labels_adaptations )

	# def __init__ ( self, inputs_adaptations, labels_adaptations, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def inputs_adaptations ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapterSettings )

		return self._inputs_adaptations
	
	# def inputs_adaptations ( self )

	# --------------------------------------------------

	@inputs_adaptations.setter
	def inputs_adaptations ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )

		self._inputs_adaptations = value
	
	# def inputs_adaptations ( self, value )

	# --------------------------------------------------

	@property
	def labels_adaptations ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapterSettings )

		return self._labels_adaptations
	
	# def labels_adaptations ( self )

	# --------------------------------------------------

	@labels_adaptations.setter
	def labels_adaptations ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineFeatureAdapterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )

		self._labels_adaptations = value
	
	# def labels_adaptations ( self, value )

# class OnlineFeatureAdapterSettings ( OnlineMapperSettings )