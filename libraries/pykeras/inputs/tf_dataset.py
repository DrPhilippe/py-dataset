# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import sys
import tensorflow

# INTERNALS
import pykeras
import pytools

# LOCALS
from .dataset         import Dataset
from .tf_dataset_data import TFDatasetData

# ##################################################
# ###              CLASS TF-DATASET              ###
# ##################################################

class TFDataset ( Dataset ):
	"""
	This dataset is used to load tf-records datasets created using `pykeras.records`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data ):
		"""
		Initializes a new instance of the `pykeras.inputs.TFDataset` class.

		Arguments:
			self     (`pykeras.inputs.TFDataset`): Instance to initialze.
			data (`pykeras.inputs.TFDatasetData`): Data of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )
		assert pytools.assertions.type_is_instance_of( data, TFDatasetData )

		# Path of the blueprint file
		blueprint_filename = data.blueprint_filename
		if '{}' in blueprint_filename:
			blueprint_filename = blueprint_filename.format( data.name )
		data.blueprint_filename = blueprint_filename
		blueprint_filepath = data.directory_path + pytools.path.FilePath( data.blueprint_filename )

		# Path of the tf records file
		tf_record_filename = data.tf_record_filename
		if '{}' in tf_record_filename:
			tf_record_filename = tf_record_filename.format( data.name )
		data.tf_record_filename = tf_record_filename
		
		super( TFDataset, self ).__init__( data )

		# Load the blueprint
		self._blueprint = pykeras.records.DatasetBlueprint( blueprint_filepath.deserialize_json() )

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		return self._blueprint.data.number_of_examples
	
	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def drop_batch_reminder ( self ):
		"""
		Should the remaining examples be dropped when batching (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		return self.data.drop_batch_reminder
	
	# def drop_batch_reminder ( self )

	# --------------------------------------------------

	@property
	def shuffle_buffer_size ( self ):
		"""
		Size of the shuffling buffer (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		return self.data.shuffle_buffer_size
	
	# def shuffle_buffer_size ( self )

	# --------------------------------------------------

	@property
	def prefetch_size ( self ):
		"""
		Number of batch to prefetch while the model is consuming the current one (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		return self.data.prefetch_size
	
	# def prefetch_size ( self )

	# --------------------------------------------------

	@property
	def steps_per_epoch ( self ):
		"""
		Number of steps per epoch (`int`).

		This is actually the number of batch required to complete one epoch.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		# Number of complete steps
		steps_per_epoch = self.number_of_examples // self.batch_size

		# Is there a batch reminder, and do we keep it ?
		reminder = self.number_of_examples % self.batch_size
		if reminder and not self.drop_batch_reminder:
			steps_per_epoch += 1

		# Result
		return steps_per_epoch

	# def steps_per_epoch ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def summary ( self, logger ):
		"""
		Prints a short description of this dataset.

		Arguments:
			self (`pykeras.inputs.TFDataset`): Dataset to summarize.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		logger.log( 'TF Dataset "{}":', self.name )
		logger.log( '                compression_type: {}', self._blueprint.data.compression_type )
		logger.log( '               compression_level: {}', self._blueprint.data.compression_level )
		logger.log( '              number_of_examples: {}', self.number_of_examples )
		logger.log( '                  features_names: {}', self._blueprint.features_names )
		logger.log( '                         shuffle: {}', self.shuffle )
		logger.log( '             shuffle_buffer_size: {}', self.shuffle_buffer_size )
		logger.log( '                      batch_size: {}', self.batch_size )
		logger.log( '             drop_batch_reminder: {}', self.drop_batch_reminder )
		logger.log( '                   prefetch_size: {}', self.prefetch_size )
		logger.log( '                 steps_per_epoch: {}', self.steps_per_epoch )
		logger.log( '           inputs_features_names: {}', self.inputs_features_names )
		logger.log( '           labels_features_names: {}', self.labels_features_names )
		logger.log( '    sample_weight_features_names: {}', self.sample_weight_features_names )
		logger.log( '                  online_mappers: {}', [mapper.settings.name for mapper in self.online_mappers] )
		logger.log( '' )

	# def summary ( self, logger )

	# --------------------------------------------------

	def use ( self, logger ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self (`pykeras.inputs.TFDataset`): Dataset to use.

		Returns:
			`tensorflow.data.TFRecordDataset`: This dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, TFDataset )

		# TFRecord filepath
		tf_recod_filepath = self.data.directory_path + pytools.path.FilePath( self.data.tf_record_filename )

		# Mampping functions used to prepate the dataset
		def parser_fn ( example ):
			return self._blueprint.parse_example_record( example )
		
		def group_fn ( features ):
			return self.group_features( features )

		def map_fn ( inputs, labels, sample_weight=None ):
			return self.map_batch( inputs, labels, sample_weight )

		# Open the dataset
		if logger:
			logger.log( 'PIPELINE {}:', self.name )
			logger.log( 'load()' )
		dataset = tensorflow.data.TFRecordDataset( str(tf_recod_filepath), self._blueprint.data.compression_type )
		
		# Parse examples
		if logger:
			logger.log( 'parse()' )
		dataset = dataset.map( parser_fn, tensorflow.data.AUTOTUNE )

		# Group inputs and labels
		if logger:
			logger.log( 'group()' )
		dataset = dataset.map( group_fn, tensorflow.data.AUTOTUNE )

		# Shuffle examples
		if self.shuffle:
			if logger:
				logger.log( 'shuffle( {} )', self.shuffle_buffer_size )
			dataset = dataset.shuffle( self.shuffle_buffer_size )

		# Batch examples
		if self.batch_size > 0:
			if logger:
				logger.log( 'batch( {}, {} )', self.batch_size, self.drop_batch_reminder )
			dataset = dataset.batch( self.batch_size, drop_remainder=self.drop_batch_reminder )
		
		# Map examples
		if logger:
			logger.log( 'map( {} )', [m.settings.name for m in self.online_mappers] )
		dataset = dataset.map( map_fn, tensorflow.data.AUTOTUNE )

		# Prefetch examples
		if self.prefetch_size > 0:
			if logger:
				logger.log( 'prefetch( {} )', self.prefetch_size )
			dataset = dataset.prefetch( self.prefetch_size )

		return dataset

	# def use ( self, logger )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def open ( cls, **kwargs ):
		"""
		Creates a new instance of the `pykeras.inputs.SequenceDataset` class.

		Named Arguments:
			directory_path                       (`str`/`pytools.path.DirectoryPath`): Location of the tfrecord and blueprint files.
			tf_record_filename                                                (`str`): TFRecord filename.
			blueprint_filename                                                (`str`): Blueprint filename.
			name                                                              (`str`): Name of the dataset.
			inputs_features_names                                   (`list` of `str`): Names of features considered as inputs.
			labels_features_names                                   (`list` of `str`): Names of features considered as labels.
			shuffle                                                          (`bool`): Should examples be shuffled.
			shuffle_buffer_size                                               (`int`): Size of the shuffling buffer.
			batch_size                                                        (`int`): Number of examples included in each batch.
			drop_batch_reminder                                              (`bool`): Should the remaining examples be dropped when batching.
			online_mappers_settings (`list` of `pykeras.inputs.OnlineMapperSettings`): Settings of the online mappers applied to this dataset.
			prefetch_size                                                     (`int`): Number of batch to prefetch while the model is consuming the current one.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Create the dataset sequence data
		data = TFDatasetData( **kwargs )

		# Create the dataset sequence
		return cls( data )

	# def open ( cls, **kwargs )

# class TFDataset ( Dataset )