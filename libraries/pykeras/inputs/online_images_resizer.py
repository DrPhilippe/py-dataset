# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import tensorflow

# INTERNALS
import pydataset.data
import pytools.assertions

# LOCALS
from .online_images_resizer_settings import OnlineImagesResizerSettings
from .online_mapper import OnlineMapper

# ##################################################
# ###        CLASS ONLINE-IMAGES-RESIZER         ###
# ##################################################

class OnlineImagesResizer ( OnlineMapper ):
	"""
	Resize the images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizer )
		assert pytools.assertions.type_is_instance_of( settings, OnlineImagesResizerSettings )

		super( OnlineImagesResizer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizer )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )

		inputs_batch = self.map_images( inputs_batch, self.settings.inputs_images_features_names, self.settings.inputs_new_sizes )
		labels_batch = self.map_images( labels_batch, self.settings.labels_images_features_names, self.settings.labels_new_sizes )
		
		return inputs_batch, labels_batch

	# def map ( self, inputs_batch, labels_batch )

	# --------------------------------------------------

	def map_images ( self, batch, images_features_names, new_sizes ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesResizer )
		assert pytools.assertions.type_is( batch, dict )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( new_sizes, list )
		assert pytools.assertions.list_items_type_is( new_sizes, pydataset.data.ShapeData )

		# Number of images to resize
		count = len(images_features_names)

		# Duplicate new size if needed
		if len(new_sizes) == 1 and count > 1:
			new_sizes = new_sizes * count
		
		# Resize all named images
		for i in range(count):

			# Get image (batched) and create new image buffer (batched)
			image_feature_name = images_features_names[ i ]
			images             = batch[ image_feature_name ]
			
			# Image shape
			shape      = images.shape
			batch_size = shape[ 0 ]
			width      = shape[ 1 ]
			height     = shape[ 2 ]
			depth      = shape[ 3 ] if len(shape) > 3 else 1

			# Fetch settings for this image
			new_size       = new_sizes[ i ]
			new_width      = new_size[ 0 ]
			new_height     = new_size[ 1 ]
			new_shape      = list(shape)
			new_shape[ 1 ] = new_width
			new_shape[ 2 ] = new_height
			if isinstance( images, numpy.ndarray ):
				old_images = numpy.reshape( images, [batch_size, width, height, depth] )
			else:
				old_images = tensorflow.reshape( images, [-1, width, height, depth], name='reshape-old-image' )

			# Resize all image in the batch
			if isinstance( old_images, numpy.ndarray ):
				
				new_images = numpy.empty( [batch_size, new_width, new_height, depth], images.dtype )
				
				for i in range( batch_size ):
					old_image = old_images[i, :, :, :]
					new_image = cv2.resize( old_image, (new_width, new_height) )
					new_image = numpy.reshape( new_image, [new_width, new_height, depth] )
					new_images[i, :, :, :] = new_image
			else:
				new_images = tensorflow.image.resize( old_images, [new_width, new_height], name='resize-images' )
			
			# Replace the images in the batch
			batch[ image_feature_name ] = new_images

		# for i in range(count)

		return batch

	# def map_images ( self, batch, images_features_names, new_size )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return OnlineImagesResizerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class OnlineImagesResizer ( OnlineMapper )