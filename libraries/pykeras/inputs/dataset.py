# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .dataset_data  import DatasetData
from .online_mapper import OnlineMapper

# ##################################################
# ###               CLASS DATASET                ###
# ##################################################

class Dataset:
	"""
	Base class for datasets used as model inputs.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, data ):
		"""
		Initialize a new instance of the `pykeras.inputs.Dataset` class.

		Arguments:
			self     (`pykeras.inputs.Dataset`): Instance to initialize.
			data (`pykeras.inputs.DatasetData`): Data describing the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( data, DatasetData )

		# Initialize fields
		self._data = data
		self._online_mappers = []

		# Reload mappers
		# self._load_online_mappers()

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing the dataset (`pykeras.inputs.DatasetData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self._data
	
	# def data ( self )
	
	# --------------------------------------------------

	@property
	def online_mappers ( self ):
		"""
		Online mappers applied to the dataset (`list` of `pykeras.inputs.OnlineMapper`).

		Online mappers are applied in the order of their definition.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		
		return self._online_mappers
	
	# def online_mappers ( self )
	
	# --------------------------------------------------

	@property
	def inputs_features_names ( self ):
		"""
		Names of features considered as inputs (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.inputs_features_names
	
	# def inputs_features_names ( self )
	
	# --------------------------------------------------

	@property
	def labels_features_names ( self ):
		"""
		Names of features considered as labels (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.labels_features_names
	
	# def labels_features_names ( self )
		
	# --------------------------------------------------

	@property
	def sample_weight_features_names ( self ):
		"""
		Names of features considered as labels (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.sample_weight_features_names
	
	# def sample_weight_features_names ( self )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of features (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.inputs_features_names + self.labels_features_names
	
	# def features_names ( self )

	# --------------------------------------------------

	@property
	def batch_size ( self ):
		"""
		Number of examples included in each batch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.batch_size
	
	# def batch_size ( self )
	
	# --------------------------------------------------

	@property
	def shuffle ( self ):
		"""
		Should examples be shuffled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.shuffle
	
	# def shuffle ( self )
	
	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.data.name
	
	# def name ( self )
		
	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		raise NotImplementedError()
	
	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def steps_per_epoch ( self ):
		"""
		Number of steps per epoch (`int`).

		This is actually the number of batch required to complete one epoch.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		# Number of complete steps
		steps_per_epoch = self.number_of_examples // self.batch_size

		# Is there a batch reminder ?
		reminder = self.number_of_examples % self.batch_size
		if reminder:
			steps_per_epoch += 1

		# Result
		return steps_per_epoch

	# def steps_per_epoch ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _load_online_mappers ( self ):
		"""
		Re-instantiates the online-mappers using their settings found in this dataset data.

		Arguments:
			self (`pykeras.inputs.Dataset`): Dataset of which to reinstantiate the online mappers.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		
		raise NotImplementedError()

	# def _load_online_mappers ( self )
	
	# --------------------------------------------------

	def group_features ( self, features_batch ):
		"""
		Groups the given batch of features into inputs and labels.

		Arguments:
			self           (`pykeras.inputs.Dataset`): Dataset to map.
			features_batch (`dict` of `str` to `any`): Batch of fetures comming from this dataset.

		Returns:
			inputs_batch (`dict` of `str` to `any`): Batch of inputs comming from this dataset.
			labels_batch (`dict` of `str` to `any`): Batch of labels comming from this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is( features_batch, dict )
		
		# Create batches
		inputs_batch = {}
		labels_batch = {}
		sample_weight_batch = {}

		# Group each feature based on its name
		for feature_name in features_batch.keys():

			# Get the feature batch
			feature_batch = features_batch[ feature_name ]
			
			# Is the feature an input
			if feature_name in self.inputs_features_names:
				
				inputs_batch[ feature_name ] = feature_batch

			# Is the feature a label
			elif feature_name in self.labels_features_names:

				labels_batch[ feature_name ] = feature_batch

			# Is the feature a sample weight
			elif feature_name in self.sample_weight_features_names:

				sample_weight_batch[ feature_name ] = feature_batch

			# Error
			else:
				pass
				# raise RuntimeError( 'Found a feature that is not an input and not a label: {}'.format(feature_name) )

		# for feature_name in features_batch.keys()

		del features_batch
		
		if self.sample_weight_features_names:
			return inputs_batch, labels_batch, sample_weight_batch
		else:
			return inputs_batch, labels_batch
		

	# def group_features ( self, features_batch )

	# --------------------------------------------------

	def map ( self, mapper_or_mapper_type, **kwargs ):
		"""
		Adds the given mapper to the list of mappers applied to this dataset.
	
		Mappers are applied only when batch are accessed, see `map_batch`.

		If a mapper type is passed instead of a mapper instance,
		a new instance of that mapper type is intantiate
		using any named arguments given to this method after the mapper type.

		Arguments:
			self                        (`pykeras.inputs.Dataset`): Dataset to map.
			mapper_or_mapper_type (`type`/`pykeras.inputs.Mapper`): Mapper to instantiate/add.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( mapper_or_mapper_type, (type, OnlineMapper) )

		if isinstance( mapper_or_mapper_type, type ):
			mapper = mapper_or_mapper_type.create( **kwargs )
		else:
			mapper = mapper_or_mapper_type

		self._online_mappers.append( mapper )
		self._data._online_mappers_settings.append( mapper.settings )

	# def map ( self, mapper_or_mapper_type, **kwargs )
	
	# --------------------------------------------------

	def map_batch ( self, inputs_batch, labels_batch, sample_weight_batch=None ):
		"""
		Applies the online mappers of this dataset to the given batched inputs and labels.
	
		Online mappers are applied in the order of their definition.

		Arguments:
			self         (`pykeras.inputs.Dataset`): Dataset of which to map a batch.
			inputs_batch (`dict` of `str` to `any`): Batch of inputs comming from this dataset.
			labels_batch (`dict` of `str` to `any`): Batch of labels comming from this dataset.

		Returns:		
			inputs_batch (`dict` of `str` to `any`): Mapped batch of inputs comming from this dataset.
			labels_batch (`dict` of `str` to `any`): Mapped batch of labels comming from this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )
		
		for online_mapper in self.online_mappers:
			
			inputs_batch, labels_batch, sample_weight_batch = online_mapper.map( inputs_batch, labels_batch, sample_weight_batch )
			
			if not isinstance(inputs_batch, dict):
				raise RuntimeError( 'Online mapper \'{}\' did not return a dictionary (`dict`) for inputs, it instead returned a `{}`'.format(
					online_mapper.settings.name,
					type(inputs_batch)
					))
			
			if not isinstance(labels_batch, dict):
				raise RuntimeError( 'Online mapper \'{}\' did not return a dictionary (`dict`) for labels, it instead returned a `{}`'.format(
					online_mapper.settings.name,
					type(labels_batch)
					))

		# for online_mapper in self.online_mappers
			
		# return batch
		if self.sample_weight_features_names:
			return inputs_batch, labels_batch, sample_weight_batch
		else:
			return inputs_batch, labels_batch

	# def map_batch ( self, inputs_batch, labels_batch, sample_weight_batch )

	# --------------------------------------------------

	def save ( self, filepath ):
		"""
		Saves this dataset.

		Arguments:
			self          (`pykeras.inputs.Dataset`): Dataset to save.
			filepath (`str`/`pytools.path.FilePath`): Path of the file where to save the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, PyDataset )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )

		filepath.serialize_json( self._data )

	# def save ( self, filepath )

	# --------------------------------------------------

	def summary ( self, logger ):
		"""
		Prints a short description of this dataset.

		Arguments:
			self (`pykeras.inputs.Dataset`): Dataset to summarize.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( logger, pytools.tasks.Logger )

		logger.log( 'Dataset "{}":', self.name )
		logger.log( '    number_of_examples: {}', self.number_of_examples )
		logger.log( '    batch_size        : {}', self.batch_size )
		logger.log( '    steps_per_epoch   : {}', self.steps_per_epoch )
		logger.log( '    shuffle           : {}', self.shuffle )
		logger.log( '    inputs            : {}', self.inputs_features_names )
		logger.log( '    labels            : {}', self.labels_features_names )
		logger.log( '    sample_weight     : {}', self.sample_weight_features_names )
		logger.log( '    mappers           : {}', [mapper.settings.name for mapper in self.online_mappers] )
		logger.log( '' )
		logger.end_line()
		logger.flush()

	# def summary ( self, logger )

	# --------------------------------------------------

	def use ( self, logger ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self (`pykeras.inputs.Dataset`): Dataset to use.

		Returns:
			`object`: A type accepted by `tensorflow.keras.Model.fit()`
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		raise NotImplementedError()

	# def use ( self, logger )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __len__ ( self ):
		"""
		Number of steps to complete one epoch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self.steps_per_epoch

	# def __len__ ( self )

# class Dataset