# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import random
from tensorflow import keras

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.path

# LOCALS
from .dataset               import Dataset
from .sequence_dataset_data import SequenceDatasetData

# ##################################################
# ###           CLASS SEQUENCE-DATASET           ###
# ##################################################

class SequenceDataset ( Dataset, keras.utils.Sequence ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, iterator, data ):
		"""
		Initializes a new instance of the dataset class.

		Arguments:
			self (`pykeras.inputs.SequenceDataset`): Dataset sequence to initialize.
			iterator (`pydataset.dataset.Iterator`): Iterator used to go through examples.
			data     (`pykeras.inputs.DatasetData`): Data describing the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDataset )
		assert pytools.assertions.type_is_instance_of( iterator, pydataset.dataset.Iterator )
		assert pytools.assertions.type_is_instance_of( data, SequenceDatasetData )
		
		# Init keras.utils.Sequence
		super( SequenceDataset, self ).__init__( data )

		# Set fields
		self._iterator = iterator
		self._indexes  = list(range(len(self._iterator)))

		# Maybe Shuffle ?
		self.on_epoch_end()

	# def __init__ ( self, iterator, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return len(self._indexes )
	
	# def number_of_examples ( self )

	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------
	
	def on_epoch_end ( self ):
		"""
		Shuffles this dataset at the end of each epoch if shuffling is enabled
		and notifies online mappers that one epoch ended.

		Arguments:
			self (`pykeras.inputs.SequenceDataset`): Dataset to shuffle.
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDataset )

		# Shuffle examples ?
		if self.shuffle:
			random.shuffle( self._indexes )

		# Notify online mappers that one epoch ended.
		for online_mapper in self.online_mappers:
			online_mapper.on_epoch_end()

	# def on_epoch_end ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def use ( self, logger ):
		"""
		Use this dataset as input of a model.

		Arguments:
			self (`pykeras.inputs.SequenceDataset`): Dataset to use.

		Returns:
			`keras.inputs.Sequence`: This dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return self

	# def use ( self, logger )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __getitem__ ( self, batch_index ):
		"""
		Get the batch with the given index and preprocess it.

		Arguments:
			self (`pykeras.inputs.DatasetSequence`): Dataset sequence of which to get a batch.
			batch_index                     (`int`): Index of the batch.

		Returns:
			inputs_batch (`dict` of `str` to `any`): Batch of inputs comming from this dataset.
			labels_batch (`dict` of `str` to `any`): Batch of labels comming from this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDataset )
		assert pytools.assertions.type_is( batch_index, int )
		
		# Create a batch of features
		features_batch = {}
		for feature_name in self.features_names:
			features_batch[ feature_name ] = []
		
		# Populate a batch of features
		for in_batch_index in range( self.batch_size ):

			# Compute the example index based on the batch index and position in the batch
			example_index = batch_index * self.batch_size + in_batch_index
			
			# Use the indexes look-up table table to find the actual example index
			# Because they can be shuffled
			example_index = self._indexes[ example_index ]
			
			# Load the example
			example = self._iterator.at( example_index )

			# Load the desired features
			features = example.get_features( self.features_names, True )

			# Add them to the batch
			for feature_name in self.features_names:

				# Get the feature value
				feature_value = features[ feature_name ].value

				# Add it to the batch
				features_batch[ feature_name ].append( feature_value )
				
			# for feature_name in self.features_names

		# for in_batch_index in range( self.batch_size )

		# Convert the batch of features to a set of numpy ndarray
		for feature_name in self.features_names:
			features_batch[ feature_name ] = numpy.asarray( features_batch[ feature_name ] )
		
		# Group batch
		inputs_batch, labels_batch = self.group_features( features_batch )
		
		# Map batch
		inputs_batch, labels_batch = self.map_batch( inputs_batch, labels_batch )
			
		# Return batch
		return inputs_batch, labels_batch

	# def __getitem__ ( self, index )

	# --------------------------------------------------

	def __len__ ( self ):
		"""
		Number of batch to go through one epoch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDataset )

		return self.steps_per_epoch

	# def __len__ ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def open ( cls, url_search_pattern, inputs_features_names, labels_features_names, batch_size=32, shuffle=True, name='sequence' ):
		"""
		Creates a new instance of the `pykeras.inputs.SequenceDataset` class.

		Arguments:
			url_search_pattern              (`str`): URL used to search for examples.
			inputs_features_names (`list` of `str`): Names of features considered as inputs.
			labels_features_names (`list` of `str`): Names of features considered as labels.
			batch_size                      (`int`): Number of examples included in each batch.
			shuffle                        (`bool`): Should examples be shuffled.
			name                            (`str`): Name of the dataset.
		"""
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( inputs_features_names, list )
		assert pytools.assertions.list_items_type_is( inputs_features_names, str )
		assert pytools.assertions.type_is( labels_features_names, list )
		assert pytools.assertions.list_items_type_is( labels_features_names, str )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( shuffle, bool )
		assert pytools.assertions.type_is( name, str )

		# Create the example iterator
		iterator = pydataset.dataset.Iterator( url_search_pattern )

		# Create the dataset sequence data
		data = SequenceDatasetData(
			   url_search_pattern = url_search_pattern,
			inputs_features_names = inputs_features_names,
			labels_features_names = labels_features_names,
			           batch_size = batch_size,
			              shuffle = shuffle,
			                 name = name
			)

		# Create the dataset sequence
		return cls( iterator, data )

	# def open ( cls, url_search_pattern, inputs_features_names, labels_features_names, batch_size, shuffle, name )

	# --------------------------------------------------

	@classmethod
	def load ( cls, filepath_or_data ):
		assert pytools.assertions.type_is_instance_of( filepath_or_data, (str, pytools.path.FilePath, PyDatasetData) )
		
		# Read the data if necessary
		if isinstance( filepath_or_data, PyDatasetData ):
			data = filepath_or_data
		elif isinstance( filepath_or_data, pytools.path.FilePath ):
			data = filepath_or_data.deserialize_json()
		else:
			data = pytools.path.FilePath( filepath_or_data ).deserialize_json()

		# Create the example iterator
		iterator = pydataset.dataset.Iterator( data.url_search_pattern )

		# Create the dataset sequence
		return cls( iterator, data )

	# def load ( cls, filepath_or_data )

# class PyDataset ( tensorflow.keras.utils.Sequence )