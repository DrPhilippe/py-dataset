# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .dataset_data import DatasetData

# ##################################################
# ###        CLASS SEQUENCE-DATASET-DATA         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SequenceDatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'url_search_pattern'
		]
	)
class SequenceDatasetData ( DatasetData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, url_search_pattern='', **kwargs ):
		"""
		Initialize a new instance of the `pykeras.inputs.SequenceDatasetData` class.

		Arguments:
			self (`pykeras.inputs.SequenceDatasetData`): Instance to initialize.
			url_search_pattern                  (`str`): URL used to search for examples.

		Named Arguments:
			inputs_features_names                                   (`list` of `str`): Names of features considered as inputs.
			labels_features_names                                   (`list` of `str`): Names of features considered as labels.
			batch_size                                                        (`int`): Number of examples included in each batch.
			shuffle                                                          (`bool`): Should examples be shuffled.
			online_mappers_settings (`list` of `pykeras.inputs.OnlineMapperSettings`): Settings of the online mappers applied to this dataset.
			name                                                              (`str`): Name of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDatasetData )
		assert pytools.assertions.type_is( url_search_pattern, str )

		super( SequenceDatasetData, self ).__init__( **kwargs )

		self._url_search_pattern = url_search_pattern

	# def __init__ ( self, url_search_pattern, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL search pattern used to find examples (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SequenceDatasetData )

		return self._url_search_pattern
	
	# def url_search_pattern ( self )

	# --------------------------------------------------

	@url_search_pattern.setter
	def url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SequenceDatasetData )
		assert pytools.assertions.type_is( value, str )

		self._url_search_pattern = value
	
	# def url_search_pattern ( self )

# class SequenceDatasetData ( DatasetData )