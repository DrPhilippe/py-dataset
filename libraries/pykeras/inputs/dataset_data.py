# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###             CLASS DATASET-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetData',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'inputs_features_names',
		'labels_features_names',
		'sample_weight_features_names',
		'batch_size',
		'shuffle',
		'online_mappers_settings',
		'name'
		]
	)
class DatasetData ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, inputs_features_names=[], labels_features_names=[], sample_weight_features_names=[], batch_size=32, shuffle=True, online_mappers_settings=[], name='dataset' ):
		"""
		Initialize a new instance of the `pykeras.inputs.DatasetData` class.

		Arguments:
			self                                       (`pykeras.inputs.DatasetData`): Instance to initialize.
			inputs_features_names                                   (`list` of `str`): Names of features considered as inputs.
			labels_features_names                                   (`list` of `str`): Names of features considered as labels.
			sample_weight_features_names                            (`list` of `str`): Names of features considered as sample weight.
			batch_size                                                        (`int`): Number of examples included in each batch.
			shuffle                                                          (`bool`): Should examples be shuffled.
			online_mappers_settings (`list` of `pykeras.inputs.OnlineMapperSettings`): Settings of the online mappers applied to this dataset.
			name                                                              (`str`): Name of the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( inputs_features_names, list )
		assert pytools.assertions.list_items_type_is( inputs_features_names, str )
		assert pytools.assertions.type_is( labels_features_names, list )
		assert pytools.assertions.list_items_type_is( labels_features_names, str )
		assert pytools.assertions.type_is( sample_weight_features_names, list )
		assert pytools.assertions.list_items_type_is( sample_weight_features_names, str )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( shuffle, bool )
		assert pytools.assertions.type_is( online_mappers_settings, list )
		assert pytools.assertions.list_items_type_is( online_mappers_settings, OnlineMapperSettings )
		assert pytools.assertions.type_is( name, str )

		self._inputs_features_names        = copy.deepcopy( inputs_features_names )
		self._labels_features_names        = copy.deepcopy( labels_features_names )
		self._sample_weight_features_names = copy.deepcopy( sample_weight_features_names )
		self._batch_size                   = batch_size
		self._shuffle                      = shuffle
		self._online_mappers_settings      = copy.deepcopy( online_mappers_settings )
		self._name                         = name

	# def __init__ ( self, inputs_features_names, labels_features_names, sample_weight_features_names, batch_size, shuffle, online_mappers_settings, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def inputs_features_names ( self ):
		"""
		Names of features considered as inputs (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._inputs_features_names
	
	# def inputs_features_names ( self )
	
	# --------------------------------------------------

	@inputs_features_names.setter
	def inputs_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._inputs_features_names = value
	
	# def inputs_features_names ( self, value )

	# --------------------------------------------------

	@property
	def labels_features_names ( self ):
		"""
		Names of features considered as labels (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._labels_features_names
	
	# def labels_features_names ( self )
	
	# --------------------------------------------------

	@labels_features_names.setter
	def labels_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._labels_features_names = value
	
	# def labels_features_names ( self, value )

	# --------------------------------------------------

	@property
	def sample_weight_features_names ( self ):
		"""
		Names of features considered as sample weight (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._sample_weight_features_names
	
	# def sample_weight_features_names ( self )
	
	# --------------------------------------------------

	@sample_weight_features_names.setter
	def sample_weight_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._sample_weight_features_names = value
	
	# def sample_weight_features_names ( self, value )
	
	# --------------------------------------------------

	@property
	def batch_size ( self ):
		"""
		Number of examples included in each batch (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._batch_size
	
	# def batch_size ( self )

	# --------------------------------------------------

	@batch_size.setter
	def batch_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		
		self._batch_size = value
	
	# def batch_size ( self, value )

	# --------------------------------------------------

	@property
	def shuffle ( self ):
		"""
		Should examples be shuffled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._shuffle
	
	# def shuffle ( self )

	# --------------------------------------------------

	@shuffle.setter
	def shuffle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, bool )

		self._shuffle = value
	
	# def shuffle ( self, value )

	# --------------------------------------------------

	@property
	def online_mappers_settings ( self ):
		"""
		Settings of the online mappers applied to this dataset (`list` of `pykeras.inputs.OnlineMapperSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._online_mappers_settings
	
	# def online_mappers_settings ( self )

	# --------------------------------------------------

	@online_mappers_settings.setter
	def online_mappers_settings ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, OnlineMapperSettings )

		self._online_mappers_settings = value
	
	# def online_mappers_settings ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this dataset sequence (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return self._name
	
	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

# class DatasetData ( pytools.serialization.Serializable )