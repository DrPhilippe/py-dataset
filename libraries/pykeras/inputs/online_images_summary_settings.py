# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###    CLASS ONLINE-IMAGES-SUMMARY-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlineImagesSummarySettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'log_directory_path',
		'inputs_images_names',
		'labels_images_names',
		'max_outputs'
		]
	)
class OnlineImagesSummarySettings ( OnlineMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, log_directory_path=pytools.path.DirectoryPath(), inputs_images_names=[], labels_images_names=[], max_outputs=8, name='online-image-summary'	):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )
		assert pytools.assertions.type_is_instance_of( log_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is( inputs_images_names, list )
		assert pytools.assertions.list_items_type_is( inputs_images_names, str )
		assert pytools.assertions.type_is( labels_images_names, list )
		assert pytools.assertions.list_items_type_is( labels_images_names, str )
		assert pytools.assertions.type_is( max_outputs, int )
		assert pytools.assertions.type_is( name, str )

		super( OnlineImagesSummarySettings, self ).__init__( name )

		self._log_directory_path  = pytools.path.DirectoryPath(log_directory_path) if isinstance(log_directory_path, str) else log_directory_path
		self._inputs_images_names = inputs_images_names
		self._labels_images_names = labels_images_names
		self._max_outputs         = max_outputs

	# def __init__ ( self, log_directory_path, inputs_images_names, labels_images_names, max_outputs, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def log_directory_path ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )

		return self._log_directory_path

	# def log_directory_path ( self )
	
	# --------------------------------------------------
	
	@log_directory_path.setter
	def log_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )
		assert pytools.assertions.type_is_instance_of( value, (pytools.path.DirectoryPath, str) )

		self._log_directory_path = pytools.path.DirectoryPath(value) if isinstance(value, str) else value

	# def log_directory_path ( self, value )

	# --------------------------------------------------
	
	@property
	def inputs_images_names ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )

		return self._inputs_images_names

	# def inputs_images_names ( self )

	# --------------------------------------------------
	
	@inputs_images_names.setter
	def inputs_images_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._inputs_images_names = value

	# def inputs_images_names ( self, value )

	# --------------------------------------------------
	
	@property
	def labels_images_names ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )

		return self._labels_images_names

	# def labels_images_names ( self )

	# --------------------------------------------------
	
	@labels_images_names.setter
	def labels_images_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._labels_images_names = value

	# def labels_images_names ( self, value )

	# --------------------------------------------------
	
	@property
	def max_outputs ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )

		return self._max_outputs

	# def max_outputs ( self )

	# --------------------------------------------------
	
	@max_outputs.setter
	def max_outputs ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummarySettings )
		assert pytools.assertions.type_is( value, int )

		self._max_outputs = value

	# def max_outputs ( self, value )

# class OnlineImagesSummarySettings ( OnlineMapperSettings )