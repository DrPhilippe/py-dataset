# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .online_mapper_settings import OnlineMapperSettings

# ##################################################
# ###    CLASS ONLINE-POINTS-MASKER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlinePointsMaskerSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'points_feature_name'
		'points_weights_feature_name'
		]
	)
class OnlinePointsMaskerSettings ( OnlineMapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, points_feature_name='', points_weights_feature_name='', name='online-points-masker' ):
		"""
		Masks points.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMaskerSettings )
		assert pytools.assertions.type_is( points_feature_name, str )
		assert pytools.assertions.type_is( points_weights_feature_name, str )

		super( OnlinePointsMaskerSettings, self ).__init__( name )

		self._points_feature_name         = points_feature_name
		self._points_weights_feature_name = points_weights_feature_name

	# def __init__ ( self, points_feature_name, points_weights_feature_name, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def points_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMaskerSettings )

		return self._points_feature_name
	
	# def points_feature_name ( self )

	# --------------------------------------------------

	@points_feature_name.setter
	def points_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMaskerSettings )
		assert pytools.assertions.type_is( value, str )

		self._points_feature_name = value
	
	# def points_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def points_weights_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMaskerSettings )

		return self._points_weights_feature_name
	
	# def points_weights_feature_name ( self )

	# --------------------------------------------------

	@points_weights_feature_name.setter
	def points_weights_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMaskerSettings )
		assert pytools.assertions.type_is( value, str )

		self._points_weights_feature_name = value
	
	# def points_weights_feature_name ( self, value )

# class OnlinePointsMaskerSettings ( OnlineMapperSettings )