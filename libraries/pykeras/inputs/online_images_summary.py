# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
from .online_images_summary_settings import OnlineImagesSummarySettings
from .online_mapper                  import OnlineMapper

# ##################################################
# ###        CLASS ONLINE-IMAGES-SUMMARY         ###
# ##################################################

class OnlineImagesSummary ( OnlineMapper ):
	"""
	Summarize images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummary )
		assert pytools.assertions.type_is_instance_of( settings, OnlineImagesSummarySettings )

		super( OnlineImagesSummary, self ).__init__( settings )

		self._file_writer = None
		self._step = 0

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummary )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )

		if self._file_writer is None:
			logdir            = self.settings.log_directory_path + pytools.path.DirectoryPath( 'images' )
			self._file_writer = tensorflow.summary.create_file_writer( str(logdir) )

		with self._file_writer.as_default():
			inputs_batch = self.summarize_images( inputs_batch, self.settings.inputs_images_names )
			labels_batch = self.summarize_images( labels_batch, self.settings.labels_images_names )

		self._step += 1

		return inputs_batch, labels_batch

	# def map ( self, inputs_batch, labels_batch )

	# --------------------------------------------------

	def summarize_images ( self, batch, images_names ):
		assert pytools.assertions.type_is_instance_of( self, OnlineImagesSummary )
		assert pytools.assertions.type_is( batch, dict )
		assert pytools.assertions.type_is( images_names, list )
		assert pytools.assertions.list_items_type_is( images_names, str )
		
		for image_name in images_names:
			
			image = batch[ image_name ]
			shape = list(image.shape)
			rank = len(shape)
			if ( rank == 4 ):
				width  = shape[1]
				height = shape[2]
				depth  = shape[3]
			elif ( rank == 3 ):
				width  = shape[1]
				height = shape[2]
				depth  = 1
			elif ( rank == 2 ):
				width  = shape[0]
				height = shape[1]
				depth  = 1

			image = tensorflow.reshape( image, [-1, width, height, depth], name='resize-image-summary' )
			tensorflow.summary.image( image_name, image, step=self._step, max_outputs=self.settings.max_outputs )

		# for image_name in images_names

		return batch

	# def summarize_images ( self, batch, images_names )
	
	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineMapper )
		
		self._step = 0

	# def on_epoch_end ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return OnlineImagesSummarySettings( **kwargs )

	# def create_settings ( cls, **kwargs )


# class OnlineImagesSummary ( OnlineMapper )