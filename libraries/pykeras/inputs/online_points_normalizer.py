# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow

# INTERNALS
import pydataset.pose_utils
import pykeras.pose_utils
import pytools.assertions

# LOCALS
from .online_mapper import OnlineMapper
from .online_points_normalizer_settings import OnlinePointsNormalizerSettings

# ##################################################
# ###      CLASS ONLINE-POINTS-NORMALIZER        ###
# ##################################################

class OnlinePointsNormalizer ( OnlineMapper ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.OnlinePointsNormalizer` class.

		Arguments:
			self             (`pykeras.inputs.OnlinePointsNormalizer`): Instance to initialize.
			settings (`pykeras.inputs.OnlinePointsNormalizerSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizer )
		assert pytools.assertions.type_is_instance_of( settings, OnlinePointsNormalizerSettings )

		super( OnlinePointsNormalizer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch, sample_weight_batch=None ):
		"""
		Normalizes the control points in the given batch of labels using the image found in the inputs.
		
		Arguments:
			self (`pykeras.inputs.OnlinePointsNormalizer`): Instance to initialize.
			inputs_batch        (`dict` of `str` to `any`): Batch of inputs.
			labels_batch        (`dict` of `str` to `any`): Batch of labels.
			sample_weight_batch (`dict` of `str` to `any`): Batch of sample weight.
		
		Returns:
			inputs_batch        (`dict` of `str` to `any`): Mapped batch of inputs.
			labels_batch        (`dict` of `str` to `any`): Mapped batch of labels.
			sample_weight_batch (`dict` of `str` to `any`): Mapped batch of sample weight.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsNormalizer )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )

		# Get the image, points, and points weights features
		image_batch   = inputs_batch[ self.settings.image_feature_name  ] # [batch_size, width, height, channels]
		points_batch  = labels_batch[ self.settings.points_feature_name ] # [batch_size, 11, 8, 2]

		# Numpy data ?
		if isinstance( points_batch, numpy.ndarray ):
			
			# Get the image(s) width and height
			image_width  = float( image_batch.shape[ 1 ] )
			image_height = float( image_batch.shape[ 2 ] )

			# Normalize points
			points_batch = pydataset.pose_utils.normalize_control_points( points_batch, image_width, image_height )

		# TensorFlow data ?
		else:
			# Get the image(s) width and height
			image_width  = tensorflow.shape( image_batch )[ 1 ]
			image_height = tensorflow.shape( image_batch )[ 2 ]

			# Normalize points
			points_batch = pykeras.pose_utils.normalize_control_points( points_batch, image_width, image_height )

		# Set points
		labels_batch[ self.settings.points_feature_name ] = points_batch

		# Return mapped batch
		return inputs_batch, labels_batch, sample_weight_batch

	# def map ( self, inputs_batch, labels_batch, sample_weight_batch )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the online mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of online mapper for which to create settings.

		Named Arguments:
			Forwarded to the online mapper's settings constructor.

		Returns:
			`pykeras.inputs.OnlineMapperSettings`: The new online mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return OnlinePointsNormalizerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class OnlinePointsNormalizer( OnlineMapper )