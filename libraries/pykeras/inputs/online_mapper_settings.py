# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###        CLASS ONLINE-MAPPER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OnlineMapperSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'name'
		]
	)
class OnlineMapperSettings ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='mapper' ):
		assert pytools.assertions.type_is_instance_of( self, OnlineMapperSettings )
		assert pytools.assertions.type_is( name, str )

		self._name = name

	# def __init__ ( self, name )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self ):
		assert pytools.assertions.type_is_instance_of( self, OnlineMapperSettings )

		return self._name
	
	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OnlineMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

# class OnlineMapperSettings ( pytools.serialization.Serializable )