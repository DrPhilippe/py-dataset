# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow

# INTERNALS
import pydataset.pose_utils
import pykeras.pose_utils
import pytools.assertions

# LOCALS
from .online_mapper import OnlineMapper
from .online_points_masker_settings import OnlinePointsMaskerSettings

# ##################################################
# ###         CLASS ONLINE-POINTS-MASKER         ###
# ##################################################

class OnlinePointsMasker ( OnlineMapper ):


	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pykeras.inputs.OnlinePointsMasker` class.

		Arguments:
			self             (`pykeras.inputs.OnlinePointsMasker`): Instance to initialize.
			settings (`pykeras.inputs.OnlinePointsMaskerSettings`): Settings of the online mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMasker )
		assert pytools.assertions.type_is_instance_of( settings, OnlinePointsMaskerSettings )

		super( OnlinePointsMasker, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, inputs_batch, labels_batch, sample_weight_batch ):
		"""
		Maps the given batch of inputs and labels features.

		Arguments:
			self     (`pykeras.inputs.OnlinePointsMasker`): Instance to initialize.
			inputs_batch        (`dict` of `str` to `any`): Batch of inputs to map.
			labels_batch        (`dict` of `str` to `any`): Batch of labels to map.
			sample_weight_batch (`dict` of `str` to `any`): batch of sample weight.
		Returns:
			inputs_batch        (`dict` of `str` to `any`): Mapped batch of inputs.
			labels_batch        (`dict` of `str` to `any`): Mapped batch of labels.
			sample_weight_batch (`dict` of `str` to `any`): Mapped batch of sample weight.
		"""
		assert pytools.assertions.type_is_instance_of( self, OnlinePointsMasker )
		assert pytools.assertions.type_is( inputs_batch, dict )
		assert pytools.assertions.type_is( labels_batch, dict )

		# Get the points and their weights
		points_batch         =        labels_batch[ self.settings.points_feature_name         ]
		points_weights_batch = sample_weight_batch[ self.settings.points_weights_feature_name ]

		# Numpy
		if isinstance( points_batch, numpy.ndarray ):
			points_batch = numpy.nan_to_num( points_batch, nan=0. )
			points_batch = numpy.multiply( points_batch, points_weights_batch )
		
		# Tensorflow
		else:
			points_batch = tensorflow.where(
				tensorflow.math.is_nan( points_batch ),
				tensorflow.zeros_like( points_batch ),
				points_batch
				)
			points_batch = tensorflow.math.multiply( points_batch, points_weights_batch )

		# Set the points
		labels_batch[ self.settings.points_feature_name ] = points_batch

		# Return batch
		return inputs_batch, labels_batch, sample_weight_batch

	# def map ( self, inputs_batch, labels_batch, sample_weight_batch )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the online mapper of type `cls`.
		
		Arguments:
			cls (`type`): Type of online mapper for which to create settings.

		Named Arguments:
			Forwarded to the online mapper's settings constructor.

		Returns:
			`pykeras.inputs.OnlinePointsMaskerSettings`: The new online mapper settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )

		return OnlinePointsMaskerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class OnlinePointsMasker( OnlineMapper )