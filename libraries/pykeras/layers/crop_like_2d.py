# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from   tensorflow import keras

# INTERNALS
import pytools.assertions

# ##################################################
# ###                CLASS RENAME                ###
# ##################################################

class CropLike2D ( keras.layers.Layer ):
	"""
	Crops a tensor to the same size has an other.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, CropLike2D )

		super( CropLike2D, self ).__init__( **kwargs )

	# def __init__( self, start, end, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def call( self, x0, x1 ):
		assert pytools.assertions.type_is_instance_of( self, CropLike2D )

		# #
		# x0 = inputs[ 0 ]
		# x1 = inputs[ 1 ]

		# Shape of tensor to crop
		x0_height   = x0.shape[ 1 ]
		x0_width    = x0.shape[ 2 ]
		x0_channels = x0.shape[ 3 ]

		# Crop size
		x1_height = x1.shape[ 1 ]
		x1_width  = x1.shape[ 2 ]

		# offsets for the top left corner of the crop
		offsets = [0, (x0_height - x1_height) // 2, (x0_width - x1_width) // 2, 0]

		# Size of the crop
		size = [-1, x1_height, x1_width, x0_channels]

		# Crop
		return tensorflow.slice( x0, offsets, size )

	# def call( self, inputs )
	
# class CropLike2D ( Layer )
