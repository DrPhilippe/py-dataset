# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pytools.assertions

# ##################################################
# ###                CLASS RENAME                ###
# ##################################################

class Slice ( keras.layers.Layer ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, start, end, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, Slice )
		assert pytools.assertions.type_is( start, int )
		assert pytools.assertions.type_is( end, int )

		super( Slice, self ).__init__( **kwargs )

		self.start = start
		self.end   = end

	# def __init__( self, start, end, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def call( self, x ):

		return x[ :, self.start:self.end ]

	# def call( self, x )
	
	# --------------------------------------------------

	def get_config( self ):
		"""
		Returns the config of the layer.
		"""
		
		config = super( Slice, self ).get_config()
		config.update({
			'start': self.start,
			'end': self.end
		})
		return config
	
	# def get_config( self )

# class Slice ( Layer )