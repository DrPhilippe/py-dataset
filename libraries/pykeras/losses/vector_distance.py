# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCAS
from .distance import Distance

# ##################################################
# ###           CLASS VECTOR-DISTANCE            ###
# ##################################################

class VectorDistance ( Distance ):
	"""
	The vector distance loss computes the sum of distances between vectors.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.VectorDistance` loss.

		Arguments:
			self (`pykeras.losses.VectorDistance`): Instance to initialize.

		Named Arguments:
			see `pykeras.losses.Distance` and `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, VectorDistance )

		super( VectorDistance, self ).__init__( axis=-1, **kwargs )

	# def __init__ ( self, **kwargs )

# class VectorDistance ( Distance )