# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import tensorflow
from   tensorflow import keras

# INTERNALS
import pytools.assertions

# LOCALS
import pykeras

# ##################################################
# ###               CLASS YOLO-V1                ###
# ##################################################

class YoloLoss ( keras.losses.Loss ):
	"""
	The YOLO V1 loss computes the weighted sum of square error.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		image_size=(448., 448.),
		number_of_cells=7,
		number_of_detectors_per_cell=2,
		number_of_classes=20,
		xy_loss='mse',
		wh_loss='mse',
		c_obj_loss='mse',
		c_noobj_loss='mse',
		classes_loss='mse',
		**kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.yolo_v1.YoloLoss` class.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance to initialize.
			image_size     (`tuple` of `2` `float`s): Image size in pixels.
			number_of_cells                  (`int`): Number of cells on the width and height of the image (aka. S).
			number_of_detectors_per_cell     (`int`): Number of bounding box detector per cell (aka. B).
			number_of_classes                (`int`): Number of classes to separate.
			xy_loss               (`str`/`function`): Loss function applied to x and y coordinates.
			wh_loss               (`str`/`function`): Loss function applied to width and height coordinates.
			c_obj_loss            (`str`/`function`): Loss function applied to the confidence of detectors in change of an object.
			c_noobj_loss          (`str`/`function`): Loss function applied to the confidence of detectors not in charge of any objects
			classes_loss          (`str`/`function`): Loss function applied to classification logits of cells containing an object.
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( image_size, tuple )
		assert pytools.assertions.equal( len(image_size), 2 )
		assert pytools.assertions.tuple_items_type_is( image_size, float )
		assert pytools.assertions.type_is( number_of_cells, int )
		assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
		assert pytools.assertions.type_is( number_of_classes, int )

		super( YoloLoss, self ).__init__( **kwargs )

		# Usefull values
		self._image_size                   = copy.deepcopy( image_size )
		self._number_of_cells              = number_of_cells
		self._number_of_detectors_per_cell = number_of_detectors_per_cell
		self._number_of_classes            = number_of_classes
		# Loss functions
		self._xy_loss      = tensorflow.keras.losses.deserialize( xy_loss      ) if isinstance( xy_loss,      str ) else xy_loss
		self._wh_loss      = tensorflow.keras.losses.deserialize( wh_loss      ) if isinstance( wh_loss,      str ) else wh_loss
		self._c_obj_loss   = tensorflow.keras.losses.deserialize( c_obj_loss   ) if isinstance( c_obj_loss,   str ) else c_obj_loss
		self._c_noobj_loss = tensorflow.keras.losses.deserialize( c_noobj_loss ) if isinstance( c_noobj_loss, str ) else c_noobj_loss
		self._classes_loss = tensorflow.keras.losses.deserialize( classes_loss ) if isinstance( classes_loss, str ) else classes_loss

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_size ( self ):
		"""
		Image size (`tuple` of `2` `floats`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )

		return self._image_size

	# def image_size ( self )

	# --------------------------------------------------
	
	@image_size.setter
	def image_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, tuple )
		assert pytools.assertions.equal( len(value), 2 )
		assert pytools.assertions.tuple_items_type_is( value, float )

		self._image_size = copy.deepcopy( value )

	# def image_size ( self, value )

	# --------------------------------------------------

	@property
	def image_width ( self ):
		"""
		Width of the images (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )

		return self._image_size[ 0 ]
	
	# def image_width( self )

	# --------------------------------------------------

	@image_width.setter
	def image_width ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, float )

		self._image_size[ 0 ] = value
	
	# def image_width( self, value )

	# --------------------------------------------------

	@property
	def image_height ( self ):
		"""
		Height of the images (`float`).
		"""
		return self._image_size[ 1 ]

	# def image_height( self )

	# --------------------------------------------------

	@image_height.setter
	def image_height ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, float )

		self._image_size[ 1 ] = value
	
	# def image_height( self, value )

	# --------------------------------------------------

	@property
	def number_of_cells ( self ):
		"""
		Number of cells. AKA S (`int`).
		"""
		return self._number_of_cells

	# def number_of_cells( self )

	# --------------------------------------------------

	@number_of_cells.setter
	def number_of_cells ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, int )

		self._number_of_cells = value
	
	# def number_of_cells ( self, value )

	# --------------------------------------------------

	@property
	def number_of_detectors_per_cell ( self ):
		"""
		Number of detectors per cells. AKA B (`int`).
		"""
		return self._number_of_detectors_per_cell

	# def number_of_detectors_per_cell( self )

	# --------------------------------------------------

	@number_of_detectors_per_cell.setter
	def number_of_detectors_per_cell ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, int )

		self._number_of_detectors_per_cell = value
	
	# def number_of_detectors_per_cell ( self, value )

	# --------------------------------------------------

	@property
	def number_of_classes ( self ):
		"""
		Number of classes. AKA C (`int`).
		"""
		return self._number_of_classes

	# def number_of_classes( self )

	# --------------------------------------------------

	@number_of_classes.setter
	def number_of_classes ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		assert pytools.assertions.type_is( value, int )

		self._number_of_classes = value
	
	# def number_of_classes ( self, value )

	# --------------------------------------------------

	@property
	def cell_width ( self ):
		"""
		Width of the cells (`float`).
		"""
		return self.image_width / self.number_of_cells
	
	# def cell_width ( self )

	# --------------------------------------------------

	@property
	def cell_height ( self ):
		"""
		Height of the cells (`float`).
		"""
		return self.image_height / self.number_of_cells

	# def cell_height ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def call ( self, y_true, y_pred ):
		"""
		Compute the the YOLO width and height loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.WHLoss`): Loss to compute.
			y_true        (`keras.Tensor`): Batch of labels [BATCH, S, S, 5+C].
			y_pred        (`keras.Tensor`): Batch of predictions [BATCH, S, S, 5*B+C].
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		
		# Usefull values
		BATCH = tensorflow.shape( y_true )[ 0 ]
		S     = self.number_of_cells
		B     = self.number_of_detectors_per_cell
		C     = self.number_of_classes
		X, Y, W, H, CONF = 0, 1, 2, 3, 4

		# Check shape
		assert tensorflow.TensorSpec( [None, S, S, 5   + C], tensorflow.dtypes.float32 ).is_compatible_with( y_true )
		assert tensorflow.TensorSpec( [None, S, S, 5*B + C], tensorflow.dtypes.float32 ).is_compatible_with( y_pred )

		# Do most of the work
		true_obj_rects, pred_obj_rects, true_noobj_rects, pred_noobj_rects, true_logits, pred_logits = self.prepare_data( y_true, y_pred )

		# Compute loss overt detectors
		xy_loss      = self._xy_loss( true_obj_rects[:, :, 0:2], pred_obj_rects[:, :, 0:2] ) # [BATCH, S*S*B]
		wh_loss      = self._wh_loss( true_obj_rects[:, :, 2:4], pred_obj_rects[:, :, 2:4] ) # [BATCH, S*S*B]
		c_obj_loss   = self._c_obj_loss( true_obj_rects[:, :, 4], pred_obj_rects[:, :, 4] ) # [BATCH, S*S*B]
		c_noobj_loss = self._c_noobj_loss( true_noobj_rects[:, :, 4], pred_noobj_rects[:, :, 4] ) # [BATCH, S*S*B]
		classes_loss = self._classes_loss( true_logits, pred_logits ) # [BATCH, S*S]

		# Sum over cells and detectors
		xy_loss      = tensorflow.math.reduce_sum( xy_loss,      axis=-1 ) # [BATCH]
		wh_loss      = tensorflow.math.reduce_sum( wh_loss,      axis=-1 ) # [BATCH]
		c_obj_loss   = tensorflow.math.reduce_sum( c_obj_loss,   axis=-1 ) # [BATCH]
		c_noobj_loss = tensorflow.math.reduce_sum( c_noobj_loss, axis=-1 ) # [BATCH]
		classes_loss = tensorflow.math.reduce_sum( classes_loss, axis=-1 ) # [BATCH]

		# Compound loss
		return 5.0 * xy_loss\
			 + 5.0 * wh_loss\
			 + 1.0 * c_obj_loss\
			 + 0.5 * c_noobj_loss\
			 + 1.0 * classes_loss

	# def call ( self, y_true, y_pred )

	# --------------------------------------------------

	def identify_non_empty_cells ( self, y_true ):
		"""
		Identifies non-empty cells in the labels.

		Non-empty cell have either a non-zero bounding rectangle with confidence 1 or a 1 in the one hot category label part.
		So we can sum over the last dimension (5+C values) and compare the result to 0.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_true             (`tensorflow.Tensor`): Labels with shape [BATCH, S, S, 5+C].

		Returns:
			`tensorflow.Tensor`: Non-empty cell mask with shape [BATCH, S, S] where non-empty cells are `True` and empty cells are `False`.
		"""
		
		# Check shape
		S = self.number_of_cells
		C = self.number_of_classes
		assert tensorflow.TensorSpec( [None, S, S, 5+C], tensorflow.dtypes.float32 ).is_compatible_with( y_true )

		# Identify non empty cells
		return pykeras.yolo_utils.identify_non_empty_cells( y_true )

	# def identify_non_empty_cells ( self, y_true )

	# --------------------------------------------------

	def slice_labels ( self, y_true ):
		"""
		Slice the bounding rectangle and the category probabilities from the labels.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_true             (`tensorflow.Tensor`): Labels with shape [BATCH, S, S, 5+C].

		Returns:
			y_true_rect   (`tensorflow.Tensor`): Label bounding rectangle [BATCH, S, S, 5].
			y_true_logits (`tensorflow.Tensor`): Label class probabilities [BATCH, S, S, C].
		"""

		# Check shape
		S = self.number_of_cells
		C = self.number_of_classes
		assert tensorflow.TensorSpec( [None, S, S, 5+C], tensorflow.dtypes.float32 ).is_compatible_with( y_true )

		# Slice labels
		return pykeras.yolo_utils.slice_labels( y_true )

	# def slice_labels ( self, y_true )

	# --------------------------------------------------

	def slice_predictions ( self, y_pred ):
		"""
		Slice the bounding rectangle and the category probabilities from the labels.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_pred             (`tensorflow.Tensor`): Predictions with shape [BATCH, S, S, 5*B+C].

		Returns:
			y_pred_rects  (`tensorflow.Tensor`): Predicted bounding rectangle [BATCH, S, S, 5*B].
			y_pred_logits (`tensorflow.Tensor`): Predicted class probabilities [BATCH, S, S, C].
		"""
		
		# Check shape
		S = self.number_of_cells
		B = self.number_of_detectors_per_cell
		C = self.number_of_classes
		assert tensorflow.TensorSpec( [None, S, S, 5*B+C], tensorflow.dtypes.float32 ).is_compatible_with( y_pred )

		# Slice predictions
		return pykeras.yolo_utils.slice_predictions( y_pred, self.number_of_detectors_per_cell )

	# def slice_labels ( self, y_true )

	# --------------------------------------------------

	def repeat_labels_rectangles_to_match_predictions ( self, y_true_rect ):
		"""
		Repeats the label rectangle as many times as predictions rectangles.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_true             (`tensorflow.Tensor`): Labels rectangles with shape [BATCH, S, S, 5].

		Returns:
			`tensorflow.Tensor`: Repeated labels rectangles with shape [BATCH, S, S, 5*B].
		"""

		# Check shape
		S = self.number_of_cells
		assert tensorflow.TensorSpec( [None, S, S, 5], tensorflow.dtypes.float32 ).is_compatible_with( y_true_rect )

		# Convert rectangles
		return pykeras.yolo_utils.repeat_labels_rectangles_to_match_predictions( y_true_rect, self.number_of_detectors_per_cell )

	# def repeat_labels_rectangles_to_match_predictions ( self, y_true_rect )

	# --------------------------------------------------

	def predicted_rectangles_to_xyxy ( self, y_pred_rects ):
		"""
		Converts yolo predicted bounding rectangles to bounding rectangles with format x_min, y_min, x_max, y_max.
		
		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_pred_rects       (`tensorflow.Tensor`): Rectangles predicted by yolo [BATCH, S, S, 5*B].

		Returns:
			`tensorflow.Tensor`: Same rectangle in format x_min, y_min, x_max, y_max [BATCH, S, S, 5*B].
		"""
		
		# Check shape
		S = self.number_of_cells
		B = self.number_of_detectors_per_cell
		assert tensorflow.TensorSpec( [None, S, S, 5*B], tensorflow.dtypes.float32 ).is_compatible_with( y_pred_rects )

		# Convert rectangles
		return pykeras.yolo_utils.predicted_rectangles_to_xyxy(
			y_pred_rects,
			self.number_of_cells,
			self.number_of_detectors_per_cell,
			self.image_size
			)

	# def predicted_rectangles_to_xyxy ( self, y_pred_rects )

	# --------------------------------------------------

	def best_rectangles_matches_mask ( self, y_true_rects, y_pred_rects ):
		"""
		Keep only the best match between prediction and labels in a cell, for all cell and all batch.
		Other rectangles are set to zero.
		Shape of labels and predictions remains the same.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_true             (`tensorflow.Tensor`): Labels rectangles, same size as yolo output rectangles [BATCH, S, S, 5*B].
			y_pred             (`tensorflow.Tensor`): Yolo output rectangles [BATCH, S, S, 5*B].
		
		Returns:
			`tensorflow.Tensor`: Mask that multiplies best match rectangles by 1, the rest by 0.
		"""
		
		# Check shape
		S = self.number_of_cells
		B = self.number_of_detectors_per_cell
		assert tensorflow.TensorSpec( [None, S, S, 5*B], tensorflow.dtypes.float32 ).is_compatible_with( y_true_rects )
		assert tensorflow.TensorSpec( [None, S, S, 5*B], tensorflow.dtypes.float32 ).is_compatible_with( y_pred_rects )

		# Find best rectangle match
		return pykeras.yolo_utils.best_rectangles_matches_mask(
			y_true_rects, y_pred_rects,
			self.number_of_cells,
			self.number_of_detectors_per_cell
			)

	# def best_rectangles_matches_mask ( self, y_true, y_pred )

	# --------------------------------------------------

	def xyxy_rectangles_to_yolo ( self, rectangles ):
		"""
		Converts rectangles in the format x_min, y_min, x_max, y_max to yolo target rectangle.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			rectangles         (`tensorflow.Tensor`): Rectangles with shape [BATCH, S, S, 5*B].

		Returns:
			`tensorflow.Tensor`: Converted rectangles with shape [BATCH, S, S, 5*B].
		"""

		# Check shape
		S = self.number_of_cells
		B = self.number_of_detectors_per_cell
		assert tensorflow.TensorSpec( [None, S, S, 5*B], tensorflow.dtypes.float32 ).is_compatible_with( rectangles )

		# Convert rectangles
		return pykeras.yolo_utils.xyxy_rectangles_to_yolo (
			rectangles,
			self.number_of_cells,
			self.number_of_detectors_per_cell,
			self.image_size
			)

	# def xyxy_rectangles_to_yolo( self, rectangles )

	# --------------------------------------------------

	def prepare_data ( self, y_true, y_pred ):
		"""
		Computes several parts of the loss.

		1. Slices the rectangles and the logits.
		2. Converts labels rectangles to yolo format and tile them to have as many as predictions per cells.
		3. Computes a mask where non-empty cells are 1, and empty cells are 0.
		4. Computes a mask where the best fitting rectangle in a cell prediciton is 1, and the rest is 0.

		Arguments:
			self (`pykeras.losses.yolo_v1.YoloLoss`): Yolo loss instance.
			y_true                  (`keras.Tensor`): Batch of labels [BATCH, S, S, 5+C].
			y_pred                  (`keras.Tensor`): Batch of predictions [BATCH, S, S, 5*B+C].
		"""
		# Usefull values
		S           = self.number_of_cells
		B           = self.number_of_detectors_per_cell
		C           = self.number_of_classes
		X, Y, W, H  = 0, 1, 2, 3

		# Check shape
		assert tensorflow.TensorSpec( [None, S, S, 5   + C], tensorflow.dtypes.float32 ).is_compatible_with( y_true )
		assert tensorflow.TensorSpec( [None, S, S, 5*B + C], tensorflow.dtypes.float32 ).is_compatible_with( y_pred )
				
		# Identify non empty cells
		non_empty = self.identify_non_empty_cells( y_true ) # [BATCH, S, S], bool
		
		# Slice labels
		true_rect_xyxy, true_logits = self.slice_labels( y_true ) # [BATCH, S, S, 5], float, [BATCH, S, S, C], float
		
		# Slice predictions
		pred_rects_yolo, pred_logits = self.slice_predictions( y_pred ) # [BATCH, S, S, 5*B], float, [BATCH, S, S, C], float

		# Repeat label bounding rectangles as many times as predicted bounding rectangles per cell
		true_rects_xyxy = self.repeat_labels_rectangles_to_match_predictions( true_rect_xyxy ) # [BATCH, S, S, 5*B], float

		# Convert predictions rectangle to the format (Xmin, Ymin, Xmax, Ymax)
		pred_rects_xyxy = self.predicted_rectangles_to_xyxy( pred_rects_yolo ) #[BATCH, S, S, 5*B], float

		# Mask that sets to 0 rectangles that are not the best match in cells
		in_charge, ious = self.best_rectangles_matches_mask( true_rects_xyxy, pred_rects_xyxy ) # [BATCH, S, S, 5*B], bool, [BATCH, S, S, 5*B], float

		# Convert label rectangles to yolo format
		true_rects_yolo = self.xyxy_rectangles_to_yolo( true_rects_xyxy ) # [BATCH, S, S, 5*B], float
		
		# Compute masks
		non_empty  = tensorflow.reshape( non_empty, [-1, S, S, 1] )
		I_obj_i    = tensorflow.tile( non_empty, [1, 1, 1, C] )
		I_obj_ij   = tensorflow.math.logical_and( tensorflow.tile( non_empty, [1, 1, 1, B*5] ), in_charge )
		I_noobj_ij = tensorflow.math.logical_not( I_obj_ij )

		# Replace label confidence by iou
		true_rects_yolo = tensorflow.where( ious > 0, ious, true_rects_yolo )

		# Empty and non-empty cells predictions and labels
		true_obj_rects   = tensorflow.multiply( true_rects_yolo, tensorflow.cast( I_obj_ij,   tensorflow.float32 ) )
		pred_obj_rects   = tensorflow.multiply( pred_rects_yolo, tensorflow.cast( I_obj_ij,   tensorflow.float32 ) )
		true_noobj_rects = tensorflow.multiply( true_rects_yolo, tensorflow.cast( I_noobj_ij, tensorflow.float32 ) )
		pred_noobj_rects = tensorflow.multiply( pred_rects_yolo, tensorflow.cast( I_noobj_ij, tensorflow.float32 ) )
		true_logits      = tensorflow.multiply( true_logits, tensorflow.cast( I_obj_i,   tensorflow.float32 ) )
		pred_logits      = tensorflow.multiply( pred_logits, tensorflow.cast( I_obj_i,   tensorflow.float32 ) )
		
		true_obj_rects   = tensorflow.reshape( true_obj_rects,   [-1, S*S*B, 5] )
		pred_obj_rects   = tensorflow.reshape( pred_obj_rects,   [-1, S*S*B, 5] )
		true_noobj_rects = tensorflow.reshape( true_noobj_rects, [-1, S*S*B, 5] )
		pred_noobj_rects = tensorflow.reshape( pred_noobj_rects, [-1, S*S*B, 5] )
		true_logits      = tensorflow.reshape( true_logits,      [-1, S*S,   C] )
		pred_logits      = tensorflow.reshape( pred_logits,      [-1, S*S,   C] )

		# confidence in empty cells should be 0

		return true_obj_rects, pred_obj_rects, true_noobj_rects, pred_noobj_rects, true_logits, pred_logits

	# def prepare_data ( self, y_true, y_pred )

# class YoloLoss ( keras.losses.Loss )