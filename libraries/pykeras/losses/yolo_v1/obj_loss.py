# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools.assertions

# LOCALS
from .yolo_loss import YoloLoss

# ##################################################
# ###               CLASS OBJ-LOSS               ###
# ##################################################

class OBJLoss ( YoloLoss ):
	"""
	Loss over bounding rectangle confidence in non-empty detectors.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, image_size=(448., 448.), number_of_cells=7, number_of_detectors_per_cell=2, number_of_classes=20, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.yolo_v1.OBJLoss` class.

		Arguments:
			self (`pykeras.losses.yolo_v1.OBJLoss`): Yolo loss instance to initialize.
			image_size    (`tuple` of `2` `float`s): Image size in pixels.
			number_of_cells                 (`int`): Number of cells on the width and height of the image (aka. S).
			number_of_detectors_per_cell    (`int`): Number of bounding box detector per cell (aka. B).
			number_of_classes               (`int`): Number of classes to separate.
		"""
		assert pytools.assertions.type_is_instance_of( self, OBJLoss )
		assert pytools.assertions.type_is( image_size, tuple )
		assert pytools.assertions.equal( len(image_size), 2 )
		assert pytools.assertions.tuple_items_type_is( image_size, float )
		assert pytools.assertions.type_is( number_of_cells, int )
		assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
		assert pytools.assertions.type_is( number_of_classes, int )

		super( OBJLoss, self ).__init__(
			image_size,
			number_of_cells,
			number_of_detectors_per_cell,
			number_of_classes,
			**kwargs
			)

	# def __init__ ( self, ... )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def call ( self, y_true, y_pred ):
		"""
		Compute the the YOLO width and height loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.WHLoss`): Loss to compute.
			y_true        (`keras.Tensor`): Batch of labels [BATCH, S, S, 5+C].
			y_pred        (`keras.Tensor`): Batch of predictions [BATCH, S, S, 5*B+C].
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloLoss )
		
		# Usefull values
		BATCH = tensorflow.shape( y_true )[ 0 ]
		S     = self.number_of_cells
		B     = self.number_of_detectors_per_cell
		C     = self.number_of_classes
		X, Y, W, H, CONF = 0, 1, 2, 3, 4

		# Check shape
		assert tensorflow.TensorSpec( [None, S, S, 5   + C], tensorflow.dtypes.float32 ).is_compatible_with( y_true )
		assert tensorflow.TensorSpec( [None, S, S, 5*B + C], tensorflow.dtypes.float32 ).is_compatible_with( y_pred )

		# Do most of the work
		true_obj_rects, pred_obj_rects, true_noobj_rects, pred_noobj_rects, true_logits, pred_logits = self.prepare_data( y_true, y_pred )

		# Compute loss overt detectors
		c_obj_loss = self._c_obj_loss( true_obj_rects[:, :, 4], pred_obj_rects[:, :, 4] ) # [BATCH, S*S*B]
		
		# Sum over cells and detectors
		c_obj_loss = tensorflow.math.reduce_sum( c_obj_loss, axis=-1 ) # [BATCH]

		# Compound loss
		return c_obj_loss

	# def call ( self, y_true, y_pred )

# class OBJLoss ( YoloLoss )