# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCAS
from .vector_distance import VectorDistance

# ##################################################
# ###     CLASS CONTROL-POINTS-DISTANCES-V1      ###
# ##################################################

class ControlPointsDistancesV1 ( VectorDistance ):
	"""
	Distance between batches of control points sets with shape [batch_size, number_of_control_points, number_of_points, 2]

	In BB8 V1 the `number_of_control_points` equals the `number_of_classes` and the `number_of_points` is 8
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, distance_scaling_factor=10.0, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.ControlPointsDistancesV1` loss.

		Arguments:
			self (`pykeras.losses.ControlPointsDistancesV1`): Instance to initialize.
			distance_scaling_factor                (`float`): Scaling factor applied to points distances before reduction.

		Named Arguments:
			see `pykeras.losses.Distance` and `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsDistancesV1 )
		assert pytools.assertions.type_is( distance_scaling_factor, float )

		super( ControlPointsDistancesV1, self ).__init__( **kwargs )

		self._distance_scaling_factor = distance_scaling_factor

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def distance_scaling_factor ( self ):
		"""
		Scaling factor applied to points distances before reduction (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsDistancesV1 )

		return self._distance_scaling_factor
	
	# def distance_scaling_factor ( ... )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def call ( self, y_true, y_pred ):
		"""
		Compute the value of this distance loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.ControlPointsDistancesV1`): Loss to compute.
			y_true                          (`keras.Tensor`): Batch of labels.
			y_true                          (`keras.Tensor`): Batch of predictions.
		
		Returns:
			`keras.Tensor`: Loss for each example in the batch.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsDistancesV1 )
		
		# constants
		distance_scaling_factor = tensorflow.constant( self.distance_scaling_factor, tensorflow.float32 )

		# Mask predictions and label [batch_size, classes, 8, 2]
		y_nan  = tensorflow.math.is_nan( y_true )	
		y_true = tensorflow.where( y_nan, tensorflow.zeros_like( y_true ), y_true ) 
		y_pred = tensorflow.where( y_nan, tensorflow.zeros_like( y_pred ), y_pred )

		# Compute point to point distances [batch_size, classes, 8]
		distances = pykeras.geometry.distances( y_true, y_pred, norm_order=self.order, axis=self.axis )
		
		# Scale distances [batch_size, classes, 8]
		distances = tensorflow.math.multiply( distances, distance_scaling_factor )

		# Sum over the 8 points [batch_size, classes]
		distances = tensorflow.math.reduce_sum( distances, axis=-1 )

		# Sum over each object [batch_size]
		return tensorflow.math.reduce_sum( distances, axis=-1 )

	# def call ( ... )

# class ControlPointsDistancesV1 ( VectorDistances )