# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCAS
from .distance import Distance

# ##################################################
# ###           CLASS MATRIX-DISTANCE            ###
# ##################################################

class MatrixDistance ( Distance ):
	"""
	The matrix distance loss computes the sum of distances between matrices.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.MatrixDistance` loss.

		Arguments:
			self (`pykeras.losses.MatrixDistance`): Instance to initialize.

		Named Arguments:
			see `pykeras.losses.Distance` and `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, MatrixDistance )

		super( MatrixDistance, self ).__init__( axis=(-2, -1), **kwargs )

	# def __init__ ( self, **kwargs )

# class MatrixDistance ( Distance )