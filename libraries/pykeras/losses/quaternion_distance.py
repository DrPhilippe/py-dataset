# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pytools.assertions

# LOCAS
from .vector_distance import VectorDistance

# ##################################################
# ###         CLASS QUATERNION-DISTANCE          ###
# ##################################################

class QuaternionDistance ( VectorDistance ):
	"""
	The quaternion distance loss computes the sum of distances
	with respect to normalized labels (unit norm of 1).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.QuaternionDistance` loss.

		Arguments:
			self (`pykeras.losses.QuaternionDistance`): Instance to initialize.

		Named Arguments:
			see `pykeras.losses.VectorDistance` and `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, QuaternionDistance )

		super( QuaternionDistance, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def call( self, y_true, y_pred ):
		"""
		Compute the value of this quaternion distance loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.QuaternionDistance`): Loss to compute.
			y_true                    (`keras.Tensor`): Batch of labels.
			y_true                    (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, QuaternionDistance )
		
		y_true, norm = tensorflow.linalg.normalize( y_true, ord=self.order, axis=self.axis )
		return super( QuaternionDistance, self ).call( y_true, y_pred )
	
	# def call( self, y_true, y_pred )

# class QuaternionDistance ( VectorDistance )