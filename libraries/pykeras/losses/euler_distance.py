# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.geometry
import pytools.assertions

# LOCAS
from .vector_distance import VectorDistance

# ##################################################
# ###            CLASS EULER-DISTANCE            ###
# ##################################################

class EulerDistance ( VectorDistance ):
	"""
	The euler distance loss computes the sum of euler angles distances.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.EulerDistance` loss.

		Arguments:
			self (`pykeras.losses.EulerDistance`): Instance to initialize.

		Named Arguments:
			see `pykeras.losses.VectorDistance` and `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, EulerDistance )

		super( EulerDistance, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def call( self, y_true, y_pred ):
		"""
		Compute the value of this quaternion distance loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.EulerDistance`): Loss to compute.
			y_true               (`keras.Tensor`): Batch of labels.
			y_true               (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, EulerDistance )
		
		# Reshape
		y_true = tensorflow.reshape( y_true, [-1, 3] )
		y_pred = tensorflow.reshape( y_pred, [-1, 3] )
		
		# Compute distances from predicted translation to ground truth translation
		d = pykeras.geometry.euler_distances( y_true, y_pred, norm_order=self.order, axis=self.axis, euler_units=pykeras.geometry.RANDIANS )
		return tensorflow.math.reduce_sum( d, axis=-1 )

	# def call( self, y_true, y_pred )

# class QuaternionDistance ( VectorDistance )