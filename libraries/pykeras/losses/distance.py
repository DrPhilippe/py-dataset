# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.geometry
import pytools.assertions

# ##################################################
# ###               CLASS DISTANCE               ###
# ##################################################

class Distance ( keras.losses.Loss ):
	"""
	The distance loss computes the sum of distances.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, axis=-1, order='euclidean', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.losses.Distance` loss.

		Arguments:
			self (`pykeras.losses.Distance`): Instance to initialize.
			axis   (`int`/`tuple` of `int`s): Axis of the tensors on which to compute the distance.
			order      (`str`/`float`/`int`): Order of the distance norm.

		Named Arguments:
			see `keras.losses.Loss` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, Distance )
		assert pytools.assertions.type_in( axis, (int, tuple) )
		if isinstance( axis, tuple ):
			assert pytools.assertions.tuple_items_type_is( axis, int )
		assert pytools.assertions.type_in( order, (float, int, str) )
		if isinstance( order, str ):
			assert pytools.assertions.value_in( order, ('euclidean', 'fro') )

		self._axis  = axis
		self._order = order

		super( Distance, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def axis ( self ):
		"""
		Axis of the tensors on which to compute the distance (`int`/`tuple` of `int`s).

		See `tensorflow.norm` for accepted values.
		"""
		assert pytools.assertions.type_is_instance_of( self, Distance )

		return self._axis
	
	# def axis ( self )

	# --------------------------------------------------

	@property
	def order ( self ):
		"""
		Order of the distance norm (`str`/`float`/`int`).

		See `tensorflow.norm` for accepted values.
		"""
		assert pytools.assertions.type_is_instance_of( self, Distance )

		return self._order
	
	# def order ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def call ( self, y_true, y_pred ):
		"""
		Compute the value of this distance loss on the given batch of labels ans predictions.

		Arguments:
			self (`pykeras.losses.Distance`): Loss to compute.
			y_true          (`keras.Tensor`): Batch of labels.
			y_true          (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, Distance )
	
		d = pykeras.geometry.distances( y_true, y_pred, norm_order=self.order, axis=self.axis )
		return tensorflow.math.reduce_sum( d, axis=-1 )

	# def call ( self, y_true, y_pred )

# class Distance ( keras.losses.Loss )