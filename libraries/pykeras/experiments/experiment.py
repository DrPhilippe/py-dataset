# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import glob
import numpy
import pandas
import tensorflow

# INTERNALS
import pytools
import pydataset

# LOCAS
import pykeras

# ##################################################
# ###              CLASS EXPERIMENT              ###
# ##################################################

class Experiment:
	"""
	Base class for experiments.
	"""
	LINE_LENGTH = 96

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def compute_metrics ( cls, all_labels, all_predictions, arguments ):
		return {}

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def draw_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Arguments:
			cls (`type`): Experiment class type.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = argparse.ArgumentParser()
		parser.add_argument(
			'--dataset_path',
			type = str,
			help = 'Path of the directory containing the dataset.'
			)
		parser.add_argument(
			'--batch_size',
			   type = int,
			default = 64,
			   help = 'Datasets batch size.'
			)
		parser.add_argument(
			'--drop_batch_reminder',
			action = 'store_true',
			  help = 'Should the batch reminder be dropped?'
			)
		parser.add_argument(
			'--prefetch_size',
			   type = int,
			default = tensorflow.data.AUTOTUNE,
			   help = 'Training and validation dataset prefetch buffer size.'
			   )
		parser.add_argument(
			'--shuffle',
			   type = bool,
			default = True,
			   help = 'Should the training dataset be shuffled?'
			   )
		parser.add_argument(
			'--shuffle_buffer_size',
			   type = int,
			default = 640,
			   help = 'Training dataset shuffle buffer size.'
			)
		return parser

	# def get_argument_parser ( cls )

	# --------------------------------------------------

	@classmethod
	def get_batch_size ( cls, inputs, arguments ):
		"""
		Returns the number of items in the given batch of inputs.

		This number is assumed to be the same in predictions and labels.
		By default it is the number of images.

		Arguments:
			cls                                (`type`): Experiment class type.
			inputs (`dict` of `str` to `numpy.ndarray`): Batch of inputs.
			arguments            (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`int`: Batch size.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return inputs[ arguments.image_feature_name ].shape[ 0 ]

	# def get_batch_size ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_callbacks ( cls, workspace_directory_path, arguments, epoch=0 ):
		"""
		Returns the callbacks used durring model training.

		Arguments:
			cls                                            (`type`): Experiment class type.
			workspace_directory_path (`pytools.path.DirectoryPath`): Workspace diretory path.
			arguments                        (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`list` of `keras.callbacks.Callback`: The classbacks.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( workspace_directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		checkpoints_directory_parh = workspace_directory_path + pytools.path.DirectoryPath( 'checkpoints' )
		tensorboard_directory_parh = workspace_directory_path + pytools.path.DirectoryPath( 'tensorboard' )
		metrics_directory_parh     = workspace_directory_path + pytools.path.DirectoryPath( 'metrics' )

		return [
			pykeras.callbacks.ModelCheckpoint( checkpoints_directory_parh ),
			pykeras.callbacks.CSVLogger( metrics_directory_parh, append=True ),
			# pykeras.callbacks.MetricsSaver( metrics_directory_parh ),
			pykeras.callbacks.TensorBoard( tensorboard_directory_parh, update_freq='batch', write_graph=(epoch==0), profile_batch=0 )
			]

	# def get_callbacks ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def get_classes_weights ( cls, arguments ):
		"""
		Returns the classes weights used durring training.

		Arguments:
			cls                                            (`type`): Experiment class type.
			arguments                        (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`dict` of `int` to `float`/`None`: The weights indexed by classes indexes.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		return None

	# def get_classes_weights ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def get_model ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def get_name ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def get_testing_dataset ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def get_training_dataset ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def get_validation_dataset ( cls, arguments ):
		"""
		Returns the dataset used to validate the model.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The validation dataset.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return cls.get_testing_dataset( arguments )

	# def get_validation_dataset ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions with shape [batch_size].
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def idenfify_correct_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def load_checkpoint ( cls, model, workspace_directory_path, checkpoint=0, logger=None, by_name=False, skip_mismatch=False ):
		"""
		Returns the latest checkpoint file path.

		Arguments:
			cls                                            (`type`): Experiment class type.
			model                                   (`keras.Model`): Model for which to load weights.
			workspace_directory_path (`pytools.path.DirectoryPath`): Workspace diretory path.
			checkpoint        (`int`/`str`/`pytools.path.FilePath`): Epoch to load, if `0` uses the latest one.
			logger                  (`None`/`pytools.tasks.Logger`): Logger.

		Returns:
			`pytools.path.FilePath` the checkpoint filepath.
		"""
		assert pytools.assertions.type_is_instance_of( workspace_directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_in( checkpoint, (int, str, pytools.path.FilePath) )
		
		# Log
		if logger:
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'LOADING CHECKPOINT:' )
		
		# Is an epoch number provided as checkpoint ?
		if isinstance( checkpoint, int ):
			
			# Compose the checkpoitns directory path
			directory = workspace_directory_path + pytools.path.DirectoryPath( 'checkpoints' )
			
			# Log
			if logger:
				logger.log( '    epoch:      {}', checkpoint if checkpoint > 0 else 'latest' )
				logger.log( '    directory:  {}', directory )
			
			# Check
			if directory.is_not_a_directory():
				raise RuntimeError( "The checkpoint directory '{}' does not exists".format(directory) )
			
			# Attempt to find the best checkpoint
			filepath = pykeras.checkpoints.find_latest_checkpoint( directory, checkpoint )
		
		# Is a texxt file path provided as checkpoint ?
		elif isinstance( checkpoint, str ):
			filepath = pytools.path.FilePath( checkpoint )

		# Log
		if logger:
			logger.log( '    checkpoint: {}'.format( filepath ) )
		
		# Check
		if filepath.is_not_a_file():
			raise RuntimeError( "The checkpoint file '{}' does not exists".format(filepath) )

		# Load it
		model.load_weights( str(filepath), by_name=by_name, skip_mismatch=skip_mismatch )

		return model

	# def load_checkpoint ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		raise NotImplementedError()

	# def score_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def save_drawings ( cls, drawings, scores, results, batch_size, directory_path, arguments, start_index=0, filename_format='result_{:06.4f}_{:06d}.jpg' ):
		"""
		Save drawings of this experiment.

		Arguments:
			cls                                  (`type`): Experiment class type.
			drawings                    (`numpy.ndarray`): Drawings (images).
			scores                      (`numpy.ndarray`): Scores (floats).
			results                     (`numpy.ndarray`): Boolean results for each drawing.
			batch_size                               (`int`): Current batch size.
			directory_path (`pytools.path.DirectoryPath`): Path of the directory where to save the drawings.
			arguments              (`argparse.Namespace`): Arguments of the experiment.
			start_index                           (`int`): Index of the first drawing in the batch.
			filename_format                       (`str`): Images filename format with one placeholder for the index.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( drawings, numpy.ndarray )
		assert pytools.assertions.equal( drawings.dtype, numpy.uint8 )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.equal( scores.dtype, numpy.float32 )
		assert pytools.assertions.type_is( results, numpy.ndarray )
		assert pytools.assertions.equal( results.dtype, numpy.bool )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		assert pytools.assertions.type_is( start_index, int )
		assert pytools.assertions.type_is( filename_format, str )

		# Directories where to save drawings
		goods_path = directory_path + pytools.path.DirectoryPath( 'goods' )
		bads_path = directory_path + pytools.path.DirectoryPath( 'bads' )
		
		# Create them if necessary
		if not goods_path.exists():
			goods_path.create()
		if not bads_path.exists():
			bads_path.create()

		# Save each drawing
		for i in range( batch_size ):
				
			# Get a drawing, the score, and the result
			drawing = drawings[ i ]
			score   =   scores[ i ]
			result  =  results[ i ]

			# Create the path of the file where to save it
			filename = filename_format.format( score, i+start_index )
			if result:
				filepath = goods_path + pytools.path.FilePath( filename )
			else:
				filepath = bads_path + pytools.path.FilePath( filename )
			
			# Save the drawing
			cv2.imwrite( str(filepath), drawing )

		# for i in range( batch_size )

	# def save_drawings ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def save_metrics( cls, metrics_values, resutls_directory_path, arguments ):
		"""
		"""
		filepath = resutls_directory_path + pytools.path.FilePath( 'scores.csv' )
		metrics_data_frame = pandas.DataFrame({
			'metric': list(metrics_values.keys()),
			'value': list(metrics_values.values())
			})
		metrics_data_frame.to_csv( str(filepath), float_format='%05.4f', na_rep='nan', index=False )

	# --------------------------------------------------

	@classmethod
	def save_predictions ( cls, predictions, labels, scores, results, directory_path, arguments ):
		"""
		Save predictions made using this experiment.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Predictions for each example.
			labels      (`dict` of `str` to `numpy.ndarray`): Labels for each example.
			results                        (`numpy.ndarray`): Result foreach example.
			directory_path    (`pytools.path.DirectoryPath`): Path of the directory where to save the predictions.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( results, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		def ravel_feature ( name, features_batch ):
			rank = len( features_batch.shape )	
			if rank == 1:
				return { name: features_batch }
			batch_size     = features_batch.shape[ 0 ]
			features_batch = numpy.reshape( features_batch, [batch_size, -1] )
			nb_values      = features_batch.shape[ 1 ]
			new_data       = {}
			for i in range(nb_values):
				new_name = '{}_{}'.format( name, i )
				new_data[ new_name ] = features_batch[ :, i ]
			return new_data

		# File where to save the predictions
		filepath = directory_path + pytools.path.FilePath( 'predictions.csv' )

		# Tabular to save by columns
		data = {}

		# Add a column for each prediction
		for feature_name in predictions.keys():
			predictions_batch = predictions[ feature_name ]
			predictions_batch = ravel_feature( 'pred_'+feature_name, predictions_batch )
			data.update( predictions_batch )
		
		# Add a column for each label
		for feature_name in labels.keys():
			labels_batch = labels[ feature_name ]
			labels_batch = ravel_feature( 'label_'+feature_name, labels_batch )
			data.update( labels_batch )

		# Save results
		data[ 'result' ] = results.ravel()

		# Convert predictions to a pandas DataFrame
		predictions = pandas.DataFrame.from_dict( data )

		# Save it as csv
		predictions.to_csv( str(filepath), float_format='%05.4f', na_rep='nan', index=True, index_label='index' )

	# def save_predictions ( cls, predictions, directory_path, arguments )

	# --------------------------------------------------

	@classmethod
	def train ( cls, argv, logger=None ):
		"""
		Trains the model of the given experiment.

		Arguments:
			cls                    (`type`): The experiment class.
			argv          (`list` of `str`): Command line arguments.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( argv, list )
		assert pytools.assertions.list_items_type_is( argv, str )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		# Parse arguments (except the experiment module path that was already parsed)
		argument_parser = cls.get_argument_parser()
		argument_parser.add_argument(
			'--workspace',
			   type = str,
			default = 'workspace',
			   help = 'Wokspace directory.'
			)
		argument_parser.add_argument(
			'--initial_epoch',
			   type = int,
			default = 0,
			   help = 'Epoch at which to start training.'
			)
		argument_parser.add_argument(
			'--number_of_epochs',
			   type = int,
			default = 1000,
			   help = 'Number of epochs to train for.'
			)
		argument_parser.add_argument(
			'--checkpoint',
			   type = str,
			default = 0,
			   help = 'Checkpoint file path.'
			)
		arguments = argument_parser.parse_args( argv )

		# Log:
		if logger:
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'TRAIN EXPERIMENT:' )
			logger.end_line()
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'ARGUMENTS:' )
			for argument in vars(arguments):
				logger.log( '    {:30s}: {}', argument, getattr(arguments, argument) )
			logger.end_line()
			logger.flush()

		# Experiment name
		name = cls.get_name( arguments )

		# Experiment workspace directory
		workspace_directory_path = pytools.path.DirectoryPath( arguments.workspace )\
		                         + pytools.path.DirectoryPath( name )
		
		# Load training datasets
		training = cls.get_training_dataset( arguments )
		# Load validation datasets
		validation = cls.get_validation_dataset( arguments )

		# Log
		if logger:
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'DATASETS:')
			training.summary( logger )
			validation.summary( logger )
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'CREATING AND COMPILING MODEL:')
			logger.end_line()
			logger.flush()
		
		# Get model
		model = cls.get_model( arguments )

		# Log
		if logger:
			model.summary( line_length=192, print_fn=logger.log )
			logger.end_line()

		# Load checkpoint ?
		if logger:
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'CHECKPOINT:')
		if arguments.initial_epoch > 0:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.initial_epoch )
			if logger:
				logger.log( 'loaded checkpoint: {}', arguments.initial_epoch )
		elif arguments.checkpoint:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.checkpoint, by_name=True, skip_mismatch=True )
			if logger:
				logger.log( 'loaded checkpoint: {}', arguments.checkpoint)
		else:
			if logger:
				logger.log( 'No Checkpoint' )
		if logger:
			logger.end_line()

		# Get callbacks
		callbacks = cls.get_callbacks( workspace_directory_path, arguments, arguments.initial_epoch )

		# Get classes weights
		classes_weights = cls.get_classes_weights( arguments )

		# Train model
		if logger:
			logger.log( '_'*cls.LINE_LENGTH )
			logger.log( 'FITTING:')
		model.fit(
			               x = training.use( logger if arguments.initial_epoch == 0 else None ),
			   initial_epoch = arguments.initial_epoch,
			          epochs = arguments.initial_epoch + arguments.number_of_epochs,
			 steps_per_epoch = training.steps_per_epoch,
			    class_weight = classes_weights,
			       callbacks = callbacks,
			 validation_data = validation.use( logger if arguments.initial_epoch == 0 else None ),
			validation_steps = validation.steps_per_epoch,
			         verbose = 1
			)
	
	# def train ( cls, argv )

	# --------------------------------------------------

	@classmethod
	def evaluate ( cls, argv, logger=None ):
		"""
		Evaluates the model of the given experiment.

		Arguments:
			cls                    (`type`): The experiment class.
			argv          (`list` of `str`): Command line arguments.
			logger (`pytools.tasks.Logger`): logger used to log.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( argv, list )
		assert pytools.assertions.list_items_type_is( argv, str )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'EVALUATE EXPERIMENT:' )
			logger.end_line()
			logger.flush()
	
		# Parse arguments (except the experiment module path that was already parsed)
		argument_parser = cls.get_argument_parser()
		argument_parser.add_argument(
			'--workspace',
			   type = str,
			default = 'workspace',
			   help = 'Wokspace directory.'
			)
		argument_parser.add_argument(
			'--checkpoint',
			   type = str,
			default = '',
			   help = 'Checkpoint used to evaluate model.'
			)
		argument_parser.add_argument(
			'--epoch',
			   type = int,
			default = 0,
			   help = 'Epoch at which to evaluate the model (0 is latest).'
			)
		argument_parser.add_argument(
			'--evaluation_filename',
			   type = str,
			default = 'evaluation_scores.csv',
			   help = 'Name of the CSV file where to save the evaluation scores.'
			)
		arguments = argument_parser.parse_args( argv )
		
		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'ARGUMENTS:' )
			for argument in vars(arguments):
				logger.log( '    {:30s}: {}', argument, getattr(arguments, argument) )
			logger.end_line()
			logger.flush()

		# Experiment name
		name = cls.get_name( arguments )

		# Experiment workspace directory
		workspace_directory_path = pytools.path.DirectoryPath( arguments.workspace )
		workspace_directory_path = workspace_directory_path + pytools.path.DirectoryPath( name )

		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'NAME AND WORKSPACE:' )
			logger.log( '    {:30s}: {}', 'name', name )
			logger.log( '    {:30s}: {}', 'workspace_directory_path', workspace_directory_path )
			logger.end_line()
			logger.flush()

		# Load validation datasets
		validation = cls.get_validation_dataset( arguments )

		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'DATASETS:')
			validation.summary( logger )
			logger.log( '_________________________________________________________________' )
			logger.log( 'CREATING AND COMPILING MODEL:')
			logger.end_line()
			logger.flush()

		# Get model
		model = cls.get_model( arguments )
		
		# Load checkpoint ?
		if arguments.checkpoint:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.checkpoint )
		else:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.epoch )

		# Get callbacks
		callbacks = cls.get_callbacks( workspace_directory_path, arguments, 0 ) # 0-> save graph
		
		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'EVALUATING MODEL:')
			logger.end_line()
			logger.flush()

		# Evaluate model
		scores = model.evaluate( validation.use(logger),
			  steps = validation.steps_per_epoch,
			verbose = 1
			)
				
		# Log
		if logger:
			logger.end_line()
			logger.log( '_________________________________________________________________' )
			logger.log( 'SAVING METRICS SCORES:')
			logger.end_line()
			logger.flush()

		# Number of metrics (it may be 1 if only the loss is used)
		number_of_metrics = len( model.metrics_names )

		# Convert scores to an array of floats
		if number_of_metrics == 1:
			scores = [scores]
		_scores = []
		names = []
		for i in range(number_of_metrics):
			score = scores[i]
			name = model.metrics_names[i]
			if isinstance( score, float ):
				_scores.append( score )
				names.append( name )
				if logger:
					logger.log( '    {:10s}: {}', name, score )
			
			elif logger:
				logger.log( 'WARNING: ignoring metric "{}" because its value is not a float.', name )
				number_of_metrics -= 1# Log

		
		scores = numpy.asarray( _scores, dtype=numpy.float32 )

		# If no metrics remains, exit
		if number_of_metrics == 0:
			return

		# Compose the path of the evaluation file
		filepath = workspace_directory_path + pytools.path.FilePath( arguments.evaluation_filename )
		
		# Save scores
		dataframe = pandas.DataFrame({
			'metrics': names,
			'scores': scores,
			})
		dataframe.to_csv(
			str(filepath),
			sep=',',
			na_rep='nan',
			float_format='%05.4f',
			index=False
			)
		
	# def evaluate ( cls, argv, logger )

	# --------------------------------------------------

	@classmethod
	def predict ( cls, argv, logger=None, progress_tracker=None ):
		"""
		Save predictions of the given experiment.

		Arguments:
			cls                                             (`type`): The experiment class.
			argv                                   (`list` of `str`): Command line arguments.
			logger                    (`None`/`pytools.task.Logger`): Logger.
			progress_tracker (`None`/`pytools.task.ProgressTracker`): Progress tracker used to log progression.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( argv, list )
		assert pytools.assertions.list_items_type_is( argv, str )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'PREDICT EXPERIMENT:' )
			logger.end_line()
			logger.flush()

		# Parse arguments (except the experiment module path that was already parsed)
		argument_parser = cls.get_argument_parser()
		argument_parser.add_argument(
			'--workspace',
			   type = str,
			default = 'workspace',
			   help = 'Wokspace directory.'
			)
		argument_parser.add_argument(
			'--checkpoint',
			   type = str,
			default = '',
			   help = 'Checkpoint used to predict with the model.'
			)
		argument_parser.add_argument(
			'--epoch',
			   type = int,
			default = 0,
			   help = 'Epoch at which to predict with the model (0 is latest).'
			)
		argument_parser.add_argument(
			'--no_drawings',
			 action = 'store_true',
			   help = 'If set (`True`), predictions drawings wont be created.'
			)
		arguments = argument_parser.parse_args( argv )
		
		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'ARGUMENTS:' )
			for argument in vars(arguments):
				logger.log( '    {:30s}: {}', argument, getattr(arguments, argument) )
			logger.end_line()
			logger.flush()

		# Experiment name
		name = cls.get_name( arguments )

		# Experiment workspace directory
		workspace_directory_path = pytools.path.DirectoryPath( arguments.workspace )
		workspace_directory_path = workspace_directory_path + pytools.path.DirectoryPath( name )
		resutls_directory_path   = workspace_directory_path + pytools.path.DirectoryPath( 'results' )
		if not resutls_directory_path.exists():
			resutls_directory_path.create()

		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'NAME AND WORKSPACE:' )
			logger.log( '    {:30s}: {}', 'name', name )
			logger.log( '    {:30s}: {}', 'workspace_directory_path', workspace_directory_path )
			logger.log( '    {:30s}: {}', 'resutls_directory_path', resutls_directory_path )
			logger.end_line()
			logger.flush()

		# Load testing datasets
		testing = cls.get_testing_dataset( arguments )
		
		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'DATASETS:')
			testing.summary( logger )
			logger.log( '_________________________________________________________________' )
			logger.log( 'CREATING AND COMPILING MODEL:')
			logger.end_line()
			logger.flush()

		# Get model
		model = cls.get_model( arguments )
		
		# Log
		if logger:
			model.summary( line_length=96, print_fn=logger.log )
			logger.end_line()

		# Load checkpoint
		if arguments.checkpoint:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.checkpoint )
		else:
			model = cls.load_checkpoint( model, workspace_directory_path, arguments.epoch )
		
		# Variables used durring the loop
		batch_index              = 1 # current bacth index
		predictions              = {} # buffer containing all the predictions
		labels                   = {} # buffer containing all the labels
		scores                   = None # buffer containing all the scores
		results                  = None # buffer containing all the results
		total_number_of_drawings = 0 # total number of drawings

		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )
		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'PREDICTING:')
			logger.end_line()
			logger.flush()

		# Go through each batch
		for batch in testing.use(logger):

			# Unpack the batch
			length = len(batch)
			if length == 2:
				inputs_batch = batch[ 0 ]
				labels_batch = batch[ 1 ]
			elif length == 3:
				inputs_batch  = batch[ 0 ]
				labels_batch  = batch[ 1 ]
				weights_batch = batch[ 2 ]
			else:
				raise RuntimeError(
					'Unsupported batch data, expected a tuple with length 2 or 3: (inputs, labels) or (inputs, labels, weights).'\
					+'Got a tuple with lenght {}'.format(
					length
					))

			# Get predictions for that batch
			predictions_batch = model( inputs_batch )

			# Convert inputs and labels to named ndarray
			inputs_batch      = { k: v.numpy() for k, v in inputs_batch.items() }
			labels_batch      = { k: v.numpy() for k, v in labels_batch.items() }
			predictions_batch = { k: v.numpy() for k, v in predictions_batch.items() }

			# Get the batch size
			batch_size = cls.get_batch_size( inputs_batch, arguments )

			# Buffer labels and predictions
			for k, v in labels_batch.items():
				if len(v.shape) == 1:
					v = numpy.reshape( v, [-1, 1] )
				labels[ k ] = v if k not in labels else numpy.vstack( [labels[ k ], v] )
			for k, v in predictions_batch.items():
				if len(v.shape) == 1:
					v = numpy.reshape( v, [-1, 1] )
				predictions[ k ] = v if k not in predictions else numpy.vstack( [predictions[ k ], v] )

			# Score predictions
			scores_batch = cls.score_predictions( predictions_batch, labels_batch, batch_size, arguments ).ravel()
			scores = scores_batch.copy() if scores is None else numpy.concatenate( (scores, scores_batch) )

			# Identify correct predictions:
			results_batch = cls.idenfify_correct_predictions( predictions_batch, labels_batch, scores_batch, batch_size, arguments ).ravel()
			results = results_batch.copy() if results is None else numpy.concatenate( (results, results_batch) )
	
			# Create and save drawings
			if not arguments.no_drawings:
				# Draw predictions
				drawings_batch = cls.draw_predictions(
					inputs_batch,
					predictions_batch,
					labels_batch,
					scores_batch,
					results_batch,
					batch_size,
					arguments )

				# Save drawings
				cls.save_drawings(
					drawings_batch,
					scores_batch,
					results_batch,
					batch_size,
					resutls_directory_path,
					arguments,
					total_number_of_drawings
					)

				# Update offset
				total_number_of_drawings += drawings_batch.shape[ 0 ]
			
			# if not arguments.no_drawings

			# Update progression
			if progress_tracker:
				progress_tracker.update( float(batch_index)/float(testing.steps_per_epoch) )

			# Next
			batch_index += 1
		
		# for batch in testing.use()

		# Save predictions
		#  cls.save_predictions( predictions, labels, scores, results, resutls_directory_path, arguments )

		metrics_values = cls.compute_metrics( labels, predictions, arguments )
		cls.save_metrics( metrics_values, resutls_directory_path, arguments )
		if logger:
			logger.log( 'METRICS:' )
			for metric_name in metrics_values.keys():
				logger.log( '    {:20s}: {}', metric_name, ['{:06.4f}'.format(value) for value in metrics_values[ metric_name ] ] )

		# # Compute the average number of correct predictions
		# mean_average_precision = numpy.sum( results.astype(numpy.float32) ) / float( results.shape[0] ) * 100.
		# # Log
		# if logger:
		# 	logger.log( 'MAP: {:06.4f}', mean_average_precision )
		# # Save proportion of correct predictions in text file
		# filepath = resutls_directory_path + pytools.path.FilePath( 'map.txt' )
		# with filepath.open( 'wt' ) as file:
		# 	file.write( 'MAP: {:06.4f} %\n'.format(mean_average_precision) )
		
		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def predict ( cls, argv, logger, progress_tracker )

	# --------------------------------------------------

	@classmethod
	def preview ( cls, argv, logger=None, progress_tracker=None ):
		
		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'PREVIEW DATASET:' )
			logger.end_line()
			logger.flush()

		# Parse arguments (except the experiment module path that was already parsed)
		argument_parser = cls.get_argument_parser()
		argument_parser.add_argument(
			'--workspace',
			   type = str,
			default = 'workspace',
			   help = 'Wokspace directory.'
			)
		argument_parser.add_argument(
			'--split',
			   type = str,
			default = 'training',
			choices = ['training', 'testing', 'validation'],
			   help = 'Split to preview.'
			)
		arguments = argument_parser.parse_args( argv )
		
		# Log:
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'ARGUMENTS:' )
			for argument in vars(arguments):
				logger.log( '    {:30s}: {}', argument, getattr(arguments, argument) )
			logger.end_line()
			logger.flush()

		# Experiment name
		name = cls.get_name( arguments )

		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'NAME:' )
			logger.log( '    {:30s}: {}', 'name', name )
			logger.end_line()
			logger.flush()

		# Load dataset
		if ( arguments.split == 'training' ):
			dataset = cls.get_training_dataset( arguments )
		if ( arguments.split == 'testing' ):
			dataset = cls.get_testing_dataset( arguments )
		if ( arguments.split == 'validation' ):
			dataset = cls.get_validation_dataset( arguments )
		
		# Log
		if logger:
			logger.log( '_________________________________________________________________' )
			logger.log( 'DATASETS:')
			dataset.summary( logger )
			logger.flush()

		# Go through each batch
		for batch in dataset.use(logger):

			# Unpack the batch
			length = len(batch)
			if length == 2:
				inputs_batch = batch[ 0 ]
				labels_batch = batch[ 1 ]
			elif length == 3:
				inputs_batch  = batch[ 0 ]
				labels_batch  = batch[ 1 ]
				weights_batch = batch[ 2 ]
			else:
				raise RuntimeError(
					'Unsupported batch data, expected a tuple with length 2 or 3: (inputs, labels) or (inputs, labels, weights).'\
					+'Got a tuple with lenght {}'.format(
					length
					))

			# Convert data to numpy
			inputs_batch  = { k: v.numpy() for k, v in inputs_batch.items() }
			labels_batch  = { k: v.numpy() for k, v in labels_batch.items() }
			weights_batch = { k: v.numpy() for k, v in weights_batch.items() }

			# Go through images
			if 'image' in inputs_batch:
				batch_size = inputs_batch[ 'image' ].shape[ 0 ]
			elif 'foreground_color' in inputs_batch:
				batch_size = inputs_batch[ 'foreground_color' ].shape[ 0 ]

			for i in range( batch_size ):

				# Fetch image (required)
				if 'image' in inputs_batch:
					image = inputs_batch[ 'image' ][ i ]
				elif 'foreground_color' in inputs_batch:
					image = inputs_batch[ 'foreground_color' ][ i ]

				# Display bounding rectangle
				if ( 'bounding_rectangle' in labels_batch ):
					bounding_rectangle = labels_batch[ 'bounding_rectangle' ][ i ]
					image = pydataset.cv2_drawing.draw_bounding_rectangle( image, bounding_rectangle.astype(numpy.int32), rect_color=(0,0,255), rect_thickness=1, points_color=(0,0,255), points_radius=1, points_thickness=1 )
				
				# Display bounding box
				if ( 'bounding_box' in labels_batch ):
					bounding_box = labels_batch[ 'bounding_box' ][ i ]
					if ( bounding_box.shape[-1] == 2 ):
						image = pydataset.cv2_drawing.draw_bounding_box ( image, bounding_box, edges_color=(0,0,255), edges_thickness=1, points_color=(0,0,255), points_radius=1, points_thickness=1 )
				
				# Display bounding box
				if ( 'bb8_v1' in labels_batch ):
					bb8_v1 = labels_batch[ 'bb8_v1' ][ i ]
					for j in range( bb8_v1.shape[0] ):
						if ( numpy.isnan(numpy.sum(bb8_v1[ j ])) ):
							continue
						if ( arguments.normalize ):
							bb8_v1[ j ] = pydataset.pose_utils.rescale_image_points( bb8_v1[ j ], image.shape[0], image.shape[1], arguments.normalize_min, arguments.normalize_max )
						image = pydataset.cv2_drawing.draw_bounding_box( image, bb8_v1[ j ], edges_color=(0,0,255), edges_thickness=1, points_color=(0,0,255), points_radius=1, points_thickness=1 )

				# Display visible mask
				if ( 'visible_mask' in inputs_batch ):
					visible_mask  = inputs_batch[ 'visible_mask' ][ i ]
					cv2.imshow( 'visible_mask', (visible_mask*255.).astype(numpy.uint8)  )

				cv2.imshow( 'image', image  )
				if ( cv2.waitKey( 1000 ) == ord('q') ):
					return

# class Experiment

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return Experiment

# def get_experiment ()
