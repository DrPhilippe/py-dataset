# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import numpy
import sys

# INTENALS
import pykeras
import pytools

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def load_experiment ( argv, logger=None ):
	"""
	Find and loads the experiment specified in the console arguments.

	Arguments:
		argv          (`list` of `str`): Command line arguments.
		logger (`pytools.tasks.Logger`): logger used to log.

	Returns:
		`type`: The experiment class.
	"""
	assert pytools.assertions.type_is( argv, list )
	assert pytools.assertions.list_items_type_is( argv, str )
	assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

	# Console argument used to find the experiment
	parser = argparse.ArgumentParser()
	parser.add_argument( 'experiment_name', type=str, help='Path of the python module containing the experiemnt to train.' )
	arguments = parser.parse_args( argv )

	# Log
	# if logger:
	# 	logger.log( 'LOAD EXPERIMENT:')
	# 	logger.log( '    experiment_name: {}', arguments.experiment_name )
	# 	logger.end_line()
	# 	logger.flush()

	# Load the experiment module
	experiment_module = pytools.plugins.load_module( arguments.experiment_name )
	
	# Get the experiment
	return experiment_module.get_experiment()

# def load_experiment ( argv )