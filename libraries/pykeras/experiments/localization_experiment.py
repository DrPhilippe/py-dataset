# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import pandas
from tensorflow import keras

# INTERNALS
import pydataset
import pytools

# LOCALS
from .experiment import Experiment

# ##################################################
# ###      CLASS CLASSIFICATION-EXPERIMENT       ###
# ##################################################

class LocalizationExperiment ( Experiment ):
	"""
	Base class for localization experiments.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )


		# Get features
		inputs_images = inputs[ arguments.image_feature_name ]
		
		# Create drawings buffer
		shape    = list(inputs_images.shape)
		drawings = numpy.empty( shape, inputs_images.dtype )

		# Draw each images
		for image_index in range( batch_size ):
				
			# Get the current image
			drawing    = inputs_images[ image_index ].copy()

			# Get the predictions and ground thruth bounding rectangles
			predictions_rects = cls.get_predictions_bounding_rectangles( image_index, predictions, arguments )
			labels_rects      = cls.get_labels_bounding_rectangles( image_index, labels, arguments )

			# Draw label bounding rectangles
			for rect_index in range( labels_rects.shape[0] ):
				drawing = cls.draw_rectangle( drawing, labels_rects[ rect_index ], (0,255,0), 2, arguments, False )

			# Draw predicted bounding rectangle
			for rect_index in range( predictions_rects.shape[0] ):
				drawing = cls.draw_rectangle( drawing, predictions_rects[ rect_index ], (0,0,255), 2, arguments, True )
			
			# Save the drawing
			drawings[ image_index ] = drawing

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def draw_rectangle ( cls, image, rect, color, thickness, arguments, label_category=True ):
		"""
		Draws a rectangle on the given image.

		Arguments:
			cls                     (`type`): Experiment class.
			image          (`numpy.ndarray`): Image on which to draw with shape [height, width, channels] and dtype uint8.
			rect           (`numpy.ndarray`): Rectangle formatted as [x1, y1, x2, y2, category index] and dtype float32.
			color        (`tuple` of `int`s): Color used to draw the rectangle, must be the same length as the image number of channels.
			thickness                (`int`): Thickness of the rectangle lines.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`numpy.ndarray`: The image with the drawn rectangle.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image, numpy.ndarray )
		assert pytools.assertions.equal( image.dtype, numpy.uint8 )
		assert pytools.assertions.type_is( rect, numpy.ndarray )
		assert pytools.assertions.equal( rect.dtype, numpy.float32 )
		assert pytools.assertions.type_is( color, tuple )
		assert pytools.assertions.tuple_items_type_is( color, int )
		assert pytools.assertions.type_is( thickness, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# Get image dimensions
		height             = float( image.shape[ 0 ] )
		width              = float( image.shape[ 1 ] )
		number_of_channels = image.shape[ 2 ]
		
		# Check
		if ( len(color) != number_of_channels ):
			raise RuntimeError(
				"The color used to draw the rectangle must have the same amount of coordinates as image channels: {} != {}".format(
				len(color), number_of_channels
				))

		# Convert rectangle to pixels
		x1       = int(width  * rect[ 0 ])
		y1       = int(height * rect[ 1 ])
		x2       = int(width  * rect[ 2 ])
		y2       = int(height * rect[ 3 ])

		# Draw the rectangle
		image = cv2.rectangle( image, (x1, y1), (x2, y2), color, thickness )

		# get the category and replace it by the category name if possible
		category      = int( rect[ 4 ] )
		classes_names = cls.get_classes_names( arguments )
		if classes_names:
			category = classes_names[ category ]

		# Display the category
		if label_category:
			image = cv2.putText( image, '{}'.format( category ), (x1+8, y1+24), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1 )

		return image

	# def draw_rectangle ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( LocalizationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle.'
			)
		parser.add_argument(
			'--category_name_feature_name',
			   type = str,
			default = 'category_name',
			   help = 'Name of the feature containing the category name.'
			)
		parser.add_argument(
			'--category_index_feature_name',
			   type = str,
			default = 'category_index',
			   help = 'Name of the feature containing the category index.'
			)
		parser.add_argument(
			'--one_hot_category_feature_name',
			   type = str,
			default = 'one_hot_category_index',
			   help = 'Name of the feature containing the one-hot category index.'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories considered in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return []

	# def get_classes_names ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def get_labels_bounding_rectangles ( cls, image_index, labels, arguments ):
		"""
		Returns the bounding rectangles corresponding to the given labels in the image with the given index.

		The returned bounding rectangle must be in the given format with all coordinates in image relative coordinates:
		[x1, y1, x2, y2, category index]
		
		Arguments:
			cls                                (`type`): Experiment class type.
			image_index                         (`int`): Index of the image.
			labels (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments            (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets localizations and categories.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		raise NotImplementedError()

	# def get_labels_bounding_rectangles ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_predictions_bounding_rectangles ( cls, image_index, predictions, arguments ):
		"""
		Returns the bounding rectangles corresponding to the given labels in the image with the given index.

		The returned bounding rectangle must be in the given format with all coordinates in image relative coordinates:
		[x1, y1, x2, y2, category index]
		
		Arguments:
			cls                                (`type`): Experiment class type.
			image_index                         (`int`): Index of the image.
			labels (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments            (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets localizations and categories.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		raise NotImplementedError()

	# def get_predictions_bounding_rectangles ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# By default scores are mean IoU
		return numpy.greater_equal( scores, 0.5 )
	
	# def idenfify_correct_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		By default this metric returns the mean IoU between ground trouth and predictions

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create scores buffer
		scores = numpy.zeros( [batch_size], numpy.float32 )

		# Go though each pair of prediction/label in the bacth
		for batch_index in range( batch_size ):

			# Get the ground trouth bounding rectangles
			labels_rectangles = cls.get_labels_bounding_rectangles( batch_index, labels, arguments )
			number_of_labels  = labels_rectangles.shape[ 0 ]

			# Get the predicted bounding rectangles
			predictions_rectangles = cls.get_predictions_bounding_rectangles( batch_index, predictions, arguments )
			number_of_predictions  = predictions_rectangles.shape[ 0 ]
			
			# Perform non-maxima suppression
			# TODO
			
			# The score will be the largest IoU over all ground trouth rectangles
			ious = numpy.zeros( [number_of_labels], dtype=numpy.float32 )

			# For each label, the score is the best IoU with any prediction
			for label_index in range( number_of_labels ):

				# Get one label
				label_rectangle = labels_rectangles[ label_index ]

				# Split category and rectangle
				label_category  = label_rectangle[ 4 ]
				label_rectangle = pydataset.data.RectangleData.from_ndarray( label_rectangle[0:4] )

				# Best IoU so far
				best_iou = 0.

				# Go through each prediction, and find the best IoU
				for prediction_index in range(number_of_predictions):
					
					# Get one prediction
					prediction_rectangle = predictions_rectangles[ prediction_index ]

					# Split its category
					prediction_category = prediction_rectangle[ 4 ]

					# Compute IoU only if the predicted category is correct
					if ( prediction_category == label_category ):

						# Convert the rectangle
						prediction_rectangle = pydataset.data.RectangleData.from_ndarray( prediction_rectangle[0:4] )

						# Compute IoU
						iou = pydataset.data.RectangleData.intersection_over_union( prediction_rectangle, label_rectangle )

						# Is it better
						if iou > best_iou:
							best_iou = iou

					# if ( prediction_category == label_category )

				# for prediction_index in range(number_of_predictions)

				ious[ label_index ] = best_iou

			# for label_index in range( number_of_labels )

			# Best IoU
			scores[ batch_index ] = numpy.amax( ious )

		return scores

	# def score_predictions ( cls, ... )

# class LocalizationExperiment ( Experiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return LocalizationExperiment

# def get_experiment ()