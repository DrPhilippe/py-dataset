# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import pandas
import transforms3d
from tensorflow import keras

# INTERNALS
import pydataset.cv2_drawing
import pydataset.pose_utils
import pytools.assertions

# LOCALS
from .experiment import Experiment

# ##################################################
# ###           CLASS POSE-EXPERIMENT            ###
# ##################################################

class PoseExperiment ( Experiment ):
	"""
	Base class for pose estimation experiments.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def compute_metrics ( cls, all_labels, all_predictions, arguments ):
		
		# Fetch all the labels
		labels_K    = all_labels[ arguments.camera_matrix_feature_name ]
		labels_r    = all_labels[ arguments.rotation_feature_name ]
		labels_t    = all_labels[ arguments.translation_feature_name ]
		labels_mesh = all_labels[ arguments.bounding_box_feature_name ]
		# labels_mesh = all_labels[ 'mesh' ]

		# Compute the predicted pose
		predictions_r, predictions_t = cls.get_pose( all_predictions, all_labels, arguments )

		# Number of images
		count = labels_K.shape[0]

		# threashold
		taus = numpy.concatenate([
			numpy.arange( 0., 5.5, 0.5 ),
			numpy.arange( 7.5, 52.5, 2.5 )
			])
		nb = taus.size

		# Per image metrics results
		five_degree                     = numpy.empty( [count, nb], dtype=numpy.bool )
		five_centimeter                 = numpy.empty( [count, nb], dtype=numpy.bool )
		five_degree_and_five_centimeter = numpy.empty( [count, nb], dtype=numpy.bool )
		add_3D                          = numpy.empty( [count, nb], dtype=numpy.bool )
		add_2D                          = numpy.empty( [count, nb], dtype=numpy.bool )
		
		# Default dist coeffs
		dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

		# Go through images
		for i in range(count):

			# Fetch labels
			label_K      = labels_K[ i ]
			label_r      = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( labels_r[ i ] )
			label_t      = labels_t[ i ]
			label_mesh   = labels_mesh[ i ]

			# Fetch predictions
			prediction_r = predictions_r[ i ]
			prediction_t = predictions_t[ i ]

			# 5 degree
			distance_r = numpy.linalg.norm( pydataset.units.radian_to_degree(label_r) - pydataset.units.radian_to_degree(prediction_r) )

			# 5 centimeter
			distance_t = numpy.linalg.norm( label_t*100. - prediction_t*100. )

			# ADD 3D bb
			label_mesh_3D       = pydataset.pose_utils.place_points( label_mesh, label_r,      label_t      )
			prediction_mesh_3D  = pydataset.pose_utils.place_points( label_mesh, prediction_r, prediction_t )
			distance_mesh_3D    = numpy.linalg.norm( label_mesh_3D - prediction_mesh_3D, axis=-1 )
			distance_mesh_3D    = distance_mesh_3D * 100. # meter to centimeter
			distance_mesh_3D    = numpy.mean( distance_mesh_3D, axis=-1 )

			# ADD 2D bb
			label_mesh_2D      = pydataset.pose_utils.project_points( label_mesh, label_K, dist_coeffs, label_r,      label_t      )
			prediction_mesh_2D = pydataset.pose_utils.project_points( label_mesh, label_K, dist_coeffs, prediction_r, prediction_t )
			distance_mesh_2D   = numpy.linalg.norm( label_mesh_2D - prediction_mesh_2D, axis=-1 )
			distance_mesh_2D   = numpy.mean( distance_mesh_2D, axis=-1 )

			# for each threshold
			for j in range( nb ):

				# fetch threshold
				tau = taus[ j ]

				# Compare
				five_degree[ i, j ]                     = distance_r <= tau
				five_centimeter[ i, j ]                 = distance_t <= tau
				five_degree_and_five_centimeter[ i, j ] = five_degree[ i, j ] and five_centimeter[ i, j ]
				add_3D[ i, j ]                          = distance_mesh_3D <= tau
				add_2D[ i, j ]                          = distance_mesh_2D <= tau

			# for j in range( nb ):

		# for i in range(count)

		# Cast metrics per images results to floats
		five_degree                     = five_degree.astype( numpy.float32 )
		five_centimeter                 = five_centimeter.astype( numpy.float32 )
		five_degree_and_five_centimeter = five_degree_and_five_centimeter.astype( numpy.float32 )
		add_3D                          = add_3D.astype( numpy.float32 )
		add_2D                          = add_2D.astype( numpy.float32 )

		# Compute mean average of each metric
		
		five_degree                     = numpy.asarray([ numpy.sum(                     five_degree[ :, j ] ) / float(count) for j in range(nb) ], dtype=numpy.float32 )
		five_centimeter                 = numpy.asarray([ numpy.sum(                 five_centimeter[ :, j ] ) / float(count) for j in range(nb) ], dtype=numpy.float32 )
		five_degree_and_five_centimeter = numpy.asarray([ numpy.sum( five_degree_and_five_centimeter[ :, j ] ) / float(count) for j in range(nb) ], dtype=numpy.float32 )
		add_3D                          = numpy.asarray([ numpy.sum(                          add_3D[ :, j ] ) / float(count) for j in range(nb) ], dtype=numpy.float32 )
		add_2D                          = numpy.asarray([ numpy.sum(                          add_2D[ :, j ] ) / float(count) for j in range(nb) ], dtype=numpy.float32 )

		return {
			'tau': taus,
			'degree': five_degree,
			'cm': five_centimeter,
			'degree_cm': five_degree_and_five_centimeter,
			'add_3d': add_3D,
			'add_2d': add_2D
			}

	# --------------------------------------------------

	@classmethod
	def save_metrics( cls, metrics, resutls_directory_path, arguments ):
		"""
		"""
		filepath = resutls_directory_path + pytools.path.FilePath( 'scores.csv' )
		print( filepath )
		metrics_data_frame = pandas.DataFrame( metrics )
		metrics_data_frame.to_csv( str(filepath), sep=',', float_format='%05.4f', na_rep='nan', index=False )

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# Get a batch of input images
		inputs_image = inputs[ arguments.image_feature_name ]
		
		# Get the corresponding batch of labels
		labels_k  = labels[ arguments.camera_matrix_feature_name ]
		labels_bb = labels[ arguments.bounding_box_feature_name  ]
		labels_r  = labels[ arguments.rotation_feature_name      ]
		labels_t  = labels[ arguments.translation_feature_name   ]

		# Get the corresponding predicted pose
		preds_r, preds_t  = cls.get_pose( predictions, labels, arguments )

		# Create drawings buffer
		shape        = list(inputs_image.shape)
		new_shape    = list(shape)
		new_shape[1] = 512
		new_shape[2] = 512
		drawings     = numpy.empty( new_shape, inputs_image.dtype )
		scores       = numpy.empty( new_shape[1], numpy.float32 )

		# Ratio used to scale the bounsung boxes
		x_ratio  = float(new_shape[ 1 ]) / float(shape[ 1 ])
		y_ratio  = float(new_shape[ 2 ]) / float(shape[ 2 ])
		
		label_color = (83, 222, 73)
		good_color  = (255, 111, 28)
		bad_color   = (28, 111, 255)

		# Draw each images
		batch_size = shape[ 0 ]
		for i in range( batch_size ):
				
			# Get the input image
			image = inputs_image[ i ]

			# Get the associated label
			label_k = labels_k[ i ]
			label_r = labels_r[ i ]
			label_t = labels_t[ i ]
			bb      = labels_bb[ i ]

			# Get the associated prediction
			pred_r = preds_r[ i ]
			pred_t = preds_t[ i ]

			# Get the associated result
			result = results[ i ]

			# Convert rotation if needed
			label_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( label_r )
			pred_r  = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( pred_r )

			# Resize drawing
			image = cv2.resize( image, (512, 512) )

			# Draw label bounding box
			label_bb, j     = cv2.projectPoints( bb, label_r, label_t, label_k, numpy.zeros([5]) )
			label_bb        = numpy.reshape( label_bb, [8, 2] )
			label_bb[:, 0] *= x_ratio
			label_bb[:, 1] *= y_ratio
			image           = pydataset.cv2_drawing.draw_bounding_box( image, label_bb, label_color, 2, label_color, 3, 3 )

			# Draw predicted bounding box
			pred_bb, j     = cv2.projectPoints( bb, pred_r, pred_t, label_k, numpy.zeros([5]) )
			pred_bb        = numpy.reshape( pred_bb, [8, 2] )
			pred_bb[:, 0] *= x_ratio
			pred_bb[:, 1] *= y_ratio
			pred_color     = good_color if result else bad_color
			image          = pydataset.cv2_drawing.draw_bounding_box( image, pred_bb, pred_color, 2, pred_color, 3, 3 )

			# Save drawing
			drawings[ i ] = image

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, inputs, labels, predictions, arguments )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Arguments:
			cls (`type`): Experiment class type.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( PoseExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image.'
			)
		parser.add_argument(
			'--camera_matrix_feature_name',
			   type = str,
			default = 'K',
			   help = 'Name of the feature containing the camera intrinsic parameters matrix.'
			)
		parser.add_argument(
			'--camera_dist_coeffs_feature_name',
			   type = str,
			default = '',
			   help = 'Name of the feature containing the camera distortions coefficients vector.'
			)
		parser.add_argument(
			'--rotation_feature_name',
			   type = str,
			default = 'q',
			   help = 'Name of the feature containing the rotation data'
			)
		parser.add_argument(
			'--translation_feature_name',
			   type = str,
			default = 't',
			   help = 'Name of the feature containing the translation data'
			)
		parser.add_argument(
			'--bounding_box_feature_name',
			   type = str,
			default = 'bounding_box',
			   help = 'Name of the feature containing the object bounding-box.'
			)
		parser.add_argument(
			'--decision_threshold',
			   type = float,
			default = 5.0,
			   help = 'Threshold in pixels used to decide of a bounding-box corner is correctly located on the image plane.'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_pose ( cls, predictions_batch, labels_batch, arguments ):
		"""
		Returns the pose of the examples whoes features are found in the given batch.

		This method is used both on labels and predictions to get the pose during prediction.
		
		Arguments:
			cls                                           (`type`): Experiment class type.
			predictions_batch (`dict` of `str` to `numpy.ndarray`): Batch of predicted features.
			labels_batch      (`dict` of `str` to `numpy.ndarray`): Batch of label features.

		Returns:
			euler_batch       (`numpy.ndarray`): Euler angles of each example in the batch.
			translation_batch (`numpy.ndarray`): Translation vector of each example in the batch.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions_batch, dict )
		assert pytools.assertions.dictionary_types_are( predictions_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels_batch, dict )
		assert pytools.assertions.dictionary_types_are( labels_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		raise NotImplementedError()

	# def get_pose ( cls, predictions_batch, labels_batch, arguments )
	
	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions with shape [batch_size].
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		# Get a batch camera intrinsics parameters and bounding-boxes
		batch_k  = labels[ arguments.camera_matrix_feature_name ]
		batch_bb = labels[ arguments.bounding_box_feature_name  ]
		labels_r = labels[ arguments.rotation_feature_name      ]
		labels_t = labels[ arguments.translation_feature_name   ]

		# Get batch size
		batch_size = batch_bb.shape[ 0 ]	

		# Get camera dist coefs if provided
		if arguments.camera_dist_coeffs_feature_name:
			batch_coefs = labels[ arguments.camera_dist_coeffs_feature_name ]
		else:
			batch_coefs = numpy.zeros( [batch_size, 5] )

		# Get a batch of pose features (labels and predictions)
		preds_r, preds_t = cls.get_pose( predictions, labels, arguments )

		# Create result buffer
		results = numpy.empty( [batch_size, 1], numpy.bool )

		# Draw each images
		for i in range( batch_size ):
			
			# Get one prediction and label
			k       =     batch_k[ i ]
			coefs   = batch_coefs[ i ]
			bb      =    batch_bb[ i ]
			label_r =    labels_r[ i ]
			label_t =    labels_t[ i ]
			pred_r  =     preds_r[ i ]
			pred_t  =     preds_t[ i ]
			
			# Convert rotation if needed
			label_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( label_r )
			pred_r  = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( pred_r )

			# Project label bounding box
			label_bb, j = cv2.projectPoints( bb, label_r, label_t, k, coefs )
			label_bb    = numpy.reshape( label_bb, [8, 2] )

			# Draw predicted bounding box
			pred_bb, j = cv2.projectPoints( bb, pred_r, pred_t, k, coefs )
			pred_bb    = numpy.reshape( pred_bb, [8, 2] )			
			
			# Point to point distance
			distances = numpy.linalg.norm( pred_bb - label_bb, axis=-1 )

			# Identify distances less than 5 pixels
			correct_distances = numpy.less_equal( distances, arguments.decision_threshold )

			# Save the result
			results[ i ] = numpy.logical_and.reduce( correct_distances )

		# for i in range( batch_size )

		return results

	# def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
	
	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Get a batch camera intrinsics parameters and bounding-boxes
		batch_k  = labels[ arguments.camera_matrix_feature_name ]
		batch_bb = labels[ arguments.bounding_box_feature_name  ]
		labels_r = labels[ arguments.rotation_feature_name      ]
		labels_t = labels[ arguments.translation_feature_name   ]

		# Get batch size
		batch_size = batch_bb.shape[ 0 ]	

		# Get camera dist coefs if provided
		if arguments.camera_dist_coeffs_feature_name:
			batch_coefs = labels[ arguments.camera_dist_coeffs_feature_name ]
		else:
			batch_coefs = numpy.zeros( [batch_size, 5] )

		# Get a batch of pose features (labels and predictions)
		preds_r, preds_t = cls.get_pose( predictions, labels, arguments )

		# Create result buffer
		scores = numpy.empty( [batch_size, 1], numpy.float32 )

		# Draw each images
		for i in range( batch_size ):
			
			# Get one prediction and label
			k       =     batch_k[ i ]
			coefs   = batch_coefs[ i ]
			bb      =    batch_bb[ i ]
			label_r =    labels_r[ i ]
			label_t =    labels_t[ i ]
			pred_r  =     preds_r[ i ]
			pred_t  =     preds_t[ i ]
			
			# Convert rotation if needed
			label_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( label_r )
			pred_r  = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( pred_r )

			# Project label bounding box
			label_bb, j = cv2.projectPoints( bb, label_r, label_t, k, coefs )
			label_bb    = numpy.reshape( label_bb, [8, 2] )

			# Draw predicted bounding box
			pred_bb, j = cv2.projectPoints( bb, pred_r, pred_t, k, coefs )
			pred_bb    = numpy.reshape( pred_bb, [8, 2] )			
			
			# Point to point distance
			distances = numpy.linalg.norm( pred_bb - label_bb, axis=-1 )

			# Save the result
			scores[ i ] = numpy.mean( distances )

		# for i in range( batch_size )

		return scores

	# def score_predictions ( cls, ... )

# class PoseExperiment ( Experiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return PoseExperiment

# def get_experiment ()
