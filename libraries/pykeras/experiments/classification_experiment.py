# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import pandas
from tensorflow import keras

# INTERNALS
import pytools.assertions

# LOCALS
from .experiment import Experiment

# ##################################################
# ###      CLASS CLASSIFICATION-EXPERIMENT       ###
# ##################################################

class ClassificationExperiment ( Experiment ):
	"""
	Base class for classification experiments.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( results, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# Get classes names
		classes_names = cls.get_classes_names( arguments )

		# Get features
		inputs_image                 = inputs[ arguments.image_feature_name ]
		labels_one_hot_category      = labels[ arguments.one_hot_category_feature_name ]
		predictions_one_hot_category = predictions[ arguments.one_hot_category_feature_name ]

		# Create drawings buffer
		shape = list(inputs_image.shape)
		depth = shape[3] if len(shape) > 3 else 1
		drawings = numpy.empty( [shape[0], 512, 512, 3], inputs_image.dtype )

		# Draw each images
		for i in range( batch_size ):
				
			# Get one image, prediction and label
			input_image                 = inputs_image[ i ]
			label_one_hot_category      = labels_one_hot_category[ i ]
			prediction_one_hot_category = predictions_one_hot_category[ i ]
			good                        = results[ i ]

			# Get top-1 prediction
			label_category_index      = numpy.argmax( label_one_hot_category )
			prediction_category_index = numpy.argmax( prediction_one_hot_category )

			# Use classes names if available
			prediction = classes_names[ prediction_category_index ] if classes_names else prediction_category_index
			label      = classes_names[ label_category_index      ] if classes_names else label_category_index

			# Get prediction confidence
			prediction_confidence = prediction_one_hot_category[ prediction_category_index ]
			label_confidence      = prediction_one_hot_category[ label_category_index ]

			# Resize image
			input_image = cv2.resize( input_image, (512, 512) )

			# Check image is RGB
			if depth == 1:
				input_image = cv2.cvtColor( input_image, cv2.COLOR_GRAY2BGR )

			# Draw text
			if good:
				text = '{} ({:05.3f})'.format( label, label_confidence )
			else:
				text = '{}/{} ({:05.3f}/{:05.3f})'.format( prediction, label, prediction_confidence, label_confidence )
			drawings[ i ] = cv2.putText( input_image, text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2, cv2.LINE_AA )

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( ClassificationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'mask',
			   help = 'Name of the feature containing the object foreground mask.'
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle of the object.'
			)
		parser.add_argument(
			'--category_index_feature_name',
			   type = str,
			default = 'category_index',
			   help = 'Name of the feature containing the category index.'
			)
		parser.add_argument(
			'--one_hot_category_feature_name',
			   type = str,
			default = 'one_hot_category',
			   help = 'Name of the feature containing the one-hot category.'
			)
		parser.add_argument(
			'--top',
			   type = int,
			default = 1,
			   help = 'A result is correst if it is amoungst the top-n best scoring classes'
			)
		return parser

	# def get_argument_parser ( cls )

	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories considered in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return []

	# def get_classes_names ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def get_predicted_categories ( cls, predictions, arguments ):
		"""
		Returns the predicted categories indexes.
		
		Arguments:
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.

		Returns:
			`numpy.ndarray`: Predicted categoryies indexes.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		one_hot_categories = predictions[ arguments.one_hot_category_feature_name ]
		return numpy.argpartition( one_hot_categories, -arguments.top )[ :, -arguments.top: ]

	# def get_predicted_categories ( cls, predictions, arguments )

	# --------------------------------------------------

	@classmethod
	def get_label_categories ( cls, labels, arguments ):
		"""
		Returns the labels categories indexes.
		
		Arguments:
			labels (`dict` of `str` to `numpy.ndarray`): Labels.

		Returns:
			`numpy.ndarray`: Labels categoryies indexes.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		one_hot_categories = labels[ arguments.one_hot_category_feature_name ]
		category_indexes   = numpy.argmax( one_hot_categories, axis=-1 )
		return category_indexes

	# def get_label_categories ( cls, predictions, arguments )
	
	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions with shape [batch_size].
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Get label category indexes
		batch_predicted_category_indexes = cls.get_predicted_categories( predictions, arguments )
		batch_label_category_indexes     = cls.get_label_categories( labels, arguments )

		# Create drawings buffer
		results = numpy.empty( [batch_size, 1], numpy.bool )

		# Go through each prediction/label
		for i in range( batch_size ):
				
			# Get one prediction and label
			predicted_category_indexes = batch_predicted_category_indexes[ i ]
			label_category_index       = batch_label_category_indexes[ i ]

			# print( 'lab: {}, preds :{}, probs: {}'.format(
			# 	label_category_index,
			# 	predicted_category_indexes, 
			# 	[float('{:06.4f}'.format(p)) for p in predictions['one_hot_category'][i]]
			# 	) )

			# Save the result
			results[ i ] = bool(label_category_index in predicted_category_indexes)

		# for i in range( batch_size )

		return results

	# def idenfify_correct_predictions ( cls, predictions, labels, arguments )

	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		# Get label category indexes
		label_category_indexes = cls.get_label_categories( labels, arguments )

		# Get predicted confidence values
		predicted_confidences = predictions[ arguments.one_hot_category_feature_name ]

		# Create drawings buffer
		batch_size = predicted_confidences.shape[ 0 ]
		scores     = numpy.empty( [batch_size, 1], numpy.float32 )

		# Go through each prediction/label
		for i in range( batch_size ):
				
			# Get one prediction and label
			label_category_index = label_category_indexes[ i ]

			# Get the prediction confidence
			predicted_confidence = predicted_confidences[ i, label_category_index ]

			# Save the result
			scores[ i ] = predicted_confidence

		# for i in range( batch_size )

		return scores
		
	# def score_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def save_predictions ( cls, predictions, labels, scores, results, directory_path, arguments ):
		"""
		Save predictions made using this experiment.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Predictions for each example.
			labels      (`dict` of `str` to `numpy.ndarray`): Labels for each example.
			results                        (`numpy.ndarray`): Result foreach example.
			directory_path    (`pytools.path.DirectoryPath`): Path of the directory where to save the predictions.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( results, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		def ravel_feature ( name, features_batch ):
			rank = len( features_batch.shape )	
			if rank == 1:
				return { name: features_batch }
			batch_size     = features_batch.shape[ 0 ]
			features_batch = numpy.reshape( features_batch, [batch_size, -1] )
			nb_values      = features_batch.shape[ 1 ]
			new_data       = {}
			for i in range(nb_values):
				new_name = '{}_{}'.format( name, i )
				new_data[ new_name ] = features_batch[ :, i ]
			return new_data

		# File where to save the predictions
		filepath = directory_path + pytools.path.FilePath( 'predictions.csv' )

		# Get classes names
		classes_names = cls.get_classes_names( arguments )
		nb_classes    = len(classes_names)

		# Get one hot caterory (batch = for each example)
		batch_prediction_one_hot_category = predictions[ arguments.one_hot_category_feature_name ]
		batch_label_one_hot_category      = labels[ arguments.one_hot_category_feature_name ]

		# Get caterory index
		batch_prediction_category_index = numpy.argmax( batch_prediction_one_hot_category, axis=-1 )
		batch_label_category_index      = numpy.argmax( batch_label_one_hot_category, axis=-1 )

		# Replace index by class name
		classes_names = cls.get_classes_names( arguments )
		batch_prediction_category_name = [ classes_names[i] for i in batch_prediction_category_index ]
		batch_label_category_name      = [ classes_names[i] for i in batch_label_category_index      ]

		# Get confidence index
		nb_examples = batch_prediction_category_index.shape[ 0 ]
		batch_prediction_confidence = numpy.empty( nb_examples, dtype=numpy.float32 )
		for i in range(nb_examples):
			category_index = batch_prediction_category_index[ i ]
			batch_prediction_confidence[ i ] = batch_prediction_one_hot_category[ i, category_index ]

		# Tabular to save by columns
		data = {
			'Prediction': batch_prediction_category_name,
			'Label': batch_label_category_name,
			'Confidence': batch_prediction_confidence.ravel(),
			'Result': results.ravel(),
		}

		# Convert predictions to a pandas DataFrame
		predictions = pandas.DataFrame.from_dict( data )

		# Save it as csv
		predictions.to_csv( str(filepath), float_format='%05.4f', index=True, index_label='index' )

	# def save_predictions ( cls, ... )
	
# class ClassificationExperiment ( Experiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return ClassificationExperiment

# def get_experiment ()