# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
from tensorflow import keras

# INTERNALS
import pydataset
import pytools

# LOCALS
from .experiment import Experiment

# ##################################################
# ###       CLASS SEGMENTATION-EXPERIMENT        ###
# ##################################################

class SegmentationExperiment ( Experiment ):
	"""
	Base class for segmentation experiments.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		def map_value ( value, a, b, A, B ):
			# Figure out how 'wide' each range is
			s = b - a
			S = B - A
			# Convert the left range into a 0-1 range (float)
			value = float( value - a ) / float( s )
			# Convert the 0-1 range into a value in the right range.
			return A + (value * S)

		# Get features
		inputs_images = inputs[ arguments.image_feature_name ]
		
		# Create drawings buffer
		shape    = list(inputs_images.shape)
		batch    = shape[ 0 ]
		height   = shape[ 1 ]
		width    = shape[ 2 ]
		depth    = shape[ 3 ]
		drawings = numpy.zeros( [batch, height, width, depth], inputs_images.dtype )

		# Draw each images
		for image_index in range( batch_size ):
			
			# Get the current image
			image        = inputs_images[ image_index ].copy().astype(numpy.float32)
			image_height = image.shape[ 0 ]
			image_width  = image.shape[ 1 ]

			# Get the predictions
			pred        = cls.get_predictions_segmentation( image_index, predictions, arguments ).astype( numpy.float32 )
			pred_height = pred.shape[ 0 ]
			pred_width  = pred.shape[ 1 ]

			# Thresholf predictions
			pred[ pred < 0.5 ] = 0.
			pred[ pred >= 0.5 ] = 1.
			pred = pred.astype( numpy.bool )

			# Get the labels
			true = cls.get_labels_segmentation( image_index, labels, arguments ).astype( numpy.float32 )
			true_height = pred.shape[ 0 ]
			true_width  = pred.shape[ 1 ]
			
			# For each category
			number_of_categories = true.shape[ 2 ]
			for category_index in range( 1, number_of_categories ): # ignore backbround

				# Index of the color used for this category in a 20 colors colormap
				color_index = int(map_value( category_index, 1, number_of_categories, 1., 19. ))

				# Get the label mask
				true_color = pydataset.data.ColorData.PREDEFINED_22[ color_index-1 ].to_tuple( False )
				true_mask  = true[ :, :, category_index]

				# Get the predicted mask
				pred_color = pydataset.data.ColorData.PREDEFINED_22[ color_index+1 ].to_array( False )
				pred_mask  = pred[ :, :, category_index]

				if numpy.sum( true_mask ) > 0:
					
					# Draw label contours
					true_mask         = numpy.multiply( true_mask, 255.).astype( numpy.uint8 )
					true_mask         = cv2.resize( true_mask, (image_width, image_height) )
					true_countours, _ = cv2.findContours( true_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE )
					cv2.drawContours( image, true_countours, -1, true_color, 2 )

					# Draw predicted contours
					pred_mask = numpy.reshape( pred_mask, [image_height, image_width, 1] )
					pred_mask = numpy.tile( pred_mask, [1, 1, 3] )
					pred_color = numpy.reshape( pred_color, [1, 1, 3] )
					pred_color = numpy.tile( pred_color, [image_height, image_width, 1] ).astype(numpy.float32)
					image = numpy.where( pred_mask, pred_color*1.0+image*0.0, image )

					# pred_mask         = numpy.multiply( pred_mask, 255.).astype( numpy.uint8 )
					# pred_mask         = cv2.resize( pred_mask, (image_width, image_height) )
					# pred_countours, _ = cv2.findContours( pred_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE )
					# cv2.drawContours( image, pred_countours, -1, pred_color, 3 )
					
			# Save the drawing
			drawings[ image_index ] = image.astype(numpy.uint8)

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( SegmentationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to segment.'
			)
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'mask',
			   help = 'Name of the feature containing the mask',
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle.',
			)
		parser.add_argument(
			'--segmentation_feature_name',
			   type = str,
			default = 'segmentation',
			   help = 'Name of the feature containing the label segmenation.'
			)
		parser.add_argument(
			'--segmentation_weights_feature_name',
			   type = str,
			default = 'segmentation_weights',
			   help = 'Name of the feature containing the segmenation weights.'
			)
		parser.add_argument(
			'--dedicated_background',
			 action = 'store_true',
			   help = 'If set (`True`), background is segmented separatly.'
			)
		return parser

	# def get_argument_parser ( cls )
		
	# --------------------------------------------------

	@classmethod
	def get_labels_segmentation ( cls, image_index, labels, arguments ):
		"""
		Returns the segmentation corresponding to the given labels in the image with the given index.
		
		Arguments:
			cls                                (`type`): Experiment class type.
			image_index                         (`int`): Index of the image.
			labels (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments            (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets segmentation.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		return labels[ arguments.segmentation_feature_name ][ image_index ]

	# def get_labels_segmentation ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_predictions_segmentation ( cls, image_index, predictions, arguments ):
		"""
		Returns the segmentation corresponding to the given predictions in the image with the given index.
		
		
		Arguments:
			cls                                     (`type`): Experiment class type.
			image_index                              (`int`): Index of the image.
			predictions (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments                 (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets segmentation.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		return predictions[ arguments.segmentation_feature_name ][ image_index ]

	# def get_predictions_segmentation ( cls, ... )
	
	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# By default scores are mean IoU
		return numpy.greater_equal( scores, 0.5 )
	
	# def idenfify_correct_predictions ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def save_predictions ( cls, predictions, labels, scores, results, directory_path, arguments ):
		"""
		Save predictions made using this experiment.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Predictions for each example.
			labels      (`dict` of `str` to `numpy.ndarray`): Labels for each example.
			results                        (`numpy.ndarray`): Result foreach example.
			directory_path    (`pytools.path.DirectoryPath`): Path of the directory where to save the predictions.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( results, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# we dont do that here

	# save_predictions ( cls, predictions, labels, scores, results, directory_path, arguments )

	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		By default this metric returns the mean IoU between ground trouth and predictions

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create scores buffer
		scores = numpy.zeros( [batch_size], numpy.float32 )

		# Go though each pair of prediction/label in the bacth
		for batch_index in range( batch_size ):

			# Get the ground trouth bounding segmentation
			label      = cls.get_labels_segmentation( batch_index, labels, arguments ).astype( numpy.bool )
			label_area = numpy.sum( label.astype(numpy.int32), axis=None, dtype=numpy.float32 ) / float(label.size)

			# Get the predicted bounding segmentation
			predi      = cls.get_predictions_segmentation( batch_index, predictions, arguments ).astype( numpy.bool )
			predi_area = numpy.sum( predi.astype(numpy.int32), axis=None, dtype=numpy.float32 ) / float(predi.size)
			
			# Compute pixel IoU
			inter      = numpy.logical_and( label, predi )
			inter_area = numpy.sum( inter.astype(numpy.int32), axis=None, dtype=numpy.float32 ) / float(inter.size)
			
			
			iou = inter_area / ( label_area + predi_area - inter_area + numpy.finfo(numpy.float32).eps )
			
			# Save score
			scores[ batch_index ] = iou

		return scores

	# def score_predictions ( cls, ... )

# class SegmentationExperiment ( Experiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return SegmentationExperiment

# def get_experiment ()