# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###           CLASS MODEL-CHECKPOINT           ###
# ##################################################

class ModelCheckpoint ( keras.callbacks.ModelCheckpoint ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, checkpoints_directory, filename_format='model-{epoch:04d}-{loss:010.6f}.keras', save_freq='epoch', verbose=1, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.callbacks.ModelCheckpoint` class.

		Arguments:
			self           (`pykeras.callbacks.ModelCheckpoint`): Instance to initialize.
			checkpoints_directory (`pytools.path.DirectoryPath`): Path of the directory where to save the checkpoints.
			filename_format                              (`str`): Checkpoints filename template.
		"""
		assert pytools.assertions.type_is_instance_of( self, ModelCheckpoint )
		assert pytools.assertions.type_is_instance_of( checkpoints_directory, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( filename_format, str )
		assert pytools.assertions.type_in( save_freq, (str, int) )
		assert pytools.assertions.type_is( verbose, int )

		self.checkpoints_directory = checkpoints_directory
		self.filename_format       = filename_format

		if not self.checkpoints_directory.exists():
			self.checkpoints_directory.create()

		super( ModelCheckpoint, self ).__init__(
			str( self.checkpoints_directory + pytools.path.FilePath(filename_format) ),
			save_freq = save_freq,
			verbose = verbose,
			**kwargs
			)

	# def __init__ ( self, checkpoints_directory, filename_format )

# class ModelCheckpoint ( keras.callbacks.ModelCheckpoint )