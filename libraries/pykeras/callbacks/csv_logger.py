# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###              CLASS CSV-LOGGER              ###
# ##################################################

class CSVLogger ( keras.callbacks.CSVLogger ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, log_directory, filename='metrics.csv', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.callbacks.CSVLogger` class.

		Arguments:
			self         (`pykeras.callbacks.CSVLogger`): Instance to initialize.
			log_directory (`pytools.path.DirectoryPath`): Path of the directory where to save the csv log.
			filename                             (`str`): CSV filename.
		"""
		assert pytools.assertions.type_is_instance_of( self, CSVLogger )
		assert pytools.assertions.type_is_instance_of( log_directory, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is_instance_of( filename, (pytools.path.FilePath, str) )

		self.log_directory = log_directory
		self.filename      = pytools.path.FilePath(filename) if isinstance(filename, str) else filename

		if not self.log_directory.exists():
			self.log_directory.create()

		super( CSVLogger, self ).__init__(
			str( self.log_directory + self.filename ),
			**kwargs
			)

	# def __init__ ( self, checkpoints_directory, filename_format )

# class CSVLogger ( keras.callbacks.CSVLogger )