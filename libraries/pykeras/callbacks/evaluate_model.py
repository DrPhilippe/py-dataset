# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import sys
from tensorflow import keras

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###            CLASS EVALUATE-MODEL            ###
# ##################################################

class EvaluateModel ( keras.callbacks.Callback ):
	"""
	https://stackoverflow.com/questions/47676248/accessing-validation-data-within-a-custom-callback
	"""

	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, validation_data, **kwargs ):
		super( EvaluateModel, self ).__init__( **kwargs )
		self._validation_data = validation_data

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	def set_model( self, model ):
		self._model = model

	# --------------------------------------------------
	
	def on_epoch_end ( self, epoch, logs={} ):
		
		sys.stdout.write( 'Epoch {:05d}: evaluating model ... '.format(epoch+1) )
		
		for metric in self._model.metrics:
			metric.reset_states()
			
		val_logs = self._model.evaluate( self._validation_data, return_dict=True )
		
		val_logs = {'val_' + name: val for name, val in val_logs.items()}
		logs.update( val_logs )

		sys.stdout.write( 'done!\n' )

		return logs

	# def on_epoch_end( self, epoch, logs )

	# def on_epoch_end ( self, epoch, logs={} ):

	# 	sys.stdout.write( 'Epoch {:05d}: evaluating model:\n'.format(epoch+1) )

	# 	inputs_names  = [input_layer.name for input_layer in self._model.inputs]
	# 	outputs_names = [output_layer.name for output_layer in self._model.outputs]

	# 	# Create local copies of the model metrics
	# 	number_of_metrics = len( self._model.metrics )
	# 	metrics           = []
	# 	metrics_names     = []
	# 	metrics_results   = []

	# 	for i in range( number_of_metrics ):
	# 		metric      = self._model.metrics[ i ]
	# 		metric_name = self._model.metrics_names[ i ]

	# 		if isinstance( metric, keras.metrics.Metric ):
	# 			metric.reset_states()
			
	# 		metrics.append( metric )
	# 		metrics_names.append( metric_name )
	# 		metrics_results.append( 0.0 )

	# 	sys.stdout.write( '    found metrics: {}'.format(','.join(metrics_names)) )

	# 	# For each validation batch
	# 	number_of_batch = len(self.validation_data)
	# 	for batch_index in range( 0, number_of_batch ):
			
	# 		# Get the batch target values
	# 		x      = self.validation_data[ batch_index ][ 0 ][ 'image' ]
	# 		y_true = self.validation_data[ batch_index ][ 1 ][ 'one_hot_digit' ]

	# 		print( 'x', x.dtype, x.shape )
	# 		print( 'y_true', y_true.dtype, y_true.shape )

	# 		# Get the batch prediction values
	# 		y_pred = numpy.asarray( self._model.predict( x ) )

	# 		# Update metrics
	# 		for i in range( number_of_metrics ):
	# 			print( 'metric:', metrics[ i ] )
	# 			metrics_results[ i ] = metrics[ i ]( y_true, y_pred )

	# 	# for batch_index in range( 0, number_of_batch )
		
	# 	# Log metrics values
	# 	for i in range( number_of_metrics ):
	# 		metric_name   = metrics_names[ i ]
	# 		metric_result = metrics_results[ i ]
	# 		sys.stdout.write( '    {}: {}\n'.format(metric_name, metric_result) )

	# 	sys.stdout.write( 'Epoch {:05d}: evaluating model done!\n'.format(epoch+1) )

	# 	return logs

	# # def on_epoch_end( self, epoch, logs )

# class EvaluateModel ( keras.callbacks.Callback )