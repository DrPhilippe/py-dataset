# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

import sys
import tensorflow
from tensorflow import keras

# ##################################################
# ###         CLASS LEARNING-RATE-LOGGER         ###
# ##################################################

class LearningRateLogger ( keras.callbacks.Callback ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self ):
		super().__init__()
		self._supports_tf_logs = True

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self, epoch, logs=None ):

		if logs is None or 'learning_rate' in logs:
			return
		
		lr = self.model.optimizer._decayed_lr( tensorflow.float32 )
		lr = keras.backend.eval( lr )

		logs['learning_rate'] = lr

		return logs

	# def on_epoch_end( self, epoch, logs=None )

	# --------------------------------------------------
	
	def on_train_batch_end ( self, batch, logs=None ):

		if logs is None or 'learning_rate' in logs:
			return

		lr = self.model.optimizer._decayed_lr( tensorflow.float32 )
		lr = keras.backend.eval( lr )

		logs['learning_rate'] = lr

		return logs
		
	# def on_train_batch_end ( self, batch, logs ):

# class LearningRateLogger ( keras.callbacks.Callback )