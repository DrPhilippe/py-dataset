# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###             CLASS TENSOR-BOARD             ###
# ##################################################

class TensorBoard ( keras.callbacks.TensorBoard ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, tensorboard_directory, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.callbacks.TensorBoard` class.

		Arguments:
			self               (`pykeras.callbacks.TensorBoard`): Instance to initialize.
			tensorboard_directory (`pytools.path.DirectoryPath`): Path of the directory where to save the tensorboard log.
		"""
		assert pytools.assertions.type_is_instance_of( self, TensorBoard )
		assert pytools.assertions.type_is_instance_of( tensorboard_directory, pytools.path.DirectoryPath )

		super( TensorBoard, self ).__init__(
			str( tensorboard_directory ),
			**kwargs
			)

	# def __init__ ( self, tensorboard_directory )

# class TensorBoard ( keras.callbacks.TensorBoard )