# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
from tensorflow import keras

# INTERNALS
import pykeras.metrics
import pytools.assertions
import pytools.path

# ##################################################
# ###        CLASS METRICS-SAVER        ###
# ##################################################

class MetricsSaver ( keras.callbacks.Callback ):
	"""
	Callback used to save the value of metrics.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, metrics_directory, log_weights=True ):
		"""
		Initializes a new instance of the `pykeras.callbacks.MetricsSaver` class.

		Aruguments:
			self        (`pykeras.callbacks.MetricsSaver`): Instance to initialize.
			metrics_directory (`pytools.path.DirectoryPath`/`str`): Path of the directory where to save the metrics.
			log_weights                                   (`bool`): If `True` also saves the metrics weights (non-trainable ones)
		"""
		assert pytools.assertions.type_is_instance_of( self, MetricsSaver )
		assert pytools.assertions.type_is_instance_of( metrics_directory, (pytools.path.DirectoryPath, str) )

		# Initialize fields
		if isinstance( metrics_directory, str ):
			self.metrics_directory = pytools.path.DirectoryPath(metrics_directory)
		else:	
			self.metrics_directory = metrics_directory
		self.log_weights = log_weights

		# Create destination directory if it doesnt exist
		if not self.metrics_directory.exists():
			self.metrics_directory.create()

	# def __init__ ( self, metrics_directory )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def on_epoch_end ( self, epoch, logs ):
		"""
		Method invoked during training at the end of epochs.

		Arguments:
			self   (`pykeras.callbacks.MetricsSaver`): Metrics saver callback instance.
			epoch                                     (`int`): Zero based index of the epoch ending.
			logs (`dict` of `str` to `float`/`numpy.ndarray`): Logged values indexed by names.

		"""
		assert pytools.assertions.type_is_instance_of( self, MetricsSaver )
		assert pytools.assertions.type_is( epoch, int )
		assert pytools.assertions.type_is( logs, dict )
		
		# At the end of the epoch
		epoch += 1 

		# Save each metric
		for metric in self.model.metrics:

			if isinstance( metric, keras.metrics.Metric ):
							
				self.save_metric( metric, epoch )
			
			# if isinstance( metric, keras.metrics.Metric )

		# for metric in self.model.metrics:

		return logs
	
	# def on_epoch_end ( self, epoch, logs )
	
	# --------------------------------------------------

	def save_metric ( self, metric, epoch ):
		"""
		Saves the current value of the given metric.

		Arguments:
			self (`pykeras.callbacks.MetricsSaver`): Metrics saver callback instance.
			metric       (`pykeras.metrics.Metric`): Metric to save.
			epoch                           (`int`): Epoch index.
		"""
		assert pytools.assertions.type_is_instance_of( self, MetricsSaver )
		assert pytools.assertions.type_is_instance_of( metric, keras.metrics.Metric )
		assert pytools.assertions.type_is( epoch, int )

		# Get the value of the metric
		headers = ['value']
		value = keras.backend.get_value( metric.result() )
		value = numpy.asarray( value )
		value = value.astype( numpy.float32 )
		shape = value.shape

		# Save weights ?
		if self.log_weights:

			for weight in metric.non_trainable_weights:
				
				# Get the weight value
				weight_value = keras.backend.get_value( weight )
				weight_value = numpy.asarray( weight_value )
				weight_value = weight_value.astype( numpy.float32 )

				# Ignore metrics whoes wheit are not the same shape as the value
				# Because I dont want to deal with it
				if weight_value.shape == shape:
					
					# Get the weight name and remove keras naming shit
					weight_name = weight.name
					weight_name = weight_name[ 0:weight_name.find(':') ]
					
					# Add the value to the value table
					value = numpy.hstack( [value, weight_value] )
					
					# Add the weight name to the headers
					headers.append( weight_name )
				
				# if weight_value.shape == shape

			# for weight in metric.non_trainable_weights:

		# if self.log_weights

		# If the array's shape is 0, 1 or 2
		# Then it can be layed out flat in a text file
		if len(value.shape) < 3:
			
			# Get the path of the file where to save the metric
			filepath = self.metrics_directory + pytools.path.FilePath.format( '{}-{:04d}.txt', metric.name, epoch )

			# If the value is a single value, reshape it
			if list(value.shape) == []:
				value = numpy.reshape( value, [1] )

			# Save the value
			numpy.savetxt( str(filepath), value, fmt='%09.6f', delimiter=', ', newline='\n', header=', '.join(headers) )

		# Otherwize save it using numpy
		else:
			# Get the path of the file where to save the metric
			filepath = self.metrics_directory + pytools.path.FilePath.format( '{}-{:04d}.npy', metric.name, epoch )

			# Save the value
			numpy.save( str(filepath), metric, epoch )

		# if len(value.shape) < 3

	# def save_metric ( self, directory_path )

# class MetricsSaver ( keras.callbacks.Callback ):