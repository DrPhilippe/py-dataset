# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def conv_block ( tensor, numbers_of_filters, name_or_index, include_batch_normalization=True ):
	"""
	Creates a Darknet19 convolutional block.
	
	A convolutional and a batch normalization layer are created for every entry in `numbers_of_filters`.
	This number must be odd as the kernel size of the convultion will alternate between 3x3 and 1x1.

	Arguments:
		tensor          (`tensorflow.Tensor`): Input tensor.
		numbers_of_filters (`list` of `int`s): Number of filters of convolutions layers to create.
		name_or_index           (`str`/`int`): Name or index of the block.
		include_batch_normalization  (`bool`): If `True` batch normalization layers are added after every convolutional layers.
	
	Retuns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( numbers_of_filters, list )
	assert pytools.assertions.list_items_type_is( numbers_of_filters, int )
	assert pytools.assertions.type_in( name_or_index, (int, str) )
	assert pytools.assertions.type_is( include_batch_normalization, bool )

	# Block name
	if isinstance( name_or_index, int ):
		name_or_index = 'block{}'.format( name_or_index )

	# Create convolution layers and batch normalization layers
	number_of_convolutions = len( numbers_of_filters )
	for convolution_index in range( number_of_convolutions ):
		
		# Get the number of filters
		number_of_filters = numbers_of_filters[ convolution_index ]

		# Kernel size
		if (convolution_index % 2) == 0:
			kernel_size = (3, 3)
		else:
			kernel_size = (1, 1)

		# Create the convultion layer and apply it
		tensor = keras.layers.Conv2D(
			    filters = number_of_filters,
			kernel_size = kernel_size,
			    strides = (1, 1),
			    padding = 'same',
			 activation = keras.activations.relu,
			       name = '{}_conv{}'.format( name_or_index, convolution_index )
			)( tensor )
		
		# If batch normalization must be included in the net architecture
		if include_batch_normalization:
			
			# Create the batch normalization layer and apply it
			tensor = keras.layers.BatchNormalization(
				name='{}_norm{}'.format( name_or_index, convolution_index )
				)( tensor )
		
		# if include_batch_normalization

	# for convolution_index in range( number_of_convolutions )

	# Create the max pooling layer and apply it
	return keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}_pool'.format( name_or_index )
		)( tensor )

# def conv_block ( tensor, numbers_of_filters, name_or_index, include_batch_normalization=True )
	
# --------------------------------------------------

def backbone ( tensor, include_batch_normalization=True ):
	"""
	Backbone of Darknet19.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		include_batch_normalization (`bool`): If `True` batch normalization layers are added after every convolutional layers.
	
	Retuns:
		`tensorflow.Tensor`: Output tensor.
	"""

	# Convolutional blocks
	tensor = conv_block( tensor, [32], 1, include_batch_normalization )
	tensor = conv_block( tensor, [64], 2, include_batch_normalization )
	tensor = conv_block( tensor, [128, 64, 128], 3, include_batch_normalization )
	tensor = conv_block( tensor, [256, 128, 256], 4, include_batch_normalization )
	tensor = conv_block( tensor, [512, 256, 512, 256, 512], 5, include_batch_normalization )
	tensor = conv_block( tensor, [1024, 512, 1024, 512, 1024], 5, include_batch_normalization )
	return tensor

# def backbone ( inputs )

# --------------------------------------------------

def head ( tensor, classes=1000, classifier_activation='softmax' ):
	"""
	Head of Darknet19.

	Arguments:
		tensor         (`tensorflow.Tensor`): input tensor.	
		classes                      (`int`): Number of classes.
		classifier_activation (`str`/`None`): Activation of the last layer.

	Retuns:
		`tensorflow.Tensor`: Output tensor with shape [BATCH, 1, 1, CLASSES].
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )

	tensor = keras.layers.Conv2D(
		    filters = classes,
		kernel_size = (1, 1),
		    strides = (1, 1),
		    padding = 'same',
		 activation = classifier_activation,
		       name = 'head_conv'
		)( tensor )
	tensor = keras.layers.GlobalAveragePooling2D(		    
		name = 'head_pool'
		)( tensor )
	return tensor

# def head ( tensor, classes, classifier_activation )
		
# --------------------------------------------------

def Darknet19 (
	include_top=True, include_batch_normalization=True,
	input_tensor=None, input_shape=(224, 224, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='one_hot_category'
	):
	"""
	Create a new instance of the Darknet19 model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )
	
	# Create the backbone
	tensor = backbone( image )

	# Create classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )

	# Rename output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	return keras.models.Model(
		    inputs = {input_name: image},
		   outputs = {output_name: tensor},
		      name = 'darknet19'
		)

# def Darknet19 ( ... )
