# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from . import vgg19net
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def BB8NetV1 (
	include_lrn=True, include_top=True,
	input_tensor=None, input_shape=(227, 227, 3), input_dtype='float32', input_name='image',
	classifier_activation='linear',
	output_shape=(11, 8, 2), output_name='bb8_v2',
	checkpoint=''
	):
	"""
	Create a new instance of the BB8NetV2 model.

	Output shape can be a set of 2D or 3D control points: `(?, 2)` ou `(?, 3)`.
	
	Arguments:
		include_lrn                (`bool`): If `True` local response nomalization is applied in the first two blocks.
		include_top                (`bool`): If `False` the classification layers are omitted.
		input_tensor      (`tensor`/`None`): Optional input tensor.
		input_shape (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                 (`str`): Data type of the input layer.
		input_name                  (`str`): Name of the input layer to create.
		classifier_activation(`str`/`None`): Activation of the last dense layer.
		output_shape(`tuple` of `3` `int`s): Shape of the predicted contol points.
		output_name                 (`str`): Name of the output layer.
		checkpoint                  (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: BB8NetV2 keras model.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_shape, tuple )
	assert pytools.assertions.equal( len(output_shape), 3 )
	assert pytools.assertions.value_in( output_shape[-1], [2, 3] )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Compute the number of units
	# nb control points sets x nb points in sets x points dimensions
	units = output_shape[0] * output_shape[1] * output_shape[2]

	# Create input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Backbone
	tensor = vgg19net.backbone( image, name='backbone' )
		
	# Top
	if include_top:
		tensor = vgg19net.head( tensor, units, classifier_activation, name='head' )
		tensor = keras.layers.Reshape(
			target_shape = output_shape,
			 input_shape = (units,),
			        name = 'reshape'
			)( tensor )

	# Rename
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'bb8net_v2'
		)
	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def BB8NetV2 ( ... )