# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def backbone ( tensor, name='backbone' ):
	"""
	Backbone of LeNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		name                 (`str`): Name of the backbone.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( name, str )

	# Block 1
	tensor = keras.layers.Conv2D(
		    filters = 32,
		kernel_size = (5, 5),
		    strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block1/conv'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block1/pool'.format( name )
		)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 64,
		kernel_size = (5, 5),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block2/pool'.format( name )
		)( tensor )

	return tensor

# def encoder ( tensor )

# --------------------------------------------------

def head ( tensor, classes=10, classifier_activation='softmax', name='head' ):
	"""
	Head of LeNet.

	Arguments:		
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the head.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 1024,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.4,
		name = '{}/dropout'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = classes,
		activation = classifier_activation,
		      name = '{}/fc2'.format( name )
		)( tensor )

	return tensor

# def head ( tensor, classes, classifier_activation )

# --------------------------------------------------

def LeNet (
	include_top=True,
	input_tensor=None, input_shape=(28,28,1), input_dtype='float32', input_name='image',
	classes=10, classifier_activation='softmax', output_name='one_hot_digit',
	checkpoint=''
	):
	"""
	Create a new instance of the LeNet model.

	Arguments:
		include_top  (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: GoogLeNet keras model.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor = backbone( image, name='backbone' )

	# Create the head
	if include_top:
		tensor = head( tensor, classes, classifier_activation, name='head' )
	
	# Rename the output
	logits = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = {input_name: image},
		   outputs = {output_name: logits},
		      name = 'lenet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def LenNet ( ... )
