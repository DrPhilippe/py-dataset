# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def backbone ( tensor, name='backbone' ):
	"""
	Backbone of VGG19.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		name                 (`str`): Name of the backbone.

	Returns
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( name, str )

	# Block 1
	tensor = keras.layers.Conv2D(
		    filters = 64,
		kernel_size = (3, 3),
		    strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block1/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 64,
		kernel_size = (3, 3),
		    strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block1/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block1/pool'.format( name )
		)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 128,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 128,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block2/pool'.format( name )
		)( tensor )

	# Block 3
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv3'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv4'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block3/pool'.format( name )
		)( tensor )

	# Block 4
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block4/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block4/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block4/conv3'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block4/conv4'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block4/pool'.format( name )
		)( tensor )

	# Block 5
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block5/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block5/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block5/conv3'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 512,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block5/conv4'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPool2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block5/pool'.format( name )
		)( tensor )
	return tensor

# def backbone ( inputs )

# --------------------------------------------------

def head ( tensor, classes=1000, classifier_activation='softmax', name='head' ):
	"""
	Head of VGG19.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the head.

	Returns
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	# assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = '{}/drop1'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.activations.relu,
		      name = '{}/fc2'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = '{}/drop2'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = classes,
		activation = classifier_activation,
		      name = '{}/fc3'.format( name )
		)( tensor )
	return tensor

# def head ( tensor, classes, classifier_activation )
		
# --------------------------------------------------

def VGG19Net (
	include_top=True,
	input_tensor=None, input_shape=(227, 227, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='one_hot_category',
	checkpoint=''
	):
	"""
	Create a new instance of the VGG19 model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	# assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )
	
	# Create the backbone
	tensor = backbone( image )

	# Create classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )

	# Rename output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.models.Model(
		    inputs = {input_name: image},
		   outputs = {output_name: tensor},
		      name = 'vgg19'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def VGG19Net ( ... )
