# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from . import alexnet
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def contraction_block ( tensor, number_of_filters, kernel_initializer, name_or_index, trainable=True ):
	"""
	Contraction block of UNet.
	
	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		number_of_filters    (`int`): Number of filters in the two convolutional layers.
		kernel_initializer   (`str`): Keras kernel initializer name.
		name_or_index  (`str`/`int`): Name or index of the block.
		trainable           (`bool`): Is the block trainable.

	Returns:
		tensor   (`tensorflow.Tensor`): Output tensor after max-pooling.
		residual (`tensorflow.Tensor`): Output residual, not yet cropped.
	"""
	assert pytools.assertions.type_is( number_of_filters, int )
	assert pytools.assertions.type_is( kernel_initializer, str )
	assert pytools.assertions.type_in( name_or_index, (str, int) )
	assert pytools.assertions.type_is( trainable, bool )

	if isinstance( name_or_index, int ):
		name_or_index = 'down{}'.format( name_or_index )

	# Convolution 1
	tensor = keras.layers.Conv2D(
		           filters = number_of_filters,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv1'.format( name_or_index ),
		         trainable = trainable
		)( tensor )

	# Convolution 2, where the residual will be cropped
	residual = keras.layers.Conv2D(
		           filters = number_of_filters,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv2'.format( name_or_index ),
		         trainable = trainable
		)( tensor )

	# Max pooling
	tensor = keras.layers.MaxPooling2D(
		pool_size = (2, 2),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/pool'.format( name_or_index ),
		trainable = trainable
		)( residual )

	# Fin
	return tensor, residual

# def contraction_block ( ... )

# --------------------------------------------------

def expansion_block ( tensor, residual, number_of_filters, kernel_initializer, name_or_index, trainable=True ):
	"""
	Expansion block of UNet.
	
	Arguments:
		tensor   (`tensorflow.Tensor`): Input tensor.
		residual (`tensorflow.Tensor`): Input residual.
		number_of_filters      (`int`): Number of filters in the two convolutional layers.
		kernel_initializer     (`str`): Keras kernel initializer name.
		name_or_index    (`str`/`int`): Name or index of the block.
		trainable             (`bool`): Is the block trainable.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( number_of_filters, int )
	assert pytools.assertions.type_in( name_or_index, (str, int) )
	assert pytools.assertions.type_is( trainable, bool )

	if isinstance( name_or_index, int ):
		name_or_index = 'up{}'.format( name_or_index )
	
	# Upsample previous tensor
	# tensor = keras.layers.UpSampling2D(
	# 	size = (2, 2),
	# 	name = '{}/up'.format( name_or_index )
	# 	)( tensor )

	# # Up Convolution
	# tensor = keras.layers.Conv2D(
	# 	           filters = number_of_filters, 
	# 	       kernel_size = (2, 2),
	# 	kernel_initializer = kernel_initializer,
	# 	           strides = (1, 1),
	# 	           padding = 'same',
	# 	        activation = keras.activations.relu,
	# 	              name = '{}/conv1'.format( name_or_index )
	# 	)( tensor )

	# Deconvolution
	tensor = keras.layers.Conv2DTranspose(
		    filters = number_of_filters,
		kernel_size = (3, 3),
		    strides = (2, 2),
		    padding = 'same',
		       name = '{}/deconv'.format( name_or_index ),
		  trainable = trainable
		)( tensor )

	# # Crop residual
	# residual = pykeras.layers.CropLike2D(
	# 	    name = '{}/crop'.format( name_or_index )
	# 	)( residual, tensor ) # second tensor is cropping referenc

	# Concatenate upsampling and residual
	tensor = keras.layers.Concatenate(
		     axis = -1,
		     name = '{}/concat'.format( name_or_index ),
		trainable = trainable
		)([ residual, tensor ])

	# First convolution
	tensor = keras.layers.Conv2D(
		           filters = number_of_filters,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv2'.format( name_or_index ),
		         trainable = trainable
		)( tensor )
	
	# Second convolution
	return keras.layers.Conv2D(
		           filters = number_of_filters,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv3'.format( name_or_index ),
		         trainable = trainable
		)( tensor )

# def expansion_block ( ... )

# --------------------------------------------------

def encoder ( tensor, kernel_initializer, name='encoder', trainable=True ):
	"""
	Encoder of UNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		kernel_initializer   (`str`): Keras kernel initializer name.
		name                 (`str`): Name of the encoder.
		trainable           (`bool`): Is the encoder trainable.

	Returns:
		residual_1 (`tensorflow.Tensor`): First Residual (largest).
		residual_2 (`tensorflow.Tensor`): Second residual.
		residual_3 (`tensorflow.Tensor`): Third residual.
		residual_4 (`tensorflow.Tensor`): Fourth residual.
		output     (`tensorflow.Tensor`): Output tensor.
	"""
	assert pytools.assertions.type_is( kernel_initializer, str )
	assert pytools.assertions.type_is( name, str )
	assert pytools.assertions.type_is( trainable, bool )

	tensor, residual_1 = contraction_block( tensor,   64, kernel_initializer, '{}/down1'.format(name), trainable )
	tensor, residual_2 = contraction_block( tensor,  128, kernel_initializer, '{}/down2'.format(name), trainable )
	tensor, residual_3 = contraction_block( tensor,  256, kernel_initializer, '{}/down3'.format(name), trainable )
	tensor, residual_4 = contraction_block( tensor,  512, kernel_initializer, '{}/down4'.format(name), trainable )
	return residual_1, residual_2, residual_3, residual_4, tensor

# def encoder ( ... )

# --------------------------------------------------

def middle ( tensor, kernel_initializer, name='middle', trainable=True ):
	"""
	Middle bone of UNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		kernel_initializer   (`str`): Keras kernel initializer name.
		name                 (`str`): Name of the middle.
		trainable           (`bool`): Is the middle trainable.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( kernel_initializer, str )
	assert pytools.assertions.type_is( name, str )
	assert pytools.assertions.type_is( trainable, bool )

	# Middle path
	tensor = keras.layers.Conv2D(
		           filters = 1024,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv1'.format( name ),
		         trainable = trainable
		)( tensor )
	tensor = keras.layers.Dropout(
		     rate = 0.5,
		     name = '{}/drop1'.format( name ),
		trainable = trainable
		)( tensor )
	tensor = keras.layers.Conv2D(
		           filters = 1024,
		       kernel_size = (3, 3),
		kernel_initializer = kernel_initializer,
		           strides = (1, 1),
		           padding = 'same',
		        activation = keras.activations.relu,
		              name = '{}/conv2'.format( name ),
		         trainable = trainable
		)( tensor )
	tensor = keras.layers.Dropout(
		     rate = 0.5,
		     name = '{}/drop2'.format( name ),
		trainable = trainable
		)( tensor )
	return tensor

# def middle ( ... )

# --------------------------------------------------

def decoder ( tensor, residual_1, residual_2, residual_3, residual_4, kernel_initializer, name='decoder', trainable=True ):
	"""
	Decoder of UNet.

	Arguments:
		tensor     (`tensorflow.Tensor`): Input tensor.
		residual_1 (`tensorflow.Tensor`): First Residual (largest).
		residual_2 (`tensorflow.Tensor`): Second residual.
		residual_3 (`tensorflow.Tensor`): Third residual.
		residual_4 (`tensorflow.Tensor`): Fourth residual.
		kernel_initializer       (`str`): Keras kernel initializer name.
		name                     (`str`): Name of the encoder.
		trainable               (`bool`): Is the decoder trainable.
	
	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( kernel_initializer, str )
	assert pytools.assertions.type_is( name, str )
	assert pytools.assertions.type_is( trainable, bool )

	tensor = expansion_block( tensor, residual_4, 512, kernel_initializer, '{}/up1'.format( name ), trainable )
	tensor = expansion_block( tensor, residual_3, 256, kernel_initializer, '{}/up2'.format( name ), trainable )
	tensor = expansion_block( tensor, residual_2, 128, kernel_initializer, '{}/up3'.format( name ), trainable )
	return   expansion_block( tensor, residual_1,  64, kernel_initializer, '{}/up4'.format( name ), trainable )

# def decoder ( ... )

# --------------------------------------------------

def backbone (
	tensor,
	kernel_initializer,
	include_decoder=True,
	name='backbone',
	encoder_and_middle_trainable=True,
	decoder_trainable=True
	):
	"""
	Backbone of UNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		kernel_initializer   (`str`): Keras kernel initializer name.
		include_decoder     (`bool`): If `False` the decoder is not included.
		name                 (`str`): Name of the backbone.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( kernel_initializer, str )
	assert pytools.assertions.type_is( include_decoder, bool )
	assert pytools.assertions.type_is( name, str )

	# Contraction path
	residual_1, residual_2, residual_3, residual_4, tensor = encoder(
		            tensor = tensor,
		kernel_initializer = kernel_initializer,
		              name = '{}/encoder'.format(name),
		         trainable = encoder_and_middle_trainable
		)
	
	# Middle
	tensor = middle(
		            tensor = tensor,
		kernel_initializer = kernel_initializer,
		              name = '{}/middle'.format(name),
		         trainable = encoder_and_middle_trainable
		)
	
	# Expansion path
	if include_decoder:
		tensor = decoder(
			            tensor = tensor,
			        residual_1 = residual_1,
			        residual_2 = residual_2,
			        residual_3 = residual_3,
			        residual_4 = residual_4,
			kernel_initializer = kernel_initializer,
			              name = '{}/decoder'.format(name),
			         trainable = decoder_trainable
			)

	# Fin
	return tensor

# def backbone ( tensor, include_lrn, kernel_initializer )

# --------------------------------------------------

def head ( tensor, classes=10, classifier_activation='softmax', name='head' ):
	"""
	Head of AlexNet.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		classes                      (`int`): Number of classes.
		classifier_activation (`str`/`None`): Activation of the last layer.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Conv2D(
		    filters = classes,
		kernel_size = (1, 1),
		    strides = (1, 1),
		    padding = 'same',
		 activation = classifier_activation,
		       name = '{}/conv'.format( name )
		)( tensor )

	return tensor

# def classifier ( tensor, classes, classifier_activation )

# --------------------------------------------------

def ENet (
	include_top=True,
	input_tensor=None, input_shape=(572, 572, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='one_hot_category',
	checkpoint=''
	):
	"""
	Create a new instance of the ENet model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: AlexNet keras model.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor = backbone( image, 'glorot_uniform', include_decoder=False, name='backbone' )
	
	# Create the classifier
	if ( include_top ):
		tensor = keras.layers.Flatten(
			name = 'head/flatten'
			)( tensor )
		tensor = keras.layers.Dense(
			     units = classes,
			activation = classifier_activation,
			      name = 'head/fc'
			)( tensor )
	
	# Rename the output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'enet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model
	
# def ENet ( ... )

# --------------------------------------------------

def UNet (
	include_top=True,
	input_tensor=None, input_shape=(572, 572, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='segmentation',
	checkpoint=''
	):
	"""
	Create a new instance of the UNet model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: AlexNet keras model.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor = backbone( image, 'glorot_uniform', name='backbone', encoder_and_middle_trainable=False )
	
	# Create the classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )
	
	# Rename the output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'unet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def UNet ( ... )
