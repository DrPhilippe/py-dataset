# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def backbone ( tensor, name='backbone' ):
	
	# Block 1
	tensor = keras.layers.Conv2D(
		filters     = 32,
		kernel_size = (3, 3),
		strides     = (2, 2),
		use_bias    = False,
		name        = name+'/block01/conv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block01/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block01/relu1'
		)( tensor )
	tensor = keras.layers.Conv2D(
		filters     = 64,
		kernel_size = (3, 3),
		use_bias    = False,
		name        = name+'/block01/conv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block01/bn2'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block01/relu2'
		)( tensor )

	# Residual 1
	residual = keras.layers.Conv2D(
		filters     = 128,
		kernel_size = (1, 1),
		strides     = (2, 2),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/res01/conv'
		)( tensor )
	residual = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/res01/bn'
		)( residual )

	# Block 2
	tensor = keras.layers.SeparableConv2D(
		filters     = 128,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block02/sepconv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block02/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block02/relu1'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 128,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block02/sepconv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block02/bn2'
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size   = (3, 3),
		strides     = (2, 2),
		padding     = 'same',
		name        = name+'/block02/pool'
		)( tensor )
	tensor = keras.layers.Add(
		name        = name+'/block02/add'
		)( [tensor, residual] )

	# Residual 2
	residual = keras.layers.Conv2D(
		filters     = 256,
		kernel_size = (1, 1),
		strides     = (2, 2),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/res02/conv'
		)( tensor )
	residual = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/res02/bn'
		)( residual )

	# Block 3
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block03/relu1'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 256,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block03/sepconv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block03/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block03/relu2'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 256,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block03/sepconv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block03/bn2'
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size   = (3, 3),
		strides     = (2, 2),
		padding     = 'same',
		name        = name+'/block03/pool'
		)( tensor )
	tensor = keras.layers.Add(
		name        = name+'/block03/add'
		)( [tensor , residual] )
	
	# Residual 3
	residual = keras.layers.Conv2D(
		filters     = 728,
		kernel_size = (1, 1),
		strides     = (2, 2),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/res03/conv'
		)( tensor )
	residual = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/res03/bn'
		)( residual )

	# Block 4
	tensor = keras.layers.Activation(
		activation  = 'relu',
		      name  = name+'/block04/relu1'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 728,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block04/sepconv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block04/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block04/relu2'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 728,
		kernel_size = (3, 3),
		padding     ='same',
		use_bias    = False,
		name        = name+'/block04/sepconv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block04/bn2'
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size   = (3, 3),
		strides     = (2, 2),
		padding     = 'same',
		name        = name+'/block04/pool'
		)( tensor )
	tensor = keras.layers.Add(
		name        = name+'/block04/add'
		)( [tensor, residual] )

	# Block 5 to 12
	for block_index in range( 5, 13 ):
		
		# Remember residual
		residual =  tensor 
		
		# Block name
		prefix = '{}/block{:02d}/'.format( name, i )

		# Block definition
		tensor = keras.layers.Activation(
			activation  = 'relu',
			name        = prefix+'relu1'
			)( tensor )
		tensor = keras.layers.SeparableConv2D(
			filters     = 728,
			kernel_size = (3, 3),
			padding     = 'same',
			use_bias    = False,
			name        = prefix+'sepconv1'
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			axis        = -1,
			name        = prefix+'bn1'
			)( tensor )
		tensor = keras.layers.Activation(
			activation  = 'relu',
			name        = prefix+'relu2'
			)( tensor )
		tensor = keras.layers.SeparableConv2D(
			filters     = 728,
			kernel_size = (3, 3),
			padding     = 'same',
			use_bias    = False,
			name        = prefix+'sepconv2'
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			axis        = -1,
			name        = prefix+'bn2'
			)( tensor )
		tensor = keras.layers.Activation(
			activation  = 'relu',
			name        = prefix + 'relu3'
			)( tensor )
		tensor = keras.layers.SeparableConv2D(
			filters     = 728,
			kernel_size = (3, 3),
			padding     = 'same',
			use_bias    = False,
			name        = prefix+'sepconv3'
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			axis        = -1,
			name        = prefix+'bn3'
			)( tensor )
		tensor = keras.layers.Add(
			name        = prefix+'add'
			)( [tensor , residual] )

	# for block_index in range( 5, 13 )
	
	# Residual 12
	residual = keras.layers.Conv2D(
		filters     = 1024,
		kernel_size = (1, 1),
		strides     = (2, 2),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/res12/conv'
		)( tensor )
	residual = keras.layers.BatchNormalization(
		axis        = -1,
		name        =  name+'/res12/bn'
		)( residual )

	# Block 13
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block13/relu1'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 728,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block13/sepconv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block13/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block13/relu2'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 1024,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block13/sepconv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block13/bn2'
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size   = (3, 3),
		strides     = (2, 2),
		padding     = 'same',
		name        = name+'/block13/pool'
		)( tensor )
	tensor = keras.layers.Add(
		name        = name+'/block13/add'
		)( [tensor, residual] )

	# Final Block 14
	tensor = keras.layers.SeparableConv2D(
		filters     = 1536,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block14/sepconv1'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block14/bn1'
		)( tensor )
	tensor = keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block14/relu1'
		)( tensor )
	tensor = keras.layers.SeparableConv2D(
		filters     = 2048,
		kernel_size = (3, 3),
		padding     = 'same',
		use_bias    = False,
		name        = name+'/block14/sepconv2'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		axis        = -1,
		name        = name+'/block14/bn2'
		)( tensor )
	return keras.layers.Activation(
		activation  = 'relu',
		name        = name+'/block14/relu2'
		)( tensor )

# def backbone ( ... )

# --------------------------------------------------

def head ( tensor, classes=1000, classifier_activation='softmax', name='head' ):
   
	tensor = layers.GlobalAveragePooling2D(
		name       = name+'/avg_pool'
		)( tensor )
	return layers.Dense(
			 units = classes,
		activation = classifier_activation,
			  name = name+'/dense'
		)( tensor )

	
# --------------------------------------------------

def Xception (
	include_top=True,
	input_tensor=None, input_shape=(299, 299, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='one_hot_category',
	checkpoint=''
	):
	"""
	Create a new instance of the Xception model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )
	
	# Create the backbone
	tensor = backbone( image )

	# Create classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )

	# Rename output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.models.Model(
			inputs = {input_name: image},
		   outputs = {output_name: tensor},
			  name = 'xception'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def Xception ( ... )