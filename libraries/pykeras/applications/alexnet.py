# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def backbone ( tensor, include_lrn=True, name='backbone' ):
	"""
	Backbone of LeNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		include_lrn         (`bool`): If `True` local response nomalization is applied in the first two blocks.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( name, str )

	# Block 1
	tensor = keras.layers.Conv2D(
		    filters = 96,
		kernel_size = (11, 11),
		    strides = (4, 4),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block1/conv'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			alpha = 0.0001,
			    k = 2,
			 beta = 0.75,
			    n = 5,
			 name = '{}/block1/lrn'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (3, 3),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block1/pool'.format( name )
		)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (5, 5),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			alpha = 0.0001,
			    k = 2,
			 beta = 0.75,
			    n = 5,
			 name = '{}/block2/lrn'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (3, 3),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block2/pool'.format( name )
		)( tensor )

	# Block 3
	tensor = keras.layers.Conv2D(
		    filters = 384,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 384,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv2'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 256,
		kernel_size = (3, 3),
			strides = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block3/conv3'.format( name )
		)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = (3, 3),
		  strides = (2, 2),
		  padding = 'valid',
		     name = '{}/block3/pool'.format( name )
		)( tensor )

	return tensor

# def backbone ( tensor, include_lrn, name )

# --------------------------------------------------

def head ( tensor, classes=10, classifier_activation='softmax', name='head' ):
	"""
	Head of AlexNet.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		classes                      (`int`): Number of classes.
		classifier_activation (`str`/`None`): Activation of the last layer.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = '{}/drop1'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.activations.relu,
		      name = '{}/fc2'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = '{}/drop2'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = classes,
		activation = classifier_activation,
		      name = '{}/fc3'.format( name )
		)( tensor )

	return tensor

# def head ( tensor, classes, classifier_activation, name )

# --------------------------------------------------

def AlexNet (
	include_lrn=True, include_top=True,
	input_tensor=None, input_shape=(227, 227, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='one_hot_category',
	checkpoint=''
	):
	"""
	Create a new instance of the AlexNet model.

	Arguments:
		include_lrn                (`bool`): If `True` local response nomalization is applied in the first two blocks.
		include_top                (`bool`): If `False` the classification layers are omitted.
		input_tensor      (`tensor`/`None`): Optional input tensor.
		input_shape (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                 (`str`): Data type of the input layer.
		input_name                  (`str`): Name of the input layer to create.
		classes                     (`int`): Number of classes to separate.
		classifier_activation(`str`/`None`): Activation of the last dense layer.
		output_name                 (`str`): Name of the output layer.
		checkpoint                  (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: AlexNet keras model.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor = backbone( image, include_lrn )
	
	# Create the classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )
	
	# Rename the output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'alexnet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def AlexNet ( ... )
