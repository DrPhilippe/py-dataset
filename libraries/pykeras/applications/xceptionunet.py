# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def backbone ( tensor ):
	"""
	Backbone of UNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""

	# Block 1
	tensor = keras.layers.Conv2D(
		    filters = 32,
		kernel_size = 3,
		    strides = 2,
		    padding = 'same',
		       name = 'block1/conv'
		)( tensor )
	tensor = keras.layers.Activation(
		activation = 'relu',
		      name = 'block1/relu'
		)( tensor )
	tensor = keras.layers.BatchNormalization(
		name = 'block1/norm'
		)( tensor )
	
	# Set aside residual
	previous_block_activation = tensor

	# Down-Sampling
	block_index = 1
	for filters in [64, 128, 256]:
		
		# Separable Convolution 1
		tensor = keras.layers.SeparableConv2D(
			    filters = filters,
			kernel_size = 3,
			    padding = 'same',
			       name = 'down{:1d}/sconv1'.format(block_index)
			)( tensor )
		tensor = keras.layers.Activation(
			activation = 'relu',
			      name = 'down{:1d}/relu1'.format(block_index)
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			name = 'down{:1d}/norm1'.format(block_index)
			)( tensor )

		# Separable Convolution 2
		tensor = keras.layers.SeparableConv2D(
			    filters = filters,
			kernel_size = 3,
			    padding = 'same',
			       name = 'down{:1d}/sconv2'.format(block_index)
			)( tensor )
		tensor = keras.layers.Activation(
			activation = 'relu',
			      name = 'down{:1d}/relu2'.format(block_index)
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			name = 'down{:1d}/norm2'.format(block_index)
			)( tensor )
		
		# Max Pooling
		tensor = keras.layers.MaxPooling2D(
			pool_size = 3,
			  strides = 2,
			  padding = 'same',
			     name = 'down{:1d}/pool'.format(block_index)
			)( tensor )

		# Project residual
		residual = keras.layers.Conv2D(
			    filters = filters,
			kernel_size = 1,
			    strides = 2,
			    padding = 'same',
			    name = 'down{:1d}/proj'.format(block_index)
			)( previous_block_activation )

		# Add residual
		tensor = keras.layers.Add(
			name = 'down{:1d}/add'.format(block_index)
			)( [tensor, residual] )
		
		# Set aside next residual
		previous_block_activation = tensor

		# Next
		block_index += 1

	# for filters in [64, 128, 256]
	
	# Up-Sampling
	block_index = 1
	for filters in [256, 128, 64, 32]:

		# Transposed Convolution 1
		tensor = keras.layers.Conv2DTranspose(
			    filters = filters,
			kernel_size = 3,
			    padding = 'same',
			       name = 'up{:1d}/tconv1'.format(block_index)
			)( tensor )
		tensor = keras.layers.Activation(
			activation = 'relu',
			      name = 'up{:1d}/relu1'.format(block_index)
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			name = 'up{:1d}/norm1'.format(block_index)
			)(tensor)

		# Transposed Convolution 2
		tensor = keras.layers.Conv2DTranspose(
			    filters = filters,
			kernel_size = 3,
			    padding = 'same',
			       name = 'up{:1d}/tconv2'.format(block_index)
			)( tensor )
		tensor = keras.layers.Activation(
			activation = 'relu',
			      name = 'up{:1d}/relu2'.format(block_index)
			)( tensor )
		tensor = keras.layers.BatchNormalization(
			name = 'up{:1d}/norm2'.format(block_index)
			)(tensor)

		# Upsampling
		tensor = keras.layers.UpSampling2D(
			size = 2,
			name = 'up{:1d}/up'.format(block_index)
			)(tensor)

		# Up-Sample residual
		residual = keras.layers.UpSampling2D(
			size = 2,
			name = 'up{:1d}/upres'.format(block_index)
			)( previous_block_activation )
		
		# Project residual
		residual = keras.layers.Conv2D(
			    filters = filters,
			kernel_size = 1,
			    padding = 'same',
			       name = 'up{:1d}/projres'.format(block_index)
			)( residual )
		
		# Add residual
		tensor = keras.layers.Add(
			name = 'up{:1d}/addres'.format(block_index)
			)( [tensor, residual] )

		# Set aside next residual
		previous_block_activation = tensor 
		
		# Next
		block_index += 1

	# for filters in [256, 128, 64, 32]

	return tensor

# def backbone ( tensor )


# --------------------------------------------------

def head ( tensor, classes=10, classifier_activation='softmax' ):
	"""
	Head of AlexNet.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		classes                      (`int`): Number of classes.
		classifier_activation (`str`/`None`): Activation of the last layer.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )

	tensor = keras.layers.Conv2D(
		    filters = classes,
		kernel_size = (1, 1),
		    strides = (1, 1),
		    padding = 'same',
		 activation = classifier_activation,
		       name = 'head'
		)( tensor ) # [BATCH,  388,  388, 1024]

	return tensor

# def classifier ( tensor, classes, classifier_activation )

# --------------------------------------------------

def XCeptionUNet (
	include_top=True,
	input_tensor=None, input_shape=(572, 572, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax', output_name='segmentation'
	):
	"""
	Create a new instance of the UNet model.

	Arguments:
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		output_name                  (`str`): Name of the output layer.

	Returns:
		`keras.Model`: AlexNet keras model.
	"""
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor = backbone( image )
	
	# Create the classifier
	if include_top:
		tensor = head( tensor, classes, classifier_activation )
	
	# Rename the output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	return keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'xception-unet'
		)

# def XCeptionUNet ( ... )
