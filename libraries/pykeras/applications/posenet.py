# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import googlenet
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def auxiliary_head ( tensor, units=7, classifier_activation='linear', name='aux_classifier' ):
	"""
	Auxiliary pose prediction branch of PoseNet.
	
	The number of predicted values depends on the representation of rotation:
		- 6 units for 3D euler angles (3) and 3D translation vector (3).
		- 7 units for 3D quaternion angles (4) and 3D translation vector (3).
		- 12 units for 3D rotation matrix (9) and 3D translation vector (3).

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		units                        (`int`): Number of values to predict.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the auxiliary head.

	Returns:
		r (`tensorflow.Tensor`): Tensor containing the predicted rotation.
		t (`tensorflow.Tensor`): Tensor containing the predicted translation.
	"""
	assert pytools.assertions.type_is( units, int )
	assert pytools.assertions.value_in( units, (6,7,12) )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format(name)
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 1024,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format(name)
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.7,
		name = '{}/dropout'.format(name)
		)( tensor )
	tensor = keras.layers.Dense(
		     units = units,
		activation = classifier_activation,
		      name = '{}/fc2'.format(name)
		)( tensor )
	r = pykeras.layers.Slice(
		start = 0,
		  end = units-3,
		 name = '{}/slice_r'.format(name)
		)( tensor )
	t = pykeras.layers.Slice(
		start = units-3,
		  end = units,
		 name = '{}/slice_t'.format(name)
		)( tensor )

	if units == 12:
		r = keras.layers.Reshape(
			target_shape = (3, 3),
			 input_shape = (9,),
			 name = '{}/reshape_r'.format(name)
			)( r )

	return r, t

# def auxiliary_head ( tensor, units, classifier_activation, name )

# --------------------------------------------------

def head ( tensor, units=7, classifier_activation='linear', name='head' ):
	"""
	Pose prediction branch of PoseNet.

	The number of predicted values depends on the representation of rotation:
		- 6 units for 3D euler angles (3) and 3D translation vector (3).
		- 7 units for 3D quaternion angles (4) and 3D translation vector (3).
		- 12 units for 3D rotation matrix (9) and 3D translation vector (3).

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.	
		units                        (`int`): Number of values to predict.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the head.

	Returns:
		r (`tensorflow.Tensor`): Tensor containing the predicted rotation.
		t (`tensorflow.Tensor`): Tensor containing the predicted translation.
	"""
	assert pytools.assertions.type_is( units, int )
	assert pytools.assertions.value_in( units, (6,7,12) )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 2048,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.4,
		name = '{}/dropout'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 7,
		activation = classifier_activation,
		      name = '{}/fc2'.format( name )
		)( tensor )
	r = pykeras.layers.Slice(
		start = 0,
		  end = units-3,
		 name = '{}/slice_r'.format( name )
		)( tensor )
	t = pykeras.layers.Slice(
		start = units-3,
		  end = units,
		 name = '{}/slice_t'.format( name )
		)( tensor )

	if units == 12:
		r = keras.layers.Reshape(
			target_shape = (3, 3),
			 input_shape = (9,),
			 name = '{}/reshape_r'.format( name )
			)( r )

	return r, t

# def head ( tensor, units, classifier_activation )

# --------------------------------------------------

def get_rotation_loss ( rotation_type='q' ):
	"""
	Returns the appropriated loss for the given rotation type.

	Arguments:
		rotation_type (`str`): One of 'e', 'q' or 'R'.

	Returns:
		`keras.losses.Loss`: The loss.
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )

	if  ( rotation_type == 'e' ):
		return pykeras.losses.EulerDistance()

	elif ( rotation_type == 'q' ):
		return pykeras.losses.QuaternionDistance()		

	else: # R
		return pykeras.losses.MatrixDistance()

# def get_rotation_loss ( rotation_type )

# --------------------------------------------------

def get_rotation_metric ( rotation_type='q', name='r', name_prefix='', name_suffix='', **kwargs ):
	"""
	Returns the appropriated metrics for the given rotation type.

	Arguments:
		rotation_type (`str`): One of 'e', 'q' or 'R'.
		name          (`str`): Name of the metric, or base name of te metrics.
		name_prefix   (`str`): Text prepended to the metric name.
		name_suffix   (`str`): Text appended to the metric name.

	Returns:
		`keras.metrics.Metric`: The metric.
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )
	assert pytools.assertions.type_is( name, str )
	assert pytools.assertions.type_is( name_prefix, str )
	assert pytools.assertions.type_is( name_suffix, str )

	fullname = name_prefix + name + name_suffix

	if  ( rotation_type == 'e' ):
		return pykeras.metrics.EulerAccuracy( name=fullname, **kwargs )

	elif ( rotation_type == 'q' ):
		return pykeras.metrics.QuaternionAccuracy( name=fullname, **kwargs )		

	else: # R
		raise NotImplementedError()

# def get_rotation_metric ( rotation_type )

# --------------------------------------------------

def get_translation_loss ():
	"""
	Returns the appropriated loss for the given rotation type.

	Returns:
		`keras.losses.Loss`: The loss.
	"""

	return pykeras.losses.VectorDistance()

# def get_translation_loss ()

# --------------------------------------------------

def get_translation_metric ( name='t', name_prefix='', name_suffix='', **kwargs ):
	"""
	Returns the appropriated metric for translation output(s).

	Arguments:
		name          (`str`): Name of the metric, or base name of te metrics.
		name_prefix   (`str`): Text prepended to the metric(s) name(s).
		name_suffix   (`str`): Text appended to the metric(s) name(s).

	Returns:
		`keras.metrics.Metric`: The metric.
	"""
	fullname = name_prefix + name + name_suffix
	return pykeras.metrics.TranslationAccuracy( name=fullname, **kwargs )

# def get_translation_metric ()

# --------------------------------------------------

def get_losses ( rotation_type='q', outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] ):
	"""
	Returns the losses used to train a PoseNet model.
	
	Translation loss is computed using 3 euclidean distance losses for the 3 translation outputs.
	Rotation loss is computed using an euler loss, quaternion loss or rotation matrix loss depending on the rotation.

	Arguments:
		rotation_type                (`str`): One of 'e', 'q' or 'R'.
		outputs_names (`list` of `6` `str`s): Name of the 6 posenet outputs.

	Returns:
		`dict` of `6` `str` to `keras.losses.Loss`: The losses indexed by output name.
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )
	assert pytools.assertions.type_is( outputs_names, list )
	assert pytools.assertions.equal( len(outputs_names), 6 )
	assert pytools.assertions.list_items_type_is( outputs_names, str )

	return {
		# Main output
		outputs_names[0]: get_rotation_loss( rotation_type ),
		outputs_names[1]: get_translation_loss(),
		# Auxiliary 1
		outputs_names[2]: get_rotation_loss( rotation_type ),
		outputs_names[3]: get_translation_loss(),
		# Auxiliary 2
		outputs_names[4]: get_rotation_loss( rotation_type ),
		outputs_names[5]: get_translation_loss()
	    }

# def get_losses ( rotation_type )

# --------------------------------------------------

def get_losses_weights ( alpha=1.0, beta=0.01, w0=1.0, w1=0.3, w2=0.3, outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] ):
	"""
	Returns the losses weights used to train a PoseNet model.
	
	Arguments:
		alpha                      (`float`): Weight of the rotations terms.
		beta                       (`float`): Weight of the translations terms.
		w0                         (`float`): Weight of the main classifier output.
		w1                         (`float`): Weight of the output of the first auxiliary classifier.
		w2                         (`float`): Weight of the output of the second auxiliary classifier.
		outputs_names (`list` of `6` `str`s): Name of the 6 posenet outputs.

	Returns:
		`dict` of `6` `str` to `float`: The weights indexed by output name.
	"""
	assert pytools.assertions.type_is( alpha, float )
	assert pytools.assertions.type_is( beta, float )
	assert pytools.assertions.type_is( w0, float )
	assert pytools.assertions.type_is( w1, float )
	assert pytools.assertions.type_is( w2, float )
	assert pytools.assertions.type_is( outputs_names, list )
	assert pytools.assertions.equal( len(outputs_names), 6 )
	assert pytools.assertions.list_items_type_is( outputs_names, str )

	return {
		# Main output
		outputs_names[0]: w0*alpha,
		outputs_names[1]: w0*beta,
		# Auxiliary 1
		outputs_names[2]: w1*alpha,
		outputs_names[3]: w1*beta,
		# Auxiliary 2
		outputs_names[4]: w2*alpha,
		outputs_names[5]: w2*beta
		}

# def get_losses_weights ( alpha, beta, w0, w1, w2 )

# --------------------------------------------------

def get_metrics ( rotation_type, outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] ):
	"""
	Returns the metrics used to evaluate a PoseNet model.
	
	Arguments:
		rotation_type                (`str`): One of 'e', 'q' or 'R'.
		outputs_names (`list` of `6` `str`s): Name of the 6 posenet outputs.

	Returns:
		`dict` of `6` `str` to `keras.metrics.Metric`: The losses indexed by output name.
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )
	assert pytools.assertions.type_is( outputs_names, list )
	assert pytools.assertions.equal( len(outputs_names), 6 )
	assert pytools.assertions.list_items_type_is( outputs_names, str )

	return {
		outputs_names[0]: [
			pykeras.applications.posenet.get_rotation_metric( rotation_type, name='r' )#,
			# pykeras.applications.posenet.get_rotation_metric( rotation_type, name='rx', axes='x' ),
			# pykeras.applications.posenet.get_rotation_metric( rotation_type, name='ry', axes='y' ),
			# pykeras.applications.posenet.get_rotation_metric( rotation_type, name='rz', axes='z' ),
			],
		outputs_names[1]: [
			pykeras.applications.posenet.get_translation_metric( name='t' ),
			pykeras.applications.posenet.get_translation_metric( name='tx', axes='x' ),
			pykeras.applications.posenet.get_translation_metric( name='ty', axes='y' ),
			pykeras.applications.posenet.get_translation_metric( name='tz', axes='z' )
			]#,
		# outputs_names[2]: pykeras.applications.posenet.get_rotation_metric( rotation_type, name='r1' ),
		# outputs_names[3]: pykeras.applications.posenet.get_translation_metric( name='t1' ),
		# outputs_names[4]: pykeras.applications.posenet.get_rotation_metric( rotation_type, name='r2' ),
		# outputs_names[5]: pykeras.applications.posenet.get_translation_metric( name='t2' )
		}

# def get_metrics ( rotation_type, outputs_names )

# --------------------------------------------------

def get_units_for_rotation ( rotation_type ):
	"""
	Returns the number of units necessary to predict the given rotation type.

	Arguments:
		rotation_type (`str`): One of 'e', 'q' or 'R'.

	Returns:
		`int`: The number of units
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )

	if rotation_type == 'e':
		return 3
	elif rotation_type == 'q':
		return 4
	else:
		return 9

# def get_units_for_rotation ( rotation_type )

# --------------------------------------------------

def get_units_for_translation ():
	"""
	Returns the number of units necessary to predict the translation.

	Returns:
		`int`: The number of units (`3`).
	"""
	return 3

# def get_units_for_translation ()

# --------------------------------------------------

def get_units ( rotation_type ):
	"""
	Returns the number of units necessary to predict the pose (rotation and translation).

	Arguments:
		rotation_type (`str`): One of 'e', 'q' or 'R'.

	Returns:
		`int`: The number of units
	"""
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('e', 'q', 'R') )

	return get_units_for_rotation( rotation_type ) + get_units_for_translation()

# def get_units ( rotation_type )

# --------------------------------------------------

def PoseNet (
	include_lrn=True, include_top=True,
	input_tensor=None, input_shape=(227, 227, 3), input_dtype='float32', input_name='image',
	units=7, rotation_type='', classifier_activation='linear',
	outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'],
	checkpoint=''
	):
	"""
	Create a new instance of the PoseNet model.
	
	Units can be:
		- 7 for qauternion and translation vectors,
		- 6 for euleur and translation vectors,
		- 12 for rotation matrix and translation vector.
	Or you can specify the rotation type directly:
		- 'e' for Euler angles, i.e. 6 units,
		- 'q' for quaternions, i.e. 7 units.
		- 'R' for rotation matrices, i.e. 12 units.


	Arguments:
		include_lrn                  (`bool`): If `True` local response nomalization is applied in the first two blocks.
		include_top                  (`bool`): If `False` the classification layers are omitted.
		input_tensor        (`tensor`/`None`): Optional input tensor.
		input_shape   (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                   (`str`): Data type of the input layer.
		input_name                    (`str`): Name of the input layer to create.
		units                  (`None`/`int`): Number of units in the last dense layer.
		rotation_type                 (`str`): Rotation type ('', 'e', 'q', 'R').
		classifier_activation  (`str`/`None`): Activation of the last dense layer.
		outputs_names  (`list` of `6` `str`s): Names of the 6 outputs layers (r, t, r1, t1, r2, t2)
		checkpoint                    (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: PoseNet keras model.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_in( units, (type(None), int) )
	assert pytools.assertions.value_in( units, (None, 6, 7, 12) )
	assert pytools.assertions.type_is( rotation_type, str )
	assert pytools.assertions.value_in( rotation_type, ('', 'e', 'q', 'R') )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( outputs_names, list )
	assert pytools.assertions.list_items_type_is( outputs_names, str )
	assert pytools.assertions.equal( len(outputs_names), 6 )
	assert pytools.assertions.type_is( checkpoint, str )

	# Compute number of units based on rotation type ?
	if rotation_type:
		units = get_units( rotation_type )
	elif not units:
		raise ValueError( "One of 'units' or 'rotation_type' must be specified" )

	# Create input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Backbone
	tensor, aux1, aux2 = googlenet.backbone( image, include_lrn )
		
	# Top
	if include_top:
		r1, t1 = auxiliary_head( aux1, units, classifier_activation, name='aux_classifier_1' )
		r2, t2 = auxiliary_head( aux2, units, classifier_activation, name='aux_classifier_2' )
		r, t   = head( tensor, units, classifier_activation )
	
	# Rename outpouts
	r = pykeras.layers.Rename(
		name = outputs_names[ 0 ]
		)( r )
	t = pykeras.layers.Rename(
		name = outputs_names[ 1 ]
		)( t )
	r1 = pykeras.layers.Rename(
		name = outputs_names[ 2 ]
		)( r1 )
	t1 = pykeras.layers.Rename(
		name = outputs_names[ 3 ]
		)( t1 )
	r2 = pykeras.layers.Rename(
		name = outputs_names[ 4 ]
		)( r2 )
	t2 = pykeras.layers.Rename(
		name = outputs_names[ 5 ]
		)( t2 )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = {
		       outputs_names[0]: r,
		       outputs_names[1]: t,
		       outputs_names[2]: r1,
		       outputs_names[3]: t1,
		       outputs_names[4]: r2,
		       outputs_names[5]: t2
		       },
		      name = 'posenet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def PoseNet ( ... )