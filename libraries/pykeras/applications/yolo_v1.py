# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def reduce_block ( tensor, filters, name ):
	"""
	Creates a simplified inception block with only two convolutional layers.

	Arguments:
		tensor        (`keras.Tensor`): Tensor to convolve on.
		filters (`list` of `2` `int`s): Number of filters on the first and second convolutional layers.
		name                   (`str`): Name of the block.

	Returns:
		`keras.Tensor`: Convoluted tensor.
	"""
	assert pytools.assertions.type_is( filters, list )
	assert pytools.assertions.equal( len(filters), 2 )
	assert pytools.assertions.list_items_type_is( filters, int )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Conv2D(
		    filters = filters[ 0 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/conv1'.format( name )
		)( tensor )

	tensor = keras.layers.Conv2D(
		    filters = filters[ 1 ],
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/conv2'.format( name )
		)( tensor )

	return tensor

# def reduce_block ( tensor, filters, names )

# --------------------------------------------------

def backbone ( tensor, include_lrn=False, name='backbone' ):
	"""
	Backbone of YOLO V1

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		include_lrn         (`bool`): If `True` local response nomalization is applied in the first two blocks.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )

	# Block 1
	tensor = keras.layers.Conv2D(
			    filters = 64,
			kernel_size = [7, 7],
			    strides = [2, 2],
			    padding = 'same',
			 activation = keras.layers.LeakyReLU( alpha=0.1 ),
			       name = '{}/block1/conv'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block1/pool'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = '{}/block1/lrn'.format( name )
			)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 192,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/block2/conv'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = '{}/block2/lrn'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block2/pool'.format( name )
		)( tensor )

	# Block 3
	tensor = reduce_block( tensor, [128, 256], name='{}/block3/reduce1'.format( name ) )
	tensor = reduce_block( tensor, [256, 512], name='{}/block3/reduce2'.format( name ) )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block3/pool'.format( name )
		)( tensor )

	# Block 4
	tensor = reduce_block( tensor, [256,  512], name='{}/block4/reduce1'.format( name ) )
	tensor = reduce_block( tensor, [256,  512], name='{}/block4/reduce2'.format( name ) )
	tensor = reduce_block( tensor, [256,  512], name='{}/block4/reduce3'.format( name ) )
	tensor = reduce_block( tensor, [256,  512], name='{}/block4/reduce4'.format( name ) )
	tensor = reduce_block( tensor, [512, 1024], name='{}/block4/reduce5'.format( name ) )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block4/pool'.format( name )
		)( tensor )

	# Block 5
	tensor = reduce_block( tensor, [512, 1024], name='{}/block5/reduce1'.format( name ) )
	tensor = reduce_block( tensor, [512, 1024], name='{}/block5/reduce2'.format( name ) )
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/block5/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [2, 2],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/block5/conv2'.format( name )
		)( tensor )

	# Block 6
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/block6/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = '{}/block6/conv2'.format( name )
		)( tensor )

	return tensor

# def body ( ... )

# --------------------------------------------------

def head ( tensor, number_of_cells=1, number_of_detectors_per_cell=1, number_of_classes=1000, classifier_activation='linear', name='head' ):
	"""
	Head of YOLO V1.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		number_of_cells              (`int`): Number of cells on the width and height of the image.
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell.
		number_of_classes            (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.

	Returns:
		`tensorflow.Tensor`: Output tensor containing the result, shape: [number_of_cells, number_of_cells, number_of_classes + number_of_detectors_per_cell * 5].
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.type_is( number_of_classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )

	# Compute the number of units required to compute predictions
	units        =   number_of_cells * number_of_cells * ( number_of_classes + number_of_detectors_per_cell * 5 )
	output_shape = ( number_of_cells,  number_of_cells,    number_of_classes + number_of_detectors_per_cell * 5 )

	# Head
	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.layers.LeakyReLU( alpha=0.1 ),
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = '{}/drop'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = units,
		activation = classifier_activation,
		      name = '{}/fc2'.format( name )
		)( tensor )
	tensor = keras.layers.Reshape(
		target_shape = output_shape,
		 input_shape = (units,),
		        name = '{}/reshape'.format( name )
		)( tensor )
	return tensor

# def head ( ... )

# --------------------------------------------------

def YoloV1 (
	# Architecture options
	include_lrn = False,
	include_top = True,
	# Input
	input_tensor = None,
	input_shape = (448, 448, 3),
	input_dtype = 'float32',
	input_name = 'image',
	# Output
	number_of_cells = 7,
	number_of_detectors_per_cell = 2,
	number_of_classes = 20,
	classifier_activation = 'linear',
	output_name = 'yolo_v1',
	# Checkpoint
	checkpoint=''
	):
	"""
	Create the Yolo V1 Model.
	
	Arguments:
		include_lrn                 (`bool`): If `True` local response nomalization is applied in the first two blocks.
		number_of_cells              (`int`): Number of cells on the width and height of the image.
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell.
		number_of_classes            (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		checkpoint                   (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: Yolo V1 model.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.type_is( number_of_classes, int )
	assert pytools.assertions.type_is( classifier_activation, str )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create input layer
	image  = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )
	tensor = image
	
	# Resize image to a fix size of 448x448
	tensor = keras.layers.experimental.preprocessing.Resizing(
		        width = 448,
		       height = 448,
		interpolation = 'bilinear'
		)( tensor )

	# Backbone
	tensor = backbone( tensor, include_lrn )
		
	# Top
	if include_top:
		tensor = head( tensor, number_of_cells, number_of_detectors_per_cell, number_of_classes, classifier_activation )

	# Rename
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'yolo_v1'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def YoloV1 ( ... )
