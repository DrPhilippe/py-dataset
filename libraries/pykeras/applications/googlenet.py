# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def inception_v2 ( tensor, filters, name='inception_v2' ):
	"""
	Inception v2 module.

	Arguments:
		tensor   (`tensorflow.Tensor`): Input tensor.
		filters (`list` of `6` `int`s): Number of filters of the convolutions layers.
		name                   (`str`): Name of the inception v2 block.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""	
	assert pytools.assertions.type_is( filters, list )
	assert pytools.assertions.list_items_type_is( filters, int )
	assert pytools.assertions.equal( len(filters), 6 )
	assert pytools.assertions.type_is( name, str )

	# Branch 1
	branch1 = keras.layers.Conv2D(
		    filters = filters[ 0 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch1/conv'.format( name )
		)( tensor )

	# Branch 2
	branch2 = keras.layers.Conv2D(
		    filters = filters[ 1 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch2/conv1'.format( name )
		)( tensor )
	branch2 = keras.layers.Conv2D(
		    filters = filters[ 2 ],
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch2/conv2'.format( name )
		)( branch2 )

	# Branch 3
	branch3 = keras.layers.Conv2D(
		    filters = filters[ 3 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch3/conv1'.format( name )
		)( tensor )
	branch3 = keras.layers.Conv2D(
		    filters = filters[ 4 ],
		kernel_size = [5, 5],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch3/conv2'.format( name )
		)( branch3 )

	# Branch 4
	branch4 = keras.layers.MaxPooling2D(
		  pool_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		       name = '{}/branch4/pool'.format( name )
		)( tensor )
	branch4 = keras.layers.Conv2D(
		    filters = filters[ 5 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/branch4/conv'.format( name )
		)( branch4 )

	# Concatenate results
	return keras.layers.Concatenate(
		axis = -1,
		name = '{}/concat'.format(name)
		)( [branch1, branch2, branch3, branch4] )

# def inception_v2 ( tensor, filters, name )

# --------------------------------------------------

def auxiliary_encoder ( tensor, name='aux_encoder' ):
	"""
	Auxialiary encoder branch.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		name                 (`str`): Name of the auxiliary classifier.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""	
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.AveragePooling2D(
		pool_size = (5, 5),
		  strides = 3,
		     name = '{}/pool'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 128,
		kernel_size = (1, 1),
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/conv'.format( name )
		)( tensor )
	return tensor

# def auxiliary_encoder ( tensor, name )

# --------------------------------------------------

def backbone ( tensor, include_lrn=True, name='backbone' ):
	"""
	Backbone of GoogLeNet.

	Arguments:
		tensor (`tensorflow.Tensor`): Input tensor.
		include_lrn         (`bool`): If `True` local response nomalization is applied in the first two blocks.
		name                 (`str`): Name of the backbone.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( name, str )

	# Block 1
	tensor = keras.layers.Conv2D(
			    filters = 64,
			kernel_size = [7, 7],
			    strides = [2, 2],
			    padding = 'same',
			 activation = keras.activations.relu,
			       name = '{}/block1/conv'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [3, 3],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block1/pool'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = '{}/block1/lrn'.format( name )
			)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 192,
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv1'.format( name )
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 192,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.activations.relu,
		       name = '{}/block2/conv2'.format( name )
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = '{}/block2/lrn'.format( name )
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [3, 3],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block2/pool'.format( name )
		)( tensor )

	# Block 3
	tensor = inception_v2( tensor,
		filters = [64, 96, 128, 16, 32, 32],
		   name = '{}/block3/inception1'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [128, 128, 192, 32, 96, 64],
		   name = '{}/block3/inception2'.format( name )
		)
	tensor = keras.layers.MaxPooling2D(
		pool_size = [3, 3],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block3/pool'.format( name )
		)( tensor )

	# Block 4
	tensor = inception_v2( tensor,
		filters = [192, 96, 208, 16, 48, 64],
		   name = '{}/block4/inception1'.format( name )
		)
	aux_1 = auxiliary_encoder( tensor,
		name = '{}aux_encoder_1'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [160, 112, 224, 24, 64, 64],
		   name = '{}/block4/inception2'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [128, 128, 256, 24, 64, 64],
		   name = '{}/block4/inception3'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [112, 144, 288, 32, 64, 64],
		   name = '{}/block4/inception4'.format( name )
		)	
	aux_2 = auxiliary_encoder( tensor,
		name = '{}/aux_encoder_2'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [256, 160, 320, 32, 128, 128],
		   name = '{}/block4/inception5'.format( name )
		)
	tensor = keras.layers.MaxPooling2D(
		pool_size = [3, 3],
		  strides = [2, 2],
		  padding = 'same',
		     name = '{}/block4/pool'.format( name )
		)( tensor )

	# Block 5
	tensor = inception_v2( tensor,
		filters = [256, 160, 320, 32, 128, 128],
		   name = '{}/block5/inception1'.format( name )
		)
	tensor = inception_v2( tensor,
		filters = [384, 192, 384, 48, 128, 128],
		   name = '{}/block5/inception2'.format( name )
		)
	tensor = keras.layers.AveragePooling2D(
		pool_size = [7, 7],
		  strides = [1, 1],
		  padding = 'valid',
		     name = '{}/block5/pool'.format( name )
		)( tensor )

	return tensor, aux_1, aux_2

# def backbone ( inputs, include_lrn )

# --------------------------------------------------

def auxiliary_head ( tensor, classes=1000, classifier_activation='softmax', name='aux_head' ):
	"""
	Auxiliary head of GoogLeNet.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the auxialiary classifier.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 1024,
		activation = keras.activations.relu,
		      name = '{}/fc1'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.7,
		name = '{}/dropout'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = classes,
		activation = classifier_activation,
		      name = '{}/fc2'.format( name )
		)( tensor )
	return tensor

# def auxiliary_head ( tensor, classes, classifier_activation, name )

# --------------------------------------------------

def head ( tensor, classes=1000, classifier_activation='softmax', name='head' ):
	"""
	Head of GoogLeNet.

	Arguments:
		tensor         (`tensorflow.Tensor`): Input tensor.	
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		name                         (`str`): Name of the head.

	Returns:
		`tensorflow.Tensor`: Output tensor.
	"""
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( name, str )

	tensor = keras.layers.Flatten(
		name = '{}/flatten'.format( name )
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.4,
		name = '{}/dropout'.format( name )
		)( tensor )
	tensor = keras.layers.Dense(
		     units = classes,
		activation = classifier_activation,
		      name = '{}/fc'.format( name )
		)( tensor )

	return tensor

# def head ( tensor, classes, classifier_activation )

# --------------------------------------------------

def get_losses ( from_logits=False ):
	"""
	Returns the losses used to train a GoogLeNet model.
	
	One loss is returned per model output, 3 in total.

	Arguments:
		from_logits (`bool`): `False` if the model outputs arbitrary logits, `True` if it outputs probabilities.

	Returns:
		`list` of `3` `keras.losses.CategoricalCrossentropy`: The losses.
	"""
	assert pytools.assertions.type_is( from_logits, bool )

	return [
		keras.losses.CategoricalCrossentropy( from_logits=from_logits ),
		keras.losses.CategoricalCrossentropy( from_logits=from_logits ),
		keras.losses.CategoricalCrossentropy( from_logits=from_logits )
		]

# def get_losses ()

# --------------------------------------------------

def get_losses_weights ( w0=1.0, w1=0.3, w2=0.3 ):
	"""
	Returns the losses weights used to train a GoogLeNet model.
	
	Arguments:
		w0 (`float`): Weight of the main classifier output.
		w1 (`float`): Weight of the output of the first auxiliary classifier.
		w2 (`float`): Weight of the output of the second auxiliary classifier.

	Returns:
		`list` of `3` `float`s: The weights.
	"""
	assert pytools.assertions.type_is( w0, float )
	assert pytools.assertions.type_is( w1, float )
	assert pytools.assertions.type_is( w2, float )

	return [w0, w1, w2]

# def get_losses_weights ( w0, w1, w2 )

# --------------------------------------------------

def GoogLeNet (
	include_lrn=True, include_top=True,
	input_tensor=None, input_shape=(224, 224, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax',
	outputs_names=['one_hot_category', 'aux1_one_hot_category', 'aux2_one_hot_category'],
	checkpoint=''
	):
	"""
	Create a new instance of the GoogLeNet model.

	Arguments:
		include_lrn                 (`bool`): If `True` local response nomalization is applied in the first two blocks.
		include_top                 (`bool`): If `False` the classification layers are omitted.
		input_tensor       (`tensor`/`None`): Optional input tensor.
		input_shape  (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                  (`str`): Data type of the input layer.
		input_name                   (`str`): Name of the input layer to create.
		classes                      (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.
		outputs_names (`list` of `3` `str`s): Names of the 3 outputs layers (main, aux_1, aux_2).
		checkpoint                   (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: GoogLeNet keras model.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( outputs_names, list )
	assert pytools.assertions.list_items_type_is( outputs_names, str )
	assert pytools.assertions.equal( len(outputs_names), 3 )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Create the backbone
	tensor, aux_1, aux_2 = backbone( image, include_lrn, name='backbone' )
	
	# Create classifier(s)
	if include_top:
		aux_1  = auxiliary_head( aux_1, classes, classifier_activation, name='aux_classifier_1' )
		aux_2  = auxiliary_head( aux_2, classes, classifier_activation, name='aux_classifier_2' )
		tensor = head( tensor, classes, classifier_activation, name='head' )
	
	# Rename outputs
	tensor = pykeras.layers.Rename(
		name = outputs_names[ 0 ]
		)( tensor )
	aux_1 = pykeras.layers.Rename(
		name = outputs_names[ 1 ]
		)( aux_1 )
	aux_2 = pykeras.layers.Rename(
		name = outputs_names[ 2 ]
		)( aux_2 )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { outputs_names[0]: tensor, outputs_names[1]: aux_1, outputs_names[2]: aux_2},
		      name = 'googlenet'
		)

	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model
	
# def GoogLeNet ( ... )
