# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import sys
from tensorflow import keras

# INTERNALS
import pykeras
import pytools.assertions
import pytools.path
import pytools.tasks

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_or_create_image_input ( input_tensor=None, input_shape=(224, 224, 3), input_dtype='float32', input_name='image' ):
	"""
	Create an input layer for an image.

	Arguments:
		input_tensor      (`tensor`/`None`): Optional input tensor.
		input_shape (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                 (`str`): Data type of the input layer.
		input_name                  (`str`): Name of the input layer to create.

	Returns:
		`keras.layers.Input`: Input layer.
	"""
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )

	if input_tensor is None:
		return keras.layers.Input(
			shape = input_shape,
			dtype = input_dtype,
			 name = input_name
			)
	else:
		if not keras.backend.is_keras_tensor( input_tensor ):
			return keras.layers.Input(
				tensor = input_tensor,
				 shape = input_shape
				)
		else:
			return input_tensor

	# if input_tensor is None

# def get_or_create_image_input ( input_tensor, input_shape, input_dtype, input_name )

# --------------------------------------------------

def get_or_create_backbone ( backbone, **kwargs ):
	"""
	Instantiate the backbone with the given name or returns the given backbone.

	Arguments:
		backbone (`str`/`keras.Model`): Backbone model or model name.

	Returns:
		`keras.Model`: Backbone model.
	"""
	assert pytools.assertions.type_is_instance_of( backbone, (keras.Model, str) )
	
	# Instantiate backbone ?
	if isinstance( backbone, str ):

		# Try to get it from keras.applications
		backbone_type = getattr( keras.applications, backbone, None )

		# If it was not in it, try to get it from pykeras.applications
		if backbone_type is None:
			backbone_type = getattr( pykeras.applications, backbone, None )

		# If it is still not found, raise an error
		if backbone_type is None:
			raise ValueError(
				"Backbone named '{}' does not exist. See `keras.applications` and `pykeras.applications` for a list of existing backbone.".format(
				backbone
				))

		# Instantiate it
		return backbone_type( **kwargs )
	
	# if isinstance( backbone, str )

	return backbone

# def get_or_create_backbone ( backbone, **kwargs )