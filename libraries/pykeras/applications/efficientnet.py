# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras.layers
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def EfficientNet (
	model_id,
	include_top=True,
	input_tensor=None, input_shape=(227, 227, 3), input_dtype='float32', input_name='image',
	classes=1000, classifier_activation='softmax',
	output_name='one_hot_category',
	checkpoint=''
	):
	"""
	Create a new instance of the GoogLeNet model.

	Arguments:
		model_id                      (`int`): EfficientNet model ID [0-7].
		include_top                  (`bool`): If `False` the classification layers are omitted.
		input_tensor        (`tensor`/`None`): Optional input tensor.
		input_shape   (`tuple` of `3` `int`s): Shape of the input `(width, height, depth)`.
		input_dtype                   (`str`): Data type of the input layer.
		input_name                    (`str`): Name of the input layer to create.
		classes                       (`int`): Number of classes to separate.
		classifier_activation  (`str`/`None`): Activation of the last dense layer.
		output_name                   (`str`): Name of the output layer.
		checkpoint                    (`str`): Checkpoint file or directory containing one.

	Returns:
		`keras.Model`: EfficientNet keras model.
	"""
	assert pytools.assertions.type_is( model_id, int )
	assert pytools.assertions.value_in( model_id, range(8) )
	assert pytools.assertions.type_is( include_top, bool )
	assert pytools.assertions.type_is( input_shape, tuple )
	assert pytools.assertions.equal( len(input_shape), 3 )
	assert pytools.assertions.tuple_items_type_is( input_shape, int )
	assert pytools.assertions.type_is( input_dtype, str )
	assert pytools.assertions.type_is( input_name, str )
	assert pytools.assertions.type_is( classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )
	assert pytools.assertions.type_is( output_name, str )
	assert pytools.assertions.type_is( checkpoint, str )

	# Create the input layer
	image = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )

	# Compose model type name
	model_typename = 'EfficientNetB{:1d}'.format( model_id )

	# Get model type
	model_type = getattr( tensorflow.keras.applications, model_typename, None )
	if model_type is None:
		raise RuntimeError(
			"Failed to get EfficientNet model with ID '{}', typename='{}' in namespace tensorflow.keras.applications".format(
			arguments.model_id,
			model_typename
			))

	# Create backbone model
	e_model = model_type(
		          include_top = include_top,
		              weights = None,
		         input_tensor = None,
		          input_shape = input_shape,
		              pooling = None,
		              classes = classes,
		classifier_activation = classifier_activation
		)

	# Apply backbone
	tensor = e_model( image )
	
	# Rename output
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	model = keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'efficientnetb3'
		)
	
	# Load checkpoints
	model = pykeras.checkpoints.load_model_checkpoint( model, checkpoint )#, by_name=True, skip_mismatch=True )

	# Return the model
	return model

# def EfficientNet ( ... )
