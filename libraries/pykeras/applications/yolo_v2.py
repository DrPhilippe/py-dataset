# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras

# INTERNALS
import pykeras
import pytools.assertions

# LOCALS
from . import utils

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def reduce_block ( tensor, filters, names ):
	"""
	Creates a simplified inception block with only two convolutional layers.

	Arguments:
		tensor        (`keras.Tensor`): Tensor to convolve on.
		filters (`list` of `2` `int`s): Number of filters on the first and second convolutional layers.
		filters (`list` of `2` `str`s): Names of the first and second convolutional layers.

	Returns:
		`keras.Tensor`: Convoluted tensor.
	"""
	assert pytools.assertions.type_is( filters, list )
	assert pytools.assertions.equal( len(filters), 2 )
	assert pytools.assertions.list_items_type_is( filters, int )
	assert pytools.assertions.type_is( names, list )
	assert pytools.assertions.equal( len(names), 2 )
	assert pytools.assertions.list_items_type_is( names, str )

	tensor = keras.layers.Conv2D(
		    filters = filters[ 0 ],
		kernel_size = [1, 1],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.3 ),
		       name = names[ 0 ]
		)( tensor )

	tensor = keras.layers.Conv2D(
		    filters = filters[ 1 ],
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.3 ),
		       name = names[ 1 ]
		)( tensor )

	return tensor

# def reduce_block ( tensor, filters, names )

# --------------------------------------------------

def backbone ( tensor, include_lrn=False ):
	"""
	Encodes the given tensor using YOLO V1 backbone.

	Arguments:
		tensor (`tensorflow.Tensor`): Tensor to encode.
		include_lrn         (`bool`): If `True` local response nomalization is applied in the first two blocks.

	Returns:
		`tensorflow.Tensor`: Encoded tensor.
	"""
	assert pytools.assertions.type_is( include_lrn, bool )

	# Block 1
	tensor = keras.layers.Conv2D(
			    filters = 64,
			kernel_size = [7, 7],
			    strides = [2, 2],
			    padding = 'same',
			 activation = keras.layers.LeakyReLU( alpha=0.1 ),
			       name = 'block1_conv'
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = 'block1_pool'
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = 'block1_lrn'
			)( tensor )

	# Block 2
	tensor = keras.layers.Conv2D(
		    filters = 192,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = 'block2_conv'
		)( tensor )
	if include_lrn:
		tensor = pykeras.layers.LRN(
			name = 'block2_lrn'
			)( tensor )
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = 'block2_pool'
		)( tensor )

	# Block 3
	tensor = reduce_block(
		tensor,
		[128, 256],
		['block3_conv1', 'block3_conv2']
		)
	tensor = reduce_block(
		tensor,
		[256, 512],
		['block3_conv3', 'block3_conv4']
		)
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = 'block3_pool'
		)( tensor )

	# Block 4
	tensor = reduce_block(
		tensor,
		[256, 512],
		['block4_conv1', 'block4_conv2']
		)
	tensor = reduce_block(
		tensor,
		[256, 512],
		['block4_conv3', 'block4_conv4']
		)
	tensor = reduce_block(
		tensor,
		[256, 512],
		['block4_conv5', 'block4_conv6']
		)
	tensor = reduce_block(
		tensor,
		[256, 512],
		['block4_conv7', 'block4_conv8']
		)
	tensor = reduce_block(
		tensor,
		[512, 1024],
		['block4_conv9', 'block4_conv10']
		)
	tensor = keras.layers.MaxPooling2D(
		pool_size = [2, 2],
		  strides = [2, 2],
		  padding = 'same',
		     name = 'block4_pool'
		)( tensor )

	# Block 5
	tensor = reduce_block(
		tensor,
		[512, 1024],
		['block5_conv1', 'block5_conv2']
		)
	tensor = reduce_block(
		tensor,
		[512, 1024],
		['block5_conv3', 'block5_conv4']
		)
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = 'block5_conv5'
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [2, 2],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = 'block5_conv6'
		)( tensor )

	# Block 6
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = 'block6_conv1'
		)( tensor )
	tensor = keras.layers.Conv2D(
		    filters = 1024,
		kernel_size = [3, 3],
		    strides = [1, 1],
		    padding = 'same',
		 activation = keras.layers.LeakyReLU( alpha=0.1 ),
		       name = 'block6_conv2'
		)( tensor )

	return tensor

# def body ( ... )

# --------------------------------------------------

def head ( tensor, number_of_cells=1, number_of_detectors_per_cell=1, number_of_classes=1000, classifier_activation='linear' ):
	"""
	Head of YOLO V1.

	Arguments:
		tensor         (`tensorflow.Tensor`): Output tensor of the body.	
		number_of_cells              (`int`): Number of cells on the width and height of the image.
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell.
		number_of_classes            (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.

	Returns:
		`tensorflow.Tensor`: Tensor containing the classification result, shape: [number_of_cells, number_of_cells, number_of_classes + number_of_detectors_per_cell * 5].
	"""
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.type_is( number_of_classes, int )
	assert pytools.assertions.type_in( classifier_activation, (str, type(None)) )

	# Compute the number of units required to compute predictions
	units        =   number_of_cells * number_of_cells * ( number_of_classes + number_of_detectors_per_cell * 5 )
	output_shape = ( number_of_cells,  number_of_cells,    number_of_classes + number_of_detectors_per_cell * 5 )

	# Head
	tensor = keras.layers.Flatten(
		name = 'flatten'
		)( tensor )
	tensor = keras.layers.Dense(
		     units = 4096,
		activation = keras.layers.LeakyReLU( alpha=0.1 ),
		      name = 'fc1'
		)( tensor )
	tensor = keras.layers.Dropout(
		rate = 0.5,
		name = 'dropout'
		)( tensor )
	tensor = keras.layers.Dense(
		     units = units,
		activation = classifier_activation,
		      name = 'fc2'
		)( tensor )
	tensor = keras.layers.Reshape(
		target_shape = output_shape,
		 input_shape = (units,),
		        name = 'reshape'
		)( tensor )
	return tensor

# def head ( ... )

# --------------------------------------------------

def YoloV2 (
	# Architecture options
	include_lrn = False,
	include_top = True,
	# Input
	input_tensor = None,
	input_shape = (448, 448, 3),
	input_dtype = 'float32',
	input_name = 'image',
	# Output
	number_of_cells = 1,
	number_of_detectors_per_cell = 1,
	number_of_classes = 20,
	classifier_activation = 'linear',
	output_name = 'yolo_v1'
	):
	"""
	Create the Yolo V1 Model.
	
	Arguments:
		include_lrn                 (`bool`): If `True` local response nomalization is applied in the first two blocks.
		number_of_cells              (`int`): Number of cells on the width and height of the image.
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell.
		number_of_classes            (`int`): Number of classes to separate.
		classifier_activation (`str`/`None`): Activation of the last dense layer.

	Returns:
		`keras.Model`: Yolo V1 model.
	"""

	# Create input layer
	image  = utils.get_or_create_image_input( input_tensor, input_shape, input_dtype, input_name )
	tensor = image
	
	# Resize image to a fix size of 448x448
	tensor = keras.layers.experimental.preprocessing.Resizing(
		        width = 448,
		       height = 448,
		interpolation = 'bilinear'
		)( tensor )

	# Backbone
	tensor = backbone( tensor, include_lrn )
		
	# Top
	if include_top:
		tensor = head( tensor, number_of_cells, number_of_detectors_per_cell, number_of_classes, classifier_activation )

	# Rename
	tensor = pykeras.layers.Rename(
		name = output_name
		)( tensor )

	# Get the input of the input
	if input_tensor is not None:
		image = keras.utils.get_source_inputs( input_tensor )

	# Create the model
	return keras.Model(
		    inputs = { input_name: image },
		   outputs = { output_name: tensor },
		      name = 'yolo_v1'
		)

# def YoloV2 ( ... )