# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import glob
import re

# INTENALS
import pytools

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

# --------------------------------------------------

"""
Pattern used to create keras checkpoint using model checkpoint callback.
"""
CHECKPOINT_PATTERN = 'model-{:04d}-{:10.6}.keras'

# --------------------------------------------------

"""
Regex used to parse checkpoint names
"""
CHECKPOINT_REGEX = re.compile( r'(?:(?:[a-z]|[A-Z]|[0-9])+)\-(?P<epoch>[0-9]+)\-(?P<loss>[0-9]+(?:\.[0-9]+|0+inf))\.keras' )

# --------------------------------------------------

"""
Pattern used to glob for all checkpoinst in a directory.
"""
CHECKPOINT_GLOB_PATTERN = 'model-*-*.keras'

"""
Pattern used to glob for all checkpoinst at the given epoch in a directory.
"""
CHECKPOINT_GLOB_PATTERN_EPOCH = 'model-{:04d}-*.keras'

# ##################################################
# ###             PRIVATE FUNCTIONS              ###
# ##################################################

# --------------------------------------------------

def _checkpoint_mapper ( checkpoint ):
	"""
	Maps a checkpoint filepath to its tuple decomposition.

	Arguments:
		checkpoint (`str`): Checkpoint filepath.

	Returns:
		`tuple` of `4` `str`s: checkpoint, model, epoch, loss
	"""
	filepath  = pytools.path.FilePath( checkpoint )
	fullmatch = CHECKPOINT_REGEX.fullmatch( filepath.name() )
	
	if fullmatch:
		return (filepath, fullmatch.group('epoch'), fullmatch.group('loss'))
	
	else:
		raise RuntimeError( 'Failed to parse checkpoint {}'.format(filepath) )

# def _checkpoint_mapper ( checkpoint )

# --------------------------------------------------

def _epoch_sorter ( checkpoint ):
	"""
	Function used to sort checkpoints based on the epoch.
	
	Arguments:
		checkpoint (`tuple` of `4` `str`s): checkpoint, model, epoch, loss.
	
	Returns:
		`str`: epoch.
	"""
	return checkpoint[ 1 ]

# def _epoch_sorter ( checkpoint )

# --------------------------------------------------

def _loss_sorter ( checkpoint ):
	"""
	Fonction used to sort the checkpoints based on loss value.

	Arguments:
		checkpoint (`tuple` of `4` `str`s): checkpoint, model, epoch, loss.
	
	Returns:
		`str`: loss.
	"""
	return checkpoint[ 2 ]

# def _loss_sorter ( checkpoint )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def find_latest_checkpoint ( directory_path, epoch=0 ):
	"""
	Finds the last checkpoint (largest epoch, smallest loss) in the given directory.

	Arguments:
		directory_path (`pytools.path.DirectoryPath`): Directory.
		epoch                                 (`int`): Specific epoch at which to load the best checkpoint.

	Returns:
		`pytools.path.FilePath`: Checkpoint filepath.
	"""
	assert pytools.assertions.type_is_instance_of( directory_path, pytools.path.DirectoryPath )
	
	# Compose checkpoints filepath pattern
	if epoch > 0:
		filepath_pattern = directory_path + pytools.path.FilePath.format( CHECKPOINT_GLOB_PATTERN_EPOCH, epoch )
	else:
		filepath_pattern = directory_path + pytools.path.FilePath( CHECKPOINT_GLOB_PATTERN )
	
	# Find checkpoints
	checkpoints = glob.glob( str(filepath_pattern) )

	# If checkpoints where found
	if checkpoints:

		# Map checkpoints
		checkpoints = list(map( _checkpoint_mapper, checkpoints ))

		# Sort checkpoints
		checkpoints.sort( key=_loss_sorter, reverse=True )
		checkpoints.sort( key=_epoch_sorter )

		return checkpoints[-1][0]

	else:
		return pytools.path.FilePath()

# def find_latest_checkpoint ( directory_path, epoch )

# --------------------------------------------------

def load_model_checkpoint ( model, checkpoint, by_name=False, skip_mismatch=False ):
	"""
	Load the models weights from the given checkpoint file,
	or from the latest checkpoint file found in the given directory.

	Arguments:
		model  (`keras.Model`): Keras model of which to load the weights.
		checkpoint     (`str`): Checkpoint file or directory containing one.
		by_name	      (`bool`): Whether to load weights by name or by topological order.
		skip_mismatch (`bool`): Whether to skip loading of layers where there is a mismatch in the number of weights, or a mismatch in the shape of the weight.
	
	Returns:
		keras.Model`: Keras model.
	"""
	
	# If checkpoint is specified ?
	if checkpoint:
		
		# Convert to a generic path
		path = pytools.path.Path( checkpoint )

		# Is the given checkpoint a directory ?
		if path.is_a_directory():
			
			# Find the latest checkpoint in the directory
			checkpoint = find_latest_checkpoint( checkpoint )
			
			# Load weights
			model.load_weights( checkpoint, by_name=by_name, skip_mismatch=skip_mismatch )

		# Is the given checkpoint a file ?		
		elif path.is_a_file():
			
			# Convert to a file path
			path = pytools.path.FilePath( checkpoint )
			
			if not path.exists():
				raise ValueError( "The file '{}' does not exist!".format(checkpoint) )

			if checkpoint.extension() != '.keras':
				raise ValueError( "The file '{}' is a not a keras checkpoint with extension '.keras'!".format(checkpoint) )
			
			model.load_weights( checkpoint, by_name=by_name, skip_mismatch=skip_mismatch )
		
		# Error
		else:
			raise ValueError( "'{}' is neither a file nor a directory !".format(checkpoint) )		
		
		# if path.is_a_directory()

	# if checkpoint

	return model

# def load_model_checkpoint ( model, checpoint ):