# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .example_blueprint_data import ExampleBlueprintData

# ##################################################
# ###        CLASS DATASET-BLUEPRINT-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = [
		'example_blueprint_data',
		'number_of_examples',
		'compression_type',
		'compression_level'
		]
	)
class DatasetBlueprintData ( pytools.serialization.Serializable ):
	"""
	Data blueprint data is used to describe the structure of the records in a tfrecords file.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example_blueprint_data=ExampleBlueprintData(), number_of_examples=0, compression_type='GZIP', compression_level=9 ):
		"""
		Initializes a new instance of the example blueprint data class.

		Arguments:
			self                   (`pykeras.records.DatasetBlueprintData`): Instance to initialize.
			example_blueprint_data (`pykeras.records.ExampleBlueprintData`): Example blueprint data.
			number_of_examples                                      (`int`): Number of examples in the tf records dataset.
			compression_type                                        (`str`): TF records compression method ['GZIP', 'ZLIB', '']
			compression_level                                       (`int`): Compression level [0, 9]
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )
		assert pytools.assertions.type_is_instance_of( example_blueprint_data, ExampleBlueprintData )
		assert pytools.assertions.type_is( number_of_examples, int )
		assert pytools.assertions.type_is( compression_type, str )
		assert pytools.assertions.value_in( compression_type, ('GZIP', 'ZLIB', '') )
		assert pytools.assertions.type_is( compression_level, int )
		assert pytools.assertions.value_in( compression_level, range(10) )

		super( DatasetBlueprintData, self ).__init__()

		self._example_blueprint_data = copy.deepcopy( example_blueprint_data )
		self._number_of_examples     = number_of_examples
		self._compression_type       = compression_type
		self._compression_level      = compression_level

	# def __init__ ( self, example_blueprint_data, number_of_examples, compression_type, compression_level )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def example_blueprint_data ( self ):
		"""
		Example blueprint data (`pykeras.records.ExampleBlueprintData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )

		return self._example_blueprint_data
	
	# def example_blueprint_data ( self )

	# --------------------------------------------------

	@example_blueprint_data.setter
	def example_blueprint_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, ExampleBlueprintData )

		self._example_blueprint_data = copy.deepcopy( value )
	
	# def example_blueprint_data ( self, value )

	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in the tf records dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )

		return self._number_of_examples
	
	# def number_of_examples ( self )
	
	# --------------------------------------------------

	@number_of_examples.setter
	def number_of_examples ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )
		assert pytools.assertions.type_is( value, int )

		self._number_of_examples = value
	
	# def number_of_examples ( self, value )

	# --------------------------------------------------

	@property
	def compression_type ( self ):
		"""
		TF records compression method (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )

		return self._compression_type
	
	# def compression_type ( self )
	
	# --------------------------------------------------

	@compression_type.setter
	def compression_type ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ('GZIP', 'ZLIB', '') )

		self._compression_type = value
	
	# def compression_type ( self, value )

	# --------------------------------------------------

	@property
	def compression_level ( self ):
		"""
		Compression level [0, 9] (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )

		return self._compression_level
	
	# def compression_level ( self )
	
	# --------------------------------------------------

	@compression_level.setter
	def compression_level ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(1, 10) )

		self._compression_level = value
	
	# def compression_level ( self, value )
	
	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features of the example of this dataset (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprintData )

		return self.example_blueprint_data.features_names
	
	# def features_names ( self )

# class DatasetBlueprintData ( pytools.serialization.Serializable )
