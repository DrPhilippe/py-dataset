# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint_data import FeatureBlueprintData

# ##################################################
# ###          CLASS FEATURE-BLUEPRINT           ###
# ##################################################

class FeatureBlueprint:
	"""
	Class used to serialize / deserialize a feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data ):
		"""
		Initializes a new instance of the feature class.

		Arguments:
			self     (`pykeras.records.FeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.FeatureBlueprintData`): Data describing the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, FeatureBlueprintData )

		self._data = copy.deepcopy( data )

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing the feature (`pykeras.records.FeatureBlueprintData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )

		return self._data
	
	# def data ( self )

	# --------------------------------------------------

	@data.setter
	def data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( value, FeatureBlueprintData )

		self._data = copy.deepcopy( value )
	
	# def data ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Returns the specifications of this feature blueprint.

		This is used to create the blueprint specifiactions
		that is therms used to parse records.
		
		Arguments:
			self (`pykeras.records.FeatureBlueprint`): Feature blueprint of which to get the specifications.

		Returns:
			`tensorflow.io.FixedLenFeature` or `tensorflow.io.VarLenFeature`.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )
		
		raise NotImplementedError()

	# def get_specifications ( self )
	
	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a tensorflow feature.

		Arguments:
			self (`pykeras.records.FeatureBlueprint`): Feature blueprint used to convert the feature.
			feature     (`pydataset.dataset.Feature`): Feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )

		raise NotImplementedError()

	# def convert_feature ( self, feature )
	
	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this feature blueprint.
		
		Arguments:
			self (`pykeras.records.FeatureBlueprint`): Feature blueprint used to parse the feature.
			record              (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprint )

		raise NotImplementedError()

	# def parse_feature_record ( self, record )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): Feature blueprint class type.
			feature (`pydataset.dataset.Feature`): Feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the feature blueprint.
		"""
		raise NotImplementedError()

	# def from_feature ( cls, feature )

# class FeatureBlueprint