# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .ndarray_feature_blueprint_data import NDArrayFeatureBlueprintData

# ##################################################
# ###     CLASS MESH-FEATURE-BLUEPRINT-DATA      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshFeatureBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = []
	)
class MeshFeatureBlueprintData ( NDArrayFeatureBlueprintData ):
	"""
	Data describing an mesh feature blueprint.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self ):
		"""
		Initializes a new instance of the mesh feature blueprint data class.

		Arguments:
			self (`pykeras.records.MeshFeatureBlueprintData`): Instance to initialize.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshFeatureBlueprintData )

		super( MeshFeatureBlueprintData, self ).__init__( pydataset.data.ShapeData(-1, 3), numpy.dtype('float32'), None )

	# def __init__ ( ... )

# class MeshFeatureBlueprintData ( NDArrayFeatureBlueprintData )
