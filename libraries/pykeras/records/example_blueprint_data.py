# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint_data import FeatureBlueprintData

# ##################################################
# ###            CLASS BLUEPRINT-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ExampleBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = [
		'features_blueprints_data'
		]
	)
class ExampleBlueprintData ( pytools.serialization.Serializable ):
	"""
	Example blueprint data are used to describe the structure of the records in a tfrecords file.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, features_blueprints_data={} ):
		"""
		Initializes a new instance of the example blueprint data class.

		Arguments:
			self                        (`pykeras.records.ExampleBlueprintData`): Instance to initialize.
			features_blueprints_data (`dict` of `str` to `FeatureBlueprintData`): Features of the example.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprintData )
		assert pytools.assertions.type_is( features_blueprints_data, dict )
		assert pytools.assertions.dictionary_types_are( features_blueprints_data, str, FeatureBlueprintData )

		super( ExampleBlueprintData, self ).__init__()

		self._features_blueprints_data = copy.deepcopy( features_blueprints_data )

	# def __init__ ( self, features_blueprints_data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def features_blueprints_data ( self ):
		"""
		Features of the example (`dict` of `str` to `FeatureBlueprintData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprintData )

		return self._features_blueprints_data
	
	# def features_blueprints_data ( self )

	# --------------------------------------------------

	@features_blueprints_data.setter
	def features_blueprints_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprintData )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, FeatureBlueprintData )

		self._features_blueprints_data = copy.deepcopy( value )
	
	# def features_blueprints_data ( self, value )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features described in this example blueprint (`list` of `str`).		
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprintData )

		return list( self._features_blueprints_data.keys() )
	
	# def features_names ( self )
	
# class ExampleBlueprintData ( pytools.serialization.Serializable )
