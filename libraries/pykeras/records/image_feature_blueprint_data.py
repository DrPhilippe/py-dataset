# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .ndarray_feature_blueprint_data import NDArrayFeatureBlueprintData

# ##################################################
# ###     CLASS IMAGE-FEATURE-BLUEPRINT-DATA     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImageFeatureBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = [
		'extension',
		'param'
		]
	)
class ImageFeatureBlueprintData ( NDArrayFeatureBlueprintData ):
	"""
	Data describing an image feature blueprint.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, shape=pydataset.data.ShapeData(), dtype=numpy.dtype('uint8'), default_value=None, extension='.jpeg', param=90 ):
		"""
		Initializes a new instance of the image feature blueprint data class.

		Arguments:
			self (`pykeras.records.ImageFeatureBlueprintData`): Instance to initialize.
			shape                 (`pydataset.data.ShapeData`): Shape of the feature.
			dtype                              (`numpy.dtype`): Data type of the ndarray.
			default_value             (`None`/`numpu.ndarray`): Default value, if missing from one record.
			extension                                  (`str`): Image file extension corresponding to the method used to compress the image.
			param                                      (`int`): Compression or quality level depending on the extension.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( shape, (pydataset.data.ShapeData, list) )
		assert pytools.assertions.type_is_instance_of( dtype, numpy.dtype )
		assert pytools.assertions.type_is_instance_of( default_value, (numpy.ndarray, type(None)) )
		assert pytools.assertions.type_is( extension, str )
		assert pytools.assertions.value_in( extension.lower(), ['.jpeg', '.jpg', '.jpe', '.png'] )
		assert pytools.assertions.type_is( param, int )

		super( ImageFeatureBlueprintData, self ).__init__( shape, dtype, default_value )

		self._extension = extension.lower()
		self._param = param

	# def __init__ ( self, shape, dtype, default_value, extension, param )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def extension ( self ):
		"""
		Image file extension corresponding to the method used to compress the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprintData )

		return self._extension
	
	# def extension ( self )

	# --------------------------------------------------

	@extension.setter
	def extension ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprintData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value.lower(), ['.jpeg', '.jpg', '.jpe', '.png'] )

		self._extension = value.lower()
	
	# def extension ( self, value )

	# --------------------------------------------------

	@property
	def param ( self ):
		"""
		Compression or quality level depending on the extension (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprintData )

		return self._param
	
	# def param ( self )

	# --------------------------------------------------

	@param.setter
	def param ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, int )

		self._param = value
	
	# def param ( self, value )

# class ImageFeatureBlueprintData ( NDArrayFeatureBlueprintData )
