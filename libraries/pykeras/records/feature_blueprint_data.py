# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###        CLASS FEATURE-BLUEPRINT-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FeatureBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = []
	)
class FeatureBlueprintData ( pytools.serialization.Serializable ):
	"""
	Data describing a feature blueprint.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self ):
		"""
		Initializes a new instance of the feature data class.

		Arguments:
			self (`pykeras.records.FeatureBlueprintData`): Instance to initialize.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureBlueprintData )

		super( FeatureBlueprintData, self ).__init__()

	# def __init__ ( self )

# class FeatureBlueprintData ( pytools.serialization.Serializable )