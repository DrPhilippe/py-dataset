# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import random
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.path
import pytools.tasks

# LOCALS
from .dataset_blueprint         import DatasetBlueprint
from .records_exporter_settings import RecordsExporterSettings

# ##################################################
# ###           CLASS RECORDS-EXPORTER           ###
# ##################################################

class RecordsExporter ( pytools.tasks.Task ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, filter_fn=None, **kwargs ):
		"""
		Initializes a new task instance.

		Arguments:
			self             (`pykeras.records.RecordsExporter`): Rexords exporter to initialize.
			settings (`pykeras.records.RecordsExporterSettings`): Settings of the records exporter.

		Named Arguments:
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the records exporter.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the records exporter.
			parent                      (`None`/`pytools.tasks.Task`): Parent task.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporter )
		assert pytools.assertions.type_is_instance_of( settings, RecordsExporterSettings )

		super( RecordsExporter, self ).__init__( settings, **kwargs )
		
		self._iterator  = None
		self._blueprint = None
		self._writer    = None
		self._filter_fn = filter_fn

	# def __init__ ( self, settings, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this records exporter.

		Arguments:
			self (`pykeras.records.RecordsExporter`): The records exporter to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporter )

		super( RecordsExporter, self ).start()

		# Log raw settings
		if self.logger:
			self.logger.log( 'RECORDS EXPORTER' )
			self.logger.log( 'raw settings:' )
			self.logger.log( "    directory:          '{}'", self.settings.directory )
			self.logger.log( "    records filename:   '{}'", self.settings.records_filename )
			self.logger.log( "    records filepath:   '{}'", self.settings.records_filepath )
			self.logger.log( "    blueprint filename: '{}'", self.settings.blueprint_filename )
			self.logger.log( "    blueprint filepath: '{}'", self.settings.blueprint_filepath )
			self.logger.log( "    url search pattern: '{}'", self.settings.url_search_pattern )
			self.logger.log( "    features names:     {}", self.settings.features_names )
			self.logger.log( "    shuffle:            '{}'", self.settings.shuffle )
			self.logger.log( "    compression type:   '{}'", self.settings.compression_type )
			self.logger.log( "    compression lvele:  {}", self.settings.compression_level )
			self.logger.end_line()
			self.logger.flush()

		# If they are no features to export
		if len(self.settings.features_names) <= 0:

			# Log a warning and exit
			if self.logger:
				self.logger.end_line()
				self.logger.log( 'WARNING: 0 features to export (or less) !' )
				self.logger.log( 'please check the "features_names" you provided using records exporter settings.' )
				self.logger.log( 'features names: {}', self.settings.features_names )
				self.logger.end_line()
			self.logger.flush()
			return

		# Load the iterator
		self._iterator = pydataset.dataset.Iterator( self.settings.url_search_pattern )

		# Log iterator infos
		if self.logger:
			self.logger.log( 'iterator:' )
			self.logger.log( '    number of examples: {}', self._iterator.count )
			self.logger.log( '    dataset:            {}', self._iterator.dataset.absolute_url )
			self.logger.log( '    root_group:         {}', self._iterator.root_group.url )
			self.logger.end_line()
			self.logger.flush()

		# If they are no examples to export
		if self._iterator.count <= 0:

			# Log a warning and exit
			if self.logger:
				self.logger.end_line()
				self.logger.log( 'WARNING: 0 examples to export (or less) !' )
				self.logger.log( 'Please check the "url_search_pattern" you provided using records exporter settings.' )
				self.logger.log( 'url search pattern: {}', self.settings.url_search_pattern )
				self.logger.end_line()
			self.logger.flush()
			return

		# Update directory
		if not self.settings.directory:
			backend_name, backend_url, dataset_name, content_url = pydataset.dataset.url.split( self._iterator.dataset.absolute_url )
			self.settings.directory = pytools.path.DirectoryPath(backend_url) + pytools.path.DirectoryPath(dataset_name)

		# Update the name of the tfrecord file
		records_filename = self.settings.records_filename
		if '{}' in records_filename:
			records_filename = records_filename.format( self._iterator.root_group.name )
		self.settings.records_filename = records_filename

		# Update the name of the blueprint json file
		blueprint_filename = self.settings.blueprint_filename
		if '{}' in blueprint_filename:
			blueprint_filename = blueprint_filename.format( self._iterator.root_group.name )
		self.settings.blueprint_filename = blueprint_filename
		
		# Log updated settings
		if self.logger:
			self.logger.log( 'updated settings:' )
			self.logger.log( "    directory:          '{}'", self.settings.directory )
			self.logger.log( "    records filename:   '{}'", self.settings.records_filename )
			self.logger.log( "    records filepath:   '{}'", self.settings.records_filepath )
			self.logger.log( "    blueprint filename: '{}'", self.settings.blueprint_filename )
			self.logger.log( "    blueprint filepath: '{}'", self.settings.blueprint_filepath )
			self.logger.log( "    url search pattern: '{}'", self.settings.url_search_pattern )
			self.logger.log( "    features names:     {}", self.settings.features_names )
			self.logger.log( "    shuffle:             {}", self.settings.shuffle )
			self.logger.log( "    compression type:    {}", self.settings.compression_type )
			self.logger.log( "    compression lvele:   {}", self.settings.compression_level )
			self.logger.end_line()
			self.logger.flush()

		# reset other fields
		self._blueprint = None
		self._writer = None

	# def start ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this records exporter.

		Arguments:
			self (`pykeras.records.RecordsExporter`): The records exporter to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporter )
		
		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# Open the tf records writer
		options = tensorflow.io.TFRecordOptions(
			 compression_type = self.settings.compression_type,
			compression_level = self.settings.compression_level
			)
		self._writer = tensorflow.io.TFRecordWriter( str(self.settings.records_filepath), options )
		
		# Export example with it
		with self._writer:

			# example indexes
			number_of_examples = self._iterator.count
			indexes            = list( range(number_of_examples) )
			final_number_of_examples = 0

			# Shuffle ?
			if self.settings.shuffle:
				random.shuffle( indexes )

			# Go through examples
			for i in range(number_of_examples):

				# Get the current example
				example_index = indexes[ i ]
				example = self._iterator.at( example_index )

				skip = False
				if self._filter_fn is not None:
					skip = self._filter_fn( example )
				if skip:
					continue
				final_number_of_examples += 1

				# Create the blueprint
				if self._blueprint is None:
					self._blueprint = DatasetBlueprint.from_example(
						           example = example,
						    features_names = self.settings.features_names,
						number_of_examples = number_of_examples,
						  compression_type = self.settings.compression_type,
						 compression_level = self.settings.compression_level
						)
					self.settings.blueprint_filepath.serialize_json( self._blueprint.data )
				
				# Convert to tensorflow example
				tf_example = self._blueprint.convert_example( example )

				# Write it to file
				self._writer.write( tf_example.SerializeToString() )

				# Update progression
				if self.progress_tracker:
					self.progress_tracker.update( float(i) / float(number_of_examples) )

			# for i in range( number_of_examples )

		# with self._writer

		# Update the blueprint
		self._blueprint.data.number_of_examples = final_number_of_examples
		self.settings.blueprint_filepath.serialize_json( self._blueprint.data )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

		# Log updated settings
		if self.logger:
			self.logger.log( 'final_number_of_examples: {}', final_number_of_examples )
			
	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, filter_fn=None, logger=None, progress_tracker=None, **kwargs ):
		"""
		Creates a new instance of the task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of task.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			**kwargs                                         (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, filter_fn=filter_fn, logger=logger, progress_tracker=progress_tracker )

	# def create ( ... )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a records exporter.
		
		Named Arguments:
			directory   (`pytools.path.DirectoryPath`/`str`): Path of the directory where to create the tfrecords file.
			records_filename                         (`str`): Name of the tfrecords file to create.
			blueprint_filename                       (`str`): Name of the blubrint json file to create.
			url_search_pattern                       (`str`): URL pattern used to search for examples to export.
			features_names                 (`list` of `str`): Names of the features to include for each examples.
			name                                     (`str`): Name of the records exporter.

		Returns:
			`pykeras.records.RecordsExporterSettings`: The settings.
		"""
		return RecordsExporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RecordsExporter ( pytools.tasks.Task )