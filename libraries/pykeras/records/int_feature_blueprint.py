# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint                    import FeatureBlueprint
from .int_feature_blueprint_data           import IntFeatureBlueprintData
from .register_feature_blueprint_attribute import RegisterFeatureBlueprintAttribute

# ##################################################
# ###        CLASS INT-FEATURE-BLUEPRINT         ###
# ##################################################

@RegisterFeatureBlueprintAttribute( pydataset.dataset.IntFeatureData )
@RegisterFeatureBlueprintAttribute( IntFeatureBlueprintData )
class IntFeatureBlueprint ( FeatureBlueprint ):
	"""
	Class used to serialize / deserialize a int feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=IntFeatureBlueprintData() ):
		"""
		Initializes a new instance of the bytes list feature class.

		Arguments:
			self     (`pykeras.records.IntFeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.IntFeatureBlueprintData`): Data describing the bytes list feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, IntFeatureBlueprintData )

		super( IntFeatureBlueprint, self ).__init__( data )

	# def __init__ ( self, data )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Returns the specifications of this int feature blueprint.
		
		Arguments:
			self (`pykeras.records.IntFeatureBlueprint`): Int feature blueprint of which to get the specifications.

		Returns:
			`tensorflow.io.FixedLenFeature`: The specifications.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureBlueprint )

		# return tensorflow.io.FixedLenFeature( [], 'int64', self.data.default_value )
		return tensorflow.io.FixedLenFeature( [], 'int64', None )

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a bytes list.

		Arguments:
			self (`pykeras.records.IntFeatureBlueprint`): Feature blueprint used to convert the feature.
			feature        (`pydataset.dataset.Feature`): Int feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.IntFeatureData )

		# Return the feature as an int64 list
		return tensorflow.train.Feature( int64_list=tensorflow.train.Int64List( value=[feature.value] ) )

	# def convert_feature ( self, feature )
	
	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this int feature blueprint.
		
		Arguments:
			self (`pykeras.records.IntFeatureBlueprint`): Feature blueprint used to parse the int feature.
			record                 (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureBlueprint )

		return record

	# def parse_feature_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): Int feature blueprint class type.
			feature (`pydataset.dataset.Feature`): Int feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the feature blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.IntFeatureData )


		data = IntFeatureBlueprintData(
			default_value = 0
			)

		return IntFeatureBlueprint( data )

	# def from_feature ( cls, feature )

# class IntFeatureBlueprint ( FeatureBlueprint )