# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint                    import FeatureBlueprint
from .ndarray_feature_blueprint_data       import NDArrayFeatureBlueprintData
from .register_feature_blueprint_attribute import RegisterFeatureBlueprintAttribute

# ##################################################
# ###      CLASS NDARRAY-FEATURE-BLUEPRINT       ###
# ##################################################

@RegisterFeatureBlueprintAttribute( pydataset.dataset.NDArrayFeatureData )
@RegisterFeatureBlueprintAttribute( NDArrayFeatureBlueprintData )
class NDArrayFeatureBlueprint ( FeatureBlueprint ):
	"""
	Class used to serialize / deserialize a ndarray feature.

	numpy ndarrays are serialized as a single value bytes list feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=NDArrayFeatureBlueprintData() ):
		"""
		Initializes a new instance of the bytes list feature class.

		Arguments:
			self     (`pykeras.records.NDArrayFeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.NDArrayFeatureBlueprintData`): Data describing the ndarray feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, NDArrayFeatureBlueprintData )

		super( NDArrayFeatureBlueprint, self ).__init__( data )

	# def __init__ ( self, data )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Returns the specifications of this ndarray feature blueprint.

		This is used to create the blueprint specifiactions
		that is therms used to parse records.
		
		Arguments:
			self (`pykeras.records.NDArrayFeatureBlueprint`): NDArray feature blueprint of which to get the specifications.

		Returns:
			`tensorflow.io.FixedLenFeature`: The specifications.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprint )
		
		# if self.data.default_value is not None:
		# 	default_value = self.data.default_value.tostring()
		# 	default_value = bytes( tensorflow.compat.as_bytes( default_value ) )
			
		# else:
		# 	default_value = None

		return tensorflow.io.FixedLenFeature( [], 'string', None )

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a tensorflow feature.

		Arguments:
			self (`pykeras.records.NDArrayFeatureBlueprint`): NDArray feature blueprint used to convert the feature.
			feature            (`pydataset.dataset.Feature`): NDArray Feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.NDArrayFeatureData )

		# Serialize the ndarray
		value = feature.value.tostring()
		value = bytes( tensorflow.compat.as_bytes( value ) )

		# Return it as text bytes
		return tensorflow.train.Feature( bytes_list=tensorflow.train.BytesList( value=[value] ) )

	# def convert_feature ( self, feature )
	
	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this ndarray feature blueprint.
		
		Arguments:
			self (`pykeras.records.NDArrayFeatureBlueprint`): Feature blueprint used to parse the ndarray feature.
			record                     (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprint )

		feature = tensorflow.io.decode_raw( record, self.data.dtype, name='decode_ndarray' )
		feature = tensorflow.reshape( feature, self.data.shape.dimensions, name='reshape_ndarray' )
		return feature

	# def parse_feature_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): NDArray feature blueprint class type.
			feature (`pydataset.dataset.Feature`): NDArray feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the ndarray feature blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.NDArrayFeatureData )

		data = NDArrayFeatureBlueprintData(
			        shape = feature.data.shape,
			        dtype = numpy.dtype( feature.data.dtype ),
			default_value = numpy.zeros( feature.data.shape.dimensions, feature.data.dtype )
			)

		return NDArrayFeatureBlueprint( data )

	# def from_feature ( cls, feature )

# class NDArrayFeatureBlueprint ( FeatureBlueprint )