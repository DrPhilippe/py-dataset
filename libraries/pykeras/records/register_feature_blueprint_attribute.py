# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .features_blueprints_factory import FeaturesBlueprintsFactory

# ##################################################
# ### CLASS REGISTER-FEATURE-BLUEPRINT-ATTRIBUTE ###
# ##################################################

class RegisterFeatureBlueprintAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Registers a class by associating it to a given typename.
	"""
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, cls ):
		"""
		Initializes a new instance of the attribute.

		Arguments:
			self (`pykeras.records.RegisterFeatureBlueprintAttribute`): Instance to initialize.
			cls                                               (`type`): Type used to reference the blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterFeatureBlueprintAttribute )
		assert pytools.assertions.type_is_instance_of( cls, type )

		super( RegisterFeatureBlueprintAttribute, self ).__init__(
			   group = FeaturesBlueprintsFactory.group_name,
			typename = cls.__name__
			)

	# def __init__ ( self, typename )
		
# class RegisterFeatureBlueprintAttribute ( pytools.factory.RegisterClassAttribute )
