# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization
import pytools.tasks

# ##################################################
# ###      CLASS RECORDS-EXPORTER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RecordsExporterSettings',
	   namespace = 'pykeras.records',
	fields_names = [
		'directory',
		'records_filename',
		'blueprint_filename',
		'url_search_pattern',
		'features_names',
		'compression_type',
		'compression_level',
		'shuffle'
		]
	)
class RecordsExporterSettings ( pytools.tasks.TaskSettings ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		directory='',
		records_filename='{}.tfrecords',
		blueprint_filename='{}.json',
		url_search_pattern='',
		features_names=[],
		compression_type='GZIP',
		compression_level=9,
		shuffle=True,
		name='records-exporter'
		):
		"""
		Initializes a new instance of the `pykeras.records.RecordsExporterSettings` class.

		Arguments:
			self (`pykeras.records.RecordsExporterSettings`): Instance to initialize.
			directory   (`pytools.path.DirectoryPath`/`str`): Path of the directory where to create the tfrecords file.
			records_filename                         (`str`): Name of the tfrecords file to create.
			blueprint_filename                       (`str`): Name of the blubrint json file to create.
			url_search_pattern                       (`str`): URL pattern used to search for examples to export.
			features_names                 (`list` of `str`): Names of the features to include for each examples.
			compression_type                         (`str`): TF records compression method ['GZIP', 'ZLIB', ''].
			compression_level                        (`int`): Compression level [0, 9].
			shuffle                                 (`bool`): If `True` examples are shuffled.
			name                                     (`str`): Name of the records exporter.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is_instance_of( directory, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( records_filename, str )
		assert records_filename.endswith( '.tfrecords' )
		assert pytools.assertions.type_is( blueprint_filename, str )
		assert blueprint_filename.endswith( '.json' )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )
		assert pytools.assertions.type_is( compression_type, str )
		assert pytools.assertions.value_in( compression_type, ('GZIP', 'ZLIB', '') )
		assert pytools.assertions.type_is( compression_level, int )
		assert pytools.assertions.value_in( compression_level, range(10) )
		assert pytools.assertions.type_is( shuffle, bool )
		assert pytools.assertions.type_is( name, str )

		super( RecordsExporterSettings, self ).__init__( name )

		self._directory          = pytools.path.DirectoryPath( directory ) if isinstance( directory, str ) else directory
		self._records_filename   = records_filename
		self._blueprint_filename = blueprint_filename
		self._url_search_pattern = url_search_pattern
		self._features_names     = list( features_names )
		self._compression_type   = compression_type
		self._compression_level  = compression_level
		self._shuffle            = shuffle

	# def __init__ ( self, directory, records_filename, blueprint_filename, url_search_pattern, features_names, shuffle, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def directory ( self ):
		"""
		Path of the directory where to create the tfrecords file (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._directory
	
	# def directory ( self )

	# --------------------------------------------------

	@directory.setter
	def directory ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._directory = pytools.path.DirectoryPath( value ) if isinstance( value, str ) else value
	
	# def directory ( self, value )

	# --------------------------------------------------

	@property
	def records_filename ( self ):
		"""
		Name of the tfrecords file to create (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._records_filename
	
	# def records_filename ( self )

	# --------------------------------------------------

	@records_filename.setter
	def records_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._records_filename = value
	
	# def records_filename ( self, value )

	# --------------------------------------------------

	@property
	def records_filepath ( self ):
		"""
		Path of the tfrecords file to create (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._directory + pytools.path.FilePath( self._records_filename )
	
	# def records_filepath ( self )

	# --------------------------------------------------

	@records_filepath.setter
	def records_filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.FilePath) )
		value = pytools.path.FilePath( value ) if isinstance( value, str ) else value
		assert value.extension() == 'tfrecords'

		self._directory = value.parent()
		self._records_filename = value.filename()
	
	# def records_filepath ( self, value )

	# --------------------------------------------------

	@property
	def blueprint_filename ( self ):
		"""
		Name of the blubrint json file to create (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._blueprint_filename
	
	# def blueprint_filename ( self )

	# --------------------------------------------------

	@blueprint_filename.setter
	def blueprint_filename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._blueprint_filename = value
	
	# def blueprint_filename ( self, value )

	# --------------------------------------------------

	@property
	def blueprint_filepath ( self ):
		"""
		Path of the blueprint json file to create (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._directory + pytools.path.FilePath( self._blueprint_filename )
	
	# def blueprint_filepath ( self )

	# --------------------------------------------------

	@blueprint_filepath.setter
	def blueprint_filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.FilePath) )
		value = pytools.path.FilePath( value ) if isinstance( value, str ) else value
		assert value.extension() == 'json'

		self._directory = value.parent()
		self._blueprint_filename = value.filename()
	
	# def blueprint_filepath ( self, value )

	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL pattern used to search for examples to export (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._url_search_pattern
	
	# def url_search_pattern ( self )

	# --------------------------------------------------

	@url_search_pattern.setter
	def url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._url_search_pattern = value
	
	# def url_search_pattern ( self, value )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features to include for each examples (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._features_names
	
	# def features_names ( self )

	# --------------------------------------------------

	@features_names.setter
	def features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._features_names = list( value )
	
	# def features_names ( self, value )
	
	# --------------------------------------------------

	@property
	def compression_type ( self ):
		"""
		TF records compression method (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._compression_type
	
	# def compression_type ( self )
	
	# --------------------------------------------------

	@compression_type.setter
	def compression_type ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ('GZIP', 'ZLIB', '') )

		self._compression_type = value
	
	# def compression_type ( self, value )

	# --------------------------------------------------

	@property
	def compression_level ( self ):
		"""
		Compression level [0, 9] (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._compression_level
	
	# def compression_level ( self )
	
	# --------------------------------------------------

	@compression_level.setter
	def compression_level ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(1, 10) )

		self._compression_level = value
	
	# def compression_level ( self, value )

	# --------------------------------------------------

	@property
	def shuffle ( self ):
		"""
		If `True` examples are shuffled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )

		return self._shuffle
	
	# def shuffle ( self )

	# --------------------------------------------------

	@shuffle.setter
	def shuffle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecordsExporterSettings )
		assert pytools.assertions.type_is( value, bool )

		self._shuffle = value
	
	# def shuffle ( self, value )

# class RecordsExporterSettings ( pytools.tasks.TaskSettings )