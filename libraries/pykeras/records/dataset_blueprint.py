# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .dataset_blueprint_data import DatasetBlueprintData
from .example_blueprint      import ExampleBlueprint

# ##################################################
# ###          CLASS DATASET-BLUEPRINT           ###
# ##################################################

class DatasetBlueprint:
	"""
	Dataset blueprints are used to export pydataset examples and features to tfrecords files
	and deserialize the records in a tfrecords file when using tfrecords as inputs.
	"""	

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, data=DatasetBlueprintData() ):
		"""
		Initializes a new instance of the exampl blueprint class.

		Arguments:
			self     (`pykeras.records.DatasetBlueprint`): Instance to initialize.
			data (`pykeras.records.DatasetBlueprintData`): Data describing the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )
		assert pytools.assertions.type_is_instance_of( data, DatasetBlueprintData )

		self._data              = copy.deepcopy( data )
		self._example_blueprint = ExampleBlueprint( self._data.example_blueprint_data )

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing the dataset blueprint (`pykeras.records.DatasetBlueprintData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )

		return self._data

	# def data ( self )

	# --------------------------------------------------

	@data.setter
	def data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )
		assert pytools.assertions.type_is_instance_of( value, DatasetBlueprintData )

		self._data              = copy.deepcopy( value )
		self._example_blueprint = ExampleBlueprint( self._data.example_blueprint_data )
	
	# def data ( self, value )

	# --------------------------------------------------

	@property
	def example_blueprint ( self ):
		"""
		Example blueprint of this dataset blueprint (`pykeras.records.ExampleBlueprint`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )

		return self._example_blueprint

	# def example_blueprint( self )
	
	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in the tf records dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )

		return self.data.number_of_examples
	
	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features of the example of this dataset (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )

		return self.data.features_names
	
	# def features_names ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Get the specifications of this dataset blueprint.

		Specification are data used by tensorflow to parse tf records examples.
		
		Arguments:
			self (`pykeras.records.DatasetBlueprint`): Dataset blueprint of which to get the specifications.

		Returns:
			`dict` of `str` to `tensorflow.io.FixedLenFeature` or `tensorflow.io.VarLenFeature`: The specification
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )
		
		return self.example_blueprint.get_specifications()

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_example ( self, example ):
		"""
		Converts a pydataset.dataset.Example to tensorflow data.
		
		Arguments:
			self (`pykeras.records.DatasetBlueprint`): Dataset blueprint used to convert the example.
			example     (`pydataset.dataset.Example`): Example to convert.

		Returns:
			`tensorflow.train.Example`: The tensorflow example.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		return self.example_blueprint.convert_example( example )

	# def convert_example ( self, example )

	# --------------------------------------------------

	def parse_example_record ( self, record ):
		"""
		Parse a single example record (binary tensor) using this dataset blueprint.
		
		Arguments:
			self (`pykeras.records.DatasetBlueprint`): Dataset blueprint used to parse the example.
			record                         (`Tensor`): A scalar string Tensor, a single serialized Example.

		Returns:
			A dict mapping feature keys to Tensor and SparseTensor values.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetBlueprint )
		
		return self.example_blueprint.parse_example_record( record )
		
	# def parse_example_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_example ( cls, example, features_names=[], number_of_examples=0, compression_type='GZIP', compression_level=9 ):
		"""
		Creates a new dataset blueprint based on an example and names of features to consider.

		Arguments:
			cls                          (`type`): Example blueprint class type.
			example (`pydataset.dataset.Example`): Example to convert into a blueprint.
			features_names      (`list` of `str`): Names of features to consider.
			number_of_examples            (`int`): Number of examples in the tf records dataset.
			compression_type              (`str`): TF records compression method `['GZIP', 'ZLIB', '']
			compression_level             (`int`): Compression level [0, 9]
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )
		assert pytools.assertions.type_is( number_of_examples, int )
		assert pytools.assertions.type_is( compression_type, str )
		assert pytools.assertions.value_in( compression_type, ('GZIP', 'ZLIB', '') )
		assert pytools.assertions.type_is( compression_level, int )
		assert pytools.assertions.value_in( compression_level, range(10) )

		# Create the example blueprint data
		example_blueprint = ExampleBlueprint.from_example( example, features_names )

		# Create the dataset blueprint data
		data = DatasetBlueprintData(
			example_blueprint_data = example_blueprint.data,
			    number_of_examples = number_of_examples,
			      compression_type = compression_type,
			     compression_level = compression_level
			)

		# Create the dataset blueprint
		return DatasetBlueprint( data )

	# def from_example ( cls, example, features_names, number_of_examples, compression_type, compression_level )

# class DatasetBlueprint
