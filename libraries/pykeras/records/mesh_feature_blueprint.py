# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .mesh_feature_blueprint_data          import MeshFeatureBlueprintData
from .ndarray_feature_blueprint            import NDArrayFeatureBlueprint
from .register_feature_blueprint_attribute import RegisterFeatureBlueprintAttribute

# ##################################################
# ###      CLASS NDARRAY-FEATURE-BLUEPRINT       ###
# ##################################################

@RegisterFeatureBlueprintAttribute( pydataset.dataset.SerializableFeatureData )
@RegisterFeatureBlueprintAttribute( MeshFeatureBlueprintData )
class MeshFeatureBlueprint ( NDArrayFeatureBlueprint ):
	"""
	Class used to serialize / deserialize a image feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=MeshFeatureBlueprintData() ):
		"""
		Initializes a new instance of the bytes list feature class.

		Arguments:
			self     (`pykeras.records.MeshFeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.MeshFeatureBlueprintData`): Data describing the image feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, MeshFeatureBlueprintData )

		super( MeshFeatureBlueprint, self ).__init__( data )

	# def __init__ ( self, data )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Returns the specifications of this mesh feature blueprint.

		This is used to create the blueprint specifiactions
		that is therms used to parse records.
		
		Arguments:
			self (`pykeras.records.MeshFeatureBlueprint`): NDArray feature blueprint of which to get the specifications.

		Returns:
			`tensorflow.io.VarLenFeature`: The specifications.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshFeatureBlueprint )

		return tensorflow.io.VarLenFeature( tensorflow.float32 )

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a tensorflow feature.

		Arguments:
			self (`pykeras.records.MeshFeatureBlueprint`): NDArray feature blueprint used to convert the feature.
			feature         (`pydataset.dataset.Feature`): Image Feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.SerializableFeatureData )
		assert pytools.assertions.type_is_instance_of( feature.value, pydataset.render.MeshData )	

		return tensorflow.train.Feature( float_list=tensorflow.train.FloatList( value=feature.value.points.ravel().tolist() ) )

	# def convert_feature ( self, feature )

	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this image feature blueprint.
		
		Arguments:
			self (`pykeras.records.MeshFeatureBlueprint`): Feature blueprint used to parse the image feature.
			record                  (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshFeatureBlueprint )

		record = tensorflow.sparse.to_dense( record, default_value=0. )
		return tensorflow.reshape( record, [-1, 3] )

	# def parse_feature_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): Image feature blueprint class type.
			feature (`pydataset.dataset.Feature`): Image feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the image feature blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.SerializableFeatureData )
		assert pytools.assertions.type_is_instance_of( feature.value, pydataset.render.MeshData )

		data = MeshFeatureBlueprintData()
		return MeshFeatureBlueprint( data )

	# def from_feature ( cls, feature )

# class MeshFeatureBlueprint ( NDArrayFeatureBlueprint )