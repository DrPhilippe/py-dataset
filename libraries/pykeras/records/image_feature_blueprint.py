# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .image_feature_blueprint_data         import ImageFeatureBlueprintData
from .ndarray_feature_blueprint            import NDArrayFeatureBlueprint
from .register_feature_blueprint_attribute import RegisterFeatureBlueprintAttribute

# ##################################################
# ###      CLASS NDARRAY-FEATURE-BLUEPRINT       ###
# ##################################################

@RegisterFeatureBlueprintAttribute( pydataset.dataset.ImageFeatureData )
@RegisterFeatureBlueprintAttribute( ImageFeatureBlueprintData )
class ImageFeatureBlueprint ( NDArrayFeatureBlueprint ):
	"""
	Class used to serialize / deserialize a image feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=ImageFeatureBlueprintData() ):
		"""
		Initializes a new instance of the bytes list feature class.

		Arguments:
			self     (`pykeras.records.ImageFeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.ImageFeatureBlueprintData`): Data describing the image feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, ImageFeatureBlueprintData )

		super( ImageFeatureBlueprint, self ).__init__( data )

	# def __init__ ( self, data )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a tensorflow feature.

		Arguments:
			self (`pykeras.records.ImageFeatureBlueprint`): NDArray feature blueprint used to convert the feature.
			feature          (`pydataset.dataset.Feature`): Image Feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.ImageFeatureData )

		# Serialize the bdarray
		value      = cv2.cvtColor( feature.value, cv2.COLOR_BGR2RGB )
		ret, value = cv2.imencode( self.data.extension, value, [self.data.param] )
		if not ret:
			raise RuntimeError( 'Failed to encode image feature {} ({}, {}) with extension={} and param={}'.format(
				feature.url,
				feature.value.shape,
				feature.value.dtype,
				self.data.extension,
				self.data.param
				))
		value = value.tostring()
		value = bytes( tensorflow.compat.as_bytes(value) )

		# Return it as text bytes
		return tensorflow.train.Feature( bytes_list=tensorflow.train.BytesList( value=[value] ) )

	# def convert_feature ( self, feature )
	
	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this image feature blueprint.
		
		Arguments:
			self (`pykeras.records.ImageFeatureBlueprint`): Feature blueprint used to parse the image feature.
			record                   (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureBlueprint )

		# Number of channels
		if self.data.shape.get_rank() == 3:
			channels = self.data.shape[2]
		else:
			channels = 1

		# Decode JPG
		if self.data.extension in ['.jpeg', '.jpg', '.jpe']:
			feature = tensorflow.io.decode_jpeg( record, channels, name='decode_image' )

		# Decode PNG
		elif self.data.extension in ['.png']:
			feature = tensorflow.io.decode_png( record, channels, name='decode_image' )

		# Decode unknown image
		else:
			feature = tensorflow.io.decode_image( record, channels, name='decode_image' )

		feature = tensorflow.reshape( feature, self.data.shape.dimensions, name='reshape_image' )
		return feature

	# def parse_feature_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): Image feature blueprint class type.
			feature (`pydataset.dataset.Feature`): Image feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the image feature blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.ImageFeatureData )

		data = ImageFeatureBlueprintData(
			        shape = feature.data.shape,
			        dtype = numpy.dtype( feature.data.dtype ),
			default_value = numpy.zeros( feature.data.shape.dimensions, feature.data.dtype ),
			    extension = feature.data.extension,
			        param = feature.data.param
			)

		return ImageFeatureBlueprint( data )

	# def from_feature ( cls, feature )

# class ImageFeatureBlueprint ( NDArrayFeatureBlueprint )