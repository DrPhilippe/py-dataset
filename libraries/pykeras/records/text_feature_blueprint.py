# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint                    import FeatureBlueprint
from .register_feature_blueprint_attribute import RegisterFeatureBlueprintAttribute
from .text_feature_blueprint_data          import TextFeatureBlueprintData

# ##################################################
# ###        CLASS TEXT-FEATURE-BLUEPRINT        ###
# ##################################################

@RegisterFeatureBlueprintAttribute( pydataset.dataset.TextFeatureData )
@RegisterFeatureBlueprintAttribute( TextFeatureBlueprintData )
class TextFeatureBlueprint ( FeatureBlueprint ):
	"""
	Class used to serialize / deserialize a int feature.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=TextFeatureBlueprintData() ):
		"""
		Initializes a new instance of the text feature bleuprint class.

		Arguments:
			self     (`pykeras.records.TextFeatureBlueprint`): Instance to initialize.
			data (`pykeras.records.TextFeatureBlueprintData`): Data describing the text feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( data, TextFeatureBlueprintData )

		super( TextFeatureBlueprint, self ).__init__( data )

	# def __init__ ( self, data )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Returns the specifications of this text feature blueprint.
		
		Arguments:
			self (`pykeras.records.TextFeatureBlueprint`): Text feature blueprint of which to get the specifications.

		Returns:
			`tensorflow.io.FixedLenFeature`: The specifications.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprint )

		# return tensorflow.io.FixedLenFeature( [], 'string', self.data.default_value )
		return tensorflow.io.FixedLenFeature( [], 'string', None )

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_feature ( self, feature ):
		"""
		Converts the given feature into a bytes list.

		Arguments:
			self (`pykeras.records.TextFeatureBlueprint`): Feature blueprint used to convert the feature.
			feature         (`pydataset.dataset.Feature`): Text feature to convert.
		
		Returns:
			`tensorflow.train.Feature`: Serialized feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprint )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.TextFeatureData )

		# Convert the str text to bytes
		text = feature.value.encode( 'utf-8' )

		# Return the feature as an byles list
		return tensorflow.train.Feature( bytes_list=tensorflow.train.BytesList( value=[text] ) )

	# def convert_feature ( self, feature )
	
	# --------------------------------------------------

	def parse_feature_record ( self, record ):
		"""
		Parse a single feature record (Tensor) using this int feature blueprint.
		
		Arguments:
			self (`pykeras.records.TextFeatureBlueprint`): Feature blueprint used to parse the int feature.
			record                  (`tensorflow.Tensor`): A scalar string Tensor, a single serialized feature.

		Returns:
			`tensorflow.Tensor`: Parsed tensor
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprint )

		return tensorflow.strings.unicode_decode( record, 'utf-8', 'strict', name='decode_string' )

	# def parse_feature_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_feature ( cls, feature ):
		"""
		Creates a new instance of the feature blueprint class cls using the given feature.

		Arguments:
			cls                          (`type`): Text feature blueprint class type.
			feature (`pydataset.dataset.Feature`): Text feature from which to create a blueprint.

		Returns:
			`cls`: A new instance of the feature blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( feature, pydataset.dataset.Feature )
		assert pytools.assertions.type_is_instance_of( feature.data, pydataset.dataset.TextFeatureData )


		data = TextFeatureBlueprintData(
			default_value = None
			)

		return TextFeatureBlueprint( data )

	# def from_feature ( cls, feature )

# class TextFeatureBlueprint ( FeatureBlueprint )