# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###     CLASS FEATURES-BLUEPRINTS-FACTORY      ###
# ##################################################

class FeaturesBlueprintsFactory ( pytools.factory.ClassFactory ):
	"""
	Extra layer over the class factory to regroup features blueprints by feature typenames.

	Allows to register and instantiate `pykeras.records.Feature`
	based on the associated `pykeras.records.FeatureData` type.
	"""
	
	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	group_name = 'bleuprints'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_feature_blueprint_for ( cls, feature_data_type ):
		"""
		Attempts to get the blueprint associated to the given feature data type.
		
		Arguments:
			cls               (`type`): Factory type.
			feature_data_type (`type`): Type of the feature.

		Returns:
			`type`/`None`: The type of feature blueprint or None.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is( feature_data_type, type )

		# Get the name of the type
		typename = feature_data_type.__name__

		# Get the names of types for which a widget exists.
		registered_typenames = cls.get_typenames_of_group( cls.group_name )

		# If the type hase a dedicated widget, use it
		if typename in registered_typenames:
			return cls.get_type( cls.group_name, typename )

		# Otherwise search in its base classes
		bases_types = list(feature_data_type.__bases__)

		while bases_types:
			up = []

			for base_type in bases_types:
				
				if base_type.__name__ in registered_typenames:
					return cls.get_type( cls.group_name, base_type.__name__ )

				else:
					for base in base_type.__bases__:
						up.append( base )

			bases_types = up

		return None

	# def get_feature_blueprint_for ( cls, feature_data_type )
	
# class FeaturesFactory ( pytools.factory.ClassFactory )