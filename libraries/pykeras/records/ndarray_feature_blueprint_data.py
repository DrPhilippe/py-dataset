# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint_data import FeatureBlueprintData

# ##################################################
# ###    CLASS NDARRAY-FEATURE-BLUEPRINT-DATA    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'NDArrayFeatureBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = [
		'shape',
		'dtype',
		'default_value'
		]
	)
class NDArrayFeatureBlueprintData ( FeatureBlueprintData ):
	"""
	Data describing a ndarray feature blueprint.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, shape=pydataset.data.ShapeData(), dtype=numpy.dtype('float32'), default_value=None ):
		"""
		Initializes a new instance of the feature data class.
		
		Default value must be serializable and compatible with `tf.io.FixedLenFeature` expectations.

		Arguments:
			self (`pykeras.records.NDArrayFeatureBlueprintData`): Instance to initialize.
			shape                   (`pydataset.data.ShapeData`): Shape of the feature.
			dtype                                (`numpy.dtype`): Data type of the ndarray.
			default_value               (`None`/`numpu.ndarray`): Default value, if missing from one record.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( shape, (pydataset.data.ShapeData, list) )
		assert pytools.assertions.type_is_instance_of( dtype, numpy.dtype )
		assert pytools.assertions.type_is_instance_of( default_value, (numpy.ndarray, type(None)) )

		super( NDArrayFeatureBlueprintData, self ).__init__()

		self._shape = copy.deepcopy( shape )
		self._dtype = dtype
		self._default_value = copy.deepcopy( default_value )

	# def __init__ ( self, shape, dtype, default_value )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def shape ( self ):
		"""
		Shape of the ndarray feature (`pydataset.dataset.ShapeData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )

		return self._shape
	
	# def shape ( self )

	# --------------------------------------------------

	@shape.setter
	def shape ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ShapeData )

		self._shape = copy.deepcopy( value )
	
	# def shape ( self, value )

	# --------------------------------------------------

	@property
	def dtype ( self ):
		"""
		Data type of the feature (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )

		return self._dtype
	
	# def dtype ( self )

	# --------------------------------------------------

	@dtype.setter
	def dtype ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, numpy.dtype )

		self._dtype = value
	
	# def dtype ( self, value )

	# --------------------------------------------------

	@property
	def default_value ( self ):
		"""
		Default value of the feature (`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )

		return self._default_value
	
	# def default_value ( self )

	# --------------------------------------------------

	@default_value.setter
	def default_value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, (numpy.ndarray, type(None)) )

		self._default_value = copy.deepcopy( value )
	
	# def default_value ( self, value )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this ndarray feature blueprint data to a python `dict`.

		Arguments:
			self (`pykeras.records.NDArrayFeatureBlueprintData`): NDArray feature to serialize.
			ignores                            (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		
		# Run pydataset.data.ItemData serialize method, but skip default_value and dtype
		if ( 'default_value' not in ignores ):
			ignores.append( 'default_value' )
		if 'dtype' not in ignores:
			ignores.append( 'dtype' )
		
		data = super( NDArrayFeatureBlueprintData, self ).serialize( ignores )

		# serialize the dtype
		data[ 'dtype' ] = str( self._dtype )

		# serialize the default value
		default_value, shape, dtype = pytools.serialization.serialize_ndarray( self.default_value )
		data[ 'default_value' ] = default_value

		# Return the data
		return data

	# def serialize ( self )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this ndarray feature blueprint from a python `dict`.

		Arguments:
			self (`pydataset.dataset.NDArrayFeatureBlueprintData`): NDArray feature blueprint to deserialize.
			data                                          (`dict`): The serialized data.
			ignores                              (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureBlueprintData )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
	
		# Run default deserialize method, but skip default_value and dtype
		if 'default_value' not in ignores:
			ignores.append( 'default_value' )
		if 'dtype' not in ignores:
			ignores.append( 'dtype' )
		
		super( NDArrayFeatureBlueprintData, self ).deserialize( data, ignores )

		# Deserialzize default dtype
		dtype = data[ 'dtype' ]
		self._dtype = numpy.dtype( dtype )

		# Deserialzize default value
		default_value = data[ 'default_value' ]
		default_value = pytools.serialization.deserialize_ndarray( default_value, self.shape.dimensions, data[ 'dtype' ] )
		self._default_value = default_value

	# def __deserialize__ ( self, data )

# class NDArrayFeatureBlueprintData ( FeatureBlueprintData )
