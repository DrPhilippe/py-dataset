# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_blueprint_data import FeatureBlueprintData

# ##################################################
# ###     CLASS TEXT-FEATURE-BLUEPRINT-DATA      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TextFeatureBlueprintData',
	   namespace = 'pykeras.records',
	fields_names = [
		'default_value'
		]
	)
class TextFeatureBlueprintData ( FeatureBlueprintData ):
	"""
	Data describing a ndarray feature blueprint.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, default_value=None ):
		"""
		Initializes a new instance of the int feature data class.
		
		Arguments:
			self (`pykeras.records.TextFeatureBlueprintData`): Instance to initialize.
			default_value                      (`None`/`str`): Default value, if missing from one record.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( default_value, (str, type(None)) )

		super( TextFeatureBlueprintData, self ).__init__()

		self._default_value = default_value

	# def __init__ ( self, default_value )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def default_value ( self ):
		"""
		Default value of the feature (`str`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprintData )

		return self._default_value
	
	# def default_value ( self )

	# --------------------------------------------------

	@default_value.setter
	def default_value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TextFeatureBlueprintData )
		assert pytools.assertions.type_is_instance_of( value, (str, type(None)) )

		self._default_value = value
	
	# def default_value ( self, value )

# class TextFeatureBlueprintData ( FeatureBlueprintData )