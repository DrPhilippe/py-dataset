# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import tensorflow

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.serialization

# LOCALS
from .example_blueprint_data      import ExampleBlueprintData
from .feature_blueprint           import FeatureBlueprint
from .features_blueprints_factory import FeaturesBlueprintsFactory

# ##################################################
# ###          CLASS EXAMPLE-BLUEPRINT           ###
# ##################################################

class ExampleBlueprint:
	"""
	Example blueprints are used to export pydataset examples and features to tfrecords files
	and deserialize the records in a tfrecords file when using tfrecords as inputs.
	"""	

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, data=ExampleBlueprintData() ):
		"""
		Initializes a new instance of the exampl blueprint class.

		Arguments:
			self     (`pykeras.records.ExampleBlueprint`): Instance to initialize.
			data (`pykeras.records.ExampleBlueprintData`): Data describing the example blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		assert pytools.assertions.type_is_instance_of( data, ExampleBlueprintData )

		self._data = copy.deepcopy( data )
		self._features_blueprints = {}

		self.load_features_blueprints()

	# def __init__ ( self, data )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing the example blueprint (`pykeras.records.ExampleBlueprintData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )

		return self._data

	# def data ( self )

	# --------------------------------------------------

	@data.setter
	def data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		assert pytools.assertions.type_is_instance_of( value, ExampleBlueprintData )

		self._data = value
		self._features_blueprints = {}

		self.load_features_blueprints()
	
	# def data ( self, value )

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features of this example (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )

		return self.data.features_names
	
	# def features_names ( self )
	
	# --------------------------------------------------

	@property
	def features_blueprints ( self ):
		"""
		Featyure blueprints of this example blueprint (`dict` of `str` to `pykeras.records.FeatureBlueprint`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )

		return self._features_blueprints

	# def features_blueprints( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def load_features_blueprints ( self ):
		"""
		Loads the features blueprints based on their data found in this example blueprint data.

		Arguments:
			self (`pykeras.records.ExampleBlueprint`): Example blueprint.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )

		# Load the blueprint of each feature
		for feature_name in self.data.features_names:

			# Get the feature blueprint data
			feature_blueprint_data      = self.data.features_blueprints_data[ feature_name ]
			feature_blueprint_data_type = type( feature_blueprint_data )

			# Instantiate the feature blueprint
			feature_blueprint_type = FeaturesBlueprintsFactory.get_feature_blueprint_for( feature_blueprint_data_type )

			# Check
			if feature_blueprint_type is None:
				raise RuntimeError( 'Failed to load feature blueprint for feature {}, data type is {}'.format(
					feature_name,
					feature_blueprint_data_type.__name__
					))

			# Instantiate
			self._features_blueprints[ feature_name ] = feature_blueprint_type( feature_blueprint_data )

		# for feature_name in self.data.features_names

	# def load_features_blueprints ( self )

	# --------------------------------------------------

	def add_feature_blueprint ( self, feature_name, feature_blueprint ):
		"""
		Add a feature blueprint to this example blueprint.

		Arguments:
			self              (`pykeras.records.ExampleBlueprint`): Example blueprint.
			feature_name                                   (`str`): Name of the feature.
			feature_blueprint (`pykeras.records.FeatureBlueprint`): Name of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		assert pytools.assertions.type_is( feature_name, str )
		assert pytools.assertions.type_is_instance_of( feature_blueprint, FeatureBlueprint )

		# Add it to the features blueprint dict
		self._features_blueprints[ feature_name ] = feature_blueprint

		# Add it to the data
		self.data.features_blueprints_data[ feature_name ] = feature_blueprint.data

	# def add_feature_blueprint ( self, feature_name, feature_blueprint )
	
	# --------------------------------------------------

	def remove_feature_blueprint ( self, feature_name ):
		"""
		Removes a feature blueprint from this example blueprint.

		Arguments:
			self (`pykeras.records.ExampleBlueprint`): Example blueprint.
			feature_name                      (`str`): Name of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		assert pytools.assertions.type_is( feature_name, str )
		assert pytools.assertions.type_is_instance_of( feature_blueprint, FeatureBlueprint )

		# Add it to the features blueprint dict
		del self._features_blueprints[ feature_name ]

		# Add it to the data
		del self.data.features_blueprints_data[ feature_name ]

	# def remove_feature_blueprint ( self, feature_name, feature_blueprint )

	# --------------------------------------------------

	def get_specifications ( self ):
		"""
		Get the specifications of this example blueprint.

		Specification are data used by tensorflow to parse tf records examples.
		
		Arguments:
			self (`pykeras.records.ExampleBlueprint`): Example blueprint of which to get the specifications.

		Returns:
			`dict` of `str` to `tensorflow.io.FixedLenFeature` or `tensorflow.io.VarLenFeature`: The specification
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		
		specifications = {}
		
		for feature_name in self.data.features_names:
			
			# Get the feature blueprint
			feature_blueprint = self.features_blueprints[ feature_name ]

			# Get its specifications
			specifications[ feature_name ] = feature_blueprint.get_specifications()

		# for feature_name in self.data.features_names

		return specifications

	# def get_specifications ( self )

	# --------------------------------------------------

	def convert_example ( self, example ):
		"""
		Converts a pydataset.dataset.Example to tensorflow data.
		
		Arguments:
			self (`pykeras.records.ExampleBlueprint`): Example blueprint used to convert the example.
			example     (`pydataset.dataset.Example`): Example to convert.

		Returns:
			`tensorflow.train.Example`: The tensorflow example.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		features_names = self.data.features_names
		features       = example.get_features( features_names, True )
		tf_features    = {}

		# Convert each feature in the example
		for feature_name in features_names:
			
			# Get the feature blueprint
			feature_blueprint = self.features_blueprints[ feature_name ]

			# Convert the feature
			tf_features[ feature_name ] = feature_blueprint.convert_feature( features[ feature_name ] )

		# for feature_name in self.data.features.keys()

		return tensorflow.train.Example(
			features = tensorflow.train.Features( feature=tf_features )
			)

	# def convert_example ( self, example )

	# --------------------------------------------------

	def parse_example_record ( self, record ):
		"""
		Parse a single example record (binary tensor) using this example blueprint.
		
		Arguments:
			self (`pykeras.records.ExampleBlueprint`): Example blueprint used to parse the example.
			record                         (`Tensor`): A scalar string Tensor, a single serialized Example.

		Returns:
			A dict mapping feature keys to Tensor and SparseTensor values.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleBlueprint )

		# Parse the example
		features = tensorflow.io.parse_single_example( record, self.get_specifications() )

		# Parse each features using their blueprint
		for feature_name in self.data.features_names:

			# Get the raw feature value and the associated blueprint
			feature_tensor    = features[ feature_name ]
			feature_blueprint = self.features_blueprints[ feature_name ]

			# Parse the feature using the blueprint
			feature_tensor = feature_blueprint.parse_feature_record( feature_tensor )

			# Save the result
			features[ feature_name ] = feature_tensor
			
		# for feature_name in self.data.features_names

		return features

	# def parse_example_record ( self, record )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_example ( cls, example, features_names=[] ):
		"""
		Creates a new example blueprint based on an example and names of features to consider.

		Arguments:
			cls                          (`type`): Example blueprint class type.
			example (`pydataset.dataset.Example`): Example to convert into a blueprint.
			features_names      (`list` of `str`): Names of features to consider.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		# Get the features from the example
		features = example.get_features( features_names, True )

		# Get the actual features name
		features_names = list( features.keys() )

		# Init the example blueprint
		example_blueprint = cls()

		# Go through each features
		for feature_name in features_names:

			# Get the feature and its type
			feature           = features[ feature_name ]
			feature_data_type = type( feature.data )

			# Attempt to get a blueprint type for the given feature
			feature_blueprint_type = FeaturesBlueprintsFactory.get_feature_blueprint_for( feature_data_type )

			# Check it
			if feature_blueprint_type is None:
				raise RuntimeError( 'Could not find a feature blueprint for the feature name={}, data type={}'.format(feature_name, feature_data_type.__name__) )

			# Create it
			feature_blueprint = feature_blueprint_type.from_feature( feature )

			# Keep it
			example_blueprint.add_feature_blueprint( feature_name, feature_blueprint )

		# for feature_name in features_names

		return example_blueprint

	# def from_example ( cls, example )

# class ExampleBlueprint
