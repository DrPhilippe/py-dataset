# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pytools.assertions

# LOCALS

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# -------------------------------------------------

def intersection_over_union ( lhs, rhs, X1=0, Y1=1, X2=2, Y2=3 ):
	"""
	Computes the intersection over union of rectangles.

	Arguments:
		lhs (`tensorflow.Tensor`): Rectangles with shape [BATCH, 4+]
		rhs (`tensorflow.Tensor`): Rectangles with shape [BATCH, 4+]


	Returns:
		`tensorflow.Tensor`: Intersections over unions with shape [BATCH,]
	"""
	lhs_depth = tensorflow.shape( lhs )[ -1 ]
	rhs_depth = tensorflow.shape( rhs )[ -1 ]

	# Reshape rectangles to have them all on axis 1
	lhs = tensorflow.reshape( lhs, [-1, lhs_depth] )
	rhs = tensorflow.reshape( rhs, [-1, rhs_depth] )
	
	# Unstack their coordinates
	lhs = tensorflow.unstack( lhs, axis=-1 )
	rhs = tensorflow.unstack( rhs, axis=-1 )

	# compute intersection area
	i_x1     = tensorflow.math.maximum( lhs[ X1 ], rhs[ X1 ] )
	i_y1     = tensorflow.math.maximum( lhs[ Y1 ], rhs[ Y1 ] )
	i_x2     = tensorflow.math.minimum( lhs[ X2 ], rhs[ X2 ] )
	i_y2     = tensorflow.math.minimum( lhs[ Y2 ], rhs[ Y2 ] )
	i_width  = tensorflow.math.subtract( i_x2, i_x1 )
	i_height = tensorflow.math.subtract( i_y2, i_y1 )
	i_width  = tensorflow.where( i_width < 0, tensorflow.zeros_like(i_width), i_width ) # Clamp min width to 0
	i_width  = tensorflow.where( i_height < 0, tensorflow.zeros_like(i_height), i_height ) # Clamp min height to 0
	i_area   = tensorflow.math.multiply( i_width, i_height )
	i_area   = tensorflow.math.abs( i_area )

	# LHS rect area
	lhs_width  = tensorflow.math.subtract( lhs[ X2 ], lhs[ X1 ] )
	lhs_height = tensorflow.math.subtract( lhs[ Y2 ], lhs[ Y1 ] )
	lhs_width  = tensorflow.where( lhs_width < 0, tensorflow.zeros_like(lhs_width), lhs_width ) # Clamp min width to 0
	lhs_height = tensorflow.where( lhs_height < 0, tensorflow.zeros_like(lhs_height), lhs_height ) # Clamp min height to 0
	lhs_area   = tensorflow.math.multiply( lhs_width, lhs_height )
	lhs_area   = tensorflow.math.abs( lhs_area )

	# RHS rect area
	rhs_width  = tensorflow.math.subtract( rhs[ X2 ], rhs[ X1 ] )
	rhs_height = tensorflow.math.subtract( rhs[ Y2 ], rhs[ Y1 ] )
	rhs_width  = tensorflow.where( rhs_width < 0, tensorflow.zeros_like(rhs_width), rhs_width ) # Clamp min width to 0
	rhs_height = tensorflow.where( rhs_height < 0, tensorflow.zeros_like(rhs_height), rhs_height ) # Clamp min height to 0
	rhs_area   = tensorflow.math.multiply( rhs_width, rhs_height )
	rhs_area   = tensorflow.math.abs( rhs_area )

	# Union area
	u_area = tensorflow.math.add( lhs_area, rhs_area )
	u_area = tensorflow.math.subtract( u_area, i_area )
	u_area = tensorflow.math.add( u_area, tensorflow.keras.backend.epsilon() )

	# Compute IoU
	return tensorflow.divide( i_area, u_area )

# def intersection_over_union ( lhs, rhs )