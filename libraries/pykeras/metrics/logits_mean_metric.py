# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pytools.assertions

# LOCALS
from .mean_metric import MeanMetric

# ##################################################
# ###          CLASS LOGITS-MEAN-METRIC          ###
# ##################################################

class LogitsMeanMetric ( MeanMetric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.LogitsMeanMetric` class.

		Arguments:
			self (`pykeras.metrics.LogitsMeanMetric`): Instance to initialize.
			activation      (`str`/`None`/`function`): Activation function used on the logits or its name.
			threshold                       (`float`): Threshold used to separate positive and negative logits.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsMeanMetric )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )

		super( LogitsMeanMetric, self ).__init__( **kwargs )

		self._activation = keras.activations.deserialize( activation ) if isinstance( activation, str ) else activation
		self._threshold  = threshold
	
	# def __init__( self, activation, threshold, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def activation ( self ):
		"""
		Activation function used by this metrics on the logits (`function`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsMeanMetric )
		
		return self._activation
	
	# def activation ( self )

	# --------------------------------------------------

	@property
	def threshold ( self ):
		"""
		Threshold used to separate positive and negative logits (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsMeanMetric )

		return self._threshold
	
	# def threshold ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_config ( self ):
		"""
		Returns the config of this logits mean metric.

		Arguments:
			self (`pykeras.metrics.LogitsMeanMetric`): Instance of which to get the config.

		Returns:
			`dict`: configuration data.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsMeanMetric )
		
		config = super( LogitsMeanMetric, self ).get_config()
		config.update({
			'activation': keras.activations.serialize( self._activation ),
			'threshold': self._threshold
		})
		return config
	
	# def get_config( self )

	# --------------------------------------------------

	def apply_activation_and_threshold ( self, y_true, y_pred ):
		"""
		Apply the actiation function to `y_pred` and compares both `y_pred` and `y_true` to the threshold.
	
		Preedictions and labels tensor must be of data type float32 and their
		shape must be equal to the batch size times the number of classes.

		They are return as indexes tensor of type bool.

		Arguments:
			self (`pykeras.metrics.LogitsMeanMetric`): Loits metric to apply.
			y_true              (`tensorflow.Tensor`): Tensor of label logits.
			y_pred              (`tensorflow.Tensor`): Tensor of predicted logits.

		Returns:
			y_pred (`tensorflow.Tensor`): Tensor of positively predicted logits.
			y_true (`tensorflow.Tensor`): Tensor of positive label logits.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsMeanMetric )

		# Cast parameters to floats
		y_true = tensorflow.cast( y_true, 'float32' )
		y_pred = tensorflow.cast( y_pred, 'float32' )

		# Apply activation function to predictions
		if self.activation:
			y_pred = self.activation( y_pred )

		# Compare prediction and labels to the threshold
		y_true = tensorflow.math.greater_equal( y_true, self.threshold )
		y_pred = tensorflow.math.greater_equal( y_pred, self.threshold )

		return y_true, y_pred

	# def apply_activation_and_threshold ( self, y_true, y_pred )

# class LogitsMeanMetric ( MeanMetric )