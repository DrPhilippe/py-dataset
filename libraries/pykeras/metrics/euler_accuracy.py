# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.geometry
import pytools.assertions

# LOCALS
from .mean_distance_accuracy import MeanDistanceAccuracy

# ##################################################
# ###            CLASS EULER-ACCURACY            ###
# ##################################################

class EulerAccuracy ( MeanDistanceAccuracy ):
	"""
	Metric measuring the average number of correctly predicted euler angles.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=pykeras.geometry.degrees_to_radians(5.), norm_order='euclidean', axes='xyz', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.EulerAccuracy` class.
		
		If the distance between predicted and ground is greather than
		or equal to the given threshold, it is considered false (0. accuracy),
		otherwise, the closer to 0. it is, the closer to 1. the accuracy will be.
		
		See `tensorflow.norm` for accepted `norm_order` values.
		Axes can be one of 'x', 'y', 'z' or any combinaison.

		Arguments:
			threshold              (`float`): Distance threshold.
			norm_order (`str`/`float`/`int`): Order of the distance norm.
			axes                     (`str`): Axes on which to compute the metric.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, EulerAccuracy )
		
		# Initialize the Metric parent class
		super( EulerAccuracy, self ).__init__( threshold, norm_order, axes, **kwargs )

	# def __init__( self, threshold, **kwargs )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this accuracy metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.EulerAccuracy`): Instance to update.
			y_true                (`keras.Tensor`): Batch of labels.
			y_pred                (`keras.Tensor`): Batch of predictions.
			sample_weight         (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, EulerAccuracy )
		
		y_true = tensorflow.reshape( y_true, [-1, 3] )
		y_pred = tensorflow.reshape( y_pred, [-1, 3] )

		# Update the count variable with the batch size
		super( MeanDistanceAccuracy, self ).update_state( y_true, y_pred, sample_weight )

		# Select axes on which to compute the distances
		if self.axes != 'xyz':
			y_true = pykeras.geometry.select_vector_axes( y_true, self.axes )
			y_pred = pykeras.geometry.select_vector_axes( y_pred, self.axes )
		
		# Compute distances from predicted translation to ground truth translation
		distances = pykeras.geometry.euler_distances( y_true, y_pred, norm_order=self.norm_order, axis=-1, euler_units=pykeras.geometry.RANDIANS )

		# Count the number of correct distances in the batch
		batch_total = pykeras.geometry.count_correct_distances( distances, self.threshold, axis=-1, dtype='float32' )
		self.total.assign_add( batch_total )
				
	# def update_state ( self, y_true, y_pred, sample_weight )

# class EulerAccuracy ( MeanDistanceAccuracy )