# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.geometry
import pytools.assertions

# LOCALS
from .mean_metric import MeanMetric

# ##################################################
# ###        CLASS MEAN-DISTANCE-ACCURACY        ###
# ##################################################

class MeanDistanceAccuracy ( MeanMetric ):
	"""
	Metric measuring the average distance accuracy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=0.05, norm_order='euclidean', axes='xyz', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.MeanDistanceAccuracy` class.
		
		If the distance between predicted and ground is greather than
		or equal to the given threshold, it is considered false (0. accuracy),
		otherwise, the closer to 0. it is, the closer to 1. the accuracy will be.
		
		See `tensorflow.norm` for accepted `norm_order` values.
		Axes can be one of 'x', 'y', 'z' or any combinaison.

		Arguments:
			threshold              (`float`): Distance threshold.
			norm_order (`str`/`float`/`int`): Order of the distance norm.
			axes                     (`str`): Axes on which to compute the metric.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanDistanceAccuracy )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.type_in( norm_order, (float, int, str) )
		if isinstance( norm_order, str ):
			assert pytools.assertions.value_in( norm_order, ('euclidean', 'fro') )
		assert pytools.assertions.type_is( axes, str )
		for axis in axes:
			assert pytools.assertions.value_in( axis, 'xyzw' )

		# Initialize the Metric parent class
		super( MeanDistanceAccuracy, self ).__init__( **kwargs )

		# Intialize properties
		self._threshold  = threshold
		self._norm_order = norm_order
		self._axes       = axes

	# def __init__( self, threshold, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def threshold ( self ):
		"""
		Distance threshold (`float`).
		"""
		return self._threshold
	
	# def threshold ( self )

	# --------------------------------------------------

	@property
	def norm_order ( self ):
		"""
		Order of the distance norm (`str`/`float`/`int`).

		See `tensorflow.norm` for accepted values.
		"""
		return self._norm_order
	
	# def norm_order ( self )

	# --------------------------------------------------

	@property
	def axes ( self ):
		"""
		Axes on which to compute the metric (`str`).

		Can be one of 'x', 'y', 'z' or any combinaison.
		"""
		return self._axes
	
	# def axes ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this accuracy metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.MeanDistanceAccuracy`): Instance to update.
			y_true                       (`keras.Tensor`): Batch of labels.
			y_pred                       (`keras.Tensor`): Batch of predictions.
			sample_weight                (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanDistanceAccuracy )
		
		# Update the count variable with the batch size
		super( MeanDistanceAccuracy, self ).update_state( y_true, y_pred, sample_weight )
		
		# Select axes on which to compute the distances
		if self.axes != 'xyz':
			y_true = pykeras.geometry.select_vector_axes( y_true, self.axes )
			y_pred = pykeras.geometry.select_vector_axes( y_pred, self.axes )
		
		# Compute distances from predicted translation to ground truth translation
		distances = pykeras.geometry.distances( y_true, y_pred, norm_order=self.norm_order, axis=-1 )
		
		# Count the number of correct distances in the batch
		batch_count = pykeras.geometry.count_correct_distances( distances, self.threshold, axis=-1, dtype='float32' )
		self.count.assign_add( batch_count )
		
	# def update_state ( self, y_true, y_pred, sample_weight )

# class MeanDistanceAccuracy ( MeanMetric )