# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
import tensorflow

# INTERNALS
import pykeras
import pytools

# LOCALS
from .mean_distance_accuracy import MeanDistanceAccuracy

# ##################################################
# ###      CLASS CONTROL-POINTS-ACCURACY-V1      ###
# ##################################################

class ControlPointsAccuracyV1 ( MeanDistanceAccuracy ):
	"""
	Metric measuring the average number of correctly predicted control points.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=5.0, norm_order='euclidean', axes='xy', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.ControlPointsAccuracyV1` class.
		
		If the distance between predicted and ground is greather than or equal to
		the given threshold, it is considered false (0. accuracy), otherwise, it is considered to be true.

		See `tensorflow.norm` for accepted `norm_order` values.
		Axes can be one of 'x', 'y', 'z' or any combinaison.

		Arguments:
			threshold              (`float`): Distance threshold (in pixel or relative image size).
			norm_order (`str`/`float`/`int`): Order of the distance norm.
			axes                     (`str`): Axes on which to compute the metric.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsAccuracyV1 )
		
		# Initialize the Metric parent class
		super( ControlPointsAccuracyV1, self ).__init__( threshold, norm_order, axes, **kwargs )

	# def __init__( self, threshold, **kwargs )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this accuracy metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.ControlPointsAccuracyV1`): Instance to update.
			y_true                          (`keras.Tensor`): Batch of labels.
			y_pred                          (`keras.Tensor`): Batch of predictions.
			sample_weight                   (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsAccuracyV1 )
		
		# Mask predictions and label [batch_size, C, 8, 2]
		y_nan_coordinate = tensorflow.math.is_nan( y_true )
		y_true           = tensorflow.where( y_nan_coordinate, tensorflow.zeros_like( y_true ), y_true )
		y_pred           = tensorflow.where( y_nan_coordinate, tensorflow.zeros_like( y_pred ), y_pred )

		# Count the number of non-NaN label control points
		y_nan_points     = tensorflow.math.reduce_any( y_nan_coordinate, axis=-1 ) # Any 2D point with (at least) one NaN coordiante [batch_size, C, 8]
		y_nan_contol     = tensorflow.math.reduce_any( y_nan_points, axis=-1 ) # Any control points set with (at least) one NaN coordiante  [batch_size, C]
		y_non_nan_contol = tensorflow.math.logical_not( y_nan_contol ) # Non-NaN control poitns sets [batch_size, C]
		batch_target      = pykeras.logits.count_positives( y_non_nan_contol, axis=None, dtype='float32' ) # Number of non-NaN control points set [1]
		self.target.assign_add( batch_target )
		
		# Compute distances from predicted points to ground truth points [batch_size, C, 8]
		distances = pykeras.geometry.distances( y_true, y_pred, norm_order=self.norm_order, axis=-1 )
		# Compare point-point distances to threshold [batch_size, C, 8]
		correct = pykeras.geometry.compare_distances( distances, self.threshold )
		# For 1 object, i.e. a set of 8 points, a control point set is correct if the 8 points are all correct [batch_size, C]
		correct = tensorflow.math.reduce_all( correct, axis=-1 )
		# Only consider control points that do not have nan coordinates [batch_size, C]
		correct = tensorflow.math.logical_and( correct, y_non_nan_contol )
		
		# Count the number of correct control point set, for each of the C objects, and over the batch
		batch_count = pykeras.logits.count_positives( correct, axis=None, dtype='float32' )
		self.count.assign_add( batch_count )
				
	# def update_state ( self, y_true, y_pred, sample_weight )

# class ControlPointsAccuracyV1 ( MeanDistanceAccuracy )