# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from .mean_metric import MeanMetric

# ##################################################
# ###           CLASS LOGITS-ACCURACY            ###
# ##################################################

class PixelWiseIOU ( MeanMetric ):
	"""
	Pixel-wise Intersection Over Union.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=0.5, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.PixelWiseIOU` class.

		Arguments:
			self (`pykeras.metrics.PixelWiseIOU`): Instance to initialize.
			threshold                   (`float`): Threshold used to separate correct and incorrect results.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, PixelWiseIOU )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )

		# Initialize the Metric parent class
		super( LogitsAccuracy, self ).__init__( threshold, **kwargs )

		# Initialize properties
		self._threshold = threshold

	# def __init__( self, threshold, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def threshold ( self ):
		"""
		Threshold used to separate correct and incorrect results (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PixelWiseIOU )

		return self._threshold
	
	# def threshold ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.PixelWiseIOU`): Instance to update.
			y_true          (`tensorflow.Tensor`): Batch of labels.
			y_pred          (`tensorflow.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, PixelWiseIOU )
		
		
		batch_total = tensorflow.shape( y_true )[ 0 ]
		self.total.assign_add( batch_total )
		
		# Number of logits in the batch (area of the tensor)		
		batch_count = tensorflow.shape( y_true )[ 0 ]
		self.count.assign_add( batch_count )

		# Compare predictions to labels to identify correctly predicted logits
		# and count them
		
	# def update_state ( self, y_true, y_pred, sample_weight )

# class PixelWiseIOU ( ManMetric )