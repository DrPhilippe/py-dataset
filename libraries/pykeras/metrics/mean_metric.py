# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError
import numpy

# INTERNALS
import pytools.assertions

# LOCALS

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

EPSILON = numpy.finfo( numpy.float32 ).eps 

# ##################################################
# ###         CLASS TRANSLATION-ACCURACY         ###
# ##################################################

class MeanMetric ( keras.metrics.Metric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.MeanMetric` class.

		This creates two variables:
			- total: used to accumulate the sum of the metric values for each sample.
			- count: used to accumulate the number of sample considered by this metric.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanMetric )

		# Initialize the Metric parent class
		super( MeanMetric, self ).__init__( **kwargs )

		# Initialize variables
		self.count = self.add_weight(
			       name = 'count',
			initializer = keras.initializers.Zeros,
			      dtype = 'float32'
			)
		self.target = self.add_weight(
			       name = 'target',
			initializer = keras.initializers.Zeros,
			      dtype = 'float32'
			)

	# def __init__( self, threshold, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def result ( self ):
		"""
		Computes and returns the result of this mean metric: `total` / `count`.
	
		Arguments:
			self (`pykeras.metrics.MeanMetric`): Mean metric of which to get the result.

		Returns:
			`keras.Tensor`: Single value tensor corresponding to `total` / `count`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanMetric )
		
		return tensorflow.math.divide(
			self.count,
			tensorflow.math.add( self.target, numpy.finfo( numpy.float32 ).eps )
			)

	# def result ( self )

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this mean metric given a batch of labels and associated predictions.

		By default this method adds `batch_size` to the `target` variable.
		
		Arguments:
			self (`pykeras.metrics.MeanMetric`): Mean metric instance to update.
			y_true             (`keras.Tensor`): Batch of labels.
			y_pred             (`keras.Tensor`): Batch of predictions.
			sample_weight      (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanMetric )
				
		# Get the batch size
		batch_target = tensorflow.shape( y_true )[ 0 ]
		batch_target = tensorflow.cast( batch_target, 'float32' )
		self.target.assign_add( batch_target )
		
	# def update_state ( self, y_true, y_pred, sample_weight )

# class MeanMetric ( keras.metrics.Metric )