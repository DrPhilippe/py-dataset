# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pytools.assertions

# LOCALS
from .. import yolo_v1_utils
from .mean_metric import MeanMetric

# ##################################################
# ###               CLASS YOLO-V1                ###
# ##################################################

class YoloV1 ( MeanMetric ):
	"""
	The YOLO V1 metric. Computes the average number of correct bounding rectangle IoU.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		number_of_cells=7,
		number_of_detectors_per_cell=2,
		number_of_classes=20,
		classification_threshod=0.75,
		localization_threshold=0.5,
		**kwargs
		):
		"""
		Initializes a new instance of the `pykeras.metrics.YoloV1` metric.

		Arguments:
			number_of_cells              (`int`): Number of cells both horizontaly and verticaly.
			number_of_detectors_per_cell (`int`): Number of detectors in each cells.
			number_of_classes            (`int`): Number of classes to classify.
			threshold                  (`float`): Threashold used to separate correnct and incorrect example based on IoU value.
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloV1 )
		assert pytools.assertions.type_is( number_of_cells, int )
		assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
		assert pytools.assertions.type_is( number_of_classes, int )
		assert pytools.assertions.type_is( classification_threshod, float )
		assert pytools.assertions.type_is( localization_threshold, float )

		super( YoloV1, self ).__init__( **kwargs )

		self.number_of_cells              = number_of_cells
		self.number_of_detectors_per_cell = number_of_detectors_per_cell
		self.number_of_classes            = number_of_classes
		self.classification_threshod      = classification_threshod
		self.localization_threshold       = localization_threshold

		self.rectangle_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			1.0, 1.0, 1.0, 1.0, 0.0
			)

		self.confidence_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			0.0, 0.0, 0.0, 0.0, 1.0
			)

		self.classes_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			0.0, 0.0, 0.0, 0.0, 0.0,
			numpy.ones( number_of_classes )
			)

		self.x_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			1.0, 0.0, 0.0, 0.0, 0.0
			)
		self.y_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			0.0, 1.0, 0.0, 0.0, 0.0
			)
		self.w_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			0.0, 0.0, 1.0, 0.0, 0.0
			)
		self.h_mask = yolo_v1_utils.build_weighting_mask(
			number_of_cells, number_of_detectors_per_cell, number_of_classes,
			0.0, 0.0, 0.0, 1.0, 0.0
			)

	# def __init__ ( self, ... )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	def centered_to_conventional_rectangle ( self, cells ):
		"""
		Converts the yolo cell values from rectangle centered on the cells to rectangle conventional coordinates.

		Arguments:
			cell (`tensorflow.Tensor`): cells values with shape [S,S, 5*B + C]
		"""

		# Number of cells			
		nb_cells = tensorflow.shape( y_true )[ 0 ]

		# Get rectangles values
		xs = tensorflow.math.multiply( cells, self.x_mask ) # X values and 0 everywhere else
		ys = tensorflow.math.multiply( cells, self.y_mask ) # Y values and 0 everywhere else
		ws = tensorflow.math.multiply( cells, self.w_mask ) # W values and 0 everywhere else
		hs = tensorflow.math.multiply( cells, self.h_mask ) # H values and 0 everywhere else

		# Convert width and height from image relative size to cell relative size
		ws = tensorflow.math.multiply( ws, nb_cells )
		hs = tensorflow.math.multiply( hs, nb_cells )

		# Compute half of width and height
		half_ws = tensorflow.math.divide( ws, 2.0 ) # W/2 values
		half_hs = tensorflow.math.divide( hs, 2.0 ) # H/2 values

		# Align W/H values with X/Y coordinates in tensors
		aligned_half_ws = tensorflow.roll( half_ws, shift=-2, axis=2 ) # W/2 values aligned with X values
		aligned_half_hs = tensorflow.roll( half_hs, shift=-2, axis=2 ) # H/2 values aligned with Y values

		# Compute x1,y1 and x2,y2
		x1 = tensorflow.math.subtract( xs, aligned_half_ws )
		y1 = tensorflow.math.subtract( ys, aligned_half_hs )
		x2 = tensorflow.math.add( xs, aligned_half_ws )
		y2 = tensorflow.math.add( ys, aligned_half_hs )

		# X1 and Y1 are already at the right indexes,
		# but we need X2 in the place of W and Y2 in the place of H
		x2 = tensorflow.roll( x2, shift=2, axis=2 )
		y2 = tensorflow.roll( y2, shift=2, axis=2 )

		# Put everything back in the cells, using addition
		cells = tensorflow.math.add( x1, y1 )
		cells = tensorflow.math.add( cells, x2 )
		cells = tensorflow.math.add( cells, y2 )
		cells = tensorflow.math.add( cells, tensorflow.math.multiply(cells, self.confidence_mask) ) # put back the confidence values
		cells = tensorflow.math.add( cells, tensorflow.math.multiply(cells, self.classes_mask) ) # put back the class probabilities
		return cells

	# def centered_to_conventional_rectangle ( self, cells )
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this mean metric given a batch of labels and associated predictions.

		By default this method adds `batch_size` to the `count` variable.
		
		Arguments:
			self (`pykeras.metrics.MeanMetric`): Mean metric instance to update.
			y_true             (`keras.Tensor`): Batch of labels.
			y_pred             (`keras.Tensor`): Batch of predictions.
			sample_weight      (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeanMetric )
				
		# Each cell must be correct, thus we will compare to the number of cell
		y_true_shape = tensorflow.shape( y_true )
		nb_cells     = tensorflow.multiply( y_true_shape[0], y_true_shape[1] )
		self.count.assign_add( nb_cells )

		# Identify cells with the corrent class probabilities
		y_true_classes  = tensorflow.math.multiply( y_true, self.cateogries_mask ) # only consider class probabilities by zero'ing everything else
		y_pred_classes  = tensorflow.math.multiply( y_pred, self.cateogries_mask ) # only consider class probabilities by zero'ing everything else
		y_pred_classes  = tensorflow.math.greater_equal( y_true, self.classification_threshod ) # compare labels to decision threshold
		y_pred_classes  = tensorflow.math.greater_equal( y_pred, self.classification_threshod ) # compare predictions to decision threshold
		correct_classes_logits = tensorflow.math.equal( y_pred_classes, y_true_classes ) # Correct logits are equal in label and predictions
		correct_classes_cells  = tensorflow.math.reduce_all( correct_logits ) # A cell is correct if all logits are predicted correclty
		

		

		# Identift cells with the corrent rectangle
		y_true_rectangles = tensorflow.math.multiply( y_true, self.rectangle_mask ) # only consider rectangle coordinates by zero'ing everything else
		y_pred_rectangles = tensorflow.math.multiply( y_pred, self.rectangle_mask ) # only consider rectangle coordinates by zero'ing everything else
		iou_rectangles    = 
		correct_rectangles = 
		correct_classes_cells  = tensorflow.math.reduce_all( correct_rectangles ) # A cell is correct if all rectangles are predicted correclty

		
		# First we will count the number of rectangle in the labels


	# def update_state ( self, y_true, y_pred, sample_weight )

# class YoloV1 ( keras.losses.Loss )