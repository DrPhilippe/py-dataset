# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.logits
import pytools.assertions

# LOCALS
from .logits_mean_metric import LogitsMeanMetric

# ##################################################
# ###           CLASS LOGITS-ACCURACY            ###
# ##################################################

class LogitsAccuracy ( LogitsMeanMetric ):
	"""
	Metric measuring the average number of correctly predicted logits.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.LogitsAccuracy` class.

		Arguments:
			self (`pykeras.metrics.LogitsAccuracy`): Instance to initialize.
			activation                      (`str`): Name of the activation function used on the logits.
			threshold                     (`float`): Threshold used to separate positive and negative logits.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsAccuracy )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )

		# Initialize the Metric parent class
		super( LogitsAccuracy, self ).__init__( activation, threshold, **kwargs )

	# def __init__( self, activation, threshold, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this accuracy metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsAccuracy`): Instance to update.
			y_true           (`keras.Tensor`): Batch of labels.
			y_pred           (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsAccuracy )
		
		# Apply activation and compare to threshold
		y_true, y_pred = self.apply_activation_and_threshold( y_true, y_pred )
		
		# Number of logits in the batch (area of the tensor)		
		batch_target = pykeras.logits.count_logits( y_true, dtype='float32' )
		self.target.assign_add( batch_target )

		# Compare predictions to labels to identify correctly predicted logits
		# and count them
		batch_count = pykeras.logits.count_true_logits( y_true, y_pred, axis=None, dtype='float32' )
		self.count.assign_add( batch_count )
		
	# def update_state ( self, y_true, y_pred, sample_weight )

# class LogitsAccuracy ( LogitsMetric )