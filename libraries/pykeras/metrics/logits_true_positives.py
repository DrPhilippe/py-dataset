# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError
import numpy

# INTERNALS
import pykeras.logits
import pytools.assertions

# LOCALS
from .logits_count_metric import LogitsCountMetric

# ##################################################
# ###        CLASS LOGITS-TRUE-POSITIVES         ###
# ##################################################

class LogitsTruePositives ( LogitsCountMetric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, activation=None, threshold=0.75, normalized=True, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.LogitsTruePositives` class.

		Arguments:
			self (`pykeras.metrics.LogitsTruePositives`): Instance to initialize.
			activation         (`str`/`None`/`function`): Activation function used on the logits or its name.
			threshold                          (`float`): Threshold used to separate positive and negative logits.
			normalized                          (`bool`): IF `True` returns a value in the range `[0, 1]`.
		
		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsTruePositives )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )
		assert pytools.assertions.type_is( normalized, bool )

		# Initialize the Metric parent class
		super( LogitsTruePositives, self ).__init__( activation, threshold, normalized, **kwargs )

	# def __init__( self, activation, threshold, normalized, **kwargs )

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this true positives metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsTruePositives`): Instance to update.
			y_true                      (`keras.Tensor`): Batch of labels.
			y_pred                      (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsTruePositives )
				
		# Apply activation and compare to threshold
		y_true, y_pred = self.apply_activation_and_threshold( y_true, y_pred )

		# Count true positives
		true_positives = pykeras.logits.count_true_positives( y_true, y_pred, axis=None, dtype='float32' )
		self.count.assign_add( true_positives )

		# Number of positive logits in the batch
		positives = pykeras.logits.count_positives( y_true, axis=None, dtype='float32' )
		self.target.assign_add( positives )
		
	# def update_state ( self, y_true, y_pred )

# class LogitsTruePositives ( LogitsCountMetric )