# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pytools.assertions

# LOCALS
from .mean_distance_accuracy import MeanDistanceAccuracy

# ##################################################
# ###         CLASS TRANSLATION-ACCURACY         ###
# ##################################################

class TranslationAccuracy ( MeanDistanceAccuracy ):
	"""
	Metric measuring the average number of correctly predicted translation vectors.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=0.05, norm_order='euclidean', axes='xyz', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.TranslationAccuracy` class.
		
		If the distance between predicted and ground is greather than or equal
		to the given threshold, it is considered false, otherwise, it is considered to be true.
		
		See `tensorflow.norm` for accepted `norm_order` values.
		Axes can be one of 'x', 'y', 'z' or any combinaison.

		Arguments:
			threshold              (`float`): Distance threshold.
			norm_order (`str`/`float`/`int`): Order of the distance norm.
			axes                     (`str`): Axes on which to compute the metric.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, TranslationAccuracy )
		
		# Initialize the Metric parent class
		super( TranslationAccuracy, self ).__init__( threshold, norm_order, axes, **kwargs )

	# def __init__( self, threshold, **kwargs )

# class TranslationAccuracy ( MeanDistanceAccuracy )