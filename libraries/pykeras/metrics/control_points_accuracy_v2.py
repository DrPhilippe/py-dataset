# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import tensorflow
from tensorflow import keras

# INTERNALS
import pykeras
import pytools

# LOCALS
from .mean_distance_accuracy import MeanDistanceAccuracy

# ##################################################
# ###      CLASS CONTROL-POINTS-ACCURACY-V2      ###
# ##################################################

class ControlPointsAccuracyV2 ( MeanDistanceAccuracy ):
	"""
	Metric measuring the average number of correctly predicted control points.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, threshold=5.0, norm_order='euclidean', axes='xy', **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.ControlPointsAccuracyV2` class.
		
		If the distance between predicted and ground is greather than or equal to
		the given threshold, it is considered false (0. accuracy), otherwise, it is considered to be true.

		See `tensorflow.norm` for accepted `norm_order` values.
		Axes can be one of 'x', 'y', 'z' or any combinaison.

		Arguments:
			threshold              (`float`): Distance threshold (in pixel or relative image size).
			norm_order (`str`/`float`/`int`): Order of the distance norm.
			axes                     (`str`): Axes on which to compute the metric.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsAccuracyV2 )
		
		# Initialize the Metric parent class
		super( ControlPointsAccuracyV2, self ).__init__( threshold, norm_order, axes, **kwargs )

	# def __init__( self, threshold, **kwargs )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this accuracy metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.ControlPointsAccuracyV2`): Instance to update.
			y_true                          (`keras.Tensor`): Batch of labels.
			y_pred                          (`keras.Tensor`): Batch of predictions.
			sample_weight                   (`keras.Tensor`): Batch of sample weights.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsAccuracyV2 )
		
		# Update the count variable with the batch size
		super( MeanDistanceAccuracy, self ).update_state( y_true, y_pred, sample_weight )

		# Reshape predictions and label to suite MeanDistanceAccuracy's needs
		# y_true = tensorflow.reshape( y_true, [-1, 2] )
		# y_pred = tensorflow.reshape( y_pred, [-1, 2] )
		
		# Compute distances from predicted translation to ground truth translation
		distances = pykeras.geometry.distances( y_true, y_pred, norm_order=self.norm_order, axis=-1 )

		# Compare distances to threshold
		correct = pykeras.geometry.compare_distances( distances, self.threshold )

		# Revert the shape back to one distance per control point
		# correct = tensorflow.reshape( correct, [-1, 8] )

		# A control point set is correct if the 8 points are all correct
		correct = tensorflow.math.reduce_all( correct, axis=-1 )

		# Count the number of correct control point set
		batch_total = pykeras.logits.count_positives( correct, axis=-1, dtype='float32' )
		self.total.assign_add( batch_total )
				
	# def update_state ( self, y_true, y_pred, sample_weight )

# class ControlPointsAccuracyV2 ( MeanDistanceAccuracy )