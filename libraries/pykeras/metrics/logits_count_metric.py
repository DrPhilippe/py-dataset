# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
from .logits_mean_metric import LogitsMeanMetric

# ##################################################
# ###         CLASS LOGITS-COUNT-METRIC          ###
# ##################################################

class LogitsCountMetric ( LogitsMeanMetric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, normalized=True, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, LogitsCountMetric )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )
		assert pytools.assertions.type_is( normalized, bool )

		# Initialize the Metric parent class
		super( LogitsCountMetric, self ).__init__( activation, threshold, **kwargs )

		# Init fields
		self._normalized = normalized

	# def __init__( self, activation, threshold, normalized, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def normalized ( self ):
		"""
		Boolean indicating if the result of this metric should be normalized (`bool`).
		"""
		return self._normalized
	
	# def normalized ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_config ( self ):
		"""
		Returns the config of this logits count metric.

		Arguments:
			self (`pykeras.metrics.LogitsCountMetric`): Instance of which to get the config.

		Returns:
			`dict`: configuration data.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsCountMetric )
		
		config = super( LogitsCountMetric, self ).get_config()
		config.update({
			'normalized': self.normalized
		})
		return config
	
	# def get_config( self )

	# --------------------------------------------------

	def result ( self ):
		"""
		Computes and returns the result of this metric.

		Arguments:
			self (`pykeras.metrics.LogitsCountMetric`): Instance of which to get the result.

		Returns:
			`tensorflow.Tensor`: Tensor corresponding to the result.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsCountMetric )

		if self.normalized:
			return tensorflow.math.divide(
				self.count,
				tensorflow.math.add( self.target, numpy.finfo( numpy.float32 ).eps )
				)
		else:
			return self.count

	# def result ( self )


# class LogitsCountMetric ( LogitsMeanMetric )