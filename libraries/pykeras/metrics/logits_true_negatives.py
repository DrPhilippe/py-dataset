# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.logits
import pytools.assertions

# LOCALS
from pykeras.metrics.logits_count_metric import LogitsCountMetric

# ##################################################
# ###        CLASS LOGITS-TRUE-NEGATIVES         ###
# ##################################################

class LogitsTrueNegatives ( LogitsCountMetric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, normalized=True, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.LogitsTrueNegatives` class.

		Arguments:
			self (`pykeras.metrics.LogitsTrueNegatives`): Instance to initialize.
			activation         (`str`/`None`/`function`): Activation function used on the logits or its name.
			threshold                          (`float`): Threshold used to separate positive and negative logits.
			normalized                          (`bool`): IF `True` returns a value in the range `[0, 1]`.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsTrueNegatives )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )
		assert pytools.assertions.type_is( normalized, bool )

		# Initialize the Metric parent class
		super( LogitsTrueNegatives, self ).__init__( activation, threshold, normalized, **kwargs )

	# def __init__( self, activation, threshold, normalized, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this true positives metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsTrueNegatives`): Instance to update.
			y_true                      (`keras.Tensor`): Batch of labels.
			y_pred                      (`keras.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsTrueNegatives )
			
		# Apply activation and compare to threshold
		y_true, y_pred = self.apply_activation_and_threshold( y_true, y_pred )

		# Identify and count true negatives
		true_negatives = pykeras.logits.count_true_negatives( y_true, y_pred, axis=None, dtype='float32' )
		self.count.assign_add( true_negatives )
		
		# Count the number of negatives logits label in the batch
		negatives = pykeras.logits.count_negatives( y_true, axis=None, dtype='float32' )
		self.target.assign_add( negatives )

	# def update_state ( self, y_true, y_pred, sample_weight )

# class LogitsTrueNegatives ( LogitsCountMetric )