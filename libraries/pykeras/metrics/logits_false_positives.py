# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pykeras.logits
from .logits_count_metric import LogitsCountMetric

# ##################################################
# ###        CLASS LOGITS-FALSE-POSITIVES        ###
# ##################################################

class LogitsFalsePositives ( LogitsCountMetric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, normalized=True, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, LogitsFalsePositives )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )
		assert pytools.assertions.type_is( normalized, bool )

		# Initialize the Metric parent class
		super( LogitsFalsePositives, self ).__init__( activation, threshold, normalized, **kwargs )

	# def __init__( self, activation, threshold, normalized, **kwargs )

	# --------------------------------------------------

	def result ( self ):
		"""
		Computes and returns the result of this true positives metric.
	
		When not normalized this is the number of correctly predicted logits.

		Arguments:
			self (`pykeras.metrics.LogitsFalsePositives`): Instance of which to get the result.

		Returns:
			`keras.Tensor`: Single value tensor corresponding to the number of true positives.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsFalsePositives )

		if self.normalized:
			return tensorflow.math.subtract(
				1.0,
				tensorflow.math.divide(
					self.count,
					tensorflow.math.add( self.target, numpy.finfo( numpy.float32 ).eps )
					)
				)
		else:
			return self.count

	# def result ( self )

	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this true positives metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsFalsePositives`): Instance to update.
			y_true                  (`tensorflow.Tensor`): Batch of labels.
			y_pred                  (`tensorflow.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsFalsePositives )
		
		# Apply activation and compare to threshold
		y_true, y_pred = self.apply_activation_and_threshold( y_true, y_pred )

		# Identify and count false positives
		batch_count = pykeras.logits.count_false_positives( y_true, y_pred, axis=None, dtype='float32' )
		self.count.assign_add( batch_count )

		# Number of negatives logits in the batch
		negatives = pykeras.logits.count_negatives( y_true, axis=None, dtype='float32' )
		self.target.assign_add( negatives )
		
	# def update_state ( self, y_true, y_pred )

# class LogitsFalsePositives ( LogitsCountMetric )