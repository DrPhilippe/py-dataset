# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pytools.assertions

# ##################################################
# ###       CLASS LOGITS-CONFUSION-MATRIX        ###
# ##################################################

class LogitsConfusionMatrix ( keras.metrics.Metric ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, number_of_classes=1000, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, LogitsConfusionMatrix )
		assert pytools.assertions.type_is( number_of_classes, int )

		# Initialize the Metric parent class
		super( LogitsConfusionMatrix, self ).__init__( **kwargs )

		# Init fields
		self.number_of_classes = number_of_classes

		# Init value
		self.confusion_matrix = self.add_weight(
			       name = 'confusion_matrix',
			      shape = (number_of_classes, number_of_classes),
			initializer = keras.initializers.Zeros,
			      dtype = 'int32'
			)

	# def __init__( self, number_of_classes, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_config ( self ):
		"""
		Returns the config of this confusion matrix metric.

		Arguments:
			self (`pykeras.metrics.LogitsConfusionMatrix`): Instance of which to get the config.

		Returns:
			`dict`: configuration data.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsConfusionMatrix )
		
		config = super( LogitsConfusionMatrix, self ).get_config()
		config.update({
			'number_of_classes': self.number_of_classes
		})
		return config
	
	# def get_config( self )

	
	# --------------------------------------------------

	def reset_state ( self ):
		"""
		Resets this confusion matrix metric.

		Arguments:
			self (`pykeras.metrics.LogitsConfusionMatrix`): Instance to reset.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsConfusionMatrix )

		self.confusion_matrix.assign(
			tensorflow.zeros_like( self.confusion_matrix )
			)

	# def reset_state ( self )

	# --------------------------------------------------

	def result ( self ):
		"""
		Computes and returns the result of this confusion matrix metric.

		Arguments:
			self (`pykeras.metrics.LogitsConfusionMatrix`): Instance of which to get the result.

		Returns:
			`keras.Tensor`: Confusion matrix.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsConfusionMatrix )

		return self.confusion_matrix

	# def result ( self )
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this confusion matrix metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsConfusionMatrix`): Instance to update.
			y_true                   (`tensorflow.Tensor`): Batch of labels.
			y_pred                   (`tensorflow.Tensor`): Batch of predictions.
			sample_weight            (`tensorflow.Tensor`): Weight of the predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsConfusionMatrix )

		# Compute the confusion matrix on the current batch	
		mat = tensorflow.math.confusion_matrix( 
			keras.backend.argmax( y_true ),
			keras.backend.argmax( y_pred ),
			weights = sample_weight,
			num_classes = self.number_of_classes,
			dtype = 'int32'
			)

		# Add it to the global matrix
		self.confusion_matrix.assign_add( mat )

	# def update_state ( self, y_true, y_pred, sample_weight )

# class LogitsConfusionMatrix ( keras.metrics.Metric )