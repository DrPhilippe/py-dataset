# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError

# INTERNALS
import pykeras.logits
import pytools.assertions

# LOCALS
from .logits_mean_metric import LogitsMeanMetric

# ##################################################
# ###           CLASS LOGITS-PRECISION           ###
# ##################################################

class LogitsPrecision ( LogitsMeanMetric ):
	"""
	This metrics computes: true_positives / (true_positives + false_negatives)

	Thus its variables are equal to:
		`total` = `true_positives`
		`count = `true_positives` + `false_positives`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__( self, activation=None, threshold=0.75, **kwargs ):
		"""
		Initializes a new instance of the `pykeras.metrics.LogitsPrecision` class.

		Arguments:
			self (`pykeras.metrics.LogitsPrecision`): Instance to initialize.
			activation     (`str`/`None`/`function`): Activation function used on the logits or its name.
			threshold                      (`float`): Threshold used to separate positive and negative logits.

		Named Arguments:
			see `keras.metrics.Metric` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsPrecision )
		assert pytools.assertions.type_in( activation, (str, type(None)) )
		assert pytools.assertions.type_is( threshold, float )
		assert pytools.assertions.value_in_range( threshold, 0.0, 1.0 )

		# Initialize the Metric parent class
		super( LogitsPrecision, self ).__init__( activation, threshold, **kwargs )
	
	# def __init__( self, activation, threshold, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def update_state ( self, y_true, y_pred, sample_weight=None ):
		"""
		Updates this true positives metric given a batch of labels and associated predictions.

		Arguments:
			self (`pykeras.metrics.LogitsPrecision`): Instance to update.
			y_true             (`tensorflow.Tensor`): Batch of labels.
			y_pred             (`tensorflow.Tensor`): Batch of predictions.
		"""
		assert pytools.assertions.type_is_instance_of( self, LogitsPrecision )
		
		# Apply activation and compare to threshold
		y_true, y_pred = self.apply_activation_and_threshold( y_true, y_pred )

		# Count true positives and false positives
		tp = pykeras.logits.count_true_positives( y_true, y_pred, axis=None, dtype='float32' )
		fp = pykeras.logits.count_false_positives( y_true, y_pred, axis=None, dtype='float32' )
		
		# Update variables
		self.count.assign_add( tp )
		self.target.assign_add( tensorflow.math.add( tp, fp ) )

	# def update_state ( self, y_true, y_pred, sample_weight )

# class LogitsPrecision ( LogitsMeanMetric )