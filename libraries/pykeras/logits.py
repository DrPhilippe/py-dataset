# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from tensorflow import keras
if keras.backend.backend() == 'tensorflow':
	import tensorflow
else:
	raise NotImplementedError
import numpy

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def count_false_negatives ( y_true, y_pred, axis=None, dtype='int32' ):
	"""
	Counts the number of false negative logits in a batch of logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
		axis                 (`int`): Axis on which to count.
		dtype                (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	false_negatives = identify_false_negatives( y_true, y_pred )
	return count_positives( false_negatives, axis=axis, dtype=dtype )

# def count_false_negatives ( x, axis, dtype )

# --------------------------------------------------

def count_false_positives ( y_true, y_pred, axis=None, dtype='int32' ):
	"""
	Counts the number of false positives logits in a batch of logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
		axis                 (`int`): Axis on which to count.
		dtype                (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	false_positives = identify_false_positives( y_true, y_pred )
	return count_positives( false_positives, axis=axis, dtype=dtype )

# def count_false_positives ( x, axis, dtype )

# --------------------------------------------------

def count_logits ( x, dtype='int32' ):
	"""
	Counts the number of logits in a batch of logits.

	Arguments:
		x (`tensorflow.Tensor`): Batch of logits (dtype `bool`, shape `batch_size*classes`).
		dtype           (`str`): Dtype of the result tensor.

	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	shape = tensorflow.shape( x )
	count = tensorflow.math.multiply( shape[0], shape[1] )
	return tensorflow.cast( count, dtype )

# def count_logits ( x, dtype )

# --------------------------------------------------

def count_negatives ( x, axis=None, dtype='int32' ):
	"""
	Counts the number of negatives logits in a batch of logits.

	Arguments:
		x (`tensorflow.Tensor`): Batch of logits (dtype `bool`, shape `batch_size*classes`).
		axis            (`int`): Axis on which to count.
		dtype           (`str`): Dtype of the result tensor.

	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	negatives = tensorflow.math.logical_not( x )
	return count_positives( negatives, axis=axis, dtype=dtype )

# def count_negatives ( x, axis, dtype )

# --------------------------------------------------

def count_positives ( x, axis=None, dtype='int32' ):
	"""
	Counts the number of positives logits in a batch of logits.

	Arguments:
		x (`tensorflow.Tensor`): Batch of logits (dtype `bool`, shape `batch_size*classes`).
		axis            (`int`): Axis on which to count.
		dtype           (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	positives = tensorflow.cast( x, dtype )
	return tensorflow.math.reduce_sum( positives, axis=axis )

# def count_positives ( x, axis, dtype )

# --------------------------------------------------

def count_true_logits ( y_true, y_pred, axis=None, dtype='int32' ):
	"""
	Counts the number of true negative logits in a batch of logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
		axis                 (`int`): Axis on which to count.
		dtype                (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	true_logits = identify_true_logits( y_true, y_pred )
	return count_positives( true_logits, axis=axis, dtype=dtype )

# def count_true_logits ( x, axis, dtype )

# --------------------------------------------------

def count_true_negatives ( y_true, y_pred, axis=None, dtype='int32' ):
	"""
	Counts the number of true negative logits in a batch of logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
		axis                 (`int`): Axis on which to count.
		dtype                (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	true_negatives = identify_true_negatives( y_true, y_pred )
	return count_positives( true_negatives, axis=axis, dtype=dtype )

# def count_true_negatives ( x, axis, dtype )

# --------------------------------------------------

def count_true_positives ( y_true, y_pred, axis=None, dtype='int32' ):
	"""
	Counts the number of true positives logits in a batch of logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
		axis                 (`int`): Axis on which to count.
		dtype                (`str`): Dtype of the result tensor.
		
	Returns:
		`tensorflow.Tensor`: Single value tensor with type `dtype`
	"""
	true_positives = identify_true_positives( y_true, y_pred )
	return count_positives( true_positives, axis=axis, dtype=dtype )

# def count_true_positives ( x, axis, dtype )

# --------------------------------------------------

def identify_false_logits ( y_true, y_pred ):
	"""
	Identifies false logits in the predicted logits using labeled logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).

	Returns:
		`tensorflow.Tensor`: Batch of false logits (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [0, 1, 0, 1]
	"""
	return tensorflow.math.logical_xor( y_true, y_pred )

# def identify_false_logits ( y_true, y_pred )

# --------------------------------------------------

def identify_false_negatives ( y_true, y_pred ):
	"""
	Identifies false negatives in a batch of predicted logits using labeled logits.
	
	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
	
	Returns:
		`tensorflow.Tensor`: Batch of false negatives (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [0, 1, 0, 0]
	"""	
	return tensorflow.math.logical_and(
		y_true,
		tensorflow.math.logical_not( y_pred )
		)

# def identify_false_negatives ( y_true, y_pred )

# --------------------------------------------------

def identify_false_positives ( y_true, y_pred ):
	"""
	Identifies false positives in a batch of predicted logits using labeled logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).

	Returns:
		`tensorflow.Tensor`: Batch of false positives (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [0, 0, 0, 1]
	"""
	return tensorflow.math.logical_and(
		tensorflow.math.logical_not( y_true ),
		y_pred
		)	

# def identify_false_positives ( y_true, y_pred )

# --------------------------------------------------

def identify_true_logits ( y_true, y_pred ):
	"""
	Identifies true logits in the predicted logits using labeled logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).

	Returns:
		`tensorflow.Tensor`: Batch of true logits (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [1, 0, 1, 0]
	"""
	return tensorflow.math.logical_not(
		identify_false_logits( y_true, y_pred )
		)

# def identify_true_logits ( y_true, y_pred )

# --------------------------------------------------

def identify_true_negatives ( y_true, y_pred ):
	"""
	Identifies true negatives in a batch of predicted logits using labeled logits.
	
	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).
	
	Returns:
		`tensorflow.Tensor`: Batch of true negatives (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [0, 0, 1, 0]
	"""	
	return tensorflow.math.logical_and(
		tensorflow.math.logical_not( y_true ),
		tensorflow.math.logical_not( y_pred )
		)

# def identify_false_negatives ( y_true, y_pred )

# --------------------------------------------------

def identify_true_positives ( y_true, y_pred ):
	"""
	Identifies true positives in a batch of predicted logits using labeled logits.

	Arguments:
		y_true (`tensorflow.Tensor`): Batch of labeled logits (dtype `bool`, shape `batch_size*classes`).
		y_pred (`tensorflow.Tensor`): Batch of predicted logits (dtype `bool`, shape `batch_size*classes`).

	Returns:
		`tensorflow.Tensor`: Batch of true positives (dtype `bool`, shape `batch_size*classes`).

	Example:
		y_true: [1, 1, 0, 0]
		y_pred: [1, 0, 0, 1]
		result: [1, 0, 0, 0]
	"""
	return tensorflow.math.logical_and( y_true, y_pred )	

# def identify_true_positives ( y_true, y_pred )