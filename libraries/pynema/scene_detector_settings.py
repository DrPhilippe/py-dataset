# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS

# INTERNALS
import pydataset.manipulators
import pytools.assertions

# LOCALS

# ##################################################
# ###       CLASS SCENE-DETECTOR-SETTINGS        ###
# ##################################################

class SceneDetectorSettings ( pydataset.manipulators.MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, SceneDetectorSettings )

		super( SceneDetectorSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

# class SceneDetectorSettings ( pydataset.manipulators.ManipulatorSettings )