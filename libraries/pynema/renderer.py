# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.render
import pytools.assertions

# LOCALS
from .renderer_settings import RendererSettings

# ##################################################
# ###               CLASS RENDERER               ###
# ##################################################

class Renderer ( pydataset.render.Renderer ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, logger=None, progress_tracker=None ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( settings, RendererSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		super( Renderer, self ).__init__( settings, logger, progress_tracker )

		self.frames_iterator = None
		self.frames_count    = 0
		self.frames_index    = 0
		self._charuco_mesh   = None

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def late_update_scene ( self, scene ):
		"""
		Overload this method to update the scene after each render.

		Arguments:
			self      (`pylinemod.Renderer`): Renderer to update
			scene (`pydataset.render.Scene`): Scene to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )
		

		# Move to the next example
		self.frames_iterator.next()
		self.frames_index += 1
		
		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( float(self.frames_index)/float(self.frames_count) )

	# def late_update_scene ( self, scene )

	# --------------------------------------------------
	
	def must_close ( self ):
		
		return super( Renderer, self ).must_close() or self.frames_index >= self.frames_count

	# def must_close ( self )
	
	# --------------------------------------------------

	def save_render ( self, render ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( render, numpy.ndarray )

		# Get the example
		example = self.frames_iterator.current()
		
		# if the rendered object is one object of the scene
		# get its example
		if self.settings.target_object not in ['scene', 'board', 'charuco', 'objects']:
			example = example.get_example( self.settings.target_object )

		# Shape
		shape = self.settings.size.dimensions
		shape = [ shape[1], shape[0] ]

		# Save the render
		if self.settings.shader == 'color':

			example.create_or_update_feature(
				     feature_name = self.settings.render_feature_name,
				feature_data_type = pydataset.dataset.ImageFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( *shape, 4 ),
				            dtype = numpy.dtype( 'uint8' ),
				            value = render,
				        extension = '.png',
				            param = 0
				)

		else:			
			example.create_or_update_feature(
				     feature_name = self.settings.render_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( *shape ),
				            dtype = numpy.dtype( 'float16' ),
				            value = render[ :, :, 0 ].astype( numpy.float16 )
				)

		# Log
		if self.logger:
			self.logger.log( example.url )

	# def save_render ( self, render )

	# --------------------------------------------------

	def setup_scene ( self, scene ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )
		
		# Get the dataset and the camera parameters matrix
		dataset     = pydataset.dataset.get( self.settings.dataset_url )
		K           = dataset.get_feature( self.settings.camera_matrix_feature_name ).value
		scene_group = dataset.get( 'groups/scenes/groups/{}'.format(self.settings.scene_name) )
		scene_data  = scene_group.get_feature( self.settings.scene_data_feature_name ).value

		# Set the camera parameters matrix
		scene.camera = pydataset.render.cameras.OpenCVCamera.create(
			                x1 = 0.0,
			                y1 = 0.0,
			                x2 = float( self.settings.size[ 0 ] ),
			                y2 = float( self.settings.size[ 1 ] ),
			                 K = K.ravel().tolist(),
			window_coordinates = 'y_down'
			)

		# Create the entity for the OpenCV frame
		opencv_data = scene_data.get_child( 'OpenCV' )
		opencv = pydataset.render.Entity(
			  data = opencv_data,
			parent = scene
			)
		opencv.local_transform = pydataset.render.numpy_utils.invert_xy()

		# Get the charuco board data
		charuco_mesh_data = opencv_data.get_child( 'ChArUcO Board' )

		# Is the charuco board rendered ?
		if self.settings.target_object in ['scene', 'board', 'charuco']:

			# Create the appropriate shader program
			if self.settings.shader == 'color':
				shader_program = pydataset.render.shaders.ColorShaderProgram()
			else:
				shader_program = pydataset.render.shaders.DepthShaderProgram()

			# Setup the charuco mesh
			self._charuco_mesh = pydataset.render.CharucoMesh(
				          data = copy.deepcopy( charuco_mesh_data ),
				shader_program = shader_program,
				        parent = opencv
				)
			self._charuco_mesh.data.set_alpha( 1.0 )
		
		# The charuco board is not rendered
		else:

			# Replace the charuco board by an entity with no visual
			self._charuco_mesh = pydataset.render.Entity(
				parent = opencv
				)

		# Are all objects rendered ?
		if self.settings.target_object in ['scene', 'objects']:
			
			# Create a mesh entity for each object on the board
			for mesh_data in charuco_mesh_data.children_data:

				# Create the appropriate shader program
				if self.settings.shader == 'color':
					shader_program = pydataset.render.shaders.ColorShaderProgram()
				else:
					shader_program = pydataset.render.shaders.DepthShaderProgram()

				mesh = pydataset.render.Mesh(
					          data = copy.deepcopy( mesh_data ),
					shader_program = shader_program,
					        parent = self._charuco_mesh
					)
				mesh.data.set_alpha( 1.0 )
				
			# for mesh_data in board_data.children_data

		# Is only one object rendered
		elif self.settings.target_object not in ['board', 'charuco', 'objects']:

			# Get the data of the object
			mesh_data = charuco_mesh_data.get_child( self.settings.target_object )
			
			# Create the appropriate shader program
			if self.settings.shader == 'color':
				shader_program = pydataset.render.shaders.ColorShaderProgram()
			else:
				shader_program = pydataset.render.shaders.DepthShaderProgram()
			
			# Create the mesh
			mesh = pydataset.render.Mesh(
				          data = copy.deepcopy( mesh_data ),
				shader_program = shader_program,
				        parent = self._charuco_mesh
				)
			
			# Set its opacity
			mesh.data.set_alpha( 1.0 )

		# if self.settings.target_object == 'scene'

		# Examples iterator
		frames_url = '{}/groups/scenes/groups/{}/groups/{}/groups/*'.format( self.settings.dataset_url, self.settings.scene_name, self.settings.sub_scene_name )
		self.frames_iterator = pydataset.dataset.Iterator( frames_url )
		self.frames_count    = self.frames_iterator.count
		self.frames_index    = 0
		
	# def setup_scene ( self, scene )

	# --------------------------------------------------
	
	def update_scene ( self, scene ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )

		# Get the group of the current frame
		frame_group = self.frames_iterator.current()
		
		# # Is the charuco board rendered ?
		# if self.settings.target_object in ['scene', 'board', 'charuco']:

		# Get the category name, rotation matrix and translation vector
		R = frame_group.get_feature( self.settings.rotation_matrix_feature_name    ).value
		t = frame_group.get_feature( self.settings.translation_vector_feature_name ).value
		
		# Set the pose of the charuco board on that frame
		self._charuco_mesh.local_transform = pydataset.render.numpy_utils.affine_transform( R, t )

	# def update_scene ( self, scene )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a new renderer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pynema.RendererSettings`: The settings.
		"""
		return RendererSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Renderer ( pydataset.render.Renderer )