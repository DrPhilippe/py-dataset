# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pydataset.render
import pytools.assertions
import pytools.serialization

# ##################################################
# ###          CLASS RENDERER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RendererSettings',
	   namespace = 'pynema',
	fields_names = [
		'dataset_url',
		'scene_name',
		'sub_scene_name',
		'scene_data_feature_name',
		'camera_matrix_feature_name',
		'rotation_matrix_feature_name',
		'translation_vector_feature_name',
		'shader',
		'target_object'
		]
	)
class RendererSettings ( pydataset.render.RendererSettings ):
	"""
	Settings for the LINEMOD renderer.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		dataset_url = '',
		scene_name = '',
		sub_scene_name = '',
		scene_data_feature_name = 'scene_data',
		camera_matrix_feature_name = 'camera_K',
		rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		shader = 'color',
		target_object = 'scene',
		**kwargs
		):
		"""
		Initializes a new instance of the `pynema.RendererSettings` class.

		Arguments:
			self        (`pynema.RendererSettings`): Instance to initialize.
			dataset_url                     (`str`): URL of the linemod dataset.
			scene_name                      (`str`): Name of the scene to render.
			sub_scene_name                  (`str`): Name of the sub-scene to render.
			scene_data_feature_name         (`str`): Name of the feature containing the scene data.			
			camera_matrix_feature_name      (`str`): Name of the feature containing the camera parameters matrix.
			rotation_matrix_feature_name    (`str`): Name of the feature containing the model view rotation matrix.
			translation_vector_feature_name (`str`): Name of the feature containing the model view translation vector.
			shader                          (`str`): Shader used to render the mesh - 'color' or 'depth'.
			target_object                   (`str`): Target object that are rendered.

		Named Arguments:
			render_feature_name       (`str`): Name of the feature in which to save the renders.
			is_dry_running           (`bool`): Boolean indicating if the renderer is dry-running.\
			size (`pydataset.data.ShapeData`): Size of the rendered image.
			name                      (`str`): Name of the renderer.
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( dataset_url, str )
		assert pytools.assertions.type_is( scene_name, str )
		assert pytools.assertions.type_is( sub_scene_name, str )
		assert pytools.assertions.type_is( scene_data_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( rotation_matrix_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is( shader, str )
		assert pytools.assertions.value_in( shader, ['color', 'depth'] )
		assert pytools.assertions.type_is( target_object, str )

		super( RendererSettings, self ).__init__(
			**kwargs
			)

		self._dataset_url                     = dataset_url
		self._scene_name                      = scene_name
		self._sub_scene_name                  = sub_scene_name
		self._scene_data_feature_name         = scene_data_feature_name
		self._camera_matrix_feature_name      = camera_matrix_feature_name
		self._rotation_matrix_feature_name    = rotation_matrix_feature_name
		self._translation_vector_feature_name = translation_vector_feature_name
		self._shader                          = shader
		self._target_object                   = target_object

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dataset_url ( self ):
		"""
		URL of the linemod dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._dataset_url
	
	# def dataset_url ( self )

	# --------------------------------------------------

	@dataset_url.setter
	def dataset_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._dataset_url = value
	
	# def dataset_url ( self, value )

	# --------------------------------------------------

	@property
	def scene_name ( self ):
		"""
		Name of the scene to render (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._scene_name
	
	# def scene_name ( self )

	# --------------------------------------------------

	@scene_name.setter
	def scene_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._scene_name = value
	
	# def scene_name ( self, value )

	# --------------------------------------------------

	@property
	def sub_scene_name ( self ):
		"""
		Name of the sub-scene to render (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._sub_scene_name
	
	# def sub_scene_name ( self )

	# --------------------------------------------------

	@sub_scene_name.setter
	def sub_scene_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._sub_scene_name = value
	
	# def sub_scene_name ( self, value )

	# --------------------------------------------------

	@property
	def scene_data_feature_name ( self ):
		"""
		Name of the feature containing the scene data (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._scene_data_feature_name
	
	# def scene_data_feature_name ( self )

	# --------------------------------------------------

	@scene_data_feature_name.setter
	def scene_data_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._scene_data_feature_name = value
	
	# def scene_data_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_matrix_feature_name ( self ):
		"""
		Name of the feature containing the model view rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._rotation_matrix_feature_name
	
	# def rotation_matrix_feature_name ( self )

	# --------------------------------------------------

	@rotation_matrix_feature_name.setter
	def rotation_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_matrix_feature_name = value
	
	# def rotation_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		"""
		Name of the feature containing the model view translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._translation_vector_feature_name
	
	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_vector_feature_name = value
	
	# def translation_vector_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def shader ( self ):
		"""
		Shader used to render the mesh: 'color' or 'depth' (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._shader
	
	# def shader ( self )

	# --------------------------------------------------

	@shader.setter
	def shader ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['color', 'depth'] )
	
		self._shader = value
	
	# def shader ( self, value )

	# --------------------------------------------------

	@property
	def target_object ( self ):
		"""
		Target object that are rendered (`str`).

		Use `'scene'` to render the charuco board and the object(s) on it.
		Use `'board'` or `'charuco`' to render the charuco board without any object.
		Use `'objects'` to render all objects but the board.
		Use the name of an object to render only this object.
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._target_object
	
	# def target_object ( self )

	# --------------------------------------------------

	@target_object.setter
	def target_object ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._target_object = value
	
	# def target_object ( self, value )

# class RendererSettings ( pydataset.render.RendererSettings )