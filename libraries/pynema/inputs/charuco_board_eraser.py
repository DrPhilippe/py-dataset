# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import random
import tensorflow

# INTERNALS
import pytools
import pykeras

# LOCALS
from .charuco_board_eraser_settings import ChArUcoBoardEraserSettings

# ##################################################
# ###         CLASS CHARUCO-BOARD-ERASER         ###
# ##################################################

class ChArUcoBoardEraser ( pykeras.inputs_v2.mappers.Mapper ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instane of the `pynema.inputs.ChArUcoBoardEraser` class.

		Arguments:
			self             (`pynema.inputs.ChArUcoBoardEraser`): Instance to initialize.
			settings (`pynema.inputs.ChArUcoBoardEraserSettings`): Settings of the charuco board eraser.
		"""
		assert pytools.assertions.type_is_instance_of( self, ChArUcoBoardEraser )
		assert pytools.assertions.type_is_instance_of( settings, ChArUcoBoardEraserSettings )

		super( ChArUcoBoardEraser, self ).__init__( settings )

	# def __init__ ( self, settings )

	# --------------------------------------------------

	def map_features ( self, features ):
		"""
		Replace the background in the given set of features.

		Arguments:
			self         (`pynema.inputs.ChArUcoBoardEraser`): Mapper instance.
			features (`dict` of `str` to `tensorflow.Tensor`): Dictionary of features to map.

		Returns:
			`dict` of `str` to `tensorflow.Tensor`: Mapped set of features.
		"""
		assert pytools.assertions.type_is_instance_of( self, ChArUcoBoardEraser )
		assert pytools.assertions.type_is( features, dict )
		assert pytools.assertions.dictionary_types_are( features, str, tensorflow.Tensor )
		
		# Fetch features
		image      = features[ self.settings.image_feature_name      ]
		mask       = features[ self.settings.mask_feature_name       ]
		background = features[ self.settings.background_feature_name ]

		# Erase background
		image = tensorflow.where( mask, image, background )

		# Update and return features
		features[ self.settings.image_feature_name ] = image
		return features

	# def map_features ( self, features )

# class ChArUcoBoardEraser ( pykeras.inputs_v2.mappers.Mapper )