# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools
import pykeras

# ##################################################
# ###    CLASS CHARUCO-BOARD-ERASER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ChArUcoBoardEraserSettings',
	   namespace = 'pykeras.inputs',
	fields_names = [
		'image_feature_name',
		'mask_feature_name',
		'background_feature_name'
		]
	)
class ChArUcoBoardEraserSettings ( pykeras.inputs_v2.mappers.MapperSettings ):
	"""
	Used to replace the charuco board with the bacground.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self,
		image_feature_name='foreground_color',
		mask_feature_name='full_mask',
		background_feature_name = 'backgound_color',
		name='charuco-eraser'
		):
		assert pytools.assertions.type_is_instance_of( self, ChArUcoBoardEraserSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( background_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( ChArUcoBoardEraserSettings, self ).__init__( name )

		self._image_feature_name      = image_feature_name
		self._mask_feature_name       = mask_feature_name
		self._background_feature_name = background_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the image feature of which to replace the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def background_feature_name ( self ):
		"""
		Name of the feature containing the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )

		return self._background_feature_name

	# def background_feature_name ( self )

	# --------------------------------------------------

	@background_feature_name.setter
	def background_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundExchangerSettings )
		assert pytools.assertions.type_is( value, str )

		self._background_feature_name = value

	# def background_feature_name ( self, value )

# class ChArUcoBoardEraserSettings ( pykeras.inputs_v2.mappers.MapperSettings )