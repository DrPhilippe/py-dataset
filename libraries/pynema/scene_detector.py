# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import transforms3d

# INTERNALS
import pydataset.charuco
import pydataset.manipulators
import pydataset.render
import pytools.assertions

# LOCALS
from .scene_detector_settings import SceneDetectorSettings

# ##################################################
# ###            CLASS SCENE-DETECTOR            ###
# ##################################################

class SceneDetector ( pydataset.manipulators.Mapper ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, SceneDetector )
		assert pytools.assertions.type_is_instance_of( settings, SceneDetectorSettings )

		super( SceneDetector, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.SceneDetector`): SceneDetector used to manipulate the example.
			root_group        (`pydataset.dataset.Group`): Group containing the examples that will be manipulated.
			number_of_examples                    (`int`): Number of examples to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneDetector )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		# Load the scene data, the charuco data, the camera intrinsic parameters
		self.scene_data   = root_group.get_feature( 'scene_data'         ).value
		self.charuco_data = root_group.get_feature( 'charuco_board'      ).value
		self.K            = root_group.get_feature( 'camera_K'           ).value
		self.dist_coeffs  = root_group.get_feature( 'camera_dist_coeffs' ).value

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def map ( self, group ):
		assert pytools.assertions.type_is_instance_of( self, SceneDetector )
		assert pytools.assertions.type_is_instance_of( group, pydataset.dataset.Group )
		
		# Get image, charuco R & t
		image     = group.get_feature( 'foreground_color' ).value
		charuco_R = group.get_feature( 'R' ).value
		charuco_t = group.get_feature( 't' ).value

		# Compute affine transform
		charuco_T = numpy.eye( 4, dtype=numpy.float32 )
		charuco_T[:3, :3] = charuco_R
		charuco_T[:3,  3] = charuco_t

		# Load scene data
		opencv_data        = self.scene_data.get_child( 'OpenCV' )
		charuco_board_data = opencv_data.get_child( 'ChArUcO Board' )

		# Go though mesh on the board
		for entity_data in charuco_board_data.children_data:

			# Is the entity a mesh
			if isinstance ( entity_data, pydataset.render.MeshData ):

				# Get the example for this mesh entity
				if group.contains_example( entity_data.name ):
					example = group.get_example( entity_data.name )
				else:
					example = group.create_example( entity_data.name )

				# Mesh local transform
				mesh_T = numpy.asarray( entity_data.local_transform, dtype=numpy.float32 )
				mesh_T = numpy.reshape( mesh_T, [4, 4] )

				# Mesh global transform
				T = numpy.dot( charuco_T, mesh_T )

				# decompose
				mesh_t, mesh_R, Z, S = transforms3d.affines.decompose( T )

				# Save its pose
				example.create_or_update_feature(
					     feature_name = 'R',
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = mesh_R,
					      auto_update = False
					)
				example.create_or_update_feature(
					     feature_name = 't',
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = mesh_t,
					      auto_update = False
					)
				example.update()

		# 	# if isintance ( entity_data, pydataset.render.MeshData )

		# for entity_data in self._scene_data.children_data
		
		return example

	# def map ( self, example )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return SceneDetectorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SceneDetector ( pydataset.manipulators.Manipulator )