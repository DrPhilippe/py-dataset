# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import json
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .raw_sub_scene_exporter_settings import RawSubSceneExporterSettings

# ##################################################
# ###        CLASS RAW-SUB-SCENE-EXPORTER        ###
# ##################################################

class RawSubSceneExporter ( pytools.tasks.Task ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, **kwargs ):
		"""
		Initializes a new instance of the `pynema.RawSubSceneExporter` class.

		Arguments:
			self             (`pynema.RawSubSceneExporter`): Raw sub-scene exporter instance to initialize.
			settings (`pynema.RawSubSceneExporterSettings`): Raw sub-scene exporter settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporter )
		assert pytools.assertions.type_is_instance_of( settings, RawSubSceneExporterSettings )

		super( RawSubSceneExporter, self ).__init__( settings, **kwargs )
		
	# def __init__ ( self, settings, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this exporter.

		Arguments:
			self (`pynema.RawSubSceneExporter`): The raw sub-scene importer to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Get the scene
		sub_scene = pydataset.dataset.get( self.settings.sub_scene_url )
		
		# Log
		if self.logger:
			self.logger.log( 'SUB-SCENE:' )
			self.logger.log( '    absolute_url: {}', sub_scene.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Create output directory if needed
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()

		# Log
		if self.logger:
			self.logger.log( 'OUTPUT DIRECTORY:' )
			self.logger.log( '    output_directory_path: {}', self.settings.output_directory_path )
			self.logger.end_line()
			self.logger.flush()

		# Ground trouth file path
		gt_file_path = self.settings.output_directory_path + pytools.path.FilePath( 'gt.json' )
		gt_file      = open( str(gt_file_path), 'wt+' )
		gt_file.write('[\n\t')
		gt_file.flush()

		# Background directory
		background_directory_path = self.settings.output_directory_path + pytools.path.DirectoryPath( 'background' )
		if background_directory_path.is_not_a_directory():
			background_directory_path.create()

		# Get background image and depth
		background_color = sub_scene.get_feature( 'background_color' ).value.astype(numpy.uint8)
		background_depth = sub_scene.get_feature( 'background_depth' ).value.astype(numpy.uint16)
		# Images file pathes
		background_color_file_path = background_directory_path + pytools.path.FilePath( 'color.png' )
		background_depth_file_path = background_directory_path + pytools.path.FilePath( 'depth.png' )
		# Save them
		cv2.imwrite( str(background_color_file_path), background_color, [cv2.IMWRITE_PNG_COMPRESSION, 9] )
		cv2.imwrite( str(background_depth_file_path), background_depth, [cv2.IMWRITE_PNG_COMPRESSION, 9] )

		# Log
		if self.logger:
			self.logger.log( 'BACKGROUND:' )
			self.logger.log( '    background_directory_path:  {}', background_directory_path )
			self.logger.log( '    background_color_file_path: {}', background_color_file_path )
			self.logger.log( '    background_depth_file_path: {}', background_depth_file_path )
			self.logger.end_line()
			self.logger.flush()

		# Frames output directory
		frames_directory_path = self.settings.output_directory_path + pytools.path.DirectoryPath( 'frames' )
		if frames_directory_path.is_not_a_directory():
			frames_directory_path.create()

		# Frames iterator
		frames_url_search_pattern = self.settings.sub_scene_url + '/groups/*'
		frames_iterator = pydataset.dataset.Iterator( frames_url_search_pattern )
		frame_count = len(frames_iterator)
	
		# Log
		if self.logger:
			self.logger.log( 'FRAMES:' )
			self.logger.log( '    frames_url_search_pattern:  {}', frames_url_search_pattern )
			self.logger.log( '    frame_count: {}', frame_count )
			self.logger.end_line()
			self.logger.flush()

		# Go through all the frames
		for frame_index in range( frame_count ):

			# Get the frame example
			frame = frames_iterator.at( frame_index )

			# Log
			if self.logger:
				self.logger.log( 'FRAME N°{:06d}:', frame_index )
				self.logger.log( '    absolute_url: {}', frame.absolute_url )
				self.logger.end_line()
				self.logger.flush()

			# Foreground color and depth
			foreground_color = frame.get_feature( 'foreground_color' ).value.astype( numpy.uint8 )
			foreground_depth = frame.get_feature( 'foreground_depth' ).value.astype( numpy.uint16 )
			foreground_color_file_path = frames_directory_path + pytools.path.FilePath.format( '{:06d}_color.png', frame_index )
			foreground_depth_file_path = frames_directory_path + pytools.path.FilePath.format( '{:06d}_depth.png', frame_index )
			cv2.imwrite( str(foreground_color_file_path), foreground_color, [cv2.IMWRITE_PNG_COMPRESSION, 9] )
			cv2.imwrite( str(foreground_depth_file_path), foreground_depth, [cv2.IMWRITE_PNG_COMPRESSION, 9] )
		
			# Log
			if self.logger:
				self.logger.log( '    IMAGES')
				self.logger.log( '        foreground_color_file_path: {}', foreground_color_file_path )
				self.logger.log( '        foreground_depth_file_path: {}', foreground_depth_file_path )
				self.logger.end_line()
				self.logger.flush()

			# Build frame ground trouth
			frame_data = []

			# Charuco board
			frame_data.append({
				 'category_name': 'charuco',
				'category_index': 0,
				             'R': frame.get_feature( 'R' ).value.ravel().tolist(),
				             't': frame.get_feature( 't' ).value.ravel().tolist(),
				})

			# Log
			if self.logger:
				self.logger.log( '    CHARUCO:' )
				self.logger.log( '        R: {}', frame_data[ 0 ][ 'R' ] )
				self.logger.log( '        t: {}', frame_data[ 0 ][ 't' ] )
				self.logger.end_line()
				self.logger.flush()

			# Go trough objects in the frame
			# Frames iterator
			objects_url_search_pattern = frame.absolute_url + '/examples/*'
			objects_iterator = pydataset.dataset.Iterator( objects_url_search_pattern )
			objects_count    = len(objects_iterator)
			for object_index in range( objects_count ):

				obj = objects_iterator.at( object_index )
				category_name = obj.name
				if category_name in self.settings.aliases:
					category_name = self.settings.aliases[ category_name ]
				category_index = self.settings.categories[ category_name ]
				
				R = obj.get_feature( 'R' ).value.ravel().tolist()
				t = obj.get_feature( 't' ).value.ravel().tolist()
				
				frame_data.append({
					 'category_name': category_name,
					'category_index': category_index,
					             'R': R,
					             't': t,
					    'visibility': visibility,
					})

				# Log
				if self.logger:
					self.logger.log( '    {}:', category_name )
					self.logger.log( '        R: {}', frame_data[ 0 ][ 'R' ] )
					self.logger.log( '        t: {}', frame_data[ 0 ][ 't' ] )
					self.logger.end_line()
					self.logger.flush()

			# for object_index in range( objects_count )

			json.dump( frame_data, gt_file,
				      skipkeys = False,
				  ensure_ascii = True,
				check_circular = False,
				     allow_nan = False,
				           cls = None,
				        indent = None,
				    separators = (',',':'),
				       default = None,
				     sort_keys = False
				)
			if ( frame_index < frame_count-1 ):
				gt_file.write( ',\n\t' )
			else:
				gt_file.write( '\n' )
			gt_file.flush()

			# Update progression
			if self.progress_tracker:
				self.progress_tracker.update( float(frame_index)/float(frame_count) )

		# for frame_index in range( frame_count )

		gt_file.write( ']' )
		gt_file.flush()
		gt_file.close()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )

	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this exporter.

		Arguments:
			self (`pynema.RawSubSceneExporter`): The exporter to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporter )
		
		super( RawSubSceneExporter, self ).start()
				
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a raw sub-scene exporter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the raw sub-scene exporter settings constructor.

		Returns:
			`pynema.RawSubSceneExporterSettings`: The settings.
		"""
		return RawSubSceneExporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RawSubSceneExporter ( pytools.tasks.Task )