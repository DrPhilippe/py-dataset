# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# LOCALS
from .raw_dataset_exporter_settings import RawDatasetExporterSettings
from .raw_scene_exporter            import RawSceneExporter

# ##################################################
# ###         CLASS RAW-DATASET-EXPORTER         ###
# ##################################################

class RawDatasetExporter ( pytools.tasks.Task ):
	"""
	Class used to export the raw NEMA dataset to an exchange format.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, **kwargs ):
		"""
		Initializes a new instance of the `pynema.RawDatasetExporter` class.

		Arguments:
			settings (`pynema.RawDatasetExporter`): Dataset exporter settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporter )
		assert pytools.assertions.type_is_instance_of( settings, RawDatasetExporterSettings )

		super( RawDatasetExporter, self ).__init__( settings, **kwargs )
		
	# def __init__ ( self, settings, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this raw dataset exporter.

		Arguments:
			self (`pynema.RawDatasetExporter`): The raw dataset importer to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporter )

		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Get the dataset
		dataset = pydataset.dataset.get( self.settings.dataset_url )

		# Log
		if self.logger:
			self.logger.log( 'DATASET:' )
			self.logger.log( '    absolute_url: {}', dataset.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Categories in the dataset
		categories = {}

		# Create output directory if needed
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()

		# Log
		if self.logger:
			self.logger.log( 'OUTPUT DIRECTORY:' )
			self.logger.log( '    output_directory_path: {}', self.settings.output_directory_path )
			self.logger.end_line()
			self.logger.flush()

		# Fetch camera parameters
		K           = dataset.get_feature( 'camera_K' ).value
		dist_coeffs = dataset.get_feature( 'camera_dist_coeffs' ).value

		# Export camera settings
		camera_file = self.settings.output_directory_path + pytools.path.FilePath( 'camera.json' )
		camera_file.serialize_json({
			         'cx': float(K[2, 0]),
			         'cy': float(K[2, 1]),
			'depth_scale': 1.0,
			'dist_coeffs': [float(value) for value in dist_coeffs.tolist()],
			         'fx': float(K[0, 0]),
			         'fy': float(K[1, 1]),
			     'height': 720,
			      'width': 1280
			})

		# Log
		if self.logger:
			self.logger.log( 'CAMERA:' )
			self.logger.log( '    K:           {}', K.ravel().tolist() )
			self.logger.log( '    cx:          {}', float(K[2, 0]) )
			self.logger.log( '    cy:          {}', float(K[2, 1]) )
			self.logger.log( '    fx:          {}', float(K[0, 0]) )
			self.logger.log( '    fy:          {}', float(K[1, 1]) )
			self.logger.log( '    depth_scale: {}', 1.0 )
			self.logger.log( '    height:      {}', 720 )
			self.logger.log( '    width:       {}', 1080 )
			self.logger.log( '    dist_coeffs: {}', dist_coeffs.tolist() )
			self.logger.log( '    file_path:   {}', camera_file )
			self.logger.end_line()
			self.logger.flush()

		# Fetch charuco settings
		charuco = dataset.get_feature( 'charuco_board' ).value
		
		# Export charuco settings
		charuco_file = self.settings.output_directory_path + pytools.path.FilePath( 'charuco.json' )
		charuco_file.serialize_json({
			'dictionary_data': charuco.dictionary_data.value,
			    'board_width': charuco.board_width,
			   'board_height': charuco.board_height,
			  'square_length': charuco.square_length,
			  'marker_length': charuco.marker_length,
			  'border_length': charuco.border_length
			})

		# Log
		if self.logger:
			self.logger.log( 'CHARUCO:' )
			self.logger.log( '    dictionary_data: {}', charuco.dictionary_data.value )
			self.logger.log( '    board_width:     {}', charuco.board_width )
			self.logger.log( '    board_height:    {}', charuco.board_height )
			self.logger.log( '    square_length:   {}', charuco.square_length )
			self.logger.log( '    marker_length:   {}', charuco.marker_length )
			self.logger.log( '    border_length:   {}', charuco.border_length )
			self.logger.log( '    file_path:       {}', charuco_file )
			self.logger.end_line()
			self.logger.flush()

		# Create models directory
		models_dir = self.settings.output_directory_path + pytools.path.DirectoryPath( 'models' )
		if models_dir.is_not_a_directory():
			models_dir.create()

		# Path of the charuco 3D model file
		charuco_3d_file = models_dir + pytools.path.FilePath( '000000.ply' )

		# Export the charuco board 3D model
		charuco_mesh_data = pydataset.render.CharucoMeshData.from_charuco_data( charuco )
		charuco_mesh_data.save_ply( charuco_3d_file )

		# Remember the charuco as the first category (with index 0)
		categories[ 'charuco' ] = 0

		# Log
		if self.logger:
			self.logger.log( 'MODELS:' )
			self.logger.log( '    directory_path: {}', models_dir )
			self.logger.log( '    charuco:' )
			self.logger.log( '        index:     0' )
			self.logger.log( '        file_path: {}', charuco_3d_file )
			self.logger.flush()

		# Copy all the objects models provided in the settings
		count = 1
		for content_path in self.settings.models_directory_path.list_contents():

			# Is it a 3D model file ?
			if content_path.is_a_file() and content_path.extension() == '.ply':

				# Copy and rename the mesh file
				mesh_new_file_path = models_dir + pytools.path.FilePath.format( '{:06d}.ply', count )
				content_path.copy( mesh_new_file_path )

				# Keep track of that object category
				category_name  = content_path.filename().lower().replace( ' ', '_' )
				category_index = count
				
				# Remember that object category
				categories[ category_name ] = category_index
				
				# Log
				if self.logger:
					self.logger.log( '    {}:', category_name )
					self.logger.log( '        index:     {}', category_index )
					self.logger.log( '        file_path: {}', mesh_new_file_path )
					self.logger.flush()

				# Next
				count += 1

		# Export models informations
		camera_file = models_dir + pytools.path.FilePath( 'info.json' )
		camera_file.serialize_json(
			[ {'category_index': val, 'category_name': key} for key,val in categories.items() ]
			)

		# Directory where to export the scenes to
		scene_dir = self.settings.output_directory_path + pytools.path.DirectoryPath( 'scenes' )
		if scene_dir.is_not_a_directory():
			scene_dir.create()

		# Create an exporter for each sceen
		scene_url_search_pattern = self.settings.dataset_url + '/groups/scenes/groups/scene_1'
		scene_iterator           = pydataset.dataset.Iterator( scene_url_search_pattern )
		scene_exporters          = []
		count                    = 0
		
		# Log
		if self.logger:
			self.logger.end_line()
			self.logger.log( 'SCENES:' )
			self.logger.log( '    scene_url_search_pattern: {}', scene_url_search_pattern )
			self.logger.log( '    count:                    {}', scene_iterator.count )

		for scene in scene_iterator:
				
			scene_output_directory_path = scene_dir + pytools.path.DirectoryPath.format( '{:06d}', count )

			# Log
			if self.logger:
				self.logger.log( '    scene {:06d}:', count )
				self.logger.log( '        url: {}', scene.absolute_url )
				self.logger.log( '        dir: {}', scene_output_directory_path )

			scene_exporter = RawSceneExporter.create(
				               logger = pytools.tasks.file_logger( 'logs/nema_scene_{:06d}_exporter.log'.format(count) ),
				     progress_tracker = self.progress_tracker.create_child() if self.progress_tracker else None,
				                index = count,
				            scene_url = scene.absolute_url,
				           categories = categories,
				              aliases = {'bearing_1': 'bottom_bearing'},
				output_directory_path = scene_output_directory_path
				)
			scene_exporters.append( scene_exporter )
			count += 1

		self.manage( scene_exporters )

		if self.progress_tracker:
			self.progress_tracker.update( 1. )

	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this raw dataset exporter.

		Arguments:
			self (`pynema.RawDatasetExporter`): The exporter to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporter )
		
		super( RawDatasetExporter, self ).start()
		
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a raw dataset importer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the raw dataset settings constructor.

		Returns:
			`pynema.RawDatasetExporterSettings`: The settings.
		"""
		return RawDatasetExporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RawDatasetExporter ( pytools.tasks.Task )