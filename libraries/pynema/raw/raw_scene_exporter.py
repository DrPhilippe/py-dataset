# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .raw_scene_exporter_settings import RawSceneExporterSettings
from .raw_sub_scene_exporter      import RawSubSceneExporter

# ##################################################
# ###          CLASS RAW-SCENE-EXPORTER          ###
# ##################################################

class RawSceneExporter ( pytools.tasks.Task ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, **kwargs ):
		"""
		Initializes a new instance of the `pynema.RawSceneExporter` class.

		Arguments:
			self             (`pynema.RawSceneExporter`): Raw scene exporter instance to initialize.
			settings (`pynema.RawSceneExporterSettings`): Raw scene exporter settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporter )
		assert pytools.assertions.type_is_instance_of( settings, RawSceneExporterSettings )

		super( RawSceneExporter, self ).__init__( settings, **kwargs )
		
	# def __init__ ( self, settings, **kwargs )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this exporter.

		Arguments:
			self (`pynema.RawSceneExporter`): The importer to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporter )

		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Get the scene
		scene = pydataset.dataset.get( self.settings.scene_url )
		
		# Log
		if self.logger:
			self.logger.log( 'SCENE:' )
			self.logger.log( '    absolute_url: {}', scene.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Create output directory if needed
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()
		
		# Log
		if self.logger:
			self.logger.log( 'OUTPUT DIRECTORY:' )
			self.logger.log( '    output_directory_path: {}', self.settings.output_directory_path )
			self.logger.end_line()
			self.logger.flush()

		# Get scene data
		scene_data = scene.get_feature( 'scene_data' ).value.get_child( 'OpenCV' ).get_child( 'ChArUcO Board' )
		data       = []

		# Add the charuco as object
		data.append({
				'local_transform': [float(v) for v in numpy.eye(4).ravel().tolist()],
				  'category_name': 'charuco',
				 'category_index': 0
				})

		# Log
		if self.logger:
			self.logger.log( 'SCENE DATA:' )
			self.logger.log( '    charuco:' )
			self.logger.log( '        local_transform: {}', [float(v) for v in numpy.eye(4).ravel().tolist()] )
			self.logger.log( '         category_index: 0' )
			self.logger.flush()


		# Add each object on the board
		for child_data in scene_data.children_data:
			
			local_transform = child_data.local_transform
			category_name   = child_data.name.lower().replace(' ', '_')
			if category_name in self.settings.aliases:
				category_name = self.settings.aliases[ category_name ]
			category_index  = self.settings.categories[ category_name ]
			
			# Add the object
			data.append({
				'local_transform': [float(v) for v in local_transform],
				  'category_name': category_name,
				 'category_index': category_index
				})

			# Log
			if self.logger:
				self.logger.log( '    {}:', category_name )
				self.logger.log( '        local_transform: {}', [float(v) for v in local_transform] )
				self.logger.log( '         category_index: {}', category_index )
				self.logger.flush()

		# Sort scene data
		data = sorted(data, key=lambda item: item['category_index'] ) 

		# Save the scene data
		scene_data_file_path = self.settings.output_directory_path + pytools.path.FilePath( 'scene_data.json' )
		scene_data_file_path.serialize_json( data )
		
		# Log
		if self.logger:
			self.logger.log( '    scene data file: {}', scene_data_file_path )
			self.logger.end_line()
			self.logger.flush()

		# Directory where to export the scenes to
		sub_scene_dir = self.settings.output_directory_path + pytools.path.DirectoryPath( 'subscenes' )
		if sub_scene_dir.is_not_a_directory():
			sub_scene_dir.create()

		# Create an exporter for each sceen
		sub_scene_url_search_pattern = self.settings.scene_url + '/groups/*'
		sub_scene_iterator           = pydataset.dataset.Iterator( sub_scene_url_search_pattern )
		sub_scene_exporters          = []
		count                        = len(sub_scene_iterator)
		
		# Log
		if self.logger:
			self.logger.end_line()
			self.logger.log( 'SUB-SCENES:' )
			self.logger.log( '    sub_scene_url_search_pattern: {}', sub_scene_url_search_pattern )
			self.logger.log( '    count:                        {}', sub_scene_iterator.count )

		sub_scene_exporters = []
		for index in range( count ):
			
			sub_scene = sub_scene_iterator.at( index )

			# Directory where to save the sub scene
			sub_scene_output_directory_path = sub_scene_dir + pytools.path.DirectoryPath.format( '{:06d}', index )

			# Log
			if self.logger:
				self.logger.log( '    sub-scene {:06d}:', count )
				self.logger.log( '        url: {}', sub_scene.absolute_url )
				self.logger.log( '        dir: {}', sub_scene_output_directory_path )

			# Create the sbu scene exporter
			sub_scene_exporter = RawSubSceneExporter.create(
				               logger = pytools.tasks.file_logger( 'logs/nema_scene_{:06d}_subscene_{:06d}_exporter.log'.format(self.settings.index, index) ),
				     progress_tracker = self.progress_tracker.create_child() if self.progress_tracker else None,
				        sub_scene_url = sub_scene.absolute_url,
				           categories = self.settings.categories,
				              aliases = self.settings.aliases,
				output_directory_path = sub_scene_output_directory_path
				)
			sub_scene_exporters.append( sub_scene_exporter )

		# Run scene exporters
		for sub_scene_exporter in sub_scene_exporters:
			sub_scene_exporter.execute()
		# self.manage( sub_scene_exporters )

		if self.progress_tracker:
			self.progress_tracker.update( 1. )

	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this raw scene exporter.

		Arguments:
			self (`pynema.RawSceneExporter`): The raw scene exporter to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporter )
		
		super( RawSceneExporter, self ).start()
				
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a raw scene importer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the raw scene exporter settings constructor.

		Returns:
			`pynema.RawSceneExporterSettings`: The settings.
		"""
		return RawSceneExporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RawSceneExporter ( pytools.tasks.Task )