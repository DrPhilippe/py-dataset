# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###     CLASS RAW-SCENE-EXPORTER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RawSceneExporterSettings',
	   namespace = 'pynema.raw',
	fields_names = [
		'index',
		'scene_url',
		'categories',
		'output_directory_path'
		]
	)
class RawSceneExporterSettings ( pytools.tasks.TaskSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, index=0, scene_url='', categories={}, aliases={}, output_directory_path='', name='nema-scene-exporter' ):
		"""
		Initializes a new instance of the `pynema.RawSceneExporterSettings` class.

		Arguments:
			self                   (`pynema.RawSceneExporterSettings`): Instance to initialize.
			index                                              (`int`): Exporter index.
			scene_url                                          (`str`): URL of the nema scene to export.
			categories                      (`dict` of `str` to `int`): Category indexes by category names.
			output_directory_path (`pytools.path.DirectoryPath`/`str`): Path of the directory where to export the scene.
			name                                               (`str`): Name of the scene exporter task.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( index, int )
		assert pytools.assertions.type_is( scene_url, str )
		assert pytools.assertions.type_is( categories, dict )
		assert pytools.assertions.dictionary_types_are( categories, str, int )
		assert pytools.assertions.type_is( aliases, dict )
		assert pytools.assertions.dictionary_types_are( aliases, str, str )
		assert pytools.assertions.type_is_instance_of( output_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is( name, str )

		super( RawSceneExporterSettings, self ).__init__( name )
		
		self._index                 = index
		self._scene_url             = scene_url
		self._categories            = copy.deepcopy( categories )
		self._aliases               = copy.deepcopy( aliases )
		self._output_directory_path = pytools.path.DirectoryPath.ensure( output_directory_path )

	# def __init__ ( self, dataset_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def index ( self ):
		"""
		Exporter index (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )

		return self._index

	# def index ( self )

	# --------------------------------------------------

	@index.setter
	def index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( value, int )
		
		self._index = value

	# def index ( self, value )

	# --------------------------------------------------

	@property
	def scene_url ( self ):
		"""
		URL of the nema scene to export (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )

		return self._scene_url

	# def scene_url ( self )

	# --------------------------------------------------

	@scene_url.setter
	def scene_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._scene_url = value

	# def scene_url ( self, value )

	# --------------------------------------------------

	@property
	def categories ( self ):
		"""
		Category indexes by category names (`dict` of `str` to `int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )

		return self._categories

	# def categories ( self )

	# --------------------------------------------------

	@categories.setter
	def categories ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, int )
		
		self._categories = copy.deepcopy( value )

	# def categories ( self, value )

	# --------------------------------------------------

	@property
	def aliases ( self ):
		"""
		Category aliases (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )

		return self._aliases

	# def aliases ( self )

	# --------------------------------------------------

	@aliases.setter
	def aliases ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )
		
		self._aliases = copy.deepcopy( value )

	# def aliases ( self, value )

	# --------------------------------------------------

	@property
	def output_directory_path ( self ):
		"""
		Path of the directory where to export the dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )

		return self._output_directory_path

	# def output_directory_path ( self )

	# --------------------------------------------------

	@output_directory_path.setter
	def output_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSceneExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._output_directory_path = value

	# def output_directory_path ( self, value )

# class RawSceneExporterSettings ( pytools.tasks.TaskSettings )