# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###   CLASS RAW-SUB-SCENE-EXPORTER-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RawSubSceneExporterSettings',
	   namespace = 'pynema.raw',
	fields_names = [
		'sub_scene_url',
		'categories',
		'aliases',
		'output_directory_path'
		]
	)
class RawSubSceneExporterSettings ( pytools.tasks.TaskSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, sub_scene_url='', categories={}, aliases={}, output_directory_path='', name='nema-sub-scene-exporter' ):
		"""
		Initializes a new instance of the `pynema.RawSubSceneExporterSettings` class.

		Arguments:
			self                (`pynema.RawSubSceneExporterSettings`): Instance to initialize.
			sub_scene_url                                      (`str`): URL of the nema scene to export.
			categories                      (`dict` of `str` to `int`): Category indexes by category names.
			aliases                         (`dict` of `str` to `str`): Category aliases.
			output_directory_path (`pytools.path.DirectoryPath`/`str`): Path of the directory where to export the scene.
			name                                               (`str`): Name of the scene exporter task.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )
		assert pytools.assertions.type_is( sub_scene_url, str )
		assert pytools.assertions.type_is( categories, dict )
		assert pytools.assertions.dictionary_types_are( categories, str, int )
		assert pytools.assertions.type_is( aliases, dict )
		assert pytools.assertions.dictionary_types_are( aliases, str, str )
		assert pytools.assertions.type_is_instance_of( output_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is( name, str )

		super( RawSubSceneExporterSettings, self ).__init__( name )
		
		self._sub_scene_url         = sub_scene_url
		self._categories            = copy.deepcopy( categories )
		self._aliases               = copy.deepcopy( aliases )
		self._output_directory_path = pytools.path.DirectoryPath.ensure( output_directory_path )

	# def __init__ ( self, dataset_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def sub_scene_url ( self ):
		"""
		URL of the nema scene to export.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )

		return self._sub_scene_url

	# def sub_scene_url ( self )

	# --------------------------------------------------

	@sub_scene_url.setter
	def sub_scene_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._scene_url = value

	# def sub_scene_url ( self, value )

	# --------------------------------------------------

	@property
	def categories ( self ):
		"""
		Category indexes by category names (`dict` of `str` to `int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )

		return self._categories

	# def categories ( self )

	# --------------------------------------------------

	@categories.setter
	def categories ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, int )
		
		self._categories = copy.deepcopy( value )

	# def categories ( self, value )

	# --------------------------------------------------

	@property
	def aliases ( self ):
		"""
		Category aliases (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )

		return self._aliases

	# def aliases ( self )

	# --------------------------------------------------

	@aliases.setter
	def aliases ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, str )
		
		self._aliases = copy.deepcopy( value )

	# def aliases ( self, value )

	# --------------------------------------------------

	@property
	def output_directory_path ( self ):
		"""
		Path of the directory where to export the dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )

		return self._output_directory_path

	# def output_directory_path ( self )

	# --------------------------------------------------

	@output_directory_path.setter
	def output_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawSubSceneExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._output_directory_path = value

	# def output_directory_path ( self, value )

# class RawSubSceneExporterSettings ( pytools.tasks.TaskSettings )