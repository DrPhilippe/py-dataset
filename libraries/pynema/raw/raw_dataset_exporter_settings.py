# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###    CLASS RAW-DATASET-EXPORTER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RawDatasetExporterSettings',
	   namespace = 'pynema.raw',
	fields_names = [
		'dataset_url',
		'models_directory_path',
		'output_directory_path'
		]
	)
class RawDatasetExporterSettings ( pytools.tasks.TaskSettings ):
	"""
	Settings used to configure the exportation of a raw NEMA dataset.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, dataset_url='', models_directory_path='', output_directory_path='', name='nema-dataset-exporter' ):
		"""
		Initializes a new instance of the `pynema.RawDatasetExporterSettings` class.

		Arguments:
			self                 (`pynema.RawDatasetExporterSettings`): Instance to initialize.
			dataset_url                                        (`str`): URL of the nema dataset to export.
			models_directory_path (`pytools.path.DirectoryPath`/`str`): Path of the directory containing the objects models.
			output_directory_path (`pytools.path.DirectoryPath`/`str`): Path of the directory where to export the dataset.
			name                                               (`str`): Name of the exporter task.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )
		assert pytools.assertions.type_is( dataset_url, str )
		assert pytools.assertions.type_is_instance_of( models_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is_instance_of( output_directory_path, (pytools.path.DirectoryPath, str) )
		assert pytools.assertions.type_is( name, str )

		super( RawDatasetExporterSettings, self ).__init__( name )
		
		self._dataset_url           = dataset_url
		self._models_directory_path = pytools.path.DirectoryPath.ensure( models_directory_path )
		self._output_directory_path = pytools.path.DirectoryPath.ensure( output_directory_path )

	# def __init__ ( self, dataset_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dataset_url ( self ):
		"""
		URL of the nema dataset to export.
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )

		return self._dataset_url

	# def dataset_url ( self )

	# --------------------------------------------------

	@dataset_url.setter
	def dataset_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._dataset_url = value

	# def dataset_url ( self, value )

	# --------------------------------------------------

	@property
	def models_directory_path ( self ):
		"""
		Path of the directory containing the objects models (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )

		return self._models_directory_path

	# def models_directory_path ( self )

	# --------------------------------------------------

	@models_directory_path.setter
	def models_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._models_directory_path = value

	# def models_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def output_directory_path ( self ):
		"""
		Path of the directory where to export the dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )

		return self._output_directory_path

	# def output_directory_path ( self )

	# --------------------------------------------------

	@output_directory_path.setter
	def output_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RawDatasetExporterSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._output_directory_path = value

	# def output_directory_path ( self, value )

# class RawDatasetExporterSettings ( pytools.tasks.TaskSettings )