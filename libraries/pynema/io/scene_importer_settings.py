# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###       CLASS SCENE-IMPORTER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SceneImporterSettings',
	   namespace = 'pynema.io',
	fields_names = [
		# SRC
		'scene_directory_path',
		'scene_index'
		]
	)
class SceneImporterSettings ( pydataset.io.ImporterSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, scene_directory_path='', scene_index=0, dst_group_url='', name='dataset_importer' ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( scene_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( scene_index, int )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( name, str )

		super( SceneImporterSettings, self ).__init__( dst_group_url, name )

		self._scene_directory_path = pytools.path.DirectoryPath.ensure( scene_directory_path )
		self._scene_index = scene_index

	# def __init__ ( self, scene_directory_path, dst_group_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def scene_directory_path ( self ):
		"""
		Path to the directory containing the dataset to import (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )

		return self._scene_directory_path

	# def scene_directory_path ( self )
	
	# --------------------------------------------------

	@scene_directory_path.setter
	def scene_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._scene_directory_path = pytools.path.DirectoryPath.ensure( value )
		
	# def scene_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def scene_index ( self ):
		"""
		Index of the scene used for logging and debugging (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )

		return self._scene_index

	# def scene_index ( self )
	
	# --------------------------------------------------

	@scene_index.setter
	def scene_index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporterSettings )
		assert pytools.assertions.type_is( value, int )

		self._scene_index = value
		
	# def scene_index ( self, value )

# class SceneImporterSettings ( pydataset.io.ImporterSettings )