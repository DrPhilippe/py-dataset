# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .dataset_importer_settings import DatasetImporterSettings
from .scene_importer            import SceneImporter

# ##################################################
# ###           CLASS DATASET-IMPORTER           ###
# ##################################################

class DatasetImporter ( pydataset.io.Importer ):
	"""
	Impors NEMA datasets.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new isntance of the `pybop.DatasetImporter` class.

		Arguments:
			self                            (`pybop.DatasetImporter`): Importer to initialize.
			settings                (`pybop.DatasetImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporter )
		assert pytools.assertions.type_is_instance_of( settings, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( DatasetImporter, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run importation.

		Arguments:
			self (`pybop.DatasetImporter`): Importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Log settings
		if self.logger:
			self.logger.log( 'NEMA DATASET IMPORTER' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			for field_name in DatasetImporterSettings.__fields_names__:
				self.logger.log( "    {:20s}: '{}'", field_name, getattr(self.settings, field_name) )
			self.logger.end_line()
			self.logger.flush()
	
		# Import		
		self.import_camera()
		self.import_charuco()
		self.import_models()
		self.import_scenes()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )
	
	# def run ( self ):

	# --------------------------------------------------

	def import_camera ( self ):

		# Files and directories
		camera_file_path = self.settings.dataset_directory_path + pytools.path.FilePath( 'camera.json' )

		# Load camera
		camera = camera_file_path.deserialize_json()

		# Create K
		K = numpy.eye( 3, dtype=numpy.float32 )
		K[ 0, 0 ] = camera[ 'fx'  ]
		K[ 1, 1 ] = camera[ 'fy'  ]
		K[ 0, 2 ] = camera[ 'ppx' ]
		K[ 1, 2 ] = camera[ 'ppy' ]

		# Create distortion coefficients
		dist_coeffs = numpy.asarray( camera[ 'dist_coeffs' ], dtype=numpy.float32 )

		# Save features
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.TextFeatureData,
			feature_name      = 'camera_name',
			value             = camera[ 'name' ]
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.TextFeatureData,
			feature_name      = 'camera_line',
			value             = camera[ 'product_line' ]
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			feature_name      = 'K',
			value             = K
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			feature_name      = 'dist_coeffs',
			value             = dist_coeffs
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.IntFeatureData,
			feature_name      = 'width',
			value             = camera[ 'width' ]
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.IntFeatureData,
			feature_name      = 'height',
			value             = camera[ 'height' ]
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.FloatFeatureData,
			feature_name      = 'depth_scale',
			value             = camera[ 'depth_scale' ] * 1000.
			)

	# def import_camera ( self )

	# --------------------------------------------------

	def import_charuco ( self ):

		# Files and directories
		charuco_file_path = self.settings.dataset_directory_path + pytools.path.FilePath( 'charuco.json' )
		
		# Load charuco
		charuco = charuco_file_path.deserialize_json()

		# Pase charuco
		data = pydataset.data.CharucoBoardData(
			dictionary_data = pydataset.data.CharucoDictionaryData( str(charuco[ 'dictionary_data' ]) ),
			board_width     =   int( charuco[ 'board_width'   ] ),
			board_height    =   int( charuco[ 'board_height'  ] ),
			square_length   = float( charuco[ 'square_length' ] ),
			marker_length   = float( charuco[ 'marker_length' ] ),
			border_length   = float( charuco[ 'border_length' ] )
			)

		# Create feature
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			feature_name      = 'charuco',
			value             = data
			)

	# def import_charuco ( self )

	# --------------------------------------------------

	def import_models ( self ):

		# Files and directories
		models_directory_path = self.settings.dataset_directory_path + pytools.path.DirectoryPath( 'models/ply' )
		models_info_file_path = self.settings.dataset_directory_path + pytools.path.DirectoryPath( 'models' ) + pytools.path.FilePath( 'info.json' )

		# Load information
		models_infos     = models_info_file_path.deserialize_json()
		number_of_models = len( models_infos )

		# Create a group where to save models
		models_group = self.dst_group.get_or_create_group( 'models' )

		# Load each models
		for model_index in range( number_of_models ):

			# Get model infos
			category_index = models_infos[ model_index ][ 'category_index' ]
			category_name  = models_infos[ model_index ][ 'category_name'  ]

			# Mesh file path
			mesh_file_path = models_directory_path + pytools.path.FilePath.format( '{:06d}.ply', category_index )

			# Load mesh
			if category_name == 'charuco':
				mesh_data = pydataset.render.CharucoMeshData.load_ply( mesh_file_path )
			else:
				mesh_data = pydataset.render.MeshData.load_ply( mesh_file_path )
			mesh_data.name = category_name
			
			# Create an example for that model
			model_example = models_group.get_or_create_example( '{:06d}'.format(category_index) )

			# Create features
			model_example.create_or_update_feature(
				feature_data_type = pydataset.dataset.IntFeatureData,
				feature_name      = 'category_index',
				value             = category_index
				)
			model_example.create_or_update_feature(
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				feature_name      = 'one_hot_category',
				value             = pydataset.classification_utils.index_to_one_hot( category_index, number_of_models )
				)
			model_example.create_or_update_feature(
				feature_data_type = pydataset.dataset.TextFeatureData,
				feature_name      = 'category_name',
				value             = category_name
				)
			model_example.create_or_update_feature(
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				feature_name      = 'mesh',
				value             = mesh_data
				)
		
		# for model_index in range( number_of_models )

	# def import_models ( self )

	# --------------------------------------------------

	def import_scenes ( self ):

		# Files and directories
		scenes_directory_path = self.settings.dataset_directory_path + pytools.path.DirectoryPath( 'scenes' )
		scenes_directories    = scenes_directory_path.list_contents()
		number_of_scenes      = len( scenes_directories )

		# Scene dst group
		scene_group = self.dst_group.get_or_create_group( 'scenes' )

		# Create scnenes importers
		scenes_importers = []
		for scene_index in range( number_of_scenes ):
			
			# fetch the scene directory path
			scene_directory_path = scenes_directories[ scene_index ]

			# Create an importer for it
			scene_importer = SceneImporter.create(
				              logger = pytools.tasks.file_logger( 'logs/nema_scene_{:06d}_importer.log'.format(scene_index) ),
				    progress_tracker = self.progress_tracker.create_child() if self.progress_tracker else None,
				scene_directory_path = scene_directory_path,
				         scene_index = scene_index,
				       dst_group_url = scene_group.absolute_url + '/groups/{:06d}'.format( scene_index ),
				                name = 'nema_scene_{:06d}_importer'.format( scene_index )
				)
			scenes_importers.append( scene_importer )
		
		# for scene_index in range( number_of_scenes )

		# Run them sequencially
		for scene_importer in scenes_importers:
			scene_importer.execute()

	# def import_scenes ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pybop.DatasetImporterSettings` class.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pybop.DatasetImporterSettings` constructor.

		Returns:
			`pybop.DatasetImporterSettings`: The settings.
		"""
		return DatasetImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class DatasetImporter ( pydataset.io.Importer )