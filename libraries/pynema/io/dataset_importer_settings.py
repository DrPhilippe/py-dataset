# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###      CLASS DATASET-IMPORTER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetImporterSettings',
	   namespace = 'pynema.io',
	fields_names = [
		# SRC
		'dataset_directory_path'
		]
	)
class DatasetImporterSettings ( pydataset.io.ImporterSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dataset_directory_path='', dst_group_url='', name='dataset_importer' ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( dataset_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( name, str )

		super( DatasetImporterSettings, self ).__init__( dst_group_url, name )

		self._dataset_directory_path = pytools.path.DirectoryPath.ensure( dataset_directory_path )

	# def __init__ ( self, dataset_directory_path, dst_group_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def dataset_directory_path ( self ):
		"""
		Path to the directory containing the dataset to import (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )

		return self._dataset_directory_path

	# def dataset_directory_path ( self )
	
	# --------------------------------------------------

	@dataset_directory_path.setter
	def dataset_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._dataset_directory_path = pytools.path.DirectoryPath.ensure( value )
		
	# def dataset_directory_path ( self, value )

# class DatasetImporterSettings ( pydataset.io.ImporterSettings )