# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###     CLASS SUB-SCENE-IMPORTER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SubSceneImporterSettings',
	   namespace = 'pynema.io',
	fields_names = [
		'sub_scene_directory_path'
		]
	)
class SubSceneImporterSettings ( pydataset.io.ImporterSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, sub_scene_directory_path='', dst_group_url='', name='dataset_importer' ):
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( sub_scene_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( name, str )

		super( SubSceneImporterSettings, self ).__init__( dst_group_url, name )

		self._sub_scene_directory_path = pytools.path.DirectoryPath.ensure( sub_scene_directory_path )

	# def __init__ ( self, sub_scene_directory_path, dst_group_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def sub_scene_directory_path ( self ):
		"""
		Path to the directory containing the dataset to import (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporterSettings )

		return self._sub_scene_directory_path

	# def sub_scene_directory_path ( self )
	
	# --------------------------------------------------

	@sub_scene_directory_path.setter
	def sub_scene_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._sub_scene_directory_path = pytools.path.DirectoryPath.ensure( value )
		
	# def sub_scene_directory_path ( self, value )

# class SubSceneImporterSettings ( pydataset.io.ImporterSettings )