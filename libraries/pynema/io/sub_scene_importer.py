# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .sub_scene_importer_settings import SubSceneImporterSettings

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

REFERENCE_URL_FORMAT = 'groups/models/examples/{:06d}/features/{}'

# ##################################################
# ###          CLASS SUB-SCENE-IMPORTER          ###
# ##################################################

class SubSceneImporter ( pydataset.io.Importer ):
	"""
	Impors NEMA sub scenes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new isntance of the `pybop.io.SubSceneImporter` class.

		Arguments:
			self                        (`pybop.io.SubSceneImporter`): Importer to initialize.
			settings            (`pybop.io.SubSceneImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporter )
		assert pytools.assertions.type_is_instance_of( settings, SubSceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( SubSceneImporter, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

		self._gt = []

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run importation.

		Arguments:
			self (`pybop.io.SubSceneImporter`): Importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Log settings
		if self.logger:
			self.logger.log( 'NEMA SUB SCENE IMPORTER' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			for field_name in SubSceneImporterSettings.__fields_names__:
				self.logger.log( "    {:20s}: '{}'", field_name, getattr(self.settings, field_name) )
			self.logger.end_line()
			self.logger.flush()

		self.import_background()
		self.import_ground_truth()
		self.import_frames()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )
	
	# def run ( self )

	# --------------------------------------------------

	def import_background ( self ):
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporter )

		# Files and directories
		background_directory_path  = self.settings.sub_scene_directory_path + pytools.path.DirectoryPath( 'background' )
		background_color_file_path = background_directory_path + pytools.path.FilePath( 'color.png' )
		background_depth_file_path = background_directory_path + pytools.path.FilePath( 'depth.png' )

		# Load
		background_color = cv2.imread( str(background_color_file_path), cv2.IMREAD_COLOR    ).astype( numpy.uint8  )
		background_depth = cv2.imread( str(background_depth_file_path), cv2.IMREAD_ANYDEPTH ).astype( numpy.uint16 )

		# Create features
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.ImageFeatureData,
			feature_name      = 'background_color',
			value             =  background_color,
			extension         = '.png',
			param             = 9
			)
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.ImageFeatureData,
			feature_name      = 'background_depth',
			value             =  background_depth,
			extension         = '.png',
			param             = 9
			)

	# def import_background ( self )

	# --------------------------------------------------

	def import_ground_truth ( self ):
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporter )

		# Files and directories
		gt_file_path = self.settings.sub_scene_directory_path + pytools.path.FilePath( 'gt.json' )

		# Load ground truth
		self._gt = gt_file_path.deserialize_json()

	# def import_ground_truth ( self )

	# --------------------------------------------------

	def import_frames ( self ):
		assert pytools.assertions.type_is_instance_of( self, SubSceneImporter )

		# Files and directories
		frames_directory_path = self.settings.sub_scene_directory_path + pytools.path.DirectoryPath( 'frames' )

		# Go through frames
		number_of_frames = len( self._gt )
		for frame_index in range( number_of_frames ):

			# Create a group for the frame
			frame_group = self.dst_group.get_or_create_group( '{:06d}'.format(frame_index) )
		
			# Files and directories
			foreground_color_file_path = frames_directory_path + pytools.path.FilePath.format( '{:06d}_color.png', frame_index )
			foreground_depth_file_path = frames_directory_path + pytools.path.FilePath.format( '{:06d}_depth.png', frame_index )

			# Load images
			foreground_color = cv2.imread( str(foreground_color_file_path), cv2.IMREAD_COLOR    ).astype( numpy.uint8  )
			foreground_depth = cv2.imread( str(foreground_depth_file_path), cv2.IMREAD_ANYDEPTH ).astype( numpy.uint16 )

			# Create features
			frame_group.create_or_update_feature(
				feature_data_type = pydataset.dataset.ImageFeatureData,
				feature_name      = 'foreground_color',
				value             =  foreground_color,
				extension         = '.png',
				param             = 9
				)
			frame_group.create_or_update_feature(
				feature_data_type = pydataset.dataset.ImageFeatureData,
				feature_name      = 'foreground_depth',
				value             =  foreground_depth,
				extension         = '.png',
				param             = 9
				)

			# Fetch ground truth
			frame_gt            = self._gt[ frame_index ]
			number_of_instances = len( frame_gt )

			# # Fetch charuco ground truth
			# charuco_gt = frame_gt[ 0 ]
			
			# # parse category
			# category_index = charuco_gt[ 'category_index' ]

			# # parse R
			# R = numpy.asarray( charuco_gt['R'], dtype=numpy.float32 )
			# R = numpy.reshape( R, [3, 3] )
			
			# # parse t
			# t = numpy.asarray( charuco_gt['t'], dtype=numpy.float32 )
			# t = numpy.reshape( t, [3] )

			# # Create features references
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.FeatureRefData,
			# 	feature_name      = 'category_index',
			# 	value             = REFERENCE_URL_FORMAT.format( category_index, 'category_index' )
			# 	)
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.FeatureRefData,
			# 	feature_name      = 'one_hot_category',
			# 	value             = REFERENCE_URL_FORMAT.format( category_index, 'one_hot_category' )
			# 	)
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.FeatureRefData,
			# 	feature_name      = 'category_name',
			# 	value             = REFERENCE_URL_FORMAT.format( category_index, 'category_name' )
			# 	)
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.FeatureRefData,
			# 	feature_name      = 'mesh',
			# 	value             = REFERENCE_URL_FORMAT.format( category_index, 'mesh' )
			# 	)

			# # Create features
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.NDArrayFeatureData,
			# 	feature_name      = 'R',
			# 	value             =  R
			# 	)
			# frame_group.create_or_update_feature(
			# 	feature_data_type = pydataset.dataset.NDArrayFeatureData,
			# 	feature_name      = 't',
			# 	value             =  t
			# 	)

			# Import each object instance
			for instance_index in range( 0, number_of_instances ):

				# Create an example for that object instance
				instance_example = frame_group.get_or_create_example( '{:06d}'.format(instance_index) )

				# Fetch ground trouth
				instance_gt = frame_gt[ instance_index ]
				
				# parse category
				category_index = instance_gt[ 'category_index' ]

				# parse R
				R = numpy.asarray( instance_gt['R'], dtype=numpy.float32 )
				R = numpy.reshape( R, [3, 3] )
				
				# parse t
				t = numpy.asarray( instance_gt['t'], dtype=numpy.float32 )
				t = numpy.reshape( t, [3] )

				# Create features references
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.FeatureRefData,
					feature_name      = 'category_index',
					value             = REFERENCE_URL_FORMAT.format( category_index, 'category_index' )
					)
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.FeatureRefData,
					feature_name      = 'one_hot_category',
					value             = REFERENCE_URL_FORMAT.format( category_index, 'one_hot_category' )
					)
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.FeatureRefData,
					feature_name      = 'category_name',
					value             = REFERENCE_URL_FORMAT.format( category_index, 'category_name' )
					)
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.FeatureRefData,
					feature_name      = 'mesh',
					value             = REFERENCE_URL_FORMAT.format( category_index, 'mesh' )
					)

				# Create features
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					feature_name      = 'R',
					value             =  R
					)
				instance_example.create_or_update_feature(
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					feature_name      = 't',
					value             =  t
					)

			# for instance_index in range( number_of_instances )

			# Update progression
			if self.progress_tracker:
				self.progress_tracker.update( float(frame_index)/float(number_of_frames) )

		# for frame_index in range( number_of_frames )

	# def import_frames ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pybop.io.SubSceneImporterSettings` class.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pybop.io.SubSceneImporterSettings` constructor.

		Returns:
			`pybop.io.SubSceneImporterSettings`: The settings.
		"""
		return SubSceneImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SubSceneImporter ( pydataset.io.Importer )