# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .scene_importer_settings import SceneImporterSettings
from .sub_scene_importer      import SubSceneImporter

# ##################################################
# ###            CLASS SCENE-IMPORTER            ###
# ##################################################

class SceneImporter ( pydataset.io.Importer ):
	"""
	Impors NEMA scenes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new isntance of the `pybop.SceneImporter` class.

		Arguments:
			self                           (`pybop.io.SceneImporter`): Importer to initialize.
			settings               (`pybop.io.SceneImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )
		assert pytools.assertions.type_is_instance_of( settings, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( SceneImporter, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run importation.

		Arguments:
			self (`pybop.io.SceneImporter`): Importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Log settings
		if self.logger:
			self.logger.log( 'NEMA SCENE IMPORTER' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			for field_name in SceneImporterSettings.__fields_names__:
				self.logger.log( "    {:20s}: '{}'", field_name, getattr(self.settings, field_name) )
			self.logger.end_line()
			self.logger.flush()
		
		self.import_scene_data()
		self.import_sub_scenes()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )
	
	# def run ( self ):

	# --------------------------------------------------

	def import_scene_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		# Files and directories
		scene_file_path = self.settings.scene_directory_path + pytools.path.FilePath( 'scene_data.json' )
		
		# Load scene data
		scene_instances     = scene_file_path.deserialize_json()
		number_of_instances = len( scene_instances )

		# Fetch models group
		models_group = self.dst_group.dataset.get_group( 'models' )

		# Create the opengl scene
		scene_data = pydataset.render.SceneData( name='scene' )
			
		# Create the opencv space
		T = numpy.eye( 4, dtype=numpy.float32 )
		T[1, 1] = -1
		T[2, 2] = -1
		cv_data = pydataset.render.EntityData( name='opencv', local_transform=T.ravel().tolist() )
		scene_data.add_child( cv_data )

		# Fetch the charuco instance
		scene_instance  = scene_instances[ 0 ]
		local_transform = scene_instance[ 'local_transform' ]
		category_index  = scene_instance[ 'category_index'  ]
		# parse local transform
		local_transform = numpy.asarray( local_transform, dtype=numpy.float32 )
		local_transform = numpy.reshape( local_transform, (4, 4) )
		# Fetch the charuco mesh
		charuco_data = models_group.get_example( '000000' ).get_feature( 'mesh' ).value
		# Create the charuco entity
		charuco_data.local_transform = local_transform.ravel().tolist()
		cv_data.add_child( charuco_data )

		# Populate charuco board
		for instance_index in range( 1, number_of_instances ):

			# Fetch the scene instance
			scene_instance  = scene_instances[ instance_index ]
			local_transform = scene_instance[ 'local_transform' ]
			category_index  = scene_instance[ 'category_index'  ]
			
			# parse local transform
			local_transform = numpy.asarray( local_transform, dtype=numpy.float32 )
			local_transform = numpy.reshape( local_transform, (4, 4) )

			# Fetch the mesh
			mesh_data = models_group.get_example( '{:06d}'.format(category_index) ).get_feature( 'mesh' ).value
			mesh_data.local_transform = local_transform.ravel().tolist()
			charuco_data.add_child( mesh_data )

		# for instance_index in range( 1, number_of_instances )

		# Create the scene data faeture
		self.dst_group.create_or_update_feature(
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			feature_name      = 'scene_data',
			value             =  scene_data
			)

	# def import_scene_data ( self )

	# --------------------------------------------------

	def import_sub_scenes ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		# Files and directories
		sub_scenes_directory   = self.settings.scene_directory_path + pytools.path.DirectoryPath( 'subscenes' )
		sub_scenes_directories = sub_scenes_directory.list_contents()
		number_of_sub_scenes   = len( sub_scenes_directories )

		# Create an importer for each subscene
		sub_scene_importers = []
		for sub_scene_index in range( number_of_sub_scenes ):

			# Fetch subscene directory
			sub_scenes_directory = sub_scenes_directories[ sub_scene_index ]

			# Name of the sub scene importer
			name = 'nema_scene_{:06d}_subscene_{:06d}_importer'.format( self.settings.scene_index, sub_scene_index )

			# Create the target group
			sub_scene_group  = self.dst_group.get_or_create_group( '{:06d}'.format(sub_scene_index) )

			# Create the sub scene importer
			sub_scene_importer = SubSceneImporter.create(
				                  logger = pytools.tasks.file_logger( 'logs/'+name+'.log' ),
				        progress_tracker = self.progress_tracker.create_child() if self.progress_tracker else None,
				sub_scene_directory_path = sub_scenes_directory,
				           dst_group_url = sub_scene_group.absolute_url,
				                    name = name
				)

			# Add it to the list of importers
			sub_scene_importers.append( sub_scene_importer )

		# for sub_scene_index in range( number_of_sub_scenes )

		self.manage( sub_scene_importers, max_threads=10 )

	# def import_sub_scenes ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pybop.io.SceneImporterSettings` class.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pybop.io.SceneImporterSettings` constructor.

		Returns:
			`pybop.io.SceneImporterSettings`: The settings.
		"""
		return SceneImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SceneImporter ( pydataset.io.Importer )