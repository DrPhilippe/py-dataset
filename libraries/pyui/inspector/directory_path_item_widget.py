# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
from .path_item_widget               import PathItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###      CLASS DIRECTORY-PATH-ITEM-WIDGET      ###
# ##################################################

@RegisterItemWidgetAttribute( 'DirectoryPath' )
class DirectoryPathItemWidget ( PathItemWidget ):
	"""
	The class `pyui.inspector.DirectoryPathItemWidget` is used to edit / display `pytools.path.DirectoryPath` in the inspector.
	"""
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=pytools.path.DirectoryPath(), is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.DirectoryPathItemWidget` class.

		Arguments:
			self (`pyui.inspector.DirectoryPathItemWidget`): Instance to initialize.
			value     (`None`/`pytools.path.DirectoryPath`): Directory path value edited / displayed in this item widget.
			is_editable                            (`bool`): If `True` value can be edited using the item widget.
			parent       (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPathItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Initialize the parent class
		super( DirectoryPathItemWidget, self ).__init__(
			      value = value,
			is_editable = is_editable,
			   filemode = PyQt5.QtWidgets.QFileDialog.Directory,
			     parent = parent
			)

	# def __init__ ( self, value, is_editable, parent )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the text is edited in the lineedit.

		Arguments:
			self (`pyui.inspector.DirectoryPathItemWidget`): Directory path item widget.
			text                                    (`str`): The new path as text.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPathItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the new path
		self._value = pytoots.path.DirectoryPath( text ) if text else pytools.path.DirectoryPath()

		# Notify listeners that value changed
		self.value_changed.emit()

	# def on_text_changed ( self, text )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_clicked ( self ):
		"""
		Slot invoked when the button to pick a directory is pressed.

		Arguments:
			self (`pyui.inspector.DirectoryPathItemWidget`): Directory path item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPathItemWidget )

		# create the directory dialog
		dialog = PyQt5.QtWidgets.QFileDialog(
			pyui.Application.instance().main_window,
			'Select a directory path',
			self.text_value
			)
		dialog.setFileMode( self.filemode )
		dialog.setViewMode( PyQt5.QtWidgets.QFileDialog.List )

		# Execute it
		if ( dialog.exec() ):

			# fetch the selection
			dialog_selection = dialog.selectedFiles()

			# Check selection
			if ( len(dialog_selection) > 0 ):

				# Update the path
				self.value = pytools.path.DirectoryPath( dialog_selection[0] )

				# Notify listeners that value changed
				self.value_changed.emit()

			# if ( len(dialog_selection) > 0 )

		# if ( dialog.exec() )

	# class on_button_clicked ( PathItemWidget )
	
# class DirectoryPathItemWidget ( PathItemWidget )
