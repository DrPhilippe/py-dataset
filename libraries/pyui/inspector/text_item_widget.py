# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###           CLASS TEXT-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'str' )
@RegisterItemWidgetAttribute( 'str_' )
@RegisterItemWidgetAttribute( 'unicode' )
@RegisterItemWidgetAttribute( 'unicode_' )
class TextItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.TextItemWidget` is used to edit / display text in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='', is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.TextItemWidget` class.

		Arguments:
			self    (`pyui.inspector.TextItemWidget`): Instance to initialize.
			value                      (`None`/`str`): Text value edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextItemWidget )
		assert pytools.assertions.type_in( value, (type(None), str, numpy.str, numpy.str_, numpy.unicode, numpy.unicode_) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( TextItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def lineedit ( self ):
		"""
		Line-Edit used to edit the text value of this item widget (`PyQt5.QtWidgets.QLineEdit`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TextItemWidget )

		return self._lineedit
	
	# def lineedit ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this text item widget.

		Arguments:
			self (`pyui.inspector.TextItemWidget`): Text item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextItemWidget )

		# Setup the parent class
		super( TextItemWidget, self ).setup()

		# Create the layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create the lineedit
		self._lineedit = PyQt5.QtWidgets.QLineEdit( self.text_value, self )
		self._lineedit.setSizePolicy( PyQt5.QtWidgets.QSizePolicy.Expanding, PyQt5.QtWidgets.QSizePolicy.Preferred )
		self._lineedit.setMinimumSize( TextItemWidget.height, TextItemWidget.height )
		self._lineedit.setMaximumSize( self._lineedit.maximumWidth(), TextItemWidget.height )
		self._lineedit.textChanged.connect( self.on_text_changed )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._lineedit )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this text item widget.

		Arguments:
			self (`pyui.inspector.TextItemWidget`): Text item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextItemWidget )

		# Update the lineedit
		self._lineedit.setText( self.text_value )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )

		# Update the parent class
		super( TextItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the value of the text item widget is edited.

		Arguments:
			self (`pyui.inspector.TextItemWidget`): Text item widget.
			text                           (`str`): The new text value.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the value
		self._value = text

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_text_changed ( self, text )

# class TextItemWidget ( ItemWidget )