# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .collection_item_widget         import CollectionItemWidget
from .int_item_widget                import IntItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###           CLASS LIST-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'list' )
class ListItemWidget ( CollectionItemWidget ):
	"""
	The class `pyui.inspector.ListItemWidget` is used to edit / display lists in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.ListItemWidget` class.

		Arguments:
			self    (`pyui.inspector.ListItemWidget`): Instance to initialize.
			value                             (`any`): Collection edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), list) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( ListItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def length_itemwidget ( self ):
		"""
		Item widget used to edit the length of the list (`pyui.inspector.IntItemWidget`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )

		return self._length_itemwidget

	# def length_itemwidget ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################
	
	# --------------------------------------------------

	def create_header ( self ):
		"""
		Creates the header section of this list item widget.

		Arguments:
			self (`pyui.inspector.ListItemWidget`): List item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )

		length = len( self.value )

		self._length_itemwidget = IntItemWidget(
			      value = length,
			is_editable = self.isEnabled() and self.is_editable,
			     parent = self
			)
		self._length_itemwidget.value_changed.connect( self.on_length_changed )
		self.layout().addRow( 'Length', self._length_itemwidget )

	# def create_header ( self )
	
	# --------------------------------------------------

	def create_items ( self ):
		"""
		Creates the items of this list item widget.

		Arguments:
			self (`pyui.inspector.ListItemWidget`): List item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )

		length = len( self.value )

		if self.value is None:

			label = PyQt5.QtWidgets.QLabel( 'None', self )
			self.gridlayout.addWidget( label, 0, 0 )
		
		elif length == 0:

			label = PyQt5.QtWidgets.QLabel( 'empty', self )
			self.gridlayout.addWidget( label, 0, 0 )
		
		elif length > 50:

			label = PyQt5.QtWidgets.QLabel( '{} items (too many to display)'.format( length ), self )
			self.gridlayout.addWidget( label, 0, 0 )

		else:
			for index in range( length ):

				item_value = self.value[ index ]				
				item = self.create_item( index, 0, item_value )
				self.add_item( item )

			# for index in range(size)

		# if ( array is None )

	# def create_items ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this list item widget.

		Arguments:
			self (`pyui.inspector.ListItemWidget`): List item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )

		# Update the length itemwidget
		self._length_itemwidget.blockSignals( True )
		self._length_itemwidget.value = len( self.value )
		self._length_itemwidget.is_editable = self.isEnabled() and self.is_editable
		self._length_itemwidget.blockSignals( False )

		# Update the parent class
		super( ListItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_length_changed ( self ):
		"""
		Slot invoked when the list is resized using the length item-widget.

		Arguments:
			self  (`pyui.inspector.ListItemWidget`): List item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )

		# Fetch the list
		length     = len( self.value )
		new_length = self._length_itemwidget.value
		new_value  = self.value

		# If the new length is <= 0, empty the list
		if new_length <= 0:
			new_value = []

		# If the new length is >= 0 and the list was enpty, create a new list
		elif self.value is None or length == 0:
			new_value = [None] * new_length

		# If the new length is less than the current length, cut the list
		elif new_length < length:
			new_value = new_value[ 0 : new_length ]

		# If the new length is greater than the current length, repeat the last item in the list
		else:
			# length difference
			diff = new_length - length

			# Last item
			item = new_value[ length-1 ]

			# Repeat it
			for i in range( diff ):
				new_value.append( copy.deepcopy(item) )

		# Set the list and update self
		self.value = new_value

		# Notify that the list changed
		self.value_changed.emit()
	
	# def on_length_changed ( self, state )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( object )
	def on_item_changed ( self, item ):
		"""
		Slot invoked when an item of this collection item widget is edited.

		Arguments:
			self      (`pyui.inspector.ListItemWidget`): List item widget.
			item (`pyui.inspector.ListItemWidget.Item`): Edited item.
		"""
		assert pytools.assertions.type_is_instance_of( self, ListItemWidget )
		assert pytools.assertions.type_is_instance_of( item, ListItemWidget.Item )

		# Save the modified value
		self.value[ item.x ] = item.widget.value

		# Notify that the list changed
		self.value_changed.emit()

	# def on_item_changed ( self, item )

# class ListItemWidget ( CollectionItemWidget )