# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###           CLASS PATH-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'Path' )
class PathItemWidget ( ItemWidget ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=pytools.path.Path(), is_editable=True, filemode=PyQt5.QtWidgets.QFileDialog.Directory, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.FilePathItemWidget` class.

		Arguments:
			self (`pyui.inspector.FilePathItemWidget`): Instance to initialize.
			value     (`None`/`pytools.path.FilePath`): Directory path value edited / displayed in this item widget.
			is_editable                       (`bool`): If `True` value can be edited using the item widget.
			filemode                           (`int`): Target type of the file dialog used to pick the path.
			parent  (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.path.Path) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is( filemode, PyQt5.QtWidgets.QFileDialog.FileMode )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Set properties
		self._filemode = filemode

		# Initialize the parent class
		super( PathItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, filemode, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def button ( self ):
		"""
		Button used to open the file dialog and select a file/directory (`PyQt5.QtWidgets.QPushButton`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		return self._button

	# def button ( self )

	# --------------------------------------------------

	@property
	def filemode ( self ):
		"""
		Filemode used to select either files or directories in the file dialog (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		return self._filemode
	
	# def filemode ( self )

	# --------------------------------------------------

	@filemode.setter
	def filemode ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		self._filemode = value
	
	# def filemode ( self, value )

	# --------------------------------------------------

	@property
	def lineedit ( self ):
		"""
		Line edit used to edit the path as text (`PyQt5.QtWidgets.QLineEdit`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		return self._lineedit

	# def lineedit ( self )

	# --------------------------------------------------

	@property
	def text_value ( self ):
		"""
		Text path (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		return str( self.value ) if self.value is not None else ''

	# def text_value ( self )	

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this path item widget.

		Arguments:
			self (`pyui.inspector.PathItemWidget`): Path item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		# Setup the parent class
		super( PathItemWidget, self ).setup()

		# create the layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 2 )
		self.setLayout( layout )

		# Create the line edit to display the path
		self._lineedit = PyQt5.QtWidgets.QLineEdit( self.text_value )
		self._lineedit.setReadOnly( self.isEnabled() and self.is_editable )
		self._lineedit.setMinimumSize( 40, ItemWidget.height )
		self._lineedit.setMaximumSize( self._lineedit.maximumWidth(), ItemWidget.height )
		self._lineedit.textChanged.connect( self.on_text_changed )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._lineedit )

		# Create the button to change the path
		self._button = PyQt5.QtWidgets.QPushButton( '...' )
		self._button.setMinimumSize( 40, ItemWidget.height )
		self._button.setMaximumSize( 40, ItemWidget.height )
		self._button.clicked.connect( self.on_button_clicked )
		self._button.setEnabled( self.isEnabled() and self.is_editable )
		layout.addWidget( self._button )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this path item widget.

		Arguments:
			self (`pyui.inspector.PathItemWidget`): Path item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		# Update the lineedit
		self._lineedit.blockSignals( True )
		self._lineedit.setText( self.text_value )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		self._lineedit.blockSignals( False )

		# Update the buttom
		self._button.setEnabled( self.isEnabled() and self.is_editable )

		# Update the parent class
		super( PathItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the text is edited in the lineedit.

		Arguments:
			self (`pyui.inspector.PathItemWidget`): Pathath item widget.
			text                           (`str`): The new path as text.
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the new path
		if ( self.filemode == PyQt5.QtWidgets.QFileDialog.Directory ):
			self._value = pytools.path.DirectoryPath( text ) if text else pytools.path.DirectoryPath()
		
		else:
			self._value = pytools.path.FilePath( text ) if text else pytools.path.FilePath()

		# Notify listeners that value changed
		self.value_changed.emit()

	# def OnTextChanged ( self, text )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_clicked ( self ):
		"""
		Slot invoked when the button to pick a directory is pressed.

		Arguments:
			self (`pyui.inspector.DirectoryPathItemWidget`): File path item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, PathItemWidget )

		# create the file dialog
		fileDialog = PyQt5.QtWidgets.QFileDialog( self, 'Select a path', self.get_text_value() )
		fileDialog.setFileMode( self.get_filemode() )
		fileDialog.setViewMode( PyQt5.QtWidgets.QFileDialog.List )

		# Execute it
		if ( fileDialog.exec() ):

			# fetch the selection
			selected = fileDialog.selectedFiles()

			# Check selection
			if ( len(selected) > 0 ):

				# Update the path
				if ( self.get_filemode() == PyQt5.QtWidgets.QFileDialog.Directory ):
					path = pytools.path.DirectoryPath( selected[0] )
				else:
					path = pytools.path.FilePath( selected[0] )

				self.set_value( path )
				self.value_changed.emit()

			# if ( len(selected) > 0 )

		# if ( fileDialog.exec() )

	# def OnPickClicked ( self )

# class PathWidget ( QtWidgets.QWidget )
