# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
from .path_item_widget               import PathItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###        CLASS FILE-PATH-ITEM-WIDGET         ###
# ##################################################

@RegisterItemWidgetAttribute( 'FilePath' )
class FilePathItemWidget ( PathItemWidget ):
	"""
	The class `pyui.inspector.FilePathItemWidget` is used to edit / display `pytools.path.FilePath` in the inspector.
	"""
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=pytools.path.FilePath(), is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.FilePathItemWidget` class.

		Arguments:
			self (`pyui.inspector.FilePathItemWidget`): Instance to initialize.
			value     (`None`/`pytools.path.FilePath`): Directory path value edited / displayed in this item widget.
			is_editable                       (`bool`): If `True` value can be edited using the item widget.
			parent  (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePathItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.path.FilePath) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		super( FilePathItemWidget, self ).__init__(
			      value = value,
			is_editable = is_editable,
			   filemode = PyQt5.QtWidgets.QFileDialog.AnyFile,
			     parent = parent
			)

	# def __init__ ( self, value, is_editable, parent )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the text is edited in the lineedit.

		Arguments:
			self (`pyui.inspector.FilePathItemWidget`): File path item widget.
			text                               (`str`): The new path as text.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePathItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the new path
		self._value = pytools.path.FilePath( text ) if text else pytools.path.FilePath()
		
		# Notify listeners that value changed
		self.value_changed.emit()

	# def on_text_changed ( self, text )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_clicked ( self ):
		"""
		Slot invoked when the button to pick a directory is pressed.

		Arguments:
			self (`pyui.inspector.DirectoryPathItemWidget`): File path item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePathItemWidget )

		# create the file dialog
		dialog = PyQt5.QtWidgets.QFileDialog(
			pyui.Application.instance().main_window,
			'Select a file path',
			self.text_value
			)
		dialog.setFileMode( self.filemode )
		dialog.setViewMode( PyQt5.QtWidgets.QFileDialog.List )

		# Execute it
		if ( dialog.exec() ):

			# fetch the selection
			dialog_selection = fileDialog.selectedFiles()

			# Check selection
			if ( len(dialog_selection) > 0 ):

				# Update the path
				self.value = pytools.path.FilePath( dialog_selection[0] )

				# Notify listeners that value changed
				self.value_changed.emit()

			# if ( len(selected) > 0 )

		# if ( fileDialog.exec() )

	# def on_button_clicked ( self )

# class FilePathItemWidget ( PathItemWidget )