# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###          CLASS ENUM-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'Enum' )
class EnumItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.BytesItemWidget` is used to edit / display `pytools.serialization.Enum` in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, value_type=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.EnumItemWidget` class.

		Arguments:
			self      (`pyui.inspector.EnumItemWidget`): Instance to initialize.
			value (`None`/`pytools.serialization.Enum`): Enum value edited / displayed in this item widget.
			value_type                  (`None`/`type`): Type of the enum value, used to get possible values of the enum.
			is_editable                        (`bool`): If `True` value can be edited using the item widget.
			parent   (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.serialization.Enum) )
		assert pytools.assertions.type_in( value_type, (type(None), type) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Set the value type
		self._value_type = type(value) if value_type is None else value_type
		
		# Initialize the parent class
		super( EnumItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, value_type, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def combobox ( self ):
		"""
		Combobox used to edit the value of the enum (`PyQt5.QtWidgets.QComboBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		return self._combobox
	
	# def combobox ( self )

	# --------------------------------------------------

	@property
	def possible_values ( self ):
		"""
		Possible values taken by the enum (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		if self.value_type is type(None):
			return [ 'None' ] 
		
		else:
			return [ 'None' ] + self.value_type.get_possible_values()
	
	# def possible_values ( self )

	# --------------------------------------------------

	@ItemWidget.value.setter
	def value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		value_type = type( value )

		# If the value has a different type than before
		if ( value_type != self.value_type and value_type != type(None) ):
			
			# Save the new value type
			self._value_type = value

			# Change the options displayed in the combobox
			self._combobox.blockSignals( True )
			self._combobox.clear()
			self._combobox.addItems( self.possible_values )
			self._combobox.blockSignals( False )

		# Set the value
		self._value = value

		# Update this enum item widget
		self.update()

	# def value ( self, value )

	# --------------------------------------------------

	@property
	def value_type ( self ):
		"""
		Type of the enum value, used to get possible values of the enum (`type`).

		This type is required when creating an item widget with `None` value.
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		return self._value_type
	
	# def value_type ( self )

	# --------------------------------------------------

	@value_type.setter
	def value_type ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )
		assert pytools.assertions.type_is( value, type )

		# Save the value type
		self._value_type = value

		# Change the options displayed in the combobox
		self._combobox.blockSignals( True )
		self._combobox.clear()
		self._combobox.addItems( self.possible_values )
		self._combobox.blockSignals( False )
	
		# Update this enum item widget
		self.update()

	# def value_type ( self, value )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this enum item widget.

		Arguments:
			self (`pyui.inspector.EnumItemWidget`): Enum item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		# Setup the parent class
		super( EnumItemWidget, self ).setup()
		
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		self._combobox = PyQt5.QtWidgets.QComboBox( self )
		self._combobox.addItems( self.possible_values )
		self._combobox.setMinimumSize( 22, EnumItemWidget.height )
		self._combobox.setMaximumSize( self._combobox.maximumWidth(), EnumItemWidget.height )
		self._combobox.setCurrentText( self.text_value )
		self._combobox.currentTextChanged.connect( self.on_current_text_changed )
		self._combobox.setEnabled( self.isEnabled() and self.is_editable )
		layout.addWidget( self._combobox )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this int item widget.

		Arguments:
			self (`pyui.inspector.EnumItemWidget`): Int item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )

		# Update the combobox
		self._combobox.blockSignals( True )
		self._combobox.setCurrentText( self.text_value )
		self._combobox.setEnabled( self.isEnabled() and self.is_editable )
		self._combobox.blockSignals( False )

		# Update the parent class
		super( EnumItemWidget, self ).update()

	# def update ( self )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_current_text_changed ( self, text ):
		"""
		Slot invoked when the value selected in this enum item widget changed.

		Arguments:
			self (`pyui.inspector.EnumItemWidget`): Float item widget.
			text                           (`str`): The new enum value.
		"""
		assert pytools.assertions.type_is_instance_of( self, EnumItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the value
		self._value.value = text if text != 'None' else None

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_current_text_changed ( self, text )

# class EnumItemWidget ( ItemWidget )