# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###           CLASS BOOL-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'bool' )
@RegisterItemWidgetAttribute( 'bool_' )
@RegisterItemWidgetAttribute( 'bool8' )
class BoolItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.BoolItemWidget` is used to edit / display boolean in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=False, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.BoolItemWidget` class.

		Arguments:
			self                         (`pyui.inspector.BoolItemWidget`): Instance to initialize.
			value (`None`/`bool`/`numpy.bool`/`numpy.bool_`/`numpy.bool8`): Boolean value edited / displayed in this item widget.
			is_editable                                           (`bool`): If `True` value can be edited using the item widget.
			parent                      (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		assert pytools.assertions.type_in( value, (
			# Python
			type(None), bool,
			# Numpy
			numpy.bool, numpy.bool_, numpy.bool8
			))

		# Initialize the parent class
		super( BoolItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def bool_value ( self ):
		"""
		Boolean value of item widget (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )

		return bool( self.value ) if self.value is not None else False

	# def bool_value ( self )

	# --------------------------------------------------

	@property
	def checkbox ( self ):
		"""
		Check-Box used to edit the boolean value of this item widget (`PyQt5.QtWidgets.QCheckBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )

		return self._checkbox
	
	# def checkbox ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this bool item widget.

		Arguments:
			self (`pyui.inspector.BoolItemWidget`): Bool item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )

		# Setup the parent class
		super( BoolItemWidget, self ).setup()

		# Create layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create checkbox
		self._checkbox = PyQt5.QtWidgets.QCheckBox( self )
		self._checkbox.setCheckState( PyQt5.QtCore.Qt.Checked if self.bool_value else PyQt5.QtCore.Qt.Unchecked )
		self._checkbox.setMinimumSize( BoolItemWidget.height, BoolItemWidget.height )
		self._checkbox.setMaximumSize( self._checkbox.maximumWidth(), BoolItemWidget.height )
		self._checkbox.stateChanged.connect( self.on_state_changed )
		self._checkbox.setEnabled( self.isEnabled() and self.is_editable )
		layout.addWidget( self._checkbox )

	# def Setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this bool item widget.

		Arguments:
			self (`pyui.inspector.BoolItemWidget`): Bool item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )

		# Update the checkbox
		self._checkbox.setCheckState( PyQt5.QtCore.Qt.Checked if self.bool_value else PyQt5.QtCore.Qt.Unchecked )
		self._checkbox.setEnabled( self.isEnabled() and self.is_editable )

		# Update the parent class
		super( BoolItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_state_changed ( self, state ):
		"""
		Slot invoked when the state of the checkbox changes.

		Arguments:
			self (`pyui.inspector.BoolItemWidget`): Bool item widget.
			state                          (`int`): New state of the checkbox.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolItemWidget )
		assert pytools.assertions.type_is( state, int )

		# Set the internal value copy
		self._value = (state == PyQt5.QtCore.Qt.Checked)

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_state_changed ( self, state )

# class BoolItemWidget ( ItemWidget )