# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .inspector_widget    import InspectorWidget
from .item_widget         import ItemWidget
from .item_widget_factory import ItemWidgetFactory

# ##################################################
# ###        CLASS COLLECTION-ITEM-WIDGET        ###
# ##################################################

class CollectionItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.CollectionItemWidget` is used to edit / display collections in the inspector.
	"""

	# ##################################################
	# ###                 CLASS ITEM                 ###
	# ##################################################

	class Item ( PyQt5.QtCore.QObject ):
		"""
		The `pyui.inspector.CollectionItemWidget.Item` class wrapps an item of the collection.

		Its jobs are:
			- keep a reference to the underlying item widget
			- keep it 2D position in the grid layout of the collection item widget
			- foward signal from the underlying item widget to the collection widget.
		"""

		"""
		Signal emited when the item is edited.

		The argument is the item being edited.
		"""
		item_changed = PyQt5.QtCore.pyqtSignal( object )

		def __init__ ( self, x, y, widget ):
			"""
			Initialize a new instance of the `pyui.inspector.CollectionItemWidget.Item` class.

			Arguments:
				self (`pyui.inspector.CollectionItemWidget.Item`): Instance to initialize. 
				x                                         (`int`): Row of the item in the layout of the collection item widget.
				y                                         (`int`): Column of the item in the layout of the collection item widget.
				widget              (`pyui.inspector.ItemWidget`): Item widget used to display/edit the value.
			"""
			super( CollectionItemWidget.Item, self ).__init__()
			self._x = x
			self._y = y
			self._widget = widget			
			if isinstance( widget, ItemWidget ):
				widget.value_changed.connect( self.on_value_changed )

		@property
		def x ( self ):
			"""
			Row of the item in the layout of the collection item widget (`int`).
			"""
			return self._x

		@property
		def y ( self ):
			"""
			Column of the item in the layout of the collection item widget (`int`).
			"""
			return self._y

		@property
		def widget ( self ):
			"""
			Item widget used to display/edit the value (`pyui.inspector.ItemWidget`).
			"""
			return self._widget

		@PyQt5.QtCore.pyqtSlot()
		def on_value_changed ( self ):
			"""
			Slot invoked when the item is edited using its item widget.

			Arguments:
				self (`pyui.inspector.CollectionItemWidget.Item`): Edited item.
			"""
			self.item_changed.emit( self )

	# class Item ( PyQt5.QtCore.QObject )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.CollectionItemWidget` class.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Instance to initialize.
			value                                (`any`): Collection edited / displayed in this item widget.
			is_editable                         (`bool`): If `True` value can be edited using the item widget.
			parent    (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( CollectionItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def items ( self ):
		"""
		Items of this collection (`list` of `pyui.inspector.CollectionItemWidget.Item`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		return self._items

	# def items ( self )

	# --------------------------------------------------

	@property
	def gridlayout ( self ):
		"""
		Layout used to arange the items widgets of this collection item widget (`PyQt5.QtWidgets.QGridLayout`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		return self._gridlayout

	# def gridlayout ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def add_item ( self, item ):
		"""
		Adds an item to this collection item widget.

		Arguments:
			self      (`pyui.inspector.CollectionItemWidget`): Collection item widget.
			item (`pyui.inspector.CollectionItemWidget.Item`): Item to add.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		assert pytools.assertions.type_is_instance_of( item, CollectionItemWidget.Item )

		# Connect the signal of the item
		item.item_changed.connect( self.on_item_changed )

		# Add the item to the list of items
		self._items.append( item )

		# Add its widget to the gridlayout
		self._gridlayout.addWidget( item.widget, item.x, item.y )
	
	# def add_item ( self, item )

	# --------------------------------------------------

	def create_footer ( self ):
		"""
		Creates the footer section of this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		
		pass

	# def create_footer ( self )

	# --------------------------------------------------

	def create_header ( self ):
		"""
		Creates the header section of this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		
		pass

	# def create_header ( self )

	# --------------------------------------------------

	def create_item ( self, x, y, item_value ):
		"""
		Creates a new item for this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget.
			x                                    (`int`): column in which to put the item widget used to edit/display the value.
			y                                    (`int`): row in which to put the item widget used to edit/display the value.
			item_value                           (`any`): value to display/edit.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		# Get the type of the value and its name
		item_type     = type( item_value )
		item_typename = item_type.__name__
		
		# Attempt to create an item widget for it
		item_widget = ItemWidgetFactory.instantiate_widget( item_type,
			      value = item_value,
			is_editable = self.isEnabled() and self.is_editable,
			     parent = self
			)
		
		# If a widget exists for this type of item, use it
		if item_widget is not None:
			
			return CollectionItemWidget.Item( x, y, item_widget )
			
		# Second, use an inspector if possible
		elif isinstance( item_value, pytools.serialization.Serializable ):
			
			item_widget = InspectorWidget(
				      value = item_value,
				is_editable = self.isEnabled() and self.is_editable,
				    is_root = False,
				     parent = self
				)
			
			return CollectionItemWidget.Item( x, y, item_widget )

		# Otherwise show an error label
		else:
			item_widget = PyQt5.QtWidgets.QLabel( '{} ?'.format(item_typename), self )
			item_widget.setStyleSheet( 'color: red;' )
			
			return CollectionItemWidget.Item( x, y, item_widget )
		
	# def create_item ( self, x, y, item_value )

	# --------------------------------------------------

	def create_items ( self ):
		"""
		Creates the items of this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		raise NotImplementedError()

	# def create_items ( self )

	# --------------------------------------------------

	def delete_items ( self ):
		"""
		Deletes the items of this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		
		while not self._gridlayout.isEmpty():
		
			item = self._gridlayout.itemAt( 0 )
			widget = item.widget()
			if widget:
				self._gridlayout.removeWidget( widget )
				widget.deleteLater()
						
		# while not self._gridlayout.isEmpty()

		self._items = []

	# def delete_items ( self )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		# Setup the parent class
		super( CollectionItemWidget, self ).setup()
		
		# Create the main layout
		layout = PyQt5.QtWidgets.QFormLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Let child classes setup some widgets
		self.create_header()

		# Create the layout for the items
		self._gridlayout = PyQt5.QtWidgets.QGridLayout()
		layout.addRow( self._gridlayout )

		# Create the items widgets
		self._items = []
		self.create_items()

		# Let child classes settup some widgets
		self.create_footer()

	# def Setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this collection item widget.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Collection item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )

		self.delete_items()
		self.create_items()
		super( CollectionItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( object )
	def on_item_changed ( self, item ):
		"""
		Slot invoked when an item of this collection item widget is edited.

		Arguments:
			self      (`pyui.inspector.CollectionItemWidget`): Collection item widget.
			item (`pyui.inspector.CollectionItemWidget.Item`): Edited item.
		"""
		assert pytools.assertions.type_is_instance_of( self, CollectionItemWidget )
		assert pytools.assertions.type_is_instance_of( item, CollectionItemWidget.Item )

		raise NotImplementedError()

	# def on_item_changed ( self, item )

# class CollectionItemWidget ( ItemWidget )