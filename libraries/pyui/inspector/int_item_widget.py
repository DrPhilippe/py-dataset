# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###          CLASS INT64-ITEM-WIDGET           ###
# ##################################################

# Integers:
@RegisterItemWidgetAttribute( 'byte' )
@RegisterItemWidgetAttribute( 'short' )
@RegisterItemWidgetAttribute( 'int' )
@RegisterItemWidgetAttribute( 'int_' )
@RegisterItemWidgetAttribute( 'intc' )
@RegisterItemWidgetAttribute( 'intp' )
@RegisterItemWidgetAttribute( 'longlong' )
@RegisterItemWidgetAttribute( 'int8' )
@RegisterItemWidgetAttribute( 'int16' )
@RegisterItemWidgetAttribute( 'int32' )
@RegisterItemWidgetAttribute( 'int64' )
# Unsigned integers:
@RegisterItemWidgetAttribute( 'ubyte' )
@RegisterItemWidgetAttribute( 'ushort' )
@RegisterItemWidgetAttribute( 'uint' )
@RegisterItemWidgetAttribute( 'uintc' )
@RegisterItemWidgetAttribute( 'uintp' )
@RegisterItemWidgetAttribute( 'ulonglong' )
@RegisterItemWidgetAttribute( 'uint8' )
@RegisterItemWidgetAttribute( 'uint16' )
@RegisterItemWidgetAttribute( 'uint32' )
@RegisterItemWidgetAttribute( 'uint64' )
class IntItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.IntItemWidget` is used to edit / display integers in the inspector.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=0, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.IntItemWidget` class.

		Arguments:
			self     (`pyui.inspector.IntItemWidget`): Instance to initialize.
			value                      (`None`/`int`): Integer value edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		assert pytools.assertions.type_in( value, (
			# Python:
			type(None), int,
			# Numpy integers:
			numpy.byte,
			numpy.short,
			numpy.int,
			numpy.int_,
			numpy.intc,
			numpy.intp,
			numpy.longlong,
			numpy.int8,
			numpy.int16,
			numpy.int32,
			numpy.int64,
			# Numpy unsigned integers:
			numpy.ubyte,
			numpy.ushort,
			numpy.uint,
			numpy.uintc,
			numpy.uintp,
			numpy.ulonglong,
			numpy.uint8,
			numpy.uint16,
			numpy.uint32,
			numpy.uint64,
			))
		
		# Initialize the parent class
		super( IntItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def int_value ( self ):
		"""
		Integer value of item widget (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )

		return int( self.value ) if self.value is not None else 0
	
	# def int_value ( self )

	# --------------------------------------------------

	@property
	def spinbox ( self ):
		"""
		Spinbox used to edit / display the int value of this item widget (`PyQt5.QtWidgets.QSpinBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )

		return self._spinbox
	
	# def spinbox ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this int item widget.

		Arguments:
			self (`pyui.inspector.IntItemWidget`): Int item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )

		# Setup the parent class
		super( IntItemWidget, self ).setup()

		# Create the layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create the spinbox
		self._spinbox = PyQt5.QtWidgets.QSpinBox( self )
		self._spinbox.setMinimumSize( 22, IntItemWidget.height )
		self._spinbox.setMaximumSize( self._spinbox.maximumWidth(), IntItemWidget.height )
		self._spinbox.setMinimum( -2147483648 )
		self._spinbox.setMaximum( 2147483647 )
		self._spinbox.setValue( self.int_value )
		self._spinbox.setReadOnly( not self.isEnabled() or self.is_not_editable )
		self._spinbox.valueChanged.connect( self.on_value_changed )
		layout.addWidget( self._spinbox )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this int item widget.

		Arguments:
			self (`pyui.inspector.IntItemWidget`): Int item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )

		# Update the spinbox
		self._spinbox.setValue( self.int_value )
		self._spinbox.setReadOnly( not self.isEnabled() or self.is_not_editable )

		# Update the parent class
		super( IntItemWidget, self ).update()

	# def update ( self )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_value_changed ( self, value ):
		"""
		Slot invoked when the value of the int item widget is edited.

		Arguments:
			self (`pyui.inspector.IntItemWidget`): Int item widget.
			text                          (`str`): The new bytes value.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntItemWidget )
		assert pytools.assertions.type_is( value, int )

		# Save the value
		self._value = value

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_value_changed ( self, value )

# class IntItemWidget ( ItemWidget )
