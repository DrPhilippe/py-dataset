# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets
import re

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###         CLASS SHAPE-WIDGET            ###
# ##################################################

@RegisterItemWidgetAttribute( 'tuple' )
class ShapeItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.ShapeItemWidget` is used to edit / display the shape of a numpy ndarray.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################
	
	# --------------------------------------------------
	
	"""
	Regular expression used to check if the textual shape is valid.
	"""
	_regex = re.compile( r"([0-9]+(x[0-9]+)*)|None" )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=tuple(), is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.ShapeItemWidget` class.

		Arguments:
			self   (`pyui.inspector.ShapeItemWidget`): Instance to initialize.
			value          (`None`/`tuple` of `int`s): Shape edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), tuple) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( ShapeItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------
	
	@property
	def text_value ( self ):
		
		value = self.value
		
		if value is None:
			return 'None'

		elif len(value) == 0:
			return ''

		elif len(value) == 1:
			return str(value[0])

		else:
			return 'x'.join( [str(i) for i in value] )

	# def text_value ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this shape item widget.

		Arguments:
			self (`pyui.inspector.ShapeItemWidget`): Shape item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeItemWidget )

		# Setup the parent class
		super( ShapeItemWidget, self ).setup()

		# create the layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 );
		self.setLayout( layout )

		# Create the line edit to display the path
		self._lineedit = PyQt5.QtWidgets.QLineEdit( self.text_value )
		self._lineedit.setMinimumSize( ShapeItemWidget.height, ShapeItemWidget.height )
		self._lineedit.setMaximumSize( self._lineedit.maximumWidth(), ShapeItemWidget.height )
		self._lineedit.textChanged.connect( self.on_text_changed )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._lineedit )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this shape item widget.

		Arguments:
			self (`pyui.inspector.ShapeItemWidget`): Shape item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeItemWidget )

		# Update the lineedit
		self._lineedit.setText( self.text_value )
		self._lineedit.setEnabled( self.isEnabled() and self.is_editable )

		# Update the parent class
		super( ShapeItemWidget, self ).update()
		
	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the value of the shape item widget is edited.

		Arguments:
			self (`pyui.inspector.ShapeItemWidget`): Text item widget.
			text                            (`str`): The new text shape.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeItemWidget )
		assert pytools.assertions.type_is( text, str )

		# reset the text color
		self.setStyleSheet( '' )
	
		# if the text is empty
		if ( not text ):

			self._value = tuple()
			self.value_changed.emit()

		# if dimensions are specified
		elif ( ShapeItemWidget._regex.fullmatch( text ) != None ):
			
			if ( text == 'None' ):
				self._value = None
				self.value_changed.emit()

			else:
				# split the txt
				txt_dims = text.split( 'x' )

				# parse each dim
				dims = []
				for txt_dim in txt_dims:

					if ( len( txt_dim ) > 0 ):
					
						dim = int( txt_dim )
						dims.append( dim )

					# if ( len( txt_dim ) > 0 )

				# for txt_dim in txt_dims:

				self._value = tuple( dims )

				# notify of the modification
				self.value_changed.emit()

		# unrecognized shape
		else:
			self.setStyleSheet( 'color: red' )
		
		# if ( ShapeDataItemWidget._regex.fullmatch( text ) != None )

	# def on_text_changed ( self, text )

# class ShapeItemWidget ( ItemWidget )
