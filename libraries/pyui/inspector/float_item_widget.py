# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###          CLASS FLOAT-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'half' )
@RegisterItemWidgetAttribute( 'single' )
@RegisterItemWidgetAttribute( 'double' )
@RegisterItemWidgetAttribute( 'float' )
@RegisterItemWidgetAttribute( 'float_' )
@RegisterItemWidgetAttribute( 'longfloat' )
@RegisterItemWidgetAttribute( 'float16' )
@RegisterItemWidgetAttribute( 'float32' )
@RegisterItemWidgetAttribute( 'float64' )
class FloatItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.FloatItemWidget` is used to edit / display floats in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=0.0, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.FloatItemWidget` class.

		Arguments:
			self   (`pyui.inspector.FloatItemWidget`): Instance to initialize.
			value                    (`None`/`float`): Float value edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		assert pytools.assertions.type_in( value, (
			# Python:
			type(None), float,
			# Numpy
			numpy.half,
			numpy.single,
			numpy.double,
			numpy.float,
			numpy.float_,
			numpy.longfloat,
			numpy.float16,
			numpy.float32,
			numpy.float64
			))
		
		# Initialize the parent class
		super( FloatItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def float_value ( self ):
		"""
		Float value of item widget (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )

		return float( self.value ) if self.value is not None else 0.0

	# def float_value ( self )	

	# --------------------------------------------------

	@property
	def spinbox ( self ):
		"""
		Spinbox used to edit / display the float value of this item widget (`PyQt5.QtWidgets.QDoubleSpinBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )

		return self._spinbox
	
	# def spinbox ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this float item widget.

		Arguments:
			self (`pyui.inspector.FloatItemWidget`): Float item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )

		# Setup the parent class
		super( FloatItemWidget, self ).setup()

		# Create the layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create the spinbox
		self._spinbox = PyQt5.QtWidgets.QDoubleSpinBox( self )
		self._spinbox.setMinimumSize( 22, FloatItemWidget.height )
		self._spinbox.setDecimals( 4 )
		self._spinbox.setMaximumSize( self._spinbox.maximumWidth(), FloatItemWidget.height )
		self._spinbox.setMinimum( -1_000_000.0 )
		self._spinbox.setMaximum( 1_000_000.0 )
		self._spinbox.setValue( self.float_value )
		self._spinbox.valueChanged.connect( self.on_value_changed )
		self._spinbox.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._spinbox )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this float item widget.

		Arguments:
			self (`pyui.inspector.FloatItemWidget`): Float item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )

		# Update the spinbox
		self._spinbox.setValue( self.float_value )
		self._spinbox.setReadOnly( not self.isEnabled() or self.is_not_editable )

		# Update the parent class
		super( FloatItemWidget, self ).update()

	# def update ( self )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( float )
	def on_value_changed ( self, value ):
		"""
		Slot invoked when the value of the float item widget is edited.

		Arguments:
			self (`pyui.inspector.FloatItemWidget`): Float item widget.
			text                            (`str`): The new bytes value.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatItemWidget )
		assert pytools.assertions.type_is( value, float )

		# Save the value
		self._value = value

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_value_changed ( self, value )

# class FloatItemWidget ( ItemWidget )