# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .collection_item_widget         import CollectionItemWidget
from .dtype_item_widget              import DTypeItemWidget
from .shape_item_widget              import ShapeItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###         CLASS NDARRAY-ITEM-WIDGET          ###
# ##################################################

@RegisterItemWidgetAttribute( 'ndarray' )
class NDArrayItemWidget ( CollectionItemWidget ):
	"""
	The class `pyui.inspector.ListItemWidget` is used to edit / display `numpy.ndarray` in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.CollectionItemWidget` class.

		Arguments:
			self (`pyui.inspector.CollectionItemWidget`): Instance to initialize.
			value               (`None`/`numpy.ndarray`): Collection edited / displayed in this item widget.
			is_editable                         (`bool`): If `True` value can be edited using the item widget.
			parent    (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), numpy.ndarray) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( NDArrayItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def shape_itemwidget ( self ):
		"""
		Item widget used to edit the shape of the array (`pyui.inspector.ShapeItemWidget`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		return self._shape_itemwidget

	# def shape_itemwidget ( self )

	# --------------------------------------------------

	@property
	def dtype_itemwidget ( self ):
		"""
		Item widget used to edit the data type of the array (`pyui.inspector.DTypeItemWidget`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		return self._dtype_itemwidget

	# def dtype_itemwidget ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################
	
	# --------------------------------------------------

	def create_header ( self ):
		"""
		Creates the header section of this ndarray item widget.

		Arguments:
			self (`pyui.inspector.NDArrayItemWidget`): NDArray item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		array = self.value
		dtype = array.dtype if array is not None else None
		shape = array.shape if array is not None else None

		self._dtype_itemwidget = DTypeItemWidget(
			      value = dtype,
			is_editable = self.isEnabled() and self.is_editable,
			     parent = self
			)
		self._dtype_itemwidget.value_changed.connect( self.on_dtype_changed )
		self.layout().addRow( 'DType', self._dtype_itemwidget )

		self._shape_itemwidget = ShapeItemWidget(
			      value = shape,
			is_editable = shape is not None and self.isEnabled() and self.is_editable,
			     parent = self
			)
		self._shape_itemwidget.setEnabled( shape is not None )
		self._shape_itemwidget.value_changed.connect( self.on_shape_changed )
		self.layout().addRow( 'Shape', self._shape_itemwidget )

	# def create_header ( self )
	
	# --------------------------------------------------

	def create_items ( self ):
		"""
		Creates the items of this ndarray item widget.

		Arguments:
			self (`pyui.inspector.NDArrayItemWidget`): NDArray item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		array = self.value

		if ( array is None ):

			label = PyQt5.QtWidgets.QLabel( 'None', self )
			self.gridlayout.addWidget( label, 0, 0 )

		elif ( array.ndim > 2 ):

			label = PyQt5.QtWidgets.QLabel( 'too many dimensions', self )
			self.gridlayout.addWidget( label, 0, 0 )
		
		elif ( array.size == 0 ):

			label = PyQt5.QtWidgets.QLabel( 'empty', self )
			self.gridlayout.addWidget( label, 0, 0 )
		
		elif ( len(array.shape) == 0 ):

			item = self.create_item( 0, 0, array.item() )
			self.add_item( item )

		elif ( array.size > 16 ):

			label = PyQt5.QtWidgets.QLabel( 'too many items', self )
			self.gridlayout.addWidget( label, 0, 0 )

		else:
			for ndindex in numpy.ndindex( array.shape ):
				x = ndindex[ 0 ]
				y = ndindex[ 1 ] if array.ndim > 1 else 0
				item_value = array[ x, y ] if array.ndim > 1 else array[ x ]
				
				item = self.create_item( x, y, item_value )
				self.add_item( item )

			# for ndindex in numpy.ndindex(array.shape)

		# if ( array is None )

	# def create_items ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this ndarray item widget.

		Arguments:
			self (`pyui.inspector.NDArrayItemWidget`): NDArray item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		array = self.value
		dtype = array.dtype if array is not None else None
		shape = array.shape if array is not None else None

		# Update the dtype itemwidget
		self._dtype_itemwidget.blockSignals( True )
		self._dtype_itemwidget.value = dtype
		self._dtype_itemwidget.is_editable = self.is_editable
		self._dtype_itemwidget.setEnabled( self.isEnabled() and dtype is not None )
		self._dtype_itemwidget.blockSignals( False )

		# Update the shape itemwidget
		self._shape_itemwidget.blockSignals( True )
		self._shape_itemwidget.value = shape
		self._shape_itemwidget.is_editable = self.is_editable
		self._shape_itemwidget.setEnabled( self.isEnabled() and dtype is not None )
		self._shape_itemwidget.blockSignals( False )

		# Update the parent class
		super( NDArrayItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_dtype_changed ( self ):
		"""
		Slot invoked when the data type of the array is changed.

		Arguments:
			self  (`pyui.inspector.NDArrayItemWidget`): NDArray item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		# Fetch the new datatype and the current array
		dtype = self._dtype_itemwidget.value
		value = self.value

		# If the array is none, create a new one
		if ( value is None ):
			value = numpy.empty( (0), dtype )

		# Otherwise cast the array
		else:
			value = value.astype( dtype )

		# Set the array value and update self
		self.value = value

		# Notify that the array changed
		self.value_changed.emit()
	
	# def on_dtype_changed ( self, state )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_shape_changed ( self ):
		"""
		Slot invoked when the shape of the array is changed.

		Arguments:
			self  (`pyui.inspector.NDArrayItemWidget`): NDArray item widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )

		# Fetch the new datatype and the array
		shape = self._shape_itemwidget.value
		value = self.value

		# Resize the array
		value.resize( shape, refcheck=False )

		# Set the array value and update self
		self.value = value

		# Notify that the array changed
		self.value_changed.emit()
	
	# def on_shape_changed ( self, state )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( object )
	def on_item_changed ( self, item ):
		"""
		Slot invoked when an item of this array item widget is edited.

		Arguments:
			self      (`pyui.inspector.NDArrayItemWidget`): NDArray item widget.
			item (`pyui.inspector.NDArrayItemWidget.Item`): Edited item.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayItemWidget )
		assert pytools.assertions.type_is_instance_of( item, NDArrayItemWidget.Item )

		value = self.value

		if value.ndim == 0:
			value.itemset( item.widget.value )

		elif value.ndim == 1:
			value[ item.x ] = item.widget.value

		elif value.ndim == 2:
			value[ item.x, item.y ] = item.widget.value

		# Notify that the array changed
		self.value_changed.emit()

	# def on_item_changed ( self, item )

# class NDArrayItemWidget ( CollectionItemWidget )