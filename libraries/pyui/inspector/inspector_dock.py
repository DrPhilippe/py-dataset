# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..widgets import Dock
from .inspector_widget import InspectorWidget

# ##################################################
# ###            CLASS INSPETCOR-DOCK            ###
# ##################################################

class InspectorDock ( Dock ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, title='Inspector', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDock )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.serialization.Serializable) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Set the value
		self._value = value
		self._is_editable = is_editable

		# Initialize the dock widget
		super( InspectorDock, self ).__init__( title, parent )
		
	# def __init__ ( self, value, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def inspector ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDock )
		
		return self._inspector

	# def inspector ( self )

	# --------------------------------------------------

	@property
	def value ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDock )
		
		return self.inspector.value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDock )

		self.inspector.value = value

	# def value ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDock )
		
		super( InspectorDock, self ).setup()
		self.setMinimumSize( 300, self.minimumHeight() )

		scroll_area = PyQt5.QtWidgets.QScrollArea( self )
		scroll_area.setWidgetResizable( True )
		self.setWidget( scroll_area )

		self._inspector = InspectorWidget(
			      value = self._value,
			is_editable = self._is_editable,
			    is_root = True,
			     parent = None
			)
		self._inspector.layout().setContentsMargins( 0, 0, 0, 0 )
		self._inspector.layout().setSpacing( 0 )
		scroll_area.setWidget( self._inspector )

	# def setup ( self )

# class InspectorDock ( Dock )