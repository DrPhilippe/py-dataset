# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget_factory import ItemWidgetFactory

# ##################################################
# ###    CLASS REGISTER-ITEM-WIDGET-ATTRIBUTE    ###
# ##################################################

class RegisterItemWidgetAttribute ( pytools.factory.RegisterClassAttribute ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, typename ):
		assert pytools.assertions.type_is( typename, str )

		super( RegisterItemWidgetAttribute, self ).__init__(
			   group = ItemWidgetFactory.group_name,
			typename = typename
			)

	# def __init__ ( self, typename )

# class RegisterItemWidgetAttribute ( pytools.factory.RegisterClassAttribute )