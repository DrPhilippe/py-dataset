# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# ##################################################
# ###             CLASS ITEM-WIDGET              ###
# ##################################################

class ItemWidget ( PyQt5.QtWidgets.QFrame ):
	"""
	The class `pyui.inspector.ItemWidget` is the base class of every item edited / displayed in the inspector.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------
	
	"""
	Height of single value item widgets (`int`).
	"""
	height = 22

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.ItemWidget` class.

		Arguments:
			self        (`pyui.inspector.ItemWidget`): Instance to initialize.
			value                             (`any`): Value edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Initialize the parent class
		super( ItemWidget, self ).__init__( parent )
		
		# Set properties
		self._value = value
		self._is_editable = is_editable

		# Setup ui
		self.setup()

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	"""
	Signal emitted when the value of this item widget is edited.
	"""
	value_changed = PyQt5.QtCore.pyqtSignal()

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def is_editable ( self ):
		"""
		If `True` the value of this item widget can be edited (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		return self._is_editable
	
	# def is_editable ( self )

	# --------------------------------------------------

	@is_editable.setter
	def is_editable ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )
		assert pytools.assertions.type_is( value, bool )
		
		self._is_editable = value
		self.update()
	
	# def is_editable ( self, value )

	# --------------------------------------------------

	@property
	def is_not_editable ( self ):
		"""
		If `True` the value of this item widget cannot be edited (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		return not self._is_editable
	
	# def is_not_editable ( self )

	# --------------------------------------------------

	@is_not_editable.setter
	def is_not_editable ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )
		assert pytools.assertions.type_is( value, bool )
		
		self._is_editable = not value
		self.update()
	
	# def is_not_editable ( self, value )

	# --------------------------------------------------

	@property
	def text_value ( self ):
		"""
		Text representation of this item widget value (`str`).
		"""
		return str( self.value ) if self.value is not None else 'None'

	# def text_value ( self )

	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		Value edited / displayed in this item widget (`any`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		return self._value

	# def value ( self )
	
	# --------------------------------------------------

	@value.setter
	def value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		# Save the value
		self._value = value

		# Update this item widget
		self.update()

	# def value ( self, value )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this item widget.

		Arguments:
			self (`pyui.inspector.ItemWidget`): Item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		pass

	# def Setup ( self )
	
	# --------------------------------------------------

	def set_value ( self, value, is_editable=True ):
		"""
		Setup the value and editable state of this item widget.

		Arguments:
			self (`pyui.inspector.ItemWidget`): Item widget of which to set properties.
			value                      (`any`): New value displayed / edited in this item widget.
			is_editable               (`bool`): If `True` the value of this item widget can be edited.
		"""

		self._value = value
		self._is_editable = is_editable
		self.update()

	# def set_value ( self, value, is_editable )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this item widget.

		Arguments:
			self (`pyui.inspector.ItemWidget`): Item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemWidget )

		# Update the parent class
		super( ItemWidget, self ).update()

	# def update ( self )

# class ItemWidget ( PyQt5.QtWidgets.QWidget )