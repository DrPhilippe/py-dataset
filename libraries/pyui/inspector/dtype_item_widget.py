# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###          CLASS DTYPE-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'dtype' )
class DTypeItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.BoolItemWidget` is used to edit / display numpy dtype in the inspector.

	https://docs.scipy.org/doc/numpy/reference/arrays.dtypes.html#attributes
	https://docs.scipy.org/doc/numpy/reference/arrays.scalars.html#arrays-scalars-built-in
	"""

	# ##################################################
	# ###               CONSTATNTS                   ###
	# ##################################################

	_items = [
		# None:
		'None',
		# Booleans:
		'bool',
		'bool_',
		'bool8',
		# Integers:
		'byte',
		'short',
		'int',
		'int_',
		'intc',
		'intp',
		'longlong',
		'int8',
		'int16',
		'int32',
		'int64',
		# Unsigned integers:
		'ubyte',
		'ushort',
		'uint',
		'uintc',
		'uintp',
		'ulonglong',
		'uint8',
		'uint16',
		'uint32',
		'uint64',
		# Floating-point numbers:
		'half',
		'single',
		'double',
		'float',
		'float_',
		'longfloat',
		'float16',
		'float32',
		'float64',
		# Complex floating-point numbers:
		'csingle',
		'complex',
		'complex_',
		'clongfloat',
		'complex64',
		'complex128',
		# Any Python object:
		'object',
		'object_',
		# Others:
		'bytes',
		'bytes_',
		'str',
		'str_',
		'unicode',
		'unicode_',
		'void'
	]

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.DTypeItemWidget` class.

		Arguments:
			self   (`pyui.inspector.DTypeItemWidget`): Instance to initialize.
			value              (`None`/`numpy.dtype`): Dtype value edited / displayed in this item widget.
			is_editable                      (`bool`): If `True` value can be edited using the item widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, DTypeItemWidget )
		assert pytools.assertions.type_in( value, (type(None), numpy.dtype ) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		super( DTypeItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def combobox ( self ):
		"""
		Combo-Box used to edit the dtype value of this item widget (`PyQt5.QtWidgets.QComboBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DTypeItemWidget )

		return self._combobox
	
	# def combobox ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this dtype item widget.

		Arguments:
			self (`pyui.inspector.DTypeItemWidget`): Dtype item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, DTypeItemWidget )

		# Setup the parent class
		super( DTypeItemWidget, self ).setup()

		# Create layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create combobox
		self._combobox = PyQt5.QtWidgets.QComboBox( self )
		self._combobox.addItems( DTypeItemWidget._items )
		self._combobox.setSizePolicy( PyQt5.QtWidgets.QSizePolicy.Expanding, PyQt5.QtWidgets.QSizePolicy.Preferred )
		self._combobox.setMinimumSize( 22, DTypeItemWidget.height )
		self._combobox.setMaximumSize( self._combobox.maximumWidth(), DTypeItemWidget.height )
		self._combobox.setCurrentText( self.text_value )
		self._combobox.currentTextChanged.connect( self.on_current_text_changed )
		self._combobox.setEnabled( self.isEnabled() and self.is_editable )
		layout.addWidget( self._combobox )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this dtype item widget.

		Arguments:
			self (`pyui.inspector.DTypeItemWidget`): Dtype item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, DTypeItemWidget )

		# Update the combobox
		self._combobox.blockSignals( True )
		self._combobox.setCurrentText( self.text_value )
		self._combobox.setEnabled( self.isEnabled() and self.is_editable )
		self._combobox.blockSignals( False )

		# Update the parent class
		super( DTypeItemWidget, self ).update()

	# def update ( self )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_current_text_changed ( self, text ):
		"""
		Slot invoked when the selected dtype changes.

		Arguments:
			self (`pyui.inspector.DTypeItemWidget`): Dtype item widget.
			state                           (`str`): Name of the selected dtype.
		"""
		assert pytools.assertions.type_is_instance_of( self, DTypeItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Set the dtype
		self._value = numpy.dtype( text ) if text != 'None' else None

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_current_text_changed ( self, text )

# class DTypeItemWidget ( ItemWidget )