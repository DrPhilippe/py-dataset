# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###         CLASS ITEM-WIDGET-FACTORY          ###
# ##################################################

class ItemWidgetFactory ( pytools.factory.ClassFactory ):

	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	group_name = 'widgets'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def has_widget_for ( cls, typename ):

		return cls.typename_is_used( cls.group_name, typename )

	# def has_widget_for ( cls, typename )

	# --------------------------------------------------

	@classmethod
	def instantiate_widget_for ( cls, typename, **kwargs ):

		return cls.instantiate( cls.group_name, typename, **kwargs )

	# def instantiate_widget_for ( cls, typename, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def instantiate_widget ( cls, type, **kwargs ):

		# Get the name of the type
		typename = type.__name__

		# Get the names of types for which a widget exists.
		registered_typenames = cls.get_typenames_of_group( cls.group_name )
		
		# If the type hase a dedicated widget, use it
		if typename in registered_typenames:
			return cls.instantiate( cls.group_name, typename, **kwargs )

		# Otherwise search in its base classes
		bases_types = list(type.__bases__)

		while bases_types:
			up = []

			for base_type in bases_types:

				if base_type.__name__ in registered_typenames:
					return cls.instantiate( cls.group_name, base_type.__name__, **kwargs )

				else:
					for base in base_type.__bases__:
						up.append( base )

			bases_types = up

		return None

	# def instantiate_widget ( cls, typename, **kwargs )

# class ItemWidgetFactory ( pytools.factory.ClassFactory )