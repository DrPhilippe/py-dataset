# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..widgets         import Dialog
from .inspector_widget import InspectorWidget

# ##################################################
# ###           CLASS INSPECTOR-DIALOG           ###
# ##################################################

class InspectorDialog ( Dialog ):
	"""
	Dialog used to inspect and edit serializable objects.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, title='Edit', text_ok='Apply', text_cancel='Cancel', parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.InspectorDialog` class.

		Arguments:
			self      (`pyui.inspector.InspectorDialog`): Instance to initialize.
			value (`pytools.serialization.Serializable`): Serializable to inspect.
			is_editable                         (`bool`): If `True` the value can be modified, otherwise its only displayed.
			title                                (`str`): Title of the dialog window.
			text_ox                              (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel                          (`str`): Text displayed on the button to cancel and close the dialog.
			parent    (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
		assert pytools.assertions.type_is_instance_of( value, pytools.serialization.Serializable )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Create a copy of the reference object
		self._value = value
		self._is_editable = is_editable

		# init the dialog
		super( InspectorDialog, self ).__init__( title, text_ok, text_cancel, parent )

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def is_editable ( self ):
		"""
		If `True` the value can be modified, otherwise its only displayed (`bool`).
		"""
		return self._is_editable

	# def is_editable ( self )

	# --------------------------------------------------

	@is_editable.setter
	def is_editable ( self, value ):
		"""
		If `True` the value can be modified, otherwise its only displayed (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
		assert pytools.assertions.type_is( value, bool )

		self._is_editable
		self.inspector.is_editable = value

	# def is_editable ( self )

	# --------------------------------------------------

	@property
	def inspector ( self ):
		"""
		Inspector used by this dialog (`pyui.inspector.InspectorWidget`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
	
		return self._inspector

	# def get_inspector ( self )

	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		Value inspected using this dialog (`pytools.serialization.Serializable`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
	
		return self._value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.serialization.Serializable) )
	
		self._value = value
		self.inspector.value = value

	# def value ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self, title, text_ok, text_cancel ):
		"""
		Setup this dialog window.

		Arguments:
			self (`pyui.inspector.InspectorDialog`): Dialog window to setup.
			title                           (`str`): Title of the dialog window.
			text_ox                         (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel                     (`str`): Text displayed on the button to cancel and close the dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )

		self.setMinimumSize( 400, 600 )
		super( InspectorDialog, self ).setup( title, text_ok, text_cancel )

	# def setup ( self )

	# --------------------------------------------------

	def setup_form ( self ):
		"""
		Setup the contents of this dialog window.

		Arguments:
			self (`pyui.inspector.InspectorDialog`): Dialog window to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )

		# Scroll Area
		scroll_area = PyQt5.QtWidgets.QScrollArea( self )
		scroll_area.setWidgetResizable( True )
		self.layout().addWidget( scroll_area )

		# Scroll Area / Inspector
		self._inspector = pyui.widgets.InspectorWidget(
			      value = self.value,
			is_editable = self.is_editable,
			    is_root = True,
			     parent = self
			)
		self._inspector.value_changed.connect( self.on_inspector_value_changed )
		self._inspector.setEnabled( self.value is not None )
		scroll_area.setWidget( self._inspector )

	# def setup_form ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# -------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot()
	def on_inspector_value_changed ( self ):
		"""
		Slot invoked when the inspected value changed.

		Arguments:
			self (`pyui.inspector.InspectorDialog`): The dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorDialog )

		pass

	# def on_inspector_value_changed ( self )
	
# class InspectorDialog ( Dialog )