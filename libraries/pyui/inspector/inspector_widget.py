# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets
import re

# INTERNALS
import pytools.assertions
import pytools.factory
import pytools.serialization
import pytools.text

# LOCALS
from .item_widget        import ItemWidget
from .item_widget_factory import ItemWidgetFactory

# ##################################################
# ###           CLASS INSPECTOR-WIDGET           ###
# ##################################################

# --------------------------------------------------

class InspectorWidget ( ItemWidget ):

	# ##################################################
	# ###                VALUE SETTER                ###
	# ##################################################

	# --------------------------------------------------

	class FieldSetter:
		def __init__ ( self, item, field_name, field_widget ):
			self.item = item
			self.field_name = field_name
			self.field_widget = field_widget

		def __call__ ( self ):
			setattr( self.item, self.field_name, self.field_widget.value )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=False, is_root=False, ignores=[], parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.InspectorWidget` class.

		Arguments:
			self             (`pyui.inspector.InspectorWidget`): Instance to initialize.
			value (`None`/`pytools.serialization.Serializable`): Value edited / displayed in this item widget.
			is_root                                    (`bool`): Root inspector are not outlined.
			is_editable                                (`bool`): If `True` value can be edited using the item widget.
			ignores                           (`list` of `str`): Name of property to not display/edit in the inspector.
			parent           (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is( is_root, bool )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		assert pytools.assertions.type_is_instance_of( value, (type(None), list, pytools.serialization.Serializable) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		if isinstance( value, list ):
			assert pytools.assertions.list_items_type_is( value, (type(None), pytools.serialization.Serializable) )

		self._is_root = is_root
		self._ignores = ignores

		super( InspectorWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, is_root, ignores, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def is_root ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		return self._is_root

	# def is_root ( self )

	# --------------------------------------------------

	@is_root.setter
	def is_root ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		self._is_root = value
		self.update()

	# def is_root ( self, value )

	# --------------------------------------------------

	@property
	def ignores ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		return self._ignores

	# def ignores ( self )

	# --------------------------------------------------

	@ignores.setter
	def ignores ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		return self._ignores

	# def ignores ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def create_item_widget ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		value_type     = type( value )
		value_typename = value_type.__name__

		# None
		if value is None:
			widget = PyQt5.QtWidgets.QLabel( 'None', parent=self )
			return widget

		widget = ItemWidgetFactory.instantiate_widget(
				       type = value_type,
				      value = value,
				is_editable = self.isEnabled() and self.is_editable,
				     parent = self
				)

		# Use the registered ItemWidget if possible
		if widget is not None:
			return widget

		# For serializable objects, use an Inspector
		elif isinstance( value, pytools.serialization.Serializable ):
			widget = InspectorWidget( value, is_root=False, parent=self )
			return widget

		# Error label
		else:
			widget = PyQt5.QtWidgets.QLabel( '{} ?'.format(value_typename), self )
			widget.setStyleSheet( 'color: red;')
			return widget
	
	# def create_item_widget ( self, value )
	
	# --------------------------------------------------

	def create_item_widgets ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )
		
		value          = self.value
		value_type     = type( value )
		value_typename = value_type.__name__

		# None value case
		if value is None:

			if self.is_root:
				return

			else:
				widget = PyQt5.QtWidgets.QLabel( '{} ?'.format(value_typename), self )
				widget.setStyleSheet( 'color: red;' )
				self.layout().addRow( widget )
				return
		
		# If its a list edit each item separatly
		elif isinstance( value, list ) and self.is_not_editable:
			for item in value:
				widget = self.create_item_widget( item )
				if isinstance( widget, ItemWidget ):
					widget.value_changed.connect( self.value_changed )
				self.layout().addRow( widget )
			return


		# Attempt to instantiate an item widget for the value
		widget = ItemWidgetFactory.instantiate_widget(
				       type = value_type,
				      value = value,
				is_editable = self.isEnabled() and self.is_editable,
				     parent = self
				)

		# Check if the type has a dedicated widget
		if widget is not None:
			
			widget.value_changed.connect( self.value_changed )
			self.layout().addRow( widget )
			return
			
		# Otherwise edit each field separatly
		else:
			# Get the names of the fields to edit / display
			fields_names = value_type.__fields_names__
			fields_count = len( fields_names )

			# for field_name in fields_names:
			for field_index in range(fields_count):
				
				# Get the name of the field to edit / display
				field_name  = fields_names[ field_index ]

				# Should it be ignored ?
				if field_name in self._ignores:
					continue

				# Get a pretty name to dispaly the field
				pretty_name = pytools.text.pretty_name( field_name )

				# Get the value of the field
				field_value = getattr( value, field_name )

				# Create a widget to display the field
				widget = self.create_item_widget( field_value )

				# if its an item widget (not an error label)
				if isinstance( widget, ItemWidget ):

					# Listen to value changes
					widget.value_changed.connect( InspectorWidget.FieldSetter(value, field_name, widget) )
					widget.value_changed.connect( self.value_changed )

				# self.layout().addWidget( widget, field_index, 1 )
				self.layout().addRow( pretty_name, widget )

			# for field_name in fields_names
			return

		# if ( value is None )

	# def create_item_widgets ( self )

	# --------------------------------------------------

	def delete_item_widgets ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )
		
		while not self.layout().isEmpty():
		
			item   = self.layout().itemAt( 0 )
			widget = item.widget()
			self.layout().removeWidget( widget )
			widget.deleteLater()
			del widget
			
		# while not self.layout().isEmpty()

	# def delete_item_widgets ( self )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		layout = PyQt5.QtWidgets.QFormLayout( self )
		self.setLayout( layout )

		self.update()

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, InspectorWidget )

		super( InspectorWidget, self ).update()
		
		self.delete_item_widgets()
		self.create_item_widgets()
		# self.repaint()
		
		if not self.is_root:
			self.setStyleSheet(
				'''
				InspectorWidget {
					border-color: rgb( 120, 120, 120 );
					border-style: solid;
					border-width: 1px;
					padding:      2px;
				}
				'''
				)
		else:
			self.setStyleSheet(
				'''
				InspectorWidget {
					border: none;
					padding: 0px;
				}
				'''
				)

		super( InspectorWidget, self ).update()

	# def update ( self )

# class Inspector ( ItemWidget )
