# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .item_widget                    import ItemWidget
from .register_item_widget_attribute import RegisterItemWidgetAttribute

# ##################################################
# ###          CLASS BYTES-ITEM-WIDGET           ###
# ##################################################

@RegisterItemWidgetAttribute( 'bytes' )
@RegisterItemWidgetAttribute( 'bytes_' )
class BytesItemWidget ( ItemWidget ):
	"""
	The class `pyui.inspector.BytesItemWidget` is used to edit / display bytes in the inspector.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=b'', is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pyui.inspector.BoolItemWidget` class.

		Arguments:
			self                         (`pyui.inspector.BoolItemWidget`): Instance to initialize.
			value (`None`/`bool`/`numpy.bool`/`numpy.bool_`/`numpy.bool8`): Bytes value edited / displayed in this item widget.
			is_editable                                           (`bool`): If `True` value can be edited using the item widget.
			parent                      (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )
		assert pytools.assertions.type_in( value, (type(None), bytes, numpy.bytes_) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( BytesItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def text_value ( self ):
		"""
		Text representation of this item widget value (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )
		
		return str( self.value, 'UTF-8' ) if self.value is not None else 'None'

	# def text_value ( self )

	# --------------------------------------------------

	@property
	def lineedit ( self ):
		"""
		Line-Edit used to edit the bytes value of this item widget (`PyQt5.QtWidgets.QLineEdit`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )

		return self._lineedit
	
	# def lineedit ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this bytes item widget.

		Arguments:
			self (`pyui.inspector.BytesItemWidget`): Bytes item widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )

		# Setup the parent class
		super( BytesItemWidget, self ).setup()

		# Create layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Create lineedit
		self._lineedit = PyQt5.QtWidgets.QLineEdit( self.text_value, self )
		self._lineedit.setMinimumSize( 22, BytesItemWidget.height )
		self._lineedit.setMaximumSize( self._lineedit.maximumWidth(), BytesItemWidget.height )
		self._lineedit.textChanged.connect( self.on_text_changed )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._lineedit )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this bytes item widget.

		Arguments:
			self (`pyui.inspector.BytesItemWidget`): Bytes item widget to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )

		# Update the lineedit
		self._lineedit.setText( self.text_value )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )

		# Update the parent class
		super( BytesItemWidget, self ).update()

	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		"""
		Slot invoked when the value of the bytes item widget is edited.

		Arguments:
			self (`pyui.inspector.BytesItemWidget`): Bytes item widget.
			text                            (`str`): The new bytes value.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesItemWidget )
		assert pytools.assertions.type_is( text, str )

		# Save the value
		self._value = bytes( text, 'UTF-8' )

		# Notify listeners that value changed
		self.value_changed.emit()
	
	# def on_text_changed ( self, text )

# class BytesItemWidget ( ItemWidget )
