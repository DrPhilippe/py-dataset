# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .application_menu import ApplicationMenu

# ##################################################
# ###             CLASS MENU-BAR                 ###
# ##################################################

class MenuBar ( PyQt5.QtWidgets.QMenuBar ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( MenuBar, self ).__init__( parent )

		self.setup()

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def application_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )

		return self._application_menu
	
	# def application_menu ( self )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )

		# Create the Application menu
		self._application_menu = ApplicationMenu( self )
		self.addMenu( self._application_menu )

	# def __init__ ( self )

# class MenuBar ( PyQt5.QtWidgets.QMenuBar )