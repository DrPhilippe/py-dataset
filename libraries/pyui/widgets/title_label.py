# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

class TitleLabel ( PyQt5.QtWidgets.QLabel ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, text='', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, TitleLabel )
		assert pytools.assertions.type_is( text, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( TitleLabel, self ).__init__( text, parent )
	
	# def __init__ ( self, text, parent )
	
# class TitleLabel ( PyQt5.QtWidgets.QLabel )