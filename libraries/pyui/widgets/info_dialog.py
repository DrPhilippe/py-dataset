# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCAL
from .dialog import Dialog

# ##################################################
# ###                CLASS DIALOG                ###
# ##################################################

class InfoDialog ( Dialog ):
	"""
	Information dialog windows.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='Information', text='', text_ok='Ok', parent=None ):
		"""
		Initialize a new isntance of the `pyui.dialogs.InfoDialog` class.

		Arguments:
			self          (`pyui.widgets.InfoDialog`): Instance to intialize.
			title                             (`str`): Title of the dialog window.
			text                              (`str`): text displayed in the dialog window.
			text_ok                           (`str`): Text displayed on the button to accept and close the dialog.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Set the text
		self._text = text

		# init the dialog
		super( InfoDialog, self ).__init__( title, text_ok, 'Cancel', parent )

	# def __init__ ( self, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def text_label ( self ):
		"""
		Label ised to display the text in this dialog (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )

		return self._text_label

	# def text_label ( self )

	# --------------------------------------------------

	@property
	def text ( self ):
		"""
		Text displayed in this dialog (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )

		return self._text

	# def text ( self )

	# --------------------------------------------------

	@text.setter
	def text ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )
		assert pytools.assertions.type_is( value, str )

		self._text = value
		self._label.setText( value )

	# def text ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self, title, text_ok, text_cancel ):
		"""
		Setup this dialog window.

		Arguments:
			self (`pyui.widgets.InfoDialog`): Dialog window to setup.
			title                    (`str`): Title of the dialog window.
			text_ox                  (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel              (`str`): Text displayed on the button to cancel and close the dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )
		
		# Set the title
		self.setWindowTitle( title )

		# Create the layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		self.setLayout( layout )

		# Setup the contents of the dialog
		self.setup_form()

		# Spacer
		spacer = PyQt5.QtWidgets.QWidget( self )
		spacer.setSizePolicy(
			PyQt5.QtWidgets.QSizePolicy.Expanding,
			PyQt5.QtWidgets.QSizePolicy.Expanding
			)
		layout.addWidget( spacer )

		# Setup the buttons of the dialog
		self.setup_buttons( text_ok, text_cancel )

	# def setup ( self )

	# --------------------------------------------------

	def setup_form ( self ):
		"""
		Setup the contents of this info dialog window.

		Arguments:
			self (`pyui.widgets.InfoDialog`): Info dialog window to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )
	
		self._text_label = PyQt5.QtWidgets.QLabel( self.text, parent=self )
		self.layout().addWidget( self._text_label )

	# def setup_form ( self )

	# --------------------------------------------------

	def setup_buttons ( self, text_ok, text_cancel ):
		"""
		Setup the buttons at the bottom of this dialog window.
		
		Arguments:
			self (`pyui.widgets.InfoDialog`): InfoDialog window to setup.
			text_ox                  (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel              (`str`): Text displayed on the button to cancel and close the dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, InfoDialog )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )

		button_layout = PyQt5.QtWidgets.QHBoxLayout()
		self.layout().addLayout( button_layout )

		# Spacer
		spacer = PyQt5.QtWidgets.QWidget( self )
		spacer.setSizePolicy(
			PyQt5.QtWidgets.QSizePolicy.Expanding,
			PyQt5.QtWidgets.QSizePolicy.Expanding
			)
		button_layout.addWidget( spacer )

		# Create the open button
		self._ok_button = PyQt5.QtWidgets.QPushButton( self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_DialogApplyButton), text_ok, self )
		self._ok_button.clicked.connect( self.on_ok_button_clicked )
		button_layout.addWidget( self._ok_button )

	# def setup_buttons ( self )
	
# class InfoDialog ( Dialog )