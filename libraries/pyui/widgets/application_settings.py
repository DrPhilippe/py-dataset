# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import PyQt5.QtCore

# INTERNALS
import pytools.assertions

# LOCALS
from .application import Application

# ##################################################
# ###         CLASS APPLICATION-SETTINGS         ###
# ##################################################

class ApplicationSettings ( PyQt5.QtCore.QSettings ):
	"""
	Persistant settings accessibility class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self ):
		"""
		Initializes a new instance of the `pyui.widgets.ApplicationSettings` class.

		Arguments:
			self  (`pyui.widgets.ApplicationSettings`): Instance to initialize.
		"""
		assert pytools.assertions.type_is_instance_of( self, ApplicationSettings )

		# Get the application
		application = Application.instance()

		# init the parent class
		super( ApplicationSettings, self ).__init__(
			PyQt5.QtCore.QSettings.IniFormat,
			PyQt5.QtCore.QSettings.UserScope,
			application.organizationName(),
			application.applicationName()
			)

	# def __init__ ( self )

# class ApplicationSettings ( PyQt5.QtCore.QSettings )