# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .application import Application

# ##################################################
# ###           CLASS APPLICATION-MENU           ###
# ##################################################

class ApplicationMenu ( PyQt5.QtWidgets.QMenu ):
	"""
	Application menu.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		"""
		Initializes a new instance of the `pyui.ApplicationMenu` class.

		Arguments:
			self     (`pyui.widgets.ApplicationMenu`): Instance to initialize.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ApplicationMenu )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Init the parent class
		super( ApplicationMenu, self ).__init__(
			Application.instance().applicationName(),
			parent
			)

		# Setup this app
		self.setup()

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def exit_action ( self ):
		"""
		Action used to exit this application (`PyQt5.QtWidgets.QAction`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ApplicationMenu )

		return self._exit_action
	
	# def exit_action ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this `pyui.widgets.ApplicationMenu`.

		Arguments:
			self (`pyui.widgets.ApplicationMenu`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ApplicationMenu )
		
		# Exit action
		self._exit_action = self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_BrowserStop ),
			'Exit',
			self.on_exit_action_clicked,
			PyQt5.QtGui.QKeySequence( 'Ctrl+Q' )
			)
		
	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_exit_action_clicked ( self ):
		"""
		Slot invoked to exit the application.

		Arguments:
			self (`pyui.widgets.ApplicationMenu`): Application menu.
		"""
		assert pytools.assertions.type_is_instance_of( self, ApplicationMenu )

		Application.instance().closeAllWindows()

	# def on_exit_action_clicked ( self )

# class ApplicationMenu ( QtWidgets.QMenu )