# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 CLASS DOCK                 ###
# ##################################################

class Dock ( PyQt5.QtWidgets.QDockWidget ):
	"""
	Base class for custom dock widget.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title, parent=None ):
		"""
		Initializes a new instance of the `pyviewer.ux.Dock` class.

		Arguments:
			self           (`pydataset.widgets.Dock`): Instance to initialize.
			title                             (`str`): Title of the dock widget.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dock )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is_instance_of( parent, PyQt5.QtWidgets.QWidget )

		# Initialize the dock widget
		super( Dock, self ).__init__( title, parent )
		
		# Setup the UX
		self.setup()

	# def __init__ ( parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this dock widget.

		Arguments:
			self (`pyui.widgets.Dock`): Dock widget to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dock )
		
		self.setFeatures( PyQt5.QtWidgets.QDockWidget.DockWidgetMovable | PyQt5.QtWidgets.QDockWidget.DockWidgetFloatable )

	# def setup ( self )

# class Dock ( PyQt5.QtWidgets.QDockWidget )