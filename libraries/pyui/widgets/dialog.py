# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# ##################################################
# ###                CLASS DIALOG                ###
# ##################################################

class Dialog ( PyQt5.QtWidgets.QDialog ):
	"""
	Base class for custom dialog windows.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='dialog', text_ok='Ok', text_cancel='Cancel', parent=None ):
		"""
		Initialize a new isntance of the `pyui.widgets.Dialog` class.

		Arguments:
			self              (`pyui.widgets.Dialog`): Instance to intialize.
			title                             (`str`): Title of the dialog window.
			text_ok                           (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel                       (`str`): Text displayed on the button to cancel and close the dialog.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# init the dialog
		super( Dialog, self ).__init__( parent, PyQt5.QtCore.Qt.Dialog )

		# Create the UI
		self.setup( title, text_ok, text_cancel )

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def ok_button ( self ):
		"""
		Button used to accept this dialog (`PyQt5.QtWidgets.QPushButton`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )

		return self._ok_button

	# def ok_button ( self )

	# --------------------------------------------------

	@property
	def cancel_button ( self ):
		"""
		Button used to cancel this dialog (`PyQt5.QtWidgets.QPushButton`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )

		return self._cancel_button

	# def cancel_button ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self, title, text_ok, text_cancel ):
		"""
		Setup this dialog window.

		Arguments:
			self (`pyui.widgets.Dialog`): Dialog window to setup.
			title                (`str`): Title of the dialog window.
			text_ox              (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel          (`str`): Text displayed on the button to cancel and close the dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )
		
		# Set the title
		self.setWindowTitle( title )

		# Create the layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		self.setLayout( layout )

		# Setup the contents of the dialog
		self.setup_form()

		# Setup the buttons of the dialog
		self.setup_buttons( text_ok, text_cancel )

	# def setup ( self )

	# --------------------------------------------------

	def setup_form ( self ):
		"""
		Setup the contents of this dialog window.

		Arguments:
			self (`pyui.widgets.Dialog`): Dialog window to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )
	
		pass

	# def setup_form ( self )

	# --------------------------------------------------

	def setup_buttons ( self, text_ok, text_cancel ):
		"""
		Setup the buttons at the bottom of this dialog window.
		
		Arguments:
			self (`pyui.widgets.Dialog`): Dialog window to setup.
			text_ox              (`str`): Text displayed on the button to accept and close the dialog.
			text_cancel          (`str`): Text displayed on the button to cancel and close the dialog.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )

		button_layout = PyQt5.QtWidgets.QHBoxLayout()
		self.layout().addLayout( button_layout, 0 )

		# Spacer
		spacer = PyQt5.QtWidgets.QWidget( self )
		spacer.setSizePolicy(
			PyQt5.QtWidgets.QSizePolicy.Expanding,
			PyQt5.QtWidgets.QSizePolicy.Expanding
			)
		button_layout.addWidget( spacer )

		# Create the open button
		self._ok_button = PyQt5.QtWidgets.QPushButton( self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_DialogApplyButton), text_ok, self )
		self._ok_button.clicked.connect( self.on_ok_button_clicked )
		button_layout.addWidget( self._ok_button )

		# Create the cancel button
		self._cancel_button = PyQt5.QtWidgets.QPushButton( self.style().standardIcon(PyQt5.QtWidgets.QStyle.SP_DialogCancelButton), text_cancel, self )
		self._cancel_button.clicked.connect( self.on_cancel_button_clicked )
		self._cancel_button.setDefault( True )
		button_layout.addWidget( self._cancel_button )

	# def setup_buttons ( self )
	
	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_ok_button_clicked ( self ):
		"""
		Slot invoked when the ok button of this dialog is clicked.

		Arguments:
			self (`pyui.widgets.Dialog`): Dialog owning the button.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )

		self.accept()

	# def on_ok_button_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_cancel_button_clicked ( self ):
		"""
		Slot invoked when the cancel button of this dialog is clicked.

		Arguments:
			self (`pyui.widgets.Dialog`): Dialog owning the button.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dialog )

		self.reject()

	# def on_cancel_button_clicked ( self )

# class Dialog ( PyQt5.QtWidgets.QDialog )