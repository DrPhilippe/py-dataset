# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .assets import Assets

# ##################################################
# ###             CLASS APPLICATION              ###
# ##################################################

class Application ( PyQt5.QtWidgets.QApplication ):
	"""
	Base class for custom applications.
	"""
	
	# ##################################################
	# ###                CLASS FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Default assets directory (`pytools.path.DirectoryPath`).
	"""
	default_assets_directory_path = pytools.path.FilePath( __file__ ).parent().parent() + pytools.path.DirectoryPath( 'assets' )
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, argv, organization_name='ITECA', application_name='PyDataset', assets_directory_path=default_assets_directory_path ):
		"""
		Initialize a new instance of the `pyui.Application` class.

		Arguments:
			self                         (`pyui.widgets.Application`): Instance to intialize.
			argv                                    (`list` of `str`): Program arguments.
			organization_name                                 (`str`): Name of the organization that develloped the application.
			application_name                                  (`str`): Name of the application.
			assets_directory_path (`str`/pytools.path.DirectoryPath`): Assets directory path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )
		assert pytools.assertions.type_is( argv, list )
		assert pytools.assertions.list_items_type_is( argv, str )
		assert pytools.assertions.type_is( organization_name, str )
		assert pytools.assertions.type_is( application_name, str )

		# Initialize the application
		super( Application, self ).__init__( argv )
		self.setOrganizationName( organization_name )
		self.setApplicationName( application_name )

		# Initialize properties
		self._assets      = Assets( assets_directory_path ) if assets_directory_path else Assets()
		self._main_window = None

		# Setup the application
		self.setup()

	# def __init__ ( self, argv )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def assets ( self ):
		"""
		Assets of this application (`pyui.widgets.Assets`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )

		return self._assets

	# def assets ( self )

	# --------------------------------------------------

	@property
	def main_window ( self ):
		"""
		Main window of this application (`None`/`PyQt5.QtWidgets.QWidget`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )

		return self._main_window

	# def main_window ( self )

	# --------------------------------------------------

	@main_window.setter
	def main_window ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Application )
		assert pytools.assertions.type_is_instance_of( value, (type(None), PyQt5.QtWidgets.QWidget) )

		self._main_window = value

	# def main_window ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def exec ( self ):
		"""
		Executes this application.

		Arguments:
			self (`pyui.widgets.Application`): Application to execute.
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )
		
		# Check
		if self.main_window:

			# Show the mainWindow
			self.main_window.show()

			# exec the QApplication
			super( Application, self ).exec()

		# if self.get_main_window() is not None

	# def exec ( self )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this application.
	
		Arguments:
			self (`pyui.widgets.Application`): Application to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )
		
		# Application style
		style = PyQt5.QtWidgets.QStyleFactory.create( 'fusion' )
		self.setStyle( style )

		# Application stylesheet
		stylesheet = self.assets.text( 'stylesheet.css' )
		self.setStyleSheet( stylesheet )

		# Application icon
		icon = self.assets.icon( 'icon_application.png' )
		self.setWindowIcon( icon )

	# def setup ( self )
	
# class Application ( PyQt5.QtWidgets.QApplication )