# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtGui

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###                CLASS ASSETS                ###
# ##################################################

class Assets:
	"""
	Assets manager
	"""

	# ##################################################
	# ###                CLASS FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Default assets directory (`pytools.path.DirectoryPath`).
	"""
	default_directory_path = pytools.path.FilePath( __file__ ).parent() + pytools.path.DirectoryPath( 'assets' )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, directory_path=default_directory_path ):
		"""
		Initializes a new instance of the `pyui.Assets` class.

		Arguments:
			self                          (`pyui.Assets`): Assets manager instance.
			filepath (`str`/`pytools.path.DirectoryPath`): The path to the assets directory.
		"""
		assert pytools.assertions.type_is( self, Assets )
		assert pytools.assertions.type_is_instance_of( directory_path, (str, pytools.path.DirectoryPath) )

		if isinstance( directory_path, str ):
			directory_path = pytools.path.DirectoryPath( directory_path )

		print( 'assets:', directory_path )
		
		self._directory_path = directory_path

	# def __init__ ( self, directory_path )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def icon ( self, filepath ):
		"""
		Load the icon with the given filepath in the assets directory.

		Arguments:
			self                     (`pyui.Assets`): Assets manager instance.
			filepath (`str`/`pytools.path.FilePath`): The path to the icon in the assets directory.

		Returns:
			`PyQt5.QtGui.QIcon`: The icon.
		"""
		assert pytools.assertions.type_is( self, Assets )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath(filepath)

		icon_path = self._directory_path + filepath

		return PyQt5.QtGui.QIcon( str(icon_path) )

	# def icon ( self, filepath )

	# --------------------------------------------------

	def pixmap ( self, filepath ):
		"""
		Load the pixmap with the given filepath in the assets directory.

		Arguments:
			self                     (`pyui.Assets`): Assets manager instance.
			filepath (`str`/`pytools.path.FilePath`): The path to the pixmap in the resources directory.

		Returns:
			`PyQt5.QtGui.QPixmap`: The pixmap.
		"""
		assert pytools.assertions.type_is( self, Assets )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath(filepath)

		icon_path = self._directory_path + filepath

		return PyQt5.QtGui.QPixmap( str(icon_path) )

	# def pixmap ( self, filepath )

	# --------------------------------------------------

	def text ( self, filepath ):
		"""
		Load the text with the given filepath in the assets directory.

		Arguments:
			self                     (`pyui.Assets`): Assets manager instance.
			filepath (`str`/`pytools.path.FilePath`): The path to the text in the resources directory.

		Returns:
			`str`: The text.
		"""
		assert pytools.assertions.type_is( self, Assets )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath(filepath)

		filepath = self._directory_path + filepath
		
		return filepath.read_txt()

	# def text ( self, filepath )

# class Assets: