# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###      CLASS DATASET-IMPORTER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetImporterSettings',
	   namespace = 'pybop',
	fields_names = [
		# SRC
		'dataset_directory_path',
		# Mode
		'mode',
		# Ref
		'cameras_group_name',
		'objects_group_name',
		'scenes_group_name',
		# objects
		'category_index_feature_name',
		'one_hot_category_feature_name',
		'mesh_feature_name',
		'mesh_diameter_feature_name',
		'ignore_fine_models',
		'category_names',
		'category_name_feature_name',
		# Camera
		'camera_matrix_feature_name',
		'depth_scale_feature_name',
		'image_size_feature_name',
		# image features
		'image_feature_name',
		'depth_feature_name',
		# instance features
		'mask_feature_name',
		'visible_mask_feature_name',
		'rotation_matrix_feature_name',
		'translation_vector_feature_name',
		# info features
		'bounding_rectangle_feature_name',
		'visible_bounding_rectangle_feature_name',
		'number_of_pixels_feature_name',
		'number_of_valid_pixels_feature_name',
		'number_of_visible_pixels_feature_name',
		'visibility_feature_name',
		]
	)
class DatasetImporterSettings ( pydataset.io.ImporterSettings ):
	"""
	BOP Dataset scene importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		# SRC
		dataset_directory_path,
		# DST
		dst_group_url,
		# MODE
		mode = 'create',
		# GROUPS
		cameras_group_name = 'cameras',
		objects_group_name = 'objects',
		scenes_group_name = 'scenes',
		# Objects
		category_index_feature_name = 'category_index',
		one_hot_category_feature_name = 'one_hot_category',
		mesh_feature_name = 'mesh',
		mesh_diameter_feature_name = 'diameter',
		ignore_fine_models = True,
		category_names = [],
		category_name_feature_name = 'category_name',
		# Camera
		camera_matrix_feature_name = 'K',
		depth_scale_feature_name = 'depth_scale',
		image_size_feature_name = 'size',
		# image features
		image_feature_name = 'image',
		depth_feature_name = 'depth',
		# instance features
		mask_feature_name = 'mask',
		visible_mask_feature_name = 'visible_mask',
		rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		# info features
		bounding_rectangle_feature_name = 'bounding_rectangle',
		visible_bounding_rectangle_feature_name = 'visible_bounding_rectangle',
		number_of_pixels_feature_name = 'number_of_pixels',
		number_of_valid_pixels_feature_name = 'number_of_valid_pixels',
		number_of_visible_pixels_feature_name = 'number_of_visible_pixels',
		visibility_feature_name = 'visibility',
		# other
		name = 'pybop-dataset-importer'
		):
		"""
		Initializes a new instance of the `pybop.DatasetImporterSettings` class.

		Arguments:
			self                      (`pybop.DatasetImporterSettings`): Instance to initialize.
			dataset_directory_path (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the dataset to import.
			mode                                                (`str`): Importation mode 'create' or 'combine'. 
			cameras_group_name                                  (`str`): Name of the group where to save the cameras informations.
			objects_group_name                                  (`str`): Name of the group where to save the objects models.
			scenes_group_name                                   (`str`): Name of the group where to save the scenes.
			category_index_feature_name                         (`str`): Name of the feature in which to save the category index.
			one_hot_category_feature_name                       (`str`): Name of the feature in which to save the one hot category.
			mesh_feature_name                                   (`str`): Name of the feature in which to save the mesh.
			mesh_diameter_feature_name                          (`str`): Name of the feature in which to save the model diameter.
			ignore_fine_models                                 (`bool`): If `True` fine models are not imported.
			category_names                           (`list` of `str`s): Names of the objects. 
			category_name_feature_name                          (`str`): Name of the feature in which to save the object name.
			camera_matrix_feature_name                          (`str`): Name of the feature in which to save the camera parameters matrix.
			depth_scale_feature_name                            (`str`): Name of the feature in which to save the camera depth scale.
			image_size_feature_name                             (`str`): Name of the feature in which to save the image size.
			image_feature_name                                  (`str`): Name of the feature in which to save the image.
			depth_feature_name                                  (`str`): Name of the feature in which to save the depth.
			mask_feature_name                                   (`str`): Name of the feature in which to save the mask.
			visible_mask_feature_name                           (`str`): Name of the feature in which to save the visible mask.
			rotation_matrix_feature_name                        (`str`): Name of the feature in which to save the model view rotation matrix.
			translation_vector_feature_name                     (`str`): Name of the feature in which to save the model view translation vector.
			bounding_rectangle_feature_name                     (`str`): Name of the feature in which to save the bounding rectangle.
			visible_bounding_rectangle_feature_name             (`str`): Name of the feature in which to save the visible bounding rectangle.
			number_of_pixels_feature_name                       (`str`): Name of the feature in which to save the number of pixels occupied by an object.
			number_of_valid_pixels_feature_name                 (`str`): Name of the feature in which to save the number of valid pixels occupied by an object.
			number_of_visible_pixels_feature_name               (`str`): Name of the feature in which to save the number of visible pixels occupied by an object.
			visibility_feature_name                             (`str`): Name of the feature in which to save the visiblity of an object.
			name                                                (`str`): Name of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( dataset_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( mode, str )
		assert pytools.assertions.value_in( mode, ['create', 'combine'] )
		assert pytools.assertions.type_is( cameras_group_name, str )
		assert pytools.assertions.type_is( objects_group_name, str )
		assert pytools.assertions.type_is( scenes_group_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( mesh_feature_name, str )
		assert pytools.assertions.type_is( mesh_diameter_feature_name, str )
		assert pytools.assertions.type_is( ignore_fine_models, bool )
		assert pytools.assertions.type_is( category_names, list )
		assert pytools.assertions.list_items_type_is( category_names, str )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( depth_scale_feature_name, str )
		assert pytools.assertions.type_is( image_size_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( depth_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( visible_mask_feature_name, str )
		assert pytools.assertions.type_is( rotation_matrix_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( visible_bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( number_of_pixels_feature_name, str )
		assert pytools.assertions.type_is( number_of_valid_pixels_feature_name, str )
		assert pytools.assertions.type_is( number_of_visible_pixels_feature_name, str )
		assert pytools.assertions.type_is( visibility_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( DatasetImporterSettings, self ).__init__( dst_group_url, name )

		self._dataset_directory_path                  = pytools.path.DirectoryPath.ensure( dataset_directory_path )
		self._mode                                    = mode
		self._cameras_group_name                      = cameras_group_name
		self._objects_group_name                      = objects_group_name
		self._scenes_group_name                       = scenes_group_name
		self._category_index_feature_name             = category_index_feature_name
		self._one_hot_category_feature_name           = one_hot_category_feature_name
		self._mesh_feature_name                       = mesh_feature_name
		self._mesh_diameter_feature_name              = mesh_diameter_feature_name
		self._ignore_fine_models                      = ignore_fine_models
		self._category_names                          = copy.deepcopy( category_names )
		self._category_name_feature_name              = category_name_feature_name
		self._camera_matrix_feature_name              = camera_matrix_feature_name
		self._depth_scale_feature_name                = depth_scale_feature_name
		self._image_size_feature_name                 = image_size_feature_name
		self._image_feature_name                      = image_feature_name
		self._depth_feature_name                      = depth_feature_name
		self._mask_feature_name                       = mask_feature_name
		self._visible_mask_feature_name               = visible_mask_feature_name
		self._rotation_matrix_feature_name            = rotation_matrix_feature_name
		self._translation_vector_feature_name         = translation_vector_feature_name
		self._bounding_rectangle_feature_name         = bounding_rectangle_feature_name
		self._visible_bounding_rectangle_feature_name = visible_bounding_rectangle_feature_name
		self._number_of_pixels_feature_name           = number_of_pixels_feature_name
		self._number_of_valid_pixels_feature_name     = number_of_valid_pixels_feature_name
		self._number_of_visible_pixels_feature_name   = number_of_visible_pixels_feature_name
		self._visibility_feature_name                 = visibility_feature_name

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def dataset_directory_path ( self ):
		"""
		Path to the directory containing the dataset to import (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )

		return self._dataset_directory_path

	# def dataset_directory_path ( self )
	
	# --------------------------------------------------

	@dataset_directory_path.setter
	def dataset_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._dataset_directory_path = pytools.path.DirectoryPath.ensure( value )
		
	# def dataset_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def mode ( self ):
		"""
		Importation mode: 'create' or 'combine' (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._mode
	
	# def mode ( self )

	# --------------------------------------------------

	@mode.setter
	def mode ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['create', 'combine'] )
	
		self._mode = value
	
	# def mode ( self, value )
	
	# --------------------------------------------------

	@property
	def cameras_group_name ( self ):
		"""
		Name of the group where to save the cameras informations (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )

		return self._cameras_group_name

	# def cameras_group_name ( self )
	
	# --------------------------------------------------

	@cameras_group_name.setter
	def cameras_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, str )

		self._cameras_group_name = value
		
	# def cameras_group_name ( self, value )

	# --------------------------------------------------

	@property
	def objects_group_name ( self ):
		"""
		Name of the group where to save the objects models (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )

		return self._objects_group_name

	# def objects_group_name ( self )
	
	# --------------------------------------------------

	@objects_group_name.setter
	def objects_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, str )

		self._objects_group_name = value
		
	# def objects_group_name ( self, value )

	# --------------------------------------------------

	@property
	def scenes_group_name ( self ):
		"""
		Name of the group where to save the scenes models (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )

		return self._scenes_group_name

	# def scenes_group_name ( self )
	
	# --------------------------------------------------

	@scenes_group_name.setter
	def scenes_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, str )

		self._scenes_group_name = value
		
	# def scenes_group_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature in which to save the category index (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._category_index_feature_name
	
	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature in which to save the category index (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )

	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_feature_name ( self ):
		"""
		Name of the feature in which to save the ply mesh (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._mesh_feature_name
	
	# def mesh_feature_name ( self )

	# --------------------------------------------------

	@mesh_feature_name.setter
	def mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_feature_name = value
	
	# def mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_diameter_feature_name ( self ):
		"""
		Name of the feature in which to save the model diamter (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._mesh_diameter_feature_name
	
	# def mesh_diameter_feature_name ( self )

	# --------------------------------------------------

	@mesh_diameter_feature_name.setter
	def mesh_diameter_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_diameter_feature_name = value
	
	# def mesh_diameter_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def ignore_fine_models ( self ):
		"""
		If `True` fine models are not imported (`bool`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._ignore_fine_models
	
	# def ignore_fine_models ( self )

	# --------------------------------------------------

	@ignore_fine_models.setter
	def ignore_fine_models ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._ignore_fine_models = value
	
	# def ignore_fine_models ( self, value )

	# --------------------------------------------------

	@property
	def category_names ( self ):
		"""
		Names of the objects (`list` of `str`s) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._category_names
	
	# def category_names ( self )

	# --------------------------------------------------

	@category_names.setter
	def category_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )
	
		self._category_names = copy.deepcopy( value )
	
	# def category_names ( self, value )

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature in which to save the object name (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature in which to save the camera parameters matrix (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def depth_scale_feature_name ( self ):
		"""
		Name of the feature in which to save the camera depth scale (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._depth_scale_feature_name
	
	# def depth_scale_feature_name ( self )

	# --------------------------------------------------

	@depth_scale_feature_name.setter
	def depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_scale_feature_name = value
	
	# def depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def image_size_feature_name ( self ):
		"""
		Name of the feature in which to save the image size (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._image_size_feature_name
	
	# def image_size_feature_name ( self )

	# --------------------------------------------------

	@image_size_feature_name.setter
	def image_size_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._image_size_feature_name = value
	
	# def image_size_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature in which to save the image (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		"""
		Name of the feature in which to save the depth image (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._depth_feature_name
	
	# def depth_feature_name ( self )

	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_feature_name = value
	
	# def depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature in which to save the mask (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._mask_feature_name
	
	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visible_mask_feature_name ( self ):
		"""
		Name of the feature in which to save the visible mask (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._visible_mask_feature_name
	
	# def visible_mask_feature_name ( self )

	# --------------------------------------------------

	@visible_mask_feature_name.setter
	def visible_mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visible_mask_feature_name = value
	
	# def visible_mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_matrix_feature_name ( self ):
		"""
		Name of the feature in which to save the model view rotation matrix (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._rotation_matrix_feature_name
	
	# def rotation_matrix_feature_name ( self )

	# --------------------------------------------------

	@rotation_matrix_feature_name.setter
	def rotation_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_matrix_feature_name = value
	
	# def rotation_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		"""
		Name of the feature in which to save the model view translation vector (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._translation_vector_feature_name
	
	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_vector_feature_name = value
	
	# def translation_vector_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature in which to save the bounding rectangle (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._bounding_rectangle_feature_name
	
	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visible_bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature in which to save the model visible bounding rectangle (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._visible_bounding_rectangle_feature_name
	
	# def visible_bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@visible_bounding_rectangle_feature_name.setter
	def visible_bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visible_bounding_rectangle_feature_name = value
	
	# def visible_bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def number_of_pixels_feature_name ( self ):
		"""
		Name of the feature in which to save the number of pixel occupied by an object (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._number_of_pixels_feature_name
	
	# def number_of_pixels_feature_name ( self )

	# --------------------------------------------------

	@number_of_pixels_feature_name.setter
	def number_of_pixels_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._number_of_pixels_feature_name = value
	
	# def number_of_pixels_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_valid_pixels_feature_name ( self ):
		"""
		Name of the feature in which to save the number of valid pixel occupied by an object (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._number_of_valid_pixels_feature_name
	
	# def number_of_valid_pixels_feature_name ( self )

	# --------------------------------------------------

	@number_of_valid_pixels_feature_name.setter
	def number_of_valid_pixels_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._number_of_valid_pixels_feature_name = value
	
	# def number_of_valid_pixels_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_visible_pixels_feature_name ( self ):
		"""
		Name of the feature in which to save the number of vivible pixel occupied by an object (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._number_of_visible_pixels_feature_name
	
	# def number_of_visible_pixels_feature_name ( self )

	# --------------------------------------------------

	@number_of_visible_pixels_feature_name.setter
	def number_of_visible_pixels_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._number_of_visible_pixels_feature_name = value
	
	
	# def number_of_visible_pixels_feature_name ( self, value )
	# --------------------------------------------------

	@property
	def visibility_feature_name ( self ):
		"""
		Name of the feature in which to save the vivibility of an object (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
	
		return self._visibility_feature_name
	
	# def visibility_feature_name ( self )

	# --------------------------------------------------

	@visibility_feature_name.setter
	def visibility_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visibility_feature_name = value
	
	# def visibility_feature_name ( self, value )

# class DatasetImporterSettings ( pydataset.io.ImporterSettings )