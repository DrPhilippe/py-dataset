# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import time

# INTERNALS
import pydataset
import pytools

# LOCALS
from .dataset_importer_settings import DatasetImporterSettings
from .scene_importer            import SceneImporter
from .scene_importer_settings   import SceneImporterSettings

# ##################################################
# ###            CLASS DATASET-IMPORTER            ###
# ##################################################

class DatasetImporter ( pydataset.io.Importer ):
	"""
	Impors BOP datasets.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new isntance of the `pybop.DatasetImporter` class.

		Arguments:
			self                            (`pybop.DatasetImporter`): Importer to initialize.
			settings                (`pybop.DatasetImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporter )
		assert pytools.assertions.type_is_instance_of( settings, DatasetImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( DatasetImporter, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run importation.

		Arguments:
			self (`pybop.DatasetImporter`): Importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetImporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Log settings
		if self.logger:
			self.logger.log( 'BOP DATASET IMPORTER' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			for field_name in DatasetImporterSettings.__fields_names__:
				self.logger.log( "    {:40s}: '{}'", field_name, getattr(self.settings, field_name) )
			self.logger.end_line()
			self.logger.log( 'SCANNING FILES AND DIRECTORIES:' )
			self.logger.flush()

		# Child tasks
		camera_files       = []
		models_directories = []
		splits_directories = []

		# Iterate over files ans directories of the dataset
		for path in self.settings.dataset_directory_path.list_contents():

			# Directory case
			if path.is_a_directory():

				# Ignore the archives directory
				if path.name() == 'archives':
					# Log
					if self.logger:
						self.logger.log( '    Ignoring archives directory:         {}', path )

				# Is it a models directory
				elif path.name().startswith( 'models' ):
					# Log
					if self.logger:
						self.logger.log( '    Found models directory:              {}', path )
					# Keep
					models_directories.append( path )

				# Otherwise try to import a scene
				else:
					# Log
					if self.logger:
						self.logger.log( '    Found a split directory:             {}', path )
					# Keep
					splits_directories.append( path )

			# Is it a file
			elif path.is_a_file():

				# Camera parameters file ?
				if path.filename().startswith( 'camera' ) and path.extension() == '.json':
					# Log
					if self.logger:
						self.logger.log( '    Found camera parameters file:        {}', path )
					# Keep
					camera_files.append( path )

				# Dataset informations ?
				elif path.name() == 'dataset_info.md':
					# Log
					if self.logger:
						self.logger.log( '    Ignoring dataset informations:       {}', path )

				# Test targets informations ?
				elif path.name() == 'test_targets_bop19.json':
					# Log
					if self.logger:
						self.logger.log( '    Found BOP 2019 testing targets file: {}', path )

				# Error
				else:
					# Log
					if self.logger:
						self.logger.log( '    Found a file that should not exist:  {}', path )
					raise IOError( "The file '{}' should not exist".format(path) )

			# Error
			else:
				# Log
				if self.logger:
					self.logger.log( '    Found an unknown path type item:     {}', path )
				raise IOError( "Could not figure out what '{}' is".format(path) )

		# for path in self.settings.dataset_directory_path.list_contents():
		
		# Log
		if self.logger:
			self.logger.log( 'DONE SCANNING' )
			self.logger.end_line()
			self.logger.flush()

		# Create destination groups
		if self.settings.mode == 'create':
			cameras_group = self.dst_group.create_group( self.settings.cameras_group_name, auto_update=True )
			objects_group = self.dst_group.create_group( self.settings.objects_group_name, auto_update=True )
			scenes_group  = self.dst_group.create_group( self.settings.scenes_group_name, auto_update=True )
		else:
			cameras_group = self.dst_group.get_group( self.settings.cameras_group_name )
			objects_group = self.dst_group.get_group( self.settings.objects_group_name )
			scenes_group  = self.dst_group.get_group( self.settings.scenes_group_name )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING CAMERAS:' )
			self.logger.end_line()
			self.logger.flush()

		# Import cameras
		for camera_file in camera_files:
			self.import_camera( camera_file, cameras_group )
		
		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING CAMERAS' )
			self.logger.end_line()
			self.logger.log( 'IMPORTING MODELS:' )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0.01 )

		# Import models
		for models_directory in models_directories:
			self.import_models( models_directory, objects_group )
		
		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING MODELS' )
			self.logger.end_line()
			self.logger.log( 'IMPORTING SPLITS:' )
			self.logger.flush()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0.02 )

		self.import_scenes( splits_directories, scenes_group )
		
		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING SPLITS' )
			self.logger.end_line()
			self.logger.log( 'DONE' )
			self.logger.flush()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )
	
	# def run ( self ):

	# --------------------------------------------------

	def import_camera ( self, camera_file, cameras_group ):
		assert pytools.assertions.type_is_instance_of( self, DatasetImporter )
		assert pytools.assertions.type_is_instance_of( camera_file, pytools.path.FilePath )
		assert pytools.assertions.true( camera_file.is_a_file() )
		assert pytools.assertions.true( camera_file.filename().startswith( 'camera' ) )
		assert pytools.assertions.equal( camera_file.extension(), '.json' )
		assert pytools.assertions.type_is_instance_of( cameras_group, pydataset.dataset.Group )
		
		# Log
		if self.logger:
			self.logger.log( '    IMPORTING CAMERA: {}', camera_file )
			self.logger.flush()

		# Figure out the name of the camera
		filename = camera_file.filename()
		if '_' in filename:
			camera_name = filename.split( '_' )[ 1 ]
		else:
			camera_name = 'default'

		# Log
		if self.logger:
			self.logger.log( '        camera_name: {}', camera_name )
			self.logger.flush()

		# If the mode is combine and the camera exists we ignore the camera 
		if self.settings.mode == 'combine' and cameras_group.contains_example( camera_name ):
			# Log
			if self.logger:
				self.logger.log( '        The camera example already exists: {}',  cameras_group.get_example( camera_name ).url )
				self.logger.log( '        Skipping camera.' )
				self.logger.log( '    DONE' )
				self.logger.flush()
			# Done
			return
		
		# Otherwise, create the camera example
		camera_example = cameras_group.create_example( camera_name )
	
		# Log
		if self.logger:
			self.logger.log( '        Created camera example {}', camera_example.url )
			self.logger.end_line()
			self.logger.log( '        CREATING FEATURES:' )
			self.logger.flush()

		# Read the camera data
		camera_data = camera_file.read_json()

		# K
		if self.settings.camera_matrix_feature_name:
			# Fetch
			cx = float( camera_data[ 'cx' ] )
			cy = float( camera_data[ 'cy' ] )
			fx = float( camera_data[ 'fx' ] )
			fy = float( camera_data[ 'fy' ] )
			# Parse
			K = numpy.asarray([
			[fx, 0., cx],
			[0., fy, cy],
			[0., 0., 1.]
			],
			dtype=numpy.float32
			)
			K = numpy.reshape( K, [3, 3] ).astype( numpy.float32 )
			# Create faeture
			feature = camera_example.create_feature(
				     feature_name = self.settings.camera_matrix_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = False,
				            value = K
				)
			# Log
			if self.logger:
				self.logger.log( '            {:12s}: {}', self.settings.camera_matrix_feature_name, feature.url )

		# Depth Scale
		if self.settings.depth_scale_feature_name:
			# Fetch
			depth_scale = float( camera_data[ 'depth_scale' ] )
			# Create faeture
			feature = camera_example.create_feature(
				     feature_name = self.settings.depth_scale_feature_name,
				feature_data_type = pydataset.dataset.FloatFeatureData,
				      auto_update = False,
				            value = depth_scale
				)
			# Log
			if self.logger:
				self.logger.log( '            {:12s}: {}', self.settings.depth_scale_feature_name, feature.url )

		# Images Size
		if self.settings.image_size_feature_name:
			# Fetch
			width  = int( camera_data[ 'width' ] )
			height = int( camera_data[ 'height' ] )
			# Create faeture
			feature = camera_example.create_feature(
				     feature_name = self.settings.image_size_feature_name,
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				      auto_update = False,
				            value = pydataset.data.ShapeData( width, height )
				)
			# Log
			if self.logger:
				self.logger.log( '            {:12s}: {}', self.settings.image_size_feature_name, feature.url )

		# Parse the camera data
		camera_example.update()
		
		# Log
		if self.logger:
			self.logger.log( '        DONE' )
			self.logger.end_line()
			self.logger.log( '    DONE IMPORTING CAMERA {}', camera_file )
			self.logger.end_line()
			self.logger.flush()

	# def import_camera ( self, camera_file )

	# --------------------------------------------------

	def import_models ( self, models_directory, objects_group ):

		# Log
		if ( self.logger ):
			self.logger.log( '    LOADING MODELS FROM DIRECTORY {}:', models_directory )
			self.logger.end_line()
			self.logger.flush()

		# Is the models directory spetial
		if '_' in models_directory.name():
			special = True
			special_postfix = models_directory.name().split('_')[1]
			if self.settings.ignore_fine_models and special_postfix == 'fine':
				# Log
				if ( self.logger ):
					self.logger.log( '        Ignoring fine models.' )
					self.logger.end_line()
					self.logger.log( '    DONE MODELS FROM DIRECTORY {}:', models_directory )
					self.logger.end_line()
					self.logger.flush()
				# Exit
				return

		else:
			special = False

		# Locate models info file
		models_info_filepath = models_directory + pytools.path.FilePath( 'models_info.json' )

		# Log
		if ( self.logger ):
			self.logger.log( '        MODELS INFO:' )
			self.logger.log( '            models_info_filepath: {}', models_info_filepath )
			self.logger.flush()

		# Check models info file
		if models_info_filepath.is_not_a_file():
			raise IOError( "The models info file '{}' does not exist".format(models_info_filepath) )

		# Parse models infos
		models_info = models_info_filepath.read_json()
		number_of_models = len( models_info )

		# Log
		if ( self.logger ):
			self.logger.log( '            number_of_models:     {}', number_of_models )
			self.logger.end_line()
			self.logger.log( '        LOADING MODELS:' )
			self.logger.end_line()
			self.logger.flush()

		# Parse each model
		for str_model_index, model_info in models_info.items():
			
			# Log
			if ( self.logger ):
				self.logger.log( '            LOADING MODEL {}:', str_model_index )

			# Category Index
			model_index = int(str_model_index)
			model_name = '{:06d}'.format(model_index)

			# If the mode is combine and the models exists we ignore it
			if self.settings.mode == 'combine' and objects_group.contains_example( model_name ):
				# Log
				if self.logger:
					self.logger.log( '                The model example already exists: {}',  objects_group.get_example( model_name ).url )
					self.logger.log( '                Skipping model.' )
					self.logger.log( '            DONE' )
					self.logger.flush()
				# Done
				continue

			# Create/get Model Example
			model_example = objects_group.get_or_create_example( '{:06d}'.format(model_index), auto_update=True )

			# Category Index
			if not special and self.settings.category_index_feature_name:
				# Compute index
				category_index = model_index-1
				# Create
				feature = model_example.create_or_update_feature(
					     feature_name = self.settings.category_index_feature_name,
					feature_data_type = pydataset.dataset.IntFeatureData,
					      auto_update = False,
					            value = category_index
					)
				# Log
				if ( self.logger ):
					self.logger.log( '                {:16s}: {}', self.settings.category_index_feature_name, feature.url )
					self.logger.flush()
			
			# Category name
			if not special and self.settings.category_name_feature_name:
				# Compute index and fetch name
				category_index = model_index-1
				category_name  = self.settings.category_names[ category_index ]
				# Create
				feature = model_example.create_or_update_feature(
					     feature_name = self.settings.category_name_feature_name,
					feature_data_type = pydataset.dataset.TextFeatureData,
					      auto_update = False,
					            value = category_name
					)
				# Log
				if ( self.logger ):
					self.logger.log( '                {:16s}: {}', self.settings.category_name_feature_name, feature.url )
					self.logger.flush()

			# One Hot Category
			if not special and self.settings.one_hot_category_feature_name:
				# Compute
				category_index = model_index-1
				one_hot_category = pydataset.classification_utils.index_to_one_hot( category_index, number_of_models )
				# Create
				feature = model_example.create_or_update_feature(
					     feature_name = self.settings.one_hot_category_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = one_hot_category
					)
				# Log
				if ( self.logger ):
					self.logger.log( '                {:16s}: {}', self.settings.one_hot_category_feature_name, feature.url )
					self.logger.flush()

			# Mesh
			if self.settings.mesh_feature_name:
				# Locate
				mesh_filepath = models_directory + pytools.path.FilePath.format( 'obj_{:06d}.ply', model_index )
				# texture_filepath = models_directory + pytools.path.FilePath.format( 'obj_{:06d}.png', model_index )
				# Check
				if mesh_filepath.is_not_a_file():
					raise IOError( "The file '{}' does not exist".format(mesh_filepath) )
				# Load the ply mesh
				try:
					mesh_data = pydataset.render.MeshData.load_ply_v2( mesh_filepath )
				except Exception as e:
					raise RuntimeError( "Failed to load mesh {}: {}".format(mesh_filepath, str(e)) )
				
				# Scale model
				mesh_data.points /= 1000.
				# Name
				mesh_feature_name = self.settings.mesh_feature_name + '_' + special_postfix if special else self.settings.mesh_feature_name
				# Create
				feature = model_example.create_or_update_feature(
					     feature_name = mesh_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = False,
					            value = mesh_data
					)
				# Log
				if ( self.logger ):
					self.logger.log( '                {:16s}: {}', mesh_feature_name, feature.url )
					self.logger.flush()

			# Mesh Diameter
			if not special and self.settings.mesh_diameter_feature_name:
				# Parse
				diameter = float( model_info['diameter'] )
				# Create
				feature = model_example.create_or_update_feature(
					     feature_name = self.settings.mesh_diameter_feature_name,
					feature_data_type = pydataset.dataset.FloatFeatureData,
					      auto_update = False,
					            value = diameter
					)
				# Log
				if ( self.logger ):
					self.logger.log( '                {:16s}: {}', self.settings.mesh_diameter_feature_name, feature.url )
					self.logger.flush()
			
			# Log
			if ( self.logger ):
				self.logger.log( '            DONE' )
				self.logger.end_line()
				self.logger.flush()

			# Update model example
			model_example.update()
					
		# for str_model_index, model_info in models_info.items()
		
		# Log
		if ( self.logger ):
			self.logger.log( '            DONE LOADING MODELS' )
			self.logger.end_line()
			self.logger.log( '    DONE LOADING MODELS FROM DIRECTORY {}:', models_directory )
			self.logger.end_line()
			self.logger.flush()

	# def import_models( self, models_directory, objects_group )

	# --------------------------------------------------

	def import_scenes ( self, splits_directories, scenes_group ):
		
		# For each scene directory of each split directroy
		# Create a task to import the scene.
		scenes_importers = []
		for split_directory in splits_directories:

			# Get the split
			split_name = split_directory.name()
			
			# Get or Create the destination split group
			if not scenes_group.contains_group( split_name ):
				split_group = scenes_group.create_group( split_name, auto_update=True )
			else:
				split_group = scenes_group.get_group( split_name )

			# Considered directories
			scenes_directories = []

			# Go though the contents of splits directories
			for path in split_directory.list_contents():

				# Directories are scenes
				if path.is_a_directory():
					
					# Considere the directory
					scenes_directories.append( path )
					
				# Rest is an error
				else:
					raise IOError( "Path '{}' should not exist".format(path) )
			
			# for path in split_directory.list_contents()			

			# Create a task to import each scene.
			for scene_directory in scenes_directories:
				
				scene_name    = scene_directory.name()
				importer_name = 'bop-scene-{}-{}-importer'.format( split_name, scene_name )
				dst_group_url = '{}/groups/{}/groups/{}'.format( scenes_group.absolute_url, split_name, scene_name )

				# Get or Create the destination scene group
				if not split_group.contains_group( scene_name ):
					split_group.create_group( scene_name, auto_update=True )

				scene_importer = SceneImporter.create(
					                   scene_directory_path = scene_directory,
					                          dst_group_url = dst_group_url,
					                                   mode = self.settings.mode,
					                     objects_group_name = self.settings.objects_group_name,
					            category_index_feature_name = self.settings.category_index_feature_name,
					          one_hot_category_feature_name = self.settings.one_hot_category_feature_name,
					                      mesh_feature_name = self.settings.mesh_feature_name,
					             category_name_feature_name = self.settings.category_name_feature_name,
					             camera_matrix_feature_name = self.settings.camera_matrix_feature_name,
					               depth_scale_feature_name = self.settings.depth_scale_feature_name,
					                     image_feature_name = self.settings.image_feature_name,
					                     depth_feature_name = self.settings.depth_feature_name,
					                      mask_feature_name = self.settings.mask_feature_name,
					              visible_mask_feature_name = self.settings.visible_mask_feature_name,
					           rotation_matrix_feature_name = self.settings.rotation_matrix_feature_name,
					        translation_vector_feature_name = self.settings.translation_vector_feature_name,
					        bounding_rectangle_feature_name = self.settings.bounding_rectangle_feature_name,
					visible_bounding_rectangle_feature_name = self.settings.visible_bounding_rectangle_feature_name,
					          number_of_pixels_feature_name = self.settings.number_of_pixels_feature_name,
					    number_of_valid_pixels_feature_name = self.settings.number_of_valid_pixels_feature_name,
					  number_of_visible_pixels_feature_name = self.settings.number_of_visible_pixels_feature_name,
					                visibility_feature_name = self.settings.visibility_feature_name,
					                                   name = importer_name,
					                                 logger = pytools.tasks.file_logger( 'logs/{}.log'.format( importer_name ) ),
					                       progress_tracker = self.progress_tracker.create_child() if self.progress_tracker else None
					)

				scenes_importers.append( scene_importer )
			
			# for scene_directory in scenes_directories

		# for split_directory in splits_directories:

		# for scene_importer in scenes_importers:
		# 	scene_importer.execute()

		# Create a manager for the tasks
		task_manager = pytools.tasks.Manager( *scenes_importers )
		
		# Start the importation of each scene
		task_manager.start_tasks()

		# Main loop
		wait_for_stop = True
		while ( wait_for_stop ):			
			wait_for_stop = False

			# Check if the task failed or was stopped
			if not self.is_running():
				break

			# Update the task status
			self.update()

			# Check if all tasks finished
			for task in task_manager.tasks:

				# If one did not finish
				if not task.stopped():

					# Continue waiting
					wait_for_stop = True

				# Check if a task failed
				if task.is_failed():
					for task in task_manager.tasks:
						task.stop()
					task_manager.join_tasks()
					raise task.error
			
			# for task in task_manager.tasks

			# Sleep
			time.sleep( 0.1 )

		# while ( wait_for_stop )

		task_manager.join_tasks()

		# Check for error again
		for task in self._task_manager.tasks:
			if task.is_failed():
				raise task.error

	# def importe_scenes ( self )	

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pybop.DatasetImporterSettings` class.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pybop.DatasetImporterSettings` constructor.

		Returns:
			`pybop.DatasetImporterSettings`: The settings.
		"""
		return DatasetImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class DatasetImporter ( pydataset.io.Importer )