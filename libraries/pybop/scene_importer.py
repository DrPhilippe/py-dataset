# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .scene_importer_settings import SceneImporterSettings

# ##################################################
# ###            CLASS SCENE-IMPORTER            ###
# ##################################################

class SceneImporter ( pydataset.io.Importer ):
	"""
	Impors BOP dataset scenes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new instance of the `pybop.SceneImporter` class.

		Arguments:
			self                              (`pybop.SceneImporter`): Importer to initialize.
			settings                  (`pybop.SceneImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )
		assert pytools.assertions.type_is_instance_of( settings, SceneImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( SceneImporter, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run importation.

		Arguments:
			self (`pybop.SceneImporter`): Importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneImporter )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0. )

		# Log settings
		if self.logger:
			self.logger.log( 'BOP SCENE IMPORTER' )
			self.logger.end_line()
			self.logger.log( 'SETTINGS:' )
			for field_name in SceneImporterSettings.__fields_names__:
				self.logger.log( "    {:40s}: '{}'", field_name, getattr(self.settings, field_name) )
			self.logger.end_line()
			self.logger.flush()

		# Sub directories and files
		depth_directory_path        = self.settings.scene_directory_path + pytools.path.DirectoryPath( 'depth' )
		mask_directory_path         = self.settings.scene_directory_path + pytools.path.DirectoryPath( 'mask' )
		visible_mask_directory_path = self.settings.scene_directory_path + pytools.path.DirectoryPath( 'mask_visib' )
		image_directory_path        = self.settings.scene_directory_path + pytools.path.DirectoryPath( 'rgb' )
		scene_camera_filepath       = self.settings.scene_directory_path + pytools.path.FilePath( 'scene_camera.json' )
		scene_gt_filepath           = self.settings.scene_directory_path + pytools.path.FilePath( 'scene_gt.json' )
		scene_gt_info_filepath      = self.settings.scene_directory_path + pytools.path.FilePath( 'scene_gt_info.json' )

		# Log
		if self.logger:
			self.logger.log( 'DIRECTORIES AND FILES:' )
			self.logger.log( '    depth_directory_path:        {}', depth_directory_path )
			self.logger.log( '    mask_directory_path:         {}', mask_directory_path )
			self.logger.log( '    visible_mask_directory_path: {}', visible_mask_directory_path )
			self.logger.log( '    image_directory_path:        {}', image_directory_path )
			self.logger.log( '    scene_camera_filepath:       {}', scene_camera_filepath )
			self.logger.log( '    scene_gt_filepath:           {}', scene_gt_filepath )
			self.logger.log( '    scene_gt_info_filepath:      {}', scene_gt_info_filepath )
			self.logger.end_line()
			self.logger.flush()

		# Check
		if depth_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(depth_directory_path) )
		if mask_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(mask_directory_path) )
		if visible_mask_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(visible_mask_directory_path) )
		if image_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(image_directory_path) )
		if scene_camera_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_camera_filepath) )
		if scene_gt_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_gt_filepath) )
		if scene_gt_info_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_gt_info_filepath) )
		
		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0.01 )

		# Load ground truth
		scene_camera_data  = scene_camera_filepath.read_json()
		scene_gt_data      = scene_gt_filepath.read_json()
		scene_gt_info_data = scene_gt_info_filepath.read_json()
		
		# Number of images
		image_keys        = list(scene_gt_data.keys())
		number_of_images = len( image_keys )
		
		# Log
		if self.logger:
			self.logger.log( 'NUMBER OF IMAGES:' )
			self.logger.log( '    number_of_images: {}', number_of_images )
			self.logger.end_line()
			self.logger.flush()

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 0.02 )

		# Import each image
		i = 0.
		for image_key in image_keys:
			
			# Get ground trouth of the image
			image_index         = int(image_key)
			image_name          = '{:06d}'.format( image_index )
			image_camera_data   = scene_camera_data[ image_key ]
			image_gt_data       = scene_gt_data[ image_key ]
			image_gt_info_data  = scene_gt_info_data[ image_key ]
			number_of_instances = len( image_gt_data )

			# Log
			if self.logger:
				self.logger.log( 'IMAGE N°{}:', image_index )
				self.logger.end_line()
				self.logger.log( '    GENERAL:' )
				self.logger.log( "        image_key:          '{}'", image_key )
				self.logger.log( "        image_index:        '{}'", image_index )
				self.logger.log( "        image_name:         '{}'", image_name )
				self.logger.log( '        number_of_instances: {}', number_of_instances )
				self.logger.end_line()
				self.logger.flush()

			# Create a group for the image
			if self.settings.mode == 'create':
				image_group = self.dst_group.create_group( image_name, auto_update=True )
			else:
				image_group = self.dst_group.get_group( image_name )
			
			# Log
			if self.logger:
				self.logger.log( '    DST GROUP' )
				self.logger.log( "        image_group: '{}'", image_group.url )
				self.logger.end_line()
				self.logger.log( '    IMPORTING FEATURES:' )
				self.logger.flush()

			# K
			if self.settings.camera_matrix_feature_name:
				# Fetch
				K = image_camera_data[ 'cam_K' ]
				# Parse
				K = numpy.asarray( K, dtype=numpy.float32 )
				K = numpy.reshape( K, [3, 3] )
				# Create feature
				feature = image_group.create_or_update_feature(
					     feature_name = self.settings.camera_matrix_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = K
					)
				# Log
				if self.logger:
					self.logger.log( '        {:20s}: {}', self.settings.camera_matrix_feature_name, feature.url )

			# Depth Scale
			if self.settings.depth_scale_feature_name:
				# Fetch
				depth_scale = image_camera_data[ 'depth_scale' ]
				# Parse
				depth_scale = float( depth_scale )
				# Create feature
				feature = image_group.create_or_update_feature(
					     feature_name = self.settings.depth_scale_feature_name,
					feature_data_type = pydataset.dataset.FloatFeatureData,
					      auto_update = False,
					            value = depth_scale
					)
				# Log
				if self.logger:
					self.logger.log( '        {:20s}: {}', self.settings.depth_scale_feature_name, feature.url )

			# RGB image
			if self.settings.image_feature_name:				
				# Path
				image_filepath = image_directory_path + pytools.path.FilePath.format( '{}.png', image_name )				
				# Check
				if image_filepath.is_a_directory():
					raise IOError( "The file '{}' does not exist".format(image_filepath) )
				# Read
				image = cv2.imread( str(image_filepath), cv2.IMREAD_UNCHANGED ).astype( numpy.uint8 )
				# Create feature
				feature = image_group.create_or_update_feature(
					     feature_name = self.settings.image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					            value = image,
				                dtype = numpy.dtype( 'uint8' ),
					        extension = '.png',
					            param = 9,
					)
				# Log
				if self.logger:
					self.logger.log( '        {:20s}: {}', self.settings.image_feature_name, feature.url )

			# Depth image
			if self.settings.depth_feature_name:
				# Path
				depth_filepath = depth_directory_path + pytools.path.FilePath.format( '{}.png', image_name )
				# Check
				if depth_filepath.is_a_directory():
					raise IOError( "The file '{}' does not exist".format(depth_filepath) )
				# Read
				depth = cv2.imread( str(depth_filepath), cv2.IMREAD_UNCHANGED ).astype( numpy.uint16 )
				# Create feature
				feature = image_group.create_or_update_feature(
					     feature_name = self.settings.depth_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					            value = depth,
				                dtype = numpy.dtype( 'uint16' ),
					        extension = '.png',
					            param = 9,
					)
				# Log
				if self.logger:
					self.logger.log( '        {:20s}: {}', self.settings.depth_feature_name, feature.url )

			# Update image group before importing images
			image_group.update()
			
			# Log
			if self.logger:
				self.logger.end_line()
				self.logger.log( '    IMPORTING INSTANCES:' )
				self.logger.flush()

			# Import each object instance
			for instance_index in range( number_of_instances ):
				
				instance_name = '{:06d}'.format( instance_index )

				# Create an example for this instance
				if self.settings.mode == 'create':
					# If we are creating the dataset, use the exact index of the instance
					instance_example = image_group.create_example( instance_name, auto_update=True )
				else:
					# If we are appending to the dataset, automaticaly pick the instance index
					instance_example = image_group.create_example( prefix='', name_format='{}{:06d}', auto_update=True )

				# Log
				if self.logger:
					self.logger.log( '        INSTANCE N°{}:', instance_index )
					self.logger.log( '            instance_index:   {}', instance_index )
					self.logger.log( '            instance_example: {}', instance_example.url )
					self.logger.end_line()
					self.logger.flush()

				# Fetch labels
				instance_gt_data      = image_gt_data[ instance_index ]
				instance_gt_info_data = image_gt_info_data[ instance_index ]
				
				# Get the category of the model
				category_index = instance_gt_data[ 'obj_id' ]
				ref_model_url  = 'groups/{}/examples/{:06d}'.format( self.settings.objects_group_name, category_index )
								
				# Log
				if self.logger:
					self.logger.log( '        CATEGORY:' )
					self.logger.log( '            category_index: {}', category_index )
					self.logger.log( '            ref_model_url:  {}', ref_model_url )
					self.logger.end_line()
					self.logger.log( '        FEATURES:' )
					self.logger.flush()
				
				# R
				if self.settings.rotation_matrix_feature_name:					
					# Fetch
					R = instance_gt_data[ 'cam_R_m2c' ]
					# Parse
					R = numpy.asarray( R, dtype=numpy.float32 )
					R = numpy.reshape( R, [3, 3] )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.rotation_matrix_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = R
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.rotation_matrix_feature_name, feature.url )

				# t
				if self.settings.translation_vector_feature_name:
					# Fetch
					t = instance_gt_data[ 'cam_t_m2c' ]
					# Parse
					t = numpy.asarray( t, dtype=numpy.float32 )
					t = numpy.reshape( t, [3] )
					t /= 1000. # mm -> m
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.translation_vector_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = t
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.translation_vector_feature_name, feature.url )

				# Category index
				if self.settings.category_index_feature_name:
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.category_index_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( ref_model_url, self.settings.category_index_feature_name )
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.category_index_feature_name, feature.url )

				# Category name
				if self.settings.category_index_feature_name:
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.category_name_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( ref_model_url, self.settings.category_name_feature_name )
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.category_name_feature_name, feature.url )

				# One Hot Category
				if self.settings.one_hot_category_feature_name:					
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.one_hot_category_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( ref_model_url, self.settings.one_hot_category_feature_name )
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.one_hot_category_feature_name, feature.url )

				# Mesh
				if self.settings.mesh_feature_name:
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.mesh_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( ref_model_url, self.settings.mesh_feature_name )
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.mesh_feature_name, feature.url )

				# Mask
				if self.settings.mask_feature_name:
					# Path
					mask_filepath = mask_directory_path + pytools.path.FilePath.format( '{}_{}.png', image_name, instance_name )
					# Check
					if mask_filepath.is_a_directory():
						raise IOError( "The file '{}' does not exist".format(mask_filepath) )
					# Read
					mask = cv2.imread( str(mask_filepath), cv2.IMREAD_UNCHANGED )
					# Parse
					mask = mask.astype( numpy.bool )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = mask
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.mask_feature_name, feature.url )
				
				# Visible Mask
				if self.settings.visible_mask_feature_name:
					# Path
					visible_mask_filepath = visible_mask_directory_path + pytools.path.FilePath.format( '{}_{}.png', image_name, instance_name )
					# Check
					if visible_mask_filepath.is_a_directory():
						raise IOError( "The file '{}' does not exist".format(visible_mask_filepath) )
					# Read
					mask = cv2.imread( str(visible_mask_filepath), cv2.IMREAD_UNCHANGED )
					# Parse
					mask = mask.astype( numpy.bool )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.visible_mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = mask
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.visible_mask_feature_name, feature.url )
				
				# Log
				if self.logger:
					self.logger.log( '        DONE' )
					self.logger.end_line()
					self.logger.log( '        EXTRA FEATURES:' )
				
				# Bounding Rectangle
				if self.settings.bounding_rectangle_feature_name:
					# Fetch
					bounding_rectangle = instance_gt_info_data[ 'bbox_obj' ]
					# Parse
					bounding_rectangle = numpy.asarray( bounding_rectangle, dtype=numpy.float32 )
					bounding_rectangle = numpy.reshape( bounding_rectangle, [2, 2] )
					bounding_rectangle[ 1, 0 ] += bounding_rectangle[ 0, 0 ]
					bounding_rectangle[ 1, 1 ] += bounding_rectangle[ 0, 1 ]
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.bounding_rectangle_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = bounding_rectangle
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.bounding_rectangle_feature_name, feature.url )
				
				# Visible Bounding Rectangle
				if self.settings.visible_bounding_rectangle_feature_name:
					# Fetch
					bounding_rectangle = instance_gt_info_data[ 'bbox_visib' ]
					# Parse
					bounding_rectangle = numpy.asarray( bounding_rectangle, dtype=numpy.float32 )
					bounding_rectangle = numpy.reshape( bounding_rectangle, [2, 2] )
					bounding_rectangle[ 1, 0 ] += bounding_rectangle[ 0, 0 ]
					bounding_rectangle[ 1, 1 ] += bounding_rectangle[ 0, 1 ]
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.visible_bounding_rectangle_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = bounding_rectangle
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.visible_bounding_rectangle_feature_name, feature.url )
				
				# Number of Pixels
				if self.settings.number_of_pixels_feature_name:
					# Fetch
					number_of_pixels = instance_gt_info_data[ 'px_count_all' ]
					# Parse
					number_of_pixels = int( number_of_pixels )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.number_of_pixels_feature_name,
						feature_data_type = pydataset.dataset.IntFeatureData,
						      auto_update = False,
						            value = number_of_pixels
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.number_of_pixels_feature_name, feature.url )
				
				# Number of Valid Pixels
				if self.settings.number_of_valid_pixels_feature_name:
					# Fetch
					number_of_pixels = instance_gt_info_data[ 'px_count_valid' ]
					# Parse
					number_of_pixels = int( number_of_pixels )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.number_of_valid_pixels_feature_name,
						feature_data_type = pydataset.dataset.IntFeatureData,
						      auto_update = False,
						            value = number_of_pixels
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.number_of_valid_pixels_feature_name, feature.url )
				
				# Number of Visible Pixels
				if self.settings.number_of_visible_pixels_feature_name:
					# Fetch
					number_of_pixels = instance_gt_info_data[ 'px_count_visib' ]
					# Parse
					number_of_pixels = int( number_of_pixels )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.number_of_visible_pixels_feature_name,
						feature_data_type = pydataset.dataset.IntFeatureData,
						      auto_update = False,
						            value = number_of_pixels
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.number_of_visible_pixels_feature_name, feature.url )
				
				# Visibility
				if self.settings.visibility_feature_name:
					# Fetch
					visibility = instance_gt_info_data[ 'visib_fract' ]
					# Parse
					visibility = float( visibility )
					# Create feature
					feature = instance_example.create_or_update_feature(
						     feature_name = self.settings.visibility_feature_name,
						feature_data_type = pydataset.dataset.FloatFeatureData,
						      auto_update = False,
						            value = visibility
						)
					# Log
					if self.logger:
						self.logger.log( '            {:20s}: {}', self.settings.visibility_feature_name, feature.url )
								
				# Log
				if self.logger:
					self.logger.log( '        DONE' )
					self.logger.end_line()

				# Update the instance example
				instance_example.update()

			# for instance_index in range( number_of_instances )
			
			# Log
			if self.logger:
				self.logger.log( 'IMAGE N°{} DONE.', image_index )
				self.logger.end_line()

			# Update progression
			i += 1.
			if self.progress_tracker:
				self.progress_tracker.update( i/float(number_of_images) )

		# for image_index in range( number_of_images )

		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( 1. )

	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pybop.SceneImporterSettings` class.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pybop.SceneImporterSettings` constructor.

		Returns:
			`pybop.SceneImporterSettings`: The settings.
		"""
		return SceneImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.io.Importer )