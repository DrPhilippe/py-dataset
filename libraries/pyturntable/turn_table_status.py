# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###          CLASS TURN-TABLE-STATUS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TurnTableStatus',
	   namespace = 'pyturntable',
	fields_names = []
	)
class TurnTableStatus  ( pytools.serialization.Enum ):

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	unknown = None
	ok      = None
	error   = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='unknown' ):
		"""
		Initializes a new instance of the turn table status enum.

		Arguments:
			self (`pyturntable.TurnTableStatus`): Instance to initialize.
			value                        (`str`): Value of the enum.
		"""
		assert pytools.assertions.type_is_instance_of( self, TurnTableStatus )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( TurnTableStatus, self ).__init__( value )

	# def __init__ ( self, value )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible values the stream type enum can take.

		Arguments:
			cls (`type`): StreamType

		Returns:
			`list` of `str`: The list of possible values.
		"""
		return [
			'unknown',
			'ok',
			'error'
			]

	# def get_possible_values ( cls )

# class TurnTableStatus  ( pytools.serialization.Enum )

TurnTableStatus.unknown = TurnTableStatus( 'unknown' )
TurnTableStatus.ok      = TurnTableStatus( 'ok' )
TurnTableStatus.error   = TurnTableStatus( 'error' )