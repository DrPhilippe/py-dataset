# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import json
import serial
import time

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .turn_table_data     import TurnTableData
from .turn_table_settings import TurnTableSettings
from .turn_table_status   import TurnTableStatus

# ##################################################
# ###              CLASS TURN-TABLE              ###
# ##################################################

class TurnTable:

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings=TurnTableSettings() ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )

		self._data = TurnTableData()
		
		try:
			self._serial = serial.Serial( settings.port, settings.baud_rate, timeout=1.0 )
			self.read_data_from_serial()
			self.set_unit_angle( settings.unit_angle )
			self.set_velocity( settings.velocity )
		
		except serial.SerialException as e:
			print( 'Failed to open Serial communication on port {} with baud-rate {}'.format(settings.port, settings.baud_rate) )
			self._serial = None

		except serial.SerialTimeoutException as e:
			print( 'Serial communication on port {} with baud-rate {} timed-out'.format(settings.port, settings.baud_rate) )
			self._serial = None

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def data ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )

		return self._data

	# def data ( self )

	# --------------------------------------------------
	
	@property
	def serial ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )

		return self._serial
	
	# def serial ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def close ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )

		self.serial.close()

	# def close ( self )

	# --------------------------------------------------

	def home ( self, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'H', wait_for_result )
	
	# def home ( self, wait_for_result )

	# --------------------------------------------------

	def rotate ( self, angle, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( angle, float )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'R {}'.format(angle), wait_for_result )
	
	# def rotate ( self, angle, wait_for_result )

	# --------------------------------------------------

	def set_unit_angle ( self, angle, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( angle, float )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'U {}'.format(angle), wait_for_result )
	
	# def set_unit_angle ( self, angle, wait_for_result )

	# --------------------------------------------------

	def set_velocity ( self, velocity, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( velocity, int )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'V {}'.format(velocity), wait_for_result )
	
	# def set_velocity ( self, velocity, wait_for_result )

	# --------------------------------------------------

	def step ( self, steps, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( steps, int )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'S {}'.format(steps), wait_for_result )
	
	# def step ( self, steps, wait_for_result )

	# --------------------------------------------------

	def turn ( self, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( wait_for_result, bool )

		self.run_command( 'T', wait_for_result )
	
	# def turn ( self, wait_for_result )

	# --------------------------------------------------

	def parse_data ( self, data ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( data, bytes )

		if not data:
			return
		
		try:
			data = json.loads( data )
		except json.decoder.JSONDecodeError as error:
			print( 'Failed to parse json data from line:', data )
			return

		self._data = TurnTableData(
			TurnTableStatus( data[ 'status' ] ),
			data[ 'angle'      ],
			data[ 'steps'      ],
			data[ 'is_home'    ],
			data[ 'unit_angle' ],
			data[ 'velocity'   ],
			data[ 'message'    ]
			)

	# def parse_data ( self, data )

	# --------------------------------------------------

	def read_data_from_serial ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )

		data = self.serial.readline()
		self.parse_data( data )

	# def read_data_from_serial ( self )

	# --------------------------------------------------

	def run_command ( self, command, wait_for_result=True ):
		assert pytools.assertions.type_is_instance_of( self, TurnTable )
		assert pytools.assertions.type_is( command, str )
		assert pytools.assertions.type_is( wait_for_result, bool )

		# Send the command
		data = bytes( command, 'ascii' )
		self.serial.write( data )

		if wait_for_result:

			data = self.serial.readline()
			while ( not data ):
				time.sleep( 0.1 )
				data = self.serial.readline()

			# Read the result
			self.parse_data( data )

	# def run_command ( self, command )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __bool__ ( self ):
	
		return self._serial is not None

	# def __bool__ ( self )

# class TurnTable