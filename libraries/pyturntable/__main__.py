# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import sys

# INTERNALS
import pyturntable


# ##################################################
# ###                  MAIN                      ###
# ##################################################

if __name__ == '__main__':
	
	parser = argparse.ArgumentParser()
	parser.add_argument( '--port', type=str, default='COM4', help='Serial USB port' )
	parser.add_argument( '--baudrate', type=int, default=9600, help='Serial USB baud rate' )
	arguments = parser.parse_args()

	settings = pyturntable.TurnTableSettings( arguments.port, arguments.baudrate )
	table = pyturntable.TurnTable( settings )
	if not table:
		exit()
	
	sys.stdout.write( 'Opened connection to Turn-Table on Serial with port \'{}\' and baudrate {}.\n'.format(arguments.port, arguments.baudrate) )
	sys.stdout.write( 'Type "help" if you dont know what to do.\n' )

	while True:

		sys.stdout.write( '> ' )
		line = input()

		if line == 'exit' or line == 'e':
			break

		if line == 'help':
			sys.stdout.write( 'R.T.F.R. (Read the fucking readme)\n' )
			continue

		table.run_command( line )

		sys.stdout.write( '{}, {:6.2f}: {}.\n'.format( str(table.data.status), table.data.angle, table.data.message ) )

	table.close()