# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import serial

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .turn_table_status import TurnTableStatus

# ##################################################
# ###           CLASS TURN-TABLE-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TurnTableData',
	   namespace = 'pyturntable',
	fields_names = [
		'status',
		'angle',
		'steps'
		'is_home'
		'message',
		'unit_angle',
		'velocity'
		]
	)
class TurnTableData ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, status=TurnTableStatus.unknown, angle=0.0, steps=0, is_home=False, unit_angle=0.0, velocity=0, message='' ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is_instance_of( status, TurnTableStatus )
		assert pytools.assertions.type_is( angle, float )
		assert pytools.assertions.type_is( steps, int )
		assert pytools.assertions.type_is( is_home, bool )
		assert pytools.assertions.type_is( unit_angle, float )
		assert pytools.assertions.type_is( velocity, int )
		assert pytools.assertions.type_is( message, str )

		# Initialize fields
		self._status     = status
		self._angle      = angle
		self._steps      = steps
		self._is_home    = is_home
		self._unit_angle = unit_angle
		self._velocity   = velocity
		self._message    = message

	# def __init__ ( self, status, angle, step, is_home, unit_angle, velocity, message )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def status ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )

		return self._status
	
	# def status ( self )
	
	# --------------------------------------------------

	@status.setter
	def status ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is_instance_of( value, TurnTableStatus )

		self._status = value

	# def status ( self, value )

	# --------------------------------------------------

	@property
	def angle ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._angle

	# def angle ( self )
	
	# --------------------------------------------------

	@angle.setter
	def angle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, float )
		
		self._angle = value
		
	# def angle ( self, value )

	# --------------------------------------------------

	@property
	def steps ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._steps

	# def steps ( self )

	# --------------------------------------------------

	@steps.setter
	def steps ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, int )
		
		self._steps = value

	# def steps ( self, value ):
	
	# --------------------------------------------------

	@property
	def is_home ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._is_home
	
	# def is_home ( self )

	# --------------------------------------------------

	@is_home.setter
	def is_home ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, bool )
		
		self._is_home = value
	
	# def is_home ( self, value )

	# --------------------------------------------------

	@property
	def unit_angle ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._unit_angle
	
	# def unit_angle ( self )

	# --------------------------------------------------

	@unit_angle.setter
	def unit_angle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, float )
		
		self._unit_angle = value
	
	# def unit_angle ( self, value )

	# --------------------------------------------------

	@property
	def velocity ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._velocity
	
	# def velocity ( self )

	# --------------------------------------------------

	@velocity.setter
	def velocity ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, int )
		
		self._velocity = value
	
	# def velocity ( self, value )

	# --------------------------------------------------

	@property
	def message ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		
		return self._message
	
	# def message ( self )

	# --------------------------------------------------

	@message.setter
	def message ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableData )
		assert pytools.assertions.type_is( value, str )
		
		self._message = value

	# def message ( self, value )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __repr__ ( self ):
		
		return '[TurnTableData: status={}, angle={}, steps={}, is_home={}, unit_angle={}, velocity={}, message={}]'.format(
			self._status,
			self._angle,
			self._steps,
			self._is_home,
			self._unit_angle,
			self._velocity,
			self._message
			)

# class TurnTableData ( pytools.serialization.Serializable )