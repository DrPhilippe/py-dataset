# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import serial

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .turn_table_status import TurnTableStatus

# ##################################################
# ###           CLASS TURN-TABLE-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TurnTableSettings',
	   namespace = 'pyturntable',
	fields_names = [
		'port',
		'baud_rate',
		'unit_angle',
		'velocity'
		]
	)
class TurnTableSettings ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, port='COM4', baud_rate=9600, unit_angle=1.0, velocity=1 ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( port, str )
		assert pytools.assertions.type_is( baud_rate, int )
		assert pytools.assertions.type_is( unit_angle, float )
		assert pytools.assertions.type_is( velocity, int )

		# Initialize fields
		self._port       = port
		self._baud_rate  = baud_rate
		self._unit_angle = unit_angle
		self._velocity   = velocity

	# def __init__ ( self, port, baud_rate, unit_angle, velocity )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def port ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )

		return self._port
	
	# def port ( self )

	# --------------------------------------------------

	@port.setter
	def port ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( value, str )

		self._port = value

	# def port ( self, value )

	# --------------------------------------------------

	@property
	def baud_rate ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		
		return self._baud_rate

	# def baud_rate ( self )
	
	# --------------------------------------------------

	@baud_rate.setter
	def baud_rate ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( value, int )
		
		self._baud_rate = value
		
	# def baud_rate ( self, value )

	# --------------------------------------------------

	@property
	def unit_angle ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		
		return self._unit_angle
	
	# def unit_angle ( self )

	# --------------------------------------------------

	@unit_angle.setter
	def unit_angle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( value, float )
		
		self._unit_angle = value
	
	# def unit_angle ( self, value )

	# --------------------------------------------------

	@property
	def velocity ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		
		return self._velocity
	
	# def velocity ( self )

	# --------------------------------------------------

	@velocity.setter
	def velocity ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( value, int )
		
		self._velocity = value
	
	# def velocity ( self, value )

	# --------------------------------------------------

	@property
	def message ( self ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		
		return self._message
	
	# def message ( self )

	# --------------------------------------------------

	@message.setter
	def message ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TurnTableSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._message = value

	# def message ( self, value )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __repr__ ( self ):
		
		return '[TurnTableSettings: port={}, baud_rate={}, unit_angle={}, velocity={}]'.format(
			self._port,
			self._baud_rate,
			self._unit_angle,
			self._velocity
			)

# class TurnTableSettings ( pytools.serialization.Serializable )