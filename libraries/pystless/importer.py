# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import json
import numpy
import math

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.io
import pytools.assertions
import pytools.tasks

# LOCALS
from .                  import constants
from .importer_settings import ImporterSettings

def euler_to_rotation_matrix ( euler_angles ): #angles in radiant
    euler_angles = [euler_angles[0], euler_angles[1], euler_angles[2]]
    cos_x = math.cos(euler_angles[0])
    sin_x = math.sin(euler_angles[0])
    R_x = numpy.array([[1, 0, 0], [0, cos_x, -sin_x], [0, sin_x, cos_x]])
    cos_y = math.cos(euler_angles[1])
    sin_y = math.sin(euler_angles[1])
    R_y = numpy.array([[cos_y, 0, sin_y], [0, 1, 0], [-sin_y, 0, cos_y]])
    cos_z = math.cos(euler_angles[2])
    sin_z = math.sin(euler_angles[2])
    R_z = numpy.array([[cos_z, -sin_z, 0], [sin_z, cos_z, 0], [0, 0, 1]])
    R = numpy.dot(R_z, numpy.dot(R_y, R_x))
    return R

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pydataset.io.Importer ):
	"""
	Importer that imports raw s-tless dataset.
	
	GitHub:
		https://github.com/MichaelRamamonjisoa/SyntheT-Less
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		Arguments:
			self                                (`pystless.Importer`): Importer to initialize.
			settings                    (`pystless.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			       dst_group = dst_group,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def import_models ( self ):
		assert pytools.assertions.type_is_instance_of( self, Importer )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING MODELS:' )
			self.logger.log( '    models_directory_path: {}', self.settings.models_directory_path )
			self.logger.log( '    models_group_name: {}', self.settings.models_group_name )
			self.logger.flush()

		# Check
		if not self.settings.models_directory_path.is_a_directory():
			raise IOError( 'The directory "{}" does not exist'.format(self.settings.models_directory_path) )

		# Group where to import the models to
		models_group = self.dst_group.create_group( self.settings.models_group_name )

		# Log
		if self.logger:
			self.logger.log( '    models_group: {}', models_group.absolute_url )
			self.logger.end_line()
			self.logger.flush()

		# Go through each models
		for model_index in range( 1, 31 ):

			# Compose model name
			model_name = self.settings.models_examples_name_format.format( model_index )

			# Compose model filename
			model_filename = model_name + '.ply'

			# Compose model filepath
			model_filepath = self.settings.models_directory_path + pytools.path.FilePath( model_filename )

			# Log
			if self.logger:
				self.logger.log( 'MODEL {}:', model_index )
				self.logger.log( '    model_index: {}', model_index )
				self.logger.log( '    model_name: {}', model_name )
				self.logger.log( '    model_filename: {}', model_filename )
				self.logger.log( '    model_filepath: {}', model_filepath )
				self.logger.log( '    is_a_file: {}', model_filepath.is_a_file() )
				self.logger.flush()

			# Check
			if not model_filepath.is_a_file():
				raise IOError( 'The file "{}" does not exist'.format(model_filepath) )

			# Load model mesh
			model_data = pydataset.render.MeshData.load_ply( model_filepath )

			# Create an example for the model
			example = models_group.create_example( model_name )

			# Create category index feature
			if self.settings.category_index_feature_name:
				index_feature = example.create_or_update_feature(
					     feature_name = self.settings.category_index_feature_name,
					feature_data_type = pydataset.dataset.IntFeatureData,
					      auto_update = True,
					            value = model_index-1,
					)

			# Create category name feature
			if self.settings.category_name_feature_name:
				index_feature = example.create_or_update_feature(
					     feature_name = self.settings.category_name_feature_name,
					feature_data_type = pydataset.dataset.TextFeatureData,
					      auto_update = True,
					            value = model_name,
					)

			# Create one-hot category feature
			if self.settings.one_hot_category_feature_name:
				one_hot_feature = example.create_or_update_feature(
					     feature_name = self.settings.one_hot_category_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					            value = pydataset.classification_utils.index_to_one_hot( model_index-1, 30 ),
					)

			# Create mesh feature
			if self.settings.mesh_feature_name:
				mesh_feature = example.create_or_update_feature(
					     feature_name = self.settings.mesh_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = model_data,
					)

			# Log
			if self.logger:
				self.logger.log( '    example: {}', example.absolute_url )
				self.logger.log( '    index feature: {}', index_feature.absolute_url )
				self.logger.log( '    one-hot feature: {}', one_hot_feature.absolute_url )
				self.logger.log( '    mesh feature: {}', mesh_feature.absolute_url )
				self.logger.end_line()
				self.logger.flush()
		
		# for model_index in range( 1, 31 )

		# Log
		if self.logger:
			self.logger.log( 'DONE IMPORTING MODELS' )
			self.logger.end_line()
			self.logger.flush()

	# def import_models ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the s-tless dataset.

		Arguments:
			self (`pystless.Importer`): Importer used to import the linemod dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		
		# Log progress
		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# Log
		if ( self.logger ):
			self.logger.log( 'SYNTHETIC TLESS Importer:' )
			self.logger.log( '    dst_group_url:              {}', self.settings.dst_group_url )
			self.logger.log( '    raw_dataset_directory_path: {}', self.settings.raw_dataset_directory_path )
			self.logger.log( '    name:                       {}', self.settings.name )
			self.logger.log( 'SETTINGS:' )
			self.logger.log( '    contours_image_feature_name:  {}', self.settings.contours_image_feature_name )
			self.logger.log( '    depth_image_feature_name:     {}', self.settings.depth_image_feature_name )
			self.logger.log( '    color_image_feature_name:     {}', self.settings.color_image_feature_name )
			self.logger.log( '    instances_image_feature_name: {}', self.settings.instances_image_feature_name )
			self.logger.log( '    mask_image_feature_name:      {}', self.settings.mask_image_feature_name )
			self.logger.log( '    normals_image_feature_name:   {}', self.settings.normals_image_feature_name )
			self.logger.end_line()
			self.logger.flush()

		# Check the raw dataset directory
		if self.settings.raw_dataset_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(self.settings.raw_dataset_directory_path) )

		# Sub-files Check
		k_filepath          = self.settings.raw_dataset_directory_path + pytools.path.FilePath( 'K.txt' )
		jobs_train_filepath = self.settings.raw_dataset_directory_path + pytools.path.FilePath( 'jobs_train.txt' )
		jobs_val_filepath   = self.settings.raw_dataset_directory_path + pytools.path.FilePath( 'jobs_val.txt' )
		# Log
		if ( self.logger ):
			self.logger.log( 'Checking raw dataset directory and files:' )
			self.logger.log( '    files:' )
			self.logger.log( '    k_filepath:               {}', k_filepath )
			self.logger.log( '    jobs_train_filepath:      {}', jobs_train_filepath )
			self.logger.log( '    jobs_val_filepath:        {}', jobs_val_filepath )
			self.logger.flush()
		# Check
		if k_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(k_filepath) )
		if jobs_train_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(jobs_train_filepath) )
		if jobs_val_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(jobs_val_filepath) )

		# Sub-folders Check
		contours_directory_path  = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'contours'  )
		depths_directory_path    = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'depth'     )
		gt_poses_directory_path  = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'gt_poses'  )
		images_directory_path    = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'images'    )
		instances_directory_path = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'instances' )
		masks_directory_path     = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'mask'      )
		normals_directory_path   = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'normals'   )
		# Log
		if ( self.logger ):
			self.logger.log( '    directories:' )
			self.logger.log( '    contours_directory_path:  {}', contours_directory_path  )
			self.logger.log( '    depths_directory_path:    {}', depths_directory_path    )
			self.logger.log( '    gt_poses_directory_path:  {}', gt_poses_directory_path  )
			self.logger.log( '    images_directory_path:    {}', images_directory_path    )
			self.logger.log( '    instances_directory_path: {}', instances_directory_path )
			self.logger.log( '    masks_directory_path:     {}', masks_directory_path     )
			self.logger.log( '    normals_directory_path:   {}', normals_directory_path   )
			self.logger.end_line()
			self.logger.flush()
		# Check
		if contours_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(contours_directory_path) )
		if depths_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(depths_directory_path) )
		if gt_poses_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(gt_poses_directory_path) )
		if images_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(images_directory_path) )
		if instances_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(instances_directory_path) )
		if masks_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(masks_directory_path) )
		if normals_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(normals_directory_path) )
		# Contents
		contours_files  = contours_directory_path.list_contents()
		depths_files    = depths_directory_path.list_contents()
		gt_poses_files  = gt_poses_directory_path.list_contents()
		images_files    = images_directory_path.list_contents()
		instances_files = instances_directory_path.list_contents()
		masks_files     = masks_directory_path.list_contents()
		normals_files   = normals_directory_path.list_contents()
		# Check
		if len(contours_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(contours_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(depths_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(depths_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(gt_poses_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} json files.".format(gt_poses_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(images_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(images_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(instances_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(instances_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(masks_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(masks_directory_path, constants.NUMBER_OF_IMAGES) )
		if len(normals_files) != constants.NUMBER_OF_IMAGES:
			raise IOError( "The directory '{}' does not contain the {} png images.".format(normals_directory_path, constants.NUMBER_OF_IMAGES) )

		# Load camera parameters
		if self.settings.camera_matrix_feature_name:

			# Parse file
			K = k_filepath.read_ndarray( shape=(3, 3), dtype='float32' )
			
			# Create feature for camera parameters
			self.dst_group.dataset.create_or_update_feature(
				     feature_name = self.settings.camera_matrix_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = True,
				            value = K
				)

		# Load meshs
		self.import_models()

		# Load training images index
		training_index = jobs_train_filepath.read_lines()
		
		# Load validation images index
		validation_index = jobs_val_filepath.read_lines()
		
		# Create groups for training and validation
		training_group   = self.dst_group.create_group( 'training'   )
		validation_group = self.dst_group.create_group( 'validation' )

		# Import each image
		for image_index in range( constants.NUMBER_OF_IMAGES ):
			
			# Log
			# if ( self.logger ):
			# 	self.logger.log( 'Loading image {:05d}/{:05d}', image_index, constants.NUMBER_OF_IMAGES )
			# 	self.logger.flush()

			# Fetch the files names
			gt_poses_file  =  gt_poses_files[ image_index ]
			contours_file  =  contours_files[ image_index ]
			depth_file     =    depths_files[ image_index ]
			image_file     =    images_files[ image_index ]
			instances_file = instances_files[ image_index ]
			mask_file      =     masks_files[ image_index ]
			normals_file   =   normals_files[ image_index ]
			
			# Load images
			contours  = cv2.imread( str(contours_file),  cv2.IMREAD_GRAYSCALE )
			depth     = cv2.imread( str(depth_file),     cv2.IMREAD_ANYDEPTH  )
			image     = cv2.imread( str(image_file),     cv2.IMREAD_UNCHANGED )
			instances = cv2.imread( str(instances_file), cv2.IMREAD_GRAYSCALE )
			mask      = cv2.imread( str(mask_file),      cv2.IMREAD_GRAYSCALE )
			normals   = cv2.imread( str(normals_file),   cv2.IMREAD_UNCHANGED )

			# # Log
			# if ( self.logger ):
			# 	self.logger.log( '    gt_poses_file:  {}', gt_poses_file )
			# 	self.logger.log( '    contours_file:  {} shape={} dtype={}', contours_file, contours.shape, contours.dtype )
			# 	self.logger.log( '    depth_file:     {} shape={} dtype={}', depth_file, depth.shape, depth.dtype )
			# 	self.logger.log( '    images_file:    {} shape={} dtype={}', image_file, image.shape, image.dtype )
			# 	self.logger.log( '    instances_file: {} shape={} dtype={}', instances_file, instances.shape, instances.dtype )
			# 	self.logger.log( '    mask_file:      {} shape={} dtype={}', mask_file, mask.shape, mask.dtype )
			# 	self.logger.log( '    normals_file:   {} shape={} dtype={}', normals_file, normals.shape, normals.dtype )
			# 	self.logger.flush()

			# Check if the image belongs to training or validation
			image_filename = image_file.name()
			# training image
			if image_filename in training_index:
				target_group = training_group
			# validation image
			elif image_filename in validation_index:
				target_group = validation_group
			# error
			else:
				raise RuntimeError( 'Image "{}" does not belong to training or validation'.format(image_filename) )

			# Create group for image
			image_group = target_group.create_group( '{:05d}'.format(image_index) )

			# Contours
			if self.settings.contours_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.contours_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = contours,
					)

			# Depth
			if self.settings.depth_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.depth_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = depth,
					)

			# Image
			if self.settings.color_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.color_image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = False,
					            value = image,
					        extension = '.png',
					            param = 0
					)

			# instances
			if self.settings.instances_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.instances_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = instances,
					)

			# mask
			if self.settings.mask_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.mask_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = mask,
					)

			# normals
			if self.settings.normals_image_feature_name:
				image_group.create_or_update_feature(
					     feature_name = self.settings.normals_image_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					            value = normals,
					)
			
			# Load ground trouth
			gt_data = gt_poses_file.read_txt()
			if not gt_data:
				raise IOError( 'Failed to read file {}'.format(gt_poses_file) )
			# Parse ground trouth
			gt_data = json.loads( gt_data )
			
			# Remove "Plane":
			del gt_data[ 'Plane' ]

			# Import each object instance
			for instance_name in sorted( gt_data.keys() ):
				
				# Convert name to index
				instance_index = int( instance_name )

				# Category index
				category_index = int( gt_data[ instance_name ][ 'type' ] )

				# Referenced features location
				obj_name = self.settings.models_examples_name_format.format( category_index+1 )
				obj_path = 'groups/{}/examples/{}/features/'.format(
					self.settings.models_group_name,
					obj_name
					)			

				# Create example for instance
				instance_example = image_group.create_example( instance_name )
				
				# Instance index
				if self.settings.instance_index_feature_name:
					instance_example.create_or_update_feature(
						     feature_name = self.settings.instance_index_feature_name,
						feature_data_type = pydataset.dataset.IntFeatureData,
						      auto_update = False,
						            value = instance_index,
						)

				# Rotation
				if self.settings.rotation_feature_name:

					# Parse rotation
					r = gt_data[ instance_name ][ 'Euler' ]
					r = [ r[0]-180, -(180+r[1]), r[2] ] # Blender to OpenCV ?
					r = numpy.asarray( r, dtype=numpy.float32 )
					r = numpy.reshape( r, (3,) )
					r = pydataset.units.degree_to_radian( r )
					R = euler_to_rotation_matrix( r )
					
					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.rotation_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = R,
						            shape = pydataset.data.ShapeData( 3, 3 ),
						            dtype = numpy.dtype( 'float32' )
						)

				# Translation
				if self.settings.translation_feature_name:
					
					# Parse translation
					t = gt_data[ instance_name ][ 'T' ]
					t = [ -t[0]*1000., -t[1]*1000, t[2]*1000 ] # Blender to OpenCV ?
					t = numpy.asarray( t, dtype=numpy.float32 )
					t = numpy.reshape( t, (3,) )

					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.translation_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = t,
						            shape = pydataset.data.ShapeData( 3 ),
						            dtype = numpy.dtype( 'float32' )
						)

				# Visibility
				if self.settings.visibility_feature_name:
					
					# Parse visibility
					mask_pixels    = float( gt_data[ instance_name ][ 'mask_pixels'    ] )
					visible_pixels = float( gt_data[ instance_name ][ 'visible_pixels' ] )
					visibility     = (visible_pixels / mask_pixels) if mask_pixels > 0. else 0.

					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.visibility_feature_name,
						feature_data_type = pydataset.dataset.FloatFeatureData,
						      auto_update = False,
						            value = visibility,
						)

				# Category Index
				if self.settings.category_index_feature_name:
					
					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.category_index_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = obj_path + self.settings.category_index_feature_name,
						)

				# Category Name
				if self.settings.category_name_feature_name:
					
					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.category_name_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = obj_path + self.settings.category_name_feature_name,
						)

				# One-Hot Category
				if self.settings.one_hot_category_feature_name:

					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.one_hot_category_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = obj_path + self.settings.one_hot_category_feature_name,
						)

				# Mesh
				if self.settings.mesh_feature_name:
					
					# Create feature
					instance_example.create_or_update_feature(
						     feature_name = self.settings.mesh_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = obj_path + self.settings.mesh_feature_name,
						)

				# Update
				instance_example.update()

			# Update image group
			image_group.update()

			# Log progress
			if self.progress_tracker:
				self.progress_tracker.update( float(image_index)/float(constants.NUMBER_OF_IMAGES) )

		# Log progress
		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a s-tless improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the s-tless importer settings constructor.

		Returns:
			`pystless.ImporterSettings`: The settings.
		"""
		return ImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.io.Importer )