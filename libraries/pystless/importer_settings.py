# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.io
import pytools.assertions
import pytools.path
import pytools.serialization

# ##################################################
# ###          CLASS IMPORTER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImporterSettings',
	   namespace = 'pystless',
	fields_names = [
		# directory
		'raw_dataset_directory_path',
		'models_directory_path',
		'models_group_name',
		'models_examples_name_format',
		# common
		'camera_matrix_feature_name',
		'mesh_feature_name',
		# per image
		'contours_image_feature_name',
		'depth_image_feature_name',
		'color_image_feature_name',
		'instances_image_feature_name',
		'mask_image_feature_name',
		'normals_image_feature_name',
		# per object instance
		'instance_index_feature_name',
		'category_index_feature_name',
		'category_name_feature_name',
		'one_hot_category_feature_name',
		'translation_feature_name',
		'rotation_feature_name',
		'visibility_feature_name'
		]
	)
class ImporterSettings ( pydataset.io.ImporterSettings ):
	"""
	Dataset importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dst_group_url, raw_dataset_directory_path, models_directory_path,
		            models_group_name = 'objects',
		  models_examples_name_format = 'obj_{:02d}',
		# common
		   camera_matrix_feature_name = 'K',
		            mesh_feature_name = 'mesh',
		# per image
		  contours_image_feature_name = 'contours',
		     depth_image_feature_name = 'depth',
		     color_image_feature_name = 'image',
		 instances_image_feature_name = 'instances',
		      mask_image_feature_name = 'mask',
		   normals_image_feature_name = 'normals',
		# per object instance
		  instance_index_feature_name = 'instance_index',
		  category_index_feature_name = 'category_index',
		   category_name_feature_name = 'category_name',
		one_hot_category_feature_name = 'one_hot_category',
		     translation_feature_name = 't',
		        rotation_feature_name = 'R',
		      visibility_feature_name = 'visibility',
		# other
		                         name = 'stless-importer'
		):
		"""
		Initializes a new instance of the synthetic tless importer settings class.

		Arguments:
			self                             (`pylinemod.ImporterSettings`): Instance to initialize.
			dst_group_url                                           (`str`): URL of the group where to import data to.
			raw_dataset_directory_path (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the raw linemod dataset.
			models_directory_path      (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the raw linemod dataset.
			models_group_name                                       (`str`): Name of the group in which to import the model.
			models_examples_name_format                             (`str`): Name format for the example where to save the model data and feautres.
			camera_matrix_feature_name                              (`str`): Name of the feature where to save the camera parameters.
			mesh_feature_name                                       (`str`): Name of the feature where to save the mesh.
			contours_image_feature_name                             (`str`): Name of the feature where to save the objets contours image.
			depth_image_feature_name                                (`str`): Name of the feature where to save the depth image.
			color_image_feature_name                                (`str`): Name of the feature where to save the color image.
			instances_image_feature_name                            (`str`): Name of the feature where to save the instances image.
			mask_image_feature_name                                 (`str`): Name of the feature where to save the mask image.
			normals_image_feature_name                              (`str`): Name of the feature where to save the normals image.
			instance_index_feature_name                             (`str`): Name of the feature where to save the instance index.
			category_index_feature_name                             (`str`): Name of the feature where to save the category index.
			category_name_feature_name                              (`str`): Name of the feature where to save the category name.
			one_hot_category_feature_name                           (`str`): Name of the feature where to save the one hot category.
			translation_feature_name                                (`str`): Name of the feature where to save the translation vector.
			rotation_feature_name                                   (`str`): Name of the feature where to save the rotation matrix.
			visibility_feature_name                                 (`str`): Name of the feature where to save the visibility proportion.
			name                                                    (`str`): Name of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is_instance_of( raw_dataset_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is_instance_of( models_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( models_group_name, str )
		assert pytools.assertions.type_is( models_examples_name_format, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( mesh_feature_name, str )
		assert pytools.assertions.type_is( contours_image_feature_name, str )
		assert pytools.assertions.type_is( depth_image_feature_name, str )
		assert pytools.assertions.type_is( color_image_feature_name, str )
		assert pytools.assertions.type_is( instances_image_feature_name, str )
		assert pytools.assertions.type_is( mask_image_feature_name, str )
		assert pytools.assertions.type_is( normals_image_feature_name, str )
		assert pytools.assertions.type_is( instance_index_feature_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( visibility_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( ImporterSettings, self ).__init__( dst_group_url, name )

		if isinstance( raw_dataset_directory_path, str ):
			raw_dataset_directory_path = pytools.path.DirectoryPath( raw_dataset_directory_path )

		if isinstance( models_directory_path, str ):
			models_directory_path = pytools.path.DirectoryPath( models_directory_path )

		self._raw_dataset_directory_path  = raw_dataset_directory_path
		self._models_directory_path       = models_directory_path
		self._models_group_name           = models_group_name
		self._models_examples_name_format = models_examples_name_format
		# common
		self._camera_matrix_feature_name = camera_matrix_feature_name
		self._mesh_feature_name          = mesh_feature_name
		# per-images
		self._contours_image_feature_name  = contours_image_feature_name
		self._depth_image_feature_name     = depth_image_feature_name
		self._color_image_feature_name     = color_image_feature_name
		self._instances_image_feature_name = instances_image_feature_name
		self._mask_image_feature_name      = mask_image_feature_name
		self._normals_image_feature_name   = normals_image_feature_name
		# per object instance
		self._instance_index_feature_name   = instance_index_feature_name
		self._category_index_feature_name   = category_index_feature_name
		self._category_name_feature_name    = category_name_feature_name
		self._one_hot_category_feature_name = one_hot_category_feature_name
		self._translation_feature_name      = translation_feature_name
		self._rotation_feature_name         = rotation_feature_name
		self._visibility_feature_name       = visibility_feature_name

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def raw_dataset_directory_path ( self ):
		"""
		Path to the directory containing the raw linemod dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._raw_dataset_directory_path

	# def raw_dataset_directory_path ( self )
	
	# --------------------------------------------------

	@raw_dataset_directory_path.setter
	def raw_dataset_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, pytools.path.DirectoryPath )

		self._raw_dataset_directory_path = value
		
	# def raw_dataset_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def models_directory_path ( self ):
		"""
		Path to the directory containing the raw linemod dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._models_directory_path

	# def models_directory_path ( self )
	
	# --------------------------------------------------

	@models_directory_path.setter
	def models_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )
		
		if isinstance( value, str ):
			value = pytools.path.DirectoryPath( value )

		self._models_directory_path = value
		
	# def models_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def models_group_name ( self ):
		"""
		Name of the group in which to import the model (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._models_group_name
	
	# def models_group_name ( self )

	# --------------------------------------------------

	@models_group_name.setter
	def models_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._models_group_name = value
	
	# def models_group_name ( self, value )

	# --------------------------------------------------

	@property
	def models_examples_name_format ( self ):
		"""
		Name format for the example where to save the model data and feautres (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._models_examples_name_format
	
	# def models_examples_name_format ( self )

	# --------------------------------------------------

	@models_examples_name_format.setter
	def models_examples_name_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._models_examples_name_format = value
	
	# def models_examples_name_format ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_feature_name ( self ):
		"""
		Name of the feature where to save the objects mesh (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._mesh_feature_name
	
	# def mesh_feature_name ( self )

	# --------------------------------------------------

	@mesh_feature_name.setter
	def mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def contours_image_feature_name ( self ):
		"""
		Name of the feature containing the objets contours image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._contours_image_feature_name
	
	# def contours_image_feature_name ( self )

	# --------------------------------------------------

	@contours_image_feature_name.setter
	def contours_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._contours_image_feature_name = value
	
	# def contours_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_image_feature_name ( self ):
		"""
		Name of the feature containing the color image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._depth_image_feature_name
	
	# def depth_image_feature_name ( self )

	# --------------------------------------------------

	@depth_image_feature_name.setter
	def depth_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_image_feature_name = value
	
	# def depth_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def color_image_feature_name ( self ):
		"""
		Name of the feature containing the color image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._color_image_feature_name
	
	# def color_image_feature_name ( self )

	# --------------------------------------------------

	@color_image_feature_name.setter
	def color_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._color_image_feature_name = value
	
	# def color_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def instances_image_feature_name ( self ):
		"""
		Name of the feature containing the instances image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._instances_image_feature_name
	
	# def instances_image_feature_name ( self )

	# --------------------------------------------------

	@instances_image_feature_name.setter
	def instances_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._instances_image_feature_name = value
	
	# def instances_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_image_feature_name ( self ):
		"""
		Name of the feature containing the mask image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._mask_image_feature_name
	
	# def mask_image_feature_name ( self )

	# --------------------------------------------------

	@mask_image_feature_name.setter
	def mask_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mask_image_feature_name = value
	
	# def mask_image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def normals_image_feature_name ( self ):
		"""
		Name of the feature containing the normals image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._normals_image_feature_name
	
	# def normals_image_feature_name ( self )

	# --------------------------------------------------

	@normals_image_feature_name.setter
	def normals_image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._normals_image_feature_name = value
	
	# def normals_image_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def instance_index_feature_name ( self ):
		"""
		Name of the feature containing the instance index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._instance_index_feature_name
	
	# def instance_index_feature_name ( self )

	# --------------------------------------------------

	@instance_index_feature_name.setter
	def instance_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._instance_index_feature_name = value
	
	# def instance_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature containing the category index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._category_index_feature_name
	
	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )

	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._translation_feature_name
	
	# def translation_feature_name ( self )

	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_feature_name = value
	
	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._rotation_feature_name
	
	# def rotation_feature_name ( self )

	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_feature_name = value
	
	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visibility_feature_name ( self ):
		"""
		Name of the feature containing the visibility proportion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._visibility_feature_name
	
	# def visibility_feature_name ( self )

	# --------------------------------------------------

	@visibility_feature_name.setter
	def visibility_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._visibility_feature_name = value
	
	# def visibility_feature_name ( self, value )

# class ImporterSettings ( pydataset.io.ImporterSettings )