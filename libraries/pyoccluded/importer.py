# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .                  import constants
from .importer_settings import ImporterSettings

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pydataset.io.Importer ):
	"""
	Importer that imports raw pyoccluded dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new pyoccluded importer instance.

		Arguments:
			self                              (`pyoccluded.Importer`): Importer to initialize.
			settings                  (`pyoccluded.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the occlued dataset.

		Arguments:
			self (`pyoccluded.Importer`): Importer used to import the occluded dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		
		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# Log
		if ( self.logger ):
			self.logger.log( 'IMPOERTER SETTINGS:' )
			for setting_name in ImporterSettings.__fields_names__:
				self.logger.log( '    {:30s}: {}', setting_name, getattr( self.settings, setting_name ) )
			self.logger.end_line()
			self.logger.flush()
			
		# Dataset sub-folders
		scenes_directory_path = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'test' )
		models_directory_path = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( 'models' )

		if ( self.logger ):
			self.logger.log( 'DATASET DIRECTORY' )
			self.logger.log( '    raw_dataset_directory_path: {}', self.settings.raw_dataset_directory_path )
			self.logger.log( '    scenes_directory_path:      {}', scenes_directory_path )
			self.logger.log( '    models_directory_path:      {}', models_directory_path )
			self.logger.end_line()
			self.logger.flush()

		# Check directories
		if self.settings.raw_dataset_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(self.settings.raw_dataset_directory_path) )
		if scenes_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(scenes_directory_path) )
		if models_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(models_directory_path) )

		# Log
		if ( self.logger ):
			self.logger.log( '    raw_dataset_directory_path: OK' )
			self.logger.log( '    lmo_directory_path:         OK' )
			self.logger.log( '    scenes_directory_path:      OK' )
			self.logger.log( '    models_directory_path:      OK' )
			self.logger.end_line()
			self.logger.flush()

		# Create the groups where to save the objects
		if '/' in self.settings.objects_group_name:
			dst_objects_group = self.dst_group.create( self.settings.objects_group_name )
		else:
			dst_objects_group = self.dst_group.create_group( self.settings.objects_group_name )
		# Create the groups where to save the scenes
		if '/' in self.settings.scenes_group_name:
			dst_scenes_group = self.dst_group.create( self.settings.scenes_group_name )
		else:
			dst_scenes_group = self.dst_group.create_group( self.settings.scenes_group_name )

		# Log
		if ( self.logger ):
			self.logger.log( 'DESTINATION GROUPS' )
			self.logger.log( '    dst_objects_group: {}', dst_objects_group.url )
			self.logger.log( '    dst_scenes_group: {}', dst_scenes_group.url )
			self.logger.end_line()
			self.logger.flush()
		
		# Locate camera infos file
		camera_filepath = self.settings.raw_dataset_directory_path + pytools.path.FilePath( 'camera.json' )

		# Log
		if ( self.logger ):
			self.logger.log( 'CAMERA DATA' )
			self.logger.log( '    camera_filepath: {}', camera_filepath )
			self.logger.flush()

		# Check camera infos file
		if camera_filepath.is_not_a_file():
			raise IOError( "The camera file '{}' does not exist".format(camera_filepath) )

		camera_data = camera_filepath.read_json()
		cx          = float( camera_data[ 'cx' ] )
		cy          = float( camera_data[ 'cy' ] )
		fx          = float( camera_data[ 'fx' ] )
		fy          = float( camera_data[ 'fy' ] )
		depth_scale = float( camera_data[ 'depth_scale' ] )
		width       = float( camera_data[ 'width' ] )
		height      = float( camera_data[ 'height' ] )
		
		# Log
		if ( self.logger ):
			self.logger.log( '    camera_filepath: OK' )
			self.logger.log( '    cx:              {}', cx )
			self.logger.log( '    cy:              {}', cy )
			self.logger.log( '    fx:              {}', fx )
			self.logger.log( '    fy:              {}', fy )
			self.logger.log( '    depth_scale:     {}', depth_scale )
			self.logger.log( '    width:           {}', width )
			self.logger.log( '    height:          {}', height )
			self.logger.end_line()
			self.logger.flush()

		# Load kinect camera parameters
		K = numpy.asarray([
			[fx, 0., cx],
			[0., fy, cy],
			[0., 0., 1.]
			],
			dtype=numpy.float32
			)
		K = numpy.reshape( K, [3, 3] ).astype( numpy.float32 )

		# Create feature for kinect camera parameters
		if self.settings.camera_matrix_feature_name:
			K_f = dst_scenes_group.dataset.create_feature(
				     feature_name = self.settings.camera_matrix_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( 3, 3 ),
				            value = K,
				            dtype = numpy.dtype('float32')
				)
		if self.settings.depth_scale_feature_name:
			d_f = dst_scenes_group.dataset.create_feature(
				     feature_name = self.settings.depth_scale_feature_name,
				feature_data_type = pydataset.dataset.FloatFeatureData,
				      auto_update = True,
				            value = depth_scale
				)

		# Log
		if ( self.logger ):
			self.logger.log( 'CAMERA FEATURES' )
			self.logger.log( '    K:           {}', K_f.url )
			self.logger.log( '    depth_scale: {}', d_f.url )
			self.logger.end_line()
			self.logger.flush()

		if self.progress_tracker:
			self.progress_tracker.update( 0.01 )
			models_tracker = None#self.progress_tracker.create_child()
			images_tracker = self.progress_tracker.create_child()
		else:
			models_tracker = None
			images_tracker = None

		# Import models
		self.import_models( models_directory_path, dst_objects_group, models_tracker )
		
		if self.progress_tracker:
			self.progress_tracker.update( 0.02 )

		# Import scenes
		self.import_scenes( scenes_directory_path, dst_scenes_group, images_tracker )

		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# --------------------------------------------------

	def import_models ( self, models_directory_path, dst_objects_group, progress_tracker=None ):

		# Update Progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if ( self.logger ):
			self.logger.log( 'LOADING MODELS:' )
			self.logger.end_line()
			self.logger.flush()

		# Locate models info file
		models_info_filepath = models_directory_path + pytools.path.FilePath( 'models_info.json' )

		# Log
		if ( self.logger ):
			self.logger.log( 'MODELS INFO:' )
			self.logger.log( '    models_info_filepath: {}', models_info_filepath )
			self.logger.flush()

		# Check models info file
		if models_info_filepath.is_not_a_file():
			raise IOError( "The models info file '{}' does not exist".format(models_info_filepath) )

		# Log
		if ( self.logger ):
			self.logger.log( '    models_info_filepath: OK' )
			self.logger.flush()

		# Parse models infos
		models_info = models_info_filepath.read_json()
		number_of_models = len( models_info )

		# Log
		if ( self.logger ):
			self.logger.log( '    number_of_models:     {}', number_of_models )
			self.logger.end_line()
			self.logger.flush()

		# Parse each model
		i = 0.
		for str_model_index, model_info in models_info.items():
			
			# Parse infos
			model_index   = int(str_model_index)
			diameter      = float( model_info['diameter'] )
			min_x         = float( model_info['min_x'] )
			min_y         = float( model_info['min_y'] )
			min_z         = float( model_info['min_z'] )
			size_x        = float( model_info['size_x'] )
			size_y        = float( model_info['size_y'] )
			size_z        = float( model_info['size_z'] )
			mesh_filepath = models_directory_path + pytools.path.FilePath.format( 'obj_{:06d}.ply', model_index )
			
			# Log
			if ( self.logger ):
				self.logger.log( 'MODEL {}:', str_model_index )
				self.logger.log( '    INFOS:' )
				self.logger.log( '        {:16s}: {}', 'model index', model_index )
				self.logger.log( '        {:16s}: {}', 'diameter', diameter )
				self.logger.log( '        {:16s}: {}', 'min_x', min_x )
				self.logger.log( '        {:16s}: {}', 'min_y', min_y )
				self.logger.log( '        {:16s}: {}', 'min_z', min_z )
				self.logger.log( '        {:16s}: {}', 'size_x', size_x )
				self.logger.log( '        {:16s}: {}', 'size_y', size_y )
				self.logger.log( '        {:16s}: {}', 'size_z', size_z )
				self.logger.log( '        {:16s}: {}', 'mesh_filepath', mesh_filepath )
			
			# Get category index, category name, one hot category
			category_index = constants.MODELS_INDEXES[ model_index ]
			category_name  = constants.MODELS_NAMES[ model_index ]
			one_hot_category = pydataset.classification_utils.index_to_one_hot( category_index, constants.NUMBER_OF_CATEGORIES )

			# # Compute Bounding Box
			# max_x = min_x + size_x
			# max_y = min_y + size_y
			# max_z = min_z + size_z
			# bounding_box = numpy.asarray([
			# 	[min_x, min_y, min_z],
			# 	[max_x, min_y, min_z],
			# 	[max_x, max_y, min_z],
			# 	[min_x, max_y, min_z],
			# 	[min_x, min_y, max_z],
			# 	[max_x, min_y, max_z],
			# 	[max_x, max_y, max_z],
			# 	[min_x, max_y, max_z]
			# 	],
			# 	dtype=numpy.float32
			# 	)

			if ( self.logger ):
				self.logger.log( '    CATEGORY:' )
				self.logger.log( '        {:16s}: {}', 'category_index',   category_index )
				self.logger.log( '        {:16s}: {}', 'one_hot_category', one_hot_category )
				self.logger.log( '        {:16s}: {}', 'category_name',    category_name )

			# Check
			if mesh_filepath.is_not_a_file():
				raise IOError( "The mesh file '{}' does not exist".format(mesh_filepath) )

			# Load the ply mesh
			mesh_data = pydataset.render.MeshData.load_ply( mesh_filepath )

			# Scale model
			mesh_data.points /= 1000.
			
			# Log
			if ( self.logger ):
				self.logger.log( '    MESH DATA:' )
				self.logger.log( '        {:16s}: {}', 'primitive', mesh_data.primitive )
				self.logger.log( '        {:16s}: {}', 'points',    mesh_data.points.shape[0] if mesh_data.points is not None else None )
				self.logger.log( '        {:16s}: {}', 'colors',    mesh_data.colors.shape[0] if mesh_data.colors is not None else None )
				self.logger.log( '        {:16s}: {}', 'normals',   mesh_data.normals.shape[0] if mesh_data.normals is not None else None )
				self.logger.log( '        {:16s}: {}', 'indexes',   mesh_data.indexes.shape[0] if mesh_data.indexes is not None else None )
				self.logger.flush()

			# Log
			if ( self.logger ):
				self.logger.log( '    FEATURES:' )
				self.logger.flush()

			# Create Model Example
			example = dst_objects_group.create_example( category_name, auto_update=True )

			# Log
			if ( self.logger ):
				self.logger.log( '        {:16s}: {}', 'example', example.url )
				self.logger.flush()

			if self.settings.category_index_feature_name:
				f = example.create_feature(
					     feature_name = self.settings.category_index_feature_name,
					feature_data_type = pydataset.dataset.IntFeatureData,
					      auto_update = False,
					            value = category_index
					)
				if ( self.logger ):
					self.logger.log( '        {:16s}: {}', self.settings.category_index_feature_name, f.url )
					self.logger.flush()

			if self.settings.one_hot_category_feature_name:
				f = example.create_feature(
					     feature_name = self.settings.one_hot_category_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = False,
					            value = one_hot_category
					)
				if ( self.logger ):
					self.logger.log( '        {:16s}: {}', self.settings.one_hot_category_feature_name, f.url )
					self.logger.flush()

			if self.settings.category_name_feature_name:
				f = example.create_feature(
					     feature_name = self.settings.category_name_feature_name,
					feature_data_type = pydataset.dataset.TextFeatureData,
					      auto_update = False,
					            value = category_name
					)
				if ( self.logger ):
					self.logger.log( '        {:16s}: {}', self.settings.category_name_feature_name, f.url )
					self.logger.flush()

			if self.settings.mesh_feature_name:
				f = example.create_feature(
					     feature_name = self.settings.mesh_feature_name,
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = False,
					            value = mesh_data
					)
				if ( self.logger ):
					self.logger.log( '        {:16s}: {}', self.settings.mesh_feature_name, f.url )
					self.logger.end_line()
					self.logger.flush()
			
			# Update example
			example.update()

			# Update Progression
			i += 1.
			if progress_tracker:
				progress_tracker.update( i / number_of_models )
		
		# for str_model_index, model_info in models_info.items()

		# Update Progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_models ( self, models_directory_path, dst_objects_group, progress_tracker=None )

	# --------------------------------------------------

	def import_scenes ( self, scenes_directory_path, dst_scenes_group, progress_tracker=None ):

		# List scene directories
		scenes_directories = scenes_directory_path.list_contents()

		# Only keep directories
		scenes_directories = list(filter( lambda p: p.is_a_directory(), scenes_directories ))

		# Count them
		number_of_scenes_directories = len( scenes_directories )

		# Create progress trackers
		if progress_tracker is not None:
			trackers = [progress_tracker.create_child() for i in range(number_of_scenes_directories)]
		else:
			trackers = [None for i in range(number_of_scenes_directories)]

		# Import each scene
		for i in range(number_of_scenes_directories):

			# Get the directory
			scene_directory = scenes_directories[ i ]

			# Create a group for the scene
			dst_scene_group = dst_scenes_group.create_group( scene_directory.name() )

			# impoty the scene
			self.import_scene( scene_directory, dst_scene_group, trackers[i] )
		
		# for i in range(number_of_scenes_directories)

	# def import_scenes ( self, scenes_directory, dst_scenes_group, progress_tracker )
	
	# --------------------------------------------------

	def import_scene ( self, scene_directory, dst_scene_group, progress_tracker=None ):

		# Update progression
		if progress_tracker:
			progress_tracker.update( 0. )

		# Log
		if self.logger:
			self.logger.log( 'IMPORTING SCENE {}', scene_directory.name() )

		if self.settings.depth_feature_name:
			depth_directory_path = scene_directory + pytools.path.DirectoryPath( 'depth' )
			# Log
			if self.logger:
				self.logger.log( 'depth_directory_path: {}', depth_directory_path )
			# Check
			if depth_directory_path.is_not_a_directory():
				raise IOError( "The directory '{}' does not exist".format(depth_directory_path) )
			# Log
			if self.logger:
				self.logger.log( 'depth_directory_path: OK' )

		if self.settings.mask_feature_name:
			mask_directory_path = scene_directory + pytools.path.DirectoryPath( 'mask' )
			# Log
			if self.logger:
				self.logger.log( 'mask_directory_path: {}', mask_directory_path )
			# Check
			if mask_directory_path.is_not_a_directory():
				raise IOError( "The directory '{}' does not exist".format(mask_directory_path) )
			# Log
			if self.logger:
				self.logger.log( 'mask_directory_path: OK' )
		
		if self.settings.visible_mask_feature_name:
			visible_mask_directory_path = scene_directory + pytools.path.DirectoryPath( 'mask_visib' )
			# Log
			if self.logger:
				self.logger.log( 'visible_mask_directory_path: {}', visible_mask_directory_path )
			# Check
			if visible_mask_directory_path.is_not_a_directory():
				raise IOError( "The directory '{}' does not exist".format(visible_mask_directory_path) )
			# Log
			if self.logger:
				self.logger.log( 'visible_mask_directory_path: OK' )
		
		if self.settings.image_feature_name:
			image_directory_path = scene_directory + pytools.path.DirectoryPath( 'rgb' )
			# Log
			if self.logger:
				self.logger.log( 'image_directory_path: {}', image_directory_path )
			# Check
			if image_directory_path.is_not_a_directory():
				raise IOError( "The directory '{}' does not exist".format(image_directory_path) )
			# Log
			if self.logger:
				self.logger.log( 'image_directory_path: OK' )

		# Label files
		scene_camera_filepath  = scene_directory + pytools.path.FilePath( 'scene_camera.json' )
		scene_gt_filepath      = scene_directory + pytools.path.FilePath( 'scene_gt.json' )
		scene_gt_info_filepath = scene_directory + pytools.path.FilePath( 'scene_gt_info.json' )
		
		# Log
		if self.logger:
			self.logger.log( 'scene_camera_filepath:  {}', scene_camera_filepath )
			self.logger.log( 'scene_gt_filepath:      {}', scene_gt_filepath )
			self.logger.log( 'scene_gt_info_filepath: {}', scene_gt_info_filepath )

		# Check
		if scene_camera_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_camera_filepath) )
		if scene_gt_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_gt_filepath) )
		if scene_gt_info_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(scene_gt_info_filepath) )
		
		# Log
		if self.logger:
			self.logger.log( 'scene_camera_filepath:  OK' )
			self.logger.log( 'scene_gt_filepath:      OK' )
			self.logger.log( 'scene_gt_info_filepath: OK' )

		# Load ground truth
		scene_camera_data  = scene_camera_filepath.read_json()
		scene_gt_data      = scene_gt_filepath.read_json()
		scene_gt_info_data = scene_gt_info_filepath.read_json()

		# Number of images
		number_of_images = len( scene_camera_data )

		# Import each image
		for image_index in range( number_of_images ):
			
			# Get ground trouth of the image
			image_key          = '{}'.format( image_index )
			image_name         = '{:06d}'.format( image_index )
			image_camera_data  = scene_camera_data[ image_key ]
			image_gt_data      = scene_gt_data[ image_key ]
			image_gt_info_data = scene_gt_info_data[ image_key ]

			# Create a group for the image
			image_group = dst_scene_group.create_group( image_name )

			# K
			if self.settings.camera_matrix_feature_name:
				
				# Fetch
				K = image_camera_data[ 'cam_K' ]

				# Parse
				K = numpy.asarray( K, dtype=numpy.float32 )
				K = numpy.reshape( K, [3, 3] )

				# Create feature
				image_group.create_feature(
					     feature_name = self.settings.camera_matrix_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					      auto_update = True,
					            value = K
					)

			# Depth scale
			if self.settings.depth_scale_feature_name:
				
				# Fetch
				depth_scale = image_camera_data[ 'depth_scale' ]
				
				# Parse
				depth_scale = float( depth_scale )

				# Create feature
				image_group.create_feature(
					     feature_name = self.settings.depth_scale_feature_name,
					feature_data_type = pydataset.dataset.FloatFeatureData,
					      auto_update = True,
					            value = depth_scale
					)

			# RGB image
			if self.settings.image_feature_name:
				
				# Path
				image_filepath = image_directory_path + pytools.path.FilePath.format( '{}.png', image_name )
				
				# Check
				if image_filepath.is_a_directory():
					raise IOError( "The file '{}' does not exist".format(image_filepath) )

				# Read
				image = cv2.imread( str(image_filepath), cv2.IMREAD_UNCHANGED )

				# Create feature
				image_group.create_feature(
					     feature_name = self.settings.image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					            value = image,
					        extension = '.png',
					            param = 0,
					)

			# Depth image
			if self.settings.depth_feature_name:
				
				# Path
				depth_filepath = depth_directory_path + pytools.path.FilePath.format( '{}.png', image_name )
				
				# Check
				if depth_filepath.is_a_directory():
					raise IOError( "The file '{}' does not exist".format(depth_filepath) )

				# Read
				depth = cv2.imread( str(depth_filepath), cv2.IMREAD_UNCHANGED )

				# Create feature
				image_group.create_feature(
					     feature_name = self.settings.depth_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
					      auto_update = True,
					            value = depth,
					        extension = '.png',
					            param = 0,
					)

			# Import each object instance
			number_of_instances = len( image_gt_data )
			for instance_index in range( number_of_instances ):

				# Key and name of the instance
				instance_name = '{:06d}'.format( instance_index )

				# Fetch labels
				instance_gt_data      = image_gt_data[ instance_index ]
				instance_gt_info_data = image_gt_info_data[ instance_index ]
				
				# Get the category of the model
				category_index = instance_gt_data[ 'obj_id' ]
				category_name  = constants.MODELS_NAMES[ category_index ]
				model_url      = 'groups/{}/examples/{}'.format( self.settings.objects_group_name, category_name )
				
				# Create an example for this instance
				instance_example = image_group.create_example( instance_name )

				# Get ground truth info
				bbox_obj       = instance_gt_info_data[ 'bbox_obj'       ]
				bbox_visib     = instance_gt_info_data[ 'bbox_visib'     ]
				px_count_all   = instance_gt_info_data[ 'px_count_all'   ]
				px_count_valid = instance_gt_info_data[ 'px_count_valid' ]
				px_count_visib = instance_gt_info_data[ 'px_count_visib' ]
				visib_fract    = instance_gt_info_data[ 'visib_fract'    ]
				
				# R
				if self.settings.rotation_matrix_feature_name:
					
					# Fetch
					R = instance_gt_data[ 'cam_R_m2c' ]

					# Parse
					R = numpy.asarray( R, dtype=numpy.float32 )
					R = numpy.reshape( R, [3, 3] )

					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.rotation_matrix_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = R
						)

				# t
				if self.settings.translation_vector_feature_name:
					
					# Fetch
					t = instance_gt_data[ 'cam_t_m2c' ]

					# Parse
					t = numpy.asarray( t, dtype=numpy.float32 )
					t = numpy.reshape( t, [3] )
					t /= 1000. # mm -> m

					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.translation_vector_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = t
						)

				# Category index
				if self.settings.category_index_feature_name:
					
					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.category_index_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( model_url, self.settings.category_index_feature_name )
						)

				# One Hot Category
				if self.settings.one_hot_category_feature_name:
					
					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.one_hot_category_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( model_url, self.settings.one_hot_category_feature_name )
						)

				# Category name
				if self.settings.category_name_feature_name:
					
					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.category_name_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( model_url, self.settings.category_name_feature_name )
						)

				# Mesh
				if self.settings.mesh_feature_name:
					
					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.mesh_feature_name,
						feature_data_type = pydataset.dataset.FeatureRefData,
						      auto_update = False,
						            value = '{}/features/{}'.format( model_url, self.settings.mesh_feature_name )
						)

				# Mask
				if self.settings.mask_feature_name:
					
					# Path
					mask_filepath = mask_directory_path + pytools.path.FilePath.format( '{}_{}.png', image_name, instance_name )
				
					# Check
					if mask_filepath.is_a_directory():
						raise IOError( "The file '{}' does not exist".format(mask_filepath) )

					# Read
					mask = cv2.imread( str(mask_filepath), cv2.IMREAD_UNCHANGED )
					
					# Parse
					mask = mask.astype( numpy.bool )

					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = mask
						)
				
				# Mask
				if self.settings.visible_mask_feature_name:
					
					# Path
					visible_mask_filepath = visible_mask_directory_path + pytools.path.FilePath.format( '{}_{}.png', image_name, instance_name )
				
					# Check
					if visible_mask_filepath.is_a_directory():
						raise IOError( "The file '{}' does not exist".format(visible_mask_filepath) )

					# Read
					mask = cv2.imread( str(visible_mask_filepath), cv2.IMREAD_UNCHANGED )
					
					# Parse
					mask = mask.astype( numpy.bool )

					# Create feature
					instance_example.create_feature(
						     feature_name = self.settings.visible_mask_feature_name,
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						      auto_update = False,
						            value = mask
						)

				# Update the instance example
				instance_example.update()

			# for instance_index in range( number_of_instances )

			# Update progression
			if progress_tracker:
				progress_tracker.update( float(image_index)/float(number_of_images) )

		# for image_index in range( number_of_images )

		# Update progression
		if progress_tracker:
			progress_tracker.update( 1. )

	# def import_scene ( self, scene_directory, dst_scene_group, progress_tracker )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a occluded improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the occluded importer settings constructor.

		Returns:
			`pytools.occluded.ImporterSettings`: The settings.
		"""
		return ImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.io.Importer )