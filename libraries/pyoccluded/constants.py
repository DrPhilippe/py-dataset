
"""
Nnumber of images in the dataset (`int`).
"""
NUMBER_OF_IMAGES = 1213

"""
Nnumber of categories in the dataset (`int`).
"""
NUMBER_OF_CATEGORIES = 8

"""
Names of the LINEMOD models (`dict` of `8` `int`s to `str`s).
"""
MODELS_NAMES = {
	1: 'ape',
	5: 'can',
	6: 'cat',
	8: 'driller',
	9: 'duck',
	10: 'eggbox',
	11: 'glue',
	12: 'holepuncher'
	}

"""
Indexes of the LINEMOD models (`dict` of `8` `int`s to `int`s).
"""
MODELS_INDEXES = {
	 1: 0,
	 5: 1,
	 6: 2,
	 8: 3,
	 9: 4,
	10: 5,
	11: 6,
	12: 7
	}