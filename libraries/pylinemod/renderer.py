# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import gc
import numpy

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.render
import pytools.assertions

# LOCALS
from .renderer_settings import RendererSettings

# ##################################################
# ###               CLASS RENDERER               ###
# ##################################################

class Renderer ( pydataset.render.Renderer ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, logger=None, progress_tracker=None ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( settings, RendererSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		super( Renderer, self ).__init__( settings, logger, progress_tracker )

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def grab_render ( self ):
		"""
		Grabs the render produced by the vieport of this renderer.

		Arguments:
			self (`pylinemod.Renderer`): Renderer of which to grab the render.

		Returns:
			`numpy.ndarray`: The render.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		
		if self.settings.shader == 'color':
			return self.viewport.grab_image( *self.settings.size.dimensions )

		elif self.settings.shader == 'mask':
			return self.viewport.grab_image( *self.settings.size.dimensions )

		elif self.settings.shader == 'depth':
			return self.viewport.grab_depth( *self.settings.size.dimensions )

	# def grab_render ( self )

	# --------------------------------------------------

	def late_update_scene ( self, scene ):
		"""
		Overload this method to update the scene after each render.

		Arguments:
			self      (`pylinemod.Renderer`): Renderer to update
			scene (`pydataset.render.Scene`): Scene to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )
		

		# Move to the next example
		self.images_iterator.next()
		self.image_index += 1
		
		# Update progression
		if self.progress_tracker:
			self.progress_tracker.update( float(self.image_index)/float(self.images_count) )

	# def late_update_scene ( self, scene )

	# --------------------------------------------------
	
	def must_close ( self ):
		
		return super( Renderer, self ).must_close() or self.image_index >= self.images_count

	# def must_close ( self )
	
	# --------------------------------------------------

	def save_render ( self, render ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( render, numpy.ndarray )

		# Get the example
		image_group = self.images_iterator.current()
		
		# Shape
		shape = self.settings.size.dimensions
		shape = [ shape[1], shape[0] ]

		# Save the render
		if self.settings.shader == 'color':
			f = image_group.create_or_update_feature(
				     feature_name = self.settings.render_feature_name,
				feature_data_type = pydataset.dataset.ImageFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( *shape, 4 ),
				            dtype = numpy.dtype( 'uint8' ),
				            value = render,
				        extension = '.png',
				            param = 9
				)

		elif self.settings.shader == 'mask':
			f = image_group.create_or_update_feature(
				     feature_name = self.settings.render_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( *shape ),
				            dtype = numpy.dtype( 'bool' ),
				            value = (render[ :, :, 3 ] > 0)
				)
		else:
			f = image_group.create_or_update_feature(
				     feature_name = self.settings.render_feature_name,
				feature_data_type = pydataset.dataset.ImageFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( *shape ),
				            dtype = numpy.dtype( 'uint16' ),
				            value = numpy.multiply( render, 1000. ).astype( numpy.uint16 ), # scene unit is meter but depth should be in mm
				        extension = '.png',
				            param = 9
				)
			image_group.create_or_update_feature(
				     feature_name = self.settings.render_feature_name+'_scale',
				feature_data_type = pydataset.dataset.FloatFeatureData,
				      auto_update = True,
				            value = 1. # scene unit is meter but depth should be in mm
				)

		# Log
		if self.logger:
			self.logger.log( "    Saved render: {}", f.url )
			self.logger.end_line()

	# def save_render ( self, render )

	# --------------------------------------------------

	def setup_scene ( self, scene ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )

		# Examples iterator
		self.images_iterator = pydataset.dataset.Iterator( self.settings.dataset_url + '/' + self.settings.images_url_search_pattern )
		self.images_count    = self.images_iterator.count
		self.image_index     = 0

		if self.logger:
			self.logger.log( "Images Iterator:" )
			self.logger.log( "    url:   {}", self.settings.dataset_url + '/' + self.settings.images_url_search_pattern )
			self.logger.log( "    count: {}", self.images_count )

		# Group iterators
		objects_iterator = pydataset.dataset.Iterator( self.settings.dataset_url + '/' + self.settings.objects_url_search_pattern )
		
		if self.logger:
			self.logger.log( "Objects Iterator:" )
			self.logger.log( "    url:   {}", self.settings.dataset_url + '/' + self.settings.objects_url_search_pattern )
			self.logger.log( "    count: {}", objects_iterator.count )
			self.logger.log( "Meshs:" )

		# Load the mesh of each category
		self.meshs = {}
		for object_example in objects_iterator:
			
			# Get the mesh data and the category name
			mesh_data     = object_example.get_feature( self.settings.mesh_feature_name          ).value
			category_name = object_example.get_feature( self.settings.category_name_feature_name ).value

			if self.logger:
				self.logger.log( "    Mesh '{}'", str(category_name) )
				self.logger.log( "        vertices:  {}", mesh_data.points.shape[0] if mesh_data.points is not None else None )
				self.logger.log( "        indexes:   {}", mesh_data.indexes.shape[0] if mesh_data.indexes is not None else None )
				self.logger.log( "        colors:    {}", mesh_data.colors.shape[0] if mesh_data.colors is not None else None )
				self.logger.log( "        primitive: {}", mesh_data.primitive )

			# If the mesh has no color
			if mesh_data.colors is None:
				
				# Use the default color
				mesh_data.set_color( self.settings.default_mesh_color )
				
				if self.logger:
					self.logger.log( "         using default color: {}", str(self.settings.default_mesh_color) )
			
			elif numpy.any( mesh_data.colors > 1. ):
				mesh_data.colors = mesh_data.colors / 255.

			# Save the mesh under its category name
			self.meshs[ str(category_name) ] = mesh_data

		# for object_example in objects_iterator

		if self.logger:
			self.logger.end_line()

	# def setup_scene ( self, scene )

	# --------------------------------------------------
	
	def update_scene ( self, scene ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, pydataset.render.Scene )

		# Remove all entities in the scene, including the camera
		scene.clear()
		gc.collect()

		# Choose the shader type
		if self.settings.shader == 'color':
			shader_type = pydataset.render.shaders.ColorShaderProgram
		elif self.settings.shader == 'mask':
			shader_type = pydataset.render.shaders.MaskShaderProgram
		else:
			shader_type = pydataset.render.shaders.DepthShaderProgram

		# Get the current image group
		image_group = self.images_iterator.current()
		
		# Get the camera parameters matrix for the image
		K = image_group.get_feature( self.settings.camera_matrix_feature_name ).value

		# Set the camera parameters matrix
		scene.camera = pydataset.render.cameras.OpenCVCamera.create(
			                x1 = 0.0,
			                y1 = 0.0,
			                x2 = float(self.settings.size[ 0 ]),
			                y2 = float(self.settings.size[ 1 ]),
			                 K = K.ravel().tolist(),
			         near_clip = 0.01,
			          far_clip = 10000.0,
			window_coordinates = 'y_down'
			)

		# Log
		if self.logger:
			self.logger.log( "Setting Up Scene:" )
			self.logger.log( "    url:      {}", image_group.url )
			self.logger.log( "    K:        {}", ['{:6.2f}'.format(v) for v in K.ravel()] )

		# Seach object instances iterators
		if self.settings.instances_url_search_pattern:
			# Create iterator
			instances_url      = image_group.absolute_url + '/' + self.settings.instances_url_search_pattern
			instances_iterator = pydataset.dataset.Iterator(
				image_group.absolute_url + '/' + self.settings.instances_url_search_pattern
				)
			# Log
			if self.logger:
				self.logger.log( "    iterator: {} ({})", instances_url, instances_iterator.count )
			# Check
			if len(instances_iterator) == 0:
				raise ValueError( "Found 0 instances to iterate on using URL '{}'!".format(
					image_group.absolute_url + '/' + self.settings.instances_url_search_pattern
					))
		else:
			# Nothing to seach for
			instances_iterator = [image_group]
			# Log
			if self.logger:
				self.logger.log( "    iterator: None (1)" )
		
		# Setup all the objects instances
		for instance_example in instances_iterator:

			# Instance name
			instance_name = instance_example.name

			# Get the category name, rotation matrix and translation vector
			R             = instance_example.get_feature( self.settings.rotation_matrix_feature_name    ).value
			t             = instance_example.get_feature( self.settings.translation_vector_feature_name ).value
			category_name = instance_example.get_feature( self.settings.category_name_feature_name      ).value

			# Create an parent for the instance and flip xy directions
			instance_parent_entity                 = pydataset.render.Entity.create( name=instance_name, parent=scene )
			instance_parent_entity.local_transform = pydataset.render.numpy_utils.invert_xy()
			instance_parent_entity.enabled         = True

			# Create the mesh entity
			instance_entity = pydataset.render.Mesh(
				          data = copy.deepcopy( self.meshs[ str(category_name) ] ),
				shader_program = shader_type(),
				        parent = instance_parent_entity
				)
			instance_entity.name            = str(category_name)
			instance_entity.local_transform = pydataset.render.numpy_utils.affine_transform( R, t )
			instance_entity.enabled         = True

			if self.logger:
				self.logger.log( "    Instance:" )
				self.logger.log( "        url:      {}", instance_example.url )
				self.logger.log( "        R:        {}", ['{:6.2f}'.format(v) for v in R.ravel()] )
				self.logger.log( "        t:        {}", ['{:6.2f}'.format(v) for v in t.ravel()] )
				self.logger.log( "        category: {}", str(category_name) )

			# for instance_example in instances_iterator:

		if self.logger:
			self.logger.end_line()

	# def update_scene ( self, scene )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a new renderer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pylinemod.RendererSettings`: The settings.
		"""
		return RendererSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Renderer ( pydataset.render.Renderer )