# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.io
import pytools.assertions
import pytools.path
import pytools.serialization

# ##################################################
# ###          CLASS IMPORTER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImporterSettings',
	   namespace = 'pylinemod',
	fields_names = [
		'raw_dataset_directory_path',
		# objects
		'objects_group_name',
		'category_index_feature_name',
		'one_hot_category_feature_name',
		'category_name_feature_name',
		'category_color_feature_name',
		'ply_mesh_feature_name',
		'xyz_mesh_feature_name',
		'old_ply_mesh_feature_name',
		'bounding_radius_feature_name',
		'mesh_transform_feature_name',
		# common to all scenes
		'scenes_group_name',
		'camera_matrix_feature_name',
		# image features
		'image_feature_name',
		'depth_feature_name',
		# instance features
		'rotation_matrix_feature_name',
		'translation_vector_feature_name'
		]
	)
class ImporterSettings ( pydataset.io.ImporterSettings ):
	"""
	Dataset importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self,
		dst_group_url,
		raw_dataset_directory_path,
		# objects
		objects_group_name = 'objects',
		category_name_feature_name = 'category_name',
		category_index_feature_name = 'category_index',
		one_hot_category_feature_name = 'one_hot_category',
		category_color_feature_name = 'category_color',
		ply_mesh_feature_name = 'mesh_ply',
		xyz_mesh_feature_name = 'mesh_xyz',
		old_ply_mesh_feature_name = 'old_mesh_ply',
		bounding_radius_feature_name = 'bounding_radius',
		mesh_transform_feature_name = 'mesh_transform',
		# common to all scenes
		scenes_group_name = 'scenes',
		camera_matrix_feature_name = 'K',
		# image features
		image_feature_name = 'image',
		depth_feature_name = 'depth',
		# instance features
		rotation_matrix_feature_name='R',
		translation_vector_feature_name='t',
		# other
		name='linemod-importer'
		):
		"""
		Initializes a new instance of the linemod importer settings class.

		Arguments:
			self                             (`pylinemod.ImporterSettings`): Instance to initialize.
			dst_group_url                                           (`str`): URL of the group where to import data to.
			raw_dataset_directory_path (`str`/`pytools.path.DirectoryPath`): Path to the directory containing the raw linemod dataset.
			objects_group_name                                      (`str`): Name of the group in which to save to save the objects indormations.
			category_name_feature_name                              (`str`): Name of the feature in which to save the category name.
			category_index_feature_name                             (`str`): Name of the feature in which to save the category index.
			one_hot_category_feature_name                           (`str`): Name of the feature in which to save the one hot category.
			category_color_feature_name                             (`str`): Name of the feature in which to save the category color.
			ply_mesh_feature_name                                   (`str`): Name of the feature in which to save the ply mesh.
			xyz_mesh_feature_name                                   (`str`): Name of the feature in which to save the xyz mesh.
			old_ply_mesh_feature_name                               (`str`): Name of the feature in which to save the xyz mesh.
			bounding_radius_feature_name                            (`str`): Name of the feature in which to save the object bounding-radius.
			mesh_transform_feature_name                             (`str`): Name of the feature in which to save the tranform that maps the old mesh to the new one.
			scenes_group_name                                       (`str`): Name of the group in which to save to save the scenes.
			camera_matrix_feature_name                              (`str`): Name of the feature in which to save the camera parameters matrix.
			image_feature_name                                      (`str`): Name of the feature in which to save the image.
			depth_feature_name                                      (`str`): Name of the feature in which to save the depth.
			rotation_matrix_feature_name                            (`str`): Name of the feature in which to save the model view rotation matrix.
			translation_vector_feature_name                         (`str`): Name of the feature in which to save the model view translation vector.
			name                                                    (`str`): Name of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is_instance_of( raw_dataset_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( objects_group_name, str )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( category_color_feature_name, str )
		assert pytools.assertions.type_is( ply_mesh_feature_name, str )
		assert pytools.assertions.type_is( xyz_mesh_feature_name, str )
		assert pytools.assertions.type_is( old_ply_mesh_feature_name, str )
		assert pytools.assertions.type_is( bounding_radius_feature_name, str )
		assert pytools.assertions.type_is( mesh_transform_feature_name, str )
		assert pytools.assertions.type_is( scenes_group_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( depth_feature_name, str )
		assert pytools.assertions.type_is( rotation_matrix_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is( name, str )

		super( ImporterSettings, self ).__init__( dst_group_url, name )

		if isinstance( raw_dataset_directory_path, str ):
			raw_dataset_directory_path = pytools.path.DirectoryPath( raw_dataset_directory_path )

		self._raw_dataset_directory_path = raw_dataset_directory_path
		# objects
		self._objects_group_name            = objects_group_name
		self._category_name_feature_name    = category_name_feature_name
		self._category_index_feature_name   = category_index_feature_name
		self._one_hot_category_feature_name = one_hot_category_feature_name
		self._category_color_feature_name   = category_color_feature_name
		self._ply_mesh_feature_name         = ply_mesh_feature_name
		self._xyz_mesh_feature_name         = xyz_mesh_feature_name
		self._old_ply_mesh_feature_name     = old_ply_mesh_feature_name
		self._bounding_radius_feature_name  = bounding_radius_feature_name
		self._mesh_transform_feature_name   = mesh_transform_feature_name
		# common to all scenes
		self._scenes_group_name = scenes_group_name
		self._camera_matrix_feature_name = camera_matrix_feature_name
		# image features
		self._image_feature_name = image_feature_name
		self._depth_feature_name = depth_feature_name
		# instance features
		self._rotation_matrix_feature_name    = rotation_matrix_feature_name
		self._translation_vector_feature_name = translation_vector_feature_name
		
	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def raw_dataset_directory_path ( self ):
		"""
		Path to the directory containing the raw linemod dataset (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._raw_dataset_directory_path

	# def raw_dataset_directory_path ( self )
	
	# --------------------------------------------------

	@raw_dataset_directory_path.setter
	def raw_dataset_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, pytools.path.DirectoryPath )

		self._raw_dataset_directory_path = value
		
	# def raw_dataset_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def objects_group_name ( self ):
		"""
		Name of the group in which to save the objects indormations (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )

		return self._objects_group_name

	# def objects_group_name ( self )
	
	# --------------------------------------------------

	@objects_group_name.setter
	def objects_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, str )

		self._objects_group_name = value
		
	# def objects_group_name ( self, value )

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature in which to save the category name (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature in which to save the category index (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._category_index_feature_name
	
	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature in which to save the category index (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )

	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_color_feature_name ( self ):
		"""
		Name of the feature in which to save the category color (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._category_color_feature_name
	
	# def category_color_feature_name ( self )

	# --------------------------------------------------

	@category_color_feature_name.setter
	def category_color_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_color_feature_name = value
	
	# def category_color_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def ply_mesh_feature_name ( self ):
		"""
		Name of the feature in which to save the ply mesh (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._ply_mesh_feature_name
	
	# def ply_mesh_feature_name ( self )

	# --------------------------------------------------

	@ply_mesh_feature_name.setter
	def ply_mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._ply_mesh_feature_name = value
	
	# def ply_mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def xyz_mesh_feature_name ( self ):
		"""
		Name of the feature in which to save the xyz mesh (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._xyz_mesh_feature_name
	
	# def xyz_mesh_feature_name ( self )

	# --------------------------------------------------

	@xyz_mesh_feature_name.setter
	def xyz_mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._xyz_mesh_feature_name = value
	
	# def xyz_mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def old_ply_mesh_feature_name ( self ):
		"""
		Name of the feature in which to save the old ply mesh (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._old_ply_mesh_feature_name
	
	# def old_ply_mesh_feature_name ( self )

	# --------------------------------------------------

	@old_ply_mesh_feature_name.setter
	def old_ply_mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._old_ply_mesh_feature_name = value
	
	# def old_ply_mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_radius_feature_name ( self ):
		"""
		Name of the feature in which to save the object bounding-radius (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._bounding_radius_feature_name
	
	# def bounding_radius_feature_name ( self )

	# --------------------------------------------------

	@bounding_radius_feature_name.setter
	def bounding_radius_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._bounding_radius_feature_name = value
	
	# def bounding_radius_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mesh_transform_feature_name ( self ):
		"""
		Name of the feature in which to save the tranform that maps the old mesh to the new one (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._mesh_transform_feature_name
	
	# def mesh_transform_feature_name ( self )

	# --------------------------------------------------

	@mesh_transform_feature_name.setter
	def mesh_transform_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_transform_feature_name = value
	
	# def mesh_transform_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def scenes_group_name ( self ):
		"""
		Name of the group in which to savethe scenes (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._scenes_group_name
	
	# def scenes_group_name ( self )

	# --------------------------------------------------

	@scenes_group_name.setter
	def scenes_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._scenes_group_name = value
	
	# def scenes_group_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature in which to save the camera parameters matrix (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature in which to save the image (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		"""
		Name of the feature in which to save the depth image (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._depth_feature_name
	
	# def depth_feature_name ( self )

	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._depth_feature_name = value
	
	# def depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_matrix_feature_name ( self ):
		"""
		Name of the feature in which to save the model view rotation matrix (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._rotation_matrix_feature_name
	
	# def rotation_matrix_feature_name ( self )

	# --------------------------------------------------

	@rotation_matrix_feature_name.setter
	def rotation_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_matrix_feature_name = value
	
	# def rotation_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		"""
		Name of the feature in which to save the model view translation vector (`str`) 
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
	
		return self._translation_vector_feature_name
	
	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_vector_feature_name = value
	
	# def translation_vector_feature_name ( self, value )

# class ImporterSettings ( pydataset.io.ImporterSettings )