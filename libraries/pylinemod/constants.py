
CATEGORIES = [ 
		{ 'index':  0, 'name': 'ape',           'count': 1236, 'color': (122,  36,  39) },
		{ 'index':  1, 'name': 'benchviseblue', 'count': 1215, 'color': (104, 143, 174) },
		{ 'index':  2, 'name': 'can',           'count': 1196, 'color': (205, 206, 188) },
		{ 'index':  3, 'name': 'cat',           'count': 1179, 'color': (214, 153, 152) },
		{ 'index':  4, 'name': 'driller',       'count': 1188, 'color': ( 45,  64,  47) },
		{ 'index':  5, 'name': 'duck',          'count': 1254, 'color': (254, 231,  22) },
		{ 'index':  6, 'name': 'glue',          'count': 1220, 'color': (196, 196, 196) },
		{ 'index':  7, 'name': 'holepuncher',   'count': 1237, 'color': ( 39,  51, 109) },
		{ 'index':  8, 'name': 'iron',          'count': 1152, 'color': (110, 149, 218) },
		{ 'index':  9, 'name': 'lamp',          'count': 1227, 'color': (253, 253, 253) },
		{ 'index': 10, 'name': 'phone',         'count': 1243, 'color': (109,  98,  76) },
		# { 'index': 11, 'name': 'cup',           'count': 1240, 'color': (28, 30, 91) },
		# { 'index': 12, 'name': 'eggbox',        'count': 1253, 'color': (198, 198, 188) },
		# { 'index': 13, 'name': 'bowl',          'count': 1233, 'color': (251, 252, 254) },
		# { 'index': 14, 'name': 'cam',           'count': 1201, 'color': ( 79,  85,  85) }
	]

CLASSES_NAMES = [
	'ape',
	'benchviseblue',
	'can',
	'cat',
	'driller',
	'duck',
	'glue',
	'holepuncher',
	'iron',
	'lamp',
	'phone'
	]