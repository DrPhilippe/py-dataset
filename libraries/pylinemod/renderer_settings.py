# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pydataset.render
import pytools.assertions
import pytools.serialization

# ##################################################
# ###          CLASS RENDERER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RendererSettings',
	   namespace = 'pylinemod',
	fields_names = [
		'dataset_url',
		'objects_url_search_pattern',
		'images_url_search_pattern',
		'instances_url_search_pattern',
		'mesh_feature_name',
		'default_mesh_color',
		'category_name_feature_name',
		'camera_matrix_feature_name',
		'rotation_matrix_feature_name',
		'translation_vector_feature_name',
		'shader'
		]
	)
class RendererSettings ( pydataset.render.RendererSettings ):
	"""
	Settings for the LINEMOD renderer.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		                    dataset_url = '',
		     objects_url_search_pattern = 'groups/objects/examples/*',
		      images_url_search_pattern = 'groups/scenes/groups/*/groups/*',
		   instances_url_search_pattern = 'example/*',
		              mesh_feature_name = 'mesh_ply',
		             default_mesh_color = pydataset.data.ColorData( 192, 192, 192, 255 ),
		     category_name_feature_name = 'category_name',
		     camera_matrix_feature_name = 'K',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		                         shader = 'color',
		                           size = pydataset.data.ShapeData( 640, 480 ),
		            render_feature_name = 'render',
		                            fps = 240,
		                 is_dry_running = False,
		                           name = 'linemod-renderer'
		):
		"""
		Initializes a new instance of the `pylinemod.RendererSettings` class.

		Arguments:
			self             (`pylinemod.RendererSettings`): Instance to initialize.
			dataset_url                             (`str`): URL of the linemod dataset.
			objects_url_search_pattern              (`str`): URL search pattern for the examples containing objects data.
			images_url_search_pattern               (`str`): URL search pattern for the images to render.	
			instances_url_search_pattern            (`str`): URL search pattern for the object instances.
			mesh_feature_name                       (`str`): Name of the feature containing the mesh data.
			default_mesh_color (`pydataset.data.ColorData`): Default color if the mesh has no color.
			category_name_feature_name              (`str`): Name of the feature containing the name of the object-category.
			camera_matrix_feature_name              (`str`): Name of the feature containing the camera parameters matrix.
			rotation_matrix_feature_name            (`str`): Name of the feature containing the model view rotation matrix.
			translation_vector_feature_name         (`str`): Name of the feature containing the model view translation vector.
			shader                                  (`str`): Shader used to render the mesh - 'color' or 'depth'.
			size               (`pydataset.data.ShapeData`): Size of the rendering viewport.
			render_feature_name                     (`str`): Name of the feature in which to save the renders.
			fps                                     (`int`): Render rate in frame per seconds.
			is_dry_running                         (`bool`): Boolean indicating if the renderer is dry-running.
			name                                    (`str`): Name of the renderer.
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( dataset_url, str )
		assert pytools.assertions.type_is( objects_url_search_pattern, str )
		assert pytools.assertions.type_is( images_url_search_pattern, str )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( mesh_feature_name, str )
		assert pytools.assertions.type_is_instance_of( default_mesh_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( rotation_matrix_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is( shader, str )
		assert pytools.assertions.value_in( shader, ['color', 'depth', 'mask'] )
		assert pytools.assertions.type_is( render_feature_name, str )
		assert pytools.assertions.type_is_instance_of( size, pydataset.data.ShapeData )
		assert pytools.assertions.type_is( fps, int )
		assert pytools.assertions.true( fps > 0 )
		assert pytools.assertions.type_is( is_dry_running, bool )
		assert pytools.assertions.type_is( name, str )

		super( RendererSettings, self ).__init__(
			render_feature_name = render_feature_name,
			               size = size,
			                fps = fps,
			     is_dry_running = is_dry_running,
			               name = name,
			)

		self._dataset_url                     = dataset_url
		self._objects_url_search_pattern      = objects_url_search_pattern
		self._images_url_search_pattern       = images_url_search_pattern
		self._instances_url_search_pattern    = instances_url_search_pattern 
		self._mesh_feature_name               = mesh_feature_name
		self._default_mesh_color              = default_mesh_color
		self._category_name_feature_name      = category_name_feature_name
		self._camera_matrix_feature_name      = camera_matrix_feature_name
		self._rotation_matrix_feature_name    = rotation_matrix_feature_name
		self._translation_vector_feature_name = translation_vector_feature_name
		self._shader                          = shader

	# def __init__ ( self, dataset_url, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dataset_url ( self ):
		"""
		URL of the linemod dataset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._dataset_url
	
	# def dataset_url ( self )

	# --------------------------------------------------

	@dataset_url.setter
	def dataset_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._dataset_url = value
	
	# def dataset_url ( self, value )

	# --------------------------------------------------

	@property
	def objects_url_search_pattern ( self ):
		"""
		URL search pattern for the examples containing objects data (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._objects_url_search_pattern
	
	# def objects_url_search_pattern ( self )

	# --------------------------------------------------

	@objects_url_search_pattern.setter
	def objects_url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._objects_url_search_pattern = value
	
	# def objects_url_search_pattern ( self, value )

	# --------------------------------------------------

	@property
	def images_url_search_pattern ( self ):
		"""
		URL search pattern for the images to render (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._images_url_search_pattern
	
	# def images_url_search_pattern ( self )

	# --------------------------------------------------

	@images_url_search_pattern.setter
	def images_url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._images_url_search_pattern = value
	
	# def images_url_search_pattern ( self, value )

	# --------------------------------------------------

	@property
	def instances_url_search_pattern ( self ):
		"""
		URL search pattern for the object instances (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._instances_url_search_pattern
	
	# def instances_url_search_pattern ( self )

	# --------------------------------------------------

	@instances_url_search_pattern.setter
	def instances_url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._instances_url_search_pattern = value
	
	# def instances_url_search_pattern ( self, value )

	# --------------------------------------------------

	@property
	def mesh_feature_name ( self ):
		"""
		Name of the feature containing the mesh data (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._mesh_feature_name
	
	# def mesh_feature_name ( self )

	# --------------------------------------------------

	@mesh_feature_name.setter
	def mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._mesh_feature_name = value
	
	# def mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def default_mesh_color ( self ):
		"""
		Default color if the mesh has no color (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._default_mesh_color
	
	# def default_mesh_color ( self )

	# --------------------------------------------------

	@default_mesh_color.setter
	def default_mesh_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )
	
		self._default_mesh_color = copy.deepcopy( value )
	
	# def default_mesh_color ( self, value )

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature containing the name of the object-category (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_matrix_feature_name ( self ):
		"""
		Name of the feature containing the model view rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._rotation_matrix_feature_name
	
	# def rotation_matrix_feature_name ( self )

	# --------------------------------------------------

	@rotation_matrix_feature_name.setter
	def rotation_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._rotation_matrix_feature_name = value
	
	# def rotation_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		"""
		Name of the feature containing the model view translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._translation_vector_feature_name
	
	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._translation_vector_feature_name = value
	
	# def translation_vector_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def shader ( self ):
		"""
		Shader used to render the mesh: 'color' or 'depth' (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._shader
	
	# def shader ( self )

	# --------------------------------------------------

	@shader.setter
	def shader ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['color', 'depth', 'mask'] )
	
		self._shader = value
	
	# def shader ( self, value )

# class RendererSettings ( pydataset.render.RendererSettings )