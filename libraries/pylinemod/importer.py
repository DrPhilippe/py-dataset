# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.io
import pydataset.render
import pytools.assertions
import pytools.tasks

# LOCALS
from .                  import constants
from .importer_settings import ImporterSettings

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pydataset.io.Importer ):
	"""
	Importer that imports raw linemod dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		Arguments:
			self                               (`pylinemod.Importer`): Importer to initialize.
			settings                   (`pylinemod.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), pydataset.dataset.Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)

	# def __init__ ( self, settings, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the linemod dataset.

		Arguments:
			self (`pylinemod.Importer`): Importer used to import the linemod dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		
		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# Log
		if ( self.logger ):
			self.logger.log( 'LINEMOD Importer:' )
			for setting_name in ImporterSettings.__fields_names__:
				self.logger.log( '    {:30s}: {}', setting_name, getattr( self.settings, setting_name ) )
			self.logger.end_line()
			self.logger.log( 'Checking raw dataset directory and files:' )
			self.logger.flush()

		# Check the raw dataset directory
		if self.settings.raw_dataset_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist.".format(self.settings.raw_dataset_directory_path) )

		# Log
		if ( self.logger ):
			self.logger.log( '    raw_dataset_directory_path: OK' )
			self.logger.end_line()
			self.logger.flush()

		# Create the groups where to save the objects
		if '/' in self.settings.objects_group_name:
			self.dst_objects_group = self.dst_group.create( self.settings.objects_group_name )
		else:
			self.dst_objects_group = self.dst_group.create_group( self.settings.objects_group_name )
		# Create the groups where to save the scenes
		if '/' in self.settings.scenes_group_name:
			self.dst_scenes_group = self.dst_group.create( self.settings.scenes_group_name )
		else:
			self.dst_scenes_group = self.dst_group.create_group( self.settings.scenes_group_name )

		# Log
		if ( self.logger ):
			self.logger.log( '    dst_objects_group: {}', self.dst_objects_group.url )
			self.logger.log( '    dst_scenes_group: {}', self.dst_scenes_group.url )
			self.logger.end_line()
			self.logger.flush()

		# Locate kinect camera parameters
		kinect_filepath = self.settings.raw_dataset_directory_path + pytools.path.FilePath( 'kinect.txt' )
		if kinect_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(kinect_filepath) )

		# Log
		if ( self.logger ):
			self.logger.log( '    kinect_filepath: {}, OK', kinect_filepath )
			self.logger.end_line()
			self.logger.flush()

		# Load kinect camera parameters
		K = numpy.loadtxt( str(kinect_filepath) )
		K = numpy.reshape( K, [3, 3] ).astype( numpy.float32 )

		# Create feature for kinect camera parameters
		if self.settings.camera_matrix_feature_name:
			self.dst_scenes_group.dataset.create_feature(
				     feature_name = self.settings.camera_matrix_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = True,
				            shape = pydataset.data.ShapeData( 3, 3 ),
				            value = K,
				            dtype = numpy.dtype('float32')
				)

		# Import each category
		number_of_categories = len(constants.CATEGORIES)
		for category_index in range(number_of_categories):

			category = constants.CATEGORIES[ category_index ]

			progress_offset = float(category_index)/float(number_of_categories)
			progress_scale  = 1.0/float(number_of_categories)
			self.import_category( category, progress_offset, progress_scale )

			if self.progress_tracker:
				self.progress_tracker.update( float(category_index)/float(number_of_categories) )

		# for category_index in range(number_of_categories)

		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# --------------------------------------------------

	def import_category ( self, category, progress_offset, progress_scale ):

		# Get category infos
		category_name  = category[ 'name'  ]
		category_index = category[ 'index' ]
		category_count = category[ 'count' ]
		category_color = category[ 'color' ]
		category_color = pydataset.data.ColorData( *category_color )
		category_directory_path = self.settings.raw_dataset_directory_path + pytools.path.DirectoryPath( category_name )
		
		# Log
		if ( self.logger ):
			self.logger.log( 'Importing category:' )
			self.logger.log( '    name:  {}', category_name )
			self.logger.log( '    index: {}', category_index )
			self.logger.log( '    count: {}', category_count )
			self.logger.log( '    color: {}', category_color )
			self.logger.log( '    path:  {}', category_directory_path )
			self.logger.end_line()
			self.logger.flush()

		# Check that the directory for the category exists
		if category_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(category_directory_path) )

		# Locate the directory and the files of the category
		data_directory_path = category_directory_path + pytools.path.DirectoryPath( 'data' )
		distance_filepath   = category_directory_path + pytools.path.FilePath( 'distance.txt'  )
		mesh_filepath       = category_directory_path + pytools.path.FilePath( 'mesh.ply'      )
		object_filepath     = category_directory_path + pytools.path.FilePath( 'object.xyz'    )
		old_mesh_filepath   = category_directory_path + pytools.path.FilePath( 'OLDmesh.ply'   )
		transform_filepath  = category_directory_path + pytools.path.FilePath( 'transform.dat' )

		# Check the directory and the files of the category
		if data_directory_path.is_not_a_directory():
			raise IOError( "The directory '{}' does not exist".format(data_directory_path) )
		if distance_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(distance_filepath) )
		if mesh_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(mesh_filepath) )
		if object_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(object_filepath) )
		if old_mesh_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(old_mesh_filepath) )
		if transform_filepath.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(transform_filepath) )
		
		# Log
		if ( self.logger ):
			self.logger.log( '    directories and files:' )
			self.logger.log( '        data_directory_path: {}', data_directory_path )
			self.logger.log( '        distance_filepath:   {}', distance_filepath )
			self.logger.log( '        mesh_filepath:       {}', mesh_filepath )
			self.logger.log( '        object_filepath:     {}', object_filepath )
			self.logger.log( '        old_mesh_filepath:   {}', old_mesh_filepath )
			self.logger.log( '        transform_filepath:  {}', transform_filepath )
			self.logger.end_line()
			self.logger.flush()

		# Create the example for the object of this category
		object_exemple = self.dst_objects_group.create_example( category_name, auto_update=True )
		object_url     = 'groups/{}/examples/{}'.format(
			self.settings.objects_group_name,
			category_name
			)

		# Create the category name feature
		if self.settings.category_name_feature_name:
			object_exemple.create_feature(
				     feature_name = self.settings.category_name_feature_name,
				feature_data_type = pydataset.dataset.TextFeatureData,
				      auto_update = False,
				            value = category_name
				)

		# Create the category index feature
		if self.settings.category_index_feature_name:
			object_exemple.create_feature(
				     feature_name = self.settings.category_index_feature_name,
				feature_data_type = pydataset.dataset.IntFeatureData,
				      auto_update = False,
				            value = category_index
				)

		# Create the one hot category feature
		if self.settings.one_hot_category_feature_name:
			object_exemple.create_feature(
				     feature_name = self.settings.one_hot_category_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = False,
				            value = pydataset.classification_utils.index_to_one_hot( category_index, len(constants.CATEGORIES) )
				)

		# Create the category color feature
		if self.settings.category_color_feature_name:
			object_exemple.create_feature(
				     feature_name = self.settings.category_color_feature_name,
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				      auto_update = False,
				            value = category_color
				)

		# Load the ply mesh
		mesh_ply = pydataset.render.MeshData.load_ply( mesh_filepath )
		
		# ply mesh geometry is in mm, convert to meters
		mesh_ply.points /= 1000.0

		# Create the feature for the mesh
		object_exemple.create_feature(
			     feature_name = self.settings.ply_mesh_feature_name,
			feature_data_type = pydataset.dataset.SerializableFeatureData,
			      auto_update = False,
			            value = mesh_ply
			)

		if self.settings.xyz_mesh_feature_name:
			# Load the xyz object
			object_xyz = pydataset.render.MeshData.load_xyz( object_filepath, items=['x', 'y', 'z', 'nx', 'ny', 'nz', 'b', 'g', 'r'] )
			
			# xyz object geometry is in cm, convert to meters
			object_xyz.points /= 100.0

			# Create the feature for the mesh
			object_exemple.create_feature(
				     feature_name = self.settings.xyz_mesh_feature_name,
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				      auto_update = False,
				            value = object_xyz
				)

		if self.settings.old_ply_mesh_feature_name:
			# Load the old ply mesh
			old_ply_mesh = pydataset.render.MeshData.load_ply( old_mesh_filepath )
			
			# old ply mesh geometry is in mm, convert to meters
			old_ply_mesh.points /= 1000.0

			# Create the feature for the mesh
			object_exemple.create_feature(
				     feature_name = self.settings.old_ply_mesh_feature_name,
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				      auto_update = False,
				            value = old_ply_mesh
				)

		if self.settings.bounding_radius_feature_name:
			# Load the distance
			bounding_radius = numpy.loadtxt( str(distance_filepath) )
			bounding_radius = numpy.reshape( bounding_radius, [1] ).astype( numpy.float32 ).item( 0 )

			# Create the distance feature
			object_exemple.create_feature(
				     feature_name = self.settings.bounding_radius_feature_name,
				feature_data_type = pydataset.dataset.FloatFeatureData,
				      auto_update = False,
				            value = bounding_radius
				)

		if self.settings.mesh_transform_feature_name:
			# Load the transform
			mesh_transform = numpy.loadtxt( str(transform_filepath), skiprows=1, usecols=(1) )
			mesh_transform = numpy.reshape( mesh_transform, [3, 4] ).astype( numpy.float32 )

			# Create the transform feature
			object_exemple.create_feature(
				     feature_name = self.settings.mesh_transform_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				      auto_update = False,
				            value = mesh_transform
				)

		# Update the object's example
		# i.e. save features refs
		object_exemple.update()

		# Create the group for the scene of this category
		scene_group = self.dst_scenes_group.create_group( category_name )

		# Import each images
		for image_index in range(category_count):

			# Locate data files
			image_filepath = data_directory_path + pytools.path.FilePath.format( 'color{}.jpg', image_index )
			depth_filepath = data_directory_path + pytools.path.FilePath.format( 'depth{}.dpt', image_index )
			rot_filepath   = data_directory_path + pytools.path.FilePath.format( 'rot{}.rot',   image_index )
			tra_filepath   = data_directory_path + pytools.path.FilePath.format( 'tra{}.tra',   image_index )
			
			# Check data files
			if image_filepath.is_not_a_file():
				raise IOError( "The file '{}' does not exist".format(image_filepath) )
			if depth_filepath.is_not_a_file():
				raise IOError( "The file '{}' does not exist".format(depth_filepath) )
			if rot_filepath.is_not_a_file():
				raise IOError( "The file '{}' does not exist".format(rot_filepath) )
			if tra_filepath.is_not_a_file():
				raise IOError( "The file '{}' does not exist".format(tra_filepath) )

			# Create teh example
			image_group = scene_group.create_group( '{:05d}'.format(image_index), auto_update=True )

			# Load the image
			image = cv2.imread( str(image_filepath) )
			image = numpy.reshape( image, [480, 640, 3] ).astype( numpy.uint8 )

			# Create the image feature
			if self.settings.image_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.image_feature_name,
					feature_data_type = pydataset.dataset.ImageFeatureData,
				          auto_update = False,
					            value = image
					)

			# Load the depth
			depth = depth_filepath.read_binary()
			shape = numpy.frombuffer( depth, dtype=numpy.uint32, count=2 )
			depth = numpy.frombuffer( depth, dtype=numpy.uint16, count=480*640 )
			depth = numpy.reshape( depth, [480, 640] ).astype( numpy.float16 )

			# Create the depth feature
			if self.settings.depth_feature_name:
				image_group.create_feature(
					     feature_name = self.settings.depth_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
				          auto_update = False,
					            value = depth
					)
			
			# Create an example for the object instance
			instance_example = image_group.create_example( '0', auto_update=False )

			# Update the image group
			image_group.update()

			# Load the rotation
			R = numpy.loadtxt( str(rot_filepath), skiprows=1 )
			R = numpy.reshape( R, [3, 3] ).astype( numpy.float32 )

			# Create the rotation feature
			if self.settings.rotation_matrix_feature_name:
				instance_example.create_feature(
					     feature_name = self.settings.rotation_matrix_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = R,
					      auto_update = False
					)

			# Load the translation
			t = numpy.loadtxt( str(tra_filepath), skiprows=1 )
			t = numpy.reshape( t, [3] ).astype( numpy.float32 )
			
			# translation is in cm, convert to meters
			t /= 100.0
			
			# Create the rotation feature
			if self.settings.translation_vector_feature_name:
				instance_example.create_feature(
					     feature_name = self.settings.translation_vector_feature_name,
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = t,
					      auto_update = False
					)
			
			# Create the category name ref feature
			if self.settings.category_name_feature_name:
				instance_example.create_feature(
					     feature_name = self.settings.category_name_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.category_name_feature_name ),
					      auto_update = False
					)
			
			# Create the category index ref feature
			if self.settings.category_index_feature_name:
				instance_example.create_feature(
					     feature_name = self.settings.category_index_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.category_index_feature_name ),
					      auto_update = False
					)

			# Create the one hot category ref feature
			if self.settings.one_hot_category_feature_name:
				instance_example.create_feature(
					     feature_name = self.settings.one_hot_category_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.one_hot_category_feature_name ),
					      auto_update = False
					)

			if self.settings.category_color_feature_name:
				# Create the category color ref feature
				instance_example.create_feature(
					     feature_name = self.settings.category_color_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.category_color_feature_name ),
					      auto_update = False
					)

			if self.settings.ply_mesh_feature_name:
				# Create the ply mesh ref feature
				instance_example.create_feature(
					     feature_name = self.settings.ply_mesh_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.ply_mesh_feature_name ),
					      auto_update = False
					)

			if self.settings.xyz_mesh_feature_name:
				# Create the xyz mesh ref feature
				instance_example.create_feature(
					     feature_name = self.settings.xyz_mesh_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.xyz_mesh_feature_name ),
					      auto_update = False
					)
			
			if self.settings.old_ply_mesh_feature_name:
				# Create the old ply mesh ref feature
				instance_example.create_feature(
					     feature_name = self.settings.old_ply_mesh_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.old_ply_mesh_feature_name ),
					      auto_update = False
					)
			
			if self.settings.bounding_radius_feature_name:
				# Create the bounding radius mesh ref feature
				instance_example.create_feature(
					     feature_name = self.settings.bounding_radius_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.bounding_radius_feature_name ),
					      auto_update = False
					)
			
			if self.settings.mesh_transform_feature_name:
				# Create the mesh transform ref feature
				instance_example.create_feature(
					     feature_name = self.settings.mesh_transform_feature_name,
					feature_data_type = pydataset.dataset.FeatureRefData,
					            value = '{}/features/{}'.format( object_url, self.settings.mesh_transform_feature_name ),
					      auto_update = False
					)

			# Update the example
			instance_example.update()

			if self.progress_tracker:
				self.progress_tracker.update( progress_offset + float(image_index)/float(category_count)*progress_scale )

		# for image_index in range(category_count)

	# def import_category ( self, category )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a linemod improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the linemod importer settings constructor.

		Returns:
			`pytools.linemod.ImporterSettings`: The settings.
		"""
		return ImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Importer ( pydataset.io.Importer )