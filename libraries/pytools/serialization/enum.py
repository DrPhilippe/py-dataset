# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .serializable import Serializable
from .register_serializable_attribute import RegisterSerializableAttribute

# ##################################################
# ###                 CLASS ENUM                 ###
# ##################################################

@RegisterSerializableAttribute(
	   namespace = 'pytools.serialization',
	    typename = 'Enum',
	fields_names = [
		'value'
		]
	)
class Enum ( Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Enum )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		self.value = value

	# def __init__ ( self, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def value ( self ):
		assert pytools.assertions.type_is_instance_of( self, Enum )

		return self._value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, Enum )
		assert pytools.assertions.type_is( val, str )
		assert pytools.assertions.value_in( val, type(self).get_possible_values() )

		self._value = val

	# def value ( self, val )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		assert pytools.assertions.type_is_instance_of( self, Enum )

		return self.value

	# def __str__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		assert pytools.assertions.type_is_instance_of( self, Enum )

		return self.value

	# def __repr__ ( self )

	# --------------------------------------------------

	def __eq__ ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, Enum )

		if isinstance( other, Enum ):
			return self.value == other.value

		return False

	# def __eq__ ( self, other )
	
	# --------------------------------------------------

	def __ne__ ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, Enum )

		if isinstance( other, Enum ):
			return self.value != other.value

		return True

	# def __ne__ ( self, other )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_possible_values ( cls ):

		raise NotImplementedError()

	# def get_possible_values ( cls )

# class Enum ( Serializable )