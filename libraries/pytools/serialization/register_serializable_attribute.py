# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .serializable import Serializable

# ##################################################
# ###    CLASS REGISTER-SERIALIZABLE-ATTRIBUTE   ###
# ##################################################

class RegisterSerializableAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Attribute used to register a `pytools.serialization.Serializable` class.
	"""

	# ##################################################
	# ###                 CONSTRUCTOR                ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, typename, namespace=None, fields_names=[] ):
		"""
		Initializes a new instance of the `pytools.serialization.RegisterSerializableAttribute` class.

		Arguments:
			self (`pytools.serialization.RegisterSerializableAttribute`): The attribute to initialize.
			typename                                             (`str`): Name of the type to register.
			namespace                                     (`str`/`None`): The namespace in which to group the type.
			fields_names                               (`list` of `str`): The names of the fields to serializes.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )
		assert pytools.assertions.type_is( typename, str )
		assert pytools.assertions.type_in( namespace, (type(None), str) )
		assert pytools.assertions.type_is( fields_names, list )
		assert pytools.assertions.list_items_type_is( fields_names, str )
		
		super( RegisterSerializableAttribute, self ).__init__(
			   group = 'Serializable',
			typename = typename
			)

		self.namespace   = namespace
		self.fieldsNames = fields_names

	# def __init__ ( self, typename, namespace, fields_names )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def fieldsNames ( self ):
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )
		"""
		The names of the fields considered during serialization
		of the object decorated by this attribute (`list` of `str`).
		"""
		return self._fields_names
	
	# def fieldsNames ( self )
	
	# --------------------------------------------------

	@fieldsNames.setter
	def fieldsNames ( self, fields_names ):		
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )
		assert pytools.assertions.type_is( fields_names, list )
		assert pytools.assertions.list_items_type_is( fields_names, str )

		self._fields_names = fields_names
	
	# def fieldsNames ( self, fields_names )

	# --------------------------------------------------

	@property
	def namespace ( self ):
		"""
		The namespace of the serializable object decorated by this attribute (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )

		return self._namespace
	
	# def namespace ( self )

	# --------------------------------------------------

	@namespace.setter
	def namespace ( self, namespace ):
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )
		assert pytools.assertions.type_in( namespace, (type(None), str) )

		self._namespace = namespace
	
	# def namespace ( self, namespace )

	# ##################################################
	# ###                  OPERATORS                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __call__ ( self, cls ):
		"""
		Sets the `typename` and `field_names` class fields in the `class` `cls`
		decorated by this attribute and registers it in the `ClassFactory` in
		the `'Serializable'` group.

		Arguments:
			self (`pytools.serialization.RegisterSerializableAttribute`): The attribute.
			cls                                                 (`type`): The class to register.

		Returns
			`type`: The class oncd registered.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterSerializableAttribute )
		assert pytools.assertions.type_is( cls, type )

		# Check that the given type inherits from the class Serializable
		if not issubclass( cls, Serializable ):

			# Raise an error
			raise pytools.factory.RegistrationError.format(
				"The class '{}' does not inherit from 'pytools.serialization.Serializable'",
				cls.__name__
				)

		# Get the names of the inherited serialized fields
		inherited_fields_names = []

		# Iterate the base classes
		for base_type in cls.__bases__:

			# Only considere 'Serializable' base class
			if issubclass( base_type, Serializable ):
				
				# Iterate the fields
				for field_name in base_type.__fields_names__:
					
					# Ignore the duplicates
					if ( field_name not in inherited_fields_names ):
						
						inherited_fields_names.append( field_name )
					
					# if ( field_name not in inherited_fields_names )

				# for field_name in base_type.__fields_names__
			
			# if issubclass( base_type, Serializable )

		# for base_type in cls.__bases__

		# Check for duplicates in the new field names
		for field_name in self.fieldsNames:

			if field_name in inherited_fields_names:

				raise pytools.factory.RegistrationError.format(
					"The field named '{}' is already defined in a base class",
					field_name
					)
			
			# if field_name in inherited_fields_names

		# Add the namespace to the typename
		if ( self.namespace ):
			self.typename = self.namespace + '.' + self.typename

		# Assign the typename and field names
		cls.__typename__     = self.typename
		cls.__fields_names__ = inherited_fields_names + self.fieldsNames

		super( RegisterSerializableAttribute, self ).__call__( cls )
		
		# Return the type
		return cls

	# def __call__ ( self, cls )

# class RegisterSerializableAttribute