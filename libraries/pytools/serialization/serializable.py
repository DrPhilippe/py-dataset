# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from . import functions

# ##################################################
# ###             CLASS SERIALIZABLE             ###
# ##################################################

class Serializable:
	"""
	Base class for type that can be serialized to python `dict`.
	"""

	# ##################################################
	# ###               STATIC-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Name of the type.
	This class field is modified when the type is registered.
	"""
	__typename__ = 'pytools.serialization.Serializable'

	# --------------------------------------------------

	"""
	Names of the instance fields to considere during serialization/deserialization.
	"""
	__fields_names__ = []

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this `Serializable` instance to a python `dict`.

		Arguments:
			self (`pytools.serialization.Serializable`): The instance to serialize.
			ignores                   (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, Serializable )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		# Save at least the typename of the Serializable
		data = { 'typename': type(self).__typename__ }

		# Save registered fields
		for field_name in type(self).__fields_names__:
			
			# Ignore ?
			if field_name in ignores:
				continue

			# Serialize
			value = getattr(self, field_name)
			data[ field_name ] = functions.serialize( value )
		
		# for field_name in type(self).__fields_names__

		# Return the data
		return data

	# def __serialize__ ( self )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this `pytools.serialization.Serializable` instance
		using the values found in the given `data` dictionary.

		Arguments:
			self (`pytools.serialization.Serializable`): The instance to deserialize.
			data                               (`dict`): Fields values.
			ignores                   (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, Serializable )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		for field_name in type(self).__fields_names__:
			
			# Ignore ?
			if field_name in ignores:
				continue

			# Deserialize
			value = data[ field_name ]
			value = functions.deserialize( value )

			try:
				setattr( self, field_name, value )

			except AttributeError as e:
				raise AttributeError( 'Cannot set attribute named \'{}\' on object of type \'{}\': {}'.format(
					field_name,
					type( self ).__typename__,
					e
					))
		
		# for field_name in type(self).__fields_names__

	# def __deserialize__ ( self, data )

# class Serializable