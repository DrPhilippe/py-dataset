# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import base64
import numpy

# INTERNALS
import pytools.assertions
import pytools.factory
import pytools.serialization

# ##################################################
# ###                  FUNCTIONS                 ###
# ##################################################

# --------------------------------------------------

def serialize ( item, ignores=[] ):
	"""
	Serializes the given `item` and its sub-items as a native python type(s).
	Object instances are converted to python `dict`.

	Arguments:
		item              (`any`): The item to serialize.
		ignores (`list` of `str`): Names of fields that should be ignored.

	Returns:
		`any`: Nested native python type representing the item.
	"""
	assert pytools.assertions.type_is( ignores, list )
	assert pytools.assertions.list_items_type_is( ignores, str )
		
	if issubclass( type(item), pytools.serialization.Serializable ):
		return item.serialize( list(ignores) )

	elif isinstance( item, list ):
		lst = []
		if ( len(item) != 0 ):
			for enty in item:
				lst.append( serialize( enty ) )
		return lst

	elif isinstance( item, dict ):
		dct = {}
		if ( len(item) != 0 ):
			for key, value in item.items():
				dct[ key ] = serialize( value )
		return dct

	else:
		return item

# def serialize ( item )

# --------------------------------------------------

def serialize_ndarray ( array ):
	"""
	Serializes the given `numpy.ndarray`.

	Arguments:
		array (`numpy.ndarray`): Array to serialize.

	Returns:
		values          (`str`): Binary string that encodes the values of the array in base 64.
		shape (`list` of `int`): Shape of the array represented as a list.
		dtype           (`str`): Name of the data type of the array.
	"""
	assert pytools.assertions.type_is_instance_of( array, (type(None), numpy.ndarray) )

	if array is None:
		return None, None, None

	# Serialize the values as binary text
	values = array.tostring()
	values = base64.encodebytes( values )
	values = str( values, 'utf8' )
	
	# Serialize the shape as list
	shape = list(array.shape)

	# Serialize the data type as text
	dtype = array.dtype.name

	# return them
	return values, shape, dtype

# def serialize_ndarray ( array )

# --------------------------------------------------

def deserialize ( data, ignores=[] ):
	"""
	Dseerializes the given `data` and its sub-items.
	Dictionary with a `'typename'` sub-item are deserialized as python class instance.

	Arguments:
		data              (`any`): Nested native python types.
		ignores (`list` of `str`): Names of fields that should be ignored.

	Returns:
		any`: Python types or class instance.
	"""
	assert pytools.assertions.type_is( ignores, list )
	assert pytools.assertions.list_items_type_is( ignores, str )

	# if it is a dict
	if isinstance( data, dict ):

		# But it is not a Serializable instance
		if not 'typename' in data.keys():

			# Iterate the key-values pairs of the dict
			dct = {}
			if ( len(data) != 0 ):
				for key, value in data.items():

					# Deserialize the values of the dict
					dct[ key ] = deserialize( value )

			# return the new dict
			return dct

		# if not 'typename' in data.keys()

		# if it is a Serializable instance
		else:

			# Fetch the typename
			typename = data[ 'typename' ]

			# Instantiate the class 
			instance = pytools.factory.ClassFactory.instantiate( group_name='Serializable', typename=typename )

			# deserialize the fields
			instance.deserialize( data, list(ignores) )

			# return the isntance
			return instance

		# else

	# if isinstance( data, dict )

	# if it is a list
	elif isinstance( data, list ):

		# iterate the value in the list
		lst = []

		if ( len(data) != 0 ):
			for value in data:
				# Deserialize them
				lst.append ( deserialize(value) )

		# return the new list
		return lst

	# elif isinstance( data, list )

	# if it is anything else
	else:

		# return it untouched
		return data

	# else

# def deserialize ( data )

# --------------------------------------------------

def deserialize_ndarray ( values, shape, dtype ):
	"""
	Deserializes an `numpy.ndarray` from its values, shape and data type.

	Arguments:
		values          (`str`): Binary string that encodes the values of the array in base 64.
		shape (`list` of `int`): Shape of the array represented as a list.
		dtype           (`str`): Name of the data type of the array.

	Returns:
		`numpy.ndarray`: The deserialized array.
	"""
	assert pytools.assertions.type_in( values, (type(None), str) )
	assert pytools.assertions.type_is( shape, list )
	assert pytools.assertions.list_items_type_is( shape, int )
	assert pytools.assertions.type_is( dtype, str )

	if values is None:
		return None

	# Deserialize the data type and the shape
	dtype = numpy.dtype(dtype)
	shape = tuple(shape)

	# Deserialzize the str values as bytes
	values = bytes( values, 'utf8' )
	values = base64.decodebytes( values )
	array  = numpy.frombuffer( values, dtype )
	array  = numpy.reshape( array, shape )
	
	# return the array
	return array

# def deserialize_ndarray ( values, shape, dtype )