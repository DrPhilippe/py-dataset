# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

def map_value ( value, a, b, A, B, dtype=None ):
	"""
	Maps a value from one range to an other.
	"""
	assert pytools.assertions.equal( type(value), type(a) )
	assert pytools.assertions.equal( type(value), type(b) )
	assert pytools.assertions.equal( type(A), type(B) )
	assert pytools.assertions.true( a <= value )
	assert pytools.assertions.true( value <= b )
	if dtype is None:
		dtype = type( A )
	else:
		assert pytools.assertions.equal( type(A), dtype )

	# Figure out how 'wide' each range is
	s = float( b - a )
	S = float( B - A )
	
	# Convert the left range into a 0-1 range (float)
	value = float( value - a ) / s
	
	# Convert the 0-1 range into a value in the right range.
	return dtype( float(A) + (value * S) )