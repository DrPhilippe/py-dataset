# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import importlib.util

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization
import pytools.tasks

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

def load_module ( module_filepath, namespace='' ):
	"""
	Loads a python module and returns it.

	Arguments:

	"""
	assert pytools.assertions.type_is_instance_of( module_filepath, (str, pytools.path.FilePath) )
	assert pytools.assertions.type_is( namespace, str )
	if isinstance( module_filepath, str ):
		module_filepath = pytools.path.FilePath( module_filepath )
	assert pytools.assertions.equal( module_filepath.extension(), '.py' )

	# Compose namespace
	if namespace:
		module_namespace = '{}.{}'.format( namespace, module_filepath.filename() )
	else:
		module_namespace = module_filepath.filename()

	# Get the module import specifications
	module_spec = importlib.util.spec_from_file_location( module_namespace, str(module_filepath) )

	# Check the package import specifications
	if ( module_spec is None ):
		raise IOError( 'Could not create module specification for module {} in file {}'.format(module_namespace, module_filepath) )
	
	# Get the package
	module = importlib.util.module_from_spec( module_spec )
	
	# Check
	if ( module is None ):
		raise IOError( 'Failed to import plugins module {} of file {}'.format(module_namespace, module_filepath) )
	
	# Execute module
	module_spec.loader.exec_module( module )

	# Return the module
	return module

# def load_module ( module_filepath, namespace )

# --------------------------------------------------

def load_plugins ( directory_path, namespace='plugins', progress_tracker=None ):
	"""
	Loades python plugin modules found in the given directory.

	Arguments:
		directory_path      (`pytools.path.DirectoryPath`): Path of the directroy in which to seach for plugins.
		namespace                                  (`str`): Namespace into which to load the plugins.
		progress_tracker (`pytools.tasks.ProgressTracker`): Progress tracker
	"""
	assert pytools.assertions.type_is( directory_path, pytools.path.DirectoryPath )
	assert pytools.assertions.type_is( namespace, str )
	assert pytools.assertions.type_is_instance_of( progress_tracker, pytools.tasks.ProgressTracker )

	# Check it exists
	if ( directory_path.is_not_a_directory() ):
		raise IOError( 'The plugin directory {} does not exist'.format(directory_path) )

	# Look for python modules in it
	plugins = directory_path.list_contents()
	count   = len(plugins)

	if progress_tracker:
		progress_tracker.update(0.0)

	for index in range(count):

		plugin_path = plugins[ index ]

		if ( plugin_path.name() in [ '__init__.py', '__pycache__' ] ):
			pass

		elif ( plugin_path.extension() == '.py' ):
			
			load_module( plugin_path, namespace )

		# if ( plugin_path.Extension() == '.py' )

		if progress_tracker:
			progress_tracker.update( float(index)/float(count) )

	# for index in range(count)

	if progress_tracker:
		progress_tracker.update(1.0)

# def load_plugins ( directory_path, namespace )