# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# ##################################################
# ###                CLASS LOADED                ###
# ##################################################

class Loaded:
	"""
	The loaded class is used to managed a serializable object save to file.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, value=None, filepath=None ):
		"""
		Initializes a new instance of the `pytools.Loaded` class.

		Arguments:
			self                             (`pytools.Loaded`): Instance to initialize.
			value (`None`/`pytools.serialization.Serializable`): The loaded value.
			filepath     (`None`/`str`:`pytools.path.FilePath`): Path of the file where the value is saved.
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.serialization.Serializable) )
		assert pytools.assertions.type_is_instance_of( filepath, (type(None), str, pytools.path.FilePath) )

		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )

		# Set properties
		self._value    = value
		self._filepath = filepath
		self._is_saved = filepath is not None and filepath.is_a_file()

	# def __init__ ( self, value, filepath )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def filepath ( self ):
		"""
		Path of the file where the object is saved (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )

		return self._filepath

	# def filepath ( self )

	# --------------------------------------------------

	@property
	def is_not_saved ( self ):
		"""
		Boolean stating if the object is not saved (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )

		return not self._is_saved
	
	# def is_not_saved ( self )

	# --------------------------------------------------

	@property
	def is_saved ( self ):
		"""
		Boolean stating if the object is saved (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )

		return self._is_saved
	
	# def is_saved ( self )

	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		The loaded value (`None`/`pytools.serialization.Serializable`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )

		return self._value
	
	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, Loaded )
		assert pytools.assertions.type_is_instance_of( val, (type(None), pytools.serialization.Serializable) )

		self._value    = val
		self._is_saved = False
	
	# def value ( self, val )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def save ( self ):
		"""
		Saves this loaded value in its file.

		Arguments:
			self (`pytools.Loaded`): Loaded value to save.
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )

		if self.filepath is None:
			raise RuntimeError( 'Cannot save the file if the filepath is not. Use save-as first.' )

		self.filepath.serialize_json( self.value )
		self._is_saved = True

	# def save ( self )

	# --------------------------------------------------

	def save_as ( self, filepath ):
		"""
		Saves this loaded value in the file at the given filepath.

		Arguments:
			self                  (`pytools.Loaded`): Loaded value to save.
			filepath (`str`:`pytools.path.FilePath`): Path of the file where to save this loaded value.
		"""
		assert pytools.assertions.type_is_instance_of( self, Loaded )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		# Cast the filepath if necessary
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )
			
		# Set the filepath
		self._filepath = filepath

		# Save the value
		self.save()

	# def save_as ( self, filepath )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@staticmethod
	def create ( value_type, **kwargs ):
		"""
		Creates a new loaded instance of the given value type.

		Arguments:
			value_type                 (`type`): Type of serializable object to create.
			**kwargs (`dict` of `str` to `any`): Arguments fowarded to the constructor of the serializable object.

		Returns:
			`pytools.Loaded`: The loaded object.
		"""
		assert pytools.assertions.type_is( value_type, type )

		# Create the value
		value = value_type( **kwargs )
		assert pytools.assertions.type_is_instance_of( value, pytools.serialization.Serializable )

		# Create the loaded instance
		loaded = Loaded( value )

		# Return it
		return loaded

	# def create ( value_type, **kwargs )

	# --------------------------------------------------

	@staticmethod
	def load ( filepath ):
		"""
		Open the file at the given filepath and loads its contents.
		
		Arguments:
			filepath (`str`/`pytools.path.FilePath`): Path of the file to load.

		Returns:
			`pytools.Loaded`: The loaded object.
		"""
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		# Cast the filepath if necessary
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )

		# Load the serialized object
		value = filepath.deserialize_json()

		# Create the loaded instance
		loaded = Loaded( value, filepath )

		# Return it
		return loaded

	# def load ( filepath )

# class Loaded