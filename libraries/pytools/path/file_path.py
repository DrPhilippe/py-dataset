# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import copy
import cv2
import json
import numpy
import os
import os.path
import shutil
import yaml

# INTERNAL
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .path import Path

# ##################################################
# ###              CLASS FILE-PATH               ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FilePath',
	   namespace = 'pytools.path',
	fields_names = []
	)
class FilePath ( Path ):
	"""
	Class used to serialize file path.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='' ):
		"""
		Initializes a new instance of the `FilePath` class.

		Arguments:
			self (`pytools.path.FilePath`): The FilePath instance to initialize
			value                  (`str`): The path of the file.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		assert pytools.assertions.type_is( value, str )

		super( FilePath, self ).__init__(
			value = value
			)

	# def __init__ ( self, value )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, path ):
		"""
		Creates a copy of this file at the given path.

		Arguments:
			self (`pytools.path.FilePath`): The file to copy.
			path (`pytools.path.FilePath`): Where to copy it.

		Raises:
			ValueError: if this file does not exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		assert pytools.assertions.type_is_instance_of( path, FilePath )

		if self.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(self.value) )

		shutil.copyfile( self.value, path.value )

	# def copy ( self, path )

	# --------------------------------------------------

	def create ( self ):
		"""
		Creates and empty file at this file path.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to create.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		f = open( self.value, 'x' )
		f.close()

	# def create ( self )

	# --------------------------------------------------
	
	def delete ( self ):
		"""
		Deletes this file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to delete.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		if self.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(self.value) )

		os.remove( self.value )
	
	# def delete ( self )
	
	# --------------------------------------------------

	def deserialize_json ( self, ignores=[] ):
		"""
		Deserializes the data found in this `JSON` file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the JSON file.
			ignores      (`list` of `str`): Names of fields that should not be deserialize.

		Returns:
			`any`: The deserialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		if self.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(self.value) )

		with self.open( 'r' ) as file:

			# Read all the file and close it
			contents = file.read()
			file.close()

		# with self.open( 'rt' ) as file

		# load the json data
		try:
			data = json.loads( contents )
		except Exception as error:
			raise RuntimeError( 'Fail to load json file {}: {}'.format(self.get_value(), str(error)) )

		# deserialize
		return pytools.serialization.deserialize( data, ignores )

	# def seserialize_json ( self )

	# --------------------------------------------------

	def ensure_uniqueness ( self, postfix_format='{}_{:d}.{}' ):
		"""
		Modifies the name of the file until it does not exists by adding a postfix.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		i = 0
		filepath = copy.deepcopy( self )
		parent = self.parent()
		while filepath.exists() or filepath.is_a_file():
			i += 1
			filepath = parent + FilePath.format( postfix_format, self.name(), i, self.extension() )

		self._value = filepath._value

	# --------------------------------------------------

	def extension ( self ):
		"""
		Returns the extension of the file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file of which to get the extension.

		Returns:
			`str`: The extension of the file.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		return os.path.splitext( os.path.basename(self.value) )[1].lower()

	# def extension ( self )
	
	# --------------------------------------------------

	def filename ( self ):
		"""
		Returns the name of the file without the extension.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file of which to get the name.

		Returns:
			`str`: The name of the file without the extension.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		return os.path.splitext( os.path.basename(self.value) )[0]

	# def filename ( self )
	
	# --------------------------------------------------

	def get_relative_to ( self, other ):
		"""
		Returns a new file path pointing to this file but relatively to the other path.

		Arguments:
			self (`pytools.path.FilePath`): The file path of which to get get a relative file path.
			other    (`pytools.path.Path`): The path from which the relative path will start.

		Returns:
			`pytools.path.FilePath`: The new relative file path.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		assert pytools.assertions.type_is_instance_of( other, Path )
	
		return FilePath( os.path.relpath( self.value, other.value ) )

	# def get_relative_to ( self, other )

	# --------------------------------------------------

	def is_a_directory ( self ):
		"""
		Returns `False` because files are never directories.

		Arguments:
			self (`pytools.path.FilePath`): The path.

		Returns:
			`bool`: `False`.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		# FilePath are never a directory
		return False

	# def is_a_directory ( self )

	# --------------------------------------------------

	def is_not_a_directory ( self ):
		"""
		Returns `True` because files are never directories.

		Arguments:
			self (`pytools.path.FilePath`): The path.

		Returns:
			`bool`: `True`.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		# FilePath are never a directory
		return True
	
	# def is_not_a_directory ( self )

	# --------------------------------------------------

	def open ( self, mode ):
		"""
		Opens this file in the given mode.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to open.
			mode                   (`str`): The mode in which to open the file.
		
		Return:
			`io.IOBase`: The file stream.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		assert pytools.assertions.type_is( mode, str )
		
		if 'r' in mode and self.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(self.value) )

		return open( self.value, mode )

	# def open ( self, mode )

	# --------------------------------------------------
	
	def parent ( self ):
		"""
		Get the parent directory of this directory path.

		Arguments:
			self (`pytools.path.FilePath`): The directory path of which to get the parent directory path.

		Returns:
			`pytools.path.DirectoryPath`: The parent directory of this path.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		
		parent_value = os.path.dirname( self.value )
		return pytools.path.DirectoryPath( parent_value )

	# def parent ( self )

	# --------------------------------------------------

	def read_binary ( self ):
		"""
		Reads the contents of this file and returns it.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to read.

		Returns:
			`bytes`: the contents of the file
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		with self.open( 'rb' ) as file:
			contents = file.read()
			file.close()
		return contents

	# def read_binary ( self )
	
	# --------------------------------------------------

	def read_image ( self, mode=cv2.IMREAD_UNCHANGED ):
		"""
		Reads the image.

		Arguments:
			self (`pytools.path.FilePath`): The image to read.
			mode                   (`int`): Read mode.

		Returns:
			`numpy.ndarray`: the image.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		return cv2.imread( self.value, mode )

	# def read_image ( self )

	# --------------------------------------------------

	def read_json ( self ):
		"""
		Reads the data found in this `JSON` file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the JSON file.

		Returns:
			`any`: The json data.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		if self.is_not_a_file():
			raise IOError( "The file '{}' does not exist".format(self.value) )

		# Read all the file and close it
		with self.open( 'r' ) as file:
			contents = file.read()
			file.close()

		# load the json data
		try:
			data = json.loads( contents )
		except Exception as error:
			raise RuntimeError( 'Fail to load json file {}: {}'.format(self.get_value(), str(error)) )

		# Return json
		return data

	# def read_json ( self )

	# --------------------------------------------------

	def read_lines ( self ):
		"""
		Read the lines of this text file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to read.
		
		Returns:
			`list` of `str`: The lines of the file.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		with self.open( 'r' ) as file:
			lines = file.readlines()
			file.close()
		# with self.open( 'rb' ) as file

		for i in range( len(lines) ):
			lines[ i ] = lines[ i ].strip( ' \n\r' )
		
		return lines

	# def read_lines ( self )

	# --------------------------------------------------

	def read_ndarray ( self, shape=None, dtype=None ):
		"""
		Reads the numpy ndarray contents of this file and returns it.

		Arguments:
			self   (`pytools.path.FilePath`): The path of the file to read.
			shape (`None`/`tuple` of `int`s): The shape of the array to read.
			shape (`None`/`str`): The data type of the array to read.

		Returns:
			`numpy.ndarray`: The array of the file
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )
		assert pytools.assertions.type_in( shape, (type(None), tuple) )
		if isinstance( shape, tuple ):
			assert pytools.assertions.tuple_items_type_is( shape, int )
		assert pytools.assertions.type_in( dtype, (type(None), str) )
		
		if self.extension() == '.txt':
			array = numpy.loadtxt( self.value )
		elif self.extension() == '.npy':
			array = numpy.load( self.value )
		else:
			raise IOError( 'Unsupported extension "{}" for numpy ndarray (file: {})'.format(self.extension(), self.value) )
		
		if shape is not None:
			array = numpy.reshape( array, shape )
		
		if dtype is not None:
			array = array.astype( dtype )

		return array

	# def read_ndarray ( self, shape, dtype )

	# --------------------------------------------------

	def read_txt ( self ):
		"""
		Reads the text contents of this file and returns it.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to read.

		Returns:
			`str`: the contents of the file
		"""
		with self.open( 'r' ) as file:
			contents = file.read()
			file.close()
		return contents

	# def read_txt ( self )

	# --------------------------------------------------

	def read_yml ( self ):
		"""
		Reads the text contents of this file and returns it.

		Arguments:
			self (`pytools.path.FilePath`): The path of the file to read.

		Returns:
			`str`: the contents of the file
		"""
		with self.open( 'r' ) as file:
			import yaml
			return yaml.load( file, Loader=yaml.CLoader )

	# def read_yml ( self )

	# --------------------------------------------------

	def serialize_json ( self, value, ignores=[] ):
		"""
		Serializes the given `value` to `JSON` and writes the text in this file.

		Arguments:
			self (`pytools.path.FilePath`): The path of the JSON file.
			value                  (`any`): Data to serialize.
		ignores (`list` of `str`): Names of fields that should not be serialized.
		"""
		assert pytools.assertions.type_is_instance_of( self, FilePath )

		# serialize the value
		data = pytools.serialization.serialize( value, ignores )

		# convert the data to json text
		contents = json.dumps(
				data,
				 sort_keys = True,
				    indent = 4,
				separators = (',', ': ')
				)

		if self.parent() and self.parent().is_not_a_directory():
			self.parent().create()

		# Open the example file
		with self.open( 'w' ) as file:
		
				# Save to file
				file.write( contents )
				
				file.close()
		
		# with self.open( 'wt' ) as file

	# def serialize_json ( self, value )

# class FilePath ( Path )
