# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import copy
import os
import os.path
import re
import shutil

# INTERNAL
import pytools.assertions
import pytools.serialization

# ##################################################
# ###                 CLASS PATH                 ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'Path',
	   namespace = 'pytools.path',
	fields_names = [
		'value'
		]
	)
class Path ( pytools.serialization.Serializable ):
	"""
	Base class used to serialize path.
	"""
	
	# ##################################################
	# ###               STATIC-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	_regex = re.compile( r'\\{1,}|\/{1,}' )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='' ):
		"""
		Initializes a new instance of the `pytools.path.Path` class.

		Arguments:
			self (`pytools.path.Path`): The path instance to initialize.
			value              (`str`): The textual value of the path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is( value, str )

		# Make sure the path uses forward slashes
		value = Path._regex.sub( '/', value )

		# Remove trailling /
		if value.endswith( '/' ):
			value = value[ 0 : -1 ]

		# set the textual path
		self.value = value

	# def __init__ ( self, value )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def ensure ( cls, value ):
		"""
		Ensures that the type of value is `cls`.

		Arguments:
			cls        (`type`): The type of path to ensure.
			value (`str`/`cls`): The path.

		Returns:
			`cls`: Path with ensured type.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( value, (cls, str) )

		return copy.deepcopy(value) if isinstance(value, cls) else cls( value )

	# --------------------------------------------------

	@classmethod
	def format ( cls, format_text, *args ):
		"""
		Creates a new `cls` instance by formating argument in `format_text`.

		Arguments:
			cls        (`type`): The type of path to create.
			format_text (`str`): The format text.
			args      (`tuple`): The arguments to format in the text.

		Returns:
			`pytools.path.Path`: The new path instance.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( format_text, str )
		assert pytools.assertions.type_is( args, tuple )

		return cls( format_text.format(*args) )

	# def format ( format_text, *args )

	# --------------------------------------------------
	
	@classmethod
	def from_nodes ( cls, nodes, separator='/' ):
		"""
		Creates a new `cls` instance from nodes.

		Arguments:
			cls             (`type`): The type of path to create.
			nodes           (`list`): The list of nodes.
			separator (`str`/`None`): The separator used to concatenate the nodes.

		Returns:
			`pytools.path.Path`: The new path instance.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( nodes, list )
		assert pytools.assertions.list_items_type_is( nodes, str )
		assert pytools.assertions.type_in( separator, (type(None), str) )

		# Convert the nodes to a textual path
		value = Path.nodes_to_textual_path( nodes, separator )

		# Create and return the new path
		return cls( value )

	# def from_nodes ( nodes, separator )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@staticmethod
	def textual_path_to_nodes ( value ):
		"""
		Converts a textual path to nodes.

		Arguments:
			value (`str`): Textual path of which to get the nodes.

		Returns:
			`list`: The list of nodes.
		"""
		assert pytools.assertions.type_in( value, (type(None), str) )

		# empty path case
		if ( not value ):
			return list()

		# Split using any possible separators
		return list( filter( lambda x: len(x)>0, Path._regex.split( value ) ) )

	# def textual_path_to_nodes ( self )
	
	# --------------------------------------------------

	@staticmethod
	def nodes_to_textual_path ( nodes, separator='/' ):
		"""
		Converts the given nodes to a textual path.

		Arguments:
			nodes    (`list`): The list of nodes.
			separator (`str`): The separator used to concatenate the nodes.

		Returns:
			`str`: This path once its value is set from the nodes.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is( nodes, list )
		assert pytools.assertions.list_items_type_is( nodes, str )
		assert pytools.assertions.type_in( separator, (type(None), str) )

		# Empty path case
		if ( not nodes ):
			return ''

		# Default separator
		if ( separator is None ):
			separator = os.sep

		# Concat nodes using separator
		return separator.join( nodes )

	# def nodes_to_textual_path ( nodes, separator )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		The textual value of this path (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return self._value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is( value, str )

		self._value = value

	# def value ( self, value )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def exists ( self ):
		"""
		Checks if this path exists (both file or directory)

		Arguments:
			self (`pytools.path.Path`): The `Path`.

		Returns:
			`bool`: `True` if the `Path` exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		return os.path.exists( self.value )

	# def exists ( self )

	# --------------------------------------------------

	def get_value ( self, separator=os.sep ):
		"""
		Returns the value of this path with the given separator between the nodes.

		Arguments:
			self (`pytools.path.Path`): Path of which to get the textual value.
			separator          (`str`): Separator used between the nodes.

		Returns:
			`str`: The textual path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		
		return self.value.replace( '/', separator )

	# def get_value ( self, separator )

	# --------------------------------------------------

	def is_absolute ( self ):
		"""
		Check if this path is absolute (i.e. starts at the root or with a drive letter).

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if this path is absolute, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		
		return os.path.isabs( self.value )

	# def is_absolute ( self )

	# --------------------------------------------------

	def is_relative ( self ):
		"""
		Check if this path is relative (i.e. does not starts at the root or with a drive letter).

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if this path is relative, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return not self.is_absolute()

	# def is_relative ( self )

	# --------------------------------------------------

	def is_a_directory ( self ):
		"""
		Checks if this path is an existing directory.

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if this path is an existing directory, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return os.path.isdir( self.value )

	# def is_a_directory ( self )

	# --------------------------------------------------

	def is_not_a_directory ( self ):
		"""
		Checks if this path is not an existing directory.

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if thius path is not an existing directory, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return not os.path.isdir( self.value )
	
	# def is_not_a_directory ( self )

	# --------------------------------------------------

	def is_a_file ( self ):
		"""
		Checks if this path is an existing file.

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if this path is an existing file, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return os.path.isfile( self.value )

	# def is_a_file ( self )

	# --------------------------------------------------

	def is_not_a_file ( self ):
		"""
		Checks if this path is not an existing file.

		Arguments:
			self (`pytools.path.Path`): The `Path`.

		Returns:
			`bool`: `True` if this path is not an existing file, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return not os.path.isfile( self.value )

	# def is_not_a_file ( self )

	# --------------------------------------------------

	def move ( self, new_path ):
		"""
		Move the content(s) located at this `Path` to a new location.
		
		The new path must point to the same type of content(s)as this path (`pytools.path.DirectoryPath` or `pytools.path.FilePath`).
	
		Arguments:
			self     (`pytools.path.Path`): Path of the content(s) to move.
			new_path (`pytools.path.Path`): Path where to move the content(s).
		
		Returns:
			`pytools.path.Path`: This path once its contents have been moved.

		Raises:
			`ValueError`: if the two path do not have the same type.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is_instance_of( new_path, Path )
		
		# Check the type of the two Path
		if ( type(self) != type(new_path) ):
			raise ValueError( 'Cannot move Path of different types: {} vs {}'.format(type(self), type(new_path)) )

		# Move the path
		shutil.move( self.value, new_path.value )

		# Save the new Path
		self.value = new_path.value

		return self

	# def move ( self, new_path )

	# --------------------------------------------------
	
	def name ( self ):
		"""
		Name of the content(s) located at this path.
		
		For a file this is the filename (with extension) and for a directory its name.

		Arguments:
			self (`pytools.path.Path`): The path of which toi get the mame.

		Returns:
			`str`: The name of the content(s) located at this path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return os.path.basename( self.value )

	# def name ( self )

	# --------------------------------------------------
	
	def parent ( self ):
		"""
		Get the parent path of this path.

		Arguments:
			self (`pytools.path.Path`): The path of which to get the parent path.

		Returns:
			`pytools.path.Path`: The parent path of this path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		
		parent_value = os.path.dirname( self.value )
		return Path( parent_value )

	# def parent ( self )

	# ##################################################
	# ###                OPERATORS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def __bool__ ( self ):
		"""
		Checks if this path is defined (its textual value is not empty or None)
		
		This does not means that the content(s) located at this path exist.

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if the path is defined, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return bool(self.value)

	# def __bool__ ( self )

	# --------------------------------------------------

	def __nonzero__ ( self ):
		"""
		Checks if this path is defined (its textual value is not empty or None)
		
		This does not means that the content(s) located at this path exist.

		Arguments:
			self (`pytools.path.Path`): The path.

		Returns:
			`bool`: `True` if the path is defined, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		
		return bool(self.value)

	# def __bool__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Creates a detailed text representation of this path.

		Arguments:
			self (`pytools.path.Path`): The path to represent.

		Returns:
			`str`: The detailed text representation of this `Path`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )

		return '[{}: \'{}\']'.format( type(self).__name__, self.get_value() )

	# def __repr__ ( self )

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Return this path as a textual path.

		Arguments:
			self (`pytools.path.Path`): The path of which to return the textual path.

		Returns:
			`str`: This path as a textual path.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return self.get_value()
	
	# def __str__ ( self )

	# --------------------------------------------------

	def __eq__ ( self, other ):
		"""
		Checks if the two path are equal.

		Arguments:
			self  (`pytools.path.Path`): The first path.
			other (`pytools.path.Path`): The second path.

		Returns:
			`bool`: `True` if the two path are equal. `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is_instance_of( other, (type(None), Path) )
		 
		if ( other is None ):
			return False

		return self.value == other.value

	# def __eq__ ( self, other )

	# --------------------------------------------------

	def __neq__ ( self, other ):
		"""
		Checks if the two path are not equal.

		Arguments:
			self  (`pytools.path.Path`): The first path.
			other (`pytools.path.Path`): The second path.

		Returns:
			`bool`: `True` if the two path are not equal. `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
		assert pytools.assertions.type_is_instance_of( other, (type(None), Path) )
		
		if ( other is None ):
			return True

		return self.value != other.value

	# def __neq__ ( self, other )

# class Path