# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import os
import tempfile
import shutil
import glob

# INTERNAL
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .path import Path

# ##################################################
# ###            CLASS DIRECTORY-PATH            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DirectoryPath',
	   namespace = 'pytools.path',
	fields_names = []
	)
class DirectoryPath ( Path ):
	"""
	Class used to serialize directory path.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='' ):
		"""
		Intialize a new instance of the `pytools.path.DirectoryPath` class.

		Arguments:
			self (`pytools.path.DirectoryPath`): Instance to intialize.
			value                       (`str`): The textual location of the directory.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )
		assert pytools.assertions.type_is( value, str )

		super( DirectoryPath, self ).__init__(
			value = value
			)

	# def __init__ ( self, value )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def temporary ( cls, suffix='', prefix='tmp', parent=None ):
		"""
		Creates a temporary `cls` directory path with the given `suffix`, `prefix` and `parent`.

		Arguments:
			cls                                 (`type`): The type of directory path to create.
			suffix                               (`str`): The suffix of the new directory.
			prefix                               (`str`): The prefix of the new directory.
			parent (`None`/`pytools.path.DirectoryPath`): The parent of the new directory.
		"""
		assert pytools.assertions.type_is( cls, str )
		assert pytools.assertions.type_is( suffix, str )
		assert pytools.assertions.type_is( prefix, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), DirectoryPath) )

		# Create the directory
		value = tempfile.mkdtemp( suffix, prefix, parent.value if parent is not None else None )

		# Create the DirectoryPath for it
		return cls( value )

	# def temporary ( suffix, prefix, parent )
	
	# --------------------------------------------------
	
	@classmethod
	def unique ( cls, parent, name, name_format='{} ({})', create=True ):
		"""
		Creates a new directory with a unique name.

		Arguments:
			parent (`pytools.path.DirectoryPath`): Parent directory.
			name                          (`str`): Desired name of the directory.
			name_format                   (`str`): Name format string with one `str` and one `int` placeholders fot the desired name and index.
			create                       (`bool`): If `True` the directory is created before returing.

		Returns:
			`pytools.path.DirectoryPath`: The path to the unique directory
		"""
		assert pytools.assertions.type_is( cls, str )
		assert pytools.assertions.type_is_instance_of( parent, DirectoryPath )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( name_format, str )
		assert pytools.assertions.type_is( create, bool )

		# Path of directory to check
		directory = parent + DirectoryPath( name )

		# Name offset
		index = 1
		
		# As long as the name is already taken
		while directory.is_a_directory():
			
			# Increment name offset
			index += 1

			# Create a new name/path for the directory
			directory = parent + DirectoryPath.format( name, index )
			
		# while directory.IsADirectory()

		# Create the directory
		if ( create ):
			directory.create()
	
		return directory
				
	# def Unique ( cls, parent, name )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def create ( self ):
		"""
		Creates this directory (and its parent).

		Arguments:
			self (`pytools.path.DirectoryPath`): The DirectoryPath to create.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )

		os.makedirs( self.value )

	# def Create ( self )
	
	# --------------------------------------------------

	def delete ( self, ignore_errors=True ):
		"""
		Deletes this directory and its contents.

		Arguments:
			self (`pytools.path.DirectoryPath`): The directory to delete.
			ignore_errors              (`bool`): If `True` errors do not raise exceptions.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )
		assert pytools.assertions.type_is( ignore_errors, bool )

		shutil.rmtree( self.value, ignore_errors )

	# def delete ( self )

	# --------------------------------------------------

	def get_relative_to ( self, other ):
		"""
		Return a new directory path pointing to this directory but relative to the other one.
		(self == other/result)

		Arguments:
			self  (`pytools.path.DirectoryPath`): This directory.
			other (`pytools.path.DirectoryPath`): The starting directory for the new relative DirectoryPath.

		Returns:
			`pytools.path.DirectoryPath`: The new directory path
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )
		assert pytools.assertions.type_is_instance_of( other, DirectoryPath )
		
		value = os.path.relpath( self.value, other.value )
		return DirectoryPath( value )

	# def GetRelativeTo ( self, other )

	# --------------------------------------------------

	def is_a_file ( self ):
		"""
		Returns `False` because directory are never files.

		Arguments:
			self (`pytools.path.DirectoryPath`): The path.

		Returns:
			`bool`: `False`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return False

	# def is_a_file ( self )

	# --------------------------------------------------

	def is_not_a_file ( self ):
		"""
		Returns `True` because directory are never files.

		Arguments:
			self (`pytools.path.DirectoryPath`): The path.

		Returns:
			`bool`: `True`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Path )
	
		return True

	# def is_not_a_file ( self )
	
	# --------------------------------------------------

	def join ( self, other ):
		"""
		Concatenates the given path to this directory path.

		Arguments:
			self (`pytools.path.DirectoryPath`): Path of the starting directory.
			other         (`pytools.path.Path`): Path to append after this directory.

		Returns:
			`pytools.path.Path`: `pytools.path.FilePath` if the new path points to a file, `pytools.path.DirectoryPath` for a directory.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )
		assert pytools.assertions.type_is_instance_of( other, (Path, pytools.path.FilePath, DirectoryPath) )
		
		new_value = '/'.join( [self.value, other.value] )

		# Dont join epty path
		if ( self.value ):

			# If other is a FilePath, return a FilePath
			if isinstance( other, pytools.path.FilePath ):

				return pytools.path.FilePath( new_value )
			
			# If other is a DirectoryPath, return a DirectoryPath
			elif isinstance( other, DirectoryPath ):

				return DirectoryPath( new_value )

			# otherwise error
			else:
				raise ValueError(
					'Dont know how to join path {} and path {} ?'.format(
						repr(self),
						repr(other)
						)
					)

		else:
			return other

	# def join ( self, other )

	# --------------------------------------------------

	def list_contents ( self ):
		"""
		Lists the contents of this directory.

		Arguments:
			self (`pytools.path.DirectoryPath`): Directory of which to list the contents.

		Returns:
			`list`: List containing the files and directories in this directory.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )

		# result list
		result = []

		# for each entry in the directory
		for content in os.listdir( self.value ):

			# the entry is a directory
			if os.path.isdir( os.path.join(self.value, content) ):
				result.append( self + DirectoryPath(content) )

			# the entry is a file
			elif os.path.isfile( os.path.join(self.value, content) ):
				result.append( self + pytools.path.FilePath(content) )

			# the entry is something else
			else:
				result.append( Path(content) )

		# for content in os.listdir( self.value )

		# return the list
		return result

	# def list_contents ( self )

	# --------------------------------------------------
	
	def parent ( self ):
		"""
		Get the parent directory of this directory path.

		Arguments:
			self (`pytools.path.DirectoryPath`): The directory path of which to get the parent directory path.

		Returns:
			`pytools.path.DirectoryPath`: The parent directory of this path.
		"""
		assert pytools.assertions.type_is_instance_of( self, DirectoryPath )
		
		parent_value = os.path.dirname( self.value )
		return DirectoryPath( parent_value )

	# def parent ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __add__ ( self, other ):
		"""
		Concatenates the given path to this directory path.

		Arguments:
			self (`pytools.path.DirectoryPath`): Path of the starting directory.
			other         (`pytools.path.Path`): Path to append to this directory path.

		Returns:
			`Path`: `pytools.path.FilePath` if the new path points to a file, `DirectoryPath` for a directory.
		"""
		return self.join( other )

	# def __add__ ( self, other )

	# --------------------------------------------------

	def __floordiv__ ( self, other ):
		"""
		Concatenates the given path to this directory path.

		Arguments:
			self (`pytools.path.DirectoryPath`): Path of the starting directory.
			other         (`pytools.path.Path`): Path to append to this directory path.

		Returns:
			`pytools.path.Path`: `pytools.path.FilePath` if the new path points to a file, `pytools.path.DirectoryPath` for a directory.
		"""
		return self.join( other )

	# def __floordiv__ ( self, other )
	
	# --------------------------------------------------

	def __truediv__ ( self, other ):
		"""
		Concatenates the given path to this directory path.

		Arguments:
			self (`pytools.path.DirectoryPath`): Path of the starting directory.
			other         (`pytools.path.Path`): Path to append to this directory path.

		Returns:
			`pytools.path.Path`: `pytools.path.FilePath` if the new path points to a file, `pytools.path.DirectoryPath` for a directory.
		"""
		return self.join( other )

	# def __truediv__ ( self, other )

# class DirectoryPath ( Path )