# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import threading
import time

# INTERNAL
import pytools.assertions

# LOCALS
from .task       import Task
from .task_state import TaskState

# ##################################################
# ###               CLASS MANAGER                ###
# ##################################################

class Manager:

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------
	
	def __init__ ( self, *args ):
		"""
		Initializes a new instance of the `pytools.tasks.Manager` class.

		The `pytools.tasks.Manager` manages the execution of tasks on threads.

		Arguments:
			self         (`pytools.tasks.Manager`): Instance to initialize.
			*args (`list` of `pytools.tasks.Task`): Task to manage.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( args, tuple )
		assert pytools.assertions.tuple_items_type_is( args, pytools.tasks.Task )

		self._tasks   = list(args)
		self._threads = []

	# def __init__ ( self, task )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def tasks ( self ):
		"""
		Tasks managed by this Manager (`list` of `pytools.tasks.Task`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )

		return self._tasks

	# def tasks ( self )

	# --------------------------------------------------

	@tasks.setter
	def tasks ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, pytools.tasks.Task )

		for task in self._tasks:
			if task.is_running():
				raise RuntimeError( 'Cannot set a new list of tasks because some tasks are running' )
		
		self._tasks = value

	# def tasks ( self, value )
		
	# --------------------------------------------------

	@property
	def threads ( self ):
		"""
		Threads used to run the tasks (`list` of `threading.Thread`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )

		return self._thread
	
	# def thread ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def join_tasks ( self ):
		"""
		Waits for the threads to join the caller and destroys the threads.
		
		This also reset the tasks to the idle state.

		Arguments:
			self (`pytools.tasks.Manager`): The Manager of which to waits for the threads to join.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		
		# Make all thread join
		for thread in self._threads:
			thread.join()
		
		# Delete the threads
		self._threads = []

		# Reset the tasks who did not fail
		for task in self._tasks:
			if task.state != TaskState.failed:
				task.state = TaskState.idle

	# def join_tasks ( self )

	# --------------------------------------------------
	
	def pause_tasks ( self, wait_for_pause=False ):
		"""
		Pauses the task managed by this Manager.

		Arguments:
			self (`pytools.tasks.Manager`): The Manager of which to pause the tasks.
			wait_for_pause        (`bool`): If `True` waits for the tasks to pause.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( wait_for_pause, bool )

		paused = []

		# Pause all the tasks that are running
		for task in self._tasks:		
			if task.state == TaskState.running:
				task.pause()
				paused.append(task)

		while ( wait_for_pause ):

			wait_for_pause = False

			for task in paused:
				if not task.is_paused():
					wait_for_pause = True

			time.sleep( 0.1 )

		# while ( wait_for_pause )

	# def pause_tasks ( self )

	# --------------------------------------------------
	
	def resume_tasks ( self, wait_for_resume=False ):
		"""
		Resumes paused task managed by this Manager.

		Arguments:
			self (`pytools.tasks.Manager`): The Manager of which to resume the task.
			wait_for_resume       (`bool`): If `True` waits for the task to resume.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( wait_for_resume, bool )

		resumed = []

		# Pause all the tasks that are running
		for task in self._tasks:		
			if task.state == TaskState.paused:
				task.resume()
				resumed.append( task )
		
		while ( wait_for_resume ):

			wait_for_resume = False

			for task in resumed:
				if task.state != TaskState.running:
					wait_for_resume = True

			time.sleep( 0.1 )

		# while ( wait_for_resume )

	# def resume_tasks ( self )

	# --------------------------------------------------
	
	def start_tasks ( self, tasks=[], wait_for_start=False ):
		"""
		Starts the tasks managed by this manager.

		Arguments:
			self        (`pytools.tasks.Manager`): The Manager of which to start the task.
			task (`list` of `pytools.tasks.Task`): Overrides the tasks managed by this manager.
			wait_for_start               (`bool`): If `True` waits for the task to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( tasks, list )
		assert pytools.assertions.list_items_type_is( tasks, pytools.tasks.Task )
		assert pytools.assertions.type_is( wait_for_start, bool )

		if tasks:
			self.tasks = tasks

		started = []

		for task in self._tasks:
			if task.state in [TaskState.failed, TaskState.idle, TaskState.stopped]:

				thread = threading.Thread( target=task )
				thread.start()

				started.append( task )

		while ( wait_for_start ):

			wait_for_start = False
			
			for task in started:
				if task.state != TaskState.running:
					wait_for_start = True
			
			time.sleep( 0.1 )

		# while ( wait_for_start )

	# def start_tasks ( self )
	
	# --------------------------------------------------
	
	def stop_tasks ( self, wait_for_stop=False ):
		"""
		Stops the tasks managed by this manager.

		Arguments:
			self (`pytools.tasks.Manager`): The manager of which to stop the tasks.
			wait_for_stop         (`bool`): If `True` waits for the tasks to stop.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( wait_for_stop, bool )

		stopped = []
		
		for task in self._tasks:
			
			if task.is_running():
				
				task.stop()
				stopped.append( task )

		if wait_for_stop:
			self.wait_for_stop( stopped )	

	# def stop_tasks ( self )

	# --------------------------------------------------

	def wait_for_stop ( self, tasks=[] ):
		"""
		Waits for tasks of this manager to stop.

		Arguments:
			self         (`pytools.tasks.Manager`): The manager of which to wait for the tasks to stop.
			tasks (`list` of `pytools.tasks.Task`): Task for which to wait.

		"""
		assert pytools.assertions.type_is_instance_of( self, Manager )
		assert pytools.assertions.type_is( tasks, list )
		assert pytools.assertions.list_items_type_is( tasks, pytools.tasks.Task )
		
		if not tasks:
			tasks = self._tasks

		wait_for_stop = True

		while ( wait_for_stop ):
			wait_for_stop = False
			for task in tasks:
				if not task.stopped():
					wait_for_stop = True
			time.sleep( 0.1 )

		# while ( wait_for_stop )

	# def wait_for_stop ( self, tasks=[] )

# class Manager