# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pytools.assertions

# ##################################################
# ###           CLASS PROGRESS-TRACKER           ###
# ##################################################

class ProgressTracker:
	"""
	The class `ProgressTracker` is the base class of progress tracker used track progress of long processes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, report_every=0.01, weight=1., parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.ProgressTracker` class.
		
		Arguments:
			self          (`pytools.tasks.ProgressTracker`): Instance to initialize.
			report_every                          (`float`): Progression delta between every reports.
			weight                                (`float`): Contribution weight to parent progress.
			parent (`None`/`pytools.tasks.ProgressTracker`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( report_every, float )
		assert pytools.assertions.type_is( weight, float )
		assert pytools.assertions.true( weight > 0. )
		assert pytools.assertions.true( weight <= 1. )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), ProgressTracker) )

		self._progress     = 0.0
		self._report_every = report_every
		self._weight       = weight
		self._parent       = parent
		self._children     = []
		self._lock         = threading.Lock()


	# def __init__ ( self, start, end, report_every, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def children ( self ):
		"""
		Children of this progress tracker (`list` of `pytools.tasks.ProgressTracker`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )

		return self._children

	# def children ( self )

	# --------------------------------------------------

	@children.setter
	def children ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, ProgressTracker )
		
		self._children = value

	# def children ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of child progress tracker (`int`).
		"""
		return len(self.children)

	# def number_of_children ( self )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent of this progress tracker (`pytools.tasks.ProgressTracker`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )

		return self._parent

	# def parent ( self )

	# --------------------------------------------------

	@parent.setter
	def parent ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ProgressTracker) )
		
		self._parent = value

	# def parent ( self, value )

	# --------------------------------------------------

	@property
	def progress ( self ):
		"""
		Current progression (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )

		return self._progress

	# def progress ( self )

	# --------------------------------------------------

	@progress.setter
	def progress ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( value, float )
		
		self._progress = value

	# def progress ( self, value )

	# --------------------------------------------------

	@property
	def report_every ( self ):
		"""
		Progression delta between every reports (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )

		return self._report_every

	# def report_every ( self )

	# --------------------------------------------------

	@report_every.setter
	def report_every ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( value, float )
		
		self._report_every = value

	# def report_every ( self, value )

	# --------------------------------------------------

	@property
	def weight ( self ):
		"""
		Contribution weight to parent progress (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )

		return self._weight

	# def weight ( self )

	# --------------------------------------------------

	@weight.setter
	def weight ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( value, float )
		assert pytools.assertions.true( value > 0. )
		assert pytools.assertions.true( value <= 1. )
		
		self._weight = value

	# def weight ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def update ( self, new_progress=None ):
		"""
		Update the progress of this progress tracker.

		Arguments:
			self (`pytools.tasks.ProgressTracker`): Progress tracker to update.
			new_progress          (`float`/`None`): The new progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_in( new_progress, (float, type(None)) )
		
		self._lock.acquire()

		# Compute progression using children
		if new_progress is None:
			new_progress = 0.
			total_weight = 0.
			for child in self.children:				
				new_progress += (child.progress * child.weight)
				total_weight += child.weight
			new_progress /= total_weight

		# Progression delta
		delta = abs( int(new_progress*100.0) - int(self.progress*100.0) )

		# Check if the task progressed enough to report progression
		if ( new_progress > self.progress and delta >= int(self.report_every*100.0) ):
		
			# Update the progression of this tracker
			self.progress = new_progress

			# Update the progression of the parent
			if ( self.parent ):			
				self.parent.update()
			
			else:
				# Report progression
				self.report()

			# if ( self.parent )

		# if ( (new_progress - self.progress) >= self.report_every )
		
		self._lock.release()

	# def update ( self, new_progress )
	
	# --------------------------------------------------

	def create_child ( self, weight=1. ):
		"""
		Creates and returns a child progress tracker to track sub-tasks or sub-routines.
		
		Arguments:
			self (`pytools.tasks.ProgressTracker`): Parent progress tracker.
			weight                       (`float`): Contribution weight to parent progress.

		Returns:
			`pytools.tasks.ProgressTracker`: The new child progress tracker.
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressTracker )
		assert pytools.assertions.type_is( weight, float )
		assert pytools.assertions.true( weight > 0. )
		assert pytools.assertions.true( weight <= 1. )

		child = ProgressTracker( report_every=self.report_every, weight=weight, parent=self )
		self.children.append( child )
		return child

	# def create_child ( self )

	# --------------------------------------------------
	
	def report ( self ):
		"""
		Method called to report progression.

		Arguments:
			self (`pytools.tasks.ProgressTracker`): Progress tracker.
		"""
		pass

	# def report ( self )

# class ProgressTracker