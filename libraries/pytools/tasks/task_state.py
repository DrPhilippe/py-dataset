# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###              CLASS TASK-STATE              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TaskStatus',
	   namespace = 'pytools.tasks',
	fields_names = []
	)
class TaskState ( pytools.serialization.Enum ):
	
	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	idle     = None
	starting = None
	running  = None
	pausing  = None
	paused   = None
	resuming = None
	stopping = None
	stopped  = None
	failed   = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='idle' ):
		assert pytools.assertions.type_is_instance_of( self, TaskState )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( TaskState, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):

		return [
			'idle',
			'starting',
			'running',
			'pausing',
			'paused',
			'resuming',
			'stopping',
			'stopped',
			'failed'
			]
		
	# def get_possible_values ( cls )

# class TaskState ( pytools.serialization.Enum )

TaskState.idle     = TaskState( 'idle' )
TaskState.starting = TaskState( 'starting' )
TaskState.running  = TaskState( 'running' )
TaskState.pausing  = TaskState( 'pausing' )
TaskState.paused   = TaskState( 'paused' )
TaskState.resuming = TaskState( 'resuming' )
TaskState.stopping = TaskState( 'stopping' )
TaskState.stopped  = TaskState( 'stopped' )
TaskState.failed   = TaskState( 'failed' )