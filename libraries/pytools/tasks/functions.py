# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import sys

# INTERNAL
import pytools.assertions

# LOCALS
from .stream_logger          import StreamLogger
from .stream_progress_logger import StreamProgressLogger
from .string_logger          import StringLogger

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def console_logger ( **kwargs ):
	"""
	Creates a new `pytools.tasks.StreamLogger` that writes a log in the console (stdout).

	Arguments:
		kwargs (`dict`): Extra arguments forwarded to the constructor of `pytools.tasks.StreamLogger`.

	Returns:
		`pytools.tasks.StreamLogger`: The new file logger.
	"""
	return StreamLogger( sys.stdout, **kwargs )

# def console_logger ( **kwargs )

# --------------------------------------------------

def console_progress_logger ( header, **kwargs ):
	"""
	Creates a new `pytools.tasks.StreamProgressLogger` that writes progression in the console (stdout).

	Arguments:
		 header (`str`): Text displayed before progression starts.
		kwargs (`dict`): Extra arguments forwarded to the constructor of `pytools.tasks.StreamProgressLogger`.

	Returns:
		`pytools.tasks.StreamProgressLogger`: The stream progress tracker.
	"""
	assert pytools.assertions.type_is( header, str )

	progress_logger = StreamProgressLogger( sys.stdout, header, **kwargs )
	return progress_logger

# def console_progress_logger ( **kwargs )

# --------------------------------------------------

def file_logger ( filepath, truncate=True, **kwargs ):
	"""
	Creates a new `pytools.tasks.StreamLogger` that writes a log in the stream at the given filepath.

	Arguments:
		filepath  (`str`): Path og the log file.
		truncate (`bool`): If `True` the file is trucated.
		kwargs   (`dict`): Extra arguments forwarded to the constructor of `pytools.tasks.StreamLogger`.

	Returns:
		`pytools.tasks.StreamLogger`: The new file logger.
	"""
	assert pytools.assertions.type_is( filepath, str )
	assert pytools.assertions.type_is( truncate, bool )

	stream = open( filepath, 'wt' if truncate else 'at' )
	return StreamLogger( stream, **kwargs )

# def file_logger ( filepath, truncate=True, **kwargs )

# --------------------------------------------------

def stream_logger ( **kwargs ):
	"""
	Creates a new `pytools.tasks.StreamLogger` that writes a log in a stream.

	Arguments:
		kwargs (`dict`): Arguments forwarded to the constructor of `pytools.tasks.StreamLogger`.

	Returns:
		`pytools.tasks.StreamLogger`: The new stream logger.
	"""
	return StreamLogger( **kwargs )

# def stream_logger ( **kwargs )

# --------------------------------------------------

def string_logger ( **kwargs ):
	"""
	Creates a new `pytools.tasks.StringLogger` that writes a log in the buffered string.

	Arguments:
		kwargs (`dict`): Arguments forwarded to the constructor of `pytools.tasks.StringLogger`.

	Returns:
		`pytools.tasks.StringLogger`: The new string logger.
	"""
	return StringLogger( **kwargs )

# def string_logger ( **kwargs )