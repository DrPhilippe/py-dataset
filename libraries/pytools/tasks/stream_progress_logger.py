# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import io

# INTERNAL
import pytools.assertions

# LOCALS
from .progress_logger import ProgressLogger
from .stream_logger   import StreamLogger

# ##################################################
# ###        CLASS STREAM-PROGRESS-LOGGER        ###
# ##################################################

class StreamProgressLogger ( ProgressLogger, StreamLogger ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, stream, header,report_every=0.01, end_line_every=0.1, indent_size=4, indent_text=' ', line_separator='\n', parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.TextProgressTracker` class.
		
		Arguments:
			self          (`pytools.tasks.StreamProgressLogger`): Instance to initialize.
			stream                             (`io.TextIOBase`): Stream where to write the log.
			header                                       (`str`): Text logged before this progress logger starts logging progression.			
			report_every                               (`float`): Progression delta between every reports.
			end_line_every                             (`float`): Progression delta after which line of the log are ended.
			indent_size                                  (`int`): Width of the indentation to the left of the log.
			indent_text                                  (`str`): Text used to indent the left of the log.
			line_separator                               (`str`): Text used to terminate lines of the log.
			parent (`None`/`pytools.tasks.StreamProgressLogger`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamProgressLogger )
		assert pytools.assertions.type_is_instance_of( stream, io.TextIOBase )
		assert pytools.assertions.type_is( header, str )
		assert pytools.assertions.type_is( report_every, float )
		assert pytools.assertions.type_is( end_line_every, float )
		assert pytools.assertions.type_is( indent_size, int )
		assert pytools.assertions.type_is( indent_text, str )
		assert pytools.assertions.type_is( line_separator, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), StreamProgressLogger) )

		# First initialize the stream loger first
		# this defines the output stream
		StreamLogger.__init__( self,
			        stream = stream,
			   indent_size = indent_size,
			   indent_text = indent_text,
			line_separator = line_separator,
			        parent = parent
			)

		# Then initialize the progress logger
		# that can safely output the header to the stream
		ProgressLogger.__init__( self,
			        header = header,
			  report_every = report_every,
			end_line_every = end_line_every,
			   indent_size = indent_size,
			   indent_text = indent_text,
			line_separator = line_separator,
			        parent = parent
			)

	# def __init__ ( self, stream, header, start, end, report_every, end_line_every, indent_size, indent_text, line_separator, parent )

# class StreamProgressLogger ( ProgressLogger, StreamLogger )