# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import math

# INTERNALS
import pytools.assertions

# LOCALS
from .logger           import Logger
from .progress_tracker import ProgressTracker

# ##################################################
# ###        CLASS TEXT-PROGRESS-TRACKER         ###
# ##################################################

class ProgressLogger ( ProgressTracker, Logger ):
	"""
	The class `ProgressLogger` reports the progression of lon processes in a log.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, header, report_every=0.01, end_line_every=0.1, indent_size=4, indent_text=' ', line_separator='\n', parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.TextProgressTracker` class.
		
		Arguments:
			self          (`pytools.tasks.ProgressLogger`): Instance to initialize.
			header                                 (`str`): Text logged before this progress logger starts logging progression.
			report_every                         (`float`): Progression delta between every reports.
			end_line_every                       (`float`): Progression delta after which line of the log are ended.
			indent_size                            (`int`): Width of the indentation to the left of the log.
			indent_text                            (`str`): Text used to indent the left of the log.
			line_separator                         (`str`): Text used to terminate lines of the log.
			parent (`None`/`pytools.tasks.ProgressLogger`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )
		assert pytools.assertions.type_is( header, str )
		assert pytools.assertions.type_is( report_every, float )
		assert pytools.assertions.type_is( end_line_every, float )
		assert pytools.assertions.type_is( indent_size, int )
		assert pytools.assertions.type_is( indent_text, str )
		assert pytools.assertions.type_is( line_separator, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), ProgressLogger) )

		ProgressTracker.__init__( self,
			  report_every = report_every,
			        parent = parent
			)
		Logger.__init__( self,
			   indent_size = indent_size,
			   indent_text = indent_text,
			line_separator = line_separator,
			        parent = parent
			)

		self.header         = header
		self.end_line_every = end_line_every

		self.log( self.header )
		
	# def __init__ ( self, start, end, report_every, end_line_every, indent_size, indent_text, line_separator, parent )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def end_line_every ( self ):
		"""
		End progression (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )

		return self._end_line_every

	# def end_line_every ( self )

	# --------------------------------------------------

	@end_line_every.setter
	def end_line_every ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )
		assert pytools.assertions.type_is( value, float )
		
		self._end_line_every = value

	# def end_line_every ( self, value )
	
	# --------------------------------------------------

	@property
	def header ( self ):
		"""
		Header text (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )

		return self._header

	# def header ( self )

	# --------------------------------------------------

	@header.setter
	def header ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )
		assert pytools.assertions.type_is( value, str )
		
		self._header = value

	# def header ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def report ( self ):
		"""
		Method called to report progression.

		Arguments:
			self (`pytools.tasks.ProgressTracker`): Progress tracker.
		"""
		assert pytools.assertions.type_is_instance_of( self, ProgressLogger )

		# Write the value of the progression
		self.write( '{:3.0f}%'.format(self.progress*100.0) )

		# Write a comma if not at 1.0
		if ( self.progress < 1.0 ):
			self.write( ', ' )
		
		# End the line ?
		mod = int(self.progress*100.0) % int(self.end_line_every*100.0)
		if ( mod == 0 ):
			self.end_line()
			self.indent()

		# Finish ?
		if ( int(self.progress*100.0) == 100 ):
			self.write( 'Done' )
			self.end_line()
			self.end_line()

		self.flush()

	# def report ( self )

# class ProgressLogger ( ProgressTracker )