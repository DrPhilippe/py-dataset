# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###            CLASS TASK-SETTINGS             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TaskSettings',
	   namespace = 'pytools.tasks',
	fields_names = [
		'name'
		]
	)
class TaskSettings ( pytools.serialization.Serializable ):
	"""
	Base class for tasks settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='task' ):
		"""
		Initializes a new instance of the `pytools.tasks.TaskSettings` class.

		Arguments:
			self (`pytools.tasks.TaskSettings`): Instance to initialize.
			name                        (`str`): Name of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, TaskSettings )
		assert pytools.assertions.type_is( name, str )

		super( TaskSettings, self ).__init__()

		self.name = name

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the task (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TaskSettings )

		return self._name

	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TaskSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
		
	# def name ( self, value )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this task settings.

		Arguments:
			self (`pytools.tasks.TaskSettings`): Task settings of which to create a text representation.

		Returns:
			`str`: The text representation of this task settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, TaskSettings )

		return "[{}: name='{}']".format(
			type(self).__name__,
			self.name
			)

	# def __str__ ( self )

# class TaskSettings ( pytools.serialization.Serializable )