# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import io

# INTERNAL
import pytools.assertions

# LOCALS
from .logger import Logger

# ##################################################
# ###            CLASS STREAM-LOGGER             ###
# ##################################################

class StreamLogger ( Logger ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, stream, indent_size=4, indent_text=' ', line_separator='\n', parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.StreamLogger` class.
		
		`stream`, `indent_size`, `indent_text` and `line_separator` are overriden by the parent's if a parent if provided.

		Arguments:
			self          (`pytools.tasks.StreamLogger`): Instance to initialize.
			stream                     (`io.TextIOBase`): Stream where to write the log.
			indent_size                          (`int`): Width of the indentation to the left of the log.
			indent_text                          (`str`): Text used to indent the left of the log.
			line_separator                       (`str`): Text used to terminate lines of the log.
			parent (`None`/`pytools.tasks.StreamLogger`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )
		assert pytools.assertions.type_is_instance_of( stream, io.TextIOBase )
		assert pytools.assertions.type_is( indent_size, int )
		assert pytools.assertions.type_is( indent_text, str )
		assert pytools.assertions.type_is( line_separator, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), StreamLogger) )

		super( StreamLogger, self ).__init__(
			   indent_size = indent_size,
			   indent_text = indent_text,
			line_separator = line_separator,
			        parent = parent
			)

		self.stream = stream if parent is None else parent.stream

	# def __init__ ( self, stream, indent_size, indent_text, line_separator, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------
	
	@property
	def stream ( self ):
		"""
		Stream where to write the log (`io.TextIOBase`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )

		return self._stream

	# def stream ( self )
	
	# --------------------------------------------------
	
	@stream.setter
	def stream ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )
		assert pytools.assertions.type_is_instance_of( value, io.TextIOBase )

		self._stream = value
		
	# def stream ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this log.

		Arguments:
			self (`pytools.tasks.StreamLogger`): Log to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )

		if self.parent is None:
			self.stream.close()

	# def close ( self )

	# --------------------------------------------------

	def flush ( self ):
		"""
		Flushes this log.

		Arguments:
			self (`pytools.tasks.StreamLogger`): Log to flush.

		Returns:
			`pytools.tasks.StreamLogger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )
		
		self.stream.flush()
		return self

	# def flush ( self )

	# --------------------------------------------------

	def write ( self, text ):
		"""
		Writes the given text in this log.

		Arguments:
			self (`pytools.tasks.StreamLogger`): Log in which to write the text.
			text                        (`str`): Text to write.

		Returns:
			`pytools.tasks.StreamLogger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )
		assert pytools.assertions.type_is( text, str )

		self.stream.write( text )
		return self

	# def write ( self, text )

# class StreamLogger ( Logger )