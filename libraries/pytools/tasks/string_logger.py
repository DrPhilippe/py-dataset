# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import io

# INTERNAL
import pytools.assertions

# LOCALS
from .stream_logger import StreamLogger

# ##################################################
# ###            CLASS STRING-LOGGER             ###
# ##################################################

class StringLogger ( StreamLogger ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, initial_text='', indent_size=4, indent_text=' ', line_separator='\n', parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.StringLogger` class.
		
		`stream`, `indent_size`, `indent_text` and `line_separator` are overriden by the parent's if a parent if provided.

		Arguments:
			self          (`pytools.tasks.StringLogger`): Instance to initialize.
			initial_text                         (`str`): Initial text in the string.
			indent_size                          (`int`): Width of the indentation to the left of the log.
			indent_text                          (`str`): Text used to indent the left of the log.
			line_separator                       (`str`): Text used to terminate lines of the log.
			parent (`None`/`pytools.tasks.StringLogger`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )
		assert pytools.assertions.type_is( initial_text, str )
		assert pytools.assertions.type_is( indent_size, int )
		assert pytools.assertions.type_is( indent_text, str )
		assert pytools.assertions.type_is( line_separator, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), StreamLogger) )

		super( StringLogger, self ).__init__(
			        stream = io.StringIO( initial_text, line_separator ) if parent is None else parent.stream,
			   indent_size = indent_size,
			   indent_text = indent_text,
			line_separator = line_separator,
			        parent = parent
			)

	# def __init__ ( self, initial_text, indent_size, indent_text, line_separator, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_text ( self ):
		"""
		Returns the text of this log.

		Arguments:
			self (`pytools.tasks.StringLogger`): Log of which to get the text.

		Returns:
			`str`: The text
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamLogger )

		if self.parent is None:
			self.stream.close()

	# def get_text ( self )

# class StringLogger ( StreamLogger )