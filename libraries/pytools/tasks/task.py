# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import threading
import time
import traceback

# INTERNALS
import pytools.assertions

# LOCALS
from .logger           import Logger
from .string_logger    import StringLogger
from .progress_tracker import ProgressTracker
from .task_settings    import TaskSettings
from .task_state       import TaskState

# ##################################################
# ###                 CLASS TASK                 ###
# ##################################################

class Task:
	"""
	Base class for task with the ability to log their activity and report their progression.
	"""

	# ##################################################
	# ###                CLASS FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Root task (`pytools.tasks.Task`).
	"""
	root = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, logger=None, progress_tracker=None, parent=None ):
		"""
		Initializes a new task instance.

		Arguments:
			self                               (`pytools.tasks.Task`): Task to initialize.
			settings                   (`pytools.tasks.TaskSettings`): Settings of the task.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			parent                      (`None`/`pytools.tasks.Task`): Parent task.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )
		assert pytools.assertions.type_is_instance_of( settings, TaskSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), ProgressTracker) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Task) )
		
		self._logger           = StringLogger() if logger is None else logger
		self._progress_tracker = ProgressTracker() if progress_tracker is None else progress_tracker 
		self._settings         = settings
		self._error            = None
		self._state_lock       = threading.Lock()
		self._state            = TaskState.idle
		self._start_time       = 0
		self._current_time     = 0
		self._parent           = Task.root if parent is None else parent
		self._children         = []

		if self.parent:
			self.parent.children.append( self )

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def children ( self ):
		"""
		Children of this task (`list` of `pytools.tasks.Task`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._children

	# def children ( self )

	# --------------------------------------------------

	@property
	def current_time ( self ):
		"""
		Latest time since epoch at which the task reported (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._current_time
	
	# def current_time ( self )

	# --------------------------------------------------

	@property
	def delta_time ( self ):
		"""
		Time delta since the task started (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self.current_time - self.start_time
	
	# def delta_time ( self )

	# --------------------------------------------------

	@property
	def error ( self ):
		"""
		Error encountered while running this task (`None`/`Exception`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._error

	# def error ( self )

	# --------------------------------------------------

	@property
	def logger ( self ):
		"""
		Logger used to log the activity of the task (`None`/`pytools.tasks.Logger`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._logger

	# def logger ( self )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent task (`pytools.tasks.Task`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._parent

	# def parent ( self )

	# --------------------------------------------------

	@property
	def progress_tracker ( self ):
		"""
		Progress tracker used to report the progression of the task (`pydataset.tasks.ProgressTracker`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._progress_tracker

	# def progress_tracker ( self )

	# --------------------------------------------------

	@property
	def settings ( self ):
		"""
		Settings of this task (`pydataset.tasks.TaskSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._settings

	# def settings ( self )
	
	# --------------------------------------------------

	@settings.setter
	def settings ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Task )
		assert pytools.assertions.type_is_instance_of( value, TaskSettings )

		self._settings = value

	# def settings ( self, value )

	# --------------------------------------------------

	@property
	def start_time ( self ):
		"""
		Time since epoch at which the task started (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self._start_time
	
	# def start_time ( self )

	# --------------------------------------------------

	@property
	def state ( self ):
		"""
		State the task is in (`TaskState`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self._state_lock.acquire()
		state = copy.deepcopy( self._state )
		self._state_lock.release()
		return state

	# def state ( self )
	
	# --------------------------------------------------

	@state.setter
	def state ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Task )
		assert pytools.assertions.type_is_instance_of( value, TaskState )

		self._state_lock.acquire()
		self._state = copy.deepcopy( value )
		self._state_lock.release()

	# def state ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def execute ( self ):
		"""
		Starts this task and runs it.

		Arguments:
			self (`pytools.tasks.Task`): The task to start and run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		try:
			self.state = TaskState.starting
			self.start()
			self.state = TaskState.running
			self.run()
			self.state = TaskState.stopped
	
		except Exception as error:
			self.fail( error )
		
		# try

	# def execute ( self )

	# --------------------------------------------------

	def fail ( self, error ):
		"""
		Set the appropriate state and error when the task fails.

		Arguments:
			self (`pytools.tasks.Task`): The task to fail.
			error         (`Exception`): The error
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )
		assert pytools.assertions.type_is_instance_of( error, Exception )

		self._error = error
		self.state  = TaskState.failed		

		if ( self.logger ):

			self.logger.end_line()
			self.logger.log( 'Task "{}" failed:', self.settings.name )
			self.logger.log( '    {}', str(traceback.format_exc()) )
			self.logger.end_line()
			self.logger.flush()

		# if ( self.logger )
	
	# def fail ( self, error )

	# --------------------------------------------------

	def is_failed ( self ):
		"""
		Checks if this task is failed.

		Arguments:
			self (`pytools.tasks.Task`): Task of which to check the state.

		Returns:
			`bool`: `True` if the task is failed, `False` otherwise.
		"""
		return self.state == TaskState.failed

	# def is_failed ( self )

	# --------------------------------------------------

	def is_paused ( self ):
		"""
		Checks if this task is paused.

		Arguments:
			self (`pytools.tasks.Task`): Task of which to check the state.

		Returns:
			`bool`: `True` if the task is paused, `False` otherwise.
		"""
		return self.state == TaskState.paused

	# def is_paused ( self )

	# --------------------------------------------------

	def is_pausing ( self ):
		"""
		Checks if this task is pausing.

		Arguments:
			self (`pytools.tasks.Task`): Task of which to check the state.

		Returns:
			`bool`: `True` if the task is pausing, `False` otherwise.
		"""
		return self.state == TaskState.pausing

	# def is_pausing ( self )

	# --------------------------------------------------

	def is_resuming ( self ):
		"""
		Checks if this task is resuming.

		Arguments:
			self (`pytools.tasks.Task`): Task of which to check the state.

		Returns:
			`bool`: `True` if the task is resuming, `False` otherwise.
		"""
		return self.state == TaskState.resuming

	# def is_resuming ( self )

	# --------------------------------------------------

	def is_running ( self ):
		"""
		Checks if this task is running.

		Arguments:
			self (`pytools.tasks.Task`): Task of which to check the state.

		Returns:
			`bool`: `True` if the task is running, `False` otherwise.
		"""
		return self.state in [
			TaskState.running,
			TaskState.pausing,
			TaskState.paused,
			TaskState.resuming
			]
	# def is_running ( self )

	# --------------------------------------------------

	def manage ( self, tasks, sleep_time=0.1, max_threads=-1 ):
		"""
		Run the given tasks up until completion or failure.

		Arguments:
			self           (`pytools.tasks.Task`): The parent task.
			tasks (`list` of pytools.tasks.Task`): Children tasks to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )
		assert pytools.assertions.type_is( tasks, list )
		assert pytools.assertions.list_items_type_is( tasks, Task )
		assert pytools.assertions.type_is( sleep_time, float )

		if not tasks:
			return

		from .manager import Manager

		queue = list( tasks )
		while len(queue) > 0:

			batch = []

			if max_threads < 0:
				batch = list( queue )
				queue.clear()
			else:
				count = 0
				while count < min( max_threads, len(queue) ):
					batch.append( queue.pop(0) )
					count += 1

			manager = Manager( *batch )
			manager.start_tasks()
			
			# Main loop
			continue_managing = True
			while ( continue_managing ):			
				
				continue_managing = False
				
				# Check if this task failed or was stopped
				if not self.is_running():
					# Stop children
					for task in manager.tasks:
						if not task.stopped():
							task.stop()
					# Join threads
					manager.join_tasks()
					# Check if any children failed
					for task in manager.tasks:
						if task.is_failed():
							# Raise the tasks error in this tread
							raise task.error
					# Exit managing loop
					break

				# Update the task status
				self.update()
				
				# Check if all tasks finished
				for task in manager.tasks:
					# If one did not finish
					if not task.stopped():
						# Continue waiting
						continue_managing = True
					# Check if a task failed
					if task.is_failed():
						# Stop all the tasks
						for task in manager.tasks:
							if not task.stopped():
								task.stop()
						# Join threads
						manager.join_tasks()
						# Raise the tasks error in this tread
						raise task.error
				# Sleep
				time.sleep( sleep_time )

			# while ( continue_managing )

			manager.join_tasks()

			# Check for error again
			for task in manager.tasks:
				if task.is_failed():
					raise task.error

		# while queue

	# def manage ( self, tasks )

	# --------------------------------------------------

	def pause ( self ):
		"""
		Pauses this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to pause.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self.state = TaskState.pausing

	# def pause ( self )

	# --------------------------------------------------

	def resume ( self ):
		"""
		Resumes this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to resume.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self.state = TaskState.resuming

	# def resume ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		raise NotImplementedError()
		
	# def run ( self )

	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self._start_time   = time.time()
		self._current_time = self._start_time

	# def start ( self )

	# --------------------------------------------------

	def started ( self ):
		"""
		Checks if this task started.

		Arguments:
			self (`pytools.tasks.Task`): The task of which to check the state.

		Returns:
			`bool`: `True` if the task is started, `False` otherwise.
		"""
		return self.state not in [TaskState.idle, TaskState.starting]

	# def started ( self )

	# --------------------------------------------------

	def stop ( self ):
		"""
		Stops this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to stop.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self.state = TaskState.stopping

	# def stop ( self )
	
	# --------------------------------------------------

	def stopped ( self ):
		"""
		Checks if this task stopped.

		Arguments:
			self (`pytools.tasks.Task`): The task of which to check the state.

		Returns:
			`bool`: `True` if the task is stopped, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		return self.state in [TaskState.failed, TaskState.idle, TaskState.stopped]

	# def stopped ( self )
	
	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, Task )
		
		# set the current time
		self._current_time = time.time()

		# If the camera must pause
		if self.is_pausing():
			self.state = pytools.tasks.TaskState.paused

		# If the camera is pause yield activity
		while self.is_paused():
			self._current_time = time.time()
			time.splee( 0.1 )

		# If resume start running again
		if self.is_resuming():
			self.status = pytools.tasks.TaskState.running

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __call__ ( self ):
		"""
		Executes this task.

		Arguments:
			self (`pytools.tasks.Task`): The task to execute.
		"""
		assert pytools.assertions.type_is_instance_of( self, Task )

		self.execute()

	# def __call__ ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, logger=None, progress_tracker=None, **kwargs ):
		"""
		Creates a new instance of the task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of task.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			**kwargs                                         (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, logger=logger, progress_tracker=progress_tracker )

	# def create ( cls, logger, progress_tracker, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a task.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.tasks.TaskSettings`: The settings.
		"""
		return TaskSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

Task.root = Task.create( name='root' )

# class Task