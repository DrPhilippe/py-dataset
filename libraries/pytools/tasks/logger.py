# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools.assertions

# ##################################################
# ###               CLASS A-LOGGER               ###
# ##################################################

class Logger:
	"""
	The class `Logger` is the base class of loggers used to log the activity long processes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __init__ ( self, indent_size=4, indent_text=' ', line_separator='\n', parent=None ):
		"""
		Initializes a new instance of the `pytools.tasks.Logger` class.
		
		`indent_size`, `indent_text` and `line_separator` are overriden by the parent's if a parent if provided.

		Arguments:
			self          (`pytools.tasks.Logger`): Instance to initialize.
			indent_size                    (`int`): Width of the indentation to the left of the log.
			indent_text                    (`str`): Text used to indent the left of the log.
			line_separator                 (`str`): Text used to terminate lines of the log.
			parent (`None`/`pytools.tasks.Logger`): Parent of this logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( indent_size, int )
		assert pytools.assertions.type_is( indent_text, str )
		assert pytools.assertions.type_is( line_separator, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Logger) )

		self.indent_size    = indent_size    if parent is None else parent.indent_size
		self.indent_text    = indent_text    if parent is None else parent.indent_text
		self.line_separator = line_separator if parent is None else parent.line_separator
		self.parent         = parent

	# def __init__ ( self, indent_size, indent_text, line_separator, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def indent_size ( self ):
		"""
		Width of the indentation to the left of the log (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		return self._indent_size

	# def indent_size ( self )
	
	# --------------------------------------------------

	@indent_size.setter
	def indent_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( value, int )

		self._indent_size = value

	# def indent_size ( self, value )

	# --------------------------------------------------

	@property
	def indent_text ( self ):
		"""
		Text used to indent the left of the log (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		return self._indent_text

	# def indent_text ( self )
	
	# --------------------------------------------------

	@indent_text.setter
	def indent_text ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( value, str )

		self._indent_text = value

	# def indent_text ( self, value )

	# --------------------------------------------------

	@property
	def line_separator ( self ):
		"""
		Text used to terminate lines of the log (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		return self._line_separator

	# def line_separator ( self )
	
	# --------------------------------------------------

	@line_separator.setter
	def line_separator ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( value, str )

		self._line_separator = value

	# def line_separator ( self, value )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent of this logger (`None`/`pytools.tasks.Logger`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		return self._parent

	# def parent ( self )
	
	# --------------------------------------------------

	@parent.setter
	def parent ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Logger) )

		self._parent = value

	# def parent ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_child ( self, **kwargs ):
		"""
		Creates and returns a child logger of the same type as this log.
		
		Arguments:
			self (`pytools.tasks.Logger`): Parent log.

		Returns:
			`pytools.tasks.Logger`: The new child logger.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		return type(self)( parent=self, **kwargs )

	# def create_child ( self, **kwargs )

	# --------------------------------------------------

	def end_line ( self ):
		"""
		Terminates the current lineof this log.

		Arguments:
			self (`pytools.tasks.Logger`): Log of which to end the current line.

		Returns:
			`pytools.tasks.Logger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )
		
		return self.write( self.line_separator )

	# def end_line ( self )
	
	# --------------------------------------------------

	def flush ( self ):
		"""
		Flushes this log.

		Arguments:
			self (`pytools.tasks.Logger`): Log to flush.

		Returns:
			`pytools.tasks.Logger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )
		
		raise NotImplementedError()

	# def flush ( self )

	# --------------------------------------------------

	def indent ( self ):
		"""
		Indents this log.
		
		Arguments:
			self (`pytools.tasks.Logger`): Log to indent.

		Returns:
			`pytools.tasks.Logger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )

		if self.parent is None:
			return self
		else:
			self.parent.indent()
			for i in range( self.indent_size ):
				self.write( self.indent_text )

		return self.flush()

	# def indent ( self )

	# --------------------------------------------------

	def log ( self, message_format, *args ):
		"""
		Logs the given text in this log, ends the line and reindent this log.

		Arguments:
			self (`pytools.tasks.Logger`): Log in which to log the text.
			message_format        (`str`): Format string of the text to log.
			args       (`tuple` of `any`): Arguments formated to the `string.format` method.

		Returns:
			`pytools.tasks.Logger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( message_format, str )
		assert pytools.assertions.type_is( args, tuple )

		if message_format.strip( ' \r\n' ) == '':
			return

		return self.indent().write( message_format.format(*args) ).end_line()

	# def log ( self, message_format, *args )

	# --------------------------------------------------

	def write ( self, text ):
		"""
		Writes the given text in this log.

		Arguments:
			self (`pytools.tasks.Logger`): Log in which to write the text.
			text                  (`str`): Text to write.

		Returns:
			`pytools.tasks.Logger`: This log.
		"""
		assert pytools.assertions.type_is_instance_of( self, Logger )
		assert pytools.assertions.type_is( text, str )

		raise NotImplementedError()

	# def write ( self, text )

# class Logger