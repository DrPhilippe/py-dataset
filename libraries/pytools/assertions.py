# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def dictionary_keys_type_is ( value, expected_type ):
	"""
	Checks if the keys of the given dictionay are all of the expected type.

	Arguments:
		value         (`dict`): Dictionary instance.
		expected_type (`type`): Expected type for the keys.

	Returns:
		`bool`: `True` if all the keys have the expected type, `False` otherwise.
	"""
	if not value:
		return True

	for key in value.keys():

		if not isinstance( key, expected_type ):
		
			return False
		
		# if not isinstance( key, expected_type )

	# for key in value.keys()

	return True

# def dictionary_keys_type_is ( value, expected_type )

# --------------------------------------------------

def dictionary_values_type_is ( value, expected_type ):
	"""
	Checks if the values of the given dictionay are all of the expected type.

	Arguments:
		value         (`dict`): Dictionary instance.
		expected_type (`type`): Expected type for the values.

	Returns:
		`bool`: `True` if all the values have the expected type, `False` otherwise.
	"""
	if not value:
		return True

	for value in value.values():

		if not isinstance( value, expected_type ):
		
			return False
		
		# if not isinstance( value, expected_type )

	# for value in value.values()

	return True

# def dictionary_values_type_is ( value, expected_type )

# --------------------------------------------------

def dictionary_types_are ( value, expected_key_type, expected_value_type ):
	"""
	Checks if the keys and values of the given dictionay are all of the expected types.

	Arguments:
		value               (`dict`): Dictionary instance.
		expected_key_type   (`type`): Expected type for the keys.
		expected_value_type (`type`): Expected type for the values.

	Returns:
		`bool`: `True` if all the keys and values have the expected type, `False` otherwise.
	"""
	if not value:
		return True

	for key, value in value.items():

		if not isinstance( key, expected_key_type ):
		
			return False
		
		# if ( key_type != expected_key_type )

		if not isinstance( value, expected_value_type ):
		
			return False
		
		# if ( key_type != expected_value_type )

	# for key, value in value

	return True

# def dictionary_types_are ( value, expected_key_type, expected_value_type )

# --------------------------------------------------

def equal ( value, other ):
	"""
	Checks if `value` is equal to `other`.

	Arguments:
		value (`any`): Value of the variable.
		other (`any`): Reference value.

	Returns:
		`bool`: `True` if the value are equal, `False` otherwise.
	"""

	return value == other

# def equal ( value, other )

# --------------------------------------------------

def list_items_type_is ( value, expected_type ):
	"""
	Checks if the items of the `list` have the `expected_type`.
	
	Arguments:
		value         (`list`): Value of the list.
		expected_type (`type`): Expected type for the items of the list.
		
	Returns:
		`bool`: `True` if all the items of the list have the expected type, `False` otherwise.
	"""

	for item in value:

		if not isinstance( item, expected_type ):

			return False

		# if not isinstance( item, expected_type )
	
	# for item in value

	return True

# def list_items_type_is ( value, expected_type )

# --------------------------------------------------

def list_not_empy ( value ):
	"""
	Checks if the given list is not empty.

	Arguments:
		value  (`list`/`None`): The list to check.

	Returns:
		`bool`: `True` if the list is not empty, `False` otherwise.
	"""

	return len(value) != 0

# def list_not_empy ( value )

# --------------------------------------------------

def not_equal ( value, other ):
	"""
	Checks if `value` is not equal to `other`.

	Arguments:
		value (`any`): Value of the variable.
		other (`any`): Reference value.

	Returns:
		`bool`: `True` if the value are not equal, `False` otherwise.
	"""

	return value != other

# def not_equal ( value, other )

# --------------------------------------------------

def not_none ( value ):
	"""
	Checks that the given value is not `None`.

	Arguments:
		value (`any`): The value to check.

	Returns:
		`bool`: `True` if the value is not `None`, `False` otherwise.
	"""

	return value is not None
	
# def not_none ( value )

# --------------------------------------------------

def true ( condition ):
	"""
	Checks if the given condition is true.

	This is just to make code look fancy.

	Arguments:
		condition (`bool`): Condition

	Returns:
		`bool`: condition
	"""
	return condition

# def true ( condition )

# --------------------------------------------------

def tuple_items_type_is ( value, expected_type ):
	"""
	Checks if the items of the given tuple are all of the expected type.

	Arguments:
		value (`tuple`): Tuple to check.
		expected_type (`type`): Expected type.

	Returns:
		`bool`: `True` if all the items of the tuple have the expected type, `False` otherwise.
	"""

	for item in value:

		if not isinstance( item, expected_type ):

			return False

		# if not isinstance( item, expected_type )
	
	# for item in value

	return True

# def tuple_items_type_is ( value, expected_type )

# --------------------------------------------------

def tuple_not_empy ( value ):
	"""
	Checks if the given tuple is not empty.

	Arguments:
		value  (`tuple`/`None`): The tuple to check.

	Returns:
		`bool`: `True` if the tuple is not empty, `False` otherwise.
	"""

	return len(value) != 0

# def tuple_not_empy ( value )

# --------------------------------------------------

def type_in ( value, expected_types ):
	"""
	Check if the value has one of the expected types.

	Arguments:
		value            (`any`): The value of which to check the type.
		expected_types (`tuple`): Tuple of expected types for the value.

	Returns:
		`bool`: `True` if the value has one of the expected types, `False` otherwise.
	"""

	return isinstance( value, expected_types )
	
# def type_in ( value, expected_types )

# --------------------------------------------------

def type_is ( value, expected_type ):
	"""
	Check if the value has the expected type.

	Arguments:
		value          (`any`): The value of which to check the type.
		expected_type (`type`): Expected type for the value.

	Returns:
		`bool`: `True` if the value has the expected types, `False` otherwise.
	"""

	return type(value) == expected_type
	
# def type_is ( value, expected_type )

# --------------------------------------------------

def type_is_instance_of ( value, expected_types ):
	"""
	Check if the value is an instance the expected type(s).

	Arguments:
		value                               (`any`): The value of which to check the type.
		expected_type (`type` or `tuple` of `type`): Expected type(s) for the value.

	Returns:
		`bool`: `True` if the value is an instance the expected type(s), `False` otherwise.
	"""

	return isinstance(value, expected_types)
	
# def type_is_instance_of ( value, expected_types )

# --------------------------------------------------

def value_in ( value, expected_values ):
	"""
	Checks if the given value has one of the expected values.

	Arguments:
		value           (`any`): The value to check.
		expected_values (`any`): The set of possible value.

	Returns:
		`bool`: `True` if the value has one of the expected values, `False` otherwise.
	"""

	return value in expected_values

# def value_in ( value, expected_values )

# --------------------------------------------------

def value_in_range ( value, lower, upper ):
	"""
	Checks if the given value has one of the expected values.

	Arguments:
		value (`any`): The value to check.
		lower (`any`): The lower bound of the range.
		upper (`any`): The upper bound of the range.

	Returns:
		`bool`: `True` if the value is in the range [`lower`, `upper`]
	"""

	return lower <= value and value <= upper 

# def value_in_range ( value, lower, upper )