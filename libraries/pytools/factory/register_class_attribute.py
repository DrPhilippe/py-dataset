# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools.assertions

# LOCAL
from .class_factory import ClassFactory

# ##################################################
# ###       CLASS REGISTER-CLASS-ATTRIBUTE       ###
# ##################################################

class RegisterClassAttribute:
	"""
	Attribute used on class definitions to register them in the `pytools.factory.ClassFactory`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, group, typename ):
		"""
		Initialize a new instance of the `pytools.factory.RegisterClassAttribute` class.

		Arguments:
			self (`pytools.factory.RegisterClassAttribute`): Instance to initialize.
			group                            (`str`/`None`): Name of the group in which to register the class.
			typename                         (`str`/`None`): Name of the type used to register the type.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )
		assert pytools.assertions.type_in( group, (type(None), str) )
		assert pytools.assertions.type_in( typename, (type(None), str) )

		self.group    = group
		self.typename = typename

	# def __init__ ( self, group, typename )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def group ( self ):
		"""
		The name of the group in which to register the type (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )

		return self._group
	
	# def group ( self )
	
	# --------------------------------------------------

	@group.setter
	def group ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )
		assert pytools.assertions.type_in( value, (type(None), str) )

		self._group = value
	
	# def group ( self, value )
	
	# --------------------------------------------------

	@property
	def typename ( self ):
		"""
		The name of the type used to register the type (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )
		
		return self._typename
	
	# def typename ( self )
	
	# --------------------------------------------------

	@typename.setter
	def typename ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )
		assert pytools.assertions.type_in( value, (type(None), str) )

		self._typename = value
	
	# def typename ( self, value )

	# ##################################################
	# ###                  OPERATOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __call__ ( self, value ):
		"""
		Call operator that register the type assicated to this `pytools.factory.RegisterClassAttribute`.

		Arguments:
			self (`pytools.factory.RegisterClassAttribute`): `RegisterClassAttribute` defining group and typename under which to register the type.
			value                                  (`type`): The type to register.

		Returns:
			`type`: The type once registered.

		Raises:
			`RegistrationError` if an other type uses the same group and typename.

		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterClassAttribute )

		# Default value for the name of the group
		group_name = self.group
		if ( not group_name ):
			group_name = 'All'

		# Default value for the name of the type
		typename = self.typename
		if ( not typename ):
			typename = type(value).__name__

		# Register the type
		ClassFactory.register(
			group_name = group_name,
			  typename = typename,
			     value = value
			)

		# Return the type
		return value

	# def __call__ ( self, value )

# class RegisterClassAttribute