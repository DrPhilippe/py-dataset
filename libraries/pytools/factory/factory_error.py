# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools
import pytools.assertions

# ##################################################
# ###            CLASS FACTORY-ERROR             ###
# ##################################################

class FactoryError ( pytools.Error ):
	"""
	Custom `Exception` raise by the `ClassFactory` class.
	"""	

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, message='' ):
		"""
		Initializes a new instance of the `FactoryError` class with the given message.

		Arguments:
			self (`FactoryError`): Instance to initialize.
			message       (`str`): Error message.
		"""
		assert pytools.assertions.type_is_instance_of( self, FactoryError )
		assert pytools.assertions.type_is( message, str )
		
		super( FactoryError, self ).__init__( message )

	# def __init__ ( self, message )

# class FactoryError ( errors.Error )