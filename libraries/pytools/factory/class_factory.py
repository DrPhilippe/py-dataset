# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools.assertions

# LOCAL
from .factory_error      import FactoryError
from .registration_error import RegistrationError

# ##################################################
# ###             CLASS CLASS-FACTORY            ###
# ##################################################

class ClassFactory:
	"""
	Static class used to register and instantiate serializable types.
	"""

	# ##################################################
	# ###               STATIC-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	The groups of registered types (`dict`).
	Each key is a group name.
	Each value us a group where the key is the name of the associated type.
	"""
	_groups = {}
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def get_group ( cls, group_name ):
		"""
		Get the group with the given `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group to get.

		Returns:
			`dict`: The group where each `str` key is mapped to a registered `type`.

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )

		# Check if the group does not exists
		if cls.group_does_not_exist( group_name ):

			# Raise an error
			raise FactoryError.format(
				"The group '{}' does not exists",
				group_name
				)
		
		# if GroupDoesNotExists( group_name )

		# Return the group
		return cls._groups[ group_name ]

	# def GetGroup ( cls, group_name )

	# --------------------------------------------------

	@classmethod
	def get_group_names ( cls ):
		"""
		Returns the names of the groups in a new `list`.

		Arguments:
			cls (`type`): The `ClassFactory` class.

		Returns:
			`list`: The names of the groups.
		"""
		assert pytools.assertions.type_is( cls, type )

		# Return all the typenames in the group
		return list( cls._groups.keys() )

	# def GetGroupsNames ( cls )

	# --------------------------------------------------

	@classmethod
	def get_or_create_group ( cls, group_name ):
		"""
		Get the group with the given `group_name`.
		If the group does not exists yet, it is first created.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group to get.

		Returns:
			`dict`: The group where each `str` key is mapped to a registered `type`.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )

		# Check if the group does not exists
		if cls.group_does_not_exist( group_name ):

			# Create it
			cls._groups[ group_name ] = {}
		
		# if cls.group_does_not_exist( group_name )

		# Return the group
		return cls._groups[ group_name ]

	# def get_or_create_group ( cls, group_name )
	
	# --------------------------------------------------

	@classmethod
	def get_type ( cls, group_name, typename ):
		"""
		Gets the type with the given `typename` in the groupe named `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.

		Returns:
			`type`: The type.

		Raises:
			`FactoryError`: if the group does not exists.
			`FactoryError`: if the typename is not registered in the group.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		# Check if the group exists and the typename is registered
		if cls.typename_is_not_used( group_name, typename ):

			raise FactoryError.format(
				"The typename '{}' in group '{}' is not registered",
				typename,
				group_name
				)
		
		# if cls.TypenameIsNotUsed( group_name, typename ):

		# Get the type
		return cls.get_group( group_name )[ typename ]

	# def get_type ( cls, group_name, typename )
	
	# --------------------------------------------------

	@classmethod
	def get_typenames_of_group ( cls, group_name ):
		"""
		Get the names of the types registered in the group with the given `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group of which to get the typenames.

		Returns:
			`list`: The list of typenames

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )

		# Return all the typenames in the group
		return list( cls.get_group( group_name ).keys() )

	# def get_typenames_of_group ( cls, group_name )
	
	# --------------------------------------------------

	@classmethod
	def group_does_not_exist ( cls, group_name ):
		"""
		Checks if the group named `group_name` exits.
		
		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			
		Returns:
			`bool`: `True` if the group exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )

		return group_name not in cls._groups

	# def group_does_not_exist ( cls, group )

	# --------------------------------------------------

	@classmethod
	def group_exist ( cls, group_name ):
		"""
		Checks if the group named `group_name` exits.
		
		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.

		Returns:
			`bool`: `True` if the group exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )

		return group_name in cls._groups

	# def group_exist ( cls, group_name )
	
	# --------------------------------------------------

	@classmethod
	def instantiate ( cls, group_name, typename, **kwargs ):
		"""
		Instantiates the type with the given `typename` in the groupe named `group_name`.

		Arguments:
			      cls (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.
			kwargs    (`dict`): arguments passed to the constructor of the type.

		Returns:
			`type`: The type instance.

		Raises:
			`FactoryError`: if the group does not exists.
			`FactoryError`: if the typename is not registered in the group.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		return cls.get_type( group_name, typename )( **kwargs )

	# def instantiate ( cls, group, typename )

	# --------------------------------------------------

	@classmethod
	def register ( cls, group_name, typename, value ):
		"""
		Registers the given type under the given `typename` in the group `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.
			value     (`type`): The type.

		Raises:
			`RegistrationError`: if the group already contains a type registered under the same name.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		# Check if the key is already used for and other type
		if ( cls.typename_is_used( group_name, typename ) and cls.get_type( group_name, typename ).__name__ != value.__name__ ):
			raise RegistrationError.format(
				"Cannot register type '{}' under the typename '{}' in group '{}' because they are already used by an other type: '{}'",
				value,
				typename,
				group_name,
				cls.get_type( group_name, typename )
				)

		# if ( cls.TypenameIsUsed( group, typename ) and cls.GetType( group_name, typename ) != value )

		# Register the type
		cls.get_or_create_group( group_name )[ typename ] = value

	# def register ( cls, group_name, typename, value )

	# --------------------------------------------------

	@classmethod
	def typename_is_not_used ( cls, group_name, typename ):
		"""
		Check if the group with the given `group_name` does not contains a type with the given `typename`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.

		Returns:
			`bool`: True if the typename is not used in the group, False otherwise

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		# Check
		return cls.group_does_not_exist( group_name ) or typename not in cls.get_group( group_name )

	# def typename_is_not_used ( cls, group_name, typename )
	
	# --------------------------------------------------

	@classmethod
	def typename_is_used ( cls, group_name, typename ):
		"""
		Check if the group with the given `group_name` contains a type with the given `typename`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.

		Returns:
			`bool`: True if the typename is used in the group, False otherwise

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		# Check
		return cls.group_exist( group_name ) and typename in cls.get_group( group_name )

	# def typename_is_used ( cls, group_name, typename )
	
	# --------------------------------------------------

	@classmethod
	def type_is_not_registered ( cls, group_name, typename, value ):
		"""
		Checks if the type `value` with the given `typename` is not registered in the group named `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.
			value     (`type`): The type.
		
		Returns:
			`bool`: `True` if the type is not registered under the given typename in the group named `group_name`.

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )

		# Check
		return cls.typename_is_not_used( group_name, typename ) or cls.get_type( group_name, typename ) != value

	# def type_is_not_registered ( cls, group_name, typename, value )
	
	# --------------------------------------------------

	@classmethod
	def type_is_registered ( cls, group_name, typename, value ):
		"""
		Checks if the type `value` with the given `typename` is registered in the group named `group_name`.

		Arguments:
			cls       (`type`): The `ClassFactory` class.
			group_name (`str`): The name of the group.
			typename   (`str`): The name of the type.
			value     (`type`): The type.
		
		Returns:
			`bool`: `True` if the type is registered under the given typename in the group named `group_name`.

		Raises:
			`FactoryError`: if the group does not exists.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( typename, str )
		
		# Check
		return cls.typename_is_used( group_name, typename ) and cls.get_type( group_name, typename ) == value

	# def type_is_registered ( cls, group_name, typename, value )

# class ClassFactory