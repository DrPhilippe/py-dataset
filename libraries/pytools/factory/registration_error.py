# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools
import pytools.assertions

# ##################################################
# ###          CLASS REGISTRATION-ERROR          ###
# ##################################################

class RegistrationError ( pytools.Error ):
	"""
	Custom `Exception` raise when registration of a serializable fails.
	"""	

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, message='' ):
		"""
		Initializes a new instance of the `pytools.serialization.RegistrationError` class.

		Arguments:
			self (`pytools.serialization.RegistrationError`): Instance to initialize.
			message                                  (`str`): Error message.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegistrationError )
		assert pytools.assertions.type_is( message, str )
		
		super( RegistrationError, self ).__init__( message )

	# def __init__ ( self, message )

# class RegistrationError ( pytools.Error )