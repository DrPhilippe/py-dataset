# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def list_to_str ( l ):
	"""
	Converts a list of strings to a single comma-separated string.

	Argumens:
		l (`list` of `str`): List to convert

	Returns:
		`str`: Single comma-separated string.
	"""
	assert pytools.assertions.type_is( l, list )
	assert pytools.assertions.list_items_type_is( l, str )

	if len(l) == 1:
		text = '[' + str( l[ 0 ] ) + ']'
	elif len(l) > 1:
		text = '[' + ','.join( l ) + ']'
	else:
		text = '[]'

	return text
	
# def list_to_str ( l )


# --------------------------------------------------

def pretty_name ( name ):
	assert pytools.assertions.type_is( name, str )

	return name.replace( '_', ' ' ).title()

# def pretty_name ( name )