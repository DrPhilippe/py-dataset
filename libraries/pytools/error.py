# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNAL
import pytools.assertions

# ##################################################
# ###                CLASS ERROR                 ###
# ##################################################

class Error ( Exception ):
	"""
	Base class for custom error types.
	"""	

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, message='' ):
		"""
		Initializes a new instance of the `pytools.errors.Error` class.

		Arguments:
			self (`pytools.errors.Error`): Instance to initialize.
			message               (`str`): Error message.
		"""
		assert pytools.assertions.type_is_instance_of( self, Error )
		assert pytools.assertions.type_is( message, str )
		
		super( Error, self ).__init__( message )

	# def __init__ ( self, message )

	# ##################################################
	# ###             STATIC-CONSTRUCTOR             ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def format ( cls, message, *args ):
		"""
		Creates new instance of the `cls` class with a given formated `message`.

		Arguments:
			cls    (`type`): Type of the error to create.
			message (`str`): Error message format.
			args   (`list`): Elements to format in the message.

		Returns:
			`Error`: The new error.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( message, str )
		assert pytools.assertions.type_is( args, tuple )

		return cls( message.format(*args) )

	# def format ( message, *args )

# class Error ( Exception )