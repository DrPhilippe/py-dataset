# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy
import matplotlib.patches
import matplotlib.pyplot
import random

# INTERNALS
import pytools.assertions

# LOCALS
from .     import units
from .data import CharucoDictionaryData, CharucoBoardData

# ##################################################
# ###            DETECTION FUNCTIONS             ###
# ##################################################

# --------------------------------------------------

def detect_markers ( charuco_data, image ):
	"""
	Detects the markers of a given charuco board on an image.

	Arguments:
		charuco_data (`pydataset.data.CharucoBoardData`): Charuco configuration.
		image                          (`numpy.ndarray`): image on which to detect the markers.

	Returns:
		markers_corners (`numpy.ndarray`/`None`): The marker corners in pixels coordinates.
		markers_ids     (`numpy.ndarray`/`None`): The marker ids.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.type_is_instance_of( charuco_data, CharucoBoardData )

	# Fetch marker dictionary
	cv2_dictionary = charuco_data.get_cv2_board().dictionary
	
	# Run marker detection
	markers_corners, markers_ids, rejected = cv2.aruco.detectMarkers( image, cv2_dictionary )

	# If markers are detected
	if len( markers_corners ) > 0:

		# Convert to numpy ndarray
		markers_corners = numpy.asarray( markers_corners, dtype=numpy.float32 )
		markers_ids     = numpy.asarray( markers_ids,     dtype=numpy.int32   )
		markers_corners = numpy.reshape( markers_corners, [-1, 4, 2]          )
		markers_ids     = numpy.reshape( markers_ids,     [-1]                )
	
	# If no markers are detected
	else:
		markers_corners = None
		markers_ids     = None

	# Return
	return markers_corners, markers_ids

# def detect_markers ( charuco_data, image )

# --------------------------------------------------

def interpolate_corners ( charuco_data, image, markers_corners, markers_ids, cam_K=None, cam_dist_coeffs=None ):
	"""
	Interpolates the positions of the charuco board corners based on markers corners.
	
	Arguments:
		charuco_data (`pydataset.data.CharucoBoardData`): Charuco board data.
		image                          (`numpy.ndarray`): Image on which to interpolate corners.
		markers_corners         (`numpy.ndarray`/`None`): Markers corners.
		markers_ids             (`numpy.ndarray`/`None`): Markers ids.
		cam_K                          (`numpy.ndarray`): Camera parameters.
		cam_dist_coeffs                (`numpy.ndarray`): Camera distortion coeficients.

	Returns:
		corners (`numpy.ndarray`/`None`): Charuco board corners
		ids     (`numpy.ndarray`/`None`): Charuco board corners ids.
	"""
	assert pytools.assertions.type_is_instance_of( charuco_data, CharucoBoardData )
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.type_is_instance_of( markers_corners, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( markers_ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( cam_K, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( cam_dist_coeffs, (type(None), numpy.ndarray) )

	# Are corners empty ?
	if markers_corners is None:
		return None, None

	assert pytools.assertions.equal( markers_corners.dtype, numpy.float32 )
	assert pytools.assertions.equal( markers_ids.dtype, numpy.int32 )

	# Get the cv2 aruco board object and the number of markers
	cv2_charuco_board = charuco_data.get_cv2_board()

	# Interpolate the corners of the charuco board
	if cam_K is not None and cam_dist_coeffs is not None:
		assert pytools.assertions.equal( cam_K.dtype, numpy.float32 )
		assert pytools.assertions.equal( list(cam_K.shape), [3,3] )
		assert pytools.assertions.equal( cam_dist_coeffs.dtype, numpy.float32 )

		# Interpolate with known camera parameters
		nb, corners, ids = cv2.aruco.interpolateCornersCharuco( markers_corners, markers_ids, image, cv2_charuco_board, cam_K, cam_dist_coeffs )
	
	else:
		# Interpolatewith without camera parameters
		nb, corners, ids = cv2.aruco.interpolateCornersCharuco( markers_corners, markers_ids, image, cv2_charuco_board )
	
	if nb > 0:
		# Convert to numpy ndarray and reshape
		corners = numpy.asarray( corners, dtype=numpy.float32 )
		ids     = numpy.asarray( ids,     dtype=numpy.int32   )
		corners = numpy.reshape( corners, [nb, 2] )
		ids     = numpy.reshape( ids,     [nb]    )

		return corners, ids

	# if nb > 0

	return None, None

# def interpolate_corners ( image, markers_corners, charuco_settings ):

# --------------------------------------------------

def estimate_pose ( charuco_data, corners, ids, camera_matrix, dist_coeffs ):
	"""
	Estimates the pose of a given charuco board using detected corners and camera parameters.

	Arguments:
		charuco_board_data (`pydataset.data.CharucoBoardData`): Board of which to estimate the pose.
		corners (`list`): Detected corners.
		ids (`list`): Detected corners ids.
		camera_matrix (`numpy.ndarray`): Camera intrinsect parameters matrix.
		dist_coeffs   (`numpy.ndarray`): Camera distortion coeficients.

	Returns:
		r (`numpy.ndarray`): Euler angles in radians.
		t (`numpy.ndarray`): Translation vector in meters.
	"""
	assert pytools.assertions.type_is_instance_of( charuco_data, CharucoBoardData )
	assert pytools.assertions.type_is_instance_of( corners, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( camera_matrix, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( dist_coeffs, numpy.ndarray )

	# Number of corners to draw
	if corners is None or ids is None:
		return False, None, None

	# Estimate pose
	ok, r, t = cv2.aruco.estimatePoseCharucoBoard(
		numpy.reshape( corners, [1, -1, 2] ),
		numpy.reshape( ids, [1, -1] ),
		charuco_data.get_cv2_board(),
		camera_matrix,
		dist_coeffs,
		numpy.zeros( 3, dtype=numpy.float32 ),
		numpy.zeros( 3, dtype=numpy.float32 )
		)

	return ok, r, t
	
# def estimate_pose ( corners, ids, charuco_data, camera_matrix, dist_coeffs )

# --------------------------------------------------

def calibrate_camera ( charuco_data, corners, ids, image_size, maximum_number_of_samples=100 ):
	assert pytools.assertions.type_is_instance_of( charuco_data, CharucoBoardData )
	# assert pytools.assertions.type_is_instance_of( corners, (type(None), numpy.ndarray) )
	# assert pytools.assertions.type_is_instance_of( ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.tuple_items_type_is( image_size, int )
	assert pytools.assertions.type_is( maximum_number_of_samples, int )

	# If no corners or ids
	if corners is None or ids is None:
		return False, None, None

	# Number of samples
	number_of_samples = len( corners )

	# Get the charuco board
	cv2_board_data = charuco_data.get_cv2_board()

	# Create an index table of all the considered samples
	samples_indexes = list( range(number_of_samples) )

	# Shuffle the index table 
	random.shuffle( samples_indexes )

	# keep 'maximum_number_of_samples' samples
	final_corners = []
	final_ids     = []
	for index in range( min(number_of_samples, maximum_number_of_samples) ):

		# Get the sample index
		sample_index = samples_indexes[ index ]

		# Get the sample
		sample_corner = corners[ sample_index ]
		sample_id     =     ids[ sample_index ]

		# Keep this sample
		final_corners.append( sample_corner )
		final_ids.append( sample_id )
	
	# Reshape
	# final_corners = numpy.asarray( final_corners, dtype=numpy.float32 )
	# final_corners = numpy.reshape( final_corners, [maximum_number_of_samples, charuco_data.number_of_corners, 1, 2] )
	# final_ids     = numpy.asarray( final_ids, dtype=numpy.int32 )
	# final_ids     = numpy.reshape( final_ids, [maximum_number_of_samples, charuco_data.number_of_corners] )

	# Intial camera parameters and distance coefficients
	K = numpy.array([
		[ 2000.0,    0.0, image_size[ 0 ]/2.0 ],
		[    0.0, 2000.0, image_size[ 1 ]/2.0 ],
		[    0.0,    0.0,                 1.0 ]
		])
	d = numpy.zeros( (5) )

	# Run camera parameter estimation
	ret, K, d, r, t, std1, std2, err = cv2.aruco.calibrateCameraCharucoExtended(
		final_corners,
		final_ids,
		cv2_board_data,
		image_size,
		K,
		d
		)

	# Reshape
	K = numpy.reshape( K, [3, 3] ).astype( numpy.float32 )
	d = numpy.reshape( d, [5]    ).astype( numpy.float32 )

	return ret, K, d

# calibrate_camera ( charuco_data, corners, ids, image_size, maximum_number_of_samples )

# ##################################################
# ###             UTILITY FUNCTIONS              ###
# ##################################################

# --------------------------------------------------

def pack_detected_markers ( markers_corners, markers_ids, number_of_markers ):
	"""
	Packes markers corners and markers ids in a single array.

	"""
	assert pytools.assertions.type_is_instance_of( markers_corners, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( markers_ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is( number_of_markers, int )

	# Create the result array Nx4x2
	result      = numpy.empty( [number_of_markers, 4, 2], dtype=numpy.float32 )
	result[ : ] = numpy.nan

	# If nothing was detected
	if markers_corners is None or markers_ids is None:
		return result

	assert pytools.assertions.equal( markers_corners.dtype, numpy.float32 )
	assert pytools.assertions.equal( markers_ids.dtype,     numpy.int32   )

	# Reshape, thus checking the shape is compatible
	markers_corners = numpy.reshape( markers_corners, [-1, 4, 2] )
	markers_ids     = numpy.reshape( markers_ids,     [-1]       )

	# Number of detected markers
	number_of_detections = len( markers_corners )

	# Set each marker in its row
	for i in range( number_of_detections ):
		
		# Fetch the corners and the id of the current marker
		current_marker_corners = markers_corners[ i, :, : ]
		current_marker_ids     = markers_ids[ i ]

		# Set the corners in the line correspnding to the marker id
		result[ current_marker_ids, :, : ] = current_marker_corners
	
	# for i in range( number_of_detections )

	return result

# def pack_detected_markers ( markers_corners, markers_ids, number_of_markers )

# --------------------------------------------------

def pack_detected_corners ( corners, ids, number_of_corners ):
	assert pytools.assertions.type_is_instance_of( corners, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is( number_of_corners, int )

	# Intialize the result array
	result      = numpy.empty( [number_of_corners, 2], dtype=numpy.float32 )
	result[ : ] = numpy.nan

	# If nothing was detected
	if corners is None or ids is None:
		return result

	assert pytools.assertions.equal( corners.dtype, numpy.float32 )
	assert pytools.assertions.equal( ids.dtype,     numpy.int32   )

	# Reshape, thus checking the shape is compatible
	corners = numpy.reshape( corners, [-1, 2] )
	ids     = numpy.reshape( ids,     [-1]    )

	# Number of detected corners
	number_of_detected_corners = len( corners )

	# Go through each detected corners
	for i in range( number_of_detected_corners ):

		# Get the current corner/id
		current_corner = corners[ i, : ]
		current_id     = ids[ i ]

		# Set it in the result
		result[ current_id, : ] = current_corner
	
	# for i in range( number_of_detected_corners )

	return result

# def pack_detected_corners ( corners, ids, number_of_corners ):

# ##################################################
# ###          OPENCV DRAWING FUNCTIONS          ###
# ##################################################

# --------------------------------------------------

def draw_axis ( image, camera_matrix, dist_coeffs, rvec, tvec, length ):

	return cv2.aruco.drawAxis( image, camera_matrix, dist_coeffs, rvec, tvec, length )

# def draw_axis ( image, camera_matrix, dist_coeffs, rvec, tvec, length )

# --------------------------------------------------

def draw_markers ( image, markers_corners, markers_ids=None, color=(255,0,0) ):
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( markers_corners, numpy.ndarray )
	assert pytools.assertions.equal( markers_corners.dtype, numpy.float32 )
	assert pytools.assertions.type_is_instance_of( markers_ids, (type(None), numpy.ndarray) )
	assert pytools.assertions.type_is_instance_of( color, tuple )
	
	# Reshape markers corners
	markers_corners = numpy.reshape( markers_corners, [-1, 1, 4, 2] )

	# Are IDs provided ?
	if markers_ids is not None:

		assert pytools.assertions.equal( markers_ids.dtype, numpy.int32 )
		markers_ids = numpy.reshape( markers_ids, [-1, 1, 1] )

		# Draw the markers on the image
		return cv2.aruco.drawDetectedMarkers( image, markers_corners, markers_ids, color )

	else:
		return cv2.aruco.drawDetectedMarkers( image, markers_corners )
	
# def draw_markers ( image, markers_corners, markers_ids, color )

# --------------------------------------------------

def draw_board_corners ( image, charuco_board_corners, color=(0,0,255), draw_ids=False ):
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( charuco_board_corners, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( color, tuple )
	assert pytools.assertions.type_is( draw_ids, bool )

	# Number of corners to draw
	number_of_corners = charuco_board_corners.shape[ 0 ]

	# Reshape
	charuco_board_corners = numpy.reshape( charuco_board_corners, [number_of_corners, 1, 2] )

	if draw_ids:
		# Create a dummy verctor for the ids
		charuco_ids = numpy.arange( number_of_corners, dtype=numpy.int32 )
		charuco_ids = numpy.reshape( charuco_ids, [number_of_corners, 1, 1] )

		# Draw the corners on the image
		return cv2.aruco.drawDetectedCornersCharuco( image, charuco_board_corners, charuco_ids, color )

	else:
		return cv2.aruco.drawDetectedCornersCharuco( image, charuco_board_corners )

# def draw_board_corners ( image, charuco_board_corners, color )

# --------------------------------------------------

def save_charuco_board ( charuco_board_data, filepath, offset=(0.01,0.01) ):
	assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
	assert pytools.assertions.type_is( filepath, str )

	figure = matplotlib.pyplot.figure( figsize=[8.27, 11.69], dpi=200 )
	axes   = matplotlib.pyplot.axes( ylim=(1, 0) )

	plot_charuco_board( axes, charuco_board_data, offset )

	matplotlib.pyplot.axis( 'equal' )
	matplotlib.pyplot.axis( 'off' )
	matplotlib.pyplot.axis( [0.0, 8.27, 11.69, 0.0] )
	matplotlib.pyplot.tight_layout( pad=0.0, w_pad=0.0, h_pad=0.0 )
	matplotlib.pyplot.savefig(
		filepath,
		format='pdf',
		papertype='A4',
		quality=100.0,
		pad_inches=0.0
		)
	# matplotlib.pyplot.show()

# def save_charuco_board ( charuco_board_data )

# --------------------------------------------------

def plot_charuco_board ( axes, charuco_board_data, offset=(0.01,0.01) ):
	assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )

	plot_chessboard( axes, charuco_board_data, offset=offset )
	plot_markers( axes, charuco_board_data, offset=offset )

# def plot_charuco_board ( axes, charuco_board_data )

# --------------------------------------------------

def plot_chessboard ( axes, charuco_board_data, offset=(0.01,0.01) ):
	assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )

	width         = charuco_board_data.board_width
	height        = charuco_board_data.board_height
	square_length = units.meter_to_inch( charuco_board_data.square_length )
	border_length = units.meter_to_inch( charuco_board_data.border_length )
	offset_x      = units.meter_to_inch( offset[ 0 ] )
	offset_y      = units.meter_to_inch( offset[ 1 ] )

	# draw outline including border
	r = matplotlib.patches.Rectangle(
			(0, 0),
			width*square_length + 2*border_length + offset_x,
			height*square_length + 2*border_length + offset_y,
			edgecolor = (0, 0, 0, 1),
			facecolor = (0, 0, 0, 0),
			linewidth = units.meter_to_inch( 0.002 ) # 2mm
			)
	axes.add_patch( r )

	# Draw the cells
	ref = height % 2
	for y in range(height):
		for x in range(width):
			
			# ??? works
			mx = x % 2
			my = y % 2
			i = (mx + my) % 2
			
			# Black cell
			if ( i != ref ):
				
				r = matplotlib.patches.Rectangle(
					(border_length + x*square_length + offset_x, border_length + y*square_length + offset_y),
					square_length,
					square_length,
					edgecolor = (0, 0, 0, 0),
					facecolor = (0, 0, 0, 1)
					)
				axes.add_patch( r )

			# White cell
			else:
				pass
		
		# for x in range(width)
	# for y in range(height)

# def plot_chessboard ( axes, charuco_board_data )

# --------------------------------------------------

def plot_marker ( axes, marker, pos_x, pos_y, square_length, marker_length, border_size ):
	assert pytools.assertions.type_is( marker, numpy.ndarray )
	assert pytools.assertions.type_is( pos_x, float )
	assert pytools.assertions.type_is( pos_y, float )
	assert pytools.assertions.type_is( square_length, float )
	assert pytools.assertions.type_is( marker_length, float )
	assert pytools.assertions.type_is( border_size, int )

	# Dimensions of the marker without the boarder
	marker_shape  = marker.shape
	marker_width  = marker_shape[ 1 ]
	marker_height = marker_shape[ 0 ]
	marker_size   = marker_width
	assert marker_width == marker_height


	# Number of bits including border bits:
	total_size = marker_size + border_size * 2

	# Length of one bit
	bit_length = marker_length / float(total_size)

	# top-left corner of the marker bits drawing area
	x0 = pos_x + (square_length - marker_length) / 2.0
	y0 = pos_y + (square_length - marker_length) / 2.0

	# Fill the marker area with black
	r = matplotlib.patches.Rectangle( (x0, y0), marker_length, marker_length,
		edgecolor = (0, 0, 0, 0),
		facecolor = (0, 0, 0, 1)
		)
	axes.add_patch( r )

	# Offset of the actual marker contents
	x0 += border_size * bit_length
	y0 += border_size * bit_length

	for x in range( marker_size ):

		xi = x0 + x * bit_length
		
		for y in range( marker_size ):
			
			yi = y0 + y * bit_length

			# is this marker white bit ?
			if marker[ y, x ] == 1:

				r = matplotlib.patches.Rectangle( (xi, yi), bit_length, bit_length,
					edgecolor = (0, 0, 0, 0),
					facecolor = (1, 1, 1, 1)
					)
				axes.add_patch( r )

		# for x in range( marker_width )

	# for y in range( marker_height )

# def plot_marker ( axes, pos_x, pos_y, marker, square_length, marker_length )

# --------------------------------------------------

def plot_markers ( axes, charuco_board_data, border_size=1, offset=[0.01,0.01] ):
	assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
	assert pytools.assertions.type_is( border_size, int )

	board_width   = charuco_board_data.board_width
	board_height  = charuco_board_data.board_height
	square_length = units.meter_to_inch( charuco_board_data.square_length )
	marker_length = units.meter_to_inch( charuco_board_data.marker_length )
	border_length = units.meter_to_inch( charuco_board_data.border_length )
	offset_x      = units.meter_to_inch( offset[ 0 ] )
	offset_y      = units.meter_to_inch( offset[ 1 ] )
	marker_id     = 0

	ref = board_height % 2
	
	for y in range(board_height):

		for x in range(board_width):
			
			# ??? works
			mx = x % 2
			my = y % 2
			i = (mx + my) % 2
			
			# Black cell
			if ( i != ref ):
				pass

			# White cell
			else:
				pos_x = x * square_length + border_length + offset_x
				pos_y = y * square_length + border_length + offset_y
				
				marker     = charuco_board_data.get_marker_data( marker_id )
				marker_id += 1

				plot_marker( axes, marker, pos_x, pos_y, square_length, marker_length, border_size )

			# if ( i != ref )

		# for x in range(board_width)

	# for y in range(board_height)

# def plot_markers ( axes, charuco_board_data )

# --------------------------------------------------

def plot_protractor ( axes, center=[0.0, 0.0], radius=0.1, ec='black', linewidth=0.5, ticks={ 10: 0.04, 1: 0.02, 0.5: 0.01}, texts={ 10: (0.010, 10) } ):

	center[ 0 ] = units.meter_to_inch( center[0] )
	center[ 1 ] = units.meter_to_inch( center[1] )
	radius = units.meter_to_inch( radius )

	line = matplotlib.pyplot.Line2D( [center[0]-0.05, center[0]+0.05], [center[1], center[1]], color='black', linewidth=linewidth )
	axes.add_artist( line )
	line = matplotlib.pyplot.Line2D( [center[0], center[0]], [center[1]-0.05, center[1]+0.05], color='black', linewidth=linewidth )
	axes.add_artist( line )

	circle = matplotlib.pyplot.Circle( center, radius=radius, fill=False, ec=ec, linewidth=linewidth )
	axes.add_artist( circle )

	ticks_keys = sorted( list(ticks.keys()) )
	texts_keys = sorted( list(texts.keys()) )

	for angle in numpy.arange( 0, 360, 0.5, dtype=numpy.float32 ):

		tick_length = 0
		for tick_key in ticks_keys:
			if ( numpy.fmod( angle, tick_key ) < numpy.finfo(numpy.float32).eps ):
				tick_length = units.meter_to_inch( ticks[ tick_key ] )

		text_length = 0
		text_fontsize = 0
		for text_key in texts_keys:
			if ( numpy.fmod( angle, text_key ) < numpy.finfo(numpy.float32).eps ):
				text_length   = units.meter_to_inch( texts[ text_key ][ 0 ] )
				text_fontsize = texts[ text_key ][ 1 ]

		rad = numpy.radians(angle)

		x_start = center[ 0 ] + numpy.cos( rad ) * (radius - tick_length)
		y_start = center[ 1 ] + numpy.sin( rad ) * (radius - tick_length)
		x_end   = center[ 0 ] + numpy.cos( rad ) * radius
		y_end   = center[ 1 ] + numpy.sin( rad ) * radius
		x_text  = center[ 0 ] + numpy.cos( rad ) * (radius - text_length)
		y_text  = center[ 1 ] + numpy.sin( rad ) * (radius - text_length)

		line = matplotlib.pyplot.Line2D( [x_start, x_end], [y_start, y_end], color='black', linewidth=linewidth )
		axes.add_artist( line )

		if ( text_length != 0 ):
			text = matplotlib.pyplot.Text( x_text, y_text, '{:3.0f}'.format(angle), fontsize=text_fontsize, horizontalalignment='center', verticalalignment='center', rotation=-angle )
			axes.add_artist( text )

		# if ( text_length != 0 )

	# for angle in NumPy.arange( 0, 360, 0.5, dtype=NumPy.float32 )

# def plot_protractor ( axes, center, radius, ec, linewidth, ticks, texts ):

# --------------------------------------------------

def save_protractor ( filepath, radius=0.084, ec='black', linewidth=0.5, ticks={ 10: 0.005, 1: 0.0035, 0.5: 0.0025}, texts={ 10: (0.010, 10) } ):
	assert pytools.assertions.type_is( filepath, str )

	figure = matplotlib.pyplot.figure( figsize=[8.27, 11.69], dpi=200 )
	axes   = matplotlib.pyplot.axes( ylim=(1, 0) )

	center = [units.inch_to_meter(8.27/2.0), units.inch_to_meter(11.69/2.0)]
	plot_protractor( axes, center, radius, ec, linewidth, ticks, texts )

	matplotlib.pyplot.axis( 'equal' )
	matplotlib.pyplot.axis( 'off' )
	matplotlib.pyplot.axis( [0.0, 8.27, 11.69, 0.0] )
	matplotlib.pyplot.tight_layout( pad=0.0, w_pad=0.0, h_pad=0.0 )
	matplotlib.pyplot.savefig(
		filepath,
		format='pdf',
		papertype='A4',
		quality=100.0,
		pad_inches=0.0
		)
	# matplotlib.pyplot.show()

# def save_charuco_board ( charuco_board_data )