# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import numpy
import plyfile

# INTERNAL
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data       import ColorData
from .entity_data import EntityData

# ##################################################
# ###              CLASS MESH-DATA               ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshData',
	   namespace = 'pydataset.render',
	fields_names = [
		'primitive',
		'points',
		'colors',
		'normals',
		'indexes'
		]
	)
class MeshData ( EntityData ):
	"""
	The `pydataset.render.MeshData` class is used to load and save 3D models.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, primitive='triangles', points=None, colors=None, normals=None, indexes=None, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.render.MeshData` class.

		Arguments:
			self (`pydataset.render.MeshData`): Instance to initialize.
			primitive                  (`str`): Primitive type.
			points    (`None`/`numpy.ndarray`): Vertex positions.
			colors    (`None`/`numpy.ndarray`): Vertex colors.
			normals   (`None`/`numpy.ndarray`): Vertex normals.
			indexes   (`None`/`numpy.ndarray`): Indexes.

		Named Arguments:
			self                    (`pydataset.render.EntityData`): Instance to initialize.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is( primitive, str )
		assert pytools.assertions.value_in( primitive, ['points', 'lines', 'triangles'] )
		assert pytools.assertions.type_in( points, (type(None), numpy.ndarray) )
		assert pytools.assertions.type_in( colors, (type(None), numpy.ndarray) )
		assert pytools.assertions.type_in( normals, (type(None), numpy.ndarray) )
		assert pytools.assertions.type_in( indexes, (type(None), numpy.ndarray) )
		if points is not None:
			assert pytools.assertions.equal( points.dtype, numpy.float32 )
			assert pytools.assertions.equal( points.shape[1], 3 )
		if colors is not None:
			assert pytools.assertions.equal( colors.dtype, numpy.float32 )
			assert pytools.assertions.equal( colors.shape[1], 4 )
		if normals is not None:
			assert pytools.assertions.equal( normals.dtype, numpy.float32 )
			assert pytools.assertions.equal( normals.shape[1], 3 )
		if indexes is not None:
			assert pytools.assertions.equal( indexes.dtype, numpy.uint32 )
			if primitive == 'points':
				assert pytools.assertions.equal( indexes.shape[1], 1 )
			elif primitive == 'lines':
				assert pytools.assertions.equal( indexes.shape[1], 2 )
			elif primitive == 'triangles':
				assert pytools.assertions.equal( indexes.shape[1], 3 )

		super( MeshData, self ).__init__( **kwargs )

		self._primitive = primitive
		self._points    = points
		self._colors    = colors
		self._normals   = normals
		self._indexes   = indexes

	# def __init__ ( self, primitive, points, colors, normals, indexes, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def primitive ( self ):
		"""
		Primitive type (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return self._primitive
		
	# def primitive ( self )

	# --------------------------------------------------

	@primitive.setter
	def primitive ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['points', 'lines', 'triangles'] )

		self._primitive = value
		
	# def primitive ( self, value )

	# --------------------------------------------------

	@property
	def points ( self ):
		"""
		Vertices positions (`None`/`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return self._points
		
	# def points ( self )

	# --------------------------------------------------

	@points.setter
	def points ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( value, (type(None), numpy.ndarray) )

		self._points = value
		
	# def points ( self, value )

	# --------------------------------------------------

	@property
	def colors ( self ):
		"""
		Vertices colors (`None`/`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return self._colors
		
	# def colors ( self )

	# --------------------------------------------------

	@colors.setter
	def colors ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( value, (type(None), numpy.ndarray) )
		if value is not None:
			assert pytools.assertions.equal( value.dtype, numpy.float32 )
			assert pytools.assertions.equal( value.shape[1], 4 )

		self._colors = value
		
	# def colors ( self, value )

	# --------------------------------------------------

	@property
	def normals ( self ):
		"""
		Vertices normals (`None`/`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return self._normals
		
	# def normals ( self )

	# --------------------------------------------------

	@normals.setter
	def normals ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( value, (type(None), numpy.ndarray) )

		self._normals = value
		
	# def normals ( self, value )

	# --------------------------------------------------

	@property
	def indexes ( self ):
		"""
		Mesh indexes (`None`/`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return self._indexes
		
	# def indexes ( self )

	# --------------------------------------------------

	@indexes.setter
	def indexes ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( value, (type(None), numpy.ndarray) )

		self._indexes = value
		
	# def indexes ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
		
	# --------------------------------------------------

	def get_bounding_box ( self ):

		# Check mesh data
		if self.get_number_of_vertices() == 0:
			bb      = numpy.empty( [8, 3], numpy.float32 )
			bb[ : ] = numpy.nan
			return bb

		xs = self.points[ :, 0 ]
		ys = self.points[ :, 1 ]
		zs = self.points[ :, 2 ]

		x_min = xs.min()
		y_min = ys.min()
		z_min = zs.min()
		x_max = xs.max()
		y_max = ys.max()
		z_max = zs.max()

		bounding_box = [
			[x_min, y_min, z_min],
			[x_max, y_min, z_min],
			[x_max, y_max, z_min],
			[x_min, y_max, z_min],
			[x_min, y_min, z_max],
			[x_max, y_min, z_max],
			[x_max, y_max, z_max],
			[x_min, y_max, z_max]
		]

		bounding_box = numpy.array( bounding_box, dtype=numpy.float32 )
		bounding_box = numpy.reshape( bounding_box, [8, 3] )
		return bounding_box

	# def get_bounding_box ( self )

	# --------------------------------------------------

	def get_number_of_vertices ( self ):

		return self.points.shape[ 0 ] if self.points is not None else 0

	# def get_number_of_vertices ( self )
	
	# --------------------------------------------------

	def get_number_of_primitives ( self ):

		return self.indexes.shape[ 0 ] if self.indexes is not None else 0

	# def get_number_of_primitives ( self )

	# --------------------------------------------------

	def get_primitive_size ( self ):

		if self.primitive == 'points':
			return 1
		elif self.primitive == 'lines':
			return 2
		elif self.primitive == 'triangles':
			return 3
		else:
			raise RuntimeError( 'Unsupported primitive: {}'.format(self.primitive) )

	# def get_primitive_size ( self )

	# --------------------------------------------------

	def get_index_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		number_of_primitives = self.get_number_of_primitives()
		primitive_size       = self.get_primitive_size()

		# Create the dtype
		dtype = [
			('index', numpy.uint32, primitive_size)
			]

		# Create the index data array
		index_data = numpy.zeros(
			number_of_primitives,
			dtype = dtype
			)

		# Copy indexes
		index_data[ 'index' ] = self.indexes.squeeze()
		
		return index_data
		
	# def get_index_data ( self )

	# --------------------------------------------------

	def get_vertex_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		number_of_vertices = self.get_number_of_vertices()

		# Create the dtype
		dtype = [
			( 'point', numpy.float32, 3),
			( 'color', numpy.float32, 4),
			('normal', numpy.float32, 3)
			]
		
		# Create the vertex data array
		vertex_data = numpy.zeros(
			number_of_vertices,
			dtype = dtype
			)

		# Copy vertex positions
		vertex_data[ 'point' ] = self.points

		# Copy vertex colors
		if self.colors is not None:
			vertex_data[ 'color' ] = self.colors

		# Copy vertex normals
		if self.normals is not None:
			vertex_data[ 'normal' ] = self.normals

		return vertex_data

	# def get_vertex_data ( self )

	# --------------------------------------------------

	def save_obj ( self, filepath, override=True ):
		"""
		Saves this mesh in OBJ format.

		Arguments:
			self       (`pydataset.render.MeshData`): Mesh to save.
			filepath (`pytools.path.FilePath`/`str`): Path of the file where to save this mesh.
			override                        (`bool`): If `True`, overrides the file if it exists, if `False` then an error is raised if the file exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )
		assert pytools.assertions.equal( filepath.extension().lower(), '.obj' )
		assert pytools.assertions.type_is( override, bool )

		if ( filepath.is_a_file() and not override ):
			raise IOError( 'The file {} already exists (override={})'.format(filepath, override) )

		if self.primitive != 'triangles':
			raise RuntimeError( 'Can only save triangular mesh' )

		number_of_points = self.points.shape[0]
		number_of_faces  = self.indexes.shape[0]
		has_colors       = self.colors is not None
		has_normals      = self.normals is not None

		with filepath.open( 'w' ) as file:

			# COMMENTS
			file.write( '# INFORMATIONS\n' )
			file.write( '# {}.obj\n'.format(self.name) )
			file.write( '#\n' )
			file.write( '# number of vertices: {}\n'.format(number_of_points) )
			file.write( '# number of faces   : {}\n'.format(number_of_faces) )
			file.write( '# has colors        : {}\n'.format(has_colors) )
			file.write( '# has normals       : {}\n'.format(has_normals) )
			file.write( '#\n' )
			file.write( 'o {}\n'.format(self.name) )

			file.write( '# VERTICES\n' )
			for index in range( number_of_points ):
				x, y, z = self.points[ index ]
				file.write( 'v {:.4f} {:.4f} {:.4f}'.format(x, y, z) )
				if has_colors:
					r, g, b, a = self.colors[ index ]
					file.write( ' {:.4f} {:.4f} {:.4f} {:.4f}'.format(r, g, b, a) )
				file.write( '\n' )
			file.write( '#\n' )

			if has_normals:
				file.write( '# NORMALS\n' )
				for index in range( number_of_points ):
					nx, ny, nz = self.normals[ index ]
					file.write( 'vn {:.4f} {:.4f} {:.4f}\n'.format( nx, ny, nz ) )
				file.write( '#\n' )

			file.write( '# FACES\n' )
			offset=0
			for index in range( number_of_faces ):
				i, j, k = self.indexes[ index ]
				if has_normals:
					file.write( 'f {:d}//{:d} {:d}//{:d} {:d}//{:d}'.format(i+offset, i+offset, j+offset, j+offset, k+offset, k+offset) )
				else:
					file.write( 'f {:d} {:d} {:d}'.format(i, j, k) )
					
				file.write( '\n' )
			file.write( '#\n' )

		# with filepath.open( 'w' ) as file

	# def save_obj ( self, filepath, override=True )

	# --------------------------------------------------

	def save_ply ( self, filepath, override=True ):
		"""
		Saves this mesh in ply format.

		Arguments:
			self       (`pydataset.render.MeshData`): Mesh to save.
			filepath (`pytools.path.FilePath`/`str`): Path of the file where to save this mesh.
			override                        (`bool`): If `True`, overrides the file if it exists, if `False` then an error is raised if the file exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )
		assert pytools.assertions.equal( filepath.extension(), '.ply' )
		assert pytools.assertions.type_is( override, bool )

		if ( filepath.is_a_file() and not override ):
			raise IOError( 'The file {} already exists (override={})'.format(filepath, override) )

		if self.primitive != 'triangles':
			raise RuntimeError( 'Can only save triangular mesh' )

		with filepath.open( 'w' ) as file:

			# Write the header
			file.write( 'ply\n' )
			file.write( 'format ascii 1.0\n' )
			file.write( 'comment created by ITECA\n' )
			file.write( 'element vertex {}\n'.format( self.get_number_of_vertices() ) )
			file.write( 'property float32 x\n' )
			file.write( 'property float32 y\n' )
			file.write( 'property float32 z\n' )

			if ( self.normals is not None ):
				file.write( 'property float32 nx\n' )
				file.write( 'property float32 ny\n' )
				file.write( 'property float32 nz\n' )

			if ( self.colors is not None ):
				file.write( 'property uint8 red\n' )
				file.write( 'property uint8 green\n' )
				file.write( 'property uint8 blue\n' )
				file.write( 'property uint8 alpha\n' )

			file.write( 'element face {}\n'.format( self.get_number_of_primitives() ) )
			file.write( 'property list uint8 int32 vertex_indices\n' )
			file.write( 'end_header\n' )

			for vertex_index in range( self.get_number_of_vertices() ):

				point = self.points[ vertex_index, : ]
				file.write( '{:.4f} {:.4f} {:.4f}'.format( point[0], point[1], point[2] ) )

				if ( self.normals is not None ):
					normal = self.normals[ vertex_index, : ]
					file.write( ' {:.4f} {:.4f} {:.4f}'.format( normal[0], normal[1], normal[2] ) )

				if ( self.colors is not None ):
					color = ( self.colors[ vertex_index, : ] * 255.0 ).astype( numpy.uint8 )
					file.write( ' {:d} {:d} {:d} {:d}'.format( color[0], color[1], color[2], color[3] ) )

				file.write( '\n' )

			# for vertex_index in range( self.header.number_of_vertices )

			for face_index in range( self.get_number_of_primitives() ):

				index = self.indexes[ face_index, : ]
				file.write( '3 {:d} {:d} {:d}\n'.format( index[0], index[1], index[2] ) )
			
			# for face_index in range( self.header.number_of_primitives )

		# with filepath.open( 'w' ) as file

	# def save_ply ( self, filepath, override )
	
	# --------------------------------------------------

	def save_xyz ( self, filepath, override=True ):
		"""
		Saves this mesh in xyz format.

		Arguments:
			self       (`pydataset.render.MeshData`): Mesh to save.
			filepath (`pytools.path.FilePath`/`str`): Path of the file where to save this mesh.
			override                        (`bool`): If `True`, overrides the file if it exists, if `False` then an error is raised if the file exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		if isinstance( filepath, str ):
			filepath = pytools.path.FilePath( filepath )
		assert pytools.assertions.equal( filepath.extension(), '.xyz' )
		assert pytools.assertions.type_is( override, bool )

		if ( filepath.is_a_file() and not override ):
			raise IOError( 'The file {} already exists (override={})'.format(filepath, override) )

		number_of_vertices = self.get_number_of_vertices()
		has_colors = self.colors is not None
		has_normals = self.normals is not None

		with filepath.open( 'w' ) as file:
			file.write( '{:d} {:.4f}\n'.format( number_of_vertices, 1.0) )

			for index in range(number_of_vertices):

				point = self.points[ index, : ]
				file.write( '{:.4f} {:.4f} {:.4f}'.format( point[0], point[1], point[2] ) )

				if has_colors:
					color = ( self.colors[ index, : ] * 255.0 ).astype( numpy.uint8 )
					file.write( ' {:d} {:d} {:d} {:d}'.format( color[0], color[1], color[2], color[3] ) )
				
				if has_normals:
					normal = self.normals[ index, : ]
					file.write( ' {:.4f} {:.4f} {:.4f}'.format( normal[0], normal[1], normal[2] ) )

				file.write( '\n' )

			# for index in range(self.number_of_vertices)

		# with filepath.open( 'w' ) as file:

	# def save_xyz ( self, filepath, override=True )

	# --------------------------------------------------

	def set_alpha ( self, alpha ):
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is( alpha, float )

		if self._colors is None:
			raise RuntimeError( 'Mesh has no colors' )
		
		self._colors[ :, 3 ] = alpha

	# def set_alpha ( self, alpha )

	# --------------------------------------------------

	def set_color ( self, color ):
		"""
		Sets the given color for all the vertices of this mesh.

		Arguments:
			self (`pydataset.render.MeshData`): Mesh of which to set the color.
			color (`pydataset.data.ColorData`): Color to give to all the vertices.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is_instance_of( color, ColorData )

		# Convert color to numpy array, discard alpha
		color = color.to_array()
		color = color.astype( numpy.float32 )
		color = color / 255.0

		# Reapeat the color N times
		color  = numpy.reshape( color, [1, 4] )
		colors = numpy.repeat(  color, self.get_number_of_vertices(), axis=0 )

		self._colors = colors

	# def set_color ( self, color )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this `pydataset.render.MeshData`.

		Arguments:
			self (`pydataset.render.MeshData`): `pydataset.render.MeshData` instance to represent.

		Returns:
			`str`: Text representation of this `pydataset.render.MeshData`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return '[Mesh: name={}, enabled={}, primitive={}, points={}, colors={}, normals={}, indexes={}]'.format(
			self.name,
			self.enabled,
			self.primitive,
			self.points.shape[0] if self.points is not None else None,
			self.colors.shape[0] if self.colors is not None else None,
			self.normals.shape[0] if self.normals is not None else None,
			self.indexes.shape[0] if self.indexes is not None else None
			)

	# def __str__ ( self )

	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this mesh data to a python `dict`.

		Arguments:
			self (`pydataset.render.MeshData`): Mesh data to serialize.
			ignores          (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized mesh data.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		# run default serialization method
		if 'points' not in ignores:
			ignores.append( 'points' )
		if 'colors' not in ignores:
			ignores.append( 'colors' )
		if 'normals' not in ignores:
			ignores.append( 'normals' )
		if 'indexes' not in ignores:
			ignores.append( 'faces' )
		data = super( MeshData, self ).serialize( ignores )
		
		# Serialize the arrays
		data[ 'points'  ], s, d = pytools.serialization.serialize_ndarray( self.points  )
		data[ 'colors'  ], s, d = pytools.serialization.serialize_ndarray( self.colors  )
		data[ 'normals' ], s, d = pytools.serialization.serialize_ndarray( self.normals )
		data[ 'indexes' ], s, d = pytools.serialization.serialize_ndarray( self.indexes )
		
		# Return the data
		return data

	# def __serialize__ ( self, ignores )

	# --------------------------------------------------

	def deserialize ( self, data, ignores ):
		"""
		Deserializes the fields of this mesh data from a python `dict`.

		Arguments:
			self (`pydataset.render.MeshData`): Mesh data to deserialize.
			data                      (`dict`): The serialized data.
			ignores          (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		# run default serialization method
		if 'points' not in ignores:
			ignores.append( 'points' )
		if 'colors' not in ignores:
			ignores.append( 'colors' )
		if 'normals' not in ignores:
			ignores.append( 'normals' )
		if 'indexes' not in ignores:
			ignores.append( 'indexes' )
		super( MeshData, self ).deserialize( data, ignores )
		
		# Deserialize the arrays
		self.points  = pytools.serialization.deserialize_ndarray( data[ 'points'  ], [-1, 3], 'float32' )
		self.colors  = pytools.serialization.deserialize_ndarray( data[ 'colors'  ], [-1, 4], 'float32' )
		self.normals = pytools.serialization.deserialize_ndarray( data[ 'normals' ], [-1, 3], 'float32' )
		self.indexes = pytools.serialization.deserialize_ndarray( data[ 'indexes' ], [-1, self.get_primitive_size()], 'uint32'  )
		
	# def deserialize ( self, data )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@staticmethod
	def load_ply ( filepath ):
		"""
		Loads a mesh from a ply file.
		
		Only triangular mesh are supported.

		Arguments:
			filepath (`pytools.path.FilePath`): Path of the ply file.

		Returns:
			`pydataset.render.MeshData`: The mesh data.
		"""
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		if isinstance(filepath, str):
			filepath = pytools.path.FilePath(filepath)
		assert pytools.assertions.equal( filepath.extension(), '.ply' )
		assert filepath.is_a_file()

		# Open the file
		f = filepath.open( 'r' )
		line_index = -1

		# Header data
		name = filepath.filename()
		vertices_properties   = [] #list of tuples (property name, property data type)
		faces_properties      = [] #list of tuples (property name, property data type)
		is_binary             = False
		header_vertex_section = False
		header_face_section   = False
		number_of_vertices    = 0
		number_of_primitives  = 0

		# Read header
		while True:
			line = f.readline().rstrip('\n').rstrip('\r') # Strip the newline character(s)
			line_index += 1

			if line.startswith('element vertex'):
				number_of_vertices = int(line.split(' ')[-1])
				header_vertex_section = True
				header_face_section = False

			elif line.startswith('element face'):
				number_of_primitives = int(line.split(' ')[-1])
				header_vertex_section = False
				header_face_section = True

			elif line.startswith('element'): # Some other element
				header_vertex_section = False
				header_face_section = False

			# Property section
			elif line.startswith('property') and header_vertex_section:

				# (name of the property, data type of the property)
				vertices_properties.append( (line.split(' ')[-1], line.split(' ')[-2]) )

			# Property list section
			elif line.startswith('property list') and header_face_section:

				elems = line.split(' ')
				
				# (name of the property, data type of the property)
				faces_properties.append( ('n_corners', elems[2]) )
				
				for i in range(3):
					faces_properties.append(('ind_' + str(i), elems[3]))

			elif line.startswith('format'):
				if 'binary' in line:
					is_binary = True

			elif line.startswith('end_header'):
				header_vertex_section = False
				header_face_section = False
				break

		# Vertices properties names
		vertices_properties_names = set( [prop[0] for prop in vertices_properties] )

		# Set in the header if the mesh has normal or colors
		has_normals = {'nx',  'ny',    'nz'  }.issubset( vertices_properties_names )
		has_colors  = {'red', 'green', 'blue'}.issubset( vertices_properties_names )

		# Prepare data structures
		points  = numpy.zeros( [number_of_vertices,   3], dtype=numpy.float32 )
		colors  = numpy.ones(  [number_of_vertices,   4], dtype=numpy.float32 ) if has_colors  else None
		normals = numpy.zeros( [number_of_vertices,   3], dtype=numpy.float32 ) if has_normals else None
		indexes = numpy.zeros( [number_of_primitives, 3], dtype=numpy.uint32   )
		
		# For binary format
		formats = {
			 'float': ('f', 4),
			'double': ('d', 8),
			   'int': ('i', 4),
			 'uchar': ('B', 1)
		}

		# Load vertices
		for vertex_index in range( number_of_vertices ):

			properties_values = {}
			loaded_properties = ['x', 'y', 'z', 'nx', 'ny', 'nz', 'red', 'green', 'blue']

			# Read binary vertex
			if is_binary:
				for prop in vertices_properties:

					prop_name      = prop[ 0 ]
					prop_data_type = prop[ 1 ]

					format = formats[ prop_data_type ]
					value  = struct.unpack( format[0], f.read(format[1]) )[ 0 ]
					
					if prop_name in loaded_properties:
						properties_values[ prop_name ] = value

			# Read ASCII vertex
			else:
				elements = f.readline().rstrip('\n').rstrip('\r').split(' ')
				line_index += 1

				for prop_index, prop in enumerate( vertices_properties ):

					prop_name      = prop[ 0 ]
					prop_data_type = prop[ 1 ]

					if prop_name in loaded_properties:
						properties_values[ prop_name ] = elements[ prop_index ]

			# Parse the vertex position
			points[ vertex_index, 0 ] = float( properties_values['x'] )
			points[ vertex_index, 1 ] = float( properties_values['y'] )
			points[ vertex_index, 2 ] = float( properties_values['z'] )

			if has_normals:
				normals[ vertex_index, 0 ] = float( properties_values['nx'] )
				normals[ vertex_index, 1 ] = float( properties_values['ny'] )
				normals[ vertex_index, 2 ] = float( properties_values['nz'] )

			if has_colors:
				colors[ vertex_index, 0 ] = float( properties_values['red'  ] ) / 255.0
				colors[ vertex_index, 1 ] = float( properties_values['green'] ) / 255.0
				colors[ vertex_index, 2 ] = float( properties_values['blue' ] ) / 255.0

		# Load faces
		for face_id in range( number_of_primitives ):

			properties_values = {}

			# Load binary face
			if is_binary:
				for prop in faces_properties:

					prop_name      = prop[ 0 ]
					prop_data_type = prop[ 1 ]

					format = formats[ prop_name ]
					value  = struct.unpack( format[0], f.read(format[1]) )[ 0 ]
					
					if ( prop_name == 'n_corners' and value != 3 ):
						raise ValueError( 'Only triangular faces are supported. not {}'.format(value) )

					properties_values[ prop_name ] = value

			# Load ASCII face
			else:
				elements = f.readline().rstrip('\n').rstrip('\r').split(' ')
				line_index += 1
				
				for prop_index, prop in enumerate( faces_properties ):
					
					prop_name      = prop[ 0 ]
					prop_data_type = prop[ 1 ]
					value          = int( elements[ prop_index ] )
					
					if ( prop_name == 'n_corners' and value != 3 ):
						raise ValueError( 'Only triangular faces are supported. not {}'.format(value) )

					properties_values[ prop_name ] = value

			indexes[ face_id, 0 ] = properties_values[ 'ind_0' ]
			indexes[ face_id, 1 ] = properties_values[ 'ind_1' ]
			indexes[ face_id, 2 ] = properties_values[ 'ind_2' ]

		f.close()

		return MeshData(
			primitive = 'triangles',
			   points = points,
			   colors = colors,
			  normals = normals,
			  indexes = indexes,
			     name = name,
			  enabled = True
			)

	# def load_ply ( filepath )
	
	# --------------------------------------------------

	@staticmethod
	def load_ply_v2 ( filepath ):
		"""
		Loads a mesh from a ply file.
		
		Only triangular mesh are supported.

		Arguments:
			filepath (`pytools.path.FilePath`): Path of the ply file.

		Returns:
			`pydataset.render.MeshData`: The mesh data.
		"""
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		filepath = pytools.path.FilePath.ensure( filepath )
		assert pytools.assertions.equal( filepath.extension(), '.ply' )
		assert filepath.is_a_file()
		
		# PLY default values
		primitive = 'points',
		points    = None
		colors    = None
		normals   = None
		indexes   = None
		name      = filepath.filename()

		# Read the PLY
		ply_data = plyfile.PlyData.read( str(filepath) )
		
		# Check that the mesh contains at least vertices
		if 'vertex' not in ply_data:
			raise IOError( "The PLY file '{}' does not contain any 'vertex' elements.".format(filepath) )
		
		# Fetch vertices
		vertices_data = ply_data[ 'vertex' ]

		# Fetch coordinates
		if 'x' not in vertices_data or 'y' not in vertices_data or 'z' not in vertices_data:
			raise IOError( "The vertex element of PLY file '{}' does not have the attributes 'x', 'y', and 'z'.".format(filepath) )

		# Fetch X,Y,Z coordinates
		x = vertices_data['x']
		y = vertices_data['y']
		z = vertices_data['z']
		# Parse X,Y,Z coordinates
		points = numpy.stack( (x,y,z), axis=-1 ).astype( numpy.float32 )
		
		# Read Colors ?
		if 'red' in vertices_data and 'green' in vertices_data and 'blue' in vertices_data:
			# Fetch colors
			r = vertices_data['red'  ].astype(numpy.float32) / 255.
			g = vertices_data['green'].astype(numpy.float32) / 255.
			b = vertices_data['blue' ].astype(numpy.float32) / 255.
			# Fetch alpha ?
			if 'alpha' in vertices_data:
				a = vertices_data['alpha' ].astype(numpy.float32) / 255.
			else:
				a = numpy.ones_like(r)
			# Parse colors
			colors = numpy.stack( (r,g,b,a), axis=-1 ).astype( numpy.float32 )
		
		# Read normals ?
		if 'nx' in vertices_data and 'ny' in vertices_data and 'nz' in vertices_data:
			# Fetch normals
			nx = vertices_data['nx']
			ny = vertices_data['ny']
			nz = vertices_data['nz']
			# Parse normals
			normals = numpy.stack( (nx,ny,nz), axis=-1 ).astype( numpy.float32 )
		

		# Read faces ?
		if 'face' in ply_data:
			# Fetch faces
			faces = ply_data[ 'face' ]
			# Check the vertex indexes
			if 'vertex_indices' in faces:
				# Fetch vertices indexes
				indexes = faces[ 'vertex_indices' ]
				# Parse vertices indexes
				indexes = numpy.vstack( indexes ).astype( numpy.uint32 )
				
				if indexes.shape[-1] == 2:
					primitive = 'lines'
				elif indexes.shape[-1] == 3:
					primitive = 'triangles'
				else:
					raise IOError( "The face element of PLY file '{}' has unsupported primitive: {}.".format(filepath, indexes.shape) )

		# Create the mesh data
		return MeshData(
			primitive = 'triangles',
			   points = points,
			   colors = colors,
			  normals = normals,
			  indexes = indexes,
			     name = name,
			  enabled = True
			)

	# def load_ply_v2 ( filepath )

	# --------------------------------------------------

	@staticmethod
	def load_xyz ( filepath, items=['x', 'y', 'z', 'nx', 'ny', 'nz', 'r', 'g', 'b'] ):
		"""
		Loads mesh data from a xyz file.
		
		Only triangular mesh are supported.

		Arguments:
			filepath (`pytools.path.FilePath`): Path of the xyz file.
			items            (`list` of `str`): Item to on each line.

		Returns:
			`pydataset.render.MeshData`: The mesh data.
		"""
		assert pytools.assertions.type_is_instance_of( filepath, pytools.path.FilePath )
		assert pytools.assertions.equal( filepath.extension(), '.xyz' )
		assert filepath.is_a_file()
		assert pytools.assertions.type_is( items, list )
		assert pytools.assertions.list_items_type_is( items, str )
		assert 'x' in items
		assert 'y' in items
		assert 'z' in items

		# Number of items on each line
		number_of_items = len( items )

		# Read the file line by line
		with filepath.open('r') as file:
			
			# Read header
			header = file.readline()

			# Check header
			if ( not header ):
				raise ValueError( 'Unexpected end-of-file on line N°{} in file {}'.format(i, filepath) )

			# trim and split the line
			header = header.rstrip('\n').rstrip('\r').split(' ')

			# Check header
			if ( len(header) != 2 ):
				raise ValueError( 'Incorrect header data on line N°{}: {}'.format(i, header) )

			# Parse header
			name               = filepath.filename()
			number_of_vertices = int( header[0] )
			vertices_size      = float( header[1] )

			# create data buffer
			points  = numpy.zeros( [number_of_vertices, 3], dtype=numpy.float32 )
			normals = numpy.zeros( [number_of_vertices, 3], dtype=numpy.float32 )
			colors  = numpy.ones(  [number_of_vertices, 4], dtype=numpy.float32 )

			# Read each voxel
			for i in range( number_of_vertices ):
				
				# Read a voxel line
				voxel = file.readline()

				# Check voxel
				if ( not voxel ):
					raise ValueError( 'Unexpected end-of-file on line N°{} in file {}'.format(i, filepath) )

				# Trim and split the voxel line
				voxel = voxel.rstrip('\n').rstrip('\r').split(' ')
				
				# Check voxel
				if ( len(voxel) != number_of_items ):
					raise ValueError(
						'Incorrect voxel data on line N°{}: expected {} items but go {} items. {}'.format(
							i,
							number_of_items,
							len(voxel),
							voxel
							)
						)

				# Parse voxel
				voxel = numpy.asarray( voxel, dtype=numpy.float32 )

				# Set each item
				for item_index in range(number_of_items):

					# Fetch item
					item = voxel[ item_index ]
					name = items[ item_index ]

					if name == 'x':
						points[ i, 0 ] = item
					elif name == 'y':
						points[ i, 1 ] = item
					elif name == 'z':
						points[ i, 2 ] = item

					elif name == 'nx':
						normals[ i, 0 ] = item
					elif name == 'ny':
						normals[ i, 1 ] = item
					elif name == 'nz':
						normals[ i, 2 ] = item

					elif name == 'r':
						colors[ i, 0 ] = item
					elif name == 'g':
						colors[ i, 1 ] = item
					elif name == 'b':
						colors[ i, 2 ] = item

					elif name == 'R':
						colors[ i, 0 ] = item / 255.0
					elif name == 'G':
						colors[ i, 1 ] = item / 255.0
					elif name == 'B':
						colors[ i, 2 ] = item / 255.0

					else:
						raise RuntimeError( 'Item name not recognized: {}'.format(name) )

				# Normalize the normal
				normal_norm = normals[ i, : ].sum()
				eps         = numpy.finfo(numpy.float32).eps
				if normal_norm > eps and (normal_norm-1.0) > eps:
					normals[ i, : ] /= normal_norm

			# for i in range( voxels_count ):

			# Create indexes for the points
			indexes = numpy.arange( number_of_vertices, dtype=numpy.uint32 )
			indexes = numpy.reshape( indexes, [number_of_vertices, 1] )

			return MeshData(
				primitive = 'points',
				   points = points,
				   colors = colors,
				  normals = normals,
				  indexes = indexes,
				     name = name,
				  enabled = True
				)

		# with filepath.open( 'r' ) as file
	
	# def load_xyz ( filepath )

	@staticmethod
	def merge ( meshs, default_color=[128,128,128,255], **kwargs ):

		default_color  = numpy.asarray( default_color, dtype=numpy.float32 ) / 255.
		default_color  = numpy.reshape( default_color, [1, 4] )
		default_normal = numpy.asarray( [0., 0., 0.], dtype=numpy.float32 ) / 255.
		default_normal = numpy.reshape( default_normal, [1, 3] )

		merge_primitive         = meshs[ 0 ].primitive
		merge_has_colors        = False
		merge_has_normals       = False
		merge_number_of_points  = 0
		merge_number_of_indexes = 0
		merge_primitive_depth   = meshs[ 0 ].indexes.shape[ 1 ] if meshs[ 0 ].indexes is not None else 0

		# First run through the meshs
		for mesh in meshs:
			# Checks primitives are consistant
			if ( mesh.primitive != merge_primitive ):
				raise ValueError( "Mesh '{}'' and '{}' have different primitives: '{}'!='{}'".format(
					meshs[ 0 ].name,
					mesh.name,
					merge_primitive,
					mesh.primitive
					) )
			# Stats
			merge_has_colors         = merge_has_colors  or mesh.colors  is not None
			merge_has_normals        = merge_has_normals or mesh.normals is not None
			merge_number_of_points  += mesh.points.shape[ 0 ]
			merge_number_of_indexes += mesh.indexes.shape[ 0 ] if mesh.indexes is not None else 0

		# Second run throught to build the merged mesh
		merge_points  = numpy.empty( [merge_number_of_points, 3], dtype=numpy.float32 )
		merge_colors  = numpy.empty( [merge_number_of_points, 4], dtype=numpy.float32 ) if merge_has_colors  else None
		merge_normals = numpy.empty( [merge_number_of_points, 3], dtype=numpy.float32 ) if merge_has_normals else None
		merge_indexes = numpy.empty( [merge_number_of_indexes, merge_primitive_depth], dtype=numpy.uint32 ) if merge_number_of_indexes > 0 else None
		offset_vertices = 0
		offset_indexes  = 0
		for mesh in meshs:
			# Number of vertices and primitives in the mesh
			number_of_points  = mesh.points.shape[ 0 ]
			number_of_indexes = mesh.indexes.shape[ 0 ] if mesh.indexes is not None else 0

			# Copy vertices
			merge_points[ offset_vertices : offset_vertices+number_of_points, : ] = mesh.points
			

			# Copy colors if needed
			if merge_has_colors:
				merge_colors[ offset_vertices : offset_vertices+number_of_points, : ] = mesh.colors if mesh.colors is not None else numpy.tile( default_color, [number_of_points, 1] )
			
			# Copy normals if needed
			if merge_has_normals:
				merge_normals[ offset_vertices : offset_vertices+number_of_points, : ] = mesh.normals if mesh.normals is not None else numpy.tile( default_normal, [number_of_points, 1] )
			
			# Copy indexes and offset them by the number of vertices already in the merged mesh
			if merge_number_of_indexes > 0:
				merge_indexes[ offset_indexes : offset_indexes+number_of_indexes ] = mesh.indexes + offset_vertices

			# move offsets
			offset_vertices += number_of_points
			offset_indexes  += number_of_indexes

		return MeshData( merge_primitive, merge_points, merge_colors, merge_normals, merge_indexes, **kwargs )

	# def merge ( ... )

# class MeshData