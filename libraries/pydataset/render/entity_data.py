# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###             CLASS ENTITY-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'EntityData',
	   namespace = 'pydataset.render',
	fields_names = [
		'name',
		'enabled',
		'local_transform',
		'children_data'
		]
	)
class EntityData ( pytools.serialization.Serializable ):
	"""
	Class `pydataset.render.EntityData`
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, children_data=[], local_transform=[1.0, 0.0, 0.0, 0.0,  0.0, 1.0, 0.0, 0.0,  0.0, 0.0, 1.0, 0.0,  0.0, 0.0, 0.0, 1.0], enabled=True, name='' ):
		"""
		Initializes a new instance of the `pydataset.render.EntityData` class.

		Arguments:
			self                    (`pydataset.render.EntityData`): Instance to initialize.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( children_data, list )
		assert pytools.assertions.list_items_type_is( children_data, EntityData )
		assert pytools.assertions.type_is( enabled, bool )
		assert pytools.assertions.type_is( local_transform, list )
		assert pytools.assertions.equal( len(local_transform), 16 )
		assert pytools.assertions.list_items_type_is( local_transform, float )
		assert pytools.assertions.type_is( name, str )

		super( EntityData, self ).__init__()

		self._children_data   = list( children_data )
		self._enabled         = enabled
		self._local_transform = list( local_transform )
		self._name            = name if name else type(self).__name__

	# def __init__ ( self, children_data, local_transform, enabled, name )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def children_data ( self ):
		"""
		Data of the children of this entity (`list` of `pydataset.render.EntityData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )

		return self._children_data
	
	# def children_data ( self )

	# --------------------------------------------------

	@children_data.setter
	def children_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, EntityData )

		self._children_data = value
	
	# def children_data ( self, value )

	# --------------------------------------------------

	@property
	def enabled ( self ):
		"""
		Enabled / disabled boolean state (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )

		return self._enabled
	
	# def enabled ( self )
	
	# --------------------------------------------------

	@enabled.setter
	def enabled ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( value, bool )

		self._enabled = value
	
	# def enabled ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this entity (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )

		return self._name
	
	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def local_transform ( self ):
		"""
		Transformation from the parent entity space to this entity's space (`list` of 16 `float`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )

		return self._local_transform

	# def local_transform ( self )

	# --------------------------------------------------

	@local_transform.setter
	def local_transform ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.equal( len(value), 16 )
		assert pytools.assertions.list_items_type_is( value, float )

		self._local_transform = value

	# def local_transform ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_child ( self, child_data ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is_instance_of( child_data, EntityData )

		self._children_data.append( child_data )

	# def add_child ( self, child_data )

	# --------------------------------------------------

	def get_child ( self, name ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( name, str )

		for child_data in self._children_data:
			if child_data.name == name:
				return child_data
		return None

	# def get_child ( self, name )

	# --------------------------------------------------

	def get_child_at ( self, index ):
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is( index, int )

		return self._children_data[ index ]

	# def get_child_at ( self, index )
	
	# --------------------------------------------------

	def remove_child ( self, child ):
		"""
		Removes a child from this entity.

		Arguments:
			self  (`pydataset.render.EntityData`): Parent entity.
			child (`pydataset.render.EntityData`): Child entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, EntityData )
		assert pytools.assertions.type_is_instance_of( child, EntityData )

		child._parent = None
		self._children_data.remove( child )

	# def remove_child ( self, child )

# class EntityData ( pytools.serialization.Serializable )