# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.tasks
import pyui.widgets

# LOCALS
from .renderer_settings import RendererSettings
from .scene             import Scene
from .viewport          import Viewport

# ##################################################
# ###               CLASS RENDERER               ###
# ##################################################

class Renderer ( pytools.tasks.Task ):
	"""
	Dataset renderer base class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings=RendererSettings(), logger=None, progress_tracker=None ):
		"""
		Initializes a new instance of the `pydataset.render.Renderer` class.

		Arguments:
			self                        (`pydataset.render.Renderer`): Instance to initialize.
			settings            (`pydataset.render.RendererSettings`): Settings of the renderer.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to report the activity of the renderer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to display progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( settings, RendererSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Renderer, self ).__init__( settings, logger, progress_tracker )

		self._application = None
		self._scene = None
		self._timer = None
		self._viewport = None

	# def __init__ ( self, settings, logger, progress_tracker )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def application ( self ):
		"""
		Appication runned by this renderer (`pyui.widgets.Application`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self._application

	# def application ( self )

	# --------------------------------------------------
	
	@property
	def scene ( self ):
		"""
		Scene rendered by this renderer (`pydataset.render.Scene`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self._scene

	# def scene ( self )

	# --------------------------------------------------
	
	@scene.setter
	def scene ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Scene) )

		self._scene = value
		
		if self._viewport:
			self._viewport.scene = value

	# def scene ( self, value )

	# --------------------------------------------------
	
	@property
	def timer ( self ):
		"""
		Timer used to go through each eample to render periodically (`PyQt5.QtCore.QTimer`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self._timer

	# def get_timer ( self )

	# --------------------------------------------------

	@property
	def viewport ( self ):
		"""
		Viewport used to render and display the scene (`pydataset.render.Viewport`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self._viewport

	# def get_viewport ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def grab_render ( self ):
		"""
		Grabs the render produced by the vieport of this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer of which to grab the render.

		Returns:
			`numpy.ndarray`: The render.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self.viewport.grab_image( *self.settings.size.dimensions )

	# def grab_render ( self )

	# --------------------------------------------------

	def late_update_scene ( self, scene ):
		"""
		Overload this method to update the scene after each render.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to update
			scene   (`pydataset.render.Scene`): Scene to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, Scene )

	# def late_update_scene ( self, scene )

	# --------------------------------------------------

	def late_update_viewport ( self, viewport ):
		"""
		Overload this method to update the viewport after each render.

		Arguments:
			self     (`pydataset.render.Renderer`): Renderer to update
			viewport (`pydataset.render.Viewport`): Viewport to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( viewport, Viewport )
		
	# def late_update_viewport ( self, viewport )

	# --------------------------------------------------

	def must_close ( self ):
		"""
		Overload this method to define when to close this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): The renderer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		return self.state == pytools.tasks.TaskState.stopping

	# def must_close ( self )

	# --------------------------------------------------

	def start ( self ):
		"""
		Start this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		# Start the task
		super( Renderer, self ).start()

		# Create the Qt Application
		self._application = PyQt5.QtWidgets.QApplication( [] )
		
		# Create the scene, the vieport and the timer
		self.setup()
		
	# def start ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		# Start the timer and the graphical application
		self._timer.start( 1000 // self.settings.fps )
		self._application.exec()

	# def run ( self )

	# --------------------------------------------------

	def save_render ( self, render ):
		"""
		Overload this method to save the render.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer that produced the render.
			render           (`numpy.ndarray`): The render.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( render, numpy.ndarray )

		raise NotImplementedError()

	# def save_render ( self, render )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the scene and viewport of this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
			
		self._scene = Scene()
		self.setup_scene( self._scene )

		self._viewport = Viewport( self._scene )
		self.setup_viewport( self._viewport )
		self._viewport.show()

		self._timer = PyQt5.QtCore.QTimer()
		self._timer.timeout.connect( self.update )

	# def setup ( self )

	# --------------------------------------------------

	def setup_scene ( self, scene ):
		"""
		Overload this method to setup the scene of this renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer of which to setup the scene.
			scene   (`pydataset.render.Scene`): Scene to settup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, Scene )
		
	# def setup_scene ( self, scene )

	# --------------------------------------------------

	def setup_viewport ( self, viewport ):
		"""
		Overload this method to setup the viewport of this renderer.

		Arguments:
			self  (`pydataset.render.Renderer`): Renderer of which to setup the scene.
			scene (`pydataset.render.Viewport`): Viewport to settup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( viewport, Viewport )

		viewport.setWindowTitle( self.settings.name )
		viewport.setMinimumSize( *self.settings.size.dimensions )
		viewport.setMaximumSize( *self.settings.size.dimensions )

	# def setup_viewport ( self, vieport )

	# --------------------------------------------------

	def update ( self ):
		"""
		Method invoked periodically by the timer to update the renderer.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )

		# Update the task
		super( Renderer, self ).update()

		# Close ?
		if self.must_close():
			self.viewport.close()
			return

		# Update the scene and the viewport
		self.update_scene( self._scene )
		self.update_viewport( self._viewport )

		# Repaint the viewport / gl
		self._viewport.repaint()

		# Grab and save the render
		if not self.settings.is_dry_running:
			render = self.grab_render()
			self.save_render( render )

		self.late_update_scene( self._scene )
		self.late_update_viewport( self._viewport )

	# def update ( self )

	# --------------------------------------------------

	def update_scene ( self, scene ):
		"""
		Overload this method to update the scene before each render.

		Arguments:
			self (`pydataset.render.Renderer`): Renderer to update
			scene   (`pydataset.render.Scene`): Scene to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( scene, Scene )

	# def update_scene ( self, scene )

	# --------------------------------------------------

	def update_viewport ( self, viewport ):
		"""
		Overload this method to update the viewport before each render.

		Arguments:
			self     (`pydataset.render.Renderer`): Renderer to update
			viewport (`pydataset.render.Viewport`): Viewport to update
		"""
		assert pytools.assertions.type_is_instance_of( self, Renderer )
		assert pytools.assertions.type_is_instance_of( viewport, Viewport )
		
	# def update_viewport ( self, viewport )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a new renderer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pydataset.render.RendererSettings`: The settings.
		"""
		return RendererSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Renderer