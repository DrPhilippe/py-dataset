# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtGui

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def affine_transform ( R, t=numpy.zeros([3],dtype=numpy.float32) ):
	"""
	Converts the given `numpy.ndarray` to a `PyQt5.QtGui.QMatrix4x4`.
	
	The numpy ndarray must be of dtype float32 and shape [3, 3] and [3] respectively.
	The first is a rotation matrix and the second a translation vector.

	Arguments:
		ndarray (`numpy.ndarray`): NumPy n-dimensional array to convert.

	Returns:
		`PyQt5.QtGui.QMatrix4x4`: The matrix.
	"""
	T = numpy.eye( 4, dtype=numpy.float32 )
	T[:3, :3] = R
	T[:3,  3] = t.ravel()
	return ndarray_to_qmatrix4x4( T )

# def affine_transform ( R, t )

# --------------------------------------------------

def invert_xy ():
	"""
	Transform that invberts the X and Y axis.

	Returns:
		`PyQt5.QtGui.QMatrix4x4`: The transform matrix.
	"""
	T = numpy.eye( 4, dtype=numpy.float32 )
	T[1, 1] = -1
	T[2, 2] = -1
	return ndarray_to_qmatrix4x4( T )

# def invert_xy ()

# --------------------------------------------------

def ndarray_to_qmatrix4x4 ( ndarray ):
	"""
	Converts a `numpy.ndarray` of a 4x4 matrix to a `PyQt5.QtGui.QMatrix4x4`.
	
	The numpy ndarray must be of dtype float32 and shape [4, 4].

	Arguments:
		ndarray (`numpy.ndarray`): NumPy n-dimensional array to convert.

	Returns:
		`PyQt5.QtGui.QMatrix4x4`: The converted matrix.
	"""
	assert pytools.assertions.type_is( ndarray, numpy.ndarray )
	assert pytools.assertions.equal( ndarray.dtype, numpy.dtype('float32') )
	assert pytools.assertions.equal( list(ndarray.shape), [4, 4] )

	return PyQt5.QtGui.QMatrix4x4( ndarray.ravel() ).transposed()

# def ndarray_to_qmatrix4x4 ( ndarray )

# --------------------------------------------------

def ndarray_to_qmatrix3x3 ( ndarray ):
	"""
	Converts a `numpy.ndarray` of a 3x3 matrix to a `PyQt5.QtGui.QMatrix3x3`.
	
	The numpy ndarray must be of dtype float32 and shape [3, 3].

	Arguments:
		ndarray (`numpy.ndarray`): NumPy n-dimensional array to convert.

	Returns:
		`PyQt5.QtGui.QMatrix3x3`: The converted matrix.
	"""
	assert pytools.assertions.type_is( ndarray, numpy.ndarray )
	assert pytools.assertions.equal( ndarray.dtype, numpy.dtype('float32') )
	assert pytools.assertions.equal( list(ndarray.shape), [3, 3] )

	return PyQt5.QtGui.QMatrix3x3( ndarray.ravel() )

# def ndarray_to_qmatrix3x3 ( ndarray )