# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .rectangle_camera_data import RectangleCameraData

# ##################################################
# ###        CLASS ORTHOGONAL-CAMERA-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OthogonalCameraData',
	   namespace = 'pydataset.render.cameras',
	fields_names = []
	)
class OrthogonalCameraData ( RectangleCameraData ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.data.OrthogonalCameraData` class.

		Arguments:
			self (`pydataset.data.OrthogonalCameraData`): Instance to initialize.

		Named Arguments:
			x1                                            (`float`): X coordinate of the camera screen top-left corner.
			y1                                            (`float`): Y coordinate of the camera screen top-left corner.
			x2                                            (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                            (`float`): Y coordinate of the camera screen bottom-right corner.
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, OrthogonalCameraData )

		super( OrthogonalCameraData, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

# class OrthogonalCameraData ( RectangleCameraData )