# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtGui

# INTERNALS
import pytools.assertions

# LOCALS
from ..entity import Entity
from .opencv_camera_data import OpenCVCameraData
from .rectangle_camera import RectangleCamera

# ##################################################
# ###            CLASS OPENCV-CAMERA             ###
# ##################################################

class OpenCVCamera ( RectangleCamera ):
	"""
	Camera with intrinsic parameters.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.cameras.OpenCVCamera` class.

		Arguments:
			self     (`pydataset.render.cameras.OpenCVCamera`): Instance to initialize.
			data (`pydataset.render.cameras.OpenCVCameraData`): Data of this camera.
			parent          (`pydataset.render.Entity`/`None`): Parent entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )
		assert pytools.assertions.type_is_instance_of( data, OpenCVCameraData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( OpenCVCamera, self ).__init__( data, parent )

	# def __init__ ( self, name, parent )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def get_projection_matrix ( self ):
		"""
		Computes the projection matrix of this camera.

		Arguments:
			self (`pydataset.render.OpenCVCamera`): The camera.

		Returns:
			`PyQt5.QtGui.QMatrix4x4`: The projection matrix.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )
		
		# Cliping
		depth =        (self.data.far_clip - self.data.near_clip)
		q     =       -(self.data.far_clip + self.data.near_clip) / depth
		qn    = -2.0 * (self.data.far_clip * self.data.near_clip) / depth

		# Locals
		K = PyQt5.QtGui.QMatrix3x3( self.data.K )
		K00 = K[ 0, 0 ]
		K01 = K[ 0, 1 ]
		K02 = K[ 0, 2 ]
		K11 = K[ 1, 1 ]
		K12 = K[ 1, 2 ]
		x   = self.data.x1
		y   = self.data.y1
		w   = self.data.width
		h   = self.data.height

		# Draw our images upside down, so that all the pixel-based coordinate
		# systems are the same
		if self.data.window_coordinates == 'y_up':
			return PyQt5.QtGui.QMatrix4x4(
				2.0 * K00 / w, -2.0 * K01 / w, (-2.0 * K02 + w + 2.0 * x) / w, 0.0,
				0.0,           -2.0 * K11 / h, (-2.0 * K12 + h + 2.0 * y) / h, 0.0,
				# This row is standard glPerspective and sets near and far planes
				0.0, 0.0, q,  qn,
				# This row is also standard glPerspective
				0.0, 0.0, -1.0, 0.0
				)


		# Draw the images right side up and modify the projection matrix so that OpenGL
		# will generate window coords that compensate for the flipped image coords
		else:
			return PyQt5.QtGui.QMatrix4x4(
				2.0 * K00 / w, -2.0 * K01 / w, (-2.0 * K02 + w + 2.0 * x) / w, 0.0,
				0.0,            2.0 * K11 / h, ( 2.0 * K12 - h + 2.0 * y) / h, 0.0,
				# This row is standard glPerspective and sets near and far planes
				0.0, 0.0, q,  qn,
				# This row is also standard glPerspective
				0.0, 0.0, -1.0, 0.0
				)

		# if self.window_coordinates == 'y_up'

	# def get_projection_matrix( self )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this camera.

		Arguments:
			self (`pydataset.render.cameras.OpenCVCamera`): Camera to represent as text.

		Returns:
			`str`: The text representation of this camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )

		return '[OpenCVCamera: K={}, window_coordinates={}, top_left=({}, {}), bottom_right=({}, {}), near_clip={}, far_clip={}, name={}, enabled={}, parent={}]'.format(
			self.data.K.data(),
			self.data.window_coordinates,
			self.data.x1,
			self.data.y1,
			self.data.x2,
			self.data.y2,
			self.data.near_clip,
			self.data.far_clip,
			self.data.name,
			self.data.enabled,
			self.parent.name if self.parent else None
			)

	# def __str__ ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new camera.

		Named Arguments:
			k                                (`list` of 9 `float`s): Intrinsic camera parameters matrix.
			window_coordinates                              (`str`): 'y_up' or 'y_down'
			x1                                            (`float`): X coordinate of the camera screen top-left corner.
			y1                                            (`float`): Y coordinate of the camera screen top-left corner.
			x2                                            (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                            (`float`): Y coordinate of the camera screen bottom-right corner.
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.cameras.OpenCVCameraData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return OpenCVCameraData( **kwargs )

	# def create_data ( cls, **kwargs )

# class OpenCVCamera ( OpenCVCamera )