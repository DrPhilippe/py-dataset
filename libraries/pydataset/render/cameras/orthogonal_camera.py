# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtGui

# INTERNALS
import pytools.assertions

# LOCALS
from ..entity import Entity
from .orthogonal_camera_data import OrthogonalCameraData
from .rectangle_camera import RectangleCamera

# ##################################################
# ###                CLASS CAMERA                ###
# ##################################################

class OrthogonalCamera ( RectangleCamera ):
	"""
	Othogonal camera entity.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.cameras.OrthogonalCamera` class.

		Arguments:
			self     (`pydataset.render.cameras.OrthogonalCamera`): Instance to initialize.
			data (`pydataset.render.cameras.OrthogonalCameraData`): Data of this camera.
			parent              (`pydataset.render.Entity`/`None`): Parent entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, OrthogonalCamera )
		assert pytools.assertions.type_is_instance_of( data, OrthogonalCameraData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( OrthogonalCamera, self ).__init__( data, parent )

	# def __init__ ( self, data, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_projection_matrix ( self ):
		"""
		Computes the projection matrix of this camera.

		Arguments:
			self (`pydataset.render.cameras.OrthogonalCamera`): The camera.

		Returns:
			`PyQt5.QtGui.QMatrix4x4`: The projection matrix.
		"""
		assert pytools.assertions.type_is_instance_of( self, OrthogonalCamera )
		
		projection_matrix = PyQt5.QtGui.QMatrix4x4()
		projection_matrix.ortho(
			self.data.x1, # left
			self.data.x2, # right
			self.data.y1, # bottom
			self.data.y2, # top
			self.data.near_clip,
			self.data.far_clip
			)
		return projection_matrix

	# def get_projection_matrix( self )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this orthogonal camera.

		Arguments:
			self (`pydataset.render.cameras.OrthogonalCamera`): Orthogonal camera to represent as text.

		Returns:
			`str`: The text representation of this orthogonal camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, OrthogonalCamera )

		return '[OrthogonalCamera: x1={}, y1={}, x2={}, y2={}, near_clip={}, far_clip={}, name={}, enabled={}, parent={}]'.format(
			self.data.x1,
			self.data.y1,
			self.data.x2,
			self.data.y2,
			self.data.near_clip,
			self.data.far_clip,
			self.data.name,
			self.data.enabled,
			self.parent.name if self.parent else None
			)

	# def __str__ ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new camera.

		Named Arguments:
			x1                                            (`float`): X coordinate of the camera screen top-left corner.
			y1                                            (`float`): Y coordinate of the camera screen top-left corner.
			x2                                            (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                            (`float`): Y coordinate of the camera screen bottom-right corner.
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.cameras.OrthogonalCameraData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return OrthogonalCameraData( **kwargs )

	# def create_data ( cls, **kwargs )

# class OrthogonalCamera ( RectangleCamera )