# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..entity import Entity
from .camera  import Camera
from .rectangle_camera_data import RectangleCameraData

# ##################################################
# ###           CLASS RECTANGLE-CAMERA           ###
# ##################################################

class RectangleCamera ( Camera ):
	"""
	Camera with rectangular screen definition.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.cameras.RectangleCamera` class.

		Arguments:
			self     (`pydataset.render.cameras.RectangleCamera`): Instance to initialize.
			data (`pydataset.render.cameras.RectangleCameraData`): Data of this camera.
			parent             (`pydataset.render.Entity`/`None`): Parent entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCamera )
		assert pytools.assertions.type_is_instance_of( data, RectangleCameraData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( RectangleCamera, self ).__init__( data, parent )

	# def __init__ ( self, data, parent )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this rectangle camera.

		Arguments:
			self (`pydataset.render.cameras.RectangleCamera`): Rectangle camera to represent as text.

		Returns:
			`str`: The text representation of this rectangle camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCamera )

		return '[RectangleCamera: x1={}, y1={}, x2={}, y2={}, near_clip={}, far_clip={}, name={}, enabled={}, parent={}]'.format(
			self.data.x1,
			self.data.y1,
			self.data.x2,
			self.data.y2,
			self.data.near_clip,
			self.data.far_clip,
			self.data.name,
			self.data.enabled,
			self.parent.name if self.parent else None
			)

	# def __str__ ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new camera.

		Named Arguments:
			x1                                            (`float`): X coordinate of the camera screen top-left corner.
			y1                                            (`float`): Y coordinate of the camera screen top-left corner.
			x2                                            (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                            (`float`): Y coordinate of the camera screen bottom-right corner.
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.cameras.RectangleCameraData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return RectangleCameraData( **kwargs )

	# def create_data ( cls, **kwargs )

# class RectangleCamera ( Camera )