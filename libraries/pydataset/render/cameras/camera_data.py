# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..entity_data import EntityData

# ##################################################
# ###             CLASS CAMERA-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CameraData',
	   namespace = 'pydataset.render.cameras',
	fields_names = [
		'near_clip',
		'far_clip'
		]
	)
class CameraData ( EntityData ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, near_clip=0.1, far_clip=1000.0, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.render.cameras.CameraData` class.

		Arguments:
			self (`pydataset.render.cameras.CameraData`): Instance to initialize.
			near_clip                          (`float`): Shortest distance at which entities are rendered.
			far_clip                           (`float`): Farthest distance at which entities are rendered.

		Named Arguments:
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraData )
		assert pytools.assertions.type_is( near_clip, float )
		assert pytools.assertions.type_is( far_clip, float )

		super( CameraData, self ).__init__( **kwargs )

		self._near_clip = near_clip
		self._far_clip  = far_clip

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def near_clip ( self ):
		"""
		Shortest distance at which entities are rendered (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraData )

		return self._near_clip

	# def near_clip ( self )

	# --------------------------------------------------

	@near_clip.setter
	def near_clip ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CameraData )
		assert pytools.assertions.type_is( value, float )

		self._near_clip = value

	# def near_clip ( self, value )

	# --------------------------------------------------

	@property
	def far_clip ( self ):
		"""
		Farthest distance at which entities are rendered (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraData )

		return self._far_clip

	# def far_clip ( self )

	# --------------------------------------------------

	@far_clip.setter
	def far_clip ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CameraData )
		assert pytools.assertions.type_is( value, float )

		self._far_clip = value

	# def far_clip ( self, value )

# class CameraData ( EntityData )