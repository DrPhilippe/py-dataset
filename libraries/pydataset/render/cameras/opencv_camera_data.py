# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .rectangle_camera_data import RectangleCameraData

# ##################################################
# ###          CLASS OPENCV-CAMERA-DATA          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OpenCVCameraData',
	   namespace = 'pydataset.render.cameras',
	fields_names = [
		'K',
		'window_coordinates'
		]
	)
class OpenCVCameraData ( RectangleCameraData ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, K=[572.4114, 0.0, 325.26110,  0.0, 573.57043, 242.04899, 0.0, 0.0, 1.0], window_coordinates='y_down', **kwargs ):
		"""
		Initializes a new instance of the `pydataset.data.OpenCVCameraData` class.

		Arguments:
			self (`pydataset.data.OpenCVCameraData`): Instance to initialize.
			k                 (`list` of 9 `float`s): Intrinsic camera parameters matrix.
			window_coordinates               (`str`): 'y_up' or 'y_down'

		Named Arguments:
			x1                                            (`float`): X coordinate of the camera screen top-left corner.
			y1                                            (`float`): Y coordinate of the camera screen top-left corner.
			x2                                            (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                            (`float`): Y coordinate of the camera screen bottom-right corner.
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraData )
		assert pytools.assertions.type_is( K, list )
		assert pytools.assertions.equal( len(K), 9 )
		assert pytools.assertions.list_items_type_is( K, float )
		assert pytools.assertions.type_is( window_coordinates, str )
		assert pytools.assertions.value_in( window_coordinates, ['y_up', 'y_down'] )

		super( OpenCVCameraData, self ).__init__( **kwargs )

		self._K = list( K )
		self._window_coordinates = window_coordinates
	
	# def __init__ ( self, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def K ( self ):
		"""
		Camera parameters matrix (`PyQt5.QtGui.QMatrix3x3`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraData )

		return self._K

	# def K ( self )

	# --------------------------------------------------

	@K.setter
	def K ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.equal( len(value), 9 )
		assert pytools.assertions.list_items_type_is( value, float )

		self._K = value

	# def K ( self, value )

	# --------------------------------------------------

	@property
	def window_coordinates ( self ):
		"""
		Window coordinate enum (`str`: 'y_down' or 'y_up').
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraData )

		return self._window_coordinates

	# def window_coordinates ( self )

	# --------------------------------------------------

	@window_coordinates.setter
	def window_coordinates ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, ['y_down', 'y_up'] )

		self._window_coordinates = value

	# def window_coordinates ( self, value )

# class OpenCVCameraData ( RectangleCameraData )