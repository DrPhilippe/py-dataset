# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .camera_data import CameraData

# ##################################################
# ###        CLASS RECTANGLE-CAMERA-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RectangleCameraData',
	   namespace = 'pydataset.render.cameras',
	fields_names = [
		'x1',
		'y1',
		'x2',
		'y2'
		]
	)
class RectangleCameraData ( CameraData ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, x1=0.0, y1=0.0, x2=640.0, y2=480.0, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.render.cameras.RectangleCameraData` class.

		Arguments:
			self (`pydataset.render.cameras.RectangleCameraData`): Instance to initialize.
			x1                                          (`float`): X coordinate of the camera screen top-left corner.
			y1                                          (`float`): Y coordinate of the camera screen top-left corner.
			x2                                          (`float`): X coordinate of the camera screen bottom-right corner.
			y2                                          (`float`): Y coordinate of the camera screen bottom-right corner.

		Named Arguments:
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )
		assert pytools.assertions.type_is( x1, float )
		assert pytools.assertions.type_is( y1, float )
		assert pytools.assertions.type_is( x2, float )
		assert pytools.assertions.type_is( y2, float )

		super( RectangleCameraData, self ).__init__( **kwargs )

		self._x1 = x1
		self._y1 = y1
		self._x2 = x2
		self._y2 = y2

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def x1 ( self ):
		"""
		X coordinate of the camera screen top-left corner (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._x1

	# def x1 ( self )

	# --------------------------------------------------

	@x1.setter
	def x1 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )
		assert pytools.assertions.type_is( value, float )

		self._x1 = value

	# def x1 ( self, value )

	# --------------------------------------------------

	@property
	def y1 ( self ):
		"""
		Y coordinate of the camera screen top-left corner (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._y1

	# def y1 ( self )

	# --------------------------------------------------

	@y1.setter
	def y1 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )
		assert pytools.assertions.type_is( value, float )

		self._y1 = value

	# def y1 ( self, value )

	# --------------------------------------------------

	@property
	def x2 ( self ):
		"""
		X coordinate of the camera screen bottom-right corner (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._x2

	# def x2 ( self )

	# --------------------------------------------------

	@x2.setter
	def x2 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )
		assert pytools.assertions.type_is( value, float )

		self._x2 = value

	# def x2 ( self, value )

	# --------------------------------------------------

	@property
	def y2 ( self ):
		"""
		Y coordinate of the camera screen bottom-right corner (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._y2

	# def y2 ( self )

	# --------------------------------------------------

	@y2.setter
	def y2 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )
		assert pytools.assertions.type_is( value, float )

		self._y2 = value

	# def y2 ( self, value )

	# --------------------------------------------------

	@property
	def width ( self ):
		"""
		Width of the camera screen in pixels (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._x2 - self._x1

	# def width ( self )

	# --------------------------------------------------

	@property
	def height ( self ):
		"""
		Height of the camera screen in pixels (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCameraData )

		return self._y2 - self._y1

	# def height ( self )

# class RectangleCameraData ( CameraData )