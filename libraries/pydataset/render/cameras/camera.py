# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..entity import Entity
from .camera_data import CameraData

# ##################################################
# ###                CLASS CAMERA                ###
# ##################################################

class Camera ( Entity ):
	"""
	Camera entity with intrinsic parameters.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Main camera used to display the current scene and all its entities (`pydataset.render.cameras.Camera`).
	"""
	main_camera = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.Camera` class.

		Arguments:
			self     (`pydataset.render.cameras.Camera`): Instance to initialize.
			data (`pydataset.render.cameras.CameraData`): Data of this camera.
			parent    (`pydataset.render.Entity`/`None`): Parent entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )
		assert pytools.assertions.type_is_instance_of( data, CameraData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( Camera, self ).__init__( data, parent )
		
		if Camera.main_camera is None:
			Camera.main_camera = self

	# def __init__ ( self, name, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_projection_matrix ( self ):
		"""
		Computes the projection matrix of this camera.

		Arguments:
			self (`pydataset.render.cameras.Camera`): The camera.

		Returns:
			`PyQt5.QtGui.QMatrix4x4`: The projection matrix.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )
		
		raise NotImplementedError()

	# def get_projection_matrix( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this camera.

		Arguments:
			self (`pydataset.render.cameras.Camera`): Camera to represent as text.

		Returns:
			`str`: The text representation of this camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )
		
		return '[Camera: near_clip={}, far_clip={}, name={}, enabled={}, parent={}]'.format(
			self.data.near_clip,
			self.data.far_clip,
			self.data.name,
			self.data.enabled,
			self.parent.name if self.parent is not None else None
			)

	# def __str__ ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new camera.

		Named Arguments:
			near_clip                                     (`float`): Shortest distance at which entities are rendered.
			far_clip                                      (`float`): Farthest distance at which entities are rendered.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.cameras.CameraData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return CameraData( **kwargs )

	# def create_data ( cls, **kwargs )

# class Camera ( Entity )