# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtGui

# INTERNALS
import pytools.assertions
import pytools.text

# LOCALS
from .entity_data import EntityData
from .register_entity_attribute import RegisterEntityAttribute

# ##################################################
# ###                CLASS ENTITY                ###
# ##################################################

@RegisterEntityAttribute( 'pydataset.render.Entity' )
class Entity:
	"""
	Base class for scene entities, includinbg the scene.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=EntityData(), parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.Entity` class.

		Arguments:
			self          (`pydataset.render.Entity`): Instance to initialize.
			data      (`pydataset.render.EntityData`): Data of this entity.
			parent (`pydataset.render.Entity`/`None`): Parent entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is_instance_of( data, EntityData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		self._data     = data
		self._changed  = True
		self._children = []
		self._parent   = parent

		if parent is not None and self.data not in parent.data.children_data:
			parent.add_child( self )

	# def __init__ ( self, name, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data of this entity (`pydataset.render.EntityData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self._data
	
	# def data ( self )

	# --------------------------------------------------

	@property
	def changed ( self ):
		"""
		Boolean indicating if this entity has changed (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self._changed
	
	# def changed ( self )
	
	# --------------------------------------------------

	@changed.setter
	def changed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is( value, bool )

		self._changed = value
	
	# def changed ( self, value )

	# --------------------------------------------------

	@property
	def children ( self ):
		"""
		Children of this entity (`list` of `pydataset.render.Entity`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self._children
	
	# def children ( self )
	
	# --------------------------------------------------

	@property
	def enabled ( self ):
		"""
		Boolean indicating if this emtity is enabled (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self.data.enabled
	
	# def enabled ( self )
	
	# --------------------------------------------------

	@enabled.setter
	def enabled ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is( value, bool )

		self.data.enabled = value
		self._changed     = True
	
	# def enabled ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this entity (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self.data.name
	
	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is( value, str )

		self.data.name = value
		self._changed  = True
	
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent of this entity (`pydataset.render.Entity`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return self._parent
	
	# def parent ( self )
	
	# --------------------------------------------------

	@parent.setter
	def parent ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Entity) )

		self._parent  = value
		self._changed = True
	
	# def parent ( self, value )

	# --------------------------------------------------

	@property
	def local_transform ( self ):
		"""
		Transformation from the parent entity space to this entity's space (`PyQt5.QtGui.QMatrix4x4`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		return PyQt5.QtGui.QMatrix4x4( self.data.local_transform )

	# def local_transform ( self )

	# --------------------------------------------------

	@local_transform.setter
	def local_transform ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is( value, PyQt5.QtGui.QMatrix4x4 )

		self.data.local_transform = list( value.data() )

	# def local_transform ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def add_child ( self, child ):
		"""
		Adds a child to this entity.

		Arguments:
			self  (`pydataset.render.Entity`): Parent entity.
			child (`pydataset.render.Entity`): Child entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is_instance_of( child, Entity )

		child._parent = self
		self._children.append( child )
		self.data.children_data.append( child.data )
		self._changed = True

	# def add_child ( self, child )

	# --------------------------------------------------

	def clear ( self ):
		"""
		Clears the children of this entity.

		Arguments:
			self (`pydataset.render.Entity`): The entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		count = len( self._children )
		for i in reversed( range( count ) ):
			self._children[ i ].delete()
			del self._children[ i ]
		del self._children
		self._children = []
		self._changed = True

	# def clear ( self )
	
	# --------------------------------------------------

	def delete ( self ):
		"""
		Deletes this this entity.

		Arguments:
			self (`pydataset.render.Entity`): The entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		
		self.clear()
		del self._data

	# def delete ( self )

	# --------------------------------------------------

	def get_child ( self, index_or_name ):
		"""
		Get a child of this entity.

		Arguments:
			self (`pydataset.render.Entity`): Parent entity.
			index_or_name      (`int`/`str`): Name of index of the child entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_in( index_or_name, (int, str) )

		# If the user asked a child by name
		# Search in the list of children
		if isinstance( index_or_name, str ):

			# Get the index of the child
			child_index = self.get_index_of_child( index_or_name )
			
			# Check it was found
			if child_index == -1:
				raise IndexError(
					'The entity {} does not contains the a child named {}'.format( self._name, index_or_name )
					)

		# If the user asked a child by index, use the index
		else:
			child_index = index_or_name

		# if isinstance( index_or_name, str )

		return self.children[ child_index ]

	# def get_child ( self, index_or_name )

	# --------------------------------------------------

	def get_index_of_child ( self, name ):
		"""
		Get a child of this entity.

		Arguments:
			self (`pydataset.render.Entity`): Parent entity.
			name                     (`str`): Name of the child entity.

		Returns:
			`int`: The index of the child if found, otherwise `-1`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is( name, str )

		# Index of the found children
		child_index = -1
		
		# Go through children
		number_of_children = self.get_number_of_children()
		for index in range(number_of_children):
			
			# Get the child
			child = self.children[ index ]

			# Check its name
			if child.name == name:
				child_index = index
		
		# for index in range(number_of_children)

		return child_index

	# def get_index_of_child ( self, name )
		
	# --------------------------------------------------

	def get_number_of_children ( self ):
		"""
		Returns the number of children of this entity.

		Arguments:
			self  (`pydataset.render.Entity`): This entity.

		Returns:
			`int`: Number of children.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		
		return len( self._children )

	# def get_number_of_children ( self )

	# --------------------------------------------------

	def get_global_transform ( self ):
		"""
		Computes the transformation from the root scene to this entity.

		Arguments:
			self (`pydataset.render.Entity`): The entity of which to get the global transfom.

		Returns:
			`PyQt5.QtGui.QMatrix4x4`: The global transform.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		t = self.local_transform
		p = self.parent
		while p is not None:
			t = p.local_transform * t
			p = p.parent
		return t

	# def get_global_transform ( self )

	# --------------------------------------------------

	def paint ( self ):
		"""
		Paints this entity.

		Arguments:
			self (`pydataset.render.Entity`): Entity to paint.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		if self._changed:
			self.update()

		for child in self._children:
			if child.enabled:
				child.paint()

	# def paint ( self )
	
	# --------------------------------------------------

	def remove_child ( self, child ):
		"""
		Removes a child to this entity.

		Arguments:
			self  (`pydataset.render.Entity`): Parent entity.
			child (`pydataset.render.Entity`): Child entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		assert pytools.assertions.type_is_instance_of( child, Entity )

		child._parent = None
		self._children.remove( child )
		self.data.children_data.remove( child.data )
		del child.data
		del child
		self._changed = True

	# def remove_child ( self, child )

	# --------------------------------------------------

	def translate ( self, dx=0.0, dy=0.0, dz=0.0 ):

		t = self.local_transform
		# t = t.transposed()
		t.translate( dx, dy, dz )
		t = t.transposed()
		self.local_transform = t

	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this entity.

		This method is called before painting this entity if it changed.
		
		Arguments:
			self (`pydataset.render.Entity`): Entity to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )

		for child in self._children:
			if child.enabled:
				child.update()

		self._changed = False

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this entity.

		Arguments:
			self (`pydataset.render.Entity`): Entity to represent as text.

		Returns:
			`str`: The text representation of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, Entity )
		
		return '[Entity: name={}, enabled={}, parent={}, children={}]'.format(
			self.name,
			self.enabled,
			self.parent.name if self.parent is not None else None,
			pytools.text.list_to_str( [child.name for child in self._children])
			)

	# def __str__ ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, parent=None, **kwargs ):
		"""
		Creates a new instance of `pydataset.render.Entity` class.

		Arguments:
			cls                       (`type`): Type of entity to create.
			parent (`pydataset.render.Entity`): Parent entity.

		Named Arguments:
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.Entity`: The new entity.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		# Create the data
		data = cls.create_data( **kwargs )

		# Create the entity
		return cls( data, parent )

	# def create ( cls, parent, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new entity.

		Named Arguments:
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.EntityData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return EntityData( **kwargs )

	# def create_data ( cls, **kwargs )

# class Entity