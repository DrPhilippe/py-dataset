# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .entity_data import EntityData

# ##################################################
# ###              CLASS SCENE-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SceneData',
	   namespace = 'pydataset.render',
	fields_names = []
	)
class SceneData ( EntityData ):
	"""
	Class `pydataset.render.SceneData`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.render.SceneData` class.

		Arguments:
			self (`pydataset.render.SceneData`): Instance to initialize.

		Named Arguments:
			self                    (`pydataset.render.EntityData`): Instance to initialize.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneData )

		super( SceneData, self ).__init__( **kwargs )

	# def __init__ ( self )
	
# class SceneData ( pytools.serialization.Serializable )