# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .entity_factory import EntityFactory

# ##################################################
# ###      CLASS REGISTER-ENTITY-ATTRIBUTE       ###
# ##################################################

class RegisterEntityAttribute ( pytools.factory.RegisterClassAttribute ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, typename ):
		assert pytools.assertions.type_is_instance_of( self, RegisterEntityAttribute )
		assert pytools.assertions.type_is( typename, str )

		super( RegisterEntityAttribute, self ).__init__(
			   group = EntityFactory.group_name,
			typename = typename
			)

	# def __init__ ( self, typename )

# class RegisterEntityAttribute ( pytools.factory.RegisterClassAttribute )