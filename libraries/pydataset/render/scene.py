# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.text

# LOCALS
from .entity import Entity
from .cameras import Camera
from .register_entity_attribute import RegisterEntityAttribute
from .scene_data import SceneData

# LOCALS

# ##################################################
# ###                CLASS SCENE                 ###
# ##################################################

@RegisterEntityAttribute( 'pydataset.render.Scene' )
class Scene ( Entity ):
	"""
	The scene is the root entity.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data=SceneData(), camera=None ):
		"""
		Initializes a new instance of the `pydataset.render.Scene` class.

		Arguments:
			self                   (`pydataset.render.Scene`): Instance to initialize.
			camera (`pydataset.render.cameras.Camera`/`None`): Camera used to view this scene.
			name                                      (`str`): Name of this scene.
		"""
		assert pytools.assertions.type_is_instance_of( self, Scene )
		assert pytools.assertions.type_is_instance_of( data, SceneData )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )

		super( Scene, self ).__init__( data, None )

		self._camera = camera

	# def __init__ ( self, data, camera )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def camera ( self ):
		"""
		Camera used to view this scene (`pydataset.render.cameras.Camera`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Scene )

		return self._camera
	
	# def camera ( self )
	
	# --------------------------------------------------

	@camera.setter
	def camera ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Scene )
		assert pytools.assertions.type_is_instance_of( value, (Camera, type(None)) )

		self._camera = value
	
	# def camera ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def clear ( self ):

		del self._camera
		self._camera = None
		
		super( Scene, self ).clear()

	# def clear ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this scene.

		Arguments:
			self (`pydataset.render.Scene`): Scene to represent as text.

		Returns:
			`str`: The text representation of this scene.
		"""
		return '[Scene: name={}, enabled={}, camera={}, children={}]'.format(
			self.camera.name if self.camera else 'None',
			self.name,
			self.enabled,
			pytools.text.list_to_str( [child.name for child in self._children])
			)
	
	# def __str__ ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new entity.

		Named Arguments:
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.SceneData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return SceneData( **kwargs )

	# def create_data ( cls, **kwargs )
	
# class Scene ( Entity )