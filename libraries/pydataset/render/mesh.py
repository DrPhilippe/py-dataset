# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import OpenGL.GL
import PyQt5.QtGui

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.path

# LOCALS
from .entity import Entity
from .cameras import Camera
from .mesh_data import MeshData
from .register_entity_attribute import RegisterEntityAttribute
from .shaders import ShaderProgram, ColorShaderProgram

# ##################################################
# ###                 CLASS MESH                 ###
# ##################################################

@RegisterEntityAttribute( 'pydataset.render.Mesh' )
class Mesh ( Entity ):
	"""
	Mesh entity.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, shader_program=None, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.Mesh` class.

		Arguments:
			self                                   (`pydataset.render.Mesh`): Instance to initialize.
			data                               (`pydataset.render.MeshData`): Mesh data.
			shader_program (`pydataset.render.shaders.ShaderProgram`/`None`): Shader program used to render the mesh.
			parent                        (`pydataset.render.Entity`/`None`): Parent of the new mesh. 
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )
		assert pytools.assertions.type_is_instance_of( data, MeshData )
		assert pytools.assertions.type_is_instance_of( shader_program, (type(None), ShaderProgram) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( Mesh, self ).__init__( data, parent )

		# buffers
		self._vertex_data   = None
		self._index_data    = None
		self._vertex_buffer = PyQt5.QtGui.QOpenGLBuffer( PyQt5.QtGui.QOpenGLBuffer.VertexBuffer )
		self._index_buffer  = PyQt5.QtGui.QOpenGLBuffer( PyQt5.QtGui.QOpenGLBuffer.IndexBuffer  )
		
		# shader program
		self._shader_program = shader_program if shader_program else ColorShaderProgram()
		self._created        = False

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def shader_program ( self ):
		"""
		Shader program used to render the mesh (`pydataset.render.shaders.ShaderProgram`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )
		
		return self._shader_program
	
	# def shader_program ( self )

	# --------------------------------------------------

	@shader_program.setter
	def shader_program ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Mesh )
		assert pytools.assertions.type_is_instance_of( shader_program, (type(None), ShaderProgram) )

		self._shader_program = value
		self._created        = False
		self._changed        = True

	# def shader_program ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _build_shader_program ( self ):
		"""
		Builds the internal shader program.

		Arguments:
			self (`pydataset.render.Shader`): Shader whoes internal shader program to create.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )

		self._shader_program.build()

	# def _build_shader_program ( self )

	# --------------------------------------------------

	def _create_buffers ( self ):
		"""
		Creates the vertex and index buffer of this mesh.

		Arguments:
			self (`pydataset.render.MeshBuffer`): MeshBuffer whoes buffers to create.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )

		if not self._vertex_buffer.create():
			raise RuntimeError(
				'Failed to create vertex buffer of mesh {}'.format( self.name )
				)

		if not self._index_buffer.create():
			raise RuntimeError(
				'Failed to create index buffer of mesh {}'.format( self.name )
				)

	# def _create_buffers ( self )

	# --------------------------------------------------

	def _upload_index_data ( self ):
		"""
		Uploads the mesh index data into the index buffer.

		Arguments:
			self (`pydataset.render.MeshBuffer`): Mesh whoes vertex to upload.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )
		
		# Bind the index buffer
		if not self._index_buffer.bind():
			raise RuntimeError(
				'Failed to bind index buffer of mesh {}'.format( self.name )
				)

		# Get index data
		self._index_data = self.data.get_index_data()

		# Allocate the buffer and copy the indexes
		self._index_buffer.allocate( self._index_data, self._index_data.nbytes )

		# Release the index buffer
		self._index_buffer.release()

	# def _upload_index_data ( self )

	# --------------------------------------------------

	def _upload_vertex_data ( self ):
		"""
		Uploads the mesh vertex data into the vertex buffer.

		Arguments:
			self (`pydataset.render.MeshBuffer`): Mesh whoes vertex to upload.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )
		
		# Bind the vertex buffer
		if not self._vertex_buffer.bind():
			raise RuntimeError(
				'Failed to bind vertex buffer of mesh {}'.format( self.name )
				)

		# Get vertex data
		self._vertex_data = self.data.get_vertex_data()

		# Allocate the buffer and copy the vertices
		self._vertex_buffer.allocate( self._vertex_data, self._vertex_data.nbytes )

		# Release the vertex buffer
		self._vertex_buffer.release()

	# def _upload_vertex_data ( self )

	# --------------------------------------------------

	def paint ( self ):
		"""
		Draws this mesh.

		Arguments:
			self (`pydataset.render.MeshBuffer`): Mesh to paint.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )

		# If not camera, no render
		if not Camera.main_camera:
			return

		# Bind the shader program and the buffers
		if not self._shader_program.bind():	raise RuntimeError( 'Failed to bind shader program of mesh {}'.format(self.name) )
		if not self._vertex_buffer.bind(): raise RuntimeError( 'Failed to bind vertex buffer of mesh {}'.format(self.name) )
		if not self._index_buffer.bind(): raise RuntimeError( 'Failed to bind index buffer of mesh {}'.format(self.name) )
		
		# Vertex/Index data constants
		stride        = self._vertex_data.strides[ 0 ]
		point_offset  = 0
		point_size    = self._vertex_data[ 'point' ].itemsize
		color_offset  = point_offset + self._vertex_data.dtype[ 'point' ].itemsize
		color_size    = self._vertex_data[ 'color' ].itemsize
		normal_offset = color_offset + self._vertex_data.dtype[ 'color' ].itemsize
		normal_size   = self._vertex_data[ 'normal' ].itemsize

		# Set vertex position data layout
		self._shader_program.enableAttributeArray( 'a_position' )
		self._shader_program.setAttributeBuffer( 'a_position', OpenGL.GL.GL_FLOAT, point_offset, point_size, stride )
		
		# Set vertex color data layout
		self._shader_program.enableAttributeArray( 'a_color' )
		self._shader_program.setAttributeBuffer( 'a_color', OpenGL.GL.GL_FLOAT, color_offset, color_size, stride )

		# Set vertex normal data layout
		self._shader_program.enableAttributeArray( 'a_normal' )
		self._shader_program.setAttributeBuffer( 'a_normal', OpenGL.GL.GL_FLOAT, normal_offset, normal_size, stride )

		# Set view matrix
		mv = self.get_global_transform()
		p  = Camera.main_camera.get_projection_matrix()
		mvp = p * mv
		self._shader_program.setUniformValue( 'u_mvp', mvp )
		self._shader_program.setUniformValue( 'u_mv', mv )

		# Set light position and ambiant weight
		self._shader_program.setUniformValue( 'u_light_eye_pos', PyQt5.QtGui.QVector3D( 0.0, 0.0, 0.0) )
		self._shader_program.setUniformValue( 'u_light_ambient_w', 0.1 )

		# Primitive
		primitive = self.data.primitive
		if primitive == 'points':
			primitive = OpenGL.GL.GL_POINTS
		elif primitive == 'lines':
			primitive = OpenGL.GL.GL_LINES
		elif primitive == 'triangles':
			primitive = OpenGL.GL.GL_TRIANGLES

		# Draw
		count = self._index_data[ 'index' ].size
		OpenGL.GL.glDrawElements( primitive, count, OpenGL.GL.GL_UNSIGNED_INT, None )
		self._shader_program.disableAttributeArray( 'a_position' )
		self._shader_program.disableAttributeArray( 'a_color' )
		self._shader_program.disableAttributeArray( 'a_normal' )

		self._index_buffer.release()
		self._vertex_buffer.release()
		self._shader_program.release()

		super( Mesh, self ).paint()

	# def paint ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this mesh.

		This method is called before painting this mesh if it changed.
		
		Arguments:
			self (`pydataset.render.Mesh`): Mesh to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mesh )

		if self.enabled:
			if not self._created:
				self._build_shader_program()
				self._create_buffers()
				self._upload_vertex_data()
				self._upload_index_data()
				self._created = True

		super( Mesh, self ).update()

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this mesh.

		Arguments:
			self (`pydataset.render.Mesh`): Mesh to represent as text.

		Returns:
			`str`: The text representation of this mesh.
		"""
		return '[Mesh: points={}, colors={}, normals={}, indexes={}, primitive={}, name={}, enabled={}, parent={}]'.format(
			'{}x{}'.format( *self.data.points.shape ),
			'{}x{}'.format( *self.data.colors.shape ) if self.data.colors is not None else 'None',
			'{}x{}'.format( *self.data.normals.shape ) if self.data.normals is not None else 'None',
			'{}x{}'.format( *self.data.indexes.shape ),
			self.data.primitive,
			self.data.name,
			self.data.enabled,
			self.parent.name if self.parent is not None else None
			)
	
	# def __str__ ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_data ( cls, **kwargs ):
		"""
		Creates the data for a new mesh.

		Named Arguments:
			primitive                                       (`str`): Primitive type.
			points                         (`None`/`numpy.ndarray`): Vertex positions.
			colors                         (`None`/`numpy.ndarray`): Vertex colors.
			normals                        (`None`/`numpy.ndarray`): Vertex normals.
			indexes                        (`None`/`numpy.ndarray`): Indexes.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.

		Returns:
			`pydataset.render.EntityData`: The new entity data.
		"""
		assert pytools.assertions.type_is( cls, type )

		return MeshData( **kwargs )

	# def create_data ( cls, **kwargs )

	# --------------------------------------------------

	@classmethod
	def load_ply ( cls, filepath, shader_program=None, parent=None ):
		"""
		Creates a new mesh instance by loading it from a ply file.

		Arguments:
			cls                                                     (`type`): Type of mesh to create.
			filepath                         (`str`/`pytools.path.FilePath`): Path of the ply file to load.
			shader_program (`pydataset.render.shaders.ShaderProgram`/`None`): Shader program used to render the mesh.
			parent                        (`pydataset.render.Entity`/`None`): Parent of the new mesh.

		Returns:
			`pydataset.render.Mesh`: The mesh.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		assert pytools.assertions.type_is_instance_of( shader_program, (type(None), ShaderProgram) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		# if the filepath is str convert to filepath
		filepath = filepath if isinstance(filepath, pytools.path.FilePath) else pytools.path.FilePath(filepath)
		assert pytools.assertions.equal( filepath.extension(), '.ply' )

		# Load the mesh data
		mesh_data = MeshData.load_ply( filepath )

		# Create the mesh
		mesh = cls( mesh_data, shader_program, parent )

		# Set the shader program
		if shader_program:
			mesh.shader_program = shader_program

		return mesh

	# def load_ply ( cls, filepath )

	# --------------------------------------------------

	@classmethod
	def load_xyz ( cls, filepath, items=['x', 'y', 'z', 'nx', 'ny', 'nz', 'r', 'g', 'b'], shader_program=None, parent=None ):
		"""
		Creates a new mesh instance by loading it from a XYZ file.

		Arguments:
			cls                                                     (`type`): Type of mesh to create.
			filepath                         (`str`/`pytools.path.FilePath`): Path of the ply file to load.
			items                                          (`list` of `str`): Item to on each line.
			shader_program (`pydataset.render.shaders.ShaderProgram`/`None`): Shader program used to render the mesh.
			parent                        (`pydataset.render.Entity`/`None`): Parent of the new mesh.

		Returns:
			`pydataset.render.Mesh`: The mesh.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		assert pytools.assertions.type_is( items, list )
		assert pytools.assertions.list_items_type_is( items, str )
		assert 'x' in items
		assert 'y' in items
		assert 'z' in items
		assert pytools.assertions.type_is_instance_of( shader_program, (type(None), ShaderProgram) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		# if the filepath is str convert to filepath
		filepath = filepath if isinstance(filepath, pytools.path.FilePath) else pytools.path.FilePath(filepath)
		assert pytools.assertions.equal( filepath.extension(), '.xyz' )

		# Load the mesh data
		mesh_data = MeshData.load_xyz( filepath, items )

		# Create the mesh
		mesh = cls( mesh_data, parent )

		# Set the shader program
		if shader_program:
			mesh.shader_program = shader_program

		return mesh

	# def load_xyz ( cls, filepath )

# class Mesh ( Entity )