# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization
import pytools.tasks


# ##################################################
# ###          CLASS RENDERER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RendererSettings',
	   namespace = 'pydataset.render',
	fields_names = [
		'render_feature_name',
		'size',
		'fps',
		'is_dry_running'
		]
	)
class RendererSettings ( pytools.tasks.TaskSettings ):
	"""
	Settings used by renderers.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, render_feature_name='render', size=pydataset.data.ShapeData(800, 600), fps=240, is_dry_running=False, name='renderer' ):
		"""
		Initializes a new instance of the `pydataset.render.RendererSettings` class.

		Arguments:
			self (`pydataset.render.RendererSettings`): Instance to initialize.
			render_feature_name                (`str`): Name of the feature where to save the render.
			size          (`pydataset.data.ShapeData`): Size of the rendering viewport.
			fps                                (`int`): Render rate in frame per seconds.
			is_dry_running                    (`bool`): Boolean indicating if the renderer is dry-running.
			name                               (`str`): Name of the renderer.
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( render_feature_name, str )
		assert pytools.assertions.type_is_instance_of( size, pydataset.data.ShapeData )
		assert pytools.assertions.type_is( fps, int )
		assert pytools.assertions.true( fps > 0 )
		assert pytools.assertions.type_is( name, str )

		super( RendererSettings, self ).__init__( name )

		self._render_feature_name = render_feature_name
		self._size                = size
		self._fps                 = fps
		self._is_dry_running      = is_dry_running

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def render_feature_name ( self ):
		"""
		Name of the feature where to save the render (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._render_feature_name
	
	# def render_feature_name ( self )

	# --------------------------------------------------

	@render_feature_name.setter
	def render_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is_instance_of( value, str )
	
		self._render_feature_name = value
	
	# def render_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def size ( self ):
		"""
		Size of the rendering viewport (`pydataset.data.ShapeData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._size
	
	# def size ( self )

	# --------------------------------------------------

	@size.setter
	def size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ShapeData )
	
		self._size = value
	
	# def size ( self, value )

	# --------------------------------------------------

	@property
	def fps ( self ):
		"""
		Render rate in frame per seconds (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
	
		return self._fps
	
	# def fps ( self )

	# --------------------------------------------------

	@fps.setter
	def fps ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is_instance_of( value, int )
		assert pytools.assertions.true( value > 0 )
	
		self._fps = value
	
	# def fps ( self, value )

	# --------------------------------------------------

	@property
	def is_dry_running ( self ):
		"""
		Boolean indicating if the renderer is dry-running (`bool`).

		When the renderer is dry running, renders are not saved.
		"""
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )

		return self._is_dry_running

	# def is_dry_running ( self )
	
	# --------------------------------------------------

	@is_dry_running.setter
	def is_dry_running ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RendererSettings )
		assert pytools.assertions.type_is( value, bool )

		self._is_dry_running = value
		
	# def is_dry_running ( self, value )

# class RendererSettings ( pytools.tasks.TaskSettings )