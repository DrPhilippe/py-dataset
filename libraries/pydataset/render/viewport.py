# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import OpenGL.GL
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from .scene import Scene

# ##################################################
# ###               CLASS VIEPORT                ###
# ##################################################

class Viewport ( PyQt5.QtWidgets.QOpenGLWidget ):
	"""
	Viewport is used to render and display a scene.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, scene=None, parent=None, window_flags=PyQt5.QtCore.Qt.Widget ):
		"""
		Initializes a new instance of the `pyremder.Viewport` class.

		Arguments:
			self                   (`pyremder.Viewport`): Instance to initialize.
			scene      (`None`/`pydataset.render.Scene`): Scene viewed in this viewport.
			parent    (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget if any.
			window_flags (`PyQt5.QtCore.Qt.WindowFlags`): Window flags.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )
		assert pytools.assertions.type_is_instance_of( scene, (type(None), Scene) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# init QOpenGLWidget
		super( Viewport, self ).__init__( parent, window_flags )
		self.setFocusPolicy( PyQt5.QtCore.Qt.StrongFocus )
		self.setTextureFormat( OpenGL.GL.GL_RGBA32F )

		# set scene
		self._scene = scene

	# def __init__ ( self, parent, window_flags )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def scene ( self ):
		"""
		Scene viewed in this viewport (`None`/`pydataset.render.Scene`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )

		return self._scene

	# def scene ( self )
	
	# --------------------------------------------------

	@scene.setter
	def scene( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Viewport )
		
		self._scene = value

	# def scene ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def grab_image ( self, width, height, depth=4 ):
		"""
		Grabs the rendered image produced by this vieport.

		Arguments:
			self (`pydataset.render.Viewport`): Viewport of which to grab the render.
			width                      (`int`): Width of the render.
			height                     (`int`): Height of the render.
			depth                      (`int`): Depth of the render (number of channels).

		Returns:
			`numpy.ndarray`: The render.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )

		# Grab the qimage render
		self.makeCurrent()
		render = self.grabFramebuffer()
		self.doneCurrent()

		ptr = render.constBits()
		ptr.setsize( width*height*depth )

		image = numpy.frombuffer( ptr, dtype=numpy.uint8 )
		image = numpy.reshape( image, [height, width, depth] )
		image = image.copy()

		return image

	# def grab_image ( self )

	# --------------------------------------------------

	def grab_depth ( self, width, height ):
		"""
		Grabs the rendered image produced by this vieport.

		Arguments:
			self (`pydataset.render.Viewport`): Viewport of which to grab the render.
			width                      (`int`): Width of the render.
			height                     (`int`): Height of the render.

		Returns:
			`numpy.ndarray`: The render.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )

		self.makeCurrent()
		depth = numpy.empty( [height, width], dtype=numpy.float32 )
		depth = OpenGL.GL.glReadPixels( 0, 0, width, height, OpenGL.GL.GL_RED, OpenGL.GL.GL_FLOAT, depth )
		self.doneCurrent()
		depth = depth.copy()
		depth = cv2.flip( depth, 0 )
		return depth#[:, :, 0]

	# def grab_depth ( self )

	# --------------------------------------------------

	def initializeGL ( self ):
		"""
		Initializes this viewport.

		Arguments:
			self (`pyremder.Viewport`): Viewport to initialize.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )
		
		# Enable depth buffer and culling
		OpenGL.GL.glEnable( OpenGL.GL.GL_DEPTH_TEST )
		OpenGL.GL.glDisable( OpenGL.GL.GL_CULL_FACE )
		
		# Background color and depth
		OpenGL.GL.glClearColor( 0.0, 0.0, 0.0, 0.0 )
		OpenGL.GL.glClearDepth( 1.0 )

		# Update the scene
		if self.scene:
			self.scene.update()

	# def initializeGL ( self )

	# --------------------------------------------------

	def paintGL ( self ):
		"""
		Paint the scene viewed in the viewport.

		Arguments:
			self (`pyremder.Viewport`): Viewport to paint.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )

		# Clear the render
		OpenGL.GL.glClear( OpenGL.GL.GL_COLOR_BUFFER_BIT | OpenGL.GL.GL_DEPTH_BUFFER_BIT )

		# Render the frame on screen
		if self.scene:
			self.scene.paint()

	# def paintGL ( self )

	# --------------------------------------------------

	def resizeGL ( self, width, height ):
		"""
		Resize this viewport.

		Arguments:
			self (`pyremder.Viewport`): Viewport to resize.
		"""
		assert pytools.assertions.type_is_instance_of( self, Viewport )

	# def resizeGL ( self, width, height )

# class Viewport ( PyQt5.QtWidgets.QOpenGLWidget )