# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .shader_program import ShaderProgram

# ##################################################
# ###                 CLASS MESH                 ###
# ##################################################

class ColorShaderProgram ( ShaderProgram ):
	"""
	Render the mesh using the colors provided in the vertices.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='Color Shader Program' ):
		"""
		Initializes a new instance of the `pydataset.render.shaders.ColorShaderProgram` class.

		Arguments:
			self (`pydataset.render.shaders.ColorShaderProgram`): Instance to initialize.
			name                                         (`str`): Name of the shader program.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorShaderProgram )

		super( ColorShaderProgram, self ).__init__( name )

	# def __init__ ( self, name )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_fragment_shader_source ( cls ):
		"""
		Returns the source code of the fragment shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the fragment shader source code.

		Returns:
			`str`: The source code of the fragment shader.
		"""
		return '''
			uniform float u_light_ambient_w;

			varying vec4 v_color;
			varying vec3 v_eye_pos;
			varying vec3 v_L;

			void main ()
			{
			    // Face normal in eye coordinates
				vec3 face_normal = normalize( cross( dFdx(v_eye_pos), dFdy(v_eye_pos) ) );

				float light_diffuse_w = max( dot( normalize(v_L), normalize(face_normal) ), 0.0 );
				float light_w = u_light_ambient_w + light_diffuse_w;
				if ( light_w > 1.0 )
				{
					light_w = 1.0;
				}
				gl_FragColor = light_w * v_color;
			}
			'''

	# def get_fragment_shader_source ( cls )

	# --------------------------------------------------

	@classmethod
	def get_vertex_shader_source ( cls ):
		"""
		Returns the source code of the vertex shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the vertex shader source code.

		Returns:
			`str`: The source code of the vertex shader.
		"""
		return '''
			uniform mat4 u_mv;
			uniform mat4 u_mvp;
			uniform vec3 u_light_eye_pos;
			
			attribute vec3 a_position;
			attribute vec4 a_color;
			attribute vec3 a_normal;

			varying vec4 v_color;
			varying vec3 v_eye_pos;
			varying vec3 v_L;

			void main ()
			{
				// Project the vertex in the image plqne
				gl_Position = u_mvp * vec4( a_position, 1.0 );
				
				// Save the color of the vertex
				v_color = a_color;

				// Vertex position in eye coordinates
				v_eye_pos = (u_mv * vec4(a_position, 1.0)).xyz; 

				// Vector to the light
				v_L = normalize( u_light_eye_pos - v_eye_pos ); 
			}
			'''

	# def get_vertex_shader_source ( cls )

# class ColorShaderProgram ( ShaderProgram )