# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .shader_program import ShaderProgram

# ##################################################
# ###                 CLASS MESH                 ###
# ##################################################

class DepthShaderProgram ( ShaderProgram ):
	"""
	Renders a depth image of the mesh.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='Depth Shader Program' ):
		"""
		Initializes a new instance of the `pydataset.render.shaders.DepthShaderProgram` class.

		Arguments:
			self (`pydataset.render.shaders.DepthShaderProgram`): Instance to initialize.
			name                                         (`str`): Name of the shader program.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthShaderProgram )

		super( DepthShaderProgram, self ).__init__( name )

	# def __init__ ( self, name )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_fragment_shader_source ( cls ):
		"""
		Returns the source code of the fragment shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the fragment shader source code.

		Returns:
			`str`: The source code of the fragment shader.
		"""
		return '''
			uniform float u_light_ambient_w;
			
			varying float v_eye_depth;

			void main()
			{
				gl_FragColor = vec4(v_eye_depth, 0.0, 0.0, 1.0);
			}
			'''

	# def get_fragment_shader_source ( cls )

	# --------------------------------------------------

	@classmethod
	def get_vertex_shader_source ( cls ):
		"""
		Returns the source code of the vertex shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the vertex shader source code.

		Returns:
			`str`: The source code of the vertex shader.
		"""
		return '''
			uniform mat4 u_mv;
			uniform mat4 u_mvp;
			uniform vec3 u_light_eye_pos;

			attribute vec3  a_position;
			attribute vec3  a_color;
			attribute vec3  a_normal;

			varying   float v_eye_depth;

			void main()
			{
				gl_Position = u_mvp * vec4(a_position, 1.0);

				// Vertex position in eye coordinates
				vec3 v_eye_pos = (u_mv * vec4(a_position, 1.0)).xyz; 

				// OpenGL Z axis goes out of the screen, so depths are negative
				v_eye_depth = -v_eye_pos.z;
			}
			'''

	# def get_vertex_shader_source ( cls )

# class DepthShaderProgram ( ShaderProgram )