# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .shader_program import ShaderProgram

# ##################################################
# ###         CLASS MASK-SHADER-PROGRAM          ###
# ##################################################

class MaskShaderProgram ( ShaderProgram ):
	"""
	Render the mesh using the white color.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='Mask Shader Program' ):
		"""
		Initializes a new instance of the `pydataset.render.shaders.MaskShaderProgram` class.

		Arguments:
			self (`pydataset.render.shaders.MaskShaderProgram`): Instance to initialize.
			name                                        (`str`): Name of the shader program.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskShaderProgram )

		super( MaskShaderProgram, self ).__init__( name )

	# def __init__ ( self, name )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_fragment_shader_source ( cls ):
		"""
		Returns the source code of the fragment shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the fragment shader source code.

		Returns:
			`str`: The source code of the fragment shader.
		"""
		return '''
			uniform float u_light_ambient_w;

			varying vec4 v_color;
			varying vec3 v_eye_pos;
			varying vec3 v_L;

			void main ()
			{
				gl_FragColor = v_color;
			}
			'''

	# def get_fragment_shader_source ( cls )

	# --------------------------------------------------

	@classmethod
	def get_vertex_shader_source ( cls ):
		"""
		Returns the source code of the vertex shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the vertex shader source code.

		Returns:
			`str`: The source code of the vertex shader.
		"""
		return '''
			uniform mat4 u_mv;
			uniform mat4 u_mvp;
			uniform vec3 u_light_eye_pos;
			
			attribute vec3 a_position;
			attribute vec4 a_color;
			attribute vec3 a_normal;

			varying vec4 v_color;

			void main ()
			{
				// Project the vertex in the image plqne
				gl_Position = u_mvp * vec4( a_position, 1.0 );
				
				// Set the vertex color as white
				v_color = vec4( 1.0, 1.0, 1.0, 1.0 );
			}
			'''

	# def get_vertex_shader_source ( cls )

# class MaskShaderProgram ( ShaderProgram )