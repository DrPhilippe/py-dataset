# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtGui

# INTERNALS
import pytools.assertions

# ##################################################
# ###                CLASS SHADER                ###
# ##################################################

class ShaderProgram ( PyQt5.QtGui.QOpenGLShaderProgram ):
	"""
	Shader program are used to render meshs.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name ):
		"""
		Initializes a new instance of the `pydataset.render.shaders.ShaderProgram` class.

		Arguments:
			self (`pydataset.render.shaders.ShaderProgram`): Instance to initialize.
			name                                    (`str`): Name of the shader program.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShaderProgram )

		super( ShaderProgram, self ).__init__()

		self._name = name

	# def __init__ ( self, name )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def build ( self ):
		"""
		Builds this shader program.

		Arguments:
			self (`pyrender.shaders.ShaderProgram`): Shader program to build.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShaderProgram )

		# Create the vertex shader
		if not self.addShaderFromSourceCode( PyQt5.QtGui.QOpenGLShader.Vertex, type(self).get_vertex_shader_source() ):
			raise RuntimeError(
				'Failed to compile vertex shader of shader program {}: {}'.format( self.name, self.log() )
				)
			
		# Create the fragment shader
		if not self.addShaderFromSourceCode( PyQt5.QtGui.QOpenGLShader.Fragment, type(self).get_fragment_shader_source() ):
			raise RuntimeError(
				'Failed to compile fragment shader of shader program {}: {}'.format( self.name, self.log() )
				)

		# Link the shader program
		if not self.link():
			raise RuntimeError(
				'Failed to link shader program {}: {}'.format( self.name, self.log() )
				)

	# def build ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_fragment_shader_source ( cls ):
		"""
		Returns the source code of the fragment shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the fragment shader source code.

		Returns:
			`str`: The source code of the fragment shader.
		"""
		raise NotImplementedError()

	# def get_fragment_shader_source ( cls )

	# --------------------------------------------------

	@classmethod
	def get_vertex_shader_source ( cls ):
		"""
		Returns the source code of the vertex shader of this type of shader program.

		Arguments:
			cls (`type`): Type of shader program of which to get the vertex shader source code.

		Returns:
			`str`: The source code of the vertex shader.
		"""
		raise NotImplementedError()

	# def get_vertex_shader_source ( cls )

# class ShaderProgram ( PyQt5.QtGui.QOpenGLShaderProgram )