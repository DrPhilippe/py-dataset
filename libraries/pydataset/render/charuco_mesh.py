# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..data     import CharucoBoardData, ColorData
from .entity                    import Entity
from .mesh                      import Mesh
from .charuco_mesh_data         import CharucoMeshData
from .register_entity_attribute import RegisterEntityAttribute
from .shaders                   import ShaderProgram

# ##################################################
# ###                 CLASS MESH                 ###
# ##################################################

@RegisterEntityAttribute( 'pydataset.render.CharucoMesh' )
class CharucoMesh ( Mesh ):
	"""
	Mesh entity.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, shader_program=None, parent=None ):
		"""
		Initializes a new instance of the `pydataset.render.CharucoMesh` class.

		Arguments:
			self                            (`pydataset.render.CharucoMesh`): Instance to initialize.
			data                        (`pydataset.render.CharucoMeshData`): Charuco mesh data.
			shader_program (`pydataset.render.shaders.ShaderProgram`/`None`): Shader program used to render the mesh.
			parent                        (`pydataset.render.Entity`/`None`): Parent of the new mesh. 
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMesh )
		assert pytools.assertions.type_is_instance_of( data, CharucoMeshData )
		assert pytools.assertions.type_is_instance_of( shader_program, (type(None), ShaderProgram) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Entity) )

		super( CharucoMesh, self ).__init__( data, shader_program, parent )

	# def __init__ ( self, data, shader_program, parent )
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def from_charuco_data ( cls, charuco_data, white_color=ColorData(255,255,255,255), black_color=ColorData(0,0,0,255), shader_program=None, parent=None, **kwargs ):

		data = CharucoMeshData.from_charuco_data(
			charuco_data,
			white_color,
			black_color,
			**kwargs
			)

		return cls( data, shader_program, parent )

# class CharucoMesh ( Mesh )