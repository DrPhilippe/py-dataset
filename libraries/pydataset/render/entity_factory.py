# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###            CLASS ENTITY-FACTORY            ###
# ##################################################

class EntityFactory ( pytools.factory.ClassFactory ):

	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	group_name = 'entities'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def has_entity ( cls, typename ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( typename, str )

		return cls.typename_is_used( cls.group_name, typename )

	# def has_entity ( cls, typename )

	# --------------------------------------------------

	@classmethod
	def instantiate_entity ( cls, typename, **kwargs ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( typename, str )

		return cls.instantiate( cls.group_name, typename, **kwargs )

	# def instantiate_entity ( cls, typename, **kwargs )

# class EntityFactory ( pytools.factory.ClassFactory )