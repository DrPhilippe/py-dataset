# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import numpy

# INTERNAL
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data     import CharucoBoardData, ColorData
from .mesh_data import MeshData

# ##################################################
# ###              CLASS MESH-DATA               ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoMeshData',
	   namespace = 'pydataset.render',
	fields_names = []
	)
class CharucoMeshData ( MeshData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.render.CharucoMeshData` class.

		Arguments:
			self (`pydataset.render.CharucoMeshData`): Instance to initialize.

		Named Arguments:
			primitive                                       (`str`): Primitive type.
			points                         (`None`/`numpy.ndarray`): Vertex positions.
			colors                         (`None`/`numpy.ndarray`): Vertex colors.
			normals                        (`None`/`numpy.ndarray`): Vertex normals.
			indexes                        (`None`/`numpy.ndarray`): Indexes.
			self                    (`pydataset.render.EntityData`): Instance to initialize.
			children_data (`list` of `pydataset.render.EntityData`): Data of the children of this entity.
			local_transform                 (`list` of 16 `float`s): Transformation from the parent entity space to this entity's space.
			enabled                                        (`bool`): Enabled / disabled boolean state.
			name                                            (`str`): Name of this entity.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMeshData )

		super( CharucoMeshData, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################
	
	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this `pydataset.render.MeshData`.

		Arguments:
			self (`pydataset.render.MeshData`): `pydataset.render.MeshData` instance to represent.

		Returns:
			`str`: Text representation of this `pydataset.render.MeshData`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshData )

		return '[CharucoMeshData: name={}, enabled={}, vertices={}, indexes={}]'.format(
			self.name,
			self.enabled,
			self.points.shape[0] if self.points is not None else 0,
			self.indexes.shape[0] if self.indexes is not None else 0
			)

	# def __str__ ( self )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@staticmethod
	def create_cell_data ( x, y, width, height, index ):
		points = numpy.asarray([
			[x,         y,          0.0],
			[x + width, y,          0.0],
			[x + width, y + height, 0.0],
			[x,         y + height, 0.0]
			],
			dtype=numpy.float32
			)
		indexes = numpy.asarray([
			[index*4, index*4 + 1, index*4 + 2],
			[index*4, index*4 + 2, index*4 + 3]
			],
			dtype=numpy.uint32
			)
		return points, indexes
	
	# def create_cell_data ( x, y, width, height, index )

	# --------------------------------------------------

	@classmethod
	def from_charuco_data ( cls, charuco_data, white_color=ColorData(255,255,255,255), black_color=ColorData(0,0,0,255), **kwargs ):
		assert pytools.assertions.type_is_instance_of( charuco_data, CharucoBoardData )
		assert pytools.assertions.type_is_instance_of( white_color, ColorData )
		assert pytools.assertions.type_is_instance_of( black_color, ColorData )

		# Board infos
		board_width     = charuco_data.board_width
		board_height    = charuco_data.board_height
		square_length   = charuco_data.square_length
		border_length   = charuco_data.border_length
		number_of_cells = charuco_data.number_of_cells
		ref             = board_height % 2
		
		total_board_width     = board_width + 2
		total_board_height    = board_height + 2
		total_number_of_cells = total_board_width * total_board_height

		# Mesh points, colors, indexes
		mesh_points  = numpy.zeros( [total_number_of_cells*4, 3], dtype=numpy.float32 )
		mesh_colors  = numpy.zeros( [total_number_of_cells*4, 4], dtype=numpy.float32 )
		mesh_indexes = numpy.zeros( [total_number_of_cells*2, 3], dtype=numpy.uint32  )

		# Convert the white color to a numpy ndarray and repeat it 4 times
		white_color  = white_color.to_array()
		white_color  = white_color.astype( numpy.float32 ) / 255.0
		white_color  = numpy.reshape( white_color, [1, 4] )
		white_colors = numpy.repeat( white_color, 4, axis=0 )

		# Convert the black color to a numpy ndarray and repeat it 4 times
		black_color  = black_color.to_array()
		black_color  = black_color.astype( numpy.float32 ) / 255.0
		black_color  = numpy.reshape( black_color, [1, 4] )
		black_colors = numpy.repeat( black_color, 4, axis=0 )

		# Mesh normals
		normal       = numpy.asarray( [0.0, 0.0, 1.0], dtype=numpy.float32 )
		normal       = numpy.reshape( normal, [1, 3] )
		mesh_normals = numpy.repeat( normal, total_number_of_cells*4, axis=0 )
		
		# Global cell index
		cell_index = 0

		# Go through cells
		for y in range( total_board_height ):

			for x in range( total_board_width ):
				
				# Default color is white
				cell_colors = white_colors

				# Top-Left corner
				if x == 0 and y == 0:					
					cell_points, cell_indexes = cls.create_cell_data(
						     x = -border_length,
						     y = -border_length,
						 width = border_length,
						height = border_length,
						 index = cell_index
						)

				# Top-Right corner
				elif x == (total_board_width-1) and y == 0:
					cell_points, cell_indexes = cls.create_cell_data(
						     x = board_width * square_length,
						     y = -border_length,
						 width = border_length,
						height = border_length,
						 index = cell_index
						)
				
				# Bottom-Left corner
				elif x == 0 and y == (total_board_height-1):					
					cell_points, cell_indexes = cls.create_cell_data(
						     x = -border_length,
						     y = board_height * square_length,
						 width = border_length,
						height = border_length,
						 index = cell_index
						)

				# Bottom-Right corner
				elif x == (total_board_width-1) and y == (total_board_height-1):					
					cell_points, cell_indexes = cls.create_cell_data(
						     x = board_width  * square_length,
						     y = board_height * square_length,
						 width = border_length,
						height = border_length,
						 index = cell_index
						)

				# Left Border
				elif x == 0:
					cell_points, cell_indexes = cls.create_cell_data(
						     x = -border_length,
						     y = (y-1) * square_length,
						 width = border_length,
						height = square_length, # regular cell height 
						 index = cell_index
						)

				# Right border
				elif x == (total_board_width-1):
					cell_points, cell_indexes = cls.create_cell_data(
						     x = board_width * square_length,
						     y = (y-1) * square_length,
						 width = border_length,
						height = square_length, # regular cell height 
						 index = cell_index
						)

				# Top border
				elif y == 0:
					cell_points, cell_indexes = cls.create_cell_data(
						     x = (x-1) * square_length,
						     y = -border_length,
						 width = square_length, # regular cell width 
						height = border_length,
						 index = cell_index
						)

				# Bottom border
				elif y == (total_board_height-1):
					cell_points, cell_indexes = cls.create_cell_data(
						     x = (x-1) * square_length,
						     y = board_height * square_length,
						 width = square_length, # regular cell width 
						height = border_length,
						 index = cell_index
						)

				else:
					# Create a regular board cell
					cell_points, cell_indexes = cls.create_cell_data(
						     x = (x-1) * square_length,
						     y = (y-1) * square_length,
						 width = square_length,
						height = square_length,
						 index = cell_index
						)

					# Figure out if we are on a black or white cell
					# Crazy math that work
					mx = x % 2
					my = y % 2
					i = (mx + my) % 2
					
					# Black cell (white is default)
					if ( i != ref ):
						cell_colors = black_colors

				# if x == 0 and y == 0

				# Assing cell data
				mesh_points [ (cell_index+0)*4 : (cell_index+1)*4, : ] = cell_points
				mesh_colors [ (cell_index+0)*4 : (cell_index+1)*4, : ] = cell_colors
				mesh_indexes[ (cell_index+0)*2 : (cell_index+1)*2, : ] = cell_indexes
				
				# Next cell
				cell_index += 1

			# for x in range( total_board_width )

		# for y in range( total_board_height )

		return cls(
			primitive = 'triangles',
			   points = mesh_points,
			   colors = mesh_colors,
			  normals = mesh_normals,
			  indexes = mesh_indexes,
			  **kwargs
			)
		
	# def from_charuco_data ( cls, charuco_data, **kwargs )

# class CharucoMeshData ( MeshData )