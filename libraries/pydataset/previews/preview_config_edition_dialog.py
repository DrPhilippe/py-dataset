# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.previews
import pytools.assertions
import pytools.serialization
import pyui.inspector

# LOCALS
from .preview_settings import PreviewSettings
from. preview_settings_wrapper_widget import PreviewSettingsWrapperWidget

# ##################################################
# ###    CLASS PREVIEW-CONFIG-EDITION-DIALOG     ###
# ##################################################

class PreviewConfigEditionDialog ( pyui.inspector.InspectorDialog ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, config=None, title='Edit Preview Config', text_ok='Apply', text_cancel='Cancel', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )
		assert pytools.assertions.type_is_instance_of( config, pydataset.previews.PreviewConfig )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is( text_ok, str )
		assert pytools.assertions.type_is( text_cancel, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		self._contents = []

		# init the dialog
		super( PreviewConfigEditionDialog, self ).__init__(
			      value = config,
			is_editable = True,
			      title = title,
			    text_ok = text_ok,
			text_cancel = text_cancel,
			     parent = parent
			)


	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	@property
	def add_button ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )

		return self._add_button

	# def add_button ( self )

	# --------------------------------------------------

	@property
	def type_combobox ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )

		return self._type_combobox
	
	# def type_combobox ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def clear ( self ):
		
		for widget in self._contents:
			self._settings_parent_layout.removeWidget( widget )
			widget.deleteLater()
			del widget

		self._contents = []

	# def clear ( self )

	# --------------------------------------------------

	def creates_settings_widget ( self, settings ):

		settings_widget = PreviewSettingsWrapperWidget( settings, is_editable=True, parent=self._settings_parent_widget )
		settings_widget.deleted.connect( self.on_delete_pressed )
		settings_widget.value_changed.connect( self.on_preview_settings_changed )
		return settings_widget

	# def creates_settings_widget ( self, settings )

	# --------------------------------------------------

	def populate ( self ):

		# Create a container for each settings
		for preview_settings in self.value.previews:

			preview_settings_widget = self.creates_settings_widget( preview_settings )
			
			self._contents.append( preview_settings_widget )
			self._settings_parent_layout.addWidget( preview_settings_widget )
		
		# for settings in self.value.previews

	# def populate ( self )

	# --------------------------------------------------

	def setup_form ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )

		# Scroll Area
		scroll_area = PyQt5.QtWidgets.QScrollArea( self )
		scroll_area.setWidgetResizable( True )
		self.layout().addWidget( scroll_area, 1 )

		# Scroll Area / Config Container
		self._settings_parent_widget = PyQt5.QtWidgets.QWidget( parent=self )
		self._settings_parent_layout = PyQt5.QtWidgets.QVBoxLayout( self._settings_parent_widget )
		self._settings_parent_layout.setContentsMargins( 0, 0, 0, 0 )
		self._settings_parent_widget.setLayout( self._settings_parent_layout )
		scroll_area.setWidget( self._settings_parent_widget )

		# Populate container
		self.populate()

		# Sub-Layout
		sublayout = PyQt5.QtWidgets.QHBoxLayout()
		sublayout.setContentsMargins( 0, 0, 0, 0 )
		sublayout.setSpacing( 5 )
		self.layout().addLayout( sublayout, 0 )

		# Sub-Layout / Type Combo-box
		types = [ '<select>' ] + pydataset.previews.PreviewSettingsFactory.get_preview_settings_names()
		self._type_combobox = PyQt5.QtWidgets.QComboBox( self )
		self._type_combobox.addItems( types )
		self._type_combobox.currentTextChanged.connect( self.on_selected_type_changed )
		sublayout.addWidget( self._type_combobox )

		# Sub-Layout / Add Button
		self._add_button = PyQt5.QtWidgets.QPushButton( '+', self )
		self._add_button.pressed.connect( self.on_add_pressed )
		self._add_button.setEnabled( False )
		self._add_button.setMaximumWidth( 30 )
		sublayout.addWidget( self._add_button )

	# def setup_form ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# -------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_add_pressed ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )

		name             = self.type_combobox.currentText()
		preview_settings = pydataset.previews.PreviewSettingsFactory.instantiate_preview_settings( name )
		
		self.value.previews.append( preview_settings )
		settings_widget = self.creates_settings_widget( preview_settings )
		self._settings_parent_layout.addWidget( settings_widget )

		self.ok_button.setEnabled( True )
		
	# def on_add_pressed ( self )

	# -------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( object )
	def on_delete_pressed ( self, widget ):

		self.value.previews.remove( widget.value )
		self._settings_parent_layout.removeWidget( widget )
		self._contents.remove( widget )

		widget.setParent( None )
		widget.deleteLater()
		del widget

	# def on_delete_pressed ( self, preview_settings ):

	# -------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_preview_settings_changed ( self ):

		self.ok_button.setEnabled( True )

	# def on_preview_settings_changed ( self ):

	# -------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_selected_type_changed ( self, name ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfigEditionDialog )

		self.add_button.setEnabled( name != '<select>' )

	# def on_selected_type_changed ( self, name )
	
# class PreviewConfigEditionDialog ( pyui.inspector.InspectorDialog )