# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###   CLASS MARKERS-CORNERS-PREVIEW-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoBoardDataPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'charuco_data_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Charuco Board Data' )
class CharucoBoardDataPreviewSettings ( PreviewSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, charuco_data_feature_name='charuco_data', **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreviewSettings )
		assert pytools.assertions.type_is( charuco_data_feature_name, str )

		super( CharucoBoardDataPreviewSettings, self ).__init__( **kwargs )

		self._charuco_data_feature_name = charuco_data_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_data_feature_name ( self ):
		"""
		Name of the image feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreviewSettings )

		return self._charuco_data_feature_name

	# def charuco_data_feature_name ( self )
	
	# --------------------------------------------------

	@charuco_data_feature_name.setter
	def charuco_data_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._charuco_data_feature_name = value

	# def charuco_data_feature_name ( self, value )

# class CharucoBoardDataPreviewSettings ( PreviewSettings )