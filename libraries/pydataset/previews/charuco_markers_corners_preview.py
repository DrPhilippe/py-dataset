# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                                 import Example, NDArrayFeatureData
from .charuco_markers_corners_preview_settings import CharucoMarkersCornersPreviewSettings
from .preview                                  import Preview
from .register_preview_attribute               import RegisterPreviewAttribute

# ##################################################
# ###   CLASS CHARUCO-MARKERS-CORNERS-PREVIEW    ###
# ##################################################

@RegisterPreviewAttribute( 'ChArUco Markers' )
class CharucoMarkersCornersPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( CharucoMarkersCornersPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		return super( CharucoMarkersCornersPreview, self ).check( example )\
		   and example.contains_feature( settings.markers_corners_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( CharucoMarkersCornersPreview, self ).draw( example )

		# Get the feature
		markers_corners = example.get_feature( self.get_settings().markers_corners_feature_name ).value
		if self.get_settings().markers_ids_feature_name:
			markers_ids = example.get_feature( self.get_settings().markers_ids_feature_name ).value
		else:
			markers_ids = None

		self.draw_markers_corners( markers_corners, markers_ids )

			
	# def draw ( self, example )

	# --------------------------------------------------

	def draw_markers_corners ( self, markers_corners, markers_ids=None ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreview )
		assert pytools.assertions.type_is_instance_of( markers_corners, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( markers_ids, (type(None), numpy.ndarray) )

		number_of_markers = markers_corners.shape[ 0 ]

		if markers_ids is not None:
			number_of_ids = markers_ids.shape[0]
			assert pytools.assertions.equal( number_of_ids, number_of_markers )

		brush       = PyQt5.QtGui.QBrush()
		color_red   = PyQt5.QtGui.QColor( *self.get_settings().markers_corners_origin_color.to_list() )
		pen_red     = PyQt5.QtGui.QPen( color_red, 2 )
		color_green = PyQt5.QtGui.QColor( *self.get_settings().markers_corners_outline_color.to_list() ) 
		pen_green   = PyQt5.QtGui.QPen( color_green, 2 )
		color_blue  = PyQt5.QtGui.QColor( *self.get_settings().markers_ids_text_color.to_list() ) 
		pen_blue    = PyQt5.QtGui.QPen( color_blue, 2 )

		for i in range(number_of_markers):
			
			corners_i = markers_corners[ i, :, : ]

			if not numpy.isnan( numpy.sum(corners_i) ):

				# Create the polygone for the detected marker
				polygone_0 = PyQt5.QtGui.QPolygonF( 4 )
				for j in range(4):
					point_j = corners_i[ j, : ]
					point_j = PyQt5.QtCore.QPointF( *point_j )
					polygone_0[ j ] = point_j
				
				corner_0 = polygone_0[ 0 ]
				rect_0   = polygone_0.boundingRect()
				center_0 = PyQt5.QtCore.QPointF( rect_0.center() )

				# Create the polygone graphic item to display the markers edge
				item = PyQt5.QtWidgets.QGraphicsPolygonItem( polygone_0, self )
				item.setPen( pen_green )
				item.setBrush( brush )
				self.addToGroup( item )

				# Create a smaller one in the top-left corner
				transform  = PyQt5.QtGui.QTransform.fromScale( 0.2, 0.2 )
				polygone_1 = transform.map( polygone_0 )
				corner_1   = polygone_1[ 0 ]
				center_1   = polygone_1.boundingRect().center()
				offset_1   = corner_0 - corner_1
				polygone_1.translate( offset_1.x(), offset_1.y() )
				offset = corner_1 - center_1
				polygone_1.translate( offset.x(), offset.y() )
				item = PyQt5.QtWidgets.QGraphicsPolygonItem( polygone_1, self )
				item.setPen( pen_red )
				item.setBrush( brush )
				self.addToGroup( item )

				# Create a text at the center for the id
				if markers_ids is not None:
					item = PyQt5.QtWidgets.QGraphicsSimpleTextItem( str(markers_ids[i]), self )
					item.setBrush( color_blue )
					font = item.font()
					font.setBold( True )
					font.setPointSize( 12 )
					item.setFont( font )
					item.setPos( center_0 - item.boundingRect().center() )
					self.addToGroup( item )

			# if not numpy.isnan( numpy.sum(corners) )

		# for i in range(number_of_markers)

	# def draw_markers_corners ( self, markers_corners )

# class CharucoMarkersCornersPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )