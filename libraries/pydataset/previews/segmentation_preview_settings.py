# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset
import pytools

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###    CLASS SEGMENTATION-PREVIEW-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SegmentationPreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'segmentation_feature_name',
		'number_of_categories',
		'dedicated_background',
		]
	)
@RegisterPreviewSettingsAttribute( 'Segmentation' )
class SegmentationPreviewSettings ( PreviewSettings ):
	"""
	Segmentation preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, segmentation_feature_name='', number_of_categories=11, dedicated_background=True, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.previews.SegmentationPreviewSettings` class.
		
		Arguments:
			self (`pyui.previews.SegmentationPreviewSettings`): Instance to initialize.
			segmentation_feature_name                  (`str`): Name of the feature containing the segmentation.
			number_of_categories                       (`int`): Number of categories in the dataset.
			dedicated_background                      (`bool`): Is the background in a dedicated channel.
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )
		assert pytools.assertions.type_is( segmentation_feature_name, str )
		assert pytools.assertions.type_is( number_of_categories, int )
		assert pytools.assertions.type_is( dedicated_background, bool )

		super( SegmentationPreviewSettings, self ).__init__( **kwargs )

		self._segmentation_feature_name = segmentation_feature_name
		self._number_of_categories      = number_of_categories
		self._dedicated_background      = dedicated_background

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def segmentation_feature_name ( self ):
		"""
		Name of the feature containing the segmentation to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )

		return self._segmentation_feature_name

	# def segmentation_feature_name ( self )
	
	# --------------------------------------------------

	@segmentation_feature_name.setter
	def segmentation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._segmentation_feature_name = value

	# def segmentation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def number_of_categories ( self ):
		"""
		Number of categories in the dataset (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )

		return self._number_of_categories

	# def number_of_categories ( self )
	
	# --------------------------------------------------

	@number_of_categories.setter
	def number_of_categories ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_categories = value

	# def number_of_categories ( self, value )


	# --------------------------------------------------

	@property
	def dedicated_background ( self ):
		"""
		Is the background in a dedicated channel (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )

		return self._dedicated_background

	# def dedicated_background ( self )
	
	# --------------------------------------------------

	@dedicated_background.setter
	def dedicated_background ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreviewSettings )
		assert pytools.assertions.type_is( value, bool )

		self._dedicated_background = value

	# def dedicated_background ( self, value )

# class SegmentationPreviewSettings ( PreviewSettings )