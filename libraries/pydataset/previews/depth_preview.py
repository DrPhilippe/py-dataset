# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                   import Example, FloatFeatureData, NDArrayFeatureData
from .                           import images_utils
from .depth_preview_settings     import DepthPreviewSettings
from .image_preview              import ImagePreview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###            CLASS DEPTH-PREVIEW             ###
# ##################################################

@RegisterPreviewAttribute( 'Depth Image' )
class DepthPreview ( ImagePreview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the image preview class.

		Arguments:
			self             (`pydataset.previews.DepthPreview`): Image preview to initialize.
			example         (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.DepthPreviewSettings`): Settings of this preview.
			parent      (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent grapgic.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, DepthPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( DepthPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self  (`pydataset.previews.ImagePreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		if not super( ImagePreview, self ).check( example ):
			return False

		if not example.contains_feature( settings.image_feature_name, NDArrayFeatureData ):
			return False

		if settings.depth_scale_feature_name:
			if not  example.contains_feature( settings.depth_scale_feature_name, FloatFeatureData ):
				return False

		return True

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self  (`pydataset.previews.DepthPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( ImagePreview, self ).draw( example )
		
		settings = self.get_settings()
		near = settings.near
		far  = settings.far

		# Get the depth image
		depth = example.get_feature( settings.image_feature_name ).value.astype( numpy.float16 )
		
		if settings.depth_scale_feature_name:
			depth_scale = example.get_feature( settings.depth_scale_feature_name ).value
			depth *= depth_scale
		
		depth = numpy.where( depth < near, near, depth )
		depth = numpy.where( depth > far,  far,  depth )
		depth = (depth - near) / (far - near) * 255.
		image = depth.astype(numpy.uint8)

		# shape = depth.shape
		# depth = depth.ravel()
		# indexes_0 = ( depth < numpy.finfo(numpy.float16).eps )
		# max_value = numpy.amax( depth, axis=0 )
		# depth[ indexes_0 ] = numpy.float16( '+inf' )
		# min_value = numpy.amin( depth, axis=0 )
		# depth[ indexes_0 ] = max_value
		# depth = numpy.interp( depth, (min_value, max_value), (0.0, 255.0) )
		# depth[ indexes_0 ] = numpy.float16( '+inf' )
		# depth = numpy.reshape( depth, shape )
		# depth = depth.astype( numpy.uint8 )
		# image = numpy.zeros( [shape[0], shape[1], 4], dtype=numpy.uint8 )
		# image[ :, :, 0 ] = depth
		# image[ :, :, 1 ] = depth
		# image[ :, :, 2 ] = depth
		# image[ :, :, 3 ] = numpy.where( depth > 0, 255.0, 0.0 )

		# Convert the image to a qpixmap
		qpixmap = images_utils.image_to_qpixmap( image )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class DepthPreview ( ImagePreview )