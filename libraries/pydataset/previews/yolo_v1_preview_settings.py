# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###       CLASS YOLO-V1-PREVIEW-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'YoloV1PreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'yolo_feature_name',
		'image_feature_name',
		'normalized'
		]
	)
@RegisterPreviewSettingsAttribute( 'YOLO V1' )
class YoloV1PreviewSettings ( PreviewSettings ):
	"""
	Draw a bounding box in a given pose (K*[R|t]).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		 yolo_feature_name = 'yolo_v1',
		image_feature_name = 'image',
		normalized = False,
		**kwargs
		):
		"""
		Initializes a new instance of the aruco marekrs cormer preview settings class.
		
		Arguments:
			self (`pydataset.previews.YoloV1PreviewSettings`): Instance to initialize.
			yolo_feature_name                         (`str`): Name of the feature containing the yolo labels.
			image_feature_nam                         (`str`): Name of the feature containing the image.
			normalized                               (`bool`): Are rectangle coordinates normalized ?
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )
		assert pytools.assertions.type_is( yolo_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( normalized, bool )

		super( YoloV1PreviewSettings, self ).__init__( **kwargs )

		self._yolo_feature_name  = yolo_feature_name
		self._image_feature_name = image_feature_name
		self._normalized         = normalized

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################


	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )

		return self._image_feature_name

	# def image_feature_name ( self )
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def yolo_feature_name ( self ):
		"""
		Name of the feature containing the yolo labels (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )

		return self._yolo_feature_name

	# def yolo_feature_name ( self )
	
	# --------------------------------------------------

	@yolo_feature_name.setter
	def yolo_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._yolo_feature_name = value

	# def yolo_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def normalized ( self ):
		"""
		Are rectangle coordinates normalized ? (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )

		return self._normalized

	# def normalized ( self )
	
	# --------------------------------------------------

	@normalized.setter
	def normalized ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1PreviewSettings )
		assert pytools.assertions.type_is( value, bool )

		self._normalized = value

	# def normalized ( self, value )

# class YoloV1PreviewSettings ( PreviewSettings )