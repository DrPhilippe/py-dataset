# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import PyQt5.QtWidgets

# INTERNALS
import pytools
import pyui

# LOCALS
from .preview_settings import PreviewSettings

# ##################################################
# ###   CLASS PREVIEW-SETTINGS-WRAPPER-WIDGET    ###
# ##################################################

class PreviewSettingsWrapperWidget ( pyui.inspector.ItemWidget ):

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################
	
	"""
	Signal emitted when the delete button is pressed.
	"""
	deleted = PyQt5.QtCore.pyqtSignal( object )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the `pydataset.previews.PreviewSettingsWrapperWidget` class.

		Arguments:
			self (`pydataset.previews.PreviewSettingsWrapperWidget`): Instance to initialize.
			value      (`pydataset.previews.PreviewSettings`/`None`): Value edited / displayed in this item widget.
			is_editable                                     (`bool`): If `True` value can be edited using the item widget.
			parent                (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettingsWrapperWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), PreviewSettings) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( PreviewSettingsWrapperWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this `pydataset.previews.PreviewSettingsWrapperWidget`.

		Arguments:
			self (`pydataset.previews.PreviewSettingsWrapperWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettingsWrapperWidget )
		
		# Main layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )
		
		# Top Widget
		top_widget = PyQt5.QtWidgets.QWidget( parent=self )
		top_layout = PyQt5.QtWidgets.QHBoxLayout( top_widget )
		top_layout.setContentsMargins( 0, 0, 0, 0 )
		top_layout.setSpacing( 0 )
		top_widget.setLayout( top_layout )
		layout.addWidget( top_widget )
		
		# Top Widget / Title label
		self._title_label = pyui.widgets.TitleLabel(
			"{}: {}".format( type(self.value).__preview_name__, self.value.name),
			parent=top_widget
			)
		top_layout.addWidget( self._title_label, 1 )
		
		# Top Widget / Delete Button
		self._delete_button = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_delete.png' ),
			'',
			parent=top_widget )
		self._delete_button.setStyleSheet(
			'''
			QPushButton {
				background-color: rgb( 80, 80, 80 );
				padding:          2px;
				border:           0px;
			}
			QPushButton:hover {
				background-color: rgb( 90, 90, 90 );
				padding:          2px;
				border:           0px;
			}
			QPushButton:pressed {
				background-color: rgb( 60, 60, 60 );
				padding:          2px;
				border:           0px;
			}
			'''
			)
		self._delete_button.pressed.connect( self.on_button_delete_pressed )
		top_layout.addWidget( self._delete_button, 0 )

		# Inspector
		self._inspector = pyui.inspector.InspectorWidget(
			      value = self.value,
			is_editable = self._is_editable,
			    is_root = True,
			     parent = self
			)
		self._inspector.value_changed.connect( self.on_inspector_value_changed )
		layout.addWidget( self._inspector )

	# def Setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this `pydataset.previews.PreviewSettingsWrapperWidget`.

		Arguments:
			self (`pydataset.previews.PreviewSettingsWrapperWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettingsWrapperWidget )

		# Update the parent class
		super( PreviewSettingsWrapperWidget, self ).update()

		self._title_label.setText( "{}: {}".format( type(self.value).__preview_name__, self.value.name ) )
		self._inspector.set_value( self.value, self.is_editable )

	# def update ( self )

	# ##################################################
	# ###                CALLBACKS                   ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_inspector_value_changed ( self ):
		"""
		Slot inboked when the preview settings are modified.

		Arguments:
			self (`pydataset.previews.PreviewSettingsWrapperWidget`): Instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettingsWrapperWidget )
	
		self._title_label.setText( "{}: {}".format( type(self.value).__preview_name__, self.value.name ) )
		self.value_changed.emit()

	# def on_inspector_value_changed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_delete_pressed ( self ):
		"""
		Slot inboked when the delete button is pressed.

		Arguments:
			self (`pydataset.previews.PreviewSettingsWrapperWidget`): Instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettingsWrapperWidget )

		self.deleted.emit( self )

	# def on_button_delete_pressed ( self )

# class PreviewSettingsWrapperWidget ( pyui.inspector.ItemWidget )