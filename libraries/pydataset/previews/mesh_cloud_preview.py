# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..                           import pose_utils
from ..dataset                    import Example, NDArrayFeatureData, SerializableFeatureData
from .mesh_cloud_preview_settings import MeshCloudPreviewSettings
from .preview                     import Preview
from .register_preview_attribute  import RegisterPreviewAttribute

# ##################################################
# ###          CLASS MESH-CLOUD-PREVIEW          ###
# ##################################################

@RegisterPreviewAttribute( 'Mesh Cloud' )
class MeshCloudPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( MeshCloudPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		ok = super( MeshCloudPreview, self ).check( example )\
		   and example.contains_feature( settings.mesh_feature_name, SerializableFeatureData )

		if not ok:
			return False

		if settings.camera_matrix_feature_name:
		   ok = example.contains_feature( settings.camera_matrix_feature_name, NDArrayFeatureData )

		if not ok:
			return False

		if settings.dist_coeffs_feature_name:
			ok = example.contains_feature( settings.dist_coeffs_feature_name, NDArrayFeatureData )

		if not ok:
			return False

		if settings.rotation_feature_name or settings.translation_feature_name:
		   ok = example.contains_feature( settings.rotation_feature_name,    NDArrayFeatureData )\
		    and example.contains_feature( settings.translation_feature_name, NDArrayFeatureData )
		

		return ok

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Setup graphic item (scale, position, rotation)
		super( MeshCloudPreview, self ).draw( example )
		
		# Fetch settings
		settings = self.get_settings()

		# Get bounding box
		mesh_points = example.get_feature( settings.mesh_feature_name ).value.points
		
		# Project bounding box
		mesh_points = self.project_points_to_image_plane_if_needed( example, mesh_points )
		# print( 'mesh_points:', mesh_points.shape )

		# Draw bounding box
		self.draw_mesh_points( mesh_points )

	# def draw ( self, example )

	# --------------------------------------------------

	def project_points_to_image_plane_if_needed ( self, example, points ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( points, numpy.ndarray )

		# Fetch settings
		settings = self.get_settings()
		
		# Do we need to project ?
		if settings.camera_matrix_feature_name:
			
			# Get camera matrix
			K  = example.get_feature( settings.camera_matrix_feature_name ).value
			
			# Get Optiona features
			if settings.dist_coeffs_feature_name:
				dist_coeffs = example.get_feature( settings.dist_coeffs_feature_name ).value
			else:
				dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

			# If rotation and/or translation are spacify use them
			# Because we will go from model space to image plane
			if settings.rotation_feature_name or settings.translation_feature_name:
				r  = example.get_feature( settings.rotation_feature_name      ).value
				t  = example.get_feature( settings.translation_feature_name   ).value
			
			# Otherwise, use 0 rotation and translation
			else:
				r = numpy.eye( 3, dtype=numpy.float32 )
				t = numpy.zeros( [3], dtype=numpy.float32 )	

			# Convert rotation to euler angles
			r = pose_utils.convert_rotation_to_eulers_if_needed( r )
			
			# Project the axes points
			points, jacobian = cv2.projectPoints( points, r, t, K, dist_coeffs )
			points = numpy.reshape( points, [-1, 2] )
		
		# if settings.camera_matrix_feature_name

		return points

	# def project_points_to_image_plane_if_needed ( self, points )

	# --------------------------------------------------

	def draw_mesh_points ( self, mesh_points ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( mesh_points, numpy.ndarray )	

		number_of_points = mesh_points.shape[ 0 ]
		decimation = self.get_settings().decimation

		if decimation > 0.:
			number_of_points_after_decimation = int( number_of_points * decimation )
			step = number_of_points // number_of_points_after_decimation
		else:
			step = 1
			
		# points
		for i in range( 0, number_of_points, step ):
			self.draw_point( mesh_points[i, :] )

	# def draw_mesh_points ( self, mesh_points )

	# --------------------------------------------------

	def draw_point ( self, point ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreview )
		assert pytools.assertions.type_is_instance_of( point, numpy.ndarray )	
		
		settings = self.get_settings()
		color    = PyQt5.QtGui.QColor( *settings.points_color.to_list() )
		brush    = PyQt5.QtGui.QBrush( color )
		pen      = PyQt5.QtGui.QPen( brush, settings.points_width )
		radius   = settings.points_width

		p = PyQt5.QtWidgets.QGraphicsEllipseItem( point[0]-radius/2., point[1]-radius/2., radius, radius, self )
		p.setPen( pen )
		p.setBrush( brush )
		self.addToGroup( p )

	# def draw_point ( self, point )

# class MeshCloudPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )