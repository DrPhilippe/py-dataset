# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                   import Example, ImageFeatureData, NDArrayFeatureData
from .                           import images_utils
from .image_preview_settings     import ImagePreviewSettings
from .preview                    import Preview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###            CLASS IMAGE-PREVIEW             ###
# ##################################################

@RegisterPreviewAttribute( 'Image' )
class ImagePreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the image preview class.

		Arguments:
			self             (`pydataset.previews.ImagePreview`): Image preview to initialize.
			example         (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.ImagePreviewSettings`): Settings of this preview.
			parent      (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent grapgic.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, ImagePreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( ImagePreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self  (`pydataset.previews.ImagePreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return super( ImagePreview, self ).check( example )\
		   and example.contains_feature( self.get_settings().image_feature_name, (ImageFeatureData,NDArrayFeatureData) )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self  (`pydataset.previews.ImagePreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( ImagePreview, self ).draw( example )
		
		# Get the image feature
		feature = example.get_feature( self.get_settings().image_feature_name )

		# Convert the image to a qpixmap
		qpixmap = images_utils.image_to_qpixmap( feature.value )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class ImagePreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview )