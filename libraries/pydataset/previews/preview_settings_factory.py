# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###       CLASS PREVIEW-SETTINGS-FACTORY       ###
# ##################################################

class PreviewSettingsFactory ( pytools.factory.ClassFactory ):
	"""
	Extra layer over the class factory to regroup previews settings by name.
	"""
	
	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	group_name = 'previews settings'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def has_preview_settings_type ( cls, name ):
		"""
		Checks is the preview settings type with the given name is registered in the preview factory.

		Arguments:
			cls (`type`): Type of factory.
			name (`str`): Name of the preview settings type to check.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		if cls.group_exist( cls.group_name ):
			return cls.typename_is_used( cls.group_name, name )

		else:
			return False

	# def has_preview_settings_type ( cls, typename )

	# --------------------------------------------------

	@classmethod
	def get_preview_settings_names ( cls ):
		"""
		Returns the names of registered preview settings types.

		Arguments:
			cls (`type`): Type of factory.

		Returns:
			`list` of `str`: Names of registered preview settings types.
		"""
		assert pytools.assertions.type_is( cls, type )

		if cls.group_exist( cls.group_name ):
			return cls.get_typenames_of_group( cls.group_name )

		else:
			return []

	# def get_preview_settings_names ( cls )

	# --------------------------------------------------

	@classmethod
	def get_preview_settings_type ( cls, name ):
		"""
		Returns the preview type registred under the given name.

		Arguments:
			cls (`type`): Type of preview settings factory.
			name (`str`): Name of the preview settings type to get.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		return cls.get_type( cls.group_name, name )

	# def get_preview_settings_type ( cls, name )
	
	# --------------------------------------------------

	@classmethod
	def instantiate_preview_settings ( cls, name, **kwargs ):
		"""
		Instantiate the preview type registred under the given name.

		Arguments:
			cls                        (`type`): Type of preview factory.
			name                        (`str`): Name of the preview settings to instantiate.
			**kwargs (`dict` of `str` to `any`): Named arguments forwarded to the preview settings constructor.

		Returns:
			`pydataset.previews.PreviewSettings`: The new preview settings instance.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		return cls.instantiate( cls.group_name, name, **kwargs )

	# def instantiate_preview_settings ( cls, name, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def register_preview_settings ( cls, name, preview_settings_type ):
		"""
		Registers the given preview type registred under the given name.

		Arguments:
			cls                   (`type`): Type of preview settings factory.
			name                   (`str`): Name of the preview settings type to register.
			preview_settings_type (`type`): Type of preview settings to register.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		cls.register( cls.group_name, name, preview_settings_type )

	# def register_preview_settings ( cls, name, preview_settings_type )

# class PreviewSettingsFactory ( pytools.factory.ClassFactory )