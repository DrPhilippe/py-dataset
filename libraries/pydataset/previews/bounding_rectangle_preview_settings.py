# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ### CLASS BOUNDING-RECTANGLE-PREVIEW-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BoundingRectanglePreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'bounding_rectangle_feature_name',
		'color',
		'thickness'
		]
	)
@RegisterPreviewSettingsAttribute( 'Bounding Rectangle' )
class BoundingRectanglePreviewSettings ( PreviewSettings ):
	"""
	Bounding-box preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		bounding_rectangle_feature_name='',
		color = pydataset.data.ColorData(0, 255, 0),
		thickness = 1.5,
		**kwargs
		):
		"""
		Initializes a new instance of the bounding-box preview settings class.
		
		Arguments:
			self (`pydataset.previews.BoundingRectanglePreviewSettings`): Instance to initialize.
			bounding_rectangle_feature_name                      (`str`): Name of the aruco markers corner feature to display.
			color                           (`pydataset.data.ColorData`): Color used to draw the rectangle.
			thickness                                          (`float`): Thichness of the rectangle outline.
		
		Named Arguments:
			see `pyui.previews.PreviewSettings` constructor.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is_instance_of( color, pydataset.data.ColorData )
		assert pytools.assertions.type_is( thickness, float )


		super( BoundingRectanglePreviewSettings, self ).__init__( **kwargs )

		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self._color                           = copy.deepcopy( color )
		self._thickness                       = thickness

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the bounding-box feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )
	
	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value

	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def color ( self ):
		"""
		Color used to draw the bounding rectangle (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )

		return self._color

	# def color ( self )
	
	# --------------------------------------------------

	@color.setter
	def color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._color = copy.deepcopy( value )

	# def color ( self, value )	

	# --------------------------------------------------

	@property
	def thickness ( self ):
		"""
		Thichness of the rectangle outline (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )

		return self._thickness

	# def thickness ( self )
	
	# --------------------------------------------------

	@thickness.setter
	def thickness ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, float )

		self._thickness = value

	# def thickness ( self, value )

# class BoundingRectanglePreviewSettings ( PreviewSettings )