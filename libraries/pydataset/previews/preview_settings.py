# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data import Point2DData

# ##################################################
# ###           CLASS PREVIEW-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'enabled',
		'position',
		'name',
		'scale'
		]
	)
class PreviewSettings ( pytools.serialization.Serializable ):
	"""
	Dataset preview settings.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, enabled=True, position=Point2DData(), scale=1.0, name='' ):
		"""
		Initializes a new instance of the preview settings class.
		
		Arguments:
			self  (`pydataset.previews.PreviewSettings`): Instance to initialize.
			enabled                             (`bool`): Boolean indicating if this preview must be displayed.
			position      (`pydataset.data.Point2DData`): Position of the preview in the scene.
			scale                              (`float`): Scale of the preview.
			name                                 (`str`): Name of this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )
		assert pytools.assertions.type_is_instance_of( position, Point2DData )
		assert pytools.assertions.type_is( enabled, bool )
		assert pytools.assertions.type_is( scale, float )
		assert pytools.assertions.type_is( name, str )

		self._enabled  = enabled
		self._position = copy.deepcopy( position )
		self._scale    = scale
		self._name     = name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def enabled ( self ):
		"""
		Boolean indicating if this preview must be displayed (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )

		return self._enabled

	# def enabled ( self )
	
	# --------------------------------------------------
	
	@enabled.setter
	def enabled ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )
		assert pytools.assertions.type_is( value, bool )

		self._enabled = value

	# def enabled ( self, value )

	# --------------------------------------------------

	@property
	def position ( self ):
		"""
		Position of the preview in the scene (`pydataset.data.Point2DData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )

		return self._position

	# def position ( self )
	
	# --------------------------------------------------
	
	@position.setter
	def position ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, Point2DData )

		self._position = value

	# def position ( self, value )

	# --------------------------------------------------

	@property
	def scale ( self ):
		"""
		Scale of this preview (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )

		return self._scale
	
	# def scale ( self )

	# --------------------------------------------------

	@scale.setter
	def scale ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._scale = value
	
	# def scale ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this preview (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )

		return self._name
	
	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

# class PreviewSettings ( pytools.serialization.Serializable )