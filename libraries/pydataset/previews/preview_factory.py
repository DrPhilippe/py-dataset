# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .preview_settings import PreviewSettings

# ##################################################
# ###           CLASS PREVIEW-FACTORY            ###
# ##################################################

class PreviewFactory ( pytools.factory.ClassFactory ):
	"""
	Extra layer over the clas factory to regroup item previews.
	"""
	
	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	group_name = 'previews'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def has_preview_type ( cls, name ):
		"""
		Checks is the preview type with the given name is registered in the preview factory.

		Arguments:
			cls (`type`): Type of factory.
			name (`str`): Name of type to check.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		if cls.group_exist( cls.group_name ):
			return cls.typename_is_used( cls.group_name, name )
			
		else:
			return False

	# def has_preview_type ( cls, typename )
	
	# --------------------------------------------------

	@classmethod
	def get_preview_names ( cls ):
		"""
		Returns the names of registered preview types.

		Arguments:
			cls (`type`): Type of factory.

		Returns:
			`list` of `str`: Names of registered preview types.
		"""
		assert pytools.assertions.type_is( cls, type )

		if cls.group_exist( cls.group_name ):
			return cls.get_typenames_of_group( cls.group_name )

		else:
			return []

	# def get_preview_names ( cls )

	# --------------------------------------------------

	@classmethod
	def get_preview_type ( cls, ename ):
		"""
		Returns the preview type registred under the given name.

		Arguments:
			cls (`type`): Type of item preview factory.
			name (`str`): Name of type to get.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		return cls.get_type( cls.group_name, name )

	# def get_preview_type ( cls, name )
	
	# --------------------------------------------------

	@classmethod
	def instantiate_preview ( cls, settings, **kwargs ):
		"""
		Instantiate the preview type registred under the given name.

		Arguments:
			cls                               (`type`): Type of preview factory.
			settings (`pyui.previews.PreviewSettings`): Settings of the preview to instantiate.
			**kwargs        (`dict` of `str` to `any`): Named arguments forwarded to the item preview.

		Returns:
			`pydataset.previews.Preview` The new preview instance.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( settings, PreviewSettings )

		return cls.instantiate( cls.group_name, type(settings).__preview_name__, settings=settings, **kwargs )

	# def instantiate_preview ( cls, settings, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def register_preview ( cls, name, preview_type ):
		"""
		Registers the given preview type under the given name.

		Arguments:
			cls          (`type`): Type of preview settings factory.
			name          (`str`): Name of the preview settings type to register.
			preview_type (`type`): Type of preview settings to register.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		cls.register( cls.group_name, name, preview_type )

	# def register_preview ( cls, name, preview_settings_type )

# class PreviewFactory ( pytools.factory.ClassFactory )