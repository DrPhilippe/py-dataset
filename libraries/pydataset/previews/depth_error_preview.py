# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                     import Example, FloatFeatureData, ImageFeatureData, NDArrayFeatureData
from .                             import images_utils
from .depth_error_preview_settings import DepthErrorPreviewSettings
from .preview                      import Preview
from .register_preview_attribute   import RegisterPreviewAttribute

# ##################################################
# ###         CLASS DEPTH-ERROR-PREVIEW          ###
# ##################################################

@RegisterPreviewAttribute( 'Depth Error' )
class DepthErrorPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the depth error preview class.

		Arguments:
			self             (`pydataset.previews.DepthErrorPreview`): Image preview to initialize.
			example              (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.DepthErrorPreviewSettings`): Settings of this preview.
			parent           (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent grapgic.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthErrorPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, ImagePreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( DepthErrorPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self  (`pydataset.previews.DepthErrorPreview`): Preview.
			self      (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthErrorPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Fetch settings
		settings = self.get_settings()

		if not super( DepthErrorPreview, self ).check( example ):
			return False
		
		if  not example.contains_feature( settings.depth_feature_name,        (ImageFeatureData,NDArrayFeatureData) )\
		and not example.contains_feature( settings.render_depth_feature_name, (ImageFeatureData,NDArrayFeatureData) ):
			return False

		if settings.depth_scale_feature_name and not example.contains_feature( settings.depth_scale_feature_name, (FloatFeatureData) ):
			return False

		if settings.render_depth_scale_feature_name and not example.contains_feature( settings.render_depth_scale_feature_name, (FloatFeatureData) ):
			return False

		return True

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self  (`pydataset.previews.DepthErrorPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthErrorPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( DepthErrorPreview, self ).draw( example )

		# Fetch settings
		settings = self.get_settings()
		
		# Get the image feature
		depth        = example.get_feature( settings.depth_feature_name )
		render_depth = example.get_feature( settings.render_depth_feature_name )

		# Convert the image to a qpixmap
		qpixmap = images_utils.image_to_qpixmap( feature.value )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class DepthErrorPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview )