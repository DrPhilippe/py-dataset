# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###        CLASS TEXT-PREVIEW-SETTINGS         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TextPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'feature_name',
		'text_format',
		'text_color'
		]
	)
@RegisterPreviewSettingsAttribute( 'Text' )
class TextPreviewSettings ( PreviewSettings ):
	"""
	Textual preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, feature_name='', text_format='{}', text_color=pydataset.data.ColorData(0,0,0), **kwargs ):
		"""
		Initializes a new instance of the textual preview settings class.
		
		Arguments:
			self (`pyui.previews.TextPreviewSettings`): Instance to initialize.
			feature_name                       (`str`): Name of the feature to display.
			text_format                        (`str`): Format of the displayed text.
			text_color    (`pydataset.data.ColorData`): Color of the text.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )
		assert pytools.assertions.type_is( feature_name, str )
		assert pytools.assertions.type_is( text_format, str )
		assert pytools.assertions.type_is_instance_of( text_color, pydataset.data.ColorData )

		super( TextPreviewSettings, self ).__init__( **kwargs )

		self._feature_name = feature_name
		self._text_format  = text_format
		self._text_color   = copy.deepcopy( text_color )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def feature_name ( self ):
		"""
		Name of the feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )

		return self._feature_name

	# def feature_name ( self )
	
	# --------------------------------------------------

	@feature_name.setter
	def feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._feature_name = value

	# def feature_name ( self, value )

	# --------------------------------------------------

	@property
	def text_format ( self ):
		"""
		Format of the displayed text (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )

		return self._text_format

	# def text_format ( self )
	
	# --------------------------------------------------

	@text_format.setter
	def text_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._text_format = value

	# def text_format ( self, value )

	# --------------------------------------------------

	@property
	def text_color ( self ):
		"""
		Color used to draw the text (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )

		return self._text_color

	# def text_color ( self )
	
	# --------------------------------------------------

	@text_color.setter
	def text_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, TextPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._text_color = value

	# def text_color ( self, value )

# class TextPreviewSettings ( PreviewSettings )