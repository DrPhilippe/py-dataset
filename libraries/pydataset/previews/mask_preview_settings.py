# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###        CLASS MASK-PREVIEW-SETTINGS         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MaskPreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'mask_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Mask' )
class MaskPreviewSettings ( PreviewSettings ):
	"""
	Mask preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, mask_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the image preview settings class.
		
		Arguments:
			self (`pyui.previews.MaskPreviewSettings`): Instance to initialize.
			mask_feature_name                  (`str`): Name of the feature containing the mask to display.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskPreviewSettings )
		assert pytools.assertions.type_is( mask_feature_name, str )

		super( MaskPreviewSettings, self ).__init__( **kwargs )

		self._mask_feature_name = mask_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskPreviewSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )
	
	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

# class MaskPreviewSettings ( PreviewSettings )