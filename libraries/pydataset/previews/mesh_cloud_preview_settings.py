# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###     CLASS MESH-CLOUD-PREVIEW-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshCloudPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'mesh_feature_name',
		'camera_matrix_feature_name',
		'dist_coeffs_feature_name',
		'rotation_feature_name',
		'translation_feature_name',
		'points_color',
		'points_width',
		'decimation'
		]
	)
@RegisterPreviewSettingsAttribute( 'Mesh Cloud' )
class MeshCloudPreviewSettings ( PreviewSettings ):
	"""
	Draw a mesh in a given pose (K*[R|t]).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		mesh_feature_name = '',
		camera_matrix_feature_name = '',
		dist_coeffs_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		points_color = pydataset.data.ColorData(255, 0, 0),
		points_width = 3.0,
		decimation = 0.,
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.previews.MeshCloudPreviewSettings` class.
		
		Arguments:
			self (`pydataset.previews.MeshCloudPreviewSettings`): Instance to initialize.
			mesh_feature_name                            (`str`): Name of the feature containing the mesh.
			camera_matrix_feature_name                   (`str`): Name of the feature containing the camera parameters matrix.
			dist_coeffs_feature_name                     (`str`): Name of the feature containing the camera distortion coefficients vector (optional).
			rotation_feature_name                        (`str`): Name of the feature containing the rotation vector or matrix.
			translation_feature_name                     (`str`): Name of the feature containing the translation vector.
			points_color            (`pydataset.data.ColorData`): Color used to draw the points.
			points_width                               (`float`): Display width of the points.
			decimation                                 (`float`): Proportion of points to discad.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( mesh_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is_instance_of( points_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is( points_width, float )
		assert pytools.assertions.type_is( decimation, float )

		super( MeshCloudPreviewSettings, self ).__init__( **kwargs )

		self._mesh_feature_name          = mesh_feature_name
		self._camera_matrix_feature_name = camera_matrix_feature_name
		self._dist_coeffs_feature_name   = dist_coeffs_feature_name
		self._rotation_feature_name      = rotation_feature_name
		self._translation_feature_name   = translation_feature_name
		self._points_color               = copy.deepcopy( points_color )
		self._points_width               = points_width
		self._decimation                 = decimation

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mesh_feature_name ( self ):
		"""
		Name of the feature containing the mesh (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._mesh_feature_name

	# def mesh_feature_name ( self )
	
	# --------------------------------------------------

	@mesh_feature_name.setter
	def mesh_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._mesh_feature_name = value

	# def mesh_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._camera_matrix_feature_name

	# def camera_matrix_feature_name ( self )
	
	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_matrix_feature_name = value

	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		"""
		Name of the feature containing the camera distortion coefficients vector (optional) (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._dist_coeffs_feature_name

	# def dist_coeffs_feature_name ( self )
	
	# --------------------------------------------------

	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._dist_coeffs_feature_name = value

	# def dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation vector or matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._rotation_feature_name

	# def rotation_feature_name ( self )
	
	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_feature_name = value

	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._translation_feature_name

	# def translation_feature_name ( self )
	
	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_feature_name = value

	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def points_color ( self ):
		"""
		Color used to draw the points (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._points_color

	# def points_color ( self )
	
	# --------------------------------------------------

	@points_color.setter
	def points_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._points_color = copy.deepcopy( value )

	# def points_color ( self, value )
	
	# --------------------------------------------------

	@property
	def points_width ( self ):
		"""
		Display width of the points (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._points_width

	# def points_width ( self )
	
	# --------------------------------------------------

	@points_width.setter
	def points_width ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._points_width = value

	# def points_width ( self, value )

	# --------------------------------------------------

	@property
	def decimation ( self ):
		"""
		Proportion of points to discard (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )

		return self._decimation

	# def decimation ( self )
	
	# --------------------------------------------------

	@decimation.setter
	def decimation ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshCloudPreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._decimation = value

	# def decimation ( self, value )

# class MeshCloudPreviewSettings ( PreviewSettings )