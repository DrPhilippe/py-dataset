# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import PyQt5.QtWidgets

# INTERNALS
import pytools
import pytools.assertions
import pyui.widgets

# LOCALS
from .preview_config                import PreviewConfig
from .preview_config_edition_dialog import PreviewConfigEditionDialog

# ##################################################
# ###             CLASS PREVIEW-MENU             ###
# ##################################################

class PreviewMenu ( PyQt5.QtWidgets.QMenu ):
	"""
	The previez menu is used to manage a preview config.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( PreviewMenu, self ).__init__( 'Preview', parent )

		self._preview_config   = pytools.Loaded()
		self._visibility_group = []
		self.setup()

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------
	
	config_opened  = PyQt5.QtCore.pyqtSignal( PreviewConfig )

	# --------------------------------------------------
	
	config_changed = PyQt5.QtCore.pyqtSignal( PreviewConfig )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def preview_config ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )

		return self._preview_config.value

	# def preview_config ( self )

	# --------------------------------------------------

	@property
	def recent_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )

		return self._recent_menu
	
	# def recent_menu ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _add_recent ( self, filepath ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		
		if isinstance( filepath, pytools.path.FilePath ):
			filepath = str(filepath)

		# Get recent preview configs
		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent_preview_configs', [] )
		
		# If filepath is not in them, add it
		if not filepath in recents:
			recents.append( filepath )

			# Keep at most 10 recent configs
			if len(recents) > 10:
				recents.pop( 0 )

		# Update the recent app settings
		settings.setValue( 'recent_preview_configs', recents )

		# Update the recent menu
		self.update_recent_menu()

	# def _add_recent ( self, filepath )

	# --------------------------------------------------

	def _remove_recent ( self, filepath ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		
		if isinstance( filepath, pytools.path.FilePath ):
			filepath = str(filepath)

		# Get recent preview configs
		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent_preview_configs', [] )
		
		# If filepath is not in them, remove it
		if filepath in recents:
			recents.remove( filepath )

		# Update the recent app settings
		settings.setValue( 'recent_preview_configs', recents )

		# Update the recent menu
		self.update_recent_menu()

	# def _remove_recent ( self, filepath )

	# --------------------------------------------------

	def _set_preview_config ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( value, pytools.Loaded )

		self._preview_config = value
		self._edit_action.setEnabled( value.value is not None )
		self.clear()
		self.populate()

	# def _set_preview_config ( self, value )
	
	# --------------------------------------------------

	def clear ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		
		for action in self._visibility_group:
			action.setParent( None )
			action.deleteLater()

		self._visibility_group = []

	# def clear ( self )

	# --------------------------------------------------

	def populate ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		
		self._visibility_group = []
		
		if self.preview_config is not None:
			
			for preview_settings in self.preview_config.previews:
				action = self.addAction( preview_settings.name )
				action.setCheckable( True )
				action.setChecked( preview_settings.enabled )
				self._visibility_group.append( action )
				
		if not self._visibility_group:
			action = self.addAction( '<empty>' )
			action.setCheckable( False )
			action.setEnabled( False )
			self._visibility_group.append( action )

	# def populate ( self )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Creates the contents of this preview config menu.
	
		Arguments:
			self (`viewer.widgets.PreviewMenu`): Menu to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )

		self._new_action =self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_FileIcon ),
			'Create Config',
			self.on_create_config
			)

		self._open_action = self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DirOpenIcon ),
			'Open Config',
			self.on_open_config
			)

		self._recent_menu = self.addMenu( 'Recent' )
		self._recent_menu.triggered.connect( self.on_recent_clicked )
		self.update_recent_menu()

		self._edit_action = self.addAction(
			pyui.widgets.Application.instance().assets.icon( 'icon_gear.png' ),
			'Edit Config',
			self.on_edit_config
			)
		self._edit_action.setEnabled( False )

		self.addSeparator()

		self.triggered.connect( self.on_action_triggered )

	# def setup ( self )
	
	# --------------------------------------------------

	def update_recent_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )

		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent_preview_configs', [] )
		
		self._recent_menu.blockSignals( True )
		self._recent_menu.clear()
		self._recent_menu.setEnabled( len(recents) > 0 )
		for recent in recents:
			self._recent_menu.addAction( recent )
		self._recent_menu.blockSignals( False )

	# def update_recent_menu ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_create_config ( self ):
		"""
		Slot invoked by the action to create a new preview config.

		Arguments:
			self (`viewer.widgets.PreviewMenu`): This menu.
		"""
		filepath = PyQt5.QtWidgets.QFileDialog.getSaveFileName(
			pyui.widgets.Application.instance().main_window,
			'Create Preview Config',
			'',
			'JSON files (*.json)'
			)[ 0 ]
		
		if filepath:
			preview_config = pytools.Loaded.create( PreviewConfig )
			preview_config.save_as( filepath )
			self._set_preview_config( preview_config )
			self._add_recent( filepath )
			self.config_opened.emit( preview_config.value )

	# def on_create_config ( self )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_open_config ( self ):
		"""
		Slot invoked by the action to open an existing preview config.

		Arguments:
			self (`viewer.widgets.PreviewMenu`): This menu.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )

		filepath = PyQt5.QtWidgets.QFileDialog.getOpenFileName(
			pyui.widgets.Application.instance().main_window,
			'Open Preview Config',
			'',
			'JSON files (*.json)'
			)[ 0 ]
		
		if filepath:
			preview_config = pytools.Loaded.load( filepath )
			self._set_preview_config( preview_config )
			self._add_recent( filepath )
			self.config_opened.emit( preview_config.value )

	# def on_open_config ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_edit_config ( self ):
		"""
		Slot invoked by the action to edit the loaded preview config.

		Arguments:
			self (`viewer.widgets.PreviewMenu`): This menu.
		"""
		dialog = PreviewConfigEditionDialog(
			config = copy.deepcopy( self._preview_config.value ),
			parent = pyui.widgets.Application.instance().main_window
			)
		
		if dialog.exec():
			self._preview_config.value = dialog.value
			self._preview_config.save()
			self._set_preview_config( self._preview_config )
			self.config_changed.emit( self._preview_config.value )

		# if dialog.exec()

	# def on_edit_config ( self )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( PyQt5.QtWidgets.QAction )
	def on_recent_clicked ( self, action ):
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( action, PyQt5.QtWidgets.QAction )

		# The url is the text of the action
		filepath = action.text()
		filepath = pytools.path.FilePath( filepath )
		
		# Check if the dataset exists
		if filepath.is_a_file():
			
			preview_config = pytools.Loaded.load( filepath )
			self._set_preview_config( preview_config )
			self.config_opened.emit( preview_config.value )

		# Otherwise, remove it fron the recent list
		else:
			self._remove_recent( filepath )
			
			info_dialog = pyui.widgets.InfoDialog(
				 title = 'Information',
				  text = 'The preview config "{}" no longer exists and was removed from the list of recently openned preview configs.'.format( filepath ),
				parent = pyui.widgets.Application.instance().main_window
				)
			info_dialog.exec()

		# filepath.is_a_file()
		
	# def on_recent_clicked ( self, action )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( PyQt5.QtWidgets.QAction )
	def on_action_triggered ( self, action ):
		"""
		Slot invoked when one of the actions of this menu is clicked on.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewMenu )
		assert pytools.assertions.type_is_instance_of( action, PyQt5.QtWidgets.QAction )

		children = self.children()
		offset   = 6

		for i in range( offset, len(children) ):

			if children[ i ] is action:
				preview_settings = self._preview_config.value.previews[  i - offset ]
				preview_settings.enabled = not preview_settings.enabled
				self.config_changed.emit( self._preview_config.value )
				return

		# for i in range( offset, len(children) )

	# def on_action_triggered ( self, action )

# class PreviewMenu ( QtWidgets.QMenu )