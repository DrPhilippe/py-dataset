# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .image_preview_settings              import ImagePreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###        CLASS IMAGE-PREVIEW-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DepthPreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'depth_scale_feature_name',
		'near',
		'far'
		]
	)
@RegisterPreviewSettingsAttribute( 'Depth Image' )
class DepthPreviewSettings ( ImagePreviewSettings ):
	"""
	Depth image preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, image_feature_name='', depth_scale_feature_name='', near=0., far=3000., **kwargs ):
		"""
		Initializes a new instance of the depth image preview settings class.
		
		Arguments:
			self (`pyui.previews.DepthPreviewSettings`): Instance to initialize.
			image_feature_name                  (`str`): Name of the image feature to display.
			depth_scale_feature_name            (`str`): Name of the feature containing the depth scale.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( depth_scale_feature_name, str )
		assert pytools.assertions.type_is( near, float )
		assert pytools.assertions.type_is( far, float )
		assert pytools.assertions.true( near <= far )

		super( DepthPreviewSettings, self ).__init__( image_feature_name, **kwargs )

		self._depth_scale_feature_name = depth_scale_feature_name
		self._near = near
		self._far = far

	# def __init__ ( self )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def depth_scale_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )

		return self._depth_scale_feature_name

	# def depth_scale_feature_name ( self )

	# --------------------------------------------------

	@depth_scale_feature_name.setter
	def depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_scale_feature_name = value

	# def depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def near ( self ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )

		return self._near

	# def near ( self )

	# --------------------------------------------------

	@near.setter
	def near ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )
		assert pytools.assertions.type_is( value, float )
		assert pytools.assertions.true( value <= self._far )

		self._near = value

	# def near ( self, value )

	# --------------------------------------------------

	@property
	def far ( self ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )

		return self._far

	# def far ( self )

	# --------------------------------------------------

	@far.setter
	def far ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthPreviewSettings )
		assert pytools.assertions.type_is( value, float )
		# assert pytools.assertions.true( self._near <= value )

		self._far = value

	# def far ( self, value )

# class DepthPreviewSettings ( ImagePreviewSettings )