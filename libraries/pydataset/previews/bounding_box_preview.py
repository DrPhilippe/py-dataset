# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..                             import pose_utils
from ..dataset                      import Example, NDArrayFeatureData
from .bounding_box_preview_settings import BoundingBoxPreviewSettings
from .preview                       import Preview
from .register_preview_attribute    import RegisterPreviewAttribute

# ##################################################
# ###         CLASS BOUNDING-BOX-PREVIEW         ###
# ##################################################

@RegisterPreviewAttribute( 'Bounding Box' )
class BoundingBoxPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( BoundingBoxPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		ok = super( BoundingBoxPreview, self ).check( example )\
		   and example.contains_feature( settings.bounding_box_feature_name, NDArrayFeatureData )

		if not ok:
			return False

		if settings.camera_matrix_feature_name:
		   ok = example.contains_feature( settings.camera_matrix_feature_name, NDArrayFeatureData )

		if not ok:
			return False

		if settings.dist_coeffs_feature_name:
			ok = example.contains_feature( settings.dist_coeffs_feature_name, NDArrayFeatureData )

		if not ok:
			return False

		if settings.rotation_feature_name or settings.translation_feature_name:
		   ok = example.contains_feature( settings.rotation_feature_name,    NDArrayFeatureData )\
		    and example.contains_feature( settings.translation_feature_name, NDArrayFeatureData )
		

		return ok

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Setup graphic item (scale, position, rotation)
		super( BoundingBoxPreview, self ).draw( example )
		
		# Fetch settings
		settings = self.get_settings()

		# Get bounding box
		bb = example.get_feature( settings.bounding_box_feature_name ).value
		
		# Project bounding box
		bb = self.project_points_to_image_plane_if_needed( example, bb )
		
		# Draw bounding box
		self.draw_bounding_box( bb )

	# def draw ( self, example )

	# --------------------------------------------------

	def project_points_to_image_plane_if_needed ( self, example, points ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		assert pytools.assertions.type_is_instance_of( points, numpy.ndarray )

		# Fetch settings
		settings = self.get_settings()
		
		# Do we need to project ?
		if settings.camera_matrix_feature_name:
			
			# Get camera matrix
			K  = example.get_feature( settings.camera_matrix_feature_name ).value
			
			# Get Optiona features
			if settings.dist_coeffs_feature_name:
				dist_coeffs = example.get_feature( settings.dist_coeffs_feature_name ).value
			else:
				dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

			# If rotation and/or translation are spacify use them
			# Because we will go from model space to image plane
			if settings.rotation_feature_name or settings.translation_feature_name:
				r  = example.get_feature( settings.rotation_feature_name      ).value
				t  = example.get_feature( settings.translation_feature_name   ).value
			
			# Otherwise, use 0 rotation and translation
			else:
				r = numpy.eye( 3, dtype=numpy.float32 )
				t = numpy.zeros( [3], dtype=numpy.float32 )	

			# Convert rotation to euler angles
			r = pose_utils.convert_rotation_to_eulers_if_needed( r )
			
			# Project the axes points
			points, jacobian = cv2.projectPoints( points, r, t, K, dist_coeffs )
			points = numpy.reshape( points, [-1, 2] )
		
		# if settings.camera_matrix_feature_name

		return points

	# def project_points_to_image_plane_if_needed ( self, points )

	# --------------------------------------------------

	def draw_bounding_box ( self, bb ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		assert pytools.assertions.type_is_instance_of( bb, numpy.ndarray )	

		# draw the bounding box edges
		self.draw_edge( bb, 0, 1 )
		self.draw_edge( bb, 1, 2 )
		self.draw_edge( bb, 2, 3 )
		self.draw_edge( bb, 3, 0 )

		self.draw_edge( bb, 4, 5 )
		self.draw_edge( bb, 5, 6 )
		self.draw_edge( bb, 6, 7 )
		self.draw_edge( bb, 7, 4 )

		self.draw_edge( bb, 0, 4 )
		self.draw_edge( bb, 1, 5 )
		self.draw_edge( bb, 2, 6 )
		self.draw_edge( bb, 3, 7 )

		# points
		for i in range( 8 ):
			self.draw_point( bb[i, :] )

	# def draw_bounding_box ( self, bb )

	# --------------------------------------------------

	def draw_edge ( self, bb, p1, p2 ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		
		settings = self.get_settings()
		color    = PyQt5.QtGui.QColor( *settings.edges_color.to_list() )
		brush    = PyQt5.QtGui.QBrush( color )
		pen      = PyQt5.QtGui.QPen( brush, settings.edges_width )

		edge = PyQt5.QtWidgets.QGraphicsLineItem( bb[p1,0], bb[p1,1], bb[p2,0], bb[p2,1], self )
		edge.setPen( pen )
		self.addToGroup( edge )
	
	# def draw_edge ( self, bb, p1, p2 )
	
	# --------------------------------------------------

	def draw_point ( self, point ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreview )
		
		settings = self.get_settings()
		color    = PyQt5.QtGui.QColor( *settings.points_color.to_list() )
		brush    = PyQt5.QtGui.QBrush( color )
		pen      = PyQt5.QtGui.QPen( brush, settings.points_width )
		radius   = settings.points_width

		p = PyQt5.QtWidgets.QGraphicsEllipseItem( point[0]-radius/2., point[1]-radius/2., radius, radius, self )
		p.setPen( pen )
		p.setBrush( brush )
		self.addToGroup( p )

	# def draw_point ( self, point )

# class BoundingBoxPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )