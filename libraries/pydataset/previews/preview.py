# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# INTERNALS
from ..dataset         import Example
from .preview_settings import PreviewSettings

# ##################################################
# ###               CLASS PREVIEW                ###
# ##################################################

class Preview:
	"""
	Previews are used to display examples of a dataset.

	They do not display all the feature of an example,
	but instead display neaningfull set of features.

	For instance an image preview would display one image of an example (1 feature),
	and a pose preview would display a mesh at a given rotation and translation (3 features).
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings ):
		"""
		Initializes a new instance of the preview class.

		Arguments:
			self             (`pydataset.previews.Preview`): Preview to initialize.
			example    (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.PreviewSettings`): Settings of this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, PreviewSettings )

		self._example  = example
		self._settings = settings
		self.setup()

	# def __init__ ( self, example, settings )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_example ( self ):
		"""
		Returns the example displayed by this preview.

		Arguments:
			self (`pydataset.previews.Preview`): Preview.

		Returns:
			`pydataset.dataset.Example`: Example displayed by this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )

		return self._example

	# def get_example ( self )

	# --------------------------------------------------

	def get_settings ( self ):
		"""
		Returns the settings of this preview.

		Arguments:
			self (`pydataset.previews.Preview`): Preview.

		Returns:
			`pydataset.previews.PreviewSettings`: Settings of this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )

		return self._settings

	# def get_settings ( self )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_example ( self, example ):
		"""
		Sets the example displayed by this preview.

		Arguments:
			self   (`pydataset.previews.Preview`): Preview.
			example (`pydataset.dataset.Example`): Example displayed by this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		self._example = example
		self.setup()

	# def set_example ( self, example )

	# --------------------------------------------------

	def set_settings ( self, settings ):
		"""
		Sets the settings of this preview.

		Arguments:
			self             (`pydataset.previews.Preview`): Preview.
			settings (`pydataset.previews.PreviewSettings`): Settings of this preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )
		assert pytools.assertions.type_is_instance_of( settings, PreviewSettings )

		self._settings = settings
		self.setup()

	# def set_settings ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self       (`pydataset.previews.Preview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return example is not None

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self       (`pydataset.previews.Preview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		
		settings = self.get_settings()

		# Set its position, rotation and order
		self.setPos( settings.position.x, settings.position.y )
		# self.setRotation( self.get_settings().rotation )
		# self.setZValue( float(self.get_settings().order) )
		# self.setOpacity( self.get_settings().opacity )
		self.setScale( self.get_settings().scale )

	# def draw ( self, example )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this preview.

		Arguments:
			self (`pyui.previews.Preview`): Preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, Preview )

		example  = self.get_example()
		settings = self.get_settings()
		
		if settings.enabled and self.check( example ):
			self.draw( example )

	# def setup ( self )

# class Preview