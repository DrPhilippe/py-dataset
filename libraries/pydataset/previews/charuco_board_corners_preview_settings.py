# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###   CLASS MARKERS-CORNERS-PREVIEW-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoBoardCornersPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'charuco_corners_feature_name',
		'charuco_corners_ids_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Charuco Board Corners' )
class CharucoBoardCornersPreviewSettings ( PreviewSettings ):
	"""
	Dataset aruco marker corners preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, charuco_corners_feature_name='', charuco_corners_ids_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the aruco marekrs cormer preview settings class.
		
		Arguments:
			self (`pydataset.previews.CharucoBoardCornersPreviewSettings`): Instance to initialize.
			charuco_corners_feature_name                           (`str`): Name of the charuco board corner feature to display.
			charuco_corners_ids_feature_name                       (`str`): Name of the charuco board corner ids feature to display.
		
		Named Arguments:
			enabled                        (`bool`): Boolean indicating if this image preview must be displayed.
			position (`pydataset.data.Point2DData`): Position of the preview in the scene.
			name                            (`str`): Name of this image preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreviewSettings )
		assert pytools.assertions.type_is( charuco_corners_feature_name, str )

		super( CharucoBoardCornersPreviewSettings, self ).__init__( **kwargs )

		self._charuco_corners_feature_name = charuco_corners_feature_name
		self._charuco_corners_ids_feature_name = charuco_corners_ids_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_corners_feature_name ( self ):
		"""
		Name of the charuco board corners feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreviewSettings )

		return self._charuco_corners_feature_name

	# def charuco_corners_feature_name ( self )
	
	# --------------------------------------------------

	@charuco_corners_feature_name.setter
	def charuco_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._charuco_corners_feature_name = value

	# def charuco_corners_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def charuco_corners_ids_feature_name ( self ):
		"""
		Name of the charuco board corners ids feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreviewSettings )

		return self._charuco_corners_ids_feature_name

	# def charuco_corners_ids_feature_name ( self )
	
	# --------------------------------------------------

	@charuco_corners_ids_feature_name.setter
	def charuco_corners_ids_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._charuco_corners_ids_feature_name = value

	# def charuco_corners_ids_feature_name ( self, value )

# class CharucoBoardCornersPreviewSettings ( PreviewSettings )