# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###    CLASS INSTANCES-MAP-PREVIEW-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'InstancesMapPreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'instances_map_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Instances Map' )
class InstancesMapPreviewSettings ( PreviewSettings ):
	"""
	Instances map preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, instances_map_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the image preview settings class.
		
		Arguments:
			self (`pyui.previews.InstancesMapPreviewSettings`): Instance to initialize.
			instances_map_feature_name                 (`str`): Name of the feature containing the instances map to display.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreviewSettings )
		assert pytools.assertions.type_is( instances_map_feature_name, str )

		super( InstancesMapPreviewSettings, self ).__init__( **kwargs )

		self._instances_map_feature_name = instances_map_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def instances_map_feature_name ( self ):
		"""
		Name of the feature containing the instances map to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreviewSettings )

		return self._instances_map_feature_name

	# def instances_map_feature_name ( self )
	
	# --------------------------------------------------

	@instances_map_feature_name.setter
	def instances_map_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._instances_map_feature_name = value

	# def instances_map_feature_name ( self, value )

# class InstancesMapPreviewSettings ( PreviewSettings )