# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..data                         import ColorData
from ..dataset                      import Example, ImageFeatureData, NDArrayFeatureData
from .                              import images_utils
from .preview                       import Preview
from .register_preview_attribute    import RegisterPreviewAttribute
from .segmentation_preview_settings import SegmentationPreviewSettings

# ##################################################
# ###         CLASS SEGMENTATION-PREVIEW         ###
# ##################################################

@RegisterPreviewAttribute( 'Segmentation' )
class SegmentationPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the image preview class.

		Arguments:
			self             (`pydataset.previews.SegmentationPreview`): Instances preview to initialize.
			example             (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.SegmentationPreviewSettings`): Settings of this preview.
			parent          (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent graphic.
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, SegmentationPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( SegmentationPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self  (`pydataset.previews.SegmentationPreview`): Preview.
			self        (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return super( SegmentationPreview, self ).check( example )\
		   and example.contains_feature( self.get_settings().segmentation_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self (`pydataset.previews.SegmentationPreview`): Preview.
			self       (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, SegmentationPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( SegmentationPreview, self ).draw( example )
		
		settings = self.get_settings()

		# Get the instances map
		segmentation = example.get_feature( settings.segmentation_feature_name ).value
		H = segmentation.shape[ 0 ]
		W = segmentation.shape[ 1 ]

		def map_value ( value, a, b, A, B ):
			# Figure out how 'wide' each range is
			s = b - a
			S = B - A
			# Convert the left range into a 0-1 range (float)
			value = float( value - a ) / float( s )
			# Convert the 0-1 range into a value in the right range.
			return A + (value * S)

		# Create the image that will be displayed
		drawing = numpy.zeros( [H, W, 4], dtype=numpy.float32 )

		# Set the color for each instances
		for i in range(settings.number_of_categories):

			# Get the mask of the current category
			mask = numpy.equal( segmentation, i )
			mask = numpy.reshape( mask, [H, W, 1] )
			mask = numpy.tile( mask, [1, 1, 4] )

			# 0 is the background
			if settings.dedicated_background and i == 0:
				color = numpy.asarray( [0, 0, 0, 0], dtype=numpy.float32 )
			else:
				# Current category color
				color_index = int(map_value( i, 0, settings.number_of_categories, 1., 19. ))
				color       = ColorData.PREDEFINED_22[ color_index ].to_tuple()
				color       = numpy.asarray( [color[2], color[1], color[0], color[3]], dtype=numpy.float32 ) / 255. # RGB -> BGR float
			
			# Create an image filled with the current category color
			colors = numpy.reshape( color, [1, 1, 4] )
			colors = numpy.tile( colors, [H, W, 1] )

			# Assign the color in the image where pixels belong to the current category
			drawing = numpy.where( mask, colors, drawing )

		# Convert the image to a qpixmap
		drawing = numpy.multiply( drawing, 255. ).astype( numpy.uint8 )
		qpixmap = images_utils.image_to_qpixmap( drawing )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class SegmentationPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview )