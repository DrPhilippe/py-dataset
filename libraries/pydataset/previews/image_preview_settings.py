# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###        CLASS IMAGE-PREVIEW-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagePreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'image_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Image' )
class ImagePreviewSettings ( PreviewSettings ):
	"""
	Image preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, image_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the image preview settings class.
		
		Arguments:
			self (`pyui.previews.ImagePreviewSettings`): Instance to initialize.
			image_feature_name                  (`str`): Name of the image feature to display.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( image_feature_name, str )

		super( ImagePreviewSettings, self ).__init__( **kwargs )

		self._image_feature_name = image_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the image feature to display (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._image_feature_name

	# def image_feature_name ( self )
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

# class ImagePreviewSettings ( PreviewSettings )