# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
from ..data                               import RectangleData
from ..dataset                            import Example, NDArrayFeatureData
from .bounding_rectangle_preview_settings import BoundingRectanglePreviewSettings
from .preview                             import Preview
from .register_preview_attribute          import RegisterPreviewAttribute

# ##################################################
# ###      CLASS BOUNDING-RECTANGLE-PREVIEW      ###
# ##################################################

@RegisterPreviewAttribute( 'Bounding Rectangle' )
class BoundingRectanglePreview ( PyQt5.QtWidgets.QGraphicsRectItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, BoundingRectanglePreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( BoundingRectanglePreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self (`pydataset.previews.BoundingRectanglePreview`): Bounding box preview.
			self        (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		return super( BoundingRectanglePreview, self ).check( example )\
		   and example.contains_feature( settings.bounding_rectangle_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self (`pydataset.previews.BoundingRectanglePreview`): Bounding box preview.
			self      (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingRectanglePreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Draw the ItemPreview
		super( BoundingRectanglePreview, self ).draw( example )
		
		# Get the bounding rectangle
		bounding_rectangle = example.get_feature( self.get_settings().bounding_rectangle_feature_name ).value
		bounding_rectangle = RectangleData.from_ndarray( bounding_rectangle )
		color = self.get_settings().color.to_list()
		color = PyQt5.QtGui.QColor(*color)
		pen   = PyQt5.QtGui.QPen( color, self.get_settings().thickness )
		# Set it
		self.setRect( bounding_rectangle.x1, bounding_rectangle.y1, bounding_rectangle.width, bounding_rectangle.height )
		self.setPen( pen )

	# def draw ( self, example )

# class BoundingRectanglePreview ( PyQt5.QtWidgets.QGraphicsRectItem, Preview )