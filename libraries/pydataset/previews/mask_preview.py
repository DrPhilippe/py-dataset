# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools

# LOCALS
from ..dataset                   import Example, ImageFeatureData, NDArrayFeatureData
from .                           import images_utils
from .mask_preview_settings      import MaskPreviewSettings
from .preview                    import Preview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###            CLASS IMAGE-PREVIEW             ###
# ##################################################

@RegisterPreviewAttribute( 'Mask' )
class MaskPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the mask preview class.

		Arguments:
			self             (`pydataset.previews.MaskPreview`): Mask preview to initialize.
			example        (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.MaskPreviewSettings`): Settings of this preview.
			parent     (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent grapgic.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, MaskPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( MaskPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self   (`pydataset.previews.MaskPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return super( MaskPreview, self ).check( example )\
		   and example.contains_feature( self.get_settings().mask_feature_name, (ImageFeatureData,NDArrayFeatureData) )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example with this preview.

		Arguments:
			self   (`pydataset.previews.MaskPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( MaskPreview, self ).draw( example )
		
		# Get the image feature
		mask = example.get_feature( self.get_settings().mask_feature_name ).value
		
		# Scale mask
		if mask.dtype == numpy.uint8 and numpy.max(mask) == 1:
			mask *= 255

		# Convert the image to a qpixmap
		qpixmap = images_utils.image_to_qpixmap( mask )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class MaskPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview )