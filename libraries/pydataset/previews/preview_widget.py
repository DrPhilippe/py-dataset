# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui

# LOCALS
from ..dataset import Example
from .preview_config  import PreviewConfig
from .preview_factory import PreviewFactory

# ##################################################
# ###            CLASS PREVIEW-WIDGET            ###
# ##################################################

class PreviewWidget ( PyQt5.QtWidgets.QFrame ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example=None, preview_config=None, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( preview_config, (type(None), PreviewConfig) )
		assert pytools.assertions.type_is_instance_of( parent, (PyQt5.QtWidgets.QWidget, type(None)) )

		# Set the items
		self._example        = example
		self._preview_config = preview_config

		# Initialize the dock widget
		super( PreviewWidget, self ).__init__( parent )
		
		# Setup the preview
		self.setup()
		self.update()

	# def __init__ ( parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def example ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		return self._example

	# def example ( self )

	# --------------------------------------------------

	@example.setter
	def example ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Example) )

		# Set the example
		self._example = value

		# update the graphic scene
		self.update()

	# def example ( self, value )

	# --------------------------------------------------

	@property
	def preview_config ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		return self._preview_config

	# def preview_config ( self )

	# --------------------------------------------------

	@preview_config.setter
	def preview_config ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
		assert pytools.assertions.type_is_instance_of( value, PreviewConfig )

		# Set the preview config
		self._preview_config = value

		# update the graphic scene
		self.update()

	# def preview_config ( self, value )

	# --------------------------------------------------

	@property
	def scene ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		return self._scene

	# def scene ( self )

	# --------------------------------------------------

	@property
	def view ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		return self._view
		
	# def view ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def clear ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
		
		self._scene.clear()

	# def clear ( self )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
				
		# Layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Toolbar
		toolbar = PyQt5.QtWidgets.QFrame( self )
		toolbar.setMinimumSize( self.minimumWidth(), 22 )
		toolbar_layout = PyQt5.QtWidgets.QHBoxLayout( toolbar )
		toolbar_layout.setContentsMargins( 0, 0, 0, 0 )
		toolbar_layout.setSpacing( 0 )
		layout.addWidget( toolbar )

		# Toolbar / Save Button
		self._button_save = PyQt5.QtWidgets.QPushButton( toolbar )
		self._button_save.setIcon( pyui.widgets.Application.instance().assets.icon('icon_save.png') )
		self._button_save.pressed.connect( self.on_button_save_pressed )
		self._button_save.setEnabled( False )
		toolbar_layout.addWidget( self._button_save, 0 )

		# Toolbar / Spacer
		spacer = PyQt5.QtWidgets.QWidget( toolbar )
		toolbar_layout.addWidget( spacer, 1 )

		# Toolbar / Dropdown Scale
		self._combobox_scale = PyQt5.QtWidgets.QComboBox( toolbar )
		self._combobox_scale.addItems([
			'25 %',
			'50 %',
			'100 %',
			'200 %',
			'400 %',
			'800 %',
			'1600 %',
			'3200 %',
			])
		self._combobox_scale.setCurrentText( '100 %' )
		self._combobox_scale.currentTextChanged.connect( self.on_scale_changed )
		self._combobox_scale.setEnabled( False )
		toolbar_layout.addWidget( self._combobox_scale, 0 )

		# Create the scroll are to croll big views
		scroll_area = PyQt5.QtWidgets.QScrollArea( self )
		scroll_area.setWidgetResizable( True )
		layout.addWidget( scroll_area )

		# Create the scene to display the features
		self._scene = PyQt5.QtWidgets.QGraphicsScene()
		
		# Create the view to display the scene
		self._view = PyQt5.QtWidgets.QGraphicsView()
		self._view.setScene( self._scene )
		scroll_area.setWidget( self._view )

	# def setup( self )

	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		super( PreviewWidget, self ).update()

		self.clear()
		self.populate()
		self._scene.setSceneRect( self._scene.itemsBoundingRect() )

	# def update( self )

	# --------------------------------------------------

	def populate ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )
		
		self._button_save.setEnabled( False )
		self._combobox_scale.setEnabled( False )

		if self._example is None:
			return

		if self._preview_config is None or self._preview_config.number_of_previews == 0:
			return

		for preview_settings in self._preview_config.previews:
			preview = PreviewFactory.instantiate_preview( settings=preview_settings, example=self._example )
			self._scene.addItem( preview )

		self._button_save.setEnabled( True )
		self._combobox_scale.setEnabled( True )

	# def populate ( self )
	
	# --------------------------------------------------

	def save ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		self._scene.setSceneRect( self._scene.itemsBoundingRect() )
		self._scene.clearSelection()
		rect = self._scene.sceneRect().toRect()
		
		# Create an image with the save size as the scene
		image = PyQt5.QtGui.QImage( rect.width(), rect.height(), PyQt5.QtGui.QImage.Format_ARGB32 )

		# Create a painter targeting the image
		painter = PyQt5.QtGui.QPainter( image )
		painter.setRenderHint( PyQt5.QtGui.QPainter.Antialiasing )
		
		# Paint the scene on the image using the painter
		self._scene.render( painter )
		
		# End the painting
		painter.end()

		# Get the name of the file where to save the image
		filename = PyQt5.QtWidgets.QFileDialog.getSaveFileName( self, 'Save Preview', '', 'Images (*.png *.jpg)' )[ 0 ]

		# If the dialog was not canceled
		if filename:

			# Save the image
			image.save( filename )

	# def save ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################


	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_save_pressed ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		self.save()

	# def on_button_save_pressed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_scale_changed ( self, text ):
		assert pytools.assertions.type_is_instance_of( self, PreviewWidget )

		text = text[ :-2 ]
		scale = float(text) / 100.0
		self._view.resetTransform()
		self._view.scale( scale, scale )

	# def on_scale_changed ( self, text )

# class PreviewWidget ( PyQt5.QtWidgets.QFrame )