# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .preview_settings_factory import PreviewSettingsFactory

# ##################################################
# ### CLASS REGISTER-PREVIEW-SETTINGS-ATTRIBUTE  ###
# ##################################################

class RegisterPreviewSettingsAttribute ( pytools.factory.RegisterClassAttribute ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name ):
		"""
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterPreviewSettingsAttribute )
		assert pytools.assertions.type_is( name, str )

		super( RegisterPreviewSettingsAttribute, self ).__init__(
			   group = PreviewSettingsFactory.group_name,
			typename = name
			)

	# def __init__ ( self, name )
	
	# ##################################################
	# ###                  OPERATORS                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __call__ ( self, cls ):
		"""
		Registers the class `cls` in the `pytools.factory.ClassFactory` in the `'previews settings'` group.

		Arguments:
			self (`pyui.previews.RegisterPreviewSettingsAttribute`): The attribute.
			cls                                            (`type`): The class to register.

		Returns
			`type`: The class onced registered.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterPreviewSettingsAttribute )

		# Register the preview settings type
		cls.__preview_name__ = self.typename
		PreviewSettingsFactory.register_preview_settings( self.typename, cls )
		
		return cls
		
	# def __call__ ( self, cls )
	
# class RegisterPreviewSettingsAttribute ( pytools.factory.RegisterClassAttribute )