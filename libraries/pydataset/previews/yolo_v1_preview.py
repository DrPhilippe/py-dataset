# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..data                      import ColorData
from ..dataset                   import Example, NDArrayFeatureData
from .yolo_v1_preview_settings   import YoloV1PreviewSettings
from .preview                    import Preview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###           CLASS YOLO-V1-PREVIEW            ###
# ##################################################

@RegisterPreviewAttribute( 'YOLO V1' )
class YoloV1Preview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1Preview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, YoloV1PreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( YoloV1Preview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		return super( YoloV1Preview, self ).check( example )\
		   and example.contains_feature( settings.yolo_feature_name, NDArrayFeatureData )\
		   and example.contains_feature( settings.image_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, YoloV1Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		

		# Setup graphic item (scale, position, rotation)
		super( YoloV1Preview, self ).draw( example )
		
		# Fetch settings
		settings = self.get_settings()
		
		# Get the image
		image        = example.get_feature( settings.image_feature_name ).value
		image_width  = image.shape[ 1 ]
		image_height = image.shape[ 0 ]

		# Get yolo labels
		yolo_labels = example.get_feature( settings.yolo_feature_name ).value
		
		# Number of cells
		number_of_cells = yolo_labels.shape[ 0 ]

		# Compute cell width
		cell_width  = float(image_width)  / float(number_of_cells)
		cell_height = float(image_height) / float(number_of_cells)
		
		# Draw cells
		for cell_index_x in range( number_of_cells ):
			for cell_index_y in range( number_of_cells ):

				# Compute cell coordinates
				cell_x1 = float(cell_index_x + 0) * cell_width
				cell_y1 = float(cell_index_y + 0) * cell_height
				cell_x2 = float(cell_index_x + 1) * cell_width
				cell_y2 = float(cell_index_y + 1) * cell_height

				# Fetch cell labels
				cell_labels = yolo_labels[ cell_index_y, cell_index_x, : ]

				# Is the cell empty ?
				if cell_labels[ 4 ] == 0.:
					
					# Draw a gray rectangle around it
					color = PyQt5.QtGui.QColor( 128, 128, 128, 255 )
					brush = PyQt5.QtGui.QBrush( color )
					pen   = PyQt5.QtGui.QPen( brush, 2.0 )
					rect = PyQt5.QtWidgets.QGraphicsRectItem( cell_x1, cell_y1,	cell_width, cell_height, self )
					rect.setPen( pen )
					self.addToGroup( rect )

					# Move on to the next cell
					continue

				# Fetch bounding rectangle and one hot category labels
				bounding_rectangle = cell_labels[ 0: 4 ]
				one_hot_category   = cell_labels[ 5:-1 ]

				# Compute category
				category_index       = int( numpy.argmax( one_hot_category ) )
				number_of_categories = int( one_hot_category.shape[ 0 ] )

				# Current instance color if the cell is not empty
				color_index = pytools.math.map_value( category_index, 0, number_of_categories, 0, 20, int )
				color       = copy.deepcopy( ColorData.PREDEFINED_22[ color_index ] )

				# Create Qt pens and brushes
				pen_color   = PyQt5.QtGui.QColor( *color.to_tuple() )
				pen_brush   = PyQt5.QtGui.QBrush( pen_color )
				pen_1       = PyQt5.QtGui.QPen( pen_brush, 1.0 )
				pen_2       = PyQt5.QtGui.QPen( pen_brush, 2.0 )
				color.alpha = 48
				brush_color = PyQt5.QtGui.QColor( *color.to_tuple() )
				brush       = PyQt5.QtGui.QBrush( brush_color )
				
				# Draw cells as a rectangle with a color to represent the category
				rect = PyQt5.QtWidgets.QGraphicsRectItem( cell_x1, cell_y1,	cell_width, cell_height, self )
				rect.setPen( pen_2 )
				rect.setBrush( brush )
				rect.setZValue( rect.zValue()+1 )
				self.addToGroup( rect )

				# Display category as text
				text = PyQt5.QtWidgets.QGraphicsSimpleTextItem( '{}/{}'.format(category_index, number_of_categories), self )
				text.setPen( pen_1 )
				text.setPos( cell_x1+2, cell_y1+2 )
				text.setZValue( text.zValue()+1 )
				self.addToGroup( text )
				
				# Unpack bounding rectangle
				rect_x1       = (bounding_rectangle[ 0 ] * image_width ) if settings.normalized else bounding_rectangle[ 0 ]
				rect_y1       = (bounding_rectangle[ 1 ] * image_height) if settings.normalized else bounding_rectangle[ 1 ]
				rect_x2       = (bounding_rectangle[ 2 ] * image_width ) if settings.normalized else bounding_rectangle[ 2 ]
				rect_y2       = (bounding_rectangle[ 3 ] * image_height) if settings.normalized else bounding_rectangle[ 3 ]
				rect_width    = rect_x2 - rect_x1
				rect_height   = rect_y2 - rect_y1
				rect_center_x = (rect_x1 + rect_x2)/2.
				rect_center_y = (rect_y1 + rect_y2)/2.

				# Draw the center point
				ellipse = PyQt5.QtWidgets.QGraphicsEllipseItem( rect_center_x-2, rect_center_y-2, 4, 4, self )
				ellipse.setPen( pen_1 )
				ellipse.setBrush( pen_brush )
				ellipse.setZValue( text.zValue()+2 )
				self.addToGroup( ellipse )

				# Draw bounding rectangle
				rect = PyQt5.QtWidgets.QGraphicsRectItem( rect_x1, rect_y1, rect_width, rect_height, self )
				rect.setPen( pen_2 )
				rect.setZValue( rect.zValue()+2 )
				self.addToGroup( rect )

				# for detector_index in range( settings.number_of_detectors_per_cell )

			# for cell_index_y in range( number_of_cells_on_height ):

		# for cell_index_x in range( number_of_cells_on_width ):

	# def draw ( self, example )

# class BoundingBoxPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )