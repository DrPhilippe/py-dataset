# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                               import Example, NDArrayFeatureData
from .charuco_board_corners_preview_settings import CharucoBoardCornersPreviewSettings
from .preview                                import Preview
from .register_preview_attribute             import RegisterPreviewAttribute

# ##################################################
# ###       CLASS CHARUCO-CORNERS-PREVIEW        ###
# ##################################################

@RegisterPreviewAttribute( 'Charuco Board Corners' )
class CharucoBoardCornersPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, CharucoBoardCornersPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( CharucoBoardCornersPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self (`pydataset.previews.CharucoBoardCornersPreview`): Preview.
			self              (`None`/`pydataset.dataset.Example`): Example to preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		return super( CharucoBoardCornersPreview, self ).check( example )\
		   and example.contains_feature( settings.charuco_corners_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self (`pydataset.previews.CharucoBoardCornersPreview`): Preview.
			self              (`None`/`pydataset.dataset.Example`): Example to preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Draw the ItemPreview
		super( CharucoBoardCornersPreview, self ).draw( example )
		
		# Get the feature
		charuco_corners = example.get_feature( self.get_settings().charuco_corners_feature_name ).value

		if self.get_settings().charuco_corners_ids_feature_name:
			charuco_ids = example.get_feature( self.get_settings().charuco_corners_ids_feature_name ).value
		else:
			charuco_ids = None

		# Draw the charuco corners
		self.draw_charuco_corners( charuco_corners, charuco_ids )

	# def draw ( self, example )

	# --------------------------------------------------

	def draw_charuco_corners ( self, charuco_corners, charuco_ids ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardCornersPreview )
		assert pytools.assertions.type_is_instance_of( charuco_corners, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( charuco_ids, (numpy.ndarray, type(None)) )
		
		number_of_corners = charuco_corners.shape[ 0 ]

		brush = PyQt5.QtGui.QBrush()
		color = PyQt5.QtGui.QColor( 0, 255, 255 ) 
		pen   = PyQt5.QtGui.QPen( color, 2 )

		for corner_index in range(number_of_corners):
			
			charuco_corner = charuco_corners[ corner_index, : ]
			
			if not numpy.isnan( numpy.sum(charuco_corner) ):

				# Get the position of the corner
				charuco_corner_x = charuco_corner[ 0 ]
				charuco_corner_y = charuco_corner[ 1 ]

				# Create a rectangle around it
				rect = PyQt5.QtCore.QRectF( charuco_corner_x-2.0, charuco_corner_y-2.0, 4.0, 4.0 )
				item = PyQt5.QtWidgets.QGraphicsRectItem( rect, self )
				item.setBrush( brush )
				item.setPen( pen )
				self.addToGroup( item )

				# Create a text at the center for the id
				if charuco_ids is not None:
					item = PyQt5.QtWidgets.QGraphicsSimpleTextItem( str(charuco_ids[corner_index]), self )
					item.setBrush( color )
					font = item.font()
					font.setBold( True )
					font.setPointSize( 12 )
					item.setFont( font )
					item.setPos( PyQt5.QtCore.QPointF(charuco_corner_x, charuco_corner_y) )
					self.addToGroup( item )

			# if not numpy.isnan( numpy.sum(charuco_corner) )

		# for corner_index in range(number_of_corners)

	# def draw_charuco_corners ( self, charuco_corners ):

# class CharucoBoardCornersPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )