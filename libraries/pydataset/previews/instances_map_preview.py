# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..data                          import ColorData
from ..dataset                       import Example, ImageFeatureData, NDArrayFeatureData
from .                               import images_utils
from .instances_map_preview_settings import InstancesMapPreviewSettings
from .preview                        import Preview
from .register_preview_attribute     import RegisterPreviewAttribute

# ##################################################
# ###        CLASS INSTANCES-MAP-PREVIEW         ###
# ##################################################

@RegisterPreviewAttribute( 'Instances Map' )
class InstancesMapPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the image preview class.

		Arguments:
			self             (`pydataset.previews.InstancesMapPreview`): Instances preview to initialize.
			example                (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.InstancesMapPreviewSettings`): Settings of this preview.
			parent             (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent graphic.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, InstancesMapPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( InstancesMapPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self  (`pydataset.previews.InstancesMapPreview`): Preview.
			self        (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return super( InstancesMapPreview, self ).check( example )\
		   and example.contains_feature( self.get_settings().instances_map_feature_name, NDArrayFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self (`pydataset.previews.InstancesMapPreview`): Preview.
			self       (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( InstancesMapPreview, self ).draw( example )
		
		# Get the instances map
		instances = example.get_feature( self.get_settings().instances_map_feature_name ).value
		shape     = (instances.shape[0], instances.shape[1], 4 ) # width, height, rgba

		# Largest object index
		max_index = numpy.amax( instances, axis=None )

		def map_value ( value, a, b, A, B):
			# Figure out how 'wide' each range is
			s = b - a
			S = B - A
			# Convert the left range into a 0-1 range (float)
			value = float( value - a ) / float( s )
			# Convert the 0-1 range into a value in the right range.
			return A + (value * S)

		# Create the image that will be displayed
		image = numpy.zeros( shape, numpy.uint8 )
		image[ :, :, 3 ] = 255

		# Set the color for each instances
		for i in range(1, max_index+1):

			# Current instance color
			color_index = int(map_value( i, 1, max_index+1, 0., 20. ))
			color = ColorData.PREDEFINED_22[ color_index ].to_tuple()
			color = ( color[2], color[1], color[0], color[3] )# RGB -> BGR
			
			# Create an image filled with the current instance color
			colors = numpy.empty( shape, numpy.uint8 )
			colors[ : ] = color
			
			# Pixels indexes corresponding to the current instance
			indexes = (instances == i)
			indexes = numpy.repeat( indexes, 4 )
			indexes = numpy.reshape( indexes, shape )

			# Assign the color in the image where pixels belong to the current instance
			image = numpy.where( indexes, colors, image )

		# Convert the image to a qpixmap
		qpixmap = images_utils.image_to_qpixmap( image )
		self.setPixmap( qpixmap )

	# def draw ( self )

# class InstancesMapPreview ( PyQt5.QtWidgets.QGraphicsPixmapItem, Preview )