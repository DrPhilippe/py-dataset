# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtCore
import PyQt5.QtGui

# INTERNALS
import pytools.assertions
import pytools.path

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def image_to_qpixmap ( image ):
	
	if image is None:
		qpixmap = PyQt5.QtGui.QPixmap()
		return PyQt5.QtGui.QImage( qpixmap )

	shape  = image.shape
	dtype  = image.dtype
	rank   = len(shape)
	value  = image
	height = shape[ 0 ] if rank > 0 else 1
	width  = shape[ 1 ] if rank > 1 else 1
	depth  = shape[ 2 ] if rank > 2 else 1
	bytes_per_line = depth * width

	# if the shape is not set, or does not reprensent an image
	if ( rank not in [1, 2, 3] ):
		qpixmap = PyQt5.QtGui.QPixmap( width, height )
		qpixmap.fill( PyQt5.QtGui.QColor.magenta )
		return qpixmap

	# Normalize and Convert to unsinged bytes:
	if dtype != numpy.uint8:
		
		# convert the array to float64
		value = value.astype( numpy.float64 )

		# replace infinit number by -1 
		indexes_inf = (value == numpy.float64('inf'))
		value[ indexes_inf ] = -1

		# normalize
		if ( depth == 1 ):
			maximum = numpy.amax( value, axis=(0,1) )
			if maximum > 0.:
				value = value / maximum * float(255)
		else:
			maximum = numpy.amax( value, axis=(0,1) )
			if maximum > 0.:
				value = value / maximum * numpy.repeat( float(255), depth )

		# replace previously infinit values with 255
		value[ indexes_inf ] = float(255)

		# convert to uint8
		value = value.astype( numpy.uint8 )

	# largest = max( width, height )
	# ratio   = float(max_size) / float(largest)
	# value   = cv2.resize( value, dsize=None, fx=ratio, fy=ratio )
	# print( 'value', value.shape )
	
	if depth == 3:
		qimage  = PyQt5.QtGui.QImage( value.data, width, height, bytes_per_line, PyQt5.QtGui.QImage.Format_RGB888 ).rgbSwapped()
		qpixmap = PyQt5.QtGui.QPixmap( qimage )
		return qpixmap
	
	elif depth == 4:
		value  = numpy.require( value, numpy.uint8, 'C' )
		qimage = PyQt5.QtGui.QImage( value.data, width, height, value.strides[0], PyQt5.QtGui.QImage.Format_ARGB32 )
		qpixmap = PyQt5.QtGui.QPixmap( qimage )
		return qpixmap

	elif depth == 1:
		qimage = PyQt5.QtGui.QImage( value.data, width, height, bytes_per_line, PyQt5.QtGui.QImage.Format_Grayscale8 )
		qpixmap = PyQt5.QtGui.QPixmap( qimage )
		return qpixmap

	else:
		qpixmap = PyQt5.QtGui.QPixmap( width, height )
		qpixmap.fill( PyQt5.QtCore.Qt.magenta )
		return qpixmap

# def image_to_qpixmap ( image )

# --------------------------------------------------

def clamp_image_to_size ( image, max_size ):

	shape      = image.shape
	width      = shape[ 1 ]
	height     = shape[ 0 ]
	max_width  = max_size[ 0 ]
	max_height = max_size[ 1 ]

	if width <= max_width and height <= max_height:
		return image

	else:
		width_ratio = float(width)/float(max_width) 
		height_ratio = float(height)/float(max_height)
		ratio = max( width_ratio, height_ratio )
		new_width = int( float(width)/ratio )
		new_height = int( float(height)/ratio )
		return cv2.resize( image, (new_width, new_height) )

# --------------------------------------------------

def save_image ( filepath, image ):

	dirpath  = filepath.parent()
	filename = filepath.filename()

	dtype = image.dtype
	shape = image.shape
	rank  = len(shape)
	depth = shape[2] if rank > 2 else 1

	if dtype == numpy.uint8 and depth in range(1, 5):
		filepath = dirpath + pytools.path.FilePath.format( '{}.jpg', filename )
		cv2.imwrite( str(filepath), image )

	else:
		filepath = dirpath + pytools.path.FilePath.format( '{}.npy', filename )
		numpy.save( str(filepath), image, True )