# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###     CLASS DEPTH-ERROR-PREVIEW-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImagePreviewSettings',
	   namespace = 'pyui.previews',
	fields_names = [
		'depth_feature_name',
		'depth_scale_feature_name',
		'render_depth_feature_name',
		'render_depth_scale_feature_name',
		'mask_feature_name'
		]
	)
@RegisterPreviewSettingsAttribute( 'Depth Error' )
class DepthErrorPreviewSettings ( PreviewSettings ):
	"""
	Depth error preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, depth_feature_name='', depth_scale_feature_name='', render_depth_feature_name='', render_depth_scale_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the image preview settings class.
		
		Arguments:
			self (`pyui.previews.DepthErrorPreviewSettings`): Instance to initialize.
			depth_feature_name                       (`str`): Name of the feature containing the pictured depth map.
			depth_scale_feature_name                 (`str`): Name of the feature containing the pictured depth map scale.
			render_depth_feature_name                (`str`): Name of the feature containing the rendered depth map.
			render_depth_scale_feature_name          (`str`): Name of the feature containing the rendered depth map scale.
			mask_feature_name                        (`str`): Name of the feature containing the mask.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( depth_feature_name, str )

		super( DepthErrorPreviewSettings, self ).__init__( **kwargs )

		self._depth_feature_name              = depth_feature_name
		self._depth_scale_feature_name        = depth_scale_feature_name
		self._render_depth_feature_name       = render_depth_feature_name
		self._render_depth_scale_feature_name = render_depth_scale_feature_name
		self._mask_feature_name               = mask_feature_name

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		"""
		Name of the feature containing the pictured depth map (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._depth_feature_name

	# def depth_feature_name ( self )
	
	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_feature_name = value

	# def depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_scale_feature_name ( self ):
		"""
		Name of the feature containing the pictured depth map scale (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._depth_scale_feature_name

	# def depth_scale_feature_name ( self )
	
	# --------------------------------------------------

	@depth_scale_feature_name.setter
	def depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_scale_feature_name = value

	# def depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_depth_feature_name ( self ):
		"""
		Name of the feature containing the rendered depth map (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._render_depth_feature_name

	# def render_depth_feature_name ( self )
	
	# --------------------------------------------------

	@render_depth_feature_name.setter
	def render_depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._render_depth_feature_name = value

	# def render_depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_depth_scale_feature_name ( self ):
		"""
		Name of the feature containing the rendered depth map scale (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._render_depth_scale_feature_name

	# def render_depth_scale_feature_name ( self )
	
	# --------------------------------------------------

	@render_depth_scale_feature_name.setter
	def render_depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._render_depth_scale_feature_name = value

	# def render_depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )
	
	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImagePreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

# class ImagePreviewSettings ( PreviewSettings )