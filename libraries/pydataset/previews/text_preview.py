# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                   import Example
from .preview                    import Preview
from .register_preview_attribute import RegisterPreviewAttribute
from .text_preview_settings      import TextPreviewSettings

# ##################################################
# ###             CLASS TEXT-PREVIEW             ###
# ##################################################

@RegisterPreviewAttribute( 'Text' )
class TextPreview ( PyQt5.QtWidgets.QGraphicsTextItem, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		"""
		Initializes a new instance of the textual preview class.

		Arguments:
			self             (`pydataset.previews.TextPreview`): Textual preview to initialize.
			example        (`None`/`pydataset.dataset.Example`): Example displayed by this preview.
			settings (`pydataset.previews.TextPreviewSettings`): Settings of this preview.
			parent     (`None`/`PyQt5.QtWidgets.QGraphicsItem`): Parent grapgic.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( settings, TextPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )

		# Initialize the parent classes
		super( TextPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)
	
	# def __init__ ( self, example, settings, parent )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self   (`pydataset.previews.TextPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		feature_name = self.get_settings().feature_name
		
		return super( TextPreview, self ).check( example )\
		   and example.contains_feature( feature_name )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self   (`pydataset.previews.TextPreview`): Preview.
			self (`None`/`pydataset.dataset.Example`): Example to draw.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Update the ItemPreview
		super( TextPreview, self ).draw( example )
		
		# Get the feature
		feature_name  = self.get_settings().feature_name
		feature       = example.get_feature( feature_name )
		feature_value = feature.value

		# Convert the value to text
		text_format = self.get_settings().text_format
		text        = text_format.format( feature_value )

		color = self.get_settings().text_color

		# Set the text
		self.setPlainText( text )
		self.setDefaultTextColor( PyQt5.QtGui.QColor(*color.to_list()) )

	# def draw ( self, example )

# class TextPreview ( PyQt5.QtWidgets.QGraphicsTextItem, Preview )