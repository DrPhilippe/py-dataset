# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings import PreviewSettings

# ##################################################
# ###            CLASS PREVIEW-CONFIG            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PreviewConfig',
	   namespace = 'pydataset.previews',
	fields_names = [
		'previews'
		]
	)
class PreviewConfig ( pytools.serialization.Serializable ):
	"""
	Dataset preview config.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, previews=[] ):
		"""
		Initializes a new instance of the previews settings class.
		
		Arguments:
			self (`pydataset.previews.PreviewConfig`): Instance to initialize.
			previews    (`list` of `PreviewSettings`): The previews of this config.
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewConfig )
		assert pytools.assertions.type_is( previews, list )
		assert pytools.assertions.list_items_type_is( previews, PreviewSettings )

		self._previews = list( previews )

	# def __init__ ( self, previews )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def number_of_previews ( self ):
		"""
		Number of preview in this config (`int`).
		"""
		return len( self._previews )
	
	# --------------------------------------------------

	@property
	def previews ( self ):
		"""
		Items previews settings (`list` of `pyui.previews.PreviewConfig`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PreviewConfig )

		return self._previews
	
	# def previews ( self )

	# --------------------------------------------------

	@previews.setter
	def previews ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewConfig )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, PreviewSettings )
		
		self._previews = value
	
	# def previews ( self, value )

# class PreviewConfig ( pytools.serialization.Serializable )