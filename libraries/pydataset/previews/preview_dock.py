# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.previews
import pytools.assertions
import pyui.widgets

# LOCALS
from ..dataset import Example
from .preview_config import PreviewConfig 
from .preview_widget import PreviewWidget

# ##################################################
# ###             CLASS PREVIEW-DOCK             ###
# ##################################################

class PreviewDock ( pyui.widgets.Dock ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example=None, preview_config=None, title='Previews', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )
		assert pytools.assertions.type_is_instance_of( example, (type(None), PreviewConfig) )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is_instance_of( parent, PyQt5.QtWidgets.QWidget )

		# Set the items
		self._example = example
		self._preview_config = preview_config

		# Initialize the dock widget
		super( PreviewDock, self ).__init__( title, parent )
		
	# def __init__ ( parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def example ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )

		return self._preview_widget.example

	# def example ( self )

	# --------------------------------------------------

	@example.setter
	def example ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Example) )

		# Set the example
		self._preview_widget.example = value

	# def example ( self, value )

	# --------------------------------------------------

	@property
	def preview_config ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )

		return self._preview_widget.preview_config

	# def preview_config ( self )

	# --------------------------------------------------

	@preview_config.setter
	def preview_config ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )
		assert pytools.assertions.type_is_instance_of( value, PreviewConfig )

		# Set the preview config
		self._preview_widget.preview_config = value

	# def preview_config ( self, value )

	# --------------------------------------------------

	@property
	def preview_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )

		return self._preview_widget

	# def preview_widget ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, PreviewDock )
		
		super( PreviewDock, self ).setup()
		
		self._preview_widget = PreviewWidget( self._example, self._preview_config, self )
		self.setWidget( self._preview_widget )

	# def setup( self )

# class PreviewDockWidget ( pyui.widgets.Dock )