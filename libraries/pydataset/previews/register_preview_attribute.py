# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .preview_factory import PreviewFactory

# ##################################################
# ###      CLASS REGISTER-PREVIEW-ATTRIBUTE      ###
# ##################################################

class RegisterPreviewAttribute ( pytools.factory.RegisterClassAttribute ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name ):
		"""
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterPreviewAttribute )
		assert pytools.assertions.type_is( name, str )

		super( RegisterPreviewAttribute, self ).__init__(
			   group = PreviewFactory.group_name,
			typename = name
			)

	# def __init__ ( self, name )
	
	# ##################################################
	# ###                  OPERATORS                 ###
	# ##################################################
	
	# --------------------------------------------------

	def __call__ ( self, cls ):
		"""
		Registers the class `cls` in the `pytools.factory.ClassFactory` in the `'previews'` group.

		Arguments:
			self (`pyui.previews.RegisterPreviewAttribute`): The attribute.
			cls                                            (`type`): The class to register.

		Returns
			`type`: The class oncd registered.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterPreviewAttribute )

		# Register the preview type
		cls.__preview__name__ = self.typename
		PreviewFactory.register_preview( self.typename, cls )
		
		return cls
		
	# def __call__ ( self, cls )
	
# class RegisterPreviewAttribute ( pytools.factory.RegisterClassAttribute )