# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .bounding_box_preview_settings       import BoundingBoxPreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ### CLASS BB8-CONTROL-POINTS-V2-PREVIEW-SETTINGS ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BB8PreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = []
	)
@RegisterPreviewSettingsAttribute( 'BB8' )
class BB8PreviewSettings ( BoundingBoxPreviewSettings ):
	"""
	Draws BB8 control points.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.previews.BB8PreviewSettings` class.
		
		Arguments:
			self (`pydataset.previews.BB8PreviewSettings`): Instance to initialize.
		
		Named Arguments:
			bounding_box_feature_name                             (`str`): Name of the feature containing the bb8 points.
			points_color                     (`pydataset.data.ColorData`): Color used to draw the points.
			points_width                                        (`float`): Display width of the points.
			edges_color                      (`pydataset.data.ColorData`): Color used to draw the edges.
			edges_width                                         (`float`): Line width of the edges.
		"""
		assert pytools.assertions.type_is_instance_of( self, BB8PreviewSettings )

		super( BB8PreviewSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

# class BB8PreviewSettings ( BoundingBoxPreviewSettings )