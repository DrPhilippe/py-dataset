# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###      CLASS LCS-AXES-PREVIEW-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BoundingBoxPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'bounding_box_feature_name',
		'camera_matrix_feature_name',
		'dist_coeffs_feature_name',
		'rotation_feature_name',
		'translation_feature_name',
		'edges_color',
		'edges_width',
		'points_color',
		'points_width'
		]
	)
@RegisterPreviewSettingsAttribute( 'Bounding Box' )
class BoundingBoxPreviewSettings ( PreviewSettings ):
	"""
	Draw a bounding box in a given pose (K*[R|t]).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		bounding_box_feature_name = '',
		camera_matrix_feature_name = '',
		dist_coeffs_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		points_color = pydataset.data.ColorData(255, 0, 0),
		points_width = 3.0,
		edges_color = pydataset.data.ColorData(0, 255, 0),
		edges_width = 1.5,
		**kwargs
		):
		"""
		Initializes a new instance of the aruco marekrs cormer preview settings class.
		
		Arguments:
			self (`pydataset.previews.BoundingBoxPreviewSettings`): Instance to initialize.
			bounding_box_feature_name                      (`str`): Name of the feature containing the bounding-box points.
			camera_matrix_feature_name                     (`str`): Name of the feature containing the camera parameters matrix.
			dist_coeffs_feature_name                       (`str`): Name of the feature containing the camera distortion coefficients vector (optional).
			rotation_feature_name                          (`str`): Name of the feature containing the rotation vector or matrix.
			translation_feature_name                       (`str`): Name of the feature containing the translation vector.
			points_color              (`pydataset.data.ColorData`): Color used to draw the points.
			points_width                                 (`float`): Display width of the points.
			edges_color               (`pydataset.data.ColorData`): Color used to draw the edges.
			edges_width                                  (`float`): Line width of the edges.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( bounding_box_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is_instance_of( points_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is( points_width, float )
		assert pytools.assertions.type_is_instance_of( edges_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is( edges_width, float )

		super( BoundingBoxPreviewSettings, self ).__init__( **kwargs )

		self._bounding_box_feature_name  = bounding_box_feature_name
		self._camera_matrix_feature_name = camera_matrix_feature_name
		self._dist_coeffs_feature_name   = dist_coeffs_feature_name
		self._rotation_feature_name      = rotation_feature_name
		self._translation_feature_name   = translation_feature_name
		self._points_color                = copy.deepcopy( points_color )
		self._points_width                = points_width
		self._edges_color                 = copy.deepcopy( edges_color )
		self._edges_width                 = edges_width

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def bounding_box_feature_name ( self ):
		"""
		Name of the feature containing the bounding-box points (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._bounding_box_feature_name

	# def bounding_box_feature_name ( self )
	
	# --------------------------------------------------

	@bounding_box_feature_name.setter
	def bounding_box_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_box_feature_name = value

	# def bounding_box_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._camera_matrix_feature_name

	# def camera_matrix_feature_name ( self )
	
	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_matrix_feature_name = value

	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		"""
		Name of the feature containing the camera distortion coefficients vector (optional) (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._dist_coeffs_feature_name

	# def dist_coeffs_feature_name ( self )
	
	# --------------------------------------------------

	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._dist_coeffs_feature_name = value

	# def dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation vector or matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._rotation_feature_name

	# def rotation_feature_name ( self )
	
	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_feature_name = value

	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._translation_feature_name

	# def translation_feature_name ( self )
	
	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_feature_name = value

	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def points_color ( self ):
		"""
		Color used to draw the points (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._points_color

	# def points_color ( self )
	
	# --------------------------------------------------

	@points_color.setter
	def points_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._points_color = copy.deepcopy( value )

	# def points_color ( self, value )
	
	# --------------------------------------------------

	@property
	def points_width ( self ):
		"""
		Display width of the points (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._points_width

	# def points_width ( self )
	
	# --------------------------------------------------

	@points_width.setter
	def points_width ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._points_width = value

	# def points_width ( self, value )

	# --------------------------------------------------

	@property
	def edges_color ( self ):
		"""
		Color used to draw the edges (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._edges_color

	# def edges_color ( self )
	
	# --------------------------------------------------

	@edges_color.setter
	def edges_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._edges_color = copy.deepcopy( value )

	# def edges_color ( self, value )
	
	# --------------------------------------------------

	@property
	def edges_width ( self ):
		"""
		Line width of the edges (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )

		return self._edges_width

	# def edges_width ( self )
	
	# --------------------------------------------------

	@edges_width.setter
	def edges_width ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BoundingBoxPreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._edges_width = value

	# def edges_width ( self, value )

# class BoundingBoxPreviewSettings ( PreviewSettings )