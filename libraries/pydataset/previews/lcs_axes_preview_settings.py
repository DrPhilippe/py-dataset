# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ##################################################
# ###      CLASS LCS-AXES-PREVIEW-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'LCSAxesPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'camera_matrix_feature_name',
		'dist_coeffs_feature_name',
		'rotation_feature_name',
		'translation_feature_name',
		'axes_length'
		]
	)
@RegisterPreviewSettingsAttribute( 'LCS Axes' )
class LCSAxesPreviewSettings ( PreviewSettings ):
	"""
	Draw local coordinates origin as 3 axes.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		camera_matrix_feature_name='',
		dist_coeffs_feature_name='',
		rotation_feature_name='',
		translation_feature_name='',
		axes_length=0.1,
		**kwargs
		):
		"""
		Initializes a new instance of the aruco marekrs cormer preview settings class.
		
		Arguments:
			self (`pydataset.previews.LCSAxesPreviewSettings`): Instance to initialize.
			camera_matrix_feature_name                 (`str`): Name of the camera parameters matrix feature.
			dist_coeffs_feature_name                   (`str`): Name of the camera distortion coefficients vector feature (optional).
			rotation_feature_name                      (`str`): Name of the rotation vector or matrix feature.
			translation_feature_name                   (`str`): Name of the translation vector feature.
			axes_length                              (`float`): Length of the axes lines in meters.
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( axes_length, float )

		super( LCSAxesPreviewSettings, self ).__init__( **kwargs )

		self._camera_matrix_feature_name = camera_matrix_feature_name
		self._dist_coeffs_feature_name   = dist_coeffs_feature_name
		self._rotation_feature_name      = rotation_feature_name
		self._translation_feature_name   = translation_feature_name
		self._axes_length                = axes_length

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the camera parameters matrix feature (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )

		return self._camera_matrix_feature_name

	# def camera_matrix_feature_name ( self )
	
	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_matrix_feature_name = value

	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		"""
		Name of the camera distortion coefficients vector feature (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )

		return self._dist_coeffs_feature_name

	# def dist_coeffs_feature_name ( self )
	
	# --------------------------------------------------

	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._dist_coeffs_feature_name = value

	# def dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the rotation vector or matrix feature (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )

		return self._rotation_feature_name

	# def rotation_feature_name ( self )
	
	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_feature_name = value

	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the translation vector feature (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )

		return self._translation_feature_name

	# def translation_feature_name ( self )
	
	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_feature_name = value

	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def axes_length ( self ):
		"""
		Length of the axes lines in meters (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )

		return self._axes_length

	# def axes_length ( self )
	
	# --------------------------------------------------

	@axes_length.setter
	def axes_length ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is( value, float )

		self._axes_length = value

	# def axes_length ( self, value )

# class LCSAxesPreviewSettings ( PreviewSettings )