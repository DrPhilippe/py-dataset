# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
from ..                                   import units
from ..data                               import CharucoBoardData
from ..dataset                            import Example, SerializableFeatureData
from .charuco_board_data_preview_settings import CharucoBoardDataPreviewSettings
from .preview                             import Preview
from .register_preview_attribute          import RegisterPreviewAttribute

# ##################################################
# ###       CLASS CHARUCO-CORNERS-PREVIEW        ###
# ##################################################

@RegisterPreviewAttribute( 'Charuco Board Data' )
class CharucoBoardDataPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, CharucoBoardDataPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( CharucoBoardDataPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		"""
		Checks if this preview can be drawn using the given example.

		Arguments:
			self (`pydataset.previews.CharucoBoardDataPreview`): Preview.
			self           (`None`/`pydataset.dataset.Example`): Example to preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		return super( CharucoBoardDataPreview, self ).check( example )\
		   and example.contains_feature( settings.charuco_data_feature_name, SerializableFeatureData )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		"""
		Draws the example onto this preview.

		Arguments:
			self (`pydataset.previews.CharucoBoardDataPreview`): Preview.
			self           (`None`/`pydataset.dataset.Example`): Example to preview.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Draw the ItemPreview
		super( CharucoBoardDataPreview, self ).draw( example )
		
		# Get the feature
		charuco_board_data = example.get_feature( self.get_settings().charuco_data_feature_name ).value

		# Draw the charuco corners
		self.draw_charuco( charuco_board_data )

	# def draw ( self, example )

	# --------------------------------------------------

	def draw_charuco ( self, charuco_board_data ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreview )
		assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
		
		self.draw_chessboard( charuco_board_data )
		self.draw_markers( charuco_board_data )
		
	# def draw_charuco ( self, charuco_board_data ):
	
	# --------------------------------------------------

	def draw_chessboard ( self, charuco_board_data ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardDataPreview )
		assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )


		width         = charuco_board_data.board_width
		height        = charuco_board_data.board_height
		square_length = charuco_board_data.square_length * 4000.0
		border_length = charuco_board_data.border_length * 4000.0

		black_color = PyQt5.QtGui.QColor( 0, 0, 0 )
		black_brush = PyQt5.QtGui.QBrush( black_color )
		black_pen   = PyQt5.QtGui.QPen( black_color )
		black_pen.setWidth( 1 )
		black_pen.setJoinStyle( PyQt5.QtCore.Qt.MiterJoin )
		transparent_color = PyQt5.QtGui.QColor( 0, 0, 0, 0 )
		transparent_brush = PyQt5.QtGui.QBrush( transparent_color )
		transparent_pen = PyQt5.QtGui.QPen( transparent_color )
		white_color  = PyQt5.QtGui.QColor( 255, 255, 255 )
		white_brush  = PyQt5.QtGui.QBrush( white_color )

		# draw outline including border
		outline_rect = PyQt5.QtWidgets.QGraphicsRectItem(
			0.0,
			0.0,
			width*square_length + 2*border_length,
			height*square_length + 2*border_length,
			self
			)
		outline_rect.setBrush( white_brush )
		outline_rect.setPen( black_pen )
		self.addToGroup( outline_rect )

		# Draw the cells
		ref = height % 2
		for y in range(height):
			for x in range(width):
				
				# ??? works
				mx = x % 2
				my = y % 2
				i = (mx + my) % 2
				
				# Black cell
				if ( i != ref ):
					black_rect = PyQt5.QtWidgets.QGraphicsRectItem(
						border_length + x*square_length,
						border_length + y*square_length,
						square_length,
						square_length,
						self
					)
					black_rect.setBrush( black_brush )
					black_rect.setPen( transparent_pen )
					self.addToGroup( black_rect )

				# White cell
				else:
					pass
			
			# for x in range(width)
		# for y in range(height)

	# def draw_chessboard ( self, charuco_board_data )
	
	# --------------------------------------------------

	def draw_marker ( self, marker, pos_x, pos_y, square_length, marker_length, border_size ):
		assert pytools.assertions.type_is( marker, numpy.ndarray )
		assert pytools.assertions.type_is( pos_x, float )
		assert pytools.assertions.type_is( pos_y, float )
		assert pytools.assertions.type_is( square_length, float )
		assert pytools.assertions.type_is( marker_length, float )
		assert pytools.assertions.type_is( border_size, int )

		# Dimensions of the marker without the boarder
		marker_shape  = marker.shape
		marker_width  = marker_shape[ 1 ]
		marker_height = marker_shape[ 0 ]
		marker_size   = marker_width
		assert marker_width == marker_height
		
		black_color = PyQt5.QtGui.QColor( 0, 0, 0 )
		black_brush = PyQt5.QtGui.QBrush( black_color )
		black_pen   = PyQt5.QtGui.QPen( black_color )
		black_pen.setWidth( 1 )
		black_pen.setJoinStyle( PyQt5.QtCore.Qt.MiterJoin )
		transparent_color = PyQt5.QtGui.QColor( 0, 0, 0, 0 )
		transparent_brush = PyQt5.QtGui.QBrush( transparent_color )
		transparent_pen   = PyQt5.QtGui.QPen( transparent_color )
		white_color  = PyQt5.QtGui.QColor( 255, 255, 255 )
		white_brush  = PyQt5.QtGui.QBrush( white_color )

		# Number of bits including border bits:
		total_size = marker_size + border_size * 2

		# Length of one bit
		bit_length = marker_length / float(total_size)

		# top-left corner of the marker bits drawing area
		x0 = pos_x + (square_length - marker_length) / 2.0
		y0 = pos_y + (square_length - marker_length) / 2.0

		# Fill the marker area with black
		black_rect = PyQt5.QtWidgets.QGraphicsRectItem(
			x0,
			y0,
			marker_length,
			marker_length,
			self
			)
		black_rect.setBrush( black_brush )
		black_rect.setPen( transparent_pen )
		self.addToGroup( black_rect )

		# Offset of the actual marker contents
		x0 += border_size * bit_length
		y0 += border_size * bit_length

		for x in range( marker_size ):

			xi = x0 + x * bit_length
			
			for y in range( marker_size ):
				
				yi = y0 + y * bit_length

				# is this marker white bit ?
				if marker[ y, x ] == 1:

					white_rect = PyQt5.QtWidgets.QGraphicsRectItem(
						xi,
						yi,
						bit_length,
						bit_length,
						self					
						)
					white_rect.setBrush( white_brush )
					white_rect.setPen( transparent_pen )
					self.addToGroup( white_rect )

			# for x in range( marker_width )

		# for y in range( marker_height )

	# def draw_marker ( axes, pos_x, pos_y, marker, square_length, marker_length )
	
	# --------------------------------------------------

	def draw_markers ( self, charuco_board_data, border_size=1 ):
		assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
		assert pytools.assertions.type_is( border_size, int )

		board_width   = charuco_board_data.board_width
		board_height  = charuco_board_data.board_height
		square_length = charuco_board_data.square_length * 4000.0
		marker_length = charuco_board_data.marker_length * 4000.0
		border_length = charuco_board_data.border_length * 4000.0
		marker_id     = 0

		ref = board_height % 2
		
		for y in range(board_height):

			for x in range(board_width):
				
				# ??? works
				mx = x % 2
				my = y % 2
				i = (mx + my) % 2
				
				# Black cell
				if ( i != ref ):
					pass

				# White cell
				else:
					pos_x = x * square_length + border_length
					pos_y = y * square_length + border_length
					
					marker     = charuco_board_data.get_marker_data( marker_id )
					marker_id += 1

					self.draw_marker( marker, pos_x, pos_y, square_length, marker_length, border_size )

				# if ( i != ref )

			# for x in range(board_width)

		# for y in range(board_height)

	# def draw_markers ( self, charuco_board_data )

# class CharucoBoardCornersPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )