# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset.data
import pytools.assertions
import pytools.serialization

# LOCALS
from .preview_settings                    import PreviewSettings
from .register_preview_settings_attribute import RegisterPreviewSettingsAttribute

# ############################################################
# ###    CLASS CHARUCO-MARKERS-CORNERS-PREVIEW-SETTINGS    ###
# ############################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoMarkersCornersPreviewSettings',
	   namespace = 'pydataset.previews',
	fields_names = [
		'markers_corners_feature_name',
		'markers_ids_feature_name',
		'markers_corners_outline_color',
		'markers_corners_origin_color',
		'markers_ids_text_color'
		]
	)
@RegisterPreviewSettingsAttribute( 'ChArUco Markers' )
class CharucoMarkersCornersPreviewSettings ( PreviewSettings ):
	"""
	Dataset aruco marker corners preview settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		markers_corners_feature_name='',
		markers_ids_feature_name='',
		markers_corners_outline_color=pydataset.data.ColorData(0, 255, 0),
		markers_corners_origin_color=pydataset.data.ColorData(255, 0, 0),
		markers_ids_text_color=pydataset.data.ColorData(0, 0, 255),
		**kwargs ):
		"""
		Initializes a new instance of the `pydataset.previews.CharucoMarkersCornersPreviewSettings` class.

		Arguments:
			self (`pydataset.previews.CharucoMarkersCornersPreviewSettings`): Instance to initialize.
			markers_corners_feature_name                             (`str`): Name of the feature containing the charuco marker corners.
			markers_ids_feature_name                                 (`str`): Name of the feature containing the charuco marker ids.
			markers_corners_outline_color       (`pydataset.data.ColorData`): Color used to display the markers outlines.
			markers_corners_origin_color        (`pydataset.data.ColorData`): Color used to display the markers origin point.
			markers_ids_text_color              (`pydataset.data.ColorData`): Color used to display the markers ids text.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is( markers_corners_feature_name, str )
		assert pytools.assertions.type_is( markers_ids_feature_name, str )
		assert pytools.assertions.type_is_instance_of( markers_corners_outline_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is_instance_of( markers_corners_origin_color, pydataset.data.ColorData )
		assert pytools.assertions.type_is_instance_of( markers_ids_text_color, pydataset.data.ColorData )

		super( CharucoMarkersCornersPreviewSettings, self ).__init__( **kwargs )

		self._markers_corners_feature_name  = markers_corners_feature_name
		self._markers_ids_feature_name      = markers_ids_feature_name
		self._markers_corners_outline_color = copy.deepcopy( markers_corners_outline_color )
		self._markers_corners_origin_color  = copy.deepcopy( markers_corners_origin_color )
		self._markers_ids_text_color        = copy.deepcopy( markers_ids_text_color )

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def markers_corners_feature_name ( self ):
		"""
		Name of the feature containing the charuco marker corners (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )

		return self._markers_corners_feature_name

	# def markers_corners_feature_name ( self )
	
	# --------------------------------------------------

	@markers_corners_feature_name.setter
	def markers_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._markers_corners_feature_name = value

	# def markers_corners_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def markers_ids_feature_name ( self ):
		"""
		Name of the feature containing the charuco marker ids (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )

		return self._markers_ids_feature_name

	# def markers_ids_feature_name ( self )
	
	# --------------------------------------------------

	@markers_ids_feature_name.setter
	def markers_ids_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is( value, str )

		self._markers_ids_feature_name = value

	# def markers_ids_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def markers_corners_outline_color ( self ):
		"""
		Color used to display the markers outlines (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )

		return self._markers_corners_outline_color

	# def markers_corners_outline_color ( self )
	
	# --------------------------------------------------

	@markers_corners_outline_color.setter
	def markers_corners_outline_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._markers_corners_outline_color = value

	# def markers_corners_outline_color ( self, value )

	# --------------------------------------------------

	@property
	def markers_corners_origin_color ( self ):
		"""
		Color used to display the markers origin point (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )

		return self._markers_corners_origin_color

	# def markers_corners_origin_color ( self )
	
	# --------------------------------------------------

	@markers_corners_origin_color.setter
	def markers_corners_origin_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._markers_corners_origin_color = value

	# def markers_corners_origin_color ( self, value )

	# --------------------------------------------------

	@property
	def markers_ids_text_color ( self ):
		"""
		Color used to display the markers ids text (`pydataset.data.ColorData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )

		return self._markers_ids_text_color

	# def markers_ids_text_color ( self )
	
	# --------------------------------------------------

	@markers_ids_text_color.setter
	def markers_ids_text_color ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoMarkersCornersPreviewSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.ColorData )

		self._markers_ids_text_color = value

	# def markers_ids_text_color ( self, value )

# class CharucoMarkersCornersPreviewSettings ( ItemPreviewSettings )