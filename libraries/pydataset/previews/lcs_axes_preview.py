# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                   import Example, NDArrayFeatureData
from .lcs_axes_preview_settings  import LCSAxesPreviewSettings
from .preview                    import Preview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###        CLASS LCS-AXES-ORIGIN-PREVIEW       ###
# ##################################################

@RegisterPreviewAttribute( 'LCS Axes' )
class LCSAxesPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, LCSAxesPreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( LCSAxesPreview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		settings = self.get_settings()

		ok = super( LCSAxesPreview, self ).check( example )\
		   and example.contains_feature( settings.camera_matrix_feature_name, NDArrayFeatureData )\
		   and example.contains_feature( settings.rotation_feature_name,      NDArrayFeatureData )\
		   and example.contains_feature( settings.translation_feature_name,   NDArrayFeatureData )

		if not ok:
			return False

		if settings.dist_coeffs_feature_name:
			return example.contains_feature( settings.dist_coeffs_feature_name, NDArrayFeatureData )

		return True

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		super( LCSAxesPreview, self ).draw( example )
		
		settings = self.get_settings()

		# Get features
		K = example.get_feature( settings.camera_matrix_feature_name ).value
		r = example.get_feature( settings.rotation_feature_name ).value
		t = example.get_feature( settings.translation_feature_name ).value
		# Get Optiona features
		if settings.dist_coeffs_feature_name:
			dist_coeffs = example.get_feature( settings.dist_coeffs_feature_name ).value
		else:
			dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

		if list(r.shape) == [3] or list(r.shape) == [3, 1]  or list(r.shape) == [1, 3]:
			pass
		elif list(r.shape) == [3, 3]:
			r, jacobian = cv2.Rodrigues( r )
		else:
			raise ValueError( 'Invalid rotation shape: {} from feature named: {}, expected [3] or [3,3]'.format(
				list(r.shape),
				settings.rotation_feature_name
				))
		
		# Create the axes points
		length = settings.axes_length
		points = numpy.array(
			[
				[    0.0,    0.0,    0.0 ],
				[ length,    0.0,    0.0 ],
				[    0.0,    0.0,    0.0 ],
				[    0.0, length,    0.0 ],
				[    0.0,    0.0,    0.0 ],
				[    0.0,    0.0, -length ]
			],
			dtype=numpy.float32
			)

		# Project the axes points
		points, jacobian = cv2.projectPoints( points, r, t, K, dist_coeffs )
		points = numpy.reshape( points, [6, 2] )
		
		# Draw the axes lines
		self.draw_axes_lines( points )

	# def draw ( self, example )

	# --------------------------------------------------

	def draw_axes_lines ( self, points ):
		assert pytools.assertions.type_is_instance_of( self, LCSAxesPreview )
		assert pytools.assertions.type_is_instance_of( points, numpy.ndarray )
		
		pens  = [
			PyQt5.QtGui.QPen( PyQt5.QtGui.QColor( 255, 0, 0 ), 2 ),
			PyQt5.QtGui.QPen( PyQt5.QtGui.QColor( 0, 255, 0 ), 2 ),
			PyQt5.QtGui.QPen( PyQt5.QtGui.QColor( 0, 0, 255 ), 2 )
			]

		for i in range( 3 ):

			x1, y1 = points[ i * 2 + 0, : ]
			x2, y2 = points[ i * 2 + 1, : ]

			pen = pens[ i ] 

			line = PyQt5.QtWidgets.QGraphicsLineItem( x1, y1, x2, y2, self )
			line.setPen( pen )
			self.addToGroup( line )

		# for i in range( 3 )

	# def draw_axes_lines ( self, points )
	
# class LCSAxesPreview ( PyQt5.QtWidgets.QGraphicsItemGroup, Preview )