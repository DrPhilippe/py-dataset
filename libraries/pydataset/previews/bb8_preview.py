# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset                   import Example, ImageFeatureData, NDArrayFeatureData
from .bb8_preview_settings       import BB8PreviewSettings
from .bounding_box_preview       import BoundingBoxPreview
from .register_preview_attribute import RegisterPreviewAttribute

# ##################################################
# ###             CLASS BB8-PREVIEW              ###
# ##################################################

@RegisterPreviewAttribute( 'BB8' )
class BB8Preview ( BoundingBoxPreview ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, settings, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, BB8Preview )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( settings, BB8PreviewSettings )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QGraphicsItem) )
		
		super( BB8Preview, self ).__init__(
			 example = example,
			settings = settings,
			  parent = parent
			)

	# def __init__ ( self, example, settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def check ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, BB8Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		return super( BB8Preview, self ).check( example )

	# def check ( self, example )

	# --------------------------------------------------

	def draw ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, BB8Preview )
		assert pytools.assertions.type_is_instance_of( example, (type(None), Example) )

		# Setup graphic item (scale, position, rotation)
		super( BoundingBoxPreview, self ).draw( example )
		
		# Fetch settings
		settings = self.get_settings()

		# Get features
		bb8 = example.get_feature( settings.bounding_box_feature_name ).value
		
		# Project bounding box
		bb8 = self.project_points_to_image_plane_if_needed( example, bb8 )

		# Only one bb8 control points set ?
		if len(bb8.shape) == 2:
			self.draw_bounding_box( bb8 )

		else:
			count = bb8.shape[0]
			for i in range(count):
				
				# If the control points set does not have NaN values
				if not numpy.isnan( numpy.sum( bb8[i] ) ):

					# Draw it				
					self.draw_bounding_box( bb8[i] )

				# if not numpy.isnan( numpy.sum( control_points[i] ) ):

			# for i in range(count)

		# if len(bb8.shape) == 2:

	# def draw ( self, example )

# class BB8Preview ( BoundingBoxPreview )