# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def index_to_one_hot ( category_index, number_of_classes ):
	assert pytools.assertions.type_is( category_index, int )
	assert pytools.assertions.type_is( number_of_classes, int )
	assert pytools.assertions.true( 0 <= category_index )
	assert pytools.assertions.true( category_index < number_of_classes )

	one_hot = numpy.zeros( (number_of_classes,), dtype=numpy.int32 )
	one_hot[ category_index ] = 1
	return one_hot

# def index_to_one_hot ( category_index, number_of_classes )

# --------------------------------------------------

def one_hot_to_index ( one_hot ):
	assert pytools.assertions.type_is( one_hot, numpy.ndarray )
	assert pytools.assertions.equal( one_hot.dtype, numpy.int32 )
	assert pytools.assertions.equal( len(one_hot.shape), 1 )

	return numpy.argmax( one_hot )

# def one_hot_to_index ( one_hot )