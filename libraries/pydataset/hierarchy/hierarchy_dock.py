# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pydataset.hierarchy
import pytools.assertions
import pyui.widgets

# LOCALS
from ..dataset import Dataset, Item

# ##################################################
# ###            CLASS HIERARCHY-DOCK            ###
# ##################################################

class HierarchyDock ( pyui.widgets.Dock ):
	
	# ##################################################
	# ###                   SIGNAL                   ###
	# ##################################################

	# --------------------------------------------------

	item_selected = PyQt5.QtCore.pyqtSignal( Item )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, datasets=[], title='Hierarchy', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyDock )
		assert pytools.assertions.type_is( datasets, list )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.list_items_type_is( datasets, Dataset )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Set the dataset
		self._datasets = datasets

		# Initialize the dock widget
		super( HierarchyDock, self ).__init__( title, parent )
		
	# def __init__ ( parent )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def current ( self ):
		"""
		Currently selected dataset item (`None`/`pydataset.dataset.Item`).
		"""
		return self._hierarchy_widget.current

	# def current ( self )

	# --------------------------------------------------

	@property
	def hierarchy_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyDock )
		
		return self._hierarchy_widget

	# def hierarchy_widget ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_dataset ( self, dataset ):
		
		self._hierarchy_widget.add_dataset( dataset )

	# --------------------------------------------------

	def remove_dataset ( self, dataset ):
		
		self._hierarchy_widget.remove_dataset( dataset )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyDock )
		
		# Setup the parent class
		super( HierarchyDock, self ).setup()
		
		# Define minimum size
		self.setMinimumSize( 300, self.minimumHeight() )

		# Setup dataset tree view
		self._hierarchy_widget = pydataset.hierarchy.HierarchyWidget( self._datasets, self )
		self._hierarchy_widget.item_selected.connect( self.item_selected )
		self.setWidget( self._hierarchy_widget )

	# def setup ( self )

# class HierarchyDock ( pyui.widgets.Dock )