# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset import Feature
from .node     import Node

# ##################################################
# ###             CLASS FEATURE-NODE             ###
# ##################################################

class FeatureNode ( Node ):
	"""
	The class `pydataset.hierarchy.FeatureNode` is used to display a feature in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, feature, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.FeatureNode` class.

		Arguments:
			self   (`pydataset.hierarchy.FeatureNode`): Instance to initialize.
			dataset      (`pydataset.dataset.Feature`): Dataset displayed in this node.
			parent        (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )
		assert pytools.assertions.type_is_instance_of( feature, Feature )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		# Init the parent class
		super( FeatureNode, self ).__init__( parent )

		# Set the feature
		self._feature = feature

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def feature ( self ):
		"""
		Feature displayed in this node (`pydataset.dataset.Feature`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )

		return self._feature

	# def feature ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the feature displayed in this node (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )

		return self.feature.name
	
	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of the feature displayed in this node (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )

		return self.feature.url
	
	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this example node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )
		
		# Alwayse 0
		return 0

	# def number_of_children ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this feature node.

		Arguments:
			self  (`pydataset.hierarchy.FeatureNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureNode )
		
		self.is_loaded = True

	# def load_children ( self )
	
# class FeatureNode ( Node )