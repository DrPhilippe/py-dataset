# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pyui.widgets

# LOCALS
from ..dataset import functions, Dataset
from ..widgets import OpenDatasetDialog

# ##################################################
# ###            CLASS HIERARCHY-MENU            ###
# ##################################################

class HierarchyMenu ( PyQt5.QtWidgets.QMenu ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='Dataset', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( HierarchyMenu, self ).__init__( title, parent )

		self.setup()

	# def __init__ ( self, parent )

	# ##################################################
	# ###                 SIGNALS                    ###
	# ##################################################

	# --------------------------------------------------

	dataset_opened = PyQt5.QtCore.pyqtSignal( Dataset )

	# --------------------------------------------------

	dataset_closed = PyQt5.QtCore.pyqtSignal()

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def open_action ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )

		return self._open_action
	
	# def open_action ( self )
	
	# --------------------------------------------------

	@property
	def open_file_action ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )

		return self._open_file_action
	
	# def open_file_action ( self )

	# --------------------------------------------------

	@property
	def close_action ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )

		return self._close_action
	
	# def close_action ( self )

	# --------------------------------------------------

	@property
	def recent_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )

		return self._recent_menu
	
	# def recent_menu ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_recent ( self, url ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		assert pytools.assertions.type_is( url, str )
		
		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent', [] )
		
		if not url in recents:
			recents.append( url )

		if len(recents) > 10:
			recents.pop(0)

		settings.setValue( 'recent', recents )

		self.update_recent_menu()

	# def add_recent ( self, url )

	# --------------------------------------------------

	def remove_recent ( self, url ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		assert pytools.assertions.type_is( url, str )
		
		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent', [] )
		
		if url in recents:
			recents.remove( url )

		settings.setValue( 'recent', recents )

		self.update_recent_menu()

	# def remove_recent ( self, url )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		
		self._open_action = self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DirOpenIcon ),
			'Open',
			self.on_open_dataset,
			PyQt5.QtGui.QKeySequence.Open
			)
		
		self._open_file_action = self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DirLinkIcon ),
			'Open (File)',
			self.on_open_file_dataset,
			PyQt5.QtGui.QKeySequence( 'Ctrl+Shift+O' )
			)

		self._recent_menu = self.addMenu( 'Recent' )
		self._recent_menu.triggered.connect( self.on_recent_clicked )
		self.update_recent_menu()

		self._close_action = self.addAction(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogCloseButton ),
			'Close',
			self.dataset_closed,
			PyQt5.QtGui.QKeySequence.Close
			)
		self._close_action.setEnabled( False )
		
	# def setup ( self )

	# --------------------------------------------------

	def update_recent_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )

		settings = pyui.widgets.ApplicationSettings()
		recents  = settings.value( 'recent', [] )
		
		self.recent_menu.blockSignals( True )
		self.recent_menu.clear()
		self.recent_menu.setEnabled( len(recents) > 0 )
		for recent in recents:
			self.recent_menu.addAction( recent )
		self.recent_menu.blockSignals( False )

	# def update_recent_menu ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_open_dataset ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		
		dialog = OpenDatasetDialog( pyui.widgets.Application.instance().main_window )

		if dialog.exec():

			url = dialog.url

			# Check if the dataset exists
			if functions.exists( url ):

				# Ok, open it
				dataset = functions.get( url )

				# Check its a dataset
				if isinstance( dataset, Dataset ):
					
					self.add_recent( url )
					self.dataset_opened.emit( dataset )
					return

				# if isinstance( dataset, Dataset )

			# if functions.exists( url )

			# Otherwise, error		
			info_dialog = pyui.widgets.InfoDialog(
				 title = 'Error',
				  text = 'The url "{}" does not point to a dataset.'.format( url ),
				parent = pyui.widgets.Application.instance().main_window
				)
			info_dialog.exec()

		# if dialog.exec()

	# def on_open_dataset ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_open_file_dataset ( self ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		
		parent  = pyui.widgets.Application.instance().main_window
		caption = 'Select the directory containing the dataset to open'
		dirpath = PyQt5.QtWidgets.QFileDialog.getExistingDirectory( parent, caption )

		if ( dirpath ):

			url = 'file://' + dirpath

			# Check if the dataset exists
			if functions.exists( url ):

				# Ok, open it
				dataset = functions.get( url )

				# Check its a dataset
				if isinstance( dataset, Dataset ):
					
					self.add_recent( url )
					self.dataset_opened.emit( dataset )
					return

				# if isinstance( dataset, Dataset )
			
			# Otherwise, error	
			info_dialog = pyui.widgets.InfoDialog(
				 title = 'Error',
				  text = 'The url "{}" does not point to a dataset.'.format( url ),
				parent = pyui.widgets.Application.instance().main_window
				)
			info_dialog.exec()

		# if ( dirpath )

	# def on_open_file_dataset ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( PyQt5.QtWidgets.QAction )
	def on_recent_clicked ( self, action ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyMenu )
		assert pytools.assertions.type_is_instance_of( action, PyQt5.QtWidgets.QAction )

		# The url is the text of the action
		url = action.text()
		
		# Check if the dataset exists
		if functions.exists( url ):
			
			# Ok open it
			dataset = functions.get( url )
			self.dataset_opened.emit( dataset )

		# Otherwise, remove it fron the recent list
		else:
			self.remove_recent( url )
			
			info_dialog = pyui.widgets.InfoDialog(
				 title = 'Information',
				  text = 'The dataset "{}" no longer exists and was removed from the list of recently openned datasets.'.format( url ),
				parent = pyui.widgets.Application.instance().main_window
				)
			info_dialog.exec()

		# if functions.exists( url )
		
	# def on_recent_clicked ( self, action )

# class HierarchyMenu ( PyQt5.QtWidgets.QMenu )