# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .feature_node import FeatureNode
from .node         import Node

# ##################################################
# ###          CLASS SUB-FEATURES-NODE           ###
# ##################################################

class SubFeaturesNode ( Node ):
	"""
	Displays the features of an example.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, features_names, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.SubFeaturesNode` class.

		Arguments:
			features_names    (`list` of `str`): Names of the features.
			parent (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		# Initialize the parent class
		super( SubFeaturesNode, self ).__init__( parent )

		# Set the features names
		self._features_names = features_names

	# def __init__ ( self, features_names, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features displayed under this node (`list` of `str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )

		return self._features_names
	
	# def features_names ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )

		return 'Features'
		
	# def name ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )

		return len( self.features_names )

	# def number_of_children ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )

		return self.parent.url + '/examples'

	# def url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this node.

		Arguments:
			self  (`pydataset.hierarchy.SubFeaturesNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubFeaturesNode )

		if self.is_not_loaded:
			
			example = self.parent.example

			for feature_name in self.features_names:

				sub_feature = example.get_feature( feature_name )
				
				child = FeatureNode( sub_feature, self )
				self.add_child( child )
			
			self.is_loaded = True

		# if self.is_not_loaded:

	# def load_children ( self )

# class SubFeaturesNode ( Node )