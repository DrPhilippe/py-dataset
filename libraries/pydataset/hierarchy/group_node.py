# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset          import Group
from .example_node      import ExampleNode
from .node              import Node
from .sub_examples_node import SubExamplesNode
from .sub_groups_node   import SubGroupsNode

# ##################################################
# ###              CLASS GROUP-NODE              ###
# ##################################################

class GroupNode ( ExampleNode ):
	"""
	The class `pydataset.hierarchy.ExampleNode` is used to display an group in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, group, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.GroupNode` class.

		Arguments:
			self     (`pydataset.hierarchy.GroupNode`): Instance to initialize.
			example        (`pydataset.dataset.Group`): Group displayed in this node.
			parent        (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupNode )
		assert pytools.assertions.type_is_instance_of( group, Group )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		super( GroupNode, self ).__init__( group, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def group ( self ):
		"""
		Group displayed in this node (`pydataset.dataset.Group`).
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupNode )

		return self.example

	# def group ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this group node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )

		total = 0

		if self.group.number_of_groups > 0:
			total += 1

		if self.group.number_of_examples > 0:
			total += 1

		if self.group.number_of_features > 0:
			total += 1

		return total

	# def number_of_children ( self )
		
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this group node.

		Arguments:
			self  (`pydataset.hierarchy.ExampleNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupNode )

		if self.is_not_loaded:

			super( GroupNode, self ).load_children()
			
			if ( self.group.number_of_groups > 0 ):
				child = SubGroupsNode( self.group.groups_names, self )
				self.add_child( child )

			if ( self.group.number_of_examples > 0 ):
				child = SubExamplesNode( self.group.examples_names, self )
				self.add_child( child )


		# if self.is_not_loaded
		
# class GroupNode ( ExampleNode )