# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset          import Example
from .node              import Node
from .sub_features_node import SubFeaturesNode

# ##################################################
# ###             CLASS EXAMPLE-NODE             ###
# ##################################################

class ExampleNode ( Node ):
	"""
	The class `pydataset.hierarchy.ExampleNode` is used to display an example in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, example, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.ExampleNode` class.

		Arguments:
			self   (`pydataset.hierarchy.ExampleNode`): Instance to initialize.
			example      (`pydataset.dataset.Example`): Example displayed in this node.
			parent        (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )
		assert pytools.assertions.type_is_instance_of( example, Example )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		# Init the parent
		super( ExampleNode, self ).__init__( parent )

		# Set the example
		self._example = example

	# def __init__ ( self, example, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def example ( self ):
		"""
		Example displayed in this node (`pydataset.dataset.Example`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )

		return self._example

	# def example ( self ):
	
	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the example displayed in this node (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )

		return self.example.name

	# def name ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of the example displayed in this node (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )
	
		return self.example.url
	
	# def url ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this example node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )
		
		# One for the sub-features node if the example has features or 0
		return 1 if self.example.number_of_features > 0 else 0

	# def number_of_children ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this example node.

		Arguments:
			self  (`pydataset.hierarchy.ExampleNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleNode )

		if self.is_not_loaded:

			nb_features = self.example.number_of_features
			
			if ( nb_features > 0 ):	
				child = SubFeaturesNode( self.example.features_names, self )
				self.add_child( child )
			
			self.is_loaded = True

		# if self.is_not_loaded
		
	# def load_children ( self )

# class ExampleNode ( Node )