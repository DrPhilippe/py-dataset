# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .example_node import ExampleNode
from .node         import Node

# ##################################################
# ###           CLASS SUBEXAMPLES-NODE           ###
# ##################################################

class SubExamplesNode ( Node ):
	"""
	Displays the examples of a group.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, examples_names, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.SubExamplesNode` class.

		Arguments:
			examples_names    (`list` of `str`): Names of the examples.
			parent (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubExamplesNode )
		assert pytools.assertions.type_is( examples_names, list )
		assert pytools.assertions.list_items_type_is( examples_names, str )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		# Init the parent class
		super( SubExamplesNode, self ).__init__( parent )

		# Set the examples names
		self._examples_names = examples_names

	# def __init__ ( self, examples_names, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def examples_names ( self ):
		"""
		Names of the examples displayed under this node (`list` of `str`)
		"""
		return self._examples_names
	
	# def examples_names ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubExamplesNode )

		return 'Examples'
	
	# def name ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SubExamplesNode )

		return len( self.examples_names )

	# def number_of_children ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubExamplesNode )

		return self.parent.url + '/examples'

	# def url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this node.

		Arguments:
			self  (`pydataset.hierarchy.SubExamplesNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubExamplesNode )

		if self.is_not_loaded:
			
			group = self.parent.group

			for example_name in self.examples_names:

				sub_example = group.get_example( example_name )
				
				child = ExampleNode( sub_example, self )
				self.add_child( child )
			
			self.is_loaded = True
		
		# if self.is_not_loaded

	# def load_children ( self )
	
# class SubexamplesNode ( Node )