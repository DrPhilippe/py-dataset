# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset   import Dataset
from .group_node import GroupNode
from .node       import Node

# ##################################################
# ###             CLASS DATASET-NODE             ###
# ##################################################

class DatasetNode ( GroupNode ):
	"""
	The class `pydataset.hierarchy.DatasetNode` is used to display a dataset in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dataset, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.DatasetNode` class.

		Arguments:
			self   (`pydataset.hierarchy.DatasetNode`): Instance to initialize.
			dataset      (`pydataset.dataset.Dataset`): Dataset displayed in this node.
			parent        (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetNode )
		assert pytools.assertions.type_is_instance_of( dataset, Dataset )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		super( DatasetNode, self ).__init__( dataset, parent )

	# def __init__ ( self, dataset, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dataset ( self ):
		"""
		Dataset displayed in this node (`pydataset.dataset.Dataset`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetNode )

		return self.group

	# def dataset ( self )

# class DatasetNode ( GroupNode )