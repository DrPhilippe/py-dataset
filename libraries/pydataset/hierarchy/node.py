# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 CLASS NODE                 ###
# ##################################################

class Node:
	"""
	The class `pydataset.hierarchy.Node` is the base class of all the nodes displayed in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None, children=[], is_loaded=False ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.Node` class.

		Arguments:
			parent      (`None`/`pydataset.hierarchy.Node`): Parent node.
			children (`list` of `pydataset.hierarchy.Node`): Children nodes.
			is_loaded                              (`bool`): Indicates if this node is loaded.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Node) )
		assert pytools.assertions.type_is( children, list )
		assert pytools.assertions.list_items_type_is( children, Node )
		assert pytools.assertions.type_is( is_loaded, bool )

		self._parent    = parent
		self._children  = list(children)
		self._is_loaded = is_loaded

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def children ( self ):
		"""
		Children nodes (`list` of `pydataset.hierarchy.Node`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		return self._children

	# def children ( self )

	# --------------------------------------------------

	@children.setter
	def children ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, Node )

		self._children = value

	# def children ( self, value )

	# --------------------------------------------------

	@property
	def is_loaded ( self ):
		"""
		Indicates if this node is loaded (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		return self._is_loaded
	
	# def is_loaded ( self )

	# --------------------------------------------------

	@is_loaded.setter
	def is_loaded ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is( value, bool )

		self._is_loaded = value
	
	# def is_loaded ( self, value )

	# --------------------------------------------------

	@property
	def is_not_loaded ( self ):
		"""
		Indicates if this node is not loaded (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		return not self._is_loaded

	# def is_not_loaded ( self )

	# --------------------------------------------------

	@is_not_loaded.setter
	def is_not_loaded ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is( value, bool )

		self._is_loaded = not value

	# def is_not_loaded ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		raise NotImplementedError()
	
	# def name ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		raise NotImplementedError()

	# def number_of_children ( self )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent node (`None`/`pydataset.hierarchy.Node`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		return self._parent

	# def parent ( self )
	
	# --------------------------------------------------

	@parent.setter
	def parent ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Node) )

		self._parent = value

	# def parent ( self, value )	

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		raise NotImplementedError()
	
	# def url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_child ( self, child ):
		"""
		Adds a child to this node.

		Arguments:
			self (`pydataset.hierarchy.Node`): Node receiving the child.
			child (`pydataset.hierarchy.Node`): Child node.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is_instance_of( child, Node )

		self._children.append( child )

	# def add_child ( self, child )

	# --------------------------------------------------

	def get_child_at ( self, index ):
		"""
		Returns the child at the given index in this node.
			
		Arguments:
			self (`pydataset.hierarchy.Node`): Node of which to get the child.
			index                     (`int`): Index of the child.

		Returns:
			`pydataset.hierarchy.Node`: The child node.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is( index, int )

		return self.children[ index ]

	# def get_child_at ( self, index )

	# --------------------------------------------------

	def get_index_of_child ( self, child ):
		"""
		Returns the index of the given child of this node.
			
		Arguments:
			self  (`pydataset.hierarchy.Node`): Node of which to get the child index.
			child (`pydataset.hierarchy.Node`): The child node.
		
		Returns:
			`int`: Index of the child.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is_instance_of( child, Node )

		return self.children.index( child )

	# def get_index_of_child ( self, child )

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this node.

		Arguments:
			self  (`pydataset.hierarchy.Node`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )

		raise NotImplementedError()

	# def load_children ( self )

	# --------------------------------------------------

	def set_child_at ( self, index, child ):
		"""
		Sets the child at the given index in this node.
			
		Arguments:
			self  (`pydataset.hierarchy.Node`): Node of which to set the child.
			index                      (`int`): Index of the child.
			child (`pydataset.hierarchy.Node`): The child node.
		"""
		assert pytools.assertions.type_is_instance_of( self, Node )
		assert pytools.assertions.type_is( index, int )
		assert pytools.assertions.type_is_instance_of( child, Node )
		
		self.children[ index ] = child

	# def set_child_at ( self, index, child )
	
# class Node