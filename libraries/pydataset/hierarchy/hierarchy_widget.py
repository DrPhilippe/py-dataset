# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset           import Dataset
from .dataset_node       import DatasetNode
from .example_node       import ExampleNode
from .feature_node       import FeatureNode
from .group_node         import GroupNode
from .hierarchy_model    import HierarchyModel
from .sub_examples_node  import SubExamplesNode
from .sub_features_node  import SubFeaturesNode
from .sub_groups_node    import SubGroupsNode

# ##################################################
# ###           CLASS HIERARCHY-WIDGET           ###
# ##################################################

class HierarchyWidget ( PyQt5.QtWidgets.QTreeView ):
	"""
	Widget used to display a `pydataset.hierarchy.HierarchyModel`.
	"""

	# ##################################################
	# ###                   SIGNAL                   ###
	# ##################################################

	# --------------------------------------------------

	"""
	Signal emitted when an item of the dataset is selected.
	"""
	item_selected = PyQt5.QtCore.pyqtSignal( object )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, datasets=[], parent=None ):
		"""
		Initializes a new instance of the `pyui.hierarchy.HierarchyWidget` class.

		Arguments:
			self     (`pydataset.hierarchy.HierarchyWidget`): Instance to initialize.
			datasets (`list` of `pydataset.dataset.Dataset`): Dataset to display.
			aprent        (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is( datasets, list )
		assert pytools.assertions.list_items_type_is( datasets, Dataset )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( HierarchyWidget, self ).__init__( parent )

		self._datasets = list(datasets)		
		model = HierarchyModel( self._datasets )
		self.setModel( model )

		self.setUniformRowHeights( True )
		self.setAlternatingRowColors( True )
		self.clicked.connect( self.on_item_clicked )

	# def __init__ ( self, dataset, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def current ( self ):
		"""
		Currently selected dataset item (`None`/`pydataset.dataset.Item`).
		"""
		return self.get_dataset_item_at( self.currentIndex() )

	# def current ( self )

	# --------------------------------------------------
	
	@property
	def datasets ( self ):
		"""
		Datasets displayed in this hierarchy widget (`list` of `pydataset.dataset.Dataset`).
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )

		return self._datasets
	
	# --------------------------------------------------

	@datasets.setter
	def datasets ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, Dataset )

		self._datasets = value
		
		if value:
			model = HierarchyModel( value )
			self.setModel( model )

		else:
			self.setModel( None )

	# def datasets ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def add_dataset ( self, dataset ):
		"""
		Adds a dataset to this hierarchy widget.

		Arguments:
			self (`pydataset.hierarchy.HierarchyWidget`): Herarchy widget.
			dataset        (`pydataset.dataset.Dataset`): The dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is_instance_of( dataset, Dataset )

		self._datasets.append( dataset )
		model = HierarchyModel( self._datasets )
		self.setModel( model )

	# def add_dataset ( self, dataset )

	# --------------------------------------------------

	def get_dataset_item_at ( self, index ):
		"""
		Get the dataset item at the given model index in the hierarchy model viewed in this widget.
		
		Arguments:
			self (`pydataset.hierarchy.HierarchyWidget`): Dataset tree view containing the hierarchy model index.
			index           (`PyQt5.QtCore.QModelIndex`): Herarchy model index.

		Returns:
			`None`/`pydataset.dataset.Item`: The item at the given index.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is_instance_of( index, PyQt5.QtCore.QModelIndex )

		model = self.model()

		if model is not None:
			
			node = model.nodeOfIndex( index )
			
			if isinstance( node, DatasetNode ):
				return node.dataset

			elif isinstance( node, SubGroupsNode ):
				return None

			elif isinstance( node, GroupNode ):
				return node.group

			elif isinstance( node, SubExamplesNode ):
				return None

			elif isinstance( node, ExampleNode ):
				return node.example

			elif isinstance( node, SubFeaturesNode ):
				return None

			elif isinstance( node, FeatureNode ):
				return node.feature
			
			else:
				return None

			# if isinstance( node, DatasetNode )

		else:
			return None

		# if model is not None

	# def get_dataset_item_at ( self, index )

	# --------------------------------------------------

	def remove_dataset ( self, dataset ):
		"""
		Removes a dataset to this hierarchy widget.

		Arguments:
			self (`pydataset.hierarchy.HierarchyWidget`): Herarchy widget.
			dataset        (`pydataset.dataset.Dataset`): The dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is_instance_of( dataset, Dataset )

		self._datasets.remove( dataset )
		model = HierarchyModel( self._datasets )
		self.setModel( model )

	# def remove_dataset ( self, dataset )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( 'QModelIndex' )
	def on_item_clicked ( self, index ):
		"""
		Slot invoked when an item of the view is clicked.

		Arguments:
			self (`pydataset.hierarchy.HierarchyWidget`): Dataset tree view containing the clicked item.
			index           (`PyQt5.QtCore.QModelIndex`): Model index of the clicked item.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyWidget )
		assert pytools.assertions.type_is_instance_of( index, PyQt5.QtCore.QModelIndex )

		item = self.get_dataset_item_at( index )
		self.item_selected.emit( item )

	# def on_item_clicked ( self, index )

# class HierarchyWidget ( PyQt5.QtWidgets.QTreeView )