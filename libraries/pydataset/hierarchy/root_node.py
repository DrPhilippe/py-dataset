# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from ..dataset     import Dataset
from .node         import Node
from .dataset_node import DatasetNode

# ##################################################
# ###              CLASS ROOT-NODE              ###
# ##################################################

class RootNode ( Node ):
	"""
	The class `pydataset.hierarchy.RootNode` is the root node in the hierarchy.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, datasets ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.RootNode` class.

		Arguments:
			self            (`pydataset.hierarchy.RootNode`): Instance to initialize.
			datasets (`list` of `pydataset.dataset.Dataset`): Dataset displayed in this node.
		"""
		assert pytools.assertions.type_is_instance_of( self, RootNode )
		assert pytools.assertions.type_is( datasets, list )
		assert pytools.assertions.list_items_type_is( datasets, Dataset )

		# Init the parent class
		super( RootNode, self ).__init__()

		# Set the datasets
		self._datasets = datasets

	# ##################################################
	# ###                 PROPETIES                  ###
	# ##################################################

	# --------------------------------------------------

	@property
	def datasets ( self ):
		"""
		Datasets displayed in this node (`list` of `pydataset.dataset.Dataset`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RootNode )

		return self._datasets

	# def datasets ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this node (`str`).
		"""
		return ''

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this node (`str`).
		"""
		return ''

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this node (`int`).
		"""
		return len( self.datasets )

	# def number_of_children ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this root node.

		Arguments:
			self  (`pydataset.hierarchy.RootNode`): Node of which to load the children.
		"""
		
		if self.is_not_loaded:

			for dataset in self.datasets:
				child = DatasetNode( dataset, self )
				self.add_child( child )

			self.is_loaded = True

		# if self.is_not_loaded

	# def load_children ( self )

# class RootNode ( Node )