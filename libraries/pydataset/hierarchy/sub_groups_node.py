# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .node import Node

# ##################################################
# ###           CLASS SUB-GROUPS-NODE            ###
# ##################################################

class SubGroupsNode ( Node ):
	"""
	Displays the groups of an group.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, groups_names, parent ):
		"""
		Initializes a new instance of the `pydataset.hierarchy.SubGroupsNode` class.

		Arguments:
			groups_names      (`list` of `str`): Names of the groups.
			parent (`pydataset.hierarchy.Node`): Parent node.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )
		assert pytools.assertions.type_is( groups_names, list )
		assert pytools.assertions.list_items_type_is( groups_names, str )
		assert pytools.assertions.type_is_instance_of( parent, Node )

		# Initialize the parent class
		super( SubGroupsNode, self ).__init__( parent )

		# Set the groups names
		self._groups_names = groups_names

	# def __init__ ( self, groups_names, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def groups_names ( self ):
		"""
		Names of the groups displayed under this node (`list` of `str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )

		return self._groups_names
	
	# def groups_names ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )

		return 'Groups'
		
	# def name ( self )

	# --------------------------------------------------

	@property
	def number_of_children ( self ):
		"""
		Number of children of this node (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )

		return len( self.groups_names )

	# def number_of_children ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this node (`str`)
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )

		return self.parent.url + '/groups'

	# def url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def load_children ( self ):
		"""
		Loads the children of this node.

		Arguments:
			self  (`pydataset.hierarchy.SubGroupsNode`): Node of which to load the children.
		"""
		assert pytools.assertions.type_is_instance_of( self, SubGroupsNode )

		if self.is_not_loaded:
			
			# import here to avoir cyclic dependancy with group
			from .group_node import GroupNode

			group = self.parent.group

			for group_name in self.groups_names:

				sub_group = group.get_group( group_name )
				
				child = GroupNode( sub_group, self )
				self.add_child( child )
			
			self.is_loaded = True

		# if self.is_not_loaded

	# def load_children ( self )

# class SubGroupsNode ( Node )