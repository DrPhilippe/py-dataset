# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .node      import Node
from .root_node import RootNode

# ##################################################
# ###           CLASS HIERARCHY-MODEL            ###
# ##################################################

class HierarchyModel ( PyQt5.QtCore.QAbstractItemModel ):
	"""
	The dataset tree model displays the contents of a dataset in the form of a tree.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Name of the colums supported by this model (`list` of `str`).
	"""
	column_names = [ 'Name' ]

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, datasets, parent=None ):
		"""
		Initializes a new instance of the `pyui.hierarchy.HierarchyModel` class.

		Arguments:
			self      (`pydataset.hierarchy.HierarchyModel`): Instance to initialize.
			datasets (`list` of `pydataset.dataset.Dataset`): Datasets displayed in this tree.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is( datasets, list )
		assert pytools.assertions.list_items_type_is( datasets, pydataset.dataset.Dataset )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtCore.QObject) )

		super( HierarchyModel, self ).__init__( parent )
		
		self._root_node = RootNode( datasets )

		if self._root_node.is_not_loaded:
			self._root_node.load_children()

	# def __init__ ( self, datasets, parent=None )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------
	
	@property	
	def root_node ( self ):
		"""
		Root node of this hierarchy model (`pydataset.hierarchy.RootNode`).
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		
		return self._root_node

	# def get_root_node ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def canFetchMore ( self, model_index ):
		"""
		Returns `True` if there is more data available for the given `model_index`; otherwise returns `False`.
		
		Arguments:
			self (`pydataset.hierarchy.HierarchyModel`): Dataset tree-model containing the node with the given model index.
			model_index      (`PyQt5.QtCore.QModelIndex`): Model index of the node for which to check if more data is available.

		Returns:
			`bool`: `True` if there is more data available for the given `model_index`; otherwise returns `False`.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( model_index, PyQt5.QtCore.QModelIndex )
		
		# Check if the index is valid
		if not model_index.isValid():
			return False

		# Get the node at the given index
		node = model_index.internalPointer()

		# Check if its data (children) are loaded
		return node.is_not_loaded

	# --------------------------------------------------

	def columnCount ( self, model_index ):
		"""
		Returns the number of data displayed on the line with the given model index.

		Arguments:
			self (`pydataset.hierarchy.HierarchyModel`): Dataset tree-model containing the node with the given model index.
			model_index      (`PyQt5.QtCore.QModelIndex`): Model index of the node for which to check the number of displayed data.

		Returns:
			`int`: Number of columns / displayed data.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( model_index, PyQt5.QtCore.QModelIndex )
		
		# Return the number of column names
		return len( HierarchyModel.column_names )
	
	# def columnCount ( self, parent_index )

	# --------------------------------------------------

	def data ( self, model_index, role ):
		"""
		Returns the data displayed at the given model index in the tree view.

		Arguments:
			self (`pydataset.hierarchy.HierarchyModel`): Dataset tree-model containing the node with the given model index.
			model_index    (`PyQt5.QtCore.QModelIndex`): Model index of the node for which to get displayed data.
			role                                (`int`): Role of the displaed data (see https://doc.qt.io/qt-5/qt.html#ItemDataRole-enum)

		Returns:
			`PyQt5.QtCore.QVariant`: The data displayed in the tree view at the given model index.
		"""
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( model_index, PyQt5.QtCore.QModelIndex )
		assert pytools.assertions.type_is( role, int )

		# Check if the index is valid and if the role is display
		if model_index.isValid() and role == PyQt5.QtCore.Qt.DisplayRole:

			# Get the node corresponding to the index
			node = self.nodeOfIndex( model_index )

			# Column 0 displays the name
			if model_index.column() == 0:
				return node.name

			# Default value
			else:
				return PyQt5.QtCore.QVariant()

		# Default value
		else:
			return PyQt5.QtCore.QVariant()
	
	# def data ( self, model_index, role )
	
	# --------------------------------------------------
	
	def flags ( self, model_index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( model_index, PyQt5.QtCore.QModelIndex )
		
		if not model_index.isValid():
			return PyQt5.QtCore.Qt.NoItemFlags
		else:
			return PyQt5.QtCore.Qt.ItemIsEnabled

	# def flags ( self, model_index )

	# --------------------------------------------------

	def headerData ( self, section_index, orientation, role ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is( section_index, int )
		assert pytools.assertions.type_is( orientation, PyQt5.QtCore.Qt.Orientation )
		assert pytools.assertions.type_is( role, int )
		
		if orientation == PyQt5.QtCore.Qt.Horizontal:

			if role == PyQt5.QtCore.Qt.DisplayRole:
				return HierarchyModel.column_names[ section_index ]

		return PyQt5.QtCore.QVariant()

	# def headerData ( self, section_index, orientation, role )

	# --------------------------------------------------

	def index ( self, row, column, parent_index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is( row, int )
		assert pytools.assertions.type_is( column, int )
		assert pytools.assertions.type_is_instance_of( parent_index, PyQt5.QtCore.QModelIndex )

		if self.hasIndex( row, column, parent_index ):
			parent = self.nodeOfIndex( parent_index )
			child = parent.get_child_at( row )
			return self.createIndex( row, column, child )

		else:
			return PyQt5.QtCore.QModelIndex()
	
	# def index ( self, row, column, parent_index )

	# --------------------------------------------------

	def indexOfNode ( self, node ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( node, Node )

		if ( node == self.root_node ):
			return PyQt5.QtCore.QModelIndex()

		else:
			row = self.rowOfNode( node )
			return self.createIndex( row, 0, node )

	# def indexOfNode ( self, node )

	# --------------------------------------------------

	def fetchMore ( self, index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( index, PyQt5.QtCore.QModelIndex )

		node = index.internalPointer()
		node.load_children()

	# def fetchMore ( self, index )

	# --------------------------------------------------

	def nodeOfIndex ( self, index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( index, PyQt5.QtCore.QModelIndex )

		if index.isValid():
			return index.internalPointer()

		else:
			return self.root_node

	# def nodeOfIndex ( self, index )

	# --------------------------------------------------

	def parent ( self, child_index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( child_index, PyQt5.QtCore.QModelIndex )
		
		child  = self.nodeOfIndex( child_index )
		parent = child.parent

		if ( parent == self.root_node ):
			return PyQt5.QtCore.QModelIndex()

		else:
			row = self.rowOfNode( parent )
			return self.createIndex( row, 0, parent )

	# def parent ( self, child_index )

	# --------------------------------------------------

	def rowCount ( self, parent_index ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( parent_index, PyQt5.QtCore.QModelIndex )
		
		return self.nodeOfIndex( parent_index ).number_of_children

	# def rowCount ( self, parent_index )

	# --------------------------------------------------

	def rowOfNode ( self, node ):
		assert pytools.assertions.type_is_instance_of( self, HierarchyModel )
		assert pytools.assertions.type_is_instance_of( node, Node )
		
		return node.parent.get_index_of_child( node )

	# def rowOfNode ( self, node )

# class HierarchyModel ( PyQt5.QtCore.QAbstractItemModel )