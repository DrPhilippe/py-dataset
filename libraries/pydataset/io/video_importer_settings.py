# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from ..data             import ShapeData
from .importer_settings import ImporterSettings

# ##################################################
# ###            CLASS TASK-SETTINGS             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'VideoImporterSettings',
	   namespace = 'pydataset.io',
	fields_names = [
		'feature_name',
		'filepath'
		]
	)
class VideoImporterSettings ( ImporterSettings ):
	"""
	Base class for importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dst_group_url, feature_name='frame', filepath='', frame_size=ShapeData(), name='video-importer' ):
		"""
		Initializes a new instance of the `pydataset.io.VideoImporterSettings` class.

		Arguments:
			self (`pydataset.io.VideoImporterSettings`): Instance to initialize.
			dst_group_url                       (`str`): URL of the group where to import data to.
			feature_name                        (`str`): Name of the feature used to save the frames.
			filepath    (`pytools.path.FilePath`/`str`): Path of the io file to import.
			frame_size     (`pydataset.data.ShapeData`): If a none-zero shape is provided, images are resized to that shape.
			name                                (`str`): Name of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( feature_name, str )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )
		assert pytools.assertions.type_is_instance_of( frame_size, ShapeData )
		assert pytools.assertions.type_is( name, str )

		super( VideoImporterSettings, self ).__init__( dst_group_url, name )

		if isinstance(filepath, str):
			filepath = pytools.path.FilePath( filepath )

		self._feature_name = feature_name
		self._filepath     = filepath
		self._frame_size   = frame_size

	# def __init__ ( self, feature_name, filepath, frame_size, name )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def feature_name ( self ):
		"""
		Path of the video file to import (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )

		return self._feature_name

	# def feature_name ( self )
	
	# --------------------------------------------------

	@feature_name.setter
	def feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )
		assert pytools.assertions.type_is( value, str )

		self._feature_name = value
		
	# def feature_name ( self, value )

	# --------------------------------------------------

	@property
	def filepath ( self ):
		"""
		Path of the video file to import (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )

		return self._filepath

	# def filepath ( self )
	
	# --------------------------------------------------

	@filepath.setter
	def filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, pytools.path.FilePath )

		self._filepath = value
		
	# def filepath ( self, value )

	# --------------------------------------------------

	@property
	def frame_size ( self ):
		"""
		If a none-zero shape is provided, images are resized to that shape (`pydataset.data.ShapeData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )

		return self._frame_size

	# def frame_size ( self )
	
	# --------------------------------------------------

	@frame_size.setter
	def frame_size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VideoImporterSettings )
		assert pytools.assertions.type_is_instance_of( value, ShapeData )

		self._frame_size = value
		
	# def frame_size ( self, value )

# class VideoImporterSettings ( pydataset.dataset.ImporterSettings )