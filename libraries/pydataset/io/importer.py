# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.tasks

# LOCALS
from ..dataset import functions, Group
from .importer_settings import ImporterSettings

# ##################################################
# ###               CLASS IMPORTER               ###
# ##################################################

class Importer ( pytools.tasks.Task ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new importer instance.

		If dst_group is `None` the importer tries to resolve the dst_group using the URL provided in the settings.
		If dst_group is specify, the URL in the settings is overriden to the URL of the provided dst_group.

		Arguments:
			self                         (`pytools.dataset.Importer`): Importer to initialize.
			settings             (`pytools.dataset.ImporterSettings`): Settings of the importer.
			dst_group              (`None`/`pydataset.dataset.Group`): Group where data are imported to.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( settings, ImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Importer, self ).__init__(
			        settings = settings,
			          logger = logger,
			progress_tracker = progress_tracker
			)
		
		self._dst_group = dst_group

	# def __init__ ( self, settings, dst_group, logger, progress_tracker )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dst_group ( self ):
		"""
		Group where data are imported to.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )

		return self._dst_group

	# def dst_group ( self )
	
	# --------------------------------------------------

	@dst_group.setter
	def dst_group ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Importer )
		assert pytools.assertions.type_is_instance_of( value, Group )

		self._dst_group = value
		
	# def dst_group ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def resolve_group ( self ):
		"""
		If a groups is not already specified, resolves the group where to import data to using the provided settings.
		
		Arguments:
			self (`pytools.dataset.Importer`): The importer of which to resolve the destination group.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )

		self.dst_group, self.settings.dst_group_url = functions.resolve_group( self.dst_group, self.settings.dst_group_url )
		
	# def resolve_group ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this importer.

		Arguments:
			self (`pytools.dataset.Importer`): The importer to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )

		raise NotImplementedError()
		
	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this importer.

		Arguments:
			self (`pytools.dataset.Importer`): The importer to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Importer )
		
		super( Importer, self ).start()

		self.resolve_group()
			
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, dst_group=None, logger=None, progress_tracker=None, **kwargs ):
		"""
		Creates a new instance of the video importer of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of task.
			dst_group              (`None`/`pydataset.dataset.Group`): Group in which to import the frames of the video.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			**kwargs                                         (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, dst_group, logger, progress_tracker )

	# def create ( cls, logger, progress_tracker, **kwargs )

	# --------------------------------------------------
	
	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an improter.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.dataset.ImporterSettings`: The settings.
		"""
		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )

# class Importer ( pytools.tasks.Task )