# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2

# INTERNALS
import pytools.assertions
import pytools.tasks

# LOCALS
from ..data                   import ShapeData
from ..dataset                import Group, ImageFeatureData
from .importer                import Importer
from .video_importer_settings import VideoImporterSettings

# ##################################################
# ###            CLASS VIDEO-IMPORTER            ###
# ##################################################

class VideoImporter ( Importer ):
	"""
	Imports frames of a video as image features of examples.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initializes a new video importer instance.

		Arguments:
			self                      (`pytools.video.VideoImporter`): Video importer to initialize.
			group                         (`pydataset.dataset.Group`): Group in which to import the images.
			settings          (`pytools.video.VideoImporterSettings`): Settings of the video importer.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the video importer.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the video importer.
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporter )
		assert pytools.assertions.type_is_instance_of( settings, VideoImporterSettings )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( VideoImporter, self ).__init__( settings, dst_group, logger, progress_tracker )

	# def __init__ ( self, settings, dst_group, logger, progress_tracker )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def run ( self ):
		"""
		Run the importation of the video.

		Arguments:
			self (`pydataset.video.VideoImporter`): Video importer used to import the video.
		"""
		assert pytools.assertions.type_is_instance_of( self, VideoImporter )

		# Loop until frames can be read from the video
		frame_index = 0
		while ( self.is_running() and frame_index < self.frame_count ):

			# Update state
			self.update()

			# Read a frame
			ret, frame = self.capture.read()
			if ( not ret ):
				raise IOError( 'Failed to get frame N°{}/{}'.format(frame_index, self.frame_count) )

			# Resize ?
			if self.settings.frame_size != ShapeData():				
				width  = self.settings.frame_size[ 0 ]
				height = self.settings.frame_size[ 1 ]
				frame = cv2.resize( frame, (width, height) )

			# Create the example and feature
			example = self.dst_group.create_example()
			feature = example.create_feature(
				feature_data_type = ImageFeatureData,
				     feature_name = self.settings.feature_name,
				            shape = ShapeData(self.frame_width, self.frame_height, 3),
				            dtype = 'uint8',
				            value = frame,
				        extension = '.jpeg',
				            param = 90

				)

			# Log
			if ( self.logger ):
				self.logger.log( '{}/{}', frame_index, self.frame_count )
				self.logger.flush()
			if ( self.progress_tracker ):
				self.progress_tracker.update( float(frame_index)/float(self.frame_count) )

			frame_index += 1
			
		# while ( frame_index < frame_count )
		
		self.capture.release()

		# Log
		if ( self.logger ):
			self.logger.log( 'done' )
			self.logger.flush()
		if ( self.progress_tracker ):
			self.progress_tracker.update( 1.0 )
			
	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this importer.

		Arguments:
			self (`pytools.dataset.Importer`): The importer to start.
		"""
		super( VideoImporter, self ).start()

		# Log
		if ( self.logger ):
			self.logger.log( 'VideoImporter' )
			self.logger.log( '    dst_group_url: {}', self.settings.dst_group_url )
			self.logger.log( '    feature_name:  {}', self.settings.feature_name )
			self.logger.log( '    filepath:      {}', self.settings.filepath )
			self.logger.end_line()
			self.logger.flush()

		# Check file
		if self.settings.filepath.is_not_a_file():
			raise IOError( 'The file {} does not exist'.format(filepath) )

		# Open the video file
		self.capture = cv2.VideoCapture( self.settings.filepath.get_value() )
		
		# Check the video was successfully opened
		if ( not self.capture.isOpened() ):
			raise IOError( "Failed to open video '{}'".format(self.settings.filepath) )

		self.frame_count  = int( self.capture.get( cv2.CAP_PROP_FRAME_COUNT  ) )
		self.frame_width  = int( self.capture.get( cv2.CAP_PROP_FRAME_WIDTH  ) )
		self.frame_height = int( self.capture.get( cv2.CAP_PROP_FRAME_HEIGHT ) )
		
		# Log
		if ( self.logger ):
			self.logger.log( 'video:' )
			self.logger.log( '    frame_count: {}', self.frame_count )
			self.logger.log( '    frame_width: {}', self.frame_width )
			self.logger.log( '    frame_height: {}', self.frame_height )
			self.logger.end_line()
			self.logger.flush()
			
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for this video importer.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the video importer settings constructor.

		Returns:
			`pydataset.video.VideoImporterSettings` The video importer settings.
		"""
		return VideoImporterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class VideoImporter ( Importer )