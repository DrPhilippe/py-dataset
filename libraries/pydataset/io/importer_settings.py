# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###          CLASS IMPORTER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImporterSettings',
	   namespace = 'pydataset.io',
	fields_names = [
		'dst_group_url'
		]
	)
class ImporterSettings ( pytools.tasks.TaskSettings ):
	"""
	Base class for importer settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dst_group_url, name='importer' ):
		"""
		Initializes a new instance of the `pydataset.io.ImporterSettings` class.

		Arguments:
			self (`pydataset.io.ImporterSettings`): Instance to initialize.
			dst_group_url                  (`str`): URL of the group where to import data to.
			name                           (`str`): Name of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImporterSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( name, str )

		super( ImporterSettings, self ).__init__( name )

		self.dst_group_url = dst_group_url

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dst_group_url ( self ):
		"""
		URL of the group where to import data to.
		"""
		return self._dst_group_url
		
	# def dst_group_url ( self )

	# --------------------------------------------------

	@dst_group_url.setter
	def dst_group_url ( self, value ):
		
		self._dst_group_url = value
	
	# def dst_group_url ( self, value )

# class ImporterSettings ( pytools.tasks.TaskSettings )