# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def create_cells_origines_mask (
	number_of_cells=7,
	number_of_detectors_per_cell=2,
	number_of_coordinates=5,
	image_size=(448,448),
	X=0,
	Y=1
	):
	"""
	Creates an array that can be used to convert from yolo bounding rectangle format to classical bounsing rectangles.
	
	The result is an array of shape `(number_of_cells, number_of_cells, number_of_detectors_per_cell, 5)`.
	It contains for each cell the x and y coordinates of the cell's top-left corners,
	and zeros in the 3 remaining coordinates.

	Arguments:
		number_of_cells              (`int`): Number of cells on the width and height of the image (aka. S).
		number_of_detectors_per_cell (`int`): Number of bounding box detector per cell (aka. B).
		image_size   (`tuple` of `2` `int`s): Yolo full image size as `(height, width)`.
		number_of_coordinates        (`int`): Number of coordinates in each rectangle.
		X                            (`int`): Index of the X coordinate of the rectangles.
		Y                            (`int`): Index of the Y coordinate of the rectangles.
	
	Returns:
		`numpy.ndarray`: Array of shape containing the cells top-left corners coordinates.
	"""
	# print( 'create_cells_origines_mask( S={}, B={}, C={}, im={}x{}, X={}, Y={} )'.format(
	# 	number_of_cells,
	# 	number_of_detectors_per_cell,
	# 	number_of_coordinates,
	# 	image_size[X], image_size[Y],
	# 	X, Y
	# 	))
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.true( number_of_cells>0 )
	assert pytools.assertions.type_is( number_of_detectors_per_cell, int )
	assert pytools.assertions.true( number_of_detectors_per_cell>0 )
	assert pytools.assertions.type_is( number_of_coordinates, int )
	assert pytools.assertions.true( number_of_coordinates>1 )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, int )
	assert pytools.assertions.type_is( X, int )
	assert pytools.assertions.type_is( Y, int )

	# Usefull values
	S            = number_of_cells
	B            = number_of_detectors_per_cell
	D            = number_of_coordinates #i.e. number of channels, depth
	image_height = float( image_size[X] )
	image_width  = float( image_size[Y] )
	cells_height = image_height / float(S)
	cells_width  = image_width  / float(S)

	mask = numpy.zeros( [S, S, 1, D], dtype=numpy.float32 )

	for y in range(S):
		for x in range(S):
			mask[y, x, 0, X] = cells_width  * float(x)
			mask[y, x, 0, Y] = cells_height * float(y)

	mask = numpy.tile( mask, [1, 1, B, 1] )
	mask = numpy.reshape( mask, [S, S, B*D] )
	return mask

# def create_cells_origines_mask ( ... )

# --------------------------------------------------

def yolo_predictions_yo_xyxy (
	rectangles,
	image_size=(448,448),
	number_of_cells=7,
	number_of_detectors_per_cell=2,
	number_of_classes=20,
	X=0,
	Y=1
	):
	"""
	Converts yolo bounding rectangles to bounding rectangles in the format x_min, y_min, x_max, y_max.
	
	Arguments:
		rectangles       (`numpy.ndarray`): Bounding rectangles in yolo format with shape [BATCH, S, S, B*5].
		image_size (`tuple` of `2` `int`s): Image size in pixels.
		X                          (`int`): Index of the X coordinate of the rectangles.
		Y                          (`int`): Index of the Y coordinate of the rectangles.

	Returns:
		`numpy.ndarray`: The bounding rectangles in x_min, y_min, x_max, y_max format.
	"""
	assert pytools.assertions.type_is( rectangles, numpy.ndarray )
	assert pytools.assertions.equal( rectangles.dtype, numpy.float32 )

	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, int )
	assert pytools.assertions.type_is( number_of_cells, int )

	# Usefull values
	S            = number_of_cells
	B            = number_of_detectors_per_cell
	C            = number_of_classes
	DEPTH        = 5
	image_height = float( image_size[X] )
	image_width  = float( image_size[Y] )
	cells_height = image_height / float(S)
	cells_width  = image_width / float(S)
	W, H         = X+2, Y+2

	# Get the batch size
	BATCH = rectangles.shape[0]

	# First reshape the rectangles to have an array of rectangles
	rectangles = numpy.reshape( rectangles, [BATCH*S*S*B, DEPTH] ) # [BATCH*S*S*B, DEPTH]

	# The bounding rectangle are localized using their center localtion
	# using relaive cell coordinates: [0,1] within the cell
	# So first we need to express them in pixel coordinates
	rectangles[ :, X ] = numpy.multiply( rectangles[ :, X ], cells_width  )  # [BATCH*S*S*B]
	rectangles[ :, Y ] = numpy.multiply( rectangles[ :, Y ], cells_height )  # [BATCH*S*S*B]

	# The bounding rectangle are sized using image relative size.
	# We need to multiply the rectangle width by the image width,
	# and to multiply the rectangle height by the image height
	rectangles[ :, W ] = numpy.multiply( numpy.square(rectangles[ :, W ]), image_width  ) # [BATCH*S*S*B]
	rectangles[ :, H ] = numpy.multiply( numpy.square(rectangles[ :, H ]), image_height ) # [BATCH*S*S*B]

	# Now the X,Y coordinates are the center of the bounding box, with respect to the cells origines
	# and the width and heights in the expected pixel size with respect to the wall image.

	# We need to add the localtion the top-left corner of a cell to the rectangle it contains.
	# We do this using a mask containing the coordinates of the top-left corner of a cell.
	cells_positions_mask = create_cells_origines_mask( S, B, DEPTH, image_size, X, Y ) # [S, S, B*DEPTH]
	
	# We repeat it for each batch
	cells_positions_mask = numpy.reshape( cells_positions_mask, [1, S, S, B*DEPTH] ) # [1, S, S, B*DEPTH]
	cells_positions_mask = numpy.tile( cells_positions_mask, [BATCH, 1, 1, 1] ) # [BATCH, S, S, B*DEPTH]
	cells_positions_mask = numpy.reshape( cells_positions_mask, [BATCH*S*S*B, DEPTH] ) # [BATCH*S*S*B, DEPTH]
	
	# We can now add the mask to the image to get the rectangles
	# where their location is defined using image pixel coordinates
	rectangles = numpy.add( rectangles, cells_positions_mask ) # [BATCH*S*S*B, DEPTH]

	# We remove half the width and height of the bounding rectangle to X and Y to get the top left corner X1 and X2
	rectangles[ :, X ] = numpy.subtract( rectangles[ :, X ], numpy.divide( rectangles[ :, W ], 2. ) ) # [BATCH*S*S*B]
	rectangles[ :, Y ] = numpy.subtract( rectangles[ :, Y ], numpy.divide( rectangles[ :, W ], 2. ) ) # [BATCH*S*S*B]

	# We also need to clamp by 0 for rectangle that where already 0
	rectangles[ :, X ] = numpy.clip( rectangles[ :, X ], 0., image_width ) # [BATCH*S*S*B]
	rectangles[ :, Y ] = numpy.clip( rectangles[ :, Y ], 0., image_height ) # [BATCH*S*S*B]

	# Now because we want to compute IoUs, it is better to have
	# rectangle in the format x_min, y_min, x_max, y_max.

	# We now add X1 to W and Y1 to H to get X2 and Y2
	rectangles[ :, W ] = numpy.add( rectangles[ :, W ], rectangles[ :, X ] ) # [BATCH*S*S*B]
	rectangles[ :, H ] = numpy.add( rectangles[ :, H ], rectangles[ :, Y ] ) # [BATCH*S*S*B]

	rectangles = numpy.reshape( rectangles, [BATCH, S, S, DEPTH*B])
	# And return the result
	return rectangles

# def yolo_rectangles_yo_xyxy ( ... )

# -------------------------------------------------

def xyxy_to_yolo_rectangles (
	rectangles,
	number_of_cells=7,
	image_size=(448, 448),
	X1=0,
	Y1=1
	):
	"""
	Converts a rectangle from the format x_min, y_min, x_max, y_max to a rectangle in the yolo format.
	"""
	assert pytools.assertions.type_is( rectangles, numpy.ndarray )
	assert pytools.assertions.equal( rectangles.dtype, numpy.float32 )
	assert pytools.assertions.true( rectangles.shape[-1] > 3 )
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, int )

	# Usefull values
	S            = number_of_cells
	image_width  = float( image_size[ X1 ] )
	image_height = float( image_size[ Y1 ] )
	cells_width  = image_width  / float(S)
	cells_height = image_height / float(S)
	X2, Y2       = X1+2, Y1+2

	# Reshape rectangle(s) to get an array of rectangles
	shape_backup = rectangles.shape
	D            = shape_backup[ -1 ]
	rectangles   = numpy.reshape( rectangles, [-1, D] )

	# Get rectangles coordinates
	rectangles_x1     = rectangles[ :, X1 ]
	rectangles_y1     = rectangles[ :, Y1 ]
	rectangles_x2     = rectangles[ :, X2 ]
	rectangles_y2     = rectangles[ :, Y2 ]
	rectangles_width  = numpy.subtract( rectangles_x2, rectangles_x1 )
	rectangles_height = numpy.subtract( rectangles_y2, rectangles_y1 )

		# Compute the coordinates of the bounding rectangles centers
	rectangles_center_x = numpy.add( rectangles_x1, numpy.divide( rectangles_width, 2. ) )
	rectangles_center_y = numpy.add( rectangles_y1, numpy.divide( rectangles_height, 2. ) )
	
	# Compute the coorinates of the cells where the bounding rectangles belong
	cell_index_x = numpy.floor( numpy.divide( rectangles_center_x, cells_width  ) )
	cell_index_y = numpy.floor( numpy.divide( rectangles_center_x, cells_height ) )
	cell_x       = numpy.multiply( cell_index_x, cells_width  )
	cell_y       = numpy.multiply( cell_index_y, cells_height )
	
	# Bounding rectangles centers with respect to the cell they belong to
	rectangles_center_x = numpy.divide( numpy.subtract( rectangles_center_x, cell_x ), cells_width  )
	rectangles_center_y = numpy.divide( numpy.subtract( rectangles_center_y, cell_y ), cells_height )
	
	# Bounding rectangle width and height with respect to the image size
	rectangles_width  = numpy.divide( rectangles_width, image_width )
	rectangles_height = numpy.divide( rectangles_height, image_height )

	# Write back the rectangle coordinates
	rectangles[ :, X1 ] = rectangles_center_x
	rectangles[ :, Y1 ] = rectangles_center_y
	rectangles[ :, X2 ] = rectangles_width
	rectangles[ :, Y2 ] = rectangles_height

	# Restore the shape of the rectangles and return them
	return numpy.reshape( rectangles, shape_backup )

# def xyxy_to_yolo_rectangles ( ... )

# -------------------------------------------------

def xywh_to_yolo_rectangles (
	rectangles,
	number_of_cells=7,
	image_size=(448, 448),
	X=0,
	Y=1
	):
	"""
	Converts a rectangle from the format x_min, y_min, width, height to a rectangle in the yolo format.
	"""
	assert pytools.assertions.type_is( rectangles, numpy.ndarray )
	assert pytools.assertions.equal( rectangles.dtype, numpy.float32 )
	assert pytools.assertions.true( rectangles.shape[-1] > 3 )
	assert pytools.assertions.type_is( number_of_cells, int )
	assert pytools.assertions.type_is( image_size, tuple )
	assert pytools.assertions.equal( len(image_size), 2 )
	assert pytools.assertions.tuple_items_type_is( image_size, int )

	# Usefull values
	S            = number_of_cells
	image_width  = float( image_size[ X ] )
	image_height = float( image_size[ Y ] )
	cells_width  = image_width  / float(S)
	cells_height = image_height / float(S)
	W, H         = X+2, Y+2

	# Reshape rectangle(s) to get an array of rectangles
	shape_backup = rectangles.shape
	D            = shape_backup[ -1 ]
	rectangles   = numpy.reshape( rectangles, [-1, D] )

	# Get rectangles coordinates
	rectangles_x1     = rectangles[ :, X ]
	rectangles_y1     = rectangles[ :, Y ]
	rectangles_width  = rectangles[ :, W ]
	rectangles_height = rectangles[ :, H ]

	# Compute the coordinates of the bounding rectangles centers
	rectangles_center_x = numpy.add( rectangles_x1, numpy.divide( rectangles_width, 2. ) )
	rectangles_center_y = numpy.add( rectangles_y1, numpy.divide( rectangles_height, 2. ) )
	
	# Compute the coorinates of the cells where the bounding rectangles belong
	cell_index_x = numpy.floor( numpy.divide( rectangles_center_x, cells_width  ) )
	cell_index_y = numpy.floor( numpy.divide( rectangles_center_x, cells_height ) )
	cell_x       = numpy.multiply( cell_index_x, cells_width  )
	cell_y       = numpy.multiply( cell_index_y, cells_height )
	
	# Bounding rectangles centers with respect to the cell they belong to
	rectangles_center_x = numpy.divide( numpy.subtract( rectangles_center_x, cell_x ), cells_width  )
	rectangles_center_y = numpy.divide( numpy.subtract( rectangles_center_y, cell_y ), cells_height )
	
	# Bounding rectangle width and height with respect to the image size
	rectangles_width  = numpy.divide( rectangles_width, image_width )
	rectangles_height = numpy.divide( rectangles_height, image_height )

	# Write back the rectangle coordinates
	rectangles[ :, X ] = rectangles_center_x
	rectangles[ :, Y ] = rectangles_center_y
	rectangles[ :, W ] = rectangles_width
	rectangles[ :, H ] = rectangles_height

	# Restore the shape of the rectangles and return them
	return numpy.reshape( rectangles, shape_backup )

# def xywh_to_yolo_rectangles ( ... )