# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###         CLASS MANIPULATOR-SETTINGS         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ManipulatorSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = [
		'dst_group_url',
		'name',
		'url_search_pattern'
		]
	)
class ManipulatorSettings ( pytools.serialization.Serializable ):
	"""
	Manipualtors settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, url_search_pattern='', dst_group_url='', name='' ):
		"""
		Initializes a new instance of the manipulator settings class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to manipulate.

		Arguments:
			self (`pydataset.manipualtors.ManipulatorSettings`): Instance to initialize.
			url_search_pattern                          (`str`): URL used to search for example(s) to manipulate.
			dst_group_url                               (`str`): URL of the group where to save the manipulated examples.
			name                                        (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		self._url_search_pattern = url_search_pattern
		self._dst_group_url      = dst_group_url
		self._name               = name

	# def __init__ ( self, url_search_pattern, dst_group_url, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL used to search for example(s) to manipulate (`str`).

		The url search pattern is used to configure an iterator,
		thus defining which example(s) to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )

		return self._url_search_pattern
	
	# def url_search_pattern ( self )

	# --------------------------------------------------

	@url_search_pattern.setter
	def url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._url_search_pattern = value
	
	# def url_search_pattern ( self, value )
	
	# --------------------------------------------------

	@property
	def dst_group_url ( self ):
		"""
		URL of the group where to save the manipulated examples (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )

		return self._dst_group_url
	
	# def dst_group_url ( self )

	# --------------------------------------------------

	@dst_group_url.setter
	def dst_group_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._dst_group_url = value
	
	# def dst_group_url ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the manipulator (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )

		return self._name
	
	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
	
	# def name ( self, value )

# class ManipulatorSettings ( pytools.serialization.Serializable )