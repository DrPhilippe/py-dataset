# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..pose import MeshToBoundingBoxSettings

# ##################################################
# ###       CLASS MESH-TO-BB8-V2-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshToBB8V2Settings',
	   namespace = 'pydataset.manipulators.bb8',
	fields_names = [
		'points_offsets'
		]
	)
class MeshToBB8V2Settings ( MeshToBoundingBoxSettings ):
	"""
	Settings of the `pykeras.manipulators.bb8.MeshToBB8V2Settings` class.
	"""

	# ##################################################
	# ###                 CONSTANTS                  ###
	# ##################################################

	# --------------------------------------------------

	DEFAULT_OFFSETS = numpy.asarray([
		[ 0.00,  0.00,  0.00 ]#,
		# [-0.01,  0.00,  0.00 ],
		# [ 0.01,  0.00,  0.00 ],
		# [ 0.00, -0.01,  0.00 ],
		# [ 0.00,  0.01,  0.00 ],
		# [ 0.00,  0.00, -0.01 ],
		# [ 0.00,  0.00,  0.01 ]
		]).astype( numpy.float32 )
		
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		# in
		mesh_data_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		camera_k_feature_name = '',
		camera_dist_coeffs_feature_name = '',
		points_offsets = DEFAULT_OFFSETS,
		# out
		bb8_feature_name = '',
		**kwargs
		):
		"""
		Creates a new instance of the `pydataset.manipulators.bb8.MeshToBB8V2Settings` class.
	
		If only `mesh_data_feature_name` is specified, the bounding box is computed in the model space.
		If `rotation_feature_name` and `translation_feature_name` are specified it is computed in camera space.
		If `camera_k_feature_name` and `camera_dist_coeffs_feature_name` are specified it is computed in image space.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V2Settings`): Instance to initialize.
			mesh_data_feature_name                          (`str`): Name of the feature containing the mesh of the object.
			rotation_feature_name                           (`str`): Name of the feature containing the rotation matrix, vector or quaternion.
			translation_feature_name                        (`str`): Name of the feature containing the translation vector.
			camera_k_feature_name                           (`str`): Name of the feature containing the camera k matrix.
			camera_dist_coeffs_feature_name                 (`str`): Name of the feature containing the camera distortion coefficient vector.
			points_offsets                        (`numpy.ndarray`): Offsets applied to each bounding box corner to create the points.
			bb8_feature_name                                (`str`): Name of the feature where to save the bb8 points.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )
		assert pytools.assertions.type_is( mesh_data_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( camera_k_feature_name, str )
		assert pytools.assertions.type_is( camera_dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( points_offsets, numpy.ndarray )
		assert pytools.assertions.equal( len(points_offsets.shape), 2 )
		assert pytools.assertions.equal( points_offsets.shape[1], 3 )
		assert pytools.assertions.equal( points_offsets.dtype, numpy.float32 )
		assert pytools.assertions.type_is( bb8_feature_name, str )


		super( MeshToBB8V2Settings, self ).__init__(
			         mesh_data_feature_name = mesh_data_feature_name,
			          rotation_feature_name = rotation_feature_name,
			       translation_feature_name = translation_feature_name,
			          camera_k_feature_name = camera_k_feature_name,
			camera_dist_coeffs_feature_name = camera_dist_coeffs_feature_name,
			      bounding_box_feature_name = bb8_feature_name,
			**kwargs
			)

		self._points_offsets = points_offsets.copy()

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def points_offsets ( self ):
		"""
		Offsets applied to each bounding box corner to create the points (`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )

		return self._points_offsets

	# def points_offsets ( self )

	# --------------------------------------------------

	@points_offsets.setter
	def points_offsets ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )
		assert pytools.assertions.type_is( value, numpy.ndarray )
		assert pytools.assertions.equal( len(value.shape), 2 )
		assert pytools.assertions.equal( value.shape[1], 3 )
		assert pytools.assertions.equal( value.dtype, numpy.float32 )

		self._points_offsets = value.copy()
	
	# def points_offsets ( self, value )

	# --------------------------------------------------

	@property
	def points_offsets_count ( self ):
		"""
		Number of offsets applied to each bounding box corner to create the points (`numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )

		return self._points_offsets.shape[ 0 ]

	# def points_offsets_count ( self )

	# --------------------------------------------------

	@property
	def bb8_feature_name ( self ):
		"""
		Name of the feature where to save the bb8 points (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )

		return self._bounding_box_feature_name

	# def bb8_feature_name ( self )

	# --------------------------------------------------

	@bb8_feature_name.setter
	def bb8_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_box_feature_name = value
	
	# def bb8_feature_name ( self, value )
	

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this `pydataset.manipulators.bb8.MeshToBB8V2Settings` into a python `dict`.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V2Settings`): MeshToBB8V2Settings to serialize.
			ignores                               (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		
		# Run pydataset.data.ItemData serialize method, but skip value and dtype
		if ( 'points_offsets' not in ignores ):
			ignores.append( 'points_offsets' )
		
		data = super( BB8CreatorSettings, self ).serialize( ignores )

		# serialize the value
		values, shape, dtype = pytools.serialization.serialize_ndarray( self.points_offsets )
		data[ 'points_offsets' ] = {
			'count': self.points_offsets.shape[ 0 ],
			'values': values
		}

		# Return the data
		return data

	# def serialize ( self, ignores )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this `pydataset.manipulators.bb8.MeshToBB8V2Settings` from a python `dict`.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V2Settings`): MeshToBB8V2Settings to deserialize.
			data                                           (`dict`): The serialized data.
			ignores                               (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2Settings )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
	
		# Run default deserialize method, but skip value
		if 'points_offsets' not in ignores:
			ignores.append( 'points_offsets' )

		super( MeshToBB8V2Settings, self ).deserialize( data, ignores )

		# Deserialzize the dtype value as str
		count  = data[ 'points_offsets' ][ 'count'  ]
		values = data[ 'points_offsets' ][ 'values' ]
		value = pytools.serialization.deserialize_ndarray( values, [count, 3], 'float32' )
		self._points_offsets = value
		
	# def __deserialize__ ( self, data, ignores )

# class MeshToBB8V2Settings ( MeshToBoundingBoxSettings )