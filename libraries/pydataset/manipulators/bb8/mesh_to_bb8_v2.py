# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from ..pose import MeshToBoundingBox
from .mesh_to_bb8_v2_settings import MeshToBB8V2Settings

# ##################################################
# ###            CLASS MESH-TO-BB8-V2            ###
# ##################################################

class MeshToBB8V2 ( MeshToBoundingBox ):
	"""
	Mapper used to create BB8 points using the mesh of the object.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.bb8.MeshToBB8V2` class.

		Arguments:
			self             (`pydataset.manipulators.bb8.MeshToBB8V2`): Instance to initialize.
			settings (`pydataset.manipulators.bb8.MeshToBB8V2Settings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2 )
		assert pytools.assertions.type_is_instance_of( settings, MeshToBB8V2Settings )

		super( MeshToBB8V2, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_offseted_points ( self, points ):
		"""
		Create a set of control points using offsets for each point in the given set of points.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V2`): MeshToBB8V2 instance.
			points                        (`numpy.ndarray`): Set of points in model space with shape `[-1, 3]` and dtype `float32`.

		Returns:
			`numpy.ndarray`: Offseted points.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2 )
		assert pytools.assertions.type_is( points, numpy.ndarray )
		assert pytools.assertions.equal( points.dtype, numpy.float32 )
		assert pytools.assertions.equal( len(points.shape), 2 )
		assert pytools.assertions.equal( points.shape[1], 3 )

		# Number of points to offset
		nb_points = points.shape[0]

		# Number of offsets
		offsets    = self.settings.points_offsets
		nb_offsets = offsets.shape[ 0 ]

		# Result array
		nb_results = nb_points*nb_offsets		
		results    = numpy.empty( [nb_results, 3], dtype=numpy.float32 )

		# Offset each point
		for point_index in range( nb_points ):

			# Get the current point
			point = points[ point_index, : ]

			# Offset each control point
			for offset_index in range( nb_offsets ):
				
				# Get the current offset
				offset = offsets[ offset_index, : ]

				# result point
				result = numpy.add( point, offset )

				# Save result
				results[ point_index*nb_offsets + offset_index, : ] = result
			
			# for offset_index in range( nb_offsets )
		# for point_index in range( nb_points )

		return results

	# def create_offseted_points ( self, points )

	# --------------------------------------------------

	def create_bb8 ( self, example, ref=False ):
		"""
		Creates the bb8 of the given example.

		This method first get the raw bounding box,
		then it is extended by creating as many offseted points as required,
		finaly the points are projected in model space, camera sapce, or image plane
		depending of the settings of the creator.

		Arguments:
			self (`pydataset.manipulators.pose.MeshToBB8V2`): BB8 creator instance.
			example            (`pydataset.dataset.Example`): Example for which to create the control points.

		Returns:
			`numpy.ndarray`: bb8 value.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2 )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Get the bounding box of the current example
		bb = self.get_bounding_box( example )
		
		# Create offseted points
		bb8 = self.create_offseted_points( bb )
		
		# Project the points
		if not ref:
			bb8 = self.project_points_if_needed( example, bb8 )

		return bb8

	# def create_bb8 ( self, example, ref )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Creates the bb8 control points for the given example.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V2`): BB8 creator instance.
			example           (`pydataset.dataset.Example`): Example for which to create the control points.

		Returns:
			`pydataset.dataset.Example`: The example with added control points.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V2 )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Create BB8 points
		bb8     = self.create_bb8( example )
		bb8_ref = self.create_bb8( example, ref=True )
		
		# Get the pose
		R = example.get_feature( self.settings.rotation_feature_name ).value
		t = example.get_feature( self.settings.translation_feature_name ).value
	
		# Save the control points
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bb8_feature_name,
			            value = bb8
			)
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bb8_feature_name+'_ref',
			            value = numpy.concatenate( [R.ravel(), t.ravel(), bb8_ref.ravel()] )
			)
		
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.bb8.MeshToBB8V2` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.bb8.MeshToBB8V2`.

		Returns:
			`pydataset.manipulators.bb8.MeshToBB8V2Settings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return MeshToBB8V2Settings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MeshToBB8V2 ( MeshToBoundingBox )