# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .mesh_to_bb8_v2_settings import MeshToBB8V2Settings

# ##################################################
# ###       CLASS MESH-TO-BB8-V1-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshToBB8V1Settings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'instances_url_search_pattern',
		'number_of_categories',
		'category_index_feature_name'
		]
	)
class MeshToBB8V1Settings ( MeshToBB8V2Settings ):
	"""
	Settings of the `pykeras.manipulators.bb8.MeshToBB8V1` class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		# in
		mesh_data_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		camera_k_feature_name = '',
		camera_dist_coeffs_feature_name = '',
		instances_url_search_pattern = 'examples/*',
		number_of_categories = 0,
		category_index_feature_name = '',
		points_offsets = MeshToBB8V2Settings.DEFAULT_OFFSETS,
		# out
		bb8_feature_name = '',
		**kwargs
		):
		"""
		Creates a new instance of the `pydataset.manipulators.bb8.MeshToBB8V1Settings` class.
	
		If only `mesh_data_feature_name` is specified, the bounding box is computed in the model space.
		If `rotation_feature_name` and `translation_feature_name` are specified it is computed in camera space.
		If `camera_k_feature_name` and `camera_dist_coeffs_feature_name` are specified it is computed in image space.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V1Settings`): Instance to initialize.
			mesh_data_feature_name                          (`str`): Name of the feature containing the mesh of the object.
			rotation_feature_name                           (`str`): Name of the feature containing the rotation matrix, vector or quaternion.
			translation_feature_name                        (`str`): Name of the feature containing the translation vector.
			camera_k_feature_name                           (`str`): Name of the feature containing the camera k matrix.
			camera_dist_coeffs_feature_name                 (`str`): Name of the feature containing the camera distortion coefficient vector.
			instances_url_search_pattern                    (`str`): URL used to find instances in the current example.
			number_of_categories                            (`int`): Number of categories in the dataset.
			category_index_feature_name                     (`str`): Name of the feature containing the category index.
			points_offsets                        (`numpy.ndarray`): Offsets applied to each bounding box corner to create the points.
			bb8_feature_name                                (`str`): Name of the feature where to save the bb8 points.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )
		assert pytools.assertions.type_is( mesh_data_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( camera_k_feature_name, str )
		assert pytools.assertions.type_is( camera_dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( number_of_categories, int )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( points_offsets, numpy.ndarray )
		assert pytools.assertions.equal( len(points_offsets.shape), 2 )
		assert pytools.assertions.equal( points_offsets.shape[1], 3 )
		assert pytools.assertions.equal( points_offsets.dtype, numpy.float32 )
		assert pytools.assertions.type_is( bb8_feature_name, str )

		super( MeshToBB8V1Settings, self ).__init__(
			mesh_data_feature_name          = mesh_data_feature_name,
			rotation_feature_name           = rotation_feature_name,
			translation_feature_name        = translation_feature_name,
			camera_k_feature_name           = camera_k_feature_name,
			camera_dist_coeffs_feature_name = camera_dist_coeffs_feature_name,
			points_offsets                  = points_offsets,
			bb8_feature_name                = bb8_feature_name,
			**kwargs
			)

		self._instances_url_search_pattern = instances_url_search_pattern
		self._number_of_categories         = number_of_categories
		self._category_index_feature_name  = category_index_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def instances_url_search_pattern ( self ):
		"""
		URL used to find instances in the current example (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )

		return self._instances_url_search_pattern
	
	# def instances_url_search_pattern ( self )

	# --------------------------------------------------

	@instances_url_search_pattern.setter
	def instances_url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )
		assert pytools.assertions.type_is( value, str )

		self._instances_url_search_pattern = value
	
	# def instances_url_search_pattern ( self, value )

	# --------------------------------------------------

	@property
	def number_of_categories ( self ):
		"""
		Number of object categories.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )

		return self._number_of_categories

	# def number_of_categories ( self )

	# --------------------------------------------------

	@number_of_categories.setter
	def number_of_categories ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_categories = value
		
	# def number_of_categories ( self, value )
	
	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Number of object categories.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )

		return self._category_index_feature_name

	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1Settings )
		assert pytools.assertions.type_is( value, str )

		self._category_index_feature_name = value
		
	# def category_index_feature_name ( self, value )
	
# class MeshToBB8V1Settings ( BB8CreatorSettings )