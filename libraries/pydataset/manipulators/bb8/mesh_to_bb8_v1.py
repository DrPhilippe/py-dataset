# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from .mesh_to_bb8_v2          import MeshToBB8V2
from .mesh_to_bb8_v1_settings import MeshToBB8V1Settings

# ##################################################
# ###            CLASS MESH-TO-BB8-V1            ###
# ##################################################

class MeshToBB8V1 ( MeshToBB8V2 ):
	"""
	Mapper used to create BB8 points using the mesh of the object.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.bb8.MeshToBB8V1` class.

		Arguments:
			self             (`pydataset.manipulators.bb8.MeshToBB8V1`): Instance to initialize.
			settings (`pydataset.manipulators.bb8.MeshToBB8V1Settings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1 )
		assert pytools.assertions.type_is_instance_of( settings, MeshToBB8V1Settings )

		super( MeshToBB8V1, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def create_instances_iterator ( self, example ):
		"""
		Creates the instances iterator.

		Arguments:
			self (`pydataset.manipualtors.bb8.MeshToBB8V1`): BB8 points creator.
			example           (`pydataset.dataset.Example`): Example being mapped.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1 )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Compose the absolute URL search pattern
		url = example.absolute_url + '/' + self.settings.instances_url_search_pattern
		
		# Create the iterator
		iterator = pydataset.dataset.Iterator( url )

		# Check it
		if len(iterator) == 0:
			raise ValueError( "Could not find object instances examples using url '{}'".format(url) )

		# Return it
		return iterator
	
	# def create_instances_iterator ( self, example )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Creates the BB8 points for the given example.

		Arguments:
			self (`pydataset.manipulators.bb8.MeshToBB8V1`): BB8 mapper.
			example           (`pydataset.dataset.Example`): Example for which to create the BB8 points.

		Returns:
			`pydataset.dataset.Example`: The example with added BB8 points.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBB8V1 )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
				
		# Get settings
		nb_categories = self.settings.number_of_categories
		nb_offsets    = self.settings.points_offsets_count
		
		# Create BB8 control points buffer
		depth = 2 if self.settings.camera_k_feature_name else 3
		bb8_v1 = numpy.empty( [nb_categories, 8*nb_offsets, depth], dtype=numpy.float32 ) # [C, 8, 2/3]
		bb8_v1.fill( numpy.nan )

		# Create BB8 refferences buffer
		# Contains R, t, ref points
		bb8_v1_ref = numpy.empty( [nb_categories, 3*3 + 3 + 8*nb_offsets*3], dtype=numpy.float32 ) # [C, 9+3+8*3]
		
		# Are objects instances in sub-examples ?
		if self.settings.instances_url_search_pattern:

			# Go though each object instance seen in the image
			for sub_example in self.create_instances_iterator( example ):

				# Create the bb8 points
				bb8     = self.create_bb8( sub_example )
				bb8_ref = self.create_bb8( sub_example, ref=True )

				# Get the pose
				R = sub_example.get_feature( self.settings.rotation_feature_name ).value
				t = sub_example.get_feature( self.settings.translation_feature_name ).value

				# Get the category index
				category_index = sub_example.get_feature( self.settings.category_index_feature_name ).value
				
				# Set control points for that object
				# If more than one object of this instance is visible
				# only the first instance considered will be kept
				if numpy.isnan( numpy.sum( bb8_v1[ category_index, :, : ] ) ):
					bb8_v1[ category_index, :, : ] = bb8
					bb8_v1_ref[ category_index, : ] = numpy.concatenate( [R.ravel(), t.ravel(), bb8_ref.ravel()] )
			
			# for sub_example in iterator:
		
		# If not, we asume only one object is visible in THIS example (self)
		else:
			# Create the bb8 points
			bb8 = self.create_bb8( example )
			bb8_ref = self.create_bb8( example, ref=True )

			# Get the pose
			R = example.get_feature( self.settings.rotation_feature_name ).value
			t = example.get_feature( self.settings.translation_feature_name ).value

			# Get the category index
			category_index = example.get_feature( self.settings.category_index_feature_name ).value

			# Set control points for this object
			bb8_v1[ category_index, :, : ] = bb8
			bb8_v1_ref[ category_index, : ] = numpy.concatenate( [R.ravel(), t.ravel(), bb8_ref.ravel()] )
			
		# if objects_url

		# Save the control points
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bb8_feature_name,
			            value = bb8_v1
			)
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bb8_feature_name+'_ref',
			            value = bb8_v1_ref
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.pose.MeshToBB8V1` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.pose.MeshToBB8V1`.

		Returns:
			`pydataset.manipulators.pose.MeshToBB8V1Settings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return MeshToBB8V1Settings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MeshToBB8V1 ( MeshToBB8V2 )