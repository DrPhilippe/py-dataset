# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###      CLASS MANIPULATOR-TASK-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ManipulatorTaskSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = [
		'url_search_pattern_prefix',
		'number_of_threads'
		]
	)
class ManipulatorTaskSettings ( pytools.tasks.TaskSettings ):
	"""
	Manipualtors task settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, url_search_pattern_prefix='', number_of_threads=8, name='Manipulator Task' ):
		"""
		Initializes a new instance of the `pydataset.manipualtors.ManipulatorTaskSettings` class.
		
		Arguments:
			self (`pydataset.manipualtors.ManipulatorTaskSettings`): Instance to initialize.
			url_search_pattern_prefix                       (`str`): Prefix of the manipulator url search pattern.
			number_of_threads                               (`int`): Number of threads used to run the manipulator.
			name                                            (`str`): Name of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTaskSettings )
		assert pytools.assertions.type_is( url_search_pattern_prefix, str )
		assert pytools.assertions.type_is( number_of_threads, int )
		assert pytools.assertions.type_is( name, str )

		super( ManipulatorTaskSettings, self ).__init__( name )

		self._url_search_pattern_prefix = url_search_pattern_prefix
		self._number_of_threads         = number_of_threads

	# def __init__ ( self, url_search_pattern_prefix, number_of_threads, name )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def url_search_pattern_prefix ( self ):
		"""
		Prefix of the manipulator url search pattern (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTaskSettings )

		return self._url_search_pattern_prefix
	
	# def url_search_pattern_prefix ( self )

	# --------------------------------------------------

	@url_search_pattern_prefix.setter
	def url_search_pattern_prefix ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTaskSettings )
		assert pytools.assertions.type_is( value, str )

		self._url_search_pattern_prefix = value
	
	# def url_search_pattern_prefix ( self, value )

	# --------------------------------------------------

	@property
	def number_of_threads ( self ):
		"""
		Number of threads used to run the manipulator (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTaskSettings )

		return self._number_of_threads
	
	# def number_of_threads ( self )

	# --------------------------------------------------

	@number_of_threads.setter
	def number_of_threads ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTaskSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_threads = value
	
	# def number_of_threads ( self, value )

# class ManipulatorTaskSettings ( pytools.tasks.TaskSettings )