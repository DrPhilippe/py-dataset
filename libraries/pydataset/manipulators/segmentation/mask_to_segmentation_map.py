# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..instances_mapper                 import InstancesMapper
from .mask_to_segmentation_map_settings import MaskToSegmentationMapSettings

# ##################################################
# ###       CLASS MASK-TO-SEGMENTATION-MAP       ###
# ##################################################

class MaskToSegmentationMap ( InstancesMapper ):
	"""
	Uses binary segmentation mask to compute a segmentation map.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.MaskToSegmentationMap` class.
		
		Arguments:
			self             (`pydataset.manipualtors.segmentation.MaskToSegmentationMap`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.segmentation.MaskToSegmentationMapSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMap )
		assert pytools.assertions.type_is_instance_of( settings, MaskToSegmentationMapSettings )

		super( MaskToSegmentationMap, self ).__init__( settings )
		
	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.MaskToSegmentationMap`): Mapper used to map the example.
			example                       (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMap )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Initialize the segmetation map on the first instance
		# Because we do not know yet the size of the image.
		segmentation = None
		
		# Are masks located in sub-examples ?
		if self.settings.instances_url_search_pattern:
			
			# Go through each obect instance
			for instance in self.create_instances_iterator( example ):

				# Get features
				mask           = instance.get_feature( self.settings.mask_feature_name ).value
				category_index = instance.get_feature( self.settings.category_index_feature_name ).value

				# Initialize the segmetation map ?
				if segmentation is None:
					# Shape
					H = mask.shape[0]
					W = mask.shape[1]
					# Init	
					segmentation = numpy.zeros( [H, W], dtype=numpy.uint8 )

				# if segmentation is None

				# Index of the category
				if self.settings.dedicated_background:
					category_index += 1

				# Pixel where the mask is `True` are assigned with the category index
				segmentation = numpy.where( mask, category_index, segmentation )

			# for instance in pydataset.dataset.Iterator( url )
		
		else:
			# Get features
			mask           = example.get_feature( self.settings.mask_feature_name ).value
			category_index = example.get_feature( self.settings.category_index_feature_name ).value
			
			# Index of the category
			if self.settings.dedicated_background:
				category_index += 1

			# Shape
			H = mask.shape[0]
			W = mask.shape[1]
			
			# Init	
			segmentation = numpy.zeros( [H, W], dtype=numpy.uint8 )

			# Pixel where the mask is `True` are assigned with the category index
			segmentation = numpy.where( mask, category_index, segmentation )

		# if self.settings.instances_url_search_pattern:

		# Create the segmentation feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.segmentation_feature_name,
			            value = segmentation
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipulators.segmentation.MaskToSegmentationMap` class.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.segmentation.MaskToSegmentationMap`.

		Returns:
			`pydataset.manipulators.segmentation.MaskToSegmentationMapSettings`: The settings.
		"""
		return MaskToSegmentationMapSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MaskToSegmentationMap ( InstancesMapper )