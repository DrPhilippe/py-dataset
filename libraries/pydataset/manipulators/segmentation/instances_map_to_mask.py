# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                        import Mapper
from .instances_map_to_mask_settings import InstancesMapToMaskSettings

# ##################################################
# ###        CLASS INSTANCES-MAP-TO-MASK         ###
# ##################################################

class InstancesMapToMask ( Mapper ):
	"""
	Mapper that computes binary segmentation mask from an instances map.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipualtors.segmentation.InstancesMapToMask` class.
		
		Arguments:
			self             (`pydataset.manipualtors.segmentation.InstancesMapToMask`): Instance to initialize.
			settings (`pydataset.manipualtors.segmentation.InstancesMapToMaskSettings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMask )
		assert pytools.assertions.type_is_instance_of( settings, InstancesMapToMaskSettings )

		super( InstancesMapToMask, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Computes the mask of the given example.

		Arguments:
			self (`pydataset.manipualtors.image.InstancesMapToMask`): Mapper used to compute the mask.
			example                    (`pydataset.dataset.Example`): Example of which to compute the mask.

		Returns:
			`pydataset.dataset.Example`: Example with computed mask.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMask )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the instances map
		instances_map = example.get_feature( self.settings.instances_map_feature_name ).value
		
		# If the instance index is provided
		# Compute the mask of that instance only
		if self.settings.instance_index_feature_name:

			# Get the instance index
			instance_index = example.get_feature( self.settings.instance_index_feature_name ).value

			# Compute the mask
			mask = numpy.equal( instances_map, instance_index )

		# Otherwise the mask is just non-null pixels
		else:
			
			# Compute the mask
			mask = numpy.greater( instances_map, 0 )

		# if self.settings.instance_index_feature_name

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.mask_feature_name,
			            value = mask
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipualtors.segmentation.InstancesMapToMask` class.

		Arguments:
			cls (`type`): The type `pydataset.manipualtors.segmentation.InstancesMapToMask`.

		Returns:
			`pydataset.manipulators.segmentation.InstancesMapToMaskSettings`: The settings.
		"""
		return InstancesMapToMaskSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class InstancesMapToMask ( Mapper )