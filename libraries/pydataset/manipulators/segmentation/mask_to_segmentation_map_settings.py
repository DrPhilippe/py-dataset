# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..instances_mapper_settings import InstancesMapperSettings

# ##################################################
# ###   CLASS MASK-TO-SEGMENTATION-MAP-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MaskToSegmentationMapSettings',
	   namespace = 'pydataset.manipulators.segmentation',
	fields_names = [
		'mask_feature_name',
		'category_index_feature_name',
		'number_of_classes',
		'dedicated_background',
		'segmentation_feature_name'
		]
	)
class MaskToSegmentationMapSettings ( InstancesMapperSettings ):
	"""
	Uses binary segmentation mask to compute a segmentation mask.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		     instances_url_search_pattern = 'examples/*',
		                mask_feature_name = 'mask',
		      category_index_feature_name = 'category_index',
		                number_of_classes = 11,
		             dedicated_background = True,
		        segmentation_feature_name = 'segmentation',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.segmentation.MaskToSegmentationMapSettings` class.
		
		Arguments:
			self (`pydataset.manipulators.segmentation.MaskToSegmentationMapSettings`): Instance to initialize.
			instances_url_search_pattern                                       (`str`): URL search pattern used to find instances examples.
			mask_feature_name                                                  (`str`): Name of the feature containing the mask.
			category_index_feature_name                                        (`str`): Name of the feature containing the category index.
			number_of_classes                                                  (`int`): Number of classes, excluding the background.
			dedicated_background                                              (`bool`): If `True` an extra channel is added to the segmentation for the background, at index `0`.
			segmentation_feature_name                                          (`str`): Name of the feature where to save the segmentation.
		
		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( number_of_classes, int )
		assert pytools.assertions.type_is( dedicated_background, bool )
		assert pytools.assertions.type_is( segmentation_feature_name, str )

		super( MaskToSegmentationMapSettings, self ).__init__( instances_url_search_pattern, **kwargs )

		self._mask_feature_name           = mask_feature_name
		self._category_index_feature_name = category_index_feature_name
		self._number_of_classes           = number_of_classes
		self._dedicated_background        = dedicated_background
		self._segmentation_feature_name   = segmentation_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature containing the category index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )

		return self._category_index_feature_name

	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( value, int )

		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_classes ( self ):
		"""
		Number of classes, excluding the background (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )

		return self._number_of_classes

	# def number_of_classes ( self )

	# --------------------------------------------------

	@number_of_classes.setter
	def number_of_classes ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_classes = value
	
	# def number_of_classes ( self, value )

	# --------------------------------------------------

	@property
	def dedicated_background ( self ):
		"""
		If `True` an extra channel is added to the segmentation for the background, at index `0` (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )

		return self._dedicated_background

	# def dedicated_background ( self )

	# --------------------------------------------------

	@dedicated_background.setter
	def dedicated_background ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( value, bool )

		self._dedicated_background = value
	
	# def dedicated_background ( self, value )

	# --------------------------------------------------

	@property
	def segmentation_feature_name ( self ):
		"""
		Name of the feature where to save the segmentation (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )

		return self._segmentation_feature_name

	# def segmentation_feature_name ( self )

	# --------------------------------------------------

	@segmentation_feature_name.setter
	def segmentation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToSegmentationMapSettings )
		assert pytools.assertions.type_is( value, int )

		self._segmentation_feature_name = value
	
	# def segmentation_feature_name ( self, value )

# class MaskToSegmentationMapSettings ( MapperSettings )