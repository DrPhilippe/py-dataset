# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###    CLASS INSTANCES-MAP-TO-MASK-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'InstancesMapToMaskSettings',
	   namespace = 'pydataset.manipulators.segmentation',
	fields_names = [
		'instances_map_feature_name',
		'instance_index_feature_name',
		'mask_feature_name'
		]
	)
class InstancesMapToMaskSettings ( MapperSettings ):
	"""
	Computes the binary segmentation mask from an instances map.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		 instances_map_feature_name = 'instances',
		instance_index_feature_name = 'instance_index',
		          mask_feature_name = 'mask',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.segmentation.InstancesMapToMaskSettings` class.
		
		Arguments:
			self (`pydataset.manipulators.segmentation.InstancesMapToMaskSettings`): Instance to initialize.
			instances_map_feature_name                                      (`str`): Name of the feature containing the instances map.
			instance_index_feature_name                                     (`str`): Name of the feature containing the instances index.
			mask_feature_name                                               (`str`): Name of the feature where to save the mask.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )
		assert pytools.assertions.type_is( instances_map_feature_name, str )
		assert pytools.assertions.type_is( instance_index_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )

		super( InstancesMapToMaskSettings, self ).__init__( **kwargs )

		self._instances_map_feature_name  = instances_map_feature_name
		self._instance_index_feature_name = instance_index_feature_name
		self._mask_feature_name           = mask_feature_name

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def instances_map_feature_name ( self ):
		"""
		Name of the feature containing the instances map (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )

		return self._instances_map_feature_name

	# def instances_map_feature_name ( self )

	# --------------------------------------------------

	@instances_map_feature_name.setter
	def instances_map_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )
		assert pytools.assertions.type_is( value, str )

		self._instances_map_feature_name = value
	
	# def instances_map_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def instance_index_feature_name ( self ):
		"""
		Name of the feature containing the instances index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )

		return self._instance_index_feature_name

	# def instance_index_feature_name ( self )

	# --------------------------------------------------

	@instance_index_feature_name.setter
	def instance_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )
		assert pytools.assertions.type_is( value, str )

		self._instance_index_feature_name = value
	
	# def instance_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature where to save the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InstancesMapToMaskSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

# class InstancesMapToMaskSettings ( MapperSettings )