# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .manipulator_settings import ManipulatorSettings

# ##################################################
# ###          CLASS AGREGATOR-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'AgregatorSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = []
	)
class AgregatorSettings ( ManipulatorSettings ):
	"""
	Agregator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the agregator settings class.
		
		Arguments:
			self (`pydataset.manipualtors.AgregatorSettings`): Instance to initialize.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to agregate.
			dst_group_url      (`str`): URL of the group where to save the agregated feature(s).
			name               (`str`): Name of the agregator.
		"""
		assert pytools.assertions.type_is_instance_of( self, AgregatorSettings )

		super( AgregatorSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

# class AgregatorSettings ( ManipulatorSettings )