# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data import ShapeData
from .agregator_settings import AgregatorSettings

# ##################################################
# ###      CLASS NDARRAY-AGREGATOR-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'NDArrayAgregatorSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = [
		'category_name_feature_name',
		'ndarray_feature_name'
		]
	)
class NDArrayAgregatorSettings ( AgregatorSettings ):
	"""
	NDArray agregator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, category_name_feature_name='', ndarray_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the ndarray agregator settings class.
		
		Arguments:
			self (`pydataset.manipualtors.NDArrayAgregatorSettings`): Instance to initialize.
			category_name_feature_name                       (`str`): Name of the feature containing the category name.
			ndarray_feature_name                             (`str`): Name of the feature containing the ndarrays to agregate.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to agregate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregatorSettings )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( ndarray_feature_name, str )

		# Init parent class
		super( NDArrayAgregatorSettings, self ).__init__( **kwargs )

		# Init properties
		self._category_name_feature_name = category_name_feature_name
		self._ndarray_feature_name       = ndarray_feature_name

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregatorSettings )

		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		"""
		Name of the feature containing the ndarrays to agregate (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def ndarray_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregatorSettings )

		return self._ndarray_feature_name
	
	# def ndarray_feature_name ( self )

	# --------------------------------------------------

	@ndarray_feature_name.setter
	def ndarray_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._ndarray_feature_name = value
	
	# def ndarray_feature_name ( self, value )

# class NDArrayAgregatorSettings ( AgregatorSettings )