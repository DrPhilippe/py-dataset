# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .manipulator     import Manipulator
from .mapper_settings import MapperSettings

# ##################################################
# ###                CLASS MAPPER                ###
# ##################################################

class Mapper ( Manipulator ):
	"""
	Base class for example mappers.

	A mappers modifies an existing example.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.Mapper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.MapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is_instance_of( settings, MapperSettings )

		super( Mapper, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this manipulator.

		Arguments:
			self (`pydataset.manipualtors.Mapper`): Mapper used to manipulate the example.
			example  (`pydataset.dataset.Example`): Example to manipulate.
			index                          (`int`): Index of the example being mapped.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Is the dst group different of the src group
		if self._dst_group != self._src_group:

			# Lock dst group
			self._dst_group_lock.acquire()

			# Get/Create example parent group
			parent_group = self._get_or_create_example_parent_in_dst_group( example )

			# Create a new exampe
			example_copy = parent_group.create_example( example.name )

			# Release dst group
			self._dst_group_lock.release()

			# Copy the src example
			example_copy.copy( example )

			# Work on the copy
			example = example_copy

		# if self._dst_group != self._src_group

		# Map the example
		example = self.map( example )

		# Check that the method map returned an example
		if ( example is None ):
			raise RuntimeError( 'The method Mapper.map() returned None. Expected an Example.' )

		# Update the example
		example.update()

		return example

	# def manipulate ( self, example, index )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.Mapper`): Mapper used to map the example.
			example  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		raise NotImplementedError()
	
	# def map ( self, example )
	
# class Mapper ( Manipulator )