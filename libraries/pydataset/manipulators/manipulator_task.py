# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNAL
import time

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.tasks

# LOCALS
from .manipulator               import Manipulator
from .manipulator_batch_task    import ManipulatorBatchTask
from .manipulator_task_settings import ManipulatorTaskSettings

# ##################################################
# ###           CLASS MANIPULATOR-TASK           ###
# ##################################################

class ManipulatorTask ( pytools.tasks.Task ):
	"""
	Task used to run a manipulator on a batch of examples.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, manipulator, settings, logger=None, progress_tracker=None, parent=None ):
		"""
		Initializes a new instance of the `pydataset.manipulators.ManipulatorTask` class.

		Arguments:
			self             (`pydataset.manipulators.ManipulatorTask`): Instance to initialize.
			manipulator          (`pydataset.manipulators.Manipulator`): Manipulator to apply.
			settings (`pydataset.manipulators.ManipulatorTaskSettings`): Settings of the manipulator task.
			logger                      (`None`/`pytools.tasks.Logger`): Logger used to log the task activity.
			progress_tracker   (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			parent                        (`None`/`pytools.tasks.Task`): Parent task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )
		assert pytools.assertions.type_is_instance_of( manipulator, Manipulator )
		assert pytools.assertions.type_is_instance_of( settings, ManipulatorTaskSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), pytools.tasks.Task) )

		super( ManipulatorTask, self ).__init__( settings, logger, progress_tracker, parent )

		self._manipulator   = manipulator
		self._iterator      = None
		self._task          = None
		self._task_manager  = None
		self._begin_tracker = None
		self._end_tracker   = None

	# def __init__ ( self, manipulator, settings, logger, progress_tracker, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def manipulator ( self ):
		"""
		Manipulator to apply to the batch (`pydataset.manipulators.Manipulator`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )
	
		return self._manipulator
	
	# def manipulator ( self )
	
	# --------------------------------------------------

	@manipulator.setter
	def manipulator ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )
		assert pytools.assertions.type_is_instance_of( value, Manipulator )

		self._manipulator = value
	
	# def manipulator ( self, value )

	# --------------------------------------------------

	@property
	def iterator ( self ):
		"""
		Iterator used to get the example(s) (`pydataset.dataset.Iterator`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )

		return self._iterator
	
	# def iterator ( self )

	# --------------------------------------------------

	@property
	def task ( self ):
		"""
		Manipulator task (`pydataset.manipulator.ManipulatorTask`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )

		return self._task
	
	# def task ( self )

	# --------------------------------------------------

	@property
	def task_manager ( self ):
		"""
		Task manager used to manage manipulator task (`pytools.tasks.Manager`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )

		return self._task_manager
	
	# def task_manager ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this manipulator task.

		Arguments:
			self (`pydataset.manipulators.ManipulatorTask`): The task to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )

		super( ManipulatorTask, self ).start()

		# Compose the url
		if ( self.settings.url_search_pattern_prefix ):
			url_search_pattern = self.settings.url_search_pattern_prefix + '/' + self.manipulator.settings.url_search_pattern
		else:
			url_search_pattern = self.manipulator.settings.url_search_pattern
		
		# Log
		if self.logger:
			self.logger.log( 'url_search_pattern: {}', url_search_pattern )

		# Create the iterator to get the example(s)
		print( '    url_search_pattern: {}'.format(url_search_pattern) )
		self._iterator     = pydataset.dataset.Iterator( url_search_pattern )
		number_of_examples = len( self._iterator )
		print( '    found {} items'.format(number_of_examples) )

		# Log
		if self.logger:
			self.logger.log( 'number_of_examples: {}', number_of_examples )

		# Check the number of examples
		if number_of_examples == 0:
			return
		
		# In the case of few examples
		# only use the appropriate number of threads
		if number_of_examples < self.settings.number_of_threads:
			self.settings.number_of_threads = number_of_examples

		# Log
		if self.logger:
			self.logger.log( 'number_of_threads: {}', self.settings.number_of_threads )

		# Copute the number of example(s) manipulated in each thread
		# And the extra amount of example(s)
		number_of_examples_per_thread = number_of_examples // self.settings.number_of_threads
		number_of_examples_remaining  = number_of_examples - (number_of_examples_per_thread * self.settings.number_of_threads)

		# Log
		if self.logger:
			self.logger.log( 'number_of_examples_per_thread: {}', number_of_examples_per_thread )
			self.logger.log( 'number_of_examples_remaining: {}', number_of_examples_remaining )

		# Create progress tracker for the begin method of the manipulator
		if self.progress_tracker:
			self._begin_tracker = self.progress_tracker.create_child()

		# Create tasks for each batch of examples
		tasks = []
		number_of_examples_assigned = 0
		for i in range( self.settings.number_of_threads ):

			# Range of examples the task will manipulate
			start_index = number_of_examples_assigned
			end_index   = number_of_examples_assigned + number_of_examples_per_thread

			# Assign one extra example to it ?
			if ( number_of_examples_remaining > 0 ):
				end_index += 1
				number_of_examples_remaining -= 1

			# Create a progress tracker
			if self.progress_tracker:
				sub_tracker = self.progress_tracker.create_child()
			else:
				sub_tracker = None

			# Create the task
			task = ManipulatorBatchTask.create(
				          logger = pytools.tasks.file_logger( 'logs/{}_batch_{}.log'.format(self.manipulator.settings.name, i) ),
				progress_tracker = sub_tracker,
				          parent = self,
				     start_index = start_index,
				       end_index = end_index
				)
			
			# Save the task
			tasks.append( task )

			# Remember the number of examples already assigned
			number_of_examples_assigned = end_index

		# for i in range( self.settings.number_of_threads )

		# Create a manager for the tasks and run them
		self._task_manager = pytools.tasks.Manager( *tasks )

		# Create progress tracker for the end method of the manipulator
		if self.progress_tracker:
			self._end_tracker = self.progress_tracker.create_child()

	# def start ( self )
	
	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this manipulator task.

		Arguments:
			self (`pydataset.manipulators.ManipulatorTask`): The manipulator task to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorTask )
		
		# Check if the task failed or was stopped
		if not self.is_running():
			return

		# Update the task status
		self.update()

		# This appends if they are 0 examples
		if self._task_manager is None:
			return

		# Begin manipulation
		if self._begin_tracker:
			self._begin_tracker.update( 0.0 )
		self.manipulator.begin( self._iterator.root_group, self._iterator.count )
		if self._begin_tracker:
			self._begin_tracker.update( 1.0 )

		# Start the manipulation of each batch
		self._task_manager.start_tasks()

		# Main loop
		wait_for_stop = True
		while ( wait_for_stop ):			
			wait_for_stop = False

			# Check if the task failed or was stopped
			if not self.is_running():
				break

			# Update the task status
			self.update()

			# Check if all tasks finished
			for task in self.task_manager.tasks:

				# If one did not finish
				if not task.stopped():

					# Continue waiting
					wait_for_stop = True

				# Check if a task failed
				if task.is_failed():
					raise task.error
			
			# for task in self.task_manager.tasks

			# Sleep
			time.sleep( 0.1 )

		# while ( wait_for_stop )

		self._task_manager.join_tasks()

		# Check for error again
		for task in self._task_manager.tasks:
			if task.is_failed():
				raise task.error

		# End manipulation
		if self._end_tracker:
			self._end_tracker.update( 0.0 )
		self.manipulator.end( self.iterator.root_group, self.iterator.count )
		if self._end_tracker:
			self._end_tracker.update( 1.0 )

	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, manipulator, logger=None, progress_tracker=None, parent=None, **kwargs ):
		"""
		Creates a new instance of a manipulator task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of manipulator task.
			manipulator        (`pydataset.manipulators.Manipulator`): Manipulator to apply using this task.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the manipulator task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the manipulator task.
			parent                      (`None`/`pytools.tasks.Task`): Parent task.
			**kwargs                                         (`dict`): Arguments forwarded to the manipulator task settings constructor.

		Returns:
			`pydataset.manipulators.ManipulatorTask`: The new manipulator task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( manipulator, Manipulator )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( manipulator, settings, logger, progress_tracker, parent )

	# def create ( cls, manipulator, logger, progress_tracker, parent, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a manipulator task of type `cls`.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the manipulator task settings constructor.

		Returns:
			`pydataset.manipulators.ManipulatorTaskSettings`: The settings.
		"""
		return ManipulatorTaskSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ManipulatorTask ( pytools.tasks.Task )