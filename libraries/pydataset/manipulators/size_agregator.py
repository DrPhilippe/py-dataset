# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import pandas
import threading

# INTERNALS
import pytools
import pydataset

# LOCALS
from .agregator               import Agregator
from .size_agregator_settings import SizeAgregatorSettings

# ##################################################
# ###            CLASS SIZE-AGREGATOR            ###
# ##################################################

class SizeAgregator ( Agregator ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregator )
		assert pytools.assertions.type_is_instance_of( settings, SizeAgregatorSettings )

		super( SizeAgregator, self ).__init__( settings )

		self._lock  = threading.RLock()
		self._sizes = {}
		self._locks = {}

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def sizes ( self ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregator )

		return self._sizes
	
	# def sizes ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get features used to compute and agregate the point of view
		category           = str(example.get_feature( self.settings.category_feature_name ).value)
		bounding_rectangle = example.get_feature( self.settings.bounding_rectangle_feature_name ).value.astype(numpy.int32)
		
		width  = bounding_rectangle[1, 0] - bounding_rectangle[0, 0]
		height = bounding_rectangle[1, 1] - bounding_rectangle[0, 1]
		size   = numpy.asarray( [width, height], dtype=numpy.int32 )

		# Agregate the point of view
		if category not in self._sizes:
			self._lock.acquire()
			self._sizes[ category ] = size
			self._locks[ category ] = threading.RLock()
			self._lock.release()
		else:
			self._locks[ category ].acquire()
			self._sizes[ category ] = numpy.vstack( (self._sizes[ category ], size) )
			self._locks[ category ].release()

	# def agregate ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self._lock  = threading.RLock()
		self._sizes = {}
		self._locks = {}

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		# Create output directory if necessary
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()

		# Concatenate everything in category all
		self._sizes[ 'all' ] = numpy.concatenate( list(self._sizes.values()), axis=0 )

		# Compute distances, azymuths and elevations
		for category_name in sorted(self._sizes.keys()):
			
			# fetch category data
			sizes = self._sizes[ category_name ]
			N     = sizes.shape[ 0 ]

			# width stats
			min_width   = sizes[:, 0].min()
			max_width   = sizes[:, 0].max()
			mean_width  = sizes[:, 0].mean()
			std_width   = sizes[:, 0].std()
			# height stats
			min_height  = sizes[:, 1].min()
			max_height  = sizes[:, 1].max()
			mean_height = sizes[:, 1].mean()
			std_height  = sizes[:, 1].std()

			# Areas
			areas = numpy.multiply( sizes[:, 0], sizes[:, 1] )
			areas = numpy.reshape( areas, [-1, 1] )
			# stats
			min_areas  = areas[:, 0].min()
			max_areas  = areas[:, 0].max()
			mean_areas = areas[:, 0].mean()
			std_areas  = areas[:, 0].std()
			
			# add to array at bottom
			sizes = numpy.vstack((
				sizes,
				numpy.asarray([
					[min_width,  min_height ],
					[max_width,  max_height ],
					[mean_width, mean_height],
					[std_width,  std_height ]
					])
				))
			areas = numpy.vstack((
				areas,
				numpy.asarray([
					[min_areas],
					[max_areas],
					[mean_areas],
					[std_areas]
					])
				))
			
			# Create the path of the file where to save everything
			filepath = self.settings.output_directory_path + pytools.path.FilePath.format( self.settings.filename_format, category_name )

			df = pandas.DataFrame({
				'width':  sizes[:, 0],
				'height': sizes[:, 1],
				'areas':  areas[:, 0]
				},
				index = ['{:010d}'.format(i) for i in range(N)] + ['min', 'max', 'mean', 'std']
				)
			df.to_csv( str(filepath),
				         sep = ',',
				      na_rep = 'nan',
				float_format = '%06.0f',
				index_label = 'index'
				)

		# for category_name in sorted(self._sizes.keys()):

	# def end ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return SizeAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SizeAgregator ( Agregator )