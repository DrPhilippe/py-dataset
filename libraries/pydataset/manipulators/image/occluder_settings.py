# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###          CLASS OCCLUDER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OccluderSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'images_features_names',
		'images_search_url',
		'rectangle_or_mask_feature_name',
		'category_feature_name',
		'min_occlusion_threshold'
		'max_occlusion_threshold'
		]
	)
class OccluderSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self,
		images_features_names=[],
		images_search_url='',
		rectangle_or_mask_feature_name='',
		category_feature_name='',
		min_occlusion_threshold=0.05,
		max_occlusion_threshold=0.4,
		**kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulatos.image.OccluderSettings` class.

		Arguments:
			images_features_names (`list` of `str`): Names of the features containing the images to occlude.
			images_search_url               (`str`): URL used to search for images used to occlude the objet.
			rectangle_or_mask_feature_name  (`str`): Names of the feature containing the rectangle surrounding the foreground or the segmentation mask of the foreground pixels to occlude.
			category_feature_name           (`str`): Name of the feature containing the category of the object to occlude.
			min_occlusion_threshold       (`float`): Minimum occlusion allowed on the foreground [0., 1.].
			max_occlusion_threshold       (`float`): Maximum occlusion allowed on the foreground [0., 1.].

		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( images_search_url, str )
		assert pytools.assertions.type_is( rectangle_or_mask_feature_name, str )
		assert pytools.assertions.type_is( category_feature_name, str )
		assert pytools.assertions.type_is( min_occlusion_threshold, float )
		assert pytools.assertions.type_is( max_occlusion_threshold, float )

		super( OccluderSettings, self ).__init__( **kwargs )

		self._images_features_names          = copy.deepcopy( images_features_names )
		self._images_search_url              = images_search_url
		self._rectangle_or_mask_feature_name = rectangle_or_mask_feature_name
		self._category_feature_name          = category_feature_name
		self._min_occlusion_threshold        = min_occlusion_threshold
		self._max_occlusion_threshold        = max_occlusion_threshold

	# def __init__ ( self, images_features_names=[], **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def images_features_names ( self ):
		"""
		Names of the features containing the images to occlude (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._images_features_names
	
	# def images_features_names ( self )

	# --------------------------------------------------

	@images_features_names.setter
	def images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._images_features_names = copy.deepcopy( value )
	
	# def images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def images_search_url ( self ):
		"""
		URL used to search for images used to occlude the objet (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._images_search_url
	
	# def images_search_url ( self )

	# --------------------------------------------------

	@images_search_url.setter
	def images_search_url ( self, value ):
		"""
		URL used to search for images used to occlude the objet (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, str )

		self._images_search_url = value
	
	# def images_search_url ( self, value )

	# --------------------------------------------------

	@property
	def rectangle_or_mask_feature_name ( self ):
		"""
		Names of the feature containing the rectangle surrounding the foreground or the segmentation mask of the foreground pixels to occlude (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._rectangle_or_mask_feature_name
	
	# def rectangle_or_mask_feature_name ( self )

	# --------------------------------------------------

	@rectangle_or_mask_feature_name.setter
	def rectangle_or_mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, str )

		self._rectangle_or_mask_feature_name = value
	
	# def rectangle_or_mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_feature_name ( self ):
		"""
		Name of the feature containing the category of the object to occlude (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._category_feature_name
	
	# def category_feature_name ( self )

	# --------------------------------------------------

	@category_feature_name.setter
	def category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_feature_name = value
	
	# def category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def min_occlusion_threshold ( self ):
		"""
		Minimum occlusion allowed on the foreground [0., 1.] (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._min_occlusion_threshold
	
	# def min_occlusion_threshold ( self )

	# --------------------------------------------------

	@min_occlusion_threshold.setter
	def min_occlusion_threshold ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, float )

		self._min_occlusion_threshold = value
	
	# def min_occlusion_threshold ( self, value )

	# --------------------------------------------------

	@property
	def max_occlusion_threshold ( self ):
		"""
		Maximum occlusion allowed on the foreground [0., 1.] (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )

		return self._max_occlusion_threshold
	
	# def max_occlusion_threshold ( self )

	# --------------------------------------------------

	@max_occlusion_threshold.setter
	def max_occlusion_threshold ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OccluderSettings )
		assert pytools.assertions.type_is( value, float )

		self._max_occlusion_threshold = value
	
	# def max_occlusion_threshold ( self, value )

# class OccluderSettings ( MapperSettings )