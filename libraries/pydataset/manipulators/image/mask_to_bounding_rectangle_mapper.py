# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                                    import Mapper
from .mask_to_bounding_rectangle_mapper_settings import MaskToBoundingRectangleMapperSettings

# ##################################################
# ###  CLASS MASK-TO-BOUNDING-RECTANGLE-MAPPER   ###
# ##################################################

class MaskToBoundingRectangleMapper ( Mapper ):
	"""
	Uses binary segmentation mask to compute a bounding-box.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.MaskToBoundingRectangleMapper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.MaskToBoundingRectangleMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapper )
		assert pytools.assertions.type_is_instance_of( settings, MaskToBoundingRectangleMapperSettings )

		super( MaskToBoundingRectangleMapper, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.MaskToBoundingRectangleMapper`): Mapper used to map the example.
			example                         (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get feature
		mask_feature = example.get_feature( self.settings.mask_feature_name )
			
		# Check feature
		if not isinstance( mask_feature.data, pydataset.dataset.NDArrayFeatureData ):
			raise RuntimeError(
				'Unsupported feature data type: type={}, expected=NDArrayFeatureData'.format( type(mask_feature.data) )
				)

		# Fetch mask
		mask = mask_feature.value

		# Scan for any non-zero pixels on each axis
		cols = numpy.any( mask, axis=0 )
		rows = numpy.any( mask, axis=1 )

		# Find the indexes of non-zero pixels on each axis
		idx_cols = numpy.where( cols )
		idx_rows = numpy.where( rows )

		# Check if the mask was not all black (object fully ocluded)
		if ( idx_cols[0].shape[0] >= 2 and idx_rows[0].shape[0] >= 2  ):

			# Read the index of the first and last
			# to get the englobing bounding box
			x1, x2 = idx_cols[ 0 ][ [0, -1] ]
			y1, y2 = idx_rows[ 0 ][ [0, -1] ]

			# Create the rect
			bounding_box = pydataset.data.RectangleData( int(x1), int(y1), int(x2), int(y2) )
		
		else:
			# Create an empty rect
			bounding_box = pydataset.data.RectangleData()

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bounding_rectangle_feature_name,
			            value = bounding_box.to_ndarray()
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a mask to bounding-box mapper class.

		Arguments:
			cls (`type`): Type of mask to bounding-box mapper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.MaskToBoundingRectangleMapperSettings`: The settings.
		"""
		return MaskToBoundingRectangleMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MaskToBoundingRectangleMapper ( Mapper )