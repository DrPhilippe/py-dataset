# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..augmenter              import Augmenter
from .blur_augmenter_settings import BlurAugmenterSettings

# ##################################################
# ###            CLASS BLUR-AUGMENTER            ###
# ##################################################

class BlurAugmenter ( Augmenter ):
	"""
	Augments examples by blurring images with various kernels.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.BlurAugmenter` class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.BlurAugmenter`): Augmenter instance to initialize.
			settings (`pydataset.manipualtors.image.BlurAugmenterSettings`): Settings of the augmenter.
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenter )
		assert pytools.assertions.type_is_instance_of( settings, BlurAugmenterSettings )

		super( BlurAugmenter, self ).__init__( settings )
		
	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def augment ( self, example ):
		"""
		Augment the given example using this augmenter.

		Arguments:
			self (`pydataset.manipualtors.image.BlurAugmenter`): Augmenter used to augment the example.
			example               (`pydataset.dataset.Example`): Example to augment.
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenter )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		augmentations      = self.create_augmentations( example, self._number_of_augmentations_per_example )
		augmentation_index = 0

		# Create an augmentation for each kernel size
		for kernel_size in self._kernel_size_range:

			# Create an augmentation for each kernel sigma
			for kernel_sigma in self._kernel_sigma_range:

				# Create the augmentation
				augmentation = augmentations[ augmentation_index ]

				# Get image
				image_feature = augmentation.get_feature( self.settings.image_feature_name )
				image         = image_feature.value
					
				# Blur image
				image = cv2.GaussianBlur( image, (kernel_size, kernel_size), kernel_sigma )

				# Save image
				image_feature.value = image
				image_feature.update()

				# next
				augmentation_index += 1

			# for kernel_sigma in numpy.arange( min_s, max_s, step_s, dtype=float )
	
		# for kernel_size in range( min_k, max_k, step_k )

	# def augment ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before augmentation starts.

		Arguments:
			self (`pydataset.manipualtors.BlurAugmenter`): Augmenter used to augment the example.
			root_group        (`pydataset.dataset.Group`): Group containing the examples that will be augmented.
			number_of_examples                    (`int`): Number of examples to augment.
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenter )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( BlurAugmenter, self ).begin( root_group, number_of_examples )

		# Get blur ranges
		min_k  = self.settings.kernel_size_range[ 0 ]
		max_k  = self.settings.kernel_size_range[ 1 ]
		step_k = self.settings.kernel_size_range[ 2 ]
		min_s  = self.settings.kernel_sigma_range[ 0 ]
		max_s  = self.settings.kernel_sigma_range[ 1 ]
		step_s = self.settings.kernel_sigma_range[ 2 ]

		# kernel size range
		self._kernel_size_range  = range( min_k, max_k, step_k )
		
		# kernel sigma range
		self._kernel_sigma_range = numpy.arange( min_s, max_s, step_s, dtype=float )

		# Number of augmentations
		self._number_of_augmentations_per_example = len(self._kernel_size_range) * len(self._kernel_sigma_range)

	# def begin ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipulators.image.BlurAugmenter`.

		Named Arguments:
			image_feature_name                   (`str`): Name of the feature containing the image to blur.
			kernel_size_range    (`tuple` of `3` `int`s): Kernel size range defined as min, max, step.
			kernel_sigma_range (`tuple` of `3` `float`s): Kernel sigma range defined as min, max, step.
			dst_group_url                        (`str`): URL of the group where to save the augmentations.
			url_search_pattern                   (`str`): URL used to search for example(s) to manipulate.
			name                                 (`str`): Name of the manipulator.

		Returns:
			`pydataset.manipulators.image.BlurAugmenterSettings`: Settings for a `pydataset.manipulators.image.BlurAugmenter`.
		"""
		
		return BlurAugmenterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class BlurAugmenter ( Augmenter )