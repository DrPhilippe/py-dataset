# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..augmenter                 import Augmenter
from .instances_cropper_settings import InstancesCropperSettings

# ##################################################
# ###          CLASS INSTANCES-CROPPER           ###
# ##################################################

class InstancesCropper ( Augmenter ):
	"""
	This augmenter creates a new example for each object instance by cropping the images around it.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.InstancesCropper` class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.InstancesCropper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.InstancesCropperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesCropper )
		assert pytools.assertions.type_is_instance_of( settings, InstancesCropperSettings )

		super( InstancesCropper, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def augment ( self, example, example_index ):
		"""
		Crop the given example's instances.

		Arguments:
			self (`pydataset.manipualtors.image.InstancesCropper`): Mapper used to map the example.
			example                  (`pydataset.dataset.Example`): Example to crop.

		Returns:
			`pydataset.dataset.Example`: Example onced cropped.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesCropper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Create instance iterator
		instances_iterator_url = example.absolute_url + '/' + self.settings.instances_url_search_pattern
		instances_iterator     = pydataset.dataset.Iterator( instances_iterator_url )
		number_of_instances    = instances_iterator.count
		
		# Results
		augmentations = []

		# Go through each instance and create an augmentation for it
		for instance_index in range( number_of_instances ):

			# Get the instance
			instance = instances_iterator.at( instance_index )
			
			# Get the bounding rectangle of this instance
			instance_rectangle = instance.get_feature( self.settings.bounding_rectangle_feature_name ).value
			instance_rectangle = pydataset.data.RectangleData.from_ndarray( instance_rectangle )
			
			# Copy the crop rectangle and center it on the instance bounding rectangle
			crop_rectangle = copy.deepcopy( self.settings.crop_rectangle )
			crop_rectangle.center_on( *instance_rectangle.center )

			# Ref image rect to check images have the same size
			# and if the crop rectangle fits inside the image
			ref_image_rectangle = None
			ref_image_name      = ''

			# Crop each images
			number_of_images_to_crop = len( self.settings.images_features_names )
			for index in range( number_of_images_to_crop ):
				
				# Get image feature
				image_feature_name = self.settings.images_features_names[ index ]
				image_feature   = example.get_feature( image_feature_name )
				image           = image_feature.value
				image_rectangle = pydataset.data.RectangleData.from_size( 0, 0, image.shape[1], image.shape[0] )

				# Is the reference image rectangle defined ?
				# I.E. Is this the first image beeing cropped ?
				if not ref_image_rectangle:

					# Define it
					ref_image_rectangle = image_rectangle
					ref_image_name      = image_feature_name

					# Check the crop rectangle fits
					if not crop_rectangle.fits_in( image_rectangle ):
						raise RuntimeError( 'The crop rectangle {} does not fit in the image {}'.format(crop_rectangle, image_rectangle) )

					# Make sure the crop rectangle is in the image
					# This is done once to make sure all crop are taken at the same location on all images
					if crop_rectangle.x1 < 0:
						crop_rectangle.shift( -crop_rectangle.x1, 0 )
					elif crop_rectangle.x2 > image_rectangle.width:
						crop_rectangle.shift( image_rectangle.width - crop_rectangle.x2, 0 )
					if crop_rectangle.y1 < 0:
						crop_rectangle.shift( 0, -crop_rectangle.y1 )
					elif crop_rectangle.y2 > image_rectangle.height:
						crop_rectangle.shift( 0, image_rectangle.height - crop_rectangle.y2 )

				# If the reference image rectangle is defined
				else:
					# Check the images are the same size
					if ref_image_rectangle != image_rectangle:
						raise RuntimeError( "Cannot crop because image '{}' does not have the same size has image '{}': {} != {}".format(
							image_feature_name,
							ref_image_name,
							ref_image_rectangle,
							image_rectangle
							))
				
				# if not ref_image_rectangle
			
			# for index in range( number_of_images_to_crop )

			# Crop rectangle is now set, and all image have been checked
		
			# Compute the crop vector and matrix
			crop_vector         = numpy.asarray( [-crop_rectangle.x1, -crop_rectangle.y1], dtype=numpy.float32 )
			crop_matrix         = numpy.eye( 3, dtype=numpy.float32 )
			crop_matrix[ 0, 2 ] = float( -crop_rectangle.x1 )
			crop_matrix[ 1, 2 ] = float( -crop_rectangle.y1 )

			# Create an augmentation for this object instance
			instance_augmentation = self.create_augmentation( example, 'same', '{:05d}-{:02d}', ['all'], instance_index )
			augmentations.append( instance_augmentation )
			
			# Crop its images
			for image_index in range( number_of_images_to_crop ):
				
				# Get image name
				image_feature_name = self.settings.images_features_names[ image_index ]
				
				# Get image feature
				image_feature = instance_augmentation.get_feature( image_feature_name )
				image         = image_feature.value
				
				# Crop image
				crop = pydataset.images_utils.crop_image( image, crop_rectangle )

				# Update image
				image_feature.value = crop
				image_feature.update()

			# for image_index in range( number_of_images_to_crop )

			# New we go through the instances again but within the augmentation
			sub_iterator_url = instance_augmentation.absolute_url + '/' + self.settings.instances_url_search_pattern
			sub_iterator     = pydataset.dataset.Iterator( sub_iterator_url )
			for sub_instance_index in range( number_of_instances ):

				# Get the example for that augmented instance
				sub_instance = sub_iterator.at( sub_instance_index )

				# Crop its mask
				mask_feature       = sub_instance.get_feature( self.settings.mask_feature_name )
				mask               = mask_feature.value
				mask               = pydataset.images_utils.crop_image( mask, crop_rectangle )
				mask_feature.value = mask
				mask_feature.update()

				# Shift the bounding rectangle
				bounding_rectangle_feature       = sub_instance.get_feature( self.settings.bounding_rectangle_feature_name )
				bounding_rectangle               = bounding_rectangle_feature.value
				bounding_rectangle               = numpy.add( bounding_rectangle, crop_vector )
				bounding_rectangle_feature.value = bounding_rectangle.astype( numpy.int32 )
				bounding_rectangle_feature.update()	
				
				# Modify affected features names
				for affected_feature_name in self.settings.affected_features_names:

					# Get the affected feature
					affected_feature = sub_instance.get_feature( affected_feature_name )
					affected_value   = affected_feature.value.copy()

					# Is the feature a 3x3 transforma matrix ?
					if affected_value.shape == (3, 3):

						# Apply translation after the affect transform
						affected_value = numpy.dot( crop_matrix, affected_value )

					# Otherwise it is considered to be a set of 2D points
					else:					
						# Translate the array, if this does not work a ValueError is raised by numpy
						affected_value = numpy.add( affected_value, crop_vector )

					# Is the feature defined at the instance level ?
					if sub_instance.contains_feature( affected_feature_name, inherited=False ):
						
						# Then simply update the existing feature
						affected_feature.value = affected_value
						affected_feature.update()

					else:
						# Otherwise, create a copy of the feature at the image level
						sub_instance.create_feature(
							feature_data_type = type(affected_feature.data),
							     feature_name = affected_feature_name,
							      auto_update = True,
							            value = affected_value
							)

				# # for affected_feature_name in self.settings.affected_features_names

				# for augmentation_index in range( iterator_count )
			
			# for sub_instance_index in range( number_of_instances ):

		# for instance_index in range( number_of_instances )

		return augmentations

	# def map ( augment, example, index )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an instances cropper type.

		Arguments:
			cls (`type`): Type of instances cropper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.InstancesCropperSettings`: The settings.
		"""
		return InstancesCropperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class InstancesCropper ( Augmenter )