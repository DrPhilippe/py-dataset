# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .image_mean_agregator_settings import ImageMeanAgregatorSettings
from ..agregator                    import Agregator

# ##################################################
# ###              CLASS AGREGATOR               ###
# ##################################################

class ImageMeanAgregator ( Agregator ):
	"""
	Image mean agregator.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the image mean agregator class.

		Arguments:
			self             (`pydataset.manipulators.image.ImageMeanAgregator`): Instance to initialize.
			settings (`pydataset.manipulators.image.ImageMeanAgregatorSettings`): Agregator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregator )
		assert pytools.assertions.type_is_instance_of( settings, ImageMeanAgregatorSettings )

		super( ImageMeanAgregator, self ).__init__( settings )

		self.weight = 0.0
		self.image_mean = None

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipulators.image.ImageMeanAgregator`): Agregator used to agregate the example.
			example                    (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		image = example.get_feature( self.settings.image_feature_name ).value

		if self.image_mean is None:
			self.image_mean = numpy.zeros( image.shape, dtype=numpy.float64 )
		
		self.image_mean = self.image_mean + self.weight * image.astype( numpy.float64 )

	# def agregate ( self, example )

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.image.ImageMeanAgregator`): Image mean agregator used to agregate the images mean.
			root_group                   (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                               (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( ImageMeanAgregator, self ).begin( root_group, number_of_examples )

		self.weight = 1.0 / float(number_of_examples)

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after agregation append.

		Arguments:
			self (`pydataset.manipulators.image.ImageMeanAgregator`): Image mean agregator used to agregate the images mean.
			root_group                   (`pydataset.dataset.Group`): Group containing the examples that have been agregated.
			number_of_examples                               (`int`): Number of examples agregated.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self.dst_group.create_or_update_feature(
			     feature_name = self.settings.image_mean_feature_name,
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            shape = pydataset.data.ShapeData( *self.image_mean.shape ), 
			            dtype = 'float64',
			            value = self.image_mean
			)

	# def end ( self, root_group, number_of_examples )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipulators.image.BlurAugmenter`.

		Named Arguments:
			image_feature_name      (`str`): Name of the feature containing the images to agregate.
			image_mean_feature_name (`str`): Name of the feature used to save the image mean.
			url_search_pattern      (`str`): URL used to search for example(s) to agregate.
			dst_group_url           (`str`): URL of the group where to save the agregated mean image feature.
			name                    (`str`): Name of the image mean agregator.

		Returns:
			`pydataset.manipulators.image.BlurAugmenterSettings`: Settings for a `pydataset.manipulators.image.BlurAugmenter`.
		"""
		
		return ImageMeanAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImageMeanAgregator ( Agregator )