# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from ..mapper                    import Mapper
from .full_mask_creator_settings import FullMaskCreatorSettings

# ##################################################
# ###          CLASS FULL-MASK-CREATOR           ###
# ##################################################

class FullMaskCreator ( Mapper ):
	"""
	Computes the mask over all object instances.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.FullMaskCreator`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.FullMaskCreatorSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullMaskCreator )
		assert pytools.assertions.type_is_instance_of( settings, FullMaskCreatorSettings )

		super( FullMaskCreator, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this full mask creator.

		Arguments:
			self (`pydataset.manipualtors.image.FullMaskCreator`): Full mask creator instance.
			example                 (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullMaskCreator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Full mask image
		full_mask = None

		# Iterate over object instances
		instances_iterator_url = example.absolute_url + '/' + self.settings.instances_url_search_pattern
		instances_iterator     = pydataset.dataset.Iterator( instances_iterator_url )
		instances_count        = len(instances_iterator)
		if instances_count == 0:
			raise RuntimeError( "Could not find object instances examples using url '{}'".format(instances_iterator_url) )
		for instance_index in range( instances_count ):

			# Skip this one ?
			if ( instance_index in self.settings.skip_indexes ):
				continue

			# Read the example
			instance_example = instances_iterator.at( instance_index )

			# Get the object instance mask
			mask = instance_example.get_feature( self.settings.mask_feature_name ).value.astype( numpy.bool )

			# If the full mask is not initialized,
			#uUse a copy of the isntance's mask
			if full_mask is None:
				full_mask = mask.copy()

			# Otherwise, use an element-wise OR
			# to get the composition of the masks
			else:
				full_mask = numpy.logical_or( full_mask, mask )
		
		# for instance_example in instances_iterator

		# Save the fill mask in this example
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.ImageFeatureData,
			     feature_name = self.settings.full_mask_feature_name,
			            value = full_mask.astype(numpy.uint8)*255,
			        extension = '.png',
			            param = 9,
			      auto_update = True
			)

		# Return the modified example
		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a full mask creator class.

		Arguments:
			cls (`type`): Type of full mask creator for which to create the settings.

		Returns:
			`pydataset.manipulators.image.FullMaskCreatorSettings`: The settings.
		"""
		return FullMaskCreatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RectangleCropper ( Mapper )