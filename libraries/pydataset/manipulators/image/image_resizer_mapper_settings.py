# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###     CLASS IMAGE-RESIZE-MAPPER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImageResizerSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'images_features_names',
		'size'
		]
	)
class ImageResizerMapperSettings ( MapperSettings ):
	"""
	Computes the binary segmentation mask from a rendered image settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, images_features_names, size, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.ImageResizerMapperSettings` class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipulators.image.ImageResizerMapperSettings`): Instance to initialize.
			images_features_names                          (`list` of `str`): Names of the features containing the images to resize.
			size                                          (`tuple` of `int`): Size to which resize images.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( size, tuple )
		assert pytools.assertions.tuple_items_type_is( size, int )

		super( ImageResizerMapperSettings, self ).__init__( **kwargs )

		self._images_features_names = images_features_names
		self._size                  = size

	# def __init__ ( self, images_features_names, size, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def images_features_names ( self ):
		"""
		Names of the features containing the images to resize (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapperSettings )

		return self._image_feature_name

	# def images_features_names ( self )

	# --------------------------------------------------

	@images_features_names.setter
	def images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._image_feature_name = value
	
	# def images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def size ( self ):
		"""
		Size to which resize images (`tuple` of `int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapperSettings )

		return self._size

	# def size ( self )

	# --------------------------------------------------

	@size.setter
	def size ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapperSettings )
		assert pytools.assertions.type_is( value, tuple )
		assert pytools.assertions.tuple_items_type_is( value, int )

		self._size = value
	
	# def size ( self, value )

# class ImageResizerMapperSettings ( MapperSettings )