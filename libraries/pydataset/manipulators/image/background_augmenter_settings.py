# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from ..augmenter_settings import AugmenterSettings

# ##################################################
# ###    CLASS BACKGROUND-AUGMENTER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BackgroundAugmenterSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'backgrounds_directory_path',
		'number_of_random_rectangle',
		'image_feature_name',
		'mask_feature_name',
		]
	)
class BackgroundAugmenterSettings ( AugmenterSettings ):
	"""
	Changes the background of images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		image_feature_name = 'image',
		depth_feature_name = 'depth',
		mask_feature_name = 'mask',
		backgrounds_directory_path = '',
		number_of_random_rectangle = 2,
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.BackgroundAugmenterSettings` class.
		
		Affected features can be any `pydataset.data.NDArrayFeatureData` as long
		as the array describes a set of 2D points belonging to the image plane
		or a 3x3 transform matrix that applies to sets of 2D points.

		Arguments:
			self (`pydataset.manipualtors.image.BackgroundAugmenterSettings`): Instance to initialize.
			image_feature_name                                        (`str`): Name of the feature containing the image.
			depth_feature_name                                        (`str`): Name of the feature containing the depth image.
			mask_feature_name                                         (`str`): Name of the feature containing the mask.
			backgrounds_directory_path   (`pytools.path.DirectoryPath`/`str`): Path of the directory containing the backgrounds images.
			number_of_random_rectangle                                (`int`): Number of random rectangle to take for each background image.

		Named Arguments:
			dst_group_url       (`str`): URL of the group where to save the augmentations.
			url_search_pattern  (`str`): URL used to search for example(s) to manipulate.
			name                (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( depth_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is_instance_of( backgrounds_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( number_of_random_rectangle, int )

		super( BackgroundAugmenterSettings, self ).__init__( **kwargs )

		if isinstance( backgrounds_directory_path, str ):
			backgrounds_directory_path = pytools.path.DirectoryPath( backgrounds_directory_path )

		self._image_feature_name         = image_feature_name
		self._depth_feature_name         = depth_feature_name
		self._mask_feature_name          = mask_feature_name		
		self._backgrounds_directory_path = backgrounds_directory_path
		self._number_of_random_rectangle = number_of_random_rectangle

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		"""
		Name of the feature containing the depth image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )

		return self._depth_feature_name
	
	# def depth_feature_name ( self )

	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_feature_name = value
	
	# def depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )

		return self._mask_feature_name
	
	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def backgrounds_directory_path ( self ):
		"""
		Path of the directory containing the backgrounds images (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )

		return self._backgrounds_directory_path
	
	# def backgrounds_directory_path ( self )

	# --------------------------------------------------

	@backgrounds_directory_path.setter
	def backgrounds_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )
		
		if isinstance( value, str ):
			value = pytools.path.DirectoryPath( value )

		self._backgrounds_directory_path = value
	
	# def backgrounds_directory_path ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_random_rectangle ( self ):
		"""
		Number of random rectangle to take for each background image (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )

		return self._number_of_random_rectangle
	
	# def number_of_random_rectangle ( self )

	# --------------------------------------------------

	@number_of_random_rectangle.setter
	def number_of_random_rectangle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenterSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_random_rectangle = value
	
	# def number_of_random_rectangle ( self, value )

# class BackgroundAugmenterSettings ( AugmenterSettings )