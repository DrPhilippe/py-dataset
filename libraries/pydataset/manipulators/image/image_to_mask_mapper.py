# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                       import Mapper
from .image_to_mask_mapper_settings import ImageToMaskMapperSettings

# ##################################################
# ###         CLASS IMAGE-TO-MASK-MAPPER         ###
# ##################################################

class ImageToMaskMapper ( Mapper ):
	"""
	Mapper that computes binary segmentation mask from a rendered image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.ImageToMaskMapper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.ImageToMaskMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapper )
		assert pytools.assertions.type_is_instance_of( settings, ImageToMaskMapperSettings )

		super( ImageToMaskMapper, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.ImageToMaskMapper`): Mapper used to map the example.
			example                   (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get feature
		image_feature = example.get_feature( self.settings.image_feature_name )
			
		# Check feature
		if not isinstance( image_feature.data, pydataset.dataset.ImageFeatureData ):
			raise RuntimeError(
				'Unsupported feature data type: type={}, expected=ImageFeatureData'.format( type(image_feature.data) )
				)

		# Fetch shape and image
		shape = image_feature.data.shape
		rank  = shape.get_rank()
		image = image_feature.data.value

		# gray image
		if rank == 2:
			mask = image

		# RGB image
		elif rank == 3 and shape[2] == 3:
			mask = image.sum( axis=2 )

		# RGBA image
		elif rank == 3 and shape[2] == 4:
			mask = image[ :, :, 3 ]

		else:
			raise RuntimeError( 'Unsupported image shape {}'.format(shape) )

		# Threshold the nmask
		mask = mask > self.settings.threshold

		# Cast mask to image
		mask = mask.astype( numpy.float32 )
		mask = mask / numpy.amax( mask ) * 255.0
		mask = mask.astype( numpy.bool )

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.mask_feature_name,
			            shape = pydataset.data.ShapeData( shape[0], shape[1] ),
			            value = mask,
			            dtype = numpy.dtype( 'bool' )
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an image to mask mapper class.

		Arguments:
			cls (`type`): Type of image to mask mapper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.ImageToMaskMapperSettings`: The settings.
		"""
		return ImageToMaskMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImageToMaskMapper ( Mapper )