# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..augmenter_settings import AugmenterSettings

# ##################################################
# ###       CLASS BLUR-AUGMENTER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BlurAugmenterSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'image_feature_name',
		'kernel_size_range',
		'kernel_sigma_range'
		]
	)
class BlurAugmenterSettings ( AugmenterSettings ):
	"""
	Creates image augmentations by blurring images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, image_feature_name='', kernel_size_range=(3, 7, 2), kernel_sigma_range=(0.0, 0.0, 1.0), **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.BlurAugmenterSettings` class.

		Arguments:
			self (`pydataset.manipualtors.image.BlurAugmenterSettings`): Instance to initialize.
			image_feature_name                                  (`str`): Name of the feature containing the image to blur.
			kernel_size_range                   (`tuple` of `3` `int`s): Kernel size range defined as min, max, step.
			kernel_sigma_range                (`tuple` of `3` `float`s): Kernel sigma range defined as min, max, step.

		Named Arguments:
			dst_group_url       (`str`): URL of the group where to save the augmentations.
			url_search_pattern  (`str`): URL used to search for example(s) to manipulate.
			name                (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( kernel_size_range, tuple )
		assert pytools.assertions.tuple_items_type_is( kernel_size_range, int )
		assert pytools.assertions.equal( len(kernel_size_range), 3 )
		assert pytools.assertions.type_is( kernel_sigma_range, tuple )
		assert pytools.assertions.tuple_items_type_is( kernel_sigma_range, float )
		assert pytools.assertions.equal( len(kernel_sigma_range), 3 )

		super( BlurAugmenterSettings, self ).__init__( **kwargs )

		self._image_feature_name = image_feature_name
		self._kernel_size_range  = kernel_size_range
		self._kernel_sigma_range = kernel_sigma_range

	# def __init__ ( self, image_feature_name, kernel_size_range, kernel_sigma_range, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image to blur (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def kernel_size_range ( self ):
		"""
		Kernel size range defined as min, max, step (`tuple` of `3` `int`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )

		return self._kernel_size_range
	
	# def kernel_size_range ( self )

	# --------------------------------------------------

	@kernel_size_range.setter
	def kernel_size_range ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._kernel_size_range = value
	
	# def kernel_size_range ( self, value )

	# --------------------------------------------------

	@property
	def kernel_sigma_range ( self ):
		"""
		Kernel sigma range defined as min, max, step (`tuple` of `3` `float`s).
		"""
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )

		return self._kernel_sigma_range
	
	# def kernel_sigma_range ( self )

	# --------------------------------------------------

	@kernel_sigma_range.setter
	def kernel_sigma_range ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, BlurAugmenterSettings )
		assert pytools.assertions.type_is( value, str )

		self._kernel_sigma_range = value
	
	# def kernel_sigma_range ( self, value )
	
# class BlurAugmenterSettings ( AugmenterSettings )