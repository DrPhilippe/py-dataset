# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ### CLASS MASK-TO-BOUNDING-BOX-MAPPER-SETTINGS ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MaskToBoundingRectangleMapperSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'mask_feature_name',
		'bounding_rectangle_feature_name'
		]
	)
class MaskToBoundingRectangleMapperSettings ( MapperSettings ):
	"""
	Uses binary segmentation mask to compute a bounding-box.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, mask_feature_name, bounding_rectangle_feature_name, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.MaskToBoundingRectangleMapperSettings` class.
		
		The url search pattern is used to configure an iterator, thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipulators.image.MaskToBoundingRectangleMapperSettings`): Instance to initialize.
			mask_feature_name                                                   (`str`): Name of the feature containing the mask.
			bounding_rectangle_feature_name                                     (`str`): Name of the feature where to save the bounding box.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapperSettings )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )

		super( MaskToBoundingRectangleMapperSettings, self ).__init__( **kwargs )

		self._mask_feature_name               = mask_feature_name
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name

	# def __init__ ( self, mask_feature_name, bounding_rectangle_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature containing the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapperSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature where to save the bounding-box (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapperSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MaskToBoundingRectangleMapperSettings )
		assert pytools.assertions.type_is( value, int )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

# class MaskToBoundingRectangleMapperSettings ( MapperSettings )