# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
import pydataset.data
import pydataset.dataset
from ..mapper_settings import MapperSettings

# ##################################################
# ###      CLASS RECTANGLE-CROPPER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RectangleCropperSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'images_features_names',
		'bounding_rectangle_feature_name',
		'crop_rectangle',
		'cropped_images_features_names',
		'cropped_bounding_rectangle_feature_name',
		'crop_offset_feature_name',
		'affected_features_names'
		]
	)
class RectangleCropperSettings ( MapperSettings ):
	"""
	Takes a crop of with given rectangle on all the listed images.
	The rectangle can optionally be centered on the objects bounding rectangle.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		images_features_names=[],
		bounding_rectangle_feature_name='',
		crop_rectangle=pydataset.data.RectangleData(),
		cropped_images_features_names=[],
		cropped_bounding_rectangle_feature_name='',
		crop_offset_feature_name='',
		affected_features_names=[],
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.RectangleCropperSettings` class.
		
		Affected features can be any `pydataset.data.NDArrayFeatureData` as long
		as the array describes a set of 2D points belonging to the image plane
		or a 3x3 transform matrix that applies to sets of 2D points.

		The url search pattern is used to configure an iterator,
		thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipulators.image.RectangleCropperSettings`): Instance to initialize.
			images_features_names                        (`list` of `str`): Names of the features containing the images to crop.
			bounding_rectangle_feature_name                        (`str`): Name of the feature containing the bounding rectangle of the object.
			crop_rectangle                (`pydataset.data.RectangleData`): Crop rectangle.
			cropped_images_features_names                (`list` of `str`): Names of the features where to save the croped the images.
			cropped_bounding_rectangle_feature_name                (`str`): Name of the feature where to save the cropped bounding rectangle.
			crop_offset_feature_name                               (`str`): Name of the feature where to save the crop offset.
			affected_features_names                      (`list` of `str`): Names of features affected by the cropping opération that should be updated.
			camera_matrix_feature_name                             (`str`): Name of the feature containing the camera parameters.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is_instance_of( crop_rectangle, pydataset.data.RectangleData )
		assert pytools.assertions.type_is( cropped_images_features_names, list )
		assert pytools.assertions.list_items_type_is( cropped_images_features_names, str )
		assert pytools.assertions.type_is( cropped_bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( crop_offset_feature_name, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )

		super( RectangleCropperSettings, self ).__init__( **kwargs )

		self._images_features_names                   = list(images_features_names)
		self._bounding_rectangle_feature_name         = bounding_rectangle_feature_name
		self._crop_rectangle                          = copy.deepcopy(crop_rectangle)
		self._cropped_images_features_names           = list(cropped_images_features_names) if cropped_images_features_names else list(images_features_names)
		self._cropped_bounding_rectangle_feature_name = cropped_bounding_rectangle_feature_name if cropped_bounding_rectangle_feature_name else bounding_rectangle_feature_name
		self._crop_offset_feature_name                = crop_offset_feature_name
		self._affected_features_names                 = list(affected_features_names)

		# Check images mapping
		assert pytools.assertions.equal( len(self._images_features_names), len(self._cropped_images_features_names) )

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def images_features_names ( self ):
		"""
		Names of the features containing the images to crop (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._images_features_names

	# def images_features_names ( self )

	# --------------------------------------------------

	@images_features_names.setter
	def images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._images_features_names = value
	
	# def images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature containing the bounding rectangle of the object (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def crop_rectangle ( self ):
		"""
		Crop rectangle (`pydataset.data.RectangleData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._crop_rectangle

	# def crop_rectangle ( self )

	# --------------------------------------------------

	@crop_rectangle.setter
	def crop_rectangle ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is_instance_of( value, pydataset.data.RectangleData )

		self._crop_rectangle = value
	
	# def crop_rectangle ( self, value )

	# --------------------------------------------------

	@property
	def cropped_images_features_names ( self ):
		"""
		Names of the features where to save the croped the images (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._cropped_images_features_names

	# def cropped_images_features_names ( self )

	# --------------------------------------------------

	@cropped_images_features_names.setter
	def cropped_images_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._cropped_images_features_names = value
	
	# def cropped_images_features_names ( self, value )

	# --------------------------------------------------

	@property
	def cropped_bounding_rectangle_feature_name ( self ):
		"""
		Names of the feature where to save the croped bounding rectangle (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._cropped_bounding_rectangle_feature_name

	# def cropped_bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@cropped_bounding_rectangle_feature_name.setter
	def cropped_bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._cropped_bounding_rectangle_feature_name = value
	
	# def cropped_bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def crop_offset_feature_name ( self ):
		"""
		Name of the feature where to save the crop offset (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._crop_offset_feature_name

	# def crop_offset_feature_name ( self )

	# --------------------------------------------------

	@crop_offset_feature_name.setter
	def crop_offset_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, str )

		self._crop_offset_feature_name = value
	
	# def crop_offset_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def affected_features_names ( self ):
		"""
		Names of features affected by the cropping opération that should be updated (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )

		return self._affected_features_names

	# def affected_features_names ( self )

	# --------------------------------------------------

	@affected_features_names.setter
	def affected_features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleCropperSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._affected_features_names = value
	
	# def affected_features_names ( self, value )

# class RectangleCropperSettings ( MapperSettings )