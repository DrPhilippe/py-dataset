# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools

# LOCALS
import pydataset.dataset
from ..instances_mapper           import InstancesMapper
from .full_depth_creator_settings import FullDepthCreatorSettings

# ##################################################
# ###          CLASS FULL-DEPTH-CREATOR          ###
# ##################################################

class FullDepthCreator ( InstancesMapper ):
	"""
	Composes the depth of all object instances in an image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipualtors.image.FullDepthCreator` class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.FullDepthCreator`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.FullDepthCreatorSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullDepthCreator )
		assert pytools.assertions.type_is_instance_of( settings, FullDepthCreatorSettings )

		super( FullDepthCreator, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this full depth creator.

		Arguments:
			self (`pydataset.manipualtors.image.FullDepthCreator`): Full mask creator instance.
			example                  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullDepthCreator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Full mask image
		full_depth = None

		# Iterate over object instances
		for instance in self.create_instances_iterator( example ):

			# Get the object instance mask
			depth = instance.get_feature( self.settings.depth_feature_name ).value
			
			# If the full mask is not initialized,
			#uUse a copy of the isntance's mask
			if full_depth is None:
				full_depth = depth.copy()

			# Otherwise, use an element-wise OR
			# to get the composition of the masks
			else:
				full_depth = numpy.minimum( full_depth, depth )
		
		# for instance in self.create_instances_iterator( example )

		# Save the fill mask in this example
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.depth_feature_name,
			            value = full_depth,
			      auto_update = True
			)

		# Return the modified example
		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a full depth creator class.

		Arguments:
			cls (`type`): Type of full depth creator for which to create the settings.

		Returns:
			`pydataset.manipulators.image.FullDepthCreatorSettings`: The settings.
		"""
		return FullDepthCreatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class FullDepthCreator ( Mapper )