# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..instances_mapper                 import InstancesMapper
from .depth_map_to_mask_mapper_settings import DepthMapToMaskMapperSettings

# ##################################################
# ###       CLASS DEPTH-MAP-TO-MASK-MAPPER       ###
# ##################################################

class DepthMapToMaskMapper ( InstancesMapper ):
	"""
	Computes the mask from depth maps.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.DepthMapToMaskMapper` class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.DepthMapToMaskMapper`): Instance to initialize.
			settings (`pydataset.manipualtors.image.DepthMapToMaskMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapper )
		assert pytools.assertions.type_is_instance_of( settings, DepthMapToMaskMapperSettings )

		super( DepthMapToMaskMapper, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.DepthMapToMaskMapper`): Mapper used to map the example.
			example                      (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the image depth map
		image_depth_map = example.get_feature( self.settings.depth_map_feature_name ).value.astype( numpy.float32 )

		# Find out valid pixels in the instance depth map
		# This is the mask
		mask = numpy.greater( image_depth_map, 0. )

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.ImageFeatureData,
			     feature_name = self.settings.mask_feature_name,
			            value = mask.astype( numpy.uint8 )*255,
			        extension = '.png',
			            param = 9,
			      auto_update = True
			)

		# If they are no instances to map, return
		if not self.settings.instances_url_search_pattern:
			return example

		# Compute the mask, visible mask and visibility of each object instance
		for instance in self.create_instances_iterator( example ):

			# Get the object instance depth map
			instance_depth_map = instance.get_feature( self.settings.depth_map_feature_name ).value.astype( numpy.float32 )

			# Find out valid pixels in the instance depth map
			# This is the mask
			instance_mask = numpy.greater( instance_depth_map, 0. )

			# Find which part of the instance depth map is visible
			# This is the visible part of the mask
			instance_visible_mask = numpy.logical_and(
				instance_mask,
				numpy.less_equal( instance_depth_map, image_depth_map )
				)

			# Compute visibility
			visibility = numpy.sum( instance_visible_mask.astype(numpy.float32) )\
			           / numpy.sum(         instance_mask.astype(numpy.float32) )

			# Create the mask feature
			instance.create_or_update_feature(
				feature_data_type = pydataset.dataset.ImageFeatureData,
				     feature_name = self.settings.mask_feature_name,
				            value = instance_mask.astype( numpy.uint8 )*255,
				        extension = '.png',
				            param = 9,
				      auto_update = False
				)

			# Create the visible mask feature
			instance.create_or_update_feature(
				feature_data_type = pydataset.dataset.ImageFeatureData,
				     feature_name = self.settings.visible_mask_feature_name,
				            value = instance_visible_mask.astype( numpy.uint8 )*255,
				        extension = '.png',
				            param = 9,
				      auto_update = False
				)

			# Create the visible mask feature
			instance.create_or_update_feature(
				feature_data_type = pydataset.dataset.FloatFeatureData,
				     feature_name = self.settings.visibility_feature_name,
				            value = float(visibility),
				      auto_update = False
				)

			# Update
			instance.update()

		# for instance in self.create_instances_iterator( example )

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipulators.image.DepthMapToMaskMapper` class.

		Arguments:
			cls (`type`): Type of mapper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.DepthMapToMaskMapperSettings`: The settings.
		"""
		return DepthMapToMaskMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class DepthMapToMaskMapper ( Mapper )