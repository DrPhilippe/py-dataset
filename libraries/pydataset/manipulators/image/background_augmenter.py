# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from ..augmenter              import Augmenter
from .background_augmenter_settings import BackgroundAugmenterSettings

# ##################################################
# ###         CLASS BACKGROUND-AUGMENTER         ###
# ##################################################

class BackgroundAugmenter ( Augmenter ):
	"""
	Augments examples by exchanging images backgrounds.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.BackgroundAugmenter` class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.BackgroundAugmenter`): Augmenter instance to initialize.
			settings (`pydataset.manipualtors.image.BackgroundAugmenterSettings`): Settings of the augmenter.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenter )
		assert pytools.assertions.type_is_instance_of( settings, BackgroundAugmenterSettings )

		super( BackgroundAugmenter, self ).__init__( settings )
		
		self._backgrounds = []
		self._number_of_backgrounds = 0

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def augment ( self, example, index ):
		"""
		Augment the given example using this augmenter.

		Arguments:
			self (`pydataset.manipualtors.image.BackgroundAugmenter`): Augmenter used to augment the example.
			example                     (`pydataset.dataset.Example`): Example to augment.
			index                                             (`int`): Index of the example being augmented.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenter )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )

		augmentations = []

		# Modify background in each augmentation
		for background_index in range( self._number_of_backgrounds ):

			# Get a background
			background           = self._backgrounds[ background_index ]
			background_rectangle = pydataset.data.RectangleData( 0, 0, background.shape[1], background.shape[0] )

			# Take random rectangles in the background
			for random_rectangle_index in range(self.settings.number_of_random_rectangle):
				
				# Create a new example in the augmentation group
				augmentation = self.create_augmentation( example, 'same', '{:05d}' )
				augmentations.append( augmentation )
				
				# Get foreground image and mask
				image_feature = augmentation.get_feature( self.settings.image_feature_name )
				image         = image_feature.value
				mask          = augmentation.get_feature( self.settings.mask_feature_name ).value

				# Mask bool -> float32
				mask = mask.astype( numpy.float32 )
				mask = mask / mask.max()

				# Take a random rectangle in the background
				background_crop_rectangle = pydataset.data.RectangleData.random_integer_in( background_rectangle, mask.shape[1], mask.shape[0] )
				background_crop           = pydataset.images_utils.crop_image( background, background_crop_rectangle )

				# past the foreground image on the background crop
				image = pydataset.images_utils.past_image( image, background_crop, mask, 'linear' )

				# Update the image feature
				image_feature.value = image
				image_feature.update()
				
				# Optionally, mask out depth
				if self.settings.depth_feature_name:
					depth_feature = augmentation.get_feature( self.settings.depth_feature_name )
					depth = depth_feature.value.copy()
					depth[ mask == 0 ] = numpy.iinfo(numpy.uint8).max
					depth_feature.value = depth
					depth_feature.update()

			# for index_rectangle in self.settings.number_of_rectangle:

		# for index_augmentation in range( self._number_of_backgrounds )

		return augmentations

	# def augment ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before augmentation starts.

		Arguments:
			self (`pydataset.manipualtors.BackgroundAugmenter`): Augmenter used to augment the example.
			root_group              (`pydataset.dataset.Group`): Group containing the examples that will be augmented.
			number_of_examples                          (`int`): Number of examples to augment.
		"""
		assert pytools.assertions.type_is_instance_of( self, BackgroundAugmenter )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( BackgroundAugmenter, self ).begin( root_group, number_of_examples )

		self._backgrounds = []

		for content_path in self.settings.backgrounds_directory_path.list_contents():

			if content_path.is_a_file() and content_path.extension() in ('.jpg', '.jpeg', '.png'):

				background = cv2.imread( str(content_path), cv2.IMREAD_COLOR )
				self._backgrounds.append( background )
			
			# if content_path.is_a_file() and content_path.extension() in ('.jpg', '.jpeg', '.png')

		# for content_path in self.settings.backgrounds_directory_path.list_contents()

		self._number_of_backgrounds = len( self._backgrounds )

	# def begin ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a `pydataset.manipulators.image.BackgroundAugmenter`.

		Named Arguments:
			image_feature_name (`str`): Name of the feature containing the image.
			mask_feature_name  (`str`): Name of the feature containing the mask.
			dst_group_url      (`str`): URL of the group where to save the augmentations.
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.

		Returns:
			`pydataset.manipulators.image.BackgroundAugmenterSettings`: Settings for a `pydataset.manipulators.image.BackgroundAugmenter`.
		"""
		
		return BackgroundAugmenterSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class BackgroundAugmenter ( Augmenter )