# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###      CLASS FULL-MASK-CREATOR-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FullMaskCreatorSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'instances_url_search_pattern',
		'mask_feature_name',
		'skip_indexes'
		]
	)
class FullMaskCreatorSettings ( MapperSettings ):
	"""
	Settings used to computes the mask over all object instances.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		instances_url_search_pattern = 'examples/*',
		mask_feature_name = 'mask',
		full_mask_feature_name = 'full_mask',
		skip_indexes = [],
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.FullMaskCreatorSettings` class.
		

		Arguments:
			self (`pydataset.manipulators.image.FullMaskCreatorSettings`): Instance to initialize.
			instances_url_search_pattern                          (`str`): URL search pattern used to locate object instances examples.
			mask_feature_name                                     (`str`): Name of the feature containing the isntance mask.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullMaskCreatorSettings )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( full_mask_feature_name, str )
		assert pytools.assertions.type_is( skip_indexes, list )

		super( FullMaskCreatorSettings, self ).__init__( **kwargs )

		self.instances_url_search_pattern = instances_url_search_pattern
		self.mask_feature_name            = mask_feature_name
		self.full_mask_feature_name       = full_mask_feature_name
		self.skip_indexes                 = list(skip_indexes)

	# def __init__ ( self, ... )

# class FullMaskCreatorSettings ( MapperSettings )