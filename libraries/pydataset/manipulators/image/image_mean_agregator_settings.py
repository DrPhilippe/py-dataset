# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###          CLASS AGREGATOR-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImageMeanAgregatorSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'image_feature_name',
		'image_mean_feature_name'
		]
	)
class ImageMeanAgregatorSettings ( AgregatorSettings ):
	"""
	Image mean agregator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, image_feature_name='',  image_mean_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the image mean agregator settings class.
		
		Arguments:
			self (`pydataset.manipualtors.image.ImageMeanAgregatorSettings`): Instance to initialize.
			image_feature_name                                       (`str`): Name of the feature containing the images to agregate.
			image_mean_feature_name                                  (`str`): Name of the feature used to save the image mean.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to agregate.
			dst_group_url      (`str`): URL of the group where to save the agregated mean image feature.
			name               (`str`): Name of the image mean agregator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregatorSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( image_mean_feature_name, str )

		self._image_feature_name      = image_feature_name
		self._image_mean_feature_name = image_mean_feature_name

		super( ImageMeanAgregatorSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregatorSettings )
		
		return self._image_feature_name

	# def image_feature_name ( self )
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def image_mean_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregatorSettings )
		
		return self._image_mean_feature_name

	# def image_mean_feature_name ( self )
	
	# --------------------------------------------------

	@image_mean_feature_name.setter
	def image_mean_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageMeanAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._image_mean_feature_name = value

	# def image_mean_feature_name ( self, value )
	
# class ImageMeanAgregatorSettings ( AgregatorSettings )