# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..instances_mapper_settings import InstancesMapperSettings

# ##################################################
# ###      CLASS FULL-MASK-CREATOR-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FullDepthCreatorSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'depth_feature_name'
		]
	)
class FullDepthCreatorSettings ( InstancesMapperSettings ):
	"""
	Settings used to compose the depth of all object instances in an image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		instances_url_search_pattern = 'examples/*',
		depth_feature_name = 'depth',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.FullDepthCreatorSettings` class.
		

		Arguments:
			self (`pydataset.manipulators.image.FullDepthCreatorSettings`): Instance to initialize.
			instances_url_search_pattern                           (`str`): URL search pattern used to locate object instances examples.
			depth_feature_name                                     (`str`): Name of the feature containing the depth image.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, FullDepthCreatorSettings )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( depth_feature_name, str )

		super( FullDepthCreatorSettings, self ).__init__( instances_url_search_pattern, **kwargs )

		self._depth_feature_name = depth_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		"""
		Name of the feature containing the depth image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FullDepthCreatorSettings )

		return self._depth_feature_name

	# def depth_feature_name ( self )

	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FullDepthCreatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_feature_name = value

	# def depth_feature_name ( self, value )

# class FullDepthCreatorSettings ( MapperSettings )