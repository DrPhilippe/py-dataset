# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..mapper                    import Mapper
from .rectangle_cropper_settings import RectangleCropperSettings

# ##################################################
# ###          CLASS RECTANGLE-CROPPER           ###
# ##################################################

class RectangleCropper ( Mapper ):
	"""
	Mapper used to crop images.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.RectangleCropper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.RectangleCropperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropper )
		assert pytools.assertions.type_is_instance_of( settings, RectangleCropperSettings )

		super( RectangleCropper, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.RectangleCropper`): Mapper used to map the example.
			example                  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleCropper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Get crop rectangle
		crop_rectangle = copy.deepcopy( self.settings.crop_rectangle )

		# Is the bounding rectangle feature used ?
		if self.settings.bounding_rectangle_feature_name:
			
			# Get bounding rectangle feature
			bounding_rectangle_feature = example.get_feature( self.settings.bounding_rectangle_feature_name )
			
			# If the feature is an ndarray
			if isinstance( bounding_rectangle_feature.data, pydataset.dataset.NDArrayFeatureData ):

				# Check the shape
				if bounding_rectangle_feature.data.shape != pydataset.data.ShapeData(2,2):
					raise RuntimeError(
					'Unsupported NDArray bounding rectangle feature: shape={}, expected=2x2'.format( bounding_rectangle_feature.data.shape )
					)

				# Check the dtype
				if bounding_rectangle_feature.data.dtype != numpy.dtype('int32'):
					raise RuntimeError(
					'Unsupported NDArray bounding rectangle feature: dtype={}, expected=int32'.format( bounding_rectangle_feature.data.dtype )
					)

				# Get the value
				bounding_rectangle = pydataset.data.RectangleData.from_ndarray( bounding_rectangle_feature.value )

			# If the feature is a serialized rectangle
			elif isinstance( bounding_rectangle_feature.data, pydataset.dataset.SerializableFeatureData ):
				
				# Get bounding rectangle
				bounding_rectangle = bounding_rectangle_feature.value

				# Check bounding rectangle
				if not isinstance( bounding_rectangle, pydataset.data.RectangleData ):
					raise RuntimeError(
						'Unsupported bounding rectangle type: type={}, expected=RectangleData'.format( type(bounding_rectangle) )
						)
			else:
				raise RuntimeError(
					'Unsupported bounding rectangle feature data type: type={}, expected=SerializableFeatureData,NDArrayFeatureData'.format( type(bounding_rectangle_feature.data) )
					)

			# if isinstance( bounding_rectangle_feature.data, pydataset.dataset.NDArrayFeatureData )
			
			# Center crop rectangle on bounding rectangle
			crop_rectangle.center_on( *bounding_rectangle.center )

		# if self.settings.bounding_rectangle_feature_name:	

		# Ref image rect to check images have the same size
		ref_image_rectangle = None

		# Crop each images
		number_of_images_to_crop = len( self.settings.images_features_names )
		for index in range( number_of_images_to_crop ):
			
			# Get image feature
			image_feature_name = self.settings.images_features_names[ index ]
			image_feature = example.get_feature( image_feature_name )
				
			# Check feature image
			if not isinstance( image_feature.data, pydataset.dataset.NDArrayFeatureData ):
				raise RuntimeError(
					'Unsupported image feature data type: type={}, expected=NDArrayFeatureData'.format( type(image_feature.data) )
					)

			# Get image
			image           = image_feature.value
			image_rectangle = pydataset.data.RectangleData.from_size( 0, 0, image.shape[1], image.shape[0] )

			# Is the reference image rectangle defined ?
			# I.E. Is this the first image beeing cropped ?
			if not ref_image_rectangle:

				# Define it
				ref_image_rectangle = image_rectangle

				# Check the crop rectangle fits
				if not crop_rectangle.fits_in( image_rectangle ):
					raise RuntimeError( 'The crop rectangle {} does not fit in the image {}'.format(crop_rectangle, image_rectangle) )

				# Make sure the crop rectangle is in the image
				# This is done once to make sure all crop are taken at the same location on all images
				if crop_rectangle.x1 < 0:
					crop_rectangle.shift( -crop_rectangle.x1, 0 )
				elif crop_rectangle.x2 > image_rectangle.width:
					crop_rectangle.shift( image_rectangle.width - crop_rectangle.x2, 0 )

				if crop_rectangle.y1 < 0:
					crop_rectangle.shift( 0, -crop_rectangle.y1 )
				elif crop_rectangle.y2 > image_rectangle.height:
					crop_rectangle.shift( 0, image_rectangle.height - crop_rectangle.y2 )
				
				# Save the crop offset
				if self.settings.crop_offset_feature_name:

					# Save the modified bounding rectangle of the object in the crop
					example.create_or_update_feature(
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						     feature_name = self.settings.crop_offset_feature_name,
						            value = numpy.asarray( [float(-crop_rectangle.x1), float(-crop_rectangle.y1)], dtype=numpy.float32 ),
						            shape = pydataset.data.ShapeData( 2 ),
						      auto_update = False
						)
				
				# if self.settings.crop_offset_feature_name

				# Is the bounding rectangle feature used ?
				if self.settings.bounding_rectangle_feature_name:

					# Compute the new bounding rectangle of the object in the crop
					new_bounding_rectangle = copy.deepcopy( bounding_rectangle )
					new_bounding_rectangle.shift( -crop_rectangle.x1, -crop_rectangle.y1 )

					# Save the modified bounding rectangle of the object in the crop
					example.create_or_update_feature(
						feature_data_type = pydataset.dataset.NDArrayFeatureData,
						     feature_name = self.settings.cropped_bounding_rectangle_feature_name,
						            value = new_bounding_rectangle.to_ndarray(),
						      auto_update = False
						)
					
				# if self.settings.bounding_rectangle_feature_name
			
			# if not ref_image_rectangle

			# Reference image rectangle is defined
			else:
				# Check all image have the same size
				if ref_image_rectangle != image_rectangle:
					raise RuntimeError( 'Image size do not match {}/{}'.format(ref_image_rectangle, image_rectangle) )
			
			# Crop image
			crop = pydataset.images_utils.crop_image( image, crop_rectangle )

			# Create/Update a feature of the same type as the image feature for the crop
			crop_feature = example.create_or_update_feature(
				feature_data_type = type(image_feature.data),
				     feature_name = self.settings.cropped_images_features_names[ index ],
				      auto_update = False
				)

			# Copy the image feature data
			crop_feature.copy( image_feature, auto_update=False )

			# Modify the image
			crop_feature.value = crop

			# Update the feature
			crop_feature.update()
		
		# for index in range( number_of_images_to_crop )

		# Update the example
		example.update()

		# Modify affected features names
		for affected_feature_name in self.settings.affected_features_names:

			# Get the affected feature
			affected_feature = example.get_feature( affected_feature_name )

			# Check affected feature data type
			if not isinstance( affected_feature.data, pydataset.dataset.NDArrayFeatureData ):
				raise RuntimeError(
					'Unsupported affected feature data type: type={}, expected=NDArrayFeatureData'.format( type(affected_feature.data) )
					)

			# Is the feature a 3x3 transforma matrix ?
			if affected_feature.data.shape == pydataset.data.ShapeData(3,3):
				
				# Create the translation matrix
				T = numpy.eye( 3, dtype=numpy.float32 )
				T[0,2] = float( -crop_rectangle.x1 )
				T[1,2] = float( -crop_rectangle.y1 )

				# Apply translation after the affect transform
				value = numpy.dot( T, affected_feature.value )

			# Otherwise it is considered to be a set of 2D points
			else:
				# Translation vector:
				t = numpy.asarray( [-crop_rectangle.x1, -crop_rectangle.y1], dtype=affected_feature.value.dtype )

				# Translate the array
				# If this does not work a ValueError is raised by numpy
				value = affected_feature.value + t

			# Save the feature for this example
			example.create_or_update_feature(
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				     feature_name = affected_feature_name,
				            value = value,
				      auto_update = True
				)

		# for affected_feature_name in self.settings.affected_features_names

		# Return the example
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a rectangle cropper class.

		Arguments:
			cls (`type`): Type of rectangle cropper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.RectangleCropperSettings`: The settings.
		"""
		return RectangleCropperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class RectangleCropper ( Mapper )