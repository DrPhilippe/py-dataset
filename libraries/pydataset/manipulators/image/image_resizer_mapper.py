# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                       import Mapper
from .image_resizer_mapper_settings import ImageResizerMapperSettings

# ##################################################
# ###         CLASS IMAGE-RESIZER-MAPPER         ###
# ##################################################

class ImageResizerMapper ( Mapper ):
	"""
	Mapper that resizes image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.ImageResizerMapper`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.image.ImageResizerMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapper )
		assert pytools.assertions.type_is_instance_of( settings, ImageResizerMapperSettings )

		super( ImageResizerMapper, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.image.ImageResizerMapper`): Mapper used to map the example.
			example                    (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageResizerMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		for image_feature_name in self.settings.images_features_names:
			
			# Get feature
			image_feature = example.get_feature( image_feature_name )
				
			# Check feature
			if not isinstance( image_feature.data, pydataset.dataset.ImageFeatureData ):
				raise RuntimeError(
					'Unsupported feature data type: type={}, expected=ImageFeatureData'.format( type(image_feature.data) )
					)

			# Resize the image
			image_feature.value = cv2.resize( image_feature.value, self.settings.size )

			# Update the feature
			image_feature.update()

		# Return the example
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an image resizer mapper class.

		Arguments:
			cls (`type`): Type of image resizer mapper for which to create the settings.

		Returns:
			`pydataset.manipulators.image.ImageResizerMapperSettings`: The settings.
		"""
		return ImageResizerMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ImageResizerMapper ( Mapper )