# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###    CLASS IMAGE-TO-MASK-MAPPER-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImageToMaskMapperSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'image_feature_name',
		'mask_feature_name',
		'threshold'
		]
	)
class ImageToMaskMapperSettings ( MapperSettings ):
	"""
	Computes the binary segmentation mask from a rendered image settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, image_feature_name, mask_feature_name, threshold, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.ImageToMaskMapperSettings` class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipulators.image.ImageToMaskMapperSettings`): Instance to initialize.
			image_feature_name                                      (`str`): Name of the feature containing the image.
			mask_feature_name                                       (`str`): Name of the feature where to save the mask.
			threshold                                               (`int`): Threshold used to compute the mask.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( threshold, int )

		super( ImageToMaskMapperSettings, self ).__init__( **kwargs )

		self._image_feature_name = image_feature_name
		self._mask_feature_name  = mask_feature_name
		self._threshold          = threshold

	# def __init__ ( self, image_feature_name, mask_feature_name, threshold, url_search_pattern, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature where to save the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def threshold ( self ):
		"""
		Threshold used to comput the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )

		return self._threshold

	# def threshold ( self )

	# --------------------------------------------------

	@threshold.setter
	def threshold ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageToMaskMapperSettings )
		assert pytools.assertions.type_is( value, int )

		self._threshold = value
	
	# def threshold ( self, value )

# class ImageToMaskMapperSettings ( MapperSettings )