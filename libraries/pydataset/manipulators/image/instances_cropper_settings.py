# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
import pydataset.data
import pydataset.dataset
from ..augmenter_settings import AugmenterSettings

# ##################################################
# ###      CLASS RECTANGLE-CROPPER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'InstancesCropperSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'crop_rectangle',
		'instances_url_search_pattern',
		'images_features_names',
		'bounding_rectangle_feature_name',
		'affected_features_names'
		]
	)
class InstancesCropperSettings ( AugmenterSettings ):
	"""
	Takes a crop of with given rectangle on all the listed images.
	The rectangle can optionally be centered on the objects bounding rectangle.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		crop_rectangle = pydataset.data.RectangleData(),
		instances_url_search_pattern = 'examples/*',
		images_features_names = [],
		bounding_rectangle_feature_name = '',
		mask_feature_name = '',
		affected_features_names = [],
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.InstancesCropperSettings` class.
		
		Affected features can be any `pydataset.data.NDArrayFeatureData` as long
		as the array describes a set of 2D points belonging to the image plane
		or a 3x3 transform matrix that applies to sets of 2D points.

		Arguments:
			self (`pydataset.manipulators.image.InstancesCropperSettings`): Instance to initialize.
			crop_rectangle                (`pydataset.data.RectangleData`): Rectangle used to crop the images.
			images_features_names                        (`list` of `str`): Names of the features containing the images to crop.
			bounding_rectangle_feature_name                        (`str`): Name of the feature containing the bounding rectangle of the object.
			mask_feature_name                                      (`str`): Name of the feature containing the isntance mask.
			affected_features_names                      (`list` of `str`): Names of features affected by the cropping opération that should be updated.


		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesCropperSettings )
		assert pytools.assertions.type_is_instance_of( crop_rectangle, pydataset.data.RectangleData )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( images_features_names, list )
		assert pytools.assertions.list_items_type_is( images_features_names, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( affected_features_names, list )
		assert pytools.assertions.list_items_type_is( affected_features_names, str )

		super( InstancesCropperSettings, self ).__init__( **kwargs )

		self.crop_rectangle                  = copy.deepcopy( crop_rectangle )
		self.instances_url_search_pattern    = instances_url_search_pattern
		self.images_features_names           = copy.deepcopy( images_features_names )
		self.bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self.mask_feature_name               = mask_feature_name
		self.affected_features_names         = copy.deepcopy( affected_features_names )

	# def __init__ ( self, ... )

# class InstancesCropperSettings ( AugmenterSettings )