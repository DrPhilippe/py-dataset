# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import random

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..mapper           import Mapper
from .occluder_settings import OccluderSettings

# ##################################################
# ###               CLASS OCCLUDER               ###
# ##################################################

class Occluder ( Mapper ):
	"""
	Mapper used to occlude some part of the image with other images of the same dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the occluder class.
		
		Arguments:
			self             (`pydataset.manipualtors.image.Occluder`): Occluder instance to initialize.
			settings (`pydataset.manipualtors.image.OccluderSettings`): Settings of the occluder.
		"""
		assert pytools.assertions.type_is_instance_of( self, Occluder )
		assert pytools.assertions.type_is_instance_of( settings, OccluderSettings )

		super( Occluder, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before occluding starts.

		Arguments:
			self (`pydataset.manipualtors.Occluder`): Occluder used to occlude the example.
			root_group   (`pydataset.dataset.Group`): Group containing the examples that will be occluded.
			number_of_examples               (`int`): Number of examples to occlude.
		"""
		assert pytools.assertions.type_is_instance_of( self, Occluder )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( Occluder, self ).begin( root_group, number_of_examples )

		url = '{}/{}'.format(
			root_group.dataset.absolute_url,
			self.settings.images_search_url
			)
		# print( 'URL:', url )

		self._iterator       = pydataset.dataset.Iterator( url )
		self._iterator_count = self._iterator.count
		# print( 'COUNT:', self._iterator_count )

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def find_occluding_example ( self, example ):
		"""
		Searches the dataset for an example that occluded the given one.

		Returns:
			`pydataset.dataset.Example`/`None`: The example that was found or `None`.
			`numpy.ndarray`/`None`: Mask or rectangle of the occluding example.
		"""
		# print( 'SEARCHING ... ')

		# Get the rectangle or mask defining the foreground region
		rectangle_or_mask = example.get_feature( self.settings.rectangle_or_mask_feature_name ).value

		# Check if the foreground is defined using a rectangle or a mask
		is_rectangle = (list(rectangle_or_mask.shape) == [2,2])
		
		# Convert to an actual rectangle if so
		if is_rectangle:
			rectangle = pydataset.data.RectangleData.from_ndarray( rectangle_or_mask )
		
		# Otherwise, compute the number of seen pixels
		else:
			mask      = rectangle_or_mask
			nb_pixels = numpy.sum( mask.astype(numpy.float32) )

		# Find an example that occludes the foreground
		indexes = list(range( self._iterator_count ))
		random.shuffle( indexes )
		nb_iteration = 0
		other_example = None
		other_rectangle_or_mask = None
		while ( nb_iteration < self._iterator_count ):

			# Get the index of an other example
			index = indexes[ nb_iteration ]

			# Get an other example
			other_example = self._iterator.at( index )
			
			# Check it is an example
			if not isinstance( other_example, pydataset.dataset.Example ):
				raise RuntimeError( "The 'images_search_url' must point to Examples, not '{}'".format(
					type(other_example)
					))

			# Get its rectangle or mask
			other_rectangle_or_mask = other_example.get_feature( self.settings.rectangle_or_mask_feature_name ).value

			# If we work on rectangles
			if is_rectangle:

				# Convert to an actual rectangle
				other_rectangle = pydataset.data.RectangleData.from_ndarray( other_rectangle_or_mask )

				# Compute intersection, it is the occluded part
				intersection = pydataset.data.RectangleData.intersection( rectangle, other_rectangle )

				# Compute ratio of occlusion
				occlusion = intersection.area / rectangle.area

			# Otherwise work on masks
			else:
				# Rename the other mask
				other_mask = other_rectangle_or_mask

				# Compute intersection
				intersection = numpy.logical_and( mask, other_mask )

				# Number of occluded pixels
				nb_occluded_pixels = numpy.sum( intersection.astype(numpy.float32) )

				# Compute ratio of occlusion
				occlusion = nb_occluded_pixels / nb_pixels
			
			# if is_rectangle
			
			# print( other_example.url, occlusion )

			# Check if the occlusion is within accapted range
			if ( self.settings.min_occlusion_threshold <= occlusion and occlusion <= self.settings.max_occlusion_threshold ):

				# print( 'FOUND ', other_example.absolute_url, occlusion )

				# Use this other example
				break

			# Reset other example
			else:
				other_example = None
				other_rectangle_or_mask = None

			# Next example, VERY important
			nb_iteration += 1

		# while ( nb_iteration < self._iterator_count )

		return other_example, other_rectangle_or_mask

	# def find_occluding_example ( self, example )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this occluder.

		Arguments:
			self (`pydataset.manipualtors.image.Occluder`): Mapper used to map the example.
			example          (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, Occluder )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# print( 'MAPPING', example.absolute_url )

		# Find an occluding example
		occluding_example, occluding_rectangle_or_mask = self.find_occluding_example( example )
		
		# Check if we found an other example
		if occluding_example is None:
			raise RuntimeError( "Failed to get an example that occluded example '{}'".format(
				example.absolute_url
				))

		# Occlude the example
		example = self.occlude_example( example, occluding_example, occluding_rectangle_or_mask )

		# Return it
		return example

	# def map ( self, example )
	
	# --------------------------------------------------

	def occlude_example ( self, example, occluding_example, occluding_rectangle_or_mask ):
		assert pytools.assertions.type_is_instance_of( self, Occluder )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is_instance_of( occluding_example, pydataset.dataset.Example )

		# Check if the foreground is defined using a rectangle or a mask
		is_rectangle = ( list(occluding_rectangle_or_mask.shape) == [2,2] )
		
		# Rename
		if is_rectangle:
			occluding_rectangle = occluding_rectangle_or_mask
		else:
			occluding_mask = occluding_rectangle_or_mask

		# print( 'OCCLUDING:' )

		# Occlude each of the images to work on
		for image_feature_name in self.settings.images_features_names:
			
			# print( '   ', image_feature_name )

			# Get the image of the example to occlude
			image_feature  = example.get_feature( image_feature_name )
			image          = image_feature.value.copy()
			original_shape = list(image.shape)
			image          = numpy.reshape( image, [image.shape[0], image.shape[1], -1] )
			depth          = image.shape[ 2 ]

			# Get the image of the occluding example
			occluding_image_feature = occluding_example.get_feature( image_feature_name )
			occluding_image         = occluding_image_feature.value.copy()
			occluding_image         = numpy.reshape( occluding_image, [occluding_image.shape[0], occluding_image.shape[1], -1] )

			# If we work on rectangle
			if is_rectangle:

				x1 = occluding_rectangle.x1
				x2 = occluding_rectangle.x2
				y1 = occluding_rectangle.y1
				y2 = occluding_rectangle.y2

				# Past the occluding foreground crop
				image[ x1:x2, y1:y2, : ] = occluding_image[ x1:x2, y1:y2, : ]

			# If we work on masks
			else:
				# Adapt mask to the depth
				adapted_mask = numpy.reshape( occluding_mask, [occluding_mask.shape[0], occluding_mask.shape[1], 1] )
				adapted_mask = numpy.repeat( adapted_mask, depth, axis=2 )

				# Select occluded pixels from the occluding image
				indexes = adapted_mask != 0
				image[ indexes ] = occluding_image[ indexes ]

			# Reset image shape
			image = numpy.reshape( image, original_shape )

			# Update the image
			image_feature.value = image
			image_feature.update()

		# for image_feature_name in self.settings.images_features_names

		# If the mask was specified, update the mask
		if not is_rectangle:

			# Get the mask
			mask_feature = example.get_feature( self.settings.rectangle_or_mask_feature_name )
			mask         = mask_feature.value.copy()
	
			# Anything under the occluded mask is occluded			
			mask[ occluding_mask ] = 0

			# Update the mask
			mask_feature.value = mask
			mask_feature.update()

		return example

	# def occlude_example ( self, example, occluding_example, occluding_rectangle_or_mask )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an occluder.

		Arguments:
			cls (`type`): Type of occluder for which to create the settings.

		Returns:
			`pydataset.manipulators.image.OccluderSettings`: The settings.
		"""
		return OccluderSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Occluder ( Mapper )
