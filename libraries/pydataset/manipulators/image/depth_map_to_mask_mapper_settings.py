# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..instances_mapper_settings import InstancesMapperSettings

# ##################################################
# ###  CLASS DEPTH-MAP-TO-MASK-MAPPER-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DepthMapToMaskMapperSettings',
	   namespace = 'pydataset.manipulators.image',
	fields_names = [
		'depth_map_feature_name',
		'mask_feature_name',
		'visible_mask_feature_name',
		'visibility_feature_name'
		]
	)
class DepthMapToMaskMapperSettings ( InstancesMapperSettings ):
	"""
	Settings used to computes the mask from depth maps.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		instances_url_search_pattern = 'examples/*',
		depth_map_feature_name = 'depth',
		mask_feature_name = 'mask',
		visible_mask_feature_name = 'visible_mask',
		visibility_feature_name = 'visibility',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.DepthMapToMaskMapperSettings` class.

		Arguments:
			self (`pydataset.manipulators.image.DepthMapToMaskMapperSettings`): Instance to initialize.
			instances_url_search_pattern                               (`str`): URL search pattern used to locate object instances examples.
			depth_map_feature_name                                     (`str`): Name of the feature containinf the depth map.
			mask_feature_name                                          (`str`): Name of the feature where to save the complete mask.
			visible_mask_feature_name                                  (`str`): Name of the feature where to save the visible part of the mask
			visibility_feature_name                                    (`str`): Name of the feature where to save the visibility proportion.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( depth_map_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( visible_mask_feature_name, str )
		assert pytools.assertions.type_is( visibility_feature_name, str )

		super( DepthMapToMaskMapperSettings, self ).__init__( instances_url_search_pattern, **kwargs )

		self._depth_map_feature_name    = depth_map_feature_name
		self._mask_feature_name         = mask_feature_name
		self._visible_mask_feature_name = visible_mask_feature_name
		self._visibility_feature_name   = visibility_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def depth_map_feature_name ( self ):
		"""
		Name of the feature containing the depth map (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )

		return self._depth_map_feature_name

	# def depth_map_feature_name ( self )

	# --------------------------------------------------

	@depth_map_feature_name.setter
	def depth_map_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._depth_map_feature_name = value
	
	# def depth_map_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		"""
		Name of the feature where to save the complete mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value
	
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visible_mask_feature_name ( self ):
		"""
		Name of the feature where to save the visible part of the mask (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )

		return self._visible_mask_feature_name

	# def visible_mask_feature_name ( self )

	# --------------------------------------------------

	@visible_mask_feature_name.setter
	def visible_mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._visible_mask_feature_name = value
	
	# def visible_mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visibility_feature_name ( self ):
		"""
		Name of the feature where to save the visibility proportion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )

		return self._visibility_feature_name

	# def visibility_feature_name ( self )

	# --------------------------------------------------

	@visibility_feature_name.setter
	def visibility_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DepthMapToMaskMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._visibility_feature_name = value
	
	# def visibility_feature_name ( self, value )

# class DepthMapToMaskMapperSettings ( InstancesMapperSettings )