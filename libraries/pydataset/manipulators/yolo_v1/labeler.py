# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..instances_mapper import InstancesMapper
from .labeler_settings import LabelerSettings

# ##################################################
# ###               CLASS LABELER                ###
# ##################################################

class Labeler ( InstancesMapper ):
	"""
	Mapper used to create yolo v1 labels.

	YOLO heavily uses IoU thus it is better to have labels
	in the format [x1, y1, x2, y2], all in image-relative
	coordinates that are bounded between 0 and 1.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.yolo_v1.Labeler` class.

		Arguments:
			self             (`pydataset.manipulators.yolo_v1.Labeler`): Labeler instance to initialize.
			settings (`pydataset.manipulators.yolo_v1.LabelerSettings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, Labeler )
		assert pytools.assertions.type_is_instance_of( settings, LabelerSettings )

		super( Labeler, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_yolo_labels ( self, example ):
		"""
		Creates the yolo labels for the given example.

		Arguments:
			self (`pydataset.manipulators.yolo_v1.Labeler`): Labeler instance.
			example           (`pydataset.dataset.Example`): Example for which to create the control points.

		Returns:
			`numpy.ndarray`: The labels
		"""
		assert pytools.assertions.type_is_instance_of( self, Labeler )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Usefull values
		S            = self.settings.number_of_cells
		C            = self.settings.number_of_classes

		# Get the image and fetch its dimensions
		image        = example.get_feature( self.settings.image_feature_name ).value
		image_width  = float( image.shape[ 1 ] )
		image_height = float( image.shape[ 0 ] )

		# Compute cell width and height
		cells_width  = image_width  / float(S)
		cells_height = image_height / float(S)
		
		# Initialize labels
		yolo_labels = numpy.zeros( [S, S, 5+C], dtype=numpy.float32 )
		
		# Cell occupancy buffer
		# We fill this buffer with the area of the assigned rectangle
		cell_occupancy = numpy.zeros( [S, S], dtype=numpy.float32 )
		
		# Iterate object instances
		for instance in self.create_instances_iterator( example ):
			
			# Get features
			bounding_rectangle = instance.get_feature( self.settings.bounding_rectangle_feature_name ).value
			one_hot_category   = instance.get_feature( self.settings.one_hot_category_feature_name   ).value.astype( numpy.float32 )

			# Convert rectangle
			bounding_rectangle = pydataset.data.RectangleData.from_ndarray( bounding_rectangle )
			
			# Get bounding rectangle center
			center_x, center_y = bounding_rectangle.center

			# Compute the indexes of the cell if falls into
			cell_index_x = int( numpy.floor( center_x / cells_width  ) )
			cell_index_y = int( numpy.floor( center_y / cells_height ) )
			cell_index_x = numpy.clip( cell_index_x, 0, S-1 )
			cell_index_y = numpy.clip( cell_index_y, 0, S-1 )

			# Check if the cell is not currently occupied
			# Or if it is occupied by a smaller rectangle
			if cell_occupancy[ cell_index_y, cell_index_x ] < bounding_rectangle.area:
				
				# YOLO heavily uses IoU thus it is better to have labels
				# in the format [x1, y1, x2, y2], all in image-relative
				# coordinates that are bounded between 0 and 1.

				# Set the bounding rectangle labels
				yolo_labels[ cell_index_y, cell_index_x, 0:5 ] = numpy.asarray([
					# Coordinates
					(bounding_rectangle.x1 / image_width ) if self.settings.normalize else bounding_rectangle.x1,
					(bounding_rectangle.y1 / image_height) if self.settings.normalize else bounding_rectangle.y1,
					(bounding_rectangle.x2 / image_width ) if self.settings.normalize else bounding_rectangle.x2,
					(bounding_rectangle.y2 / image_height) if self.settings.normalize else bounding_rectangle.y2, 
					# Confidence
					1.0
					])

				# Set the category labels
				yolo_labels[ cell_index_y, cell_index_x, 5: ] = one_hot_category

				# Flag the cell as occupied using the rectangle area
				cell_occupancy[ cell_index_y, cell_index_x ] = bounding_rectangle.area
			
			# if cell_occupancy[ cell_index_y, cell_index_x ] < bounding_rectangle.area

		# for instance_example in pydataset.dataset.Iterator( url )

		return yolo_labels

	# def create_yolo_labels ( self, example )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Creates the yolo labels for the mapped example.

		Arguments:
			self (`pydataset.manipulators.yolo_v1.Labeler`): Labeler instance.
			example           (`pydataset.dataset.Example`): Example for which to create the control points.

		Returns:
			`pydataset.dataset.Example`: The example with added yolo labels.
		"""
		assert pytools.assertions.type_is_instance_of( self, Labeler )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Create the labels
		yolo_labels = self.create_yolo_labels( example )
		
		# Save the control points
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.yolo_feature_name,
			            value = yolo_labels
			)
		
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		assert pytools.assertions.type_is( cls, type )

		return LabelerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Labeler ( InstancesMapper )