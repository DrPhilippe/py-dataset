# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..instances_mapper_settings import InstancesMapperSettings

# ##################################################
# ###              LABELER-SETTINGS              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'LabelerSettings',
	   namespace = 'pydataset.manipulators.yolo_v1',
	fields_names = [
		'number_of_cells',
		'number_of_classes',
		'image_feature_name',
		'bounding_rectangle_feature_name',
		'one_hot_category_feature_name',
		'yolo_feature_name',
		'normalize'
		]
	)
class LabelerSettings ( InstancesMapperSettings ):
	"""
	Settings used to create yolo v1 labels.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		number_of_cells=7,
		number_of_classes=20,
		instances_url_search_pattern='examples/*',
		image_feature_name='image',
		bounding_rectangle_feature_name='bounding_rectangle',
		one_hot_category_feature_name='one_hot_category',
		yolo_feature_name='yolo_v1',
		normalize=True,
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.yolo_v1.LabelerSettings` class.
		
		The labels contain only one bounding box with associated class per cell.
		Thus the number of detectors per cell, names B in the paper, is not used here.

		Arguments:
			self (`pydataset.manipulators.yolo_v1.LabelerSettings`): Instance to initialize.
			number_of_cells                                 (`int`): Number of cells on the width and height of the image, called S in the paper.
			number_of_classes                               (`str`): Number of classes to classify, called C in the paper.
			instances_url_search_pattern                    (`str`): URL used to find instances with respect to the current example that contains the image.
			image_feature_name                              (`str`): Name of the feature containing the image.
			bounding_rectangle_feature_name                 (`str`): Name of the feature containing the bounding rectangle.
			one_hot_category_feature_name                   (`str`): Name of the feature containing the one hot category.
			yolo_feature_name                               (`str`): Name of the feature where to save the yolo labels.
			normalize                                      (`bool`): Normalize rectangle coordinates ?
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( number_of_cells, int )
		assert pytools.assertions.true( number_of_cells > 0 )
		assert pytools.assertions.type_is( number_of_classes, int )
		assert pytools.assertions.true( number_of_classes > 1 )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( yolo_feature_name, str )
		assert pytools.assertions.type_is( normalize, bool )

		super( LabelerSettings, self ).__init__( instances_url_search_pattern, **kwargs )

		self._number_of_cells                 = number_of_cells
		self._number_of_classes               = number_of_classes
		self._image_feature_name              = image_feature_name
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self._one_hot_category_feature_name   = one_hot_category_feature_name
		self._yolo_feature_name               = yolo_feature_name
		self._normalize                       = normalize
	
	# def __init__ ( ... )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def number_of_cells ( self ):
		"""
		Number of cells on the width and height of the image, called S in the paper (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._number_of_cells
	
	# def number_of_cells ( self )
		
	# --------------------------------------------------

	@number_of_cells.setter
	def number_of_cells ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.true( value > 0 )

		self._number_of_cells = value
	
	# def number_of_cells ( self, value )

	# --------------------------------------------------
	
	@property
	def number_of_classes ( self ):
		"""
		Number of classes to classify, called C in the paper (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._number_of_classes
	
	# def number_of_classes ( self )
		
	# --------------------------------------------------

	@number_of_classes.setter
	def number_of_classes ( self ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.true( value > 2 )
		
		self._number_of_classes = value
	
	# def number_of_classes ( self )

	# --------------------------------------------------
	
	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )
		
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )
	
	# --------------------------------------------------
	
	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature containing the bounding rectangle (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._bounding_rectangle_feature_name
	
	# def bounding_rectangle_feature_name ( self )
		
	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )
	
	# --------------------------------------------------
	
	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature containing the one hot category (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )
		
	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, str )

		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )
	
	# --------------------------------------------------
	
	@property
	def yolo_feature_name ( self ):
		"""
		Name of the feature where to save the yolo labels (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._yolo_feature_name
	
	# def yolo_feature_name ( self )
	
	# --------------------------------------------------

	@yolo_feature_name.setter
	def yolo_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, str )

		self._yolo_feature_name = value
	
	# def yolo_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def normalize ( self ):
		"""
		Normalize rectangle coordinates ? (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._normalize

	# def normalize ( self )

	# --------------------------------------------------

	@normalize.setter
	def normalize ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, bool )

		self._normalize = value

	# def normalize ( self, value )

# class Labeler ( MapperSettings )	