# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###      CLASS INSTANCES-MAPPER-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'InstancesMapperSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = [
		'instances_url_search_pattern'
		]
	)
class InstancesMapperSettings ( MapperSettings ):
	"""
	Settings used to compose the depth of all object instances in an image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		instances_url_search_pattern = 'examples/*',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.InstancesMapperSettings` class.
		

		Arguments:
			self (`pydataset.manipulators.InstancesMapperSettings`): Instance to initialize.
			instances_url_search_pattern                    (`str`): URL search pattern used to locate object instances examples.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapperSettings )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )

		super( InstancesMapperSettings, self ).__init__( **kwargs )

		self._instances_url_search_pattern = instances_url_search_pattern

	# def __init__ ( self, ... )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################

	# --------------------------------------------------

	@property
	def instances_url_search_pattern ( self ):
		"""
		URL search pattern used to locate object instances examples (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapperSettings )

		return self._instances_url_search_pattern

	# def instances_url_search_pattern ( self )

	# --------------------------------------------------

	@instances_url_search_pattern.setter
	def instances_url_search_pattern ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, InstancesMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._instances_url_search_pattern = value

	# def instances_url_search_pattern ( self, value )

# class InstancesMapperSettings ( MapperSettings )