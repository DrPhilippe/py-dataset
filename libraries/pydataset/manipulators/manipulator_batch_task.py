# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pytools.tasks

# LOCALS
from .manipulator                     import Manipulator
from .manipulator_batch_task_settings import ManipulatorBatchTaskSettings

# ##################################################
# ###        CLASS MANIPULATOR-BATCH-TASK        ###
# ##################################################

class ManipulatorBatchTask ( pytools.tasks.Task ):
	"""
	Task used to run a manipulator on a batch of examples.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, logger, progress_tracker, parent ):
		"""
		Initializes a new instance of the `pydataset.manipulators.ManipulatorBatchTask` class.

		Arguments:
			self        (`pydataset.manipulators.ManipulatorBatchTask`): Instance to initialize.
			settings (`pydataset.manipulators.ManipulatorTaskSettings`): Settings of the manipulator task.
			logger                      (`None`/`pytools.tasks.Logger`): Logger used to log the task activity.
			progress_tracker   (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			parent                        (`None`/`pytools.tasks.Task`): Parent task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTask )
		assert pytools.assertions.type_is_instance_of( settings, ManipulatorBatchTaskSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), pytools.tasks.Task) )

		super( ManipulatorBatchTask, self ).__init__( settings, logger, progress_tracker, parent )

	# def __init__ ( self, manipulator, iterator, settings, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this manipulator batch task.

		Arguments:
			self (`pydataset.manipulators.ManipulatorTask`): The manipulator task to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTask )

		if self.logger:
			self.logger.log( 'Manipulating examples batch:' )
			self.logger.flush()

		# Go through each example
		indexes = list(range( self.settings.start_index, self.settings.end_index ))
		number_of_indexes = len(indexes)
		for i in range(number_of_indexes):
			
			# Check if the task failed or was stopped
			if not self.is_running():
				break

			# Update the task status
			self.update()

			# Get a new example from the iterator
			index   = indexes[ i ]
			example = self.parent.iterator.at( index )
			
			# Manipulate the example
			self.parent.manipulator.manipulate( example, index )

			if self.logger:
				self.logger.log( '    {}/{}', index, self.settings.end_index )
				self.logger.flush()
			
			if self.progress_tracker:
				self.progress_tracker.update( float(i)/float(number_of_indexes) )

		# for index in range( self.settings.start_index, self.settings.end_index )
		
		if self.logger:
			self.logger.log( '    Done' )
			self.logger.flush()

		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this manipulator batch task.

		Arguments:
			self (`pydataset.manipulators.ManipulatorBatchTask`): The task to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTask )

		super( ManipulatorBatchTask, self ).start()

		if self.settings.end_index < 0:
			self.settings.end_index = self.parent.iterator.count

		if self.logger:
			self.logger.log( 'Manipulator Batch Task Settings:' )
			self.logger.log( '    start index: {}', self.settings.start_index )
			self.logger.log( '      end index: {}', self.settings.end_index )
			self.logger.end_line()
			self.logger.flush()

	# def start ( self )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, logger, progress_tracker, parent, **kwargs ):
		"""
		Creates a new instance of the manipulator task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The manipulator type of task.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the manipulator task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the manipulator task.
			parent                      (`None`/`pytools.tasks.Task`): Parent task.
			**kwargs                                         (`dict`): Arguments forwarded to the manipulator task settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), pytools.tasks.Task) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, logger, progress_tracker, parent )

	# def create ( cls, logger, progress_tracker, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a manipulator task.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the manipulator task settings constructor.

		Returns:
			`pydataset.manipulators.ManipulatorTaskSettings`: The settings.
		"""
		return ManipulatorBatchTaskSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ManipulatorBatchTask ( pytools.tasks.Task )