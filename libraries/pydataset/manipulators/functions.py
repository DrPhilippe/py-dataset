# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import os
import time

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .manipulator               import Manipulator
from .manipulator_task          import ManipulatorTask
from .manipulator_task_settings import ManipulatorTaskSettings

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

def apply ( manipulator, url_search_pattern_prefix, progress_tracker=None ):
	"""
	Applies the given manipulator to examples.

	Arguments:
		manipulator       (`pydataset.manupulators.Manipulapor`): Manipulator to apply.
		url_search_pattern_prefix                        (`str`): URL search pattern prepended to the one defined in the manipulator settings.
		progress_tracker (`None`/pytools.tasks.ProgressTracker`): Progress tracker used to report overall progression.
	"""
	assert pytools.assertions.type_is_instance_of( manipulator, Manipulator )
	assert pytools.assertions.type_is( url_search_pattern_prefix, str )
	assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
	
	# Compose the url
	if ( url_search_pattern_prefix ):
		url_search_pattern = url_search_pattern_prefix + '/' + manipulator.settings.url_search_pattern
	else:
		url_search_pattern = manipulator.settings.url_search_pattern

	# Create the iterator to get the example(s)
	print( 'Creating iterator:' )
	print( '    url_search_pattern: {}'.format(url_search_pattern) )
	iterator           = pydataset.dataset.Iterator( url_search_pattern )
	number_of_examples = len( iterator )
	print( '    done' )
	print( '    found {} items'.format(number_of_examples) )

	# Check the number of examples
	if number_of_examples == 0:
		return

	# Update progress tracker
	if progress_tracker:
		progress_tracker.update( 0.0 )

	# Begin manipulation
	manipulator.begin( iterator.root_group, number_of_examples )
	
	# Manipulate examples
	for example_index in range( number_of_examples ):

		# Get the example
		example = iterator.at( example_index )

		# Manipulate the example
		manipulator.manipulate( example, example_index )

		# Update progress tracker
		if progress_tracker:
			progress_tracker.update( float(example_index)/float(number_of_examples) )

	# for example_index in range( number_of_examples )

	# End manipulation
	manipulator.end( iterator.root_group, number_of_examples )

	# Update progress tracker
	if progress_tracker:
		progress_tracker.update( 1.0 )

# def apply ( manipulator, url_search_pattern_prefix, progress_tracker )

# --------------------------------------------------

def apply_async ( manipulator, url_search_pattern_prefix='', number_of_threads=os.cpu_count(), progress_tracker=None, wait_for_application=True ):
	"""
	Applies the given mnipulator to examples.

	Arguments:
		manipulator       (`pydataset.manupulators.Manipulapor`): Manipulator to apply.
		url_search_pattern_prefix                        (`str`): URL search pattern prepended to the one defined in the manipulator settings.
		number_of_threads                                (`int`): Number of threads used to apply the manipulator.
		progress_tracker (`None`/pytools.tasks.ProgressTracker`): Progress tracker used to report overall progression.
		wait_for_application                            (`bool`): If `True` wait for the manipulation to finish.
	"""
	assert pytools.assertions.type_is_instance_of( manipulator, Manipulator )
	assert pytools.assertions.type_is( url_search_pattern_prefix, str )
	assert pytools.assertions.type_is( number_of_threads, int )
	assert pytools.assertions.true( number_of_threads > 0 )
	assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
	
	# Create the task performing the manipulation
	task = ManipulatorTask.create(
		              manipulator = manipulator,
		url_search_pattern_prefix = url_search_pattern_prefix,
		        number_of_threads = number_of_threads,
		         progress_tracker = progress_tracker,
		                   logger = pytools.tasks.file_logger( 'logs/{}.log'.format(manipulator.settings.name) )
		)

	# Create a manager for it
	manager = pytools.tasks.Manager( task )
	manager.start_tasks()

	# Await for completion
	if wait_for_application:

		# Wait for the task to complete
		manager.wait_for_stop()

		# Join thread
		manager.join_tasks()

		# Check for error
		if task.is_failed():
			raise task.error

	# if wait_for_application

	return manager

# def apply_async ( manipulator, url_search_pattern_prefix, number_of_threads, progress_tracker )