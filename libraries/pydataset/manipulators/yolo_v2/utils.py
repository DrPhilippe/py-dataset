# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

def rectangle_iou ( lhs, rhs ):
	
	# upack rectangles
	lhs_w, lhs_h = lhs[ 0 ], lhs[ 1 ]
	rhs_w, rhs_h = rhs[ 0 ], rhs[ 1 ]
	
	# compute intersection (the smallest)
	i_w, i_h = min( lhs_w, rhs_w ), min( lhs_h, rhs_h )
	
	# compute areas
	lhs_a = lhs_w * lhs_h
	rhs_a = rhs_w * rhs_h
	i_a   = i_w   * i_h
	
	# Compute IoU
	return i_a / (lhs_a + rhs_a - i_a + numpy.finfo(numpy.float32).eps )

# def rectangle_iou ( lhs, rhs )