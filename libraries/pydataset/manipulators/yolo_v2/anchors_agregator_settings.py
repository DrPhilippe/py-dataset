# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###      CLASS ANCHORS-AGREGATOR-SETTINGS      ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'AnchorsAgregatorSettings',
	   namespace = 'pydataset.manipulators.yolo_v2',
	fields_names = [
		'number_of_anchors',
		'image_feature_name',
		'bounding_rectangle_feature_name',
		'anchors_feature_name',
		'anchors_filepath'
		]
	)
class AnchorsAgregatorSettings ( AgregatorSettings ):
	"""
	Splitter data-agregator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		number_of_anchors=5,
		image_feature_name='image',
		bounding_rectangle_feature_name='bounding_rectangle',
		anchors_feature_name='yolo_v2_anchors',
		anchors_filepath='',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipualtors.yolo_v2.AnchorsAgregatorSettings` class.
		
		Arguments:
			self (`pydataset.manipualtors.AnchorsAgregatorSettings`): Instance to initialize.
			number_of_anchors                                (`int`): Number of anchors clusters to compute.
			image_feature_name                               (`str`): Name of the feature containing the image.
			bounding_rectangle_feature_name                  (`str`): Name of the feature containing the bounding rectangles.
			anchors_feature_name                             (`str`): Name of the feature where to save the anchors rectangles priors.
			anchors_filepath         (`pytools.path.FilePath`/`str`): Path of the file where to save the anchors rectangles priors.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to split.
			name               (`str`): Name of the agregator.
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_is( number_of_anchors, int )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( anchors_feature_name, str )
		assert pytools.assertions.type_in( anchors_filepath, (pytools.path.FilePath, str) )

		# Init parent class
		super( AnchorsAgregatorSettings, self ).__init__( **kwargs )

		# Init properties
		self._number_of_anchors               = number_of_anchors
		self._image_feature_name              = image_feature_name
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self._anchors_feature_name            = anchors_feature_name
		self._anchors_filepath                = pytools.path.FilePath(anchors_filepath) if isinstance( anchors_filepath, str ) else anchors_filepath

	# def __init__ ( self, category_name_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def number_of_anchors ( self ):
		"""
		Number of anchors clusters to compute (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )

		return self._number_of_anchors
	
	# def number_of_anchors ( self )

	# --------------------------------------------------

	@number_of_anchors.setter
	def number_of_anchors ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_anchors = value
	
	# def number_of_anchors ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )

		return self._image_feature_name
	
	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature containing the bounding rectangles (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )

		return self._bounding_rectangle_feature_name
	
	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def anchors_feature_name ( self ):
		"""
		Name of the feature where to save the anchors rectangles priors (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )

		return self._anchors_feature_name
	
	# def anchors_feature_name ( self )

	# --------------------------------------------------

	@anchors_feature_name.setter
	def anchors_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._anchors_feature_name = value
	
	# def anchors_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def anchors_filepath ( self ):
		"""
		Path of the file where to save the anchors rectangles priors (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )

		return self._anchors_filepath
	
	# def anchors_filepath ( self )

	# --------------------------------------------------

	@anchors_filepath.setter
	def anchors_filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregatorSettings )
		assert pytools.assertions.type_in( value, (pytools.path.FilePath, str) )

		self._anchors_filepath = pytools.path.FilePath(value) if isinstance( value, str ) else value
	
	# def anchors_filepath ( self, value )

# class AnchorsAgregatorSettings ( AgregatorSettings )