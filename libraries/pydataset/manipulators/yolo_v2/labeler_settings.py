# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .. import yolo_v1

# ##################################################
# ###              LABELER-SETTINGS              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'LabelerSettings',
	   namespace = 'pydataset.manipulators.yolo_v2',
	fields_names = [
		'anchors_feature_name'
		]
	)
class LabelerSettings ( yolo_v1.LabelerSettings ):
	"""
	Settings used to create yolo v1 labels.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		                number_of_cells = 7,
		              number_of_classes = 20,
		   instances_url_search_pattern = 'examples/*',
		           anchors_feature_name = 'yolo_v2_anchors',
		             image_feature_name = 'image',
		bounding_rectangle_feature_name = 'bounding_rectangle',
		  one_hot_category_feature_name = 'one_hot_category',
		              yolo_feature_name = 'yolo_v2',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.yolo_v1.LabelerSettings` class.
		
		The labels contain only one bounding box with associated class per cell.
		Thus the number of detectors per cell, names B in the paper, is not used here.

		Arguments:
			self (`pydataset.manipulators.yolo_v1.LabelerSettings`): Instance to initialize.
			number_of_cells                                 (`int`): Number of cells on the width and height of the image, called S in the paper.
			number_of_classes                               (`str`): Number of classes to classify, called C in the paper.
			instances_url_search_pattern                    (`str`): URL used to find instances with respect to the current example that contains the image.
			anchors_feature_name                            (`str`): Name of the feature containing the YOLO v2 bounding rectangles anchors.
			image_feature_name                              (`str`): Name of the feature containing the image.
			bounding_rectangle_feature_name                 (`str`): Name of the feature containing the bounding rectangle.
			one_hot_category_feature_name                   (`str`): Name of the feature containing the one hot category.
			yolo_feature_name                               (`str`): Name of the feature where to save the yolo labels.
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( number_of_cells, int )
		assert pytools.assertions.true( number_of_cells > 0 )
		assert pytools.assertions.type_is( number_of_classes, int )
		assert pytools.assertions.true( number_of_classes > 1 )
		assert pytools.assertions.type_is( instances_url_search_pattern, str )
		assert pytools.assertions.type_is( anchors_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( yolo_feature_name, str )

		super( LabelerSettings, self ).__init__(
			number_of_cells,
			number_of_classes,
			instances_url_search_pattern,
			image_feature_name,
			bounding_rectangle_feature_name,
			one_hot_category_feature_name,
			yolo_feature_name,
			**kwargs
			)

		self._anchors_feature_name = anchors_feature_name
	
	# def __init__ ( ... )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def anchors_feature_name ( self ):
		"""
		Name of the feature containing the YOLO v2 bounding rectangles anchors (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )

		return self._anchors_feature_name
	
	# def anchors_feature_name ( self )
		
	# --------------------------------------------------

	@anchors_feature_name.setter
	def anchors_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, LabelerSettings )
		assert pytools.assertions.type_is( value, str )

		self._anchors_feature_name = value
	
	# def anchors_feature_name ( self, value )

# class LabelerSettings ( MapperSettings )	