# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import pyclustering.cluster.center_initializer
import pyclustering.cluster.kmeans
import pyclustering.utils.metric

# INTERNALS
import pydataset
import pytools

# LOCALS
from ..agregator                 import Agregator
from .anchors_agregator_settings import AnchorsAgregatorSettings

# ##################################################
# ###          CLASS ANCHORS-AGREGATOR           ###
# ##################################################

class AnchorsAgregator ( Agregator ):
	"""
	Base class for agregators.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.yolo_v2.AnchorsAgregator` class.

		Arguments:
			self             (`pydataset.manipulators.yolo_v2.AnchorsAgregator`): Instance to initialize.
			settings (`pydataset.manipulators.yolo_v2.AnchorsAgregatorSettings`): Anchors agregator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregator )
		assert pytools.assertions.type_is_instance_of( settings, AnchorsAgregatorSettings )

		super( AnchorsAgregator, self ).__init__( settings )

		self.anchors = None
		self.rectangles = None

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before bounding rectangles agregation starts.

		Arguments:
			self (`pydataset.manipualtors.AnchorsAgregator`): Anchors agregator used to agregate the example.
			root_group           (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                       (`int`): Number of examples to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self.rectangles = []

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.AnchorsAgregator`): Anchors agregator used to agregate the example.
			example            (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the rectangle
		image              = example.get_feature( self.settings.image_feature_name ).value
		bounding_rectangle = example.get_feature( self.settings.bounding_rectangle_feature_name ).value.ravel()
		
		# Fetch image dimensions
		image_height = float( image.shape[ 0 ] )
		image_width  = float( image.shape[ 1 ] )

		# Get bounding rectangle dimensions
		rect_width  = bounding_rectangle[ 2 ]
		rect_height = bounding_rectangle[ 3 ]

		rect = numpy.asarray([
			rect_width  / image_width,
			rect_height / image_height
			])

		# Add the width and height of the bounding rectangle to the list
		self.rectangles.append( rect )
		
	# def agregate ( self, example )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after bounding rectangles agregaion completed.
		
		The actual K-Mean clustering is done in this method.

		Arguments:
			self (`pydataset.manipualtors.AnchorsAgregator`): Anchors agregator used to manipulate the example.
			root_group           (`pydataset.dataset.Group`): Group containing the examples that have been manipulated.
			number_of_examples                       (`int`): Number of examples manipulated.
		"""
		assert pytools.assertions.type_is_instance_of( self, AnchorsAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		# Usefull value
		K = self.settings.number_of_anchors

		# Convert the agregated bounding rectangles list into a numpy ndarray
		self.rectangles = numpy.asarray( self.rectangles, dtype=numpy.float32 )
		self.rectangles = numpy.reshape( self.rectangles, (number_of_examples, 2) )

		# Initialize k-mean ckustering centers
		initializer  = pyclustering.cluster.center_initializer.kmeans_plusplus_initializer( self.rectangles, K )
		# initializer  = pyclustering.cluster.center_initializer.random_center_initializer( self.rectangles, K )
		anchors_init = initializer.initialize()

		# Custom IoU metric
		def rectangle_iou_metric_fn ( lhs, rhs ):
			# upack rectangles
			lhs_w, lhs_h = lhs[ 0 ], lhs[ 1 ]
			rhs_w, rhs_h = rhs[ 0 ], rhs[ 1 ]
			# compute intersection (the smallest)
			i_w, i_h = min( lhs_w, rhs_w ), min( lhs_h, rhs_h )
			# compute areas
			lhs_a = lhs_w * lhs_h
			rhs_a = rhs_w * rhs_h
			i_a   = i_w   * i_h
			# Compute IoU
			iou = i_a / (lhs_a + rhs_a - i_a + numpy.finfo(numpy.float32).eps )
			# metric
			return 1. - iou

		# Instantiate the metric handle
		rectangle_iou_metric = pyclustering.utils.metric.distance_metric(
			pyclustering.utils.metric.type_metric.USER_DEFINED,
			func=rectangle_iou_metric_fn
			)

		# Run K-Mean clustering
		kmean = pyclustering.cluster.kmeans.kmeans( self.rectangles, anchors_init, metric=rectangle_iou_metric )
		kmean.process()

		# Get centers
		anchors = kmean.get_centers()
		anchors = numpy.asarray( anchors, dtype=numpy.float32 )
		anchors = numpy.reshape( anchors, (self.settings.number_of_anchors, 2) )
		#anchors = numpy.sort( anchors, axis=0 )
		self.anchors = anchors

		# Save the anchors as a feature
		if self.settings.anchors_feature_name:
			root_group.create_or_update_feature(
				     feature_name = self.settings.anchors_feature_name,
				feature_data_type = pydataset.dataset.NDArrayFeatureData,
				            value = self.anchors
				)

		# Save them in a file
		if self.settings.anchors_filepath:
			numpy.savetxt( str(self.settings.anchors_filepath), self.anchors, fmt='%9.4f' )

		import matplotlib.pyplot as plt
		plt.hist2d( self.rectangles[:,0], self.rectangles[:,1], 50, density=True, facecolor='red' )
		plt.xlabel( 'Width' )
		plt.ylabel( 'Height' )
		plt.title( 'Areas Histogram' )
		plt.show()

		# areas = numpy.prod( self.rectangles, axis=-1 )
		# plt.hist( areas, 50, density=True, facecolor='red' )
		# plt.xlabel( 'Areas' )
		# plt.ylabel( 'Density' )
		# plt.title( 'Areas Histogram' )
		# plt.show()

		# aspects = numpy.divide.reduce( self.rectangles, axis=-1 )
		# plt.hist( aspects, 50, density=True, facecolor='red' )
		# plt.xlabel( 'Aspects Ratios' )
		# plt.ylabel( 'Density' )
		# plt.title( 'Aspects Ratios Histogram' )
		# plt.show()


	# def end ( self, root_group, number_of_examples )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for an agregator of type `pydataset.manipulators.yolo_v2.AnchorsAgregator`.

		Arguments:
			**kwargs (`dict`): Arguments forwarded to the `pydataset.manipulators.yolo_v2.AnchorsAgregatorSettings` constructor.

		Returns:
			`pydataset.manipulators.yolo_v2.AnchorsAgregatorSettings`: Settings for the manipulator of type `pydataset.manipulators.yolo_v2.AnchorsAgregator`.
		"""
		
		return pydataset.manipulators.yolo_v2.AnchorsAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class AnchorsAgregator ( Agregator )