# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset
import pytools.assertions

# LOCALS
from .. import yolo_v1
from .  import utils
from .labeler_settings import LabelerSettings

# ##################################################
# ###               CLASS LABELER                ###
# ##################################################

class Labeler ( yolo_v1.Labeler ):
	"""
	Mapper used to create YOLO v2 labels.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.yolo_v2.Labeler` class.

		Arguments:
			self             (`pydataset.manipulators.yolo_v2.Labeler`): Labeler instance to initialize.
			settings (`pydataset.manipulators.yolo_v2.LabelerSettings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, Labeler )
		assert pytools.assertions.type_is_instance_of( settings, LabelerSettings )

		super( Labeler, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_yolo_labels ( self, example ):
		"""
		Creates the yolo labels for the given example.

		Arguments:
			self (`pydataset.manipulators.yolo_v2.Labeler`): Labeler instance.
			example           (`pydataset.dataset.Example`): Example for which to create the control points.

		Returns:
			`numpy.ndarray`: The labels
		"""
		assert pytools.assertions.type_is_instance_of( self, Labeler )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Usefull values
		S = self.settings.number_of_cells
		C = self.settings.number_of_classes

		# Get the image and fetch its dimensions
		image        = example.get_feature( self.settings.image_feature_name ).value
		image_width  = float( image.shape[ 1 ] )
		image_height = float( image.shape[ 0 ] )

		# Compute cell width and height
		cells_width  = image_width  / float(S)
		cells_height = image_height / float(S)

		# Get the anchor boxes
		anchors = example.get_feature( self.settings.anchors_feature_name ).value
		B       = anchors.shape[0]

		# Initialize labels
		yolo_labels = numpy.zeros( [S, S, B*(5+C)], dtype=numpy.float32 )
		
		# Cell occupancy buffer
		# We fill this buffer with the iou of the assigned rectangle
		# with respect to the anchor in the box.
		cell_occupancy = numpy.zeros( [S, S, B], dtype=numpy.float32 )

		# Iterate object instances
		for instance in self.create_instances_iterator( example ):
			
			# Get features
			bounding_rectangle = instance.get_feature( self.settings.bounding_rectangle_feature_name ).value
			one_hot_category   = instance.get_feature( self.settings.one_hot_category_feature_name   ).value.astype( numpy.float32 )

			# Convert rectangle
			bounding_rectangle = pydataset.data.RectangleData.from_ndarray( bounding_rectangle )
			
			# Get bounding rectangle center
			center_x, center_y = bounding_rectangle.center

			# Compute the indexes of the cell if falls into
			cell_index_x = int( numpy.floor( center_x / cells_width  ) )
			cell_index_y = int( numpy.floor( center_y / cells_height ) )
			
			# Find out which detector+anchors should predict this object.
			best_anchor_index = None
			best_anchor_iou = 0
			for b in range(B):

				# Get the current anchor
				anchor = anchors[ b ]
				anchor = numpy.reshape( anchor, (2,) ).astype( numpy.float32 )
				
				# Compute the IoU between anchor and bounding rectangle
				iou = utils.rectangle_iou( anchor, [bounding_rectangle.height, bounding_rectangle.width] )

				# If the IoU is better, use this anchor
				if iou > best_anchor_iou:
					best_anchor_iou   = iou
					best_anchor_index = b
				# if iou > best_anchor_iou

			# for b in range(B):

			# If the cell we want to assign the rectangle to is empty
			# Or the rectangle has a better IoU with it, assign it
			if cell_occupancy[ cell_index_y, cell_index_x, b ] < best_anchor_iou:
			
				# Flag the cell as occupied using the rectangle area
				cell_occupancy[ cell_index_y, cell_index_x, b ] = best_anchor_iou

				# Create the labels
				labels = numpy.asarray([
					# Rectangle coordinates
					float( bounding_rectangle.x1 ),
					float( bounding_rectangle.y1 ),
					float( bounding_rectangle.x2 ),
					float( bounding_rectangle.y2 ), 
					# Confidence
					1.0
					] )

				# Add the object category
				labels = numpy.append( labels, one_hot_category )

				# Shifted index of the detector in the cell
				box_start = (b+0)*(5+C)
				box_end   = (b+1)*(5+C)

				# Write the cell labels
				yolo_labels[ cell_index_y, cell_index_x, box_start:box_end ] = labels

			# if cell_occupancy[ cell_index_y, cell_index_x, b ] < best_anchor_iou:
			
		# for instance_example in pydataset.dataset.Iterator( url )

		return yolo_labels

	# def create_yolo_labels ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		assert pytools.assertions.type_is( cls, type )

		return LabelerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Labeler ( yolo_v1.Labeler )