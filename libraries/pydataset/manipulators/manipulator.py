# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .manipulator_settings import ManipulatorSettings

# ##################################################
# ###             CLASS MANIPULATOR              ###
# ##################################################

class Manipulator:
	"""
	Base class for augmenters, fileters and mappers.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the manipulator class.

		Arguments:
			self             (`pydataset.manipulators.Manipulator`): Instance to initialize.
			settings (`pydataset.manipulators.ManipulatorSettings`): Manipulator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( settings, ManipulatorSettings )

		self._settings       = settings
		self._dst_group      = None
		self._dst_group_lock = threading.Lock()
		self._src_group      = None

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def settings ( self ):
		"""
		Settings of this manipulator (`pydataset.manipulators.ManipulatorSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )

		return self._settings
	
	# def settings ( self )

	# --------------------------------------------------

	@settings.setter
	def settings ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( value, ManipulatorSettings )

		self._settings = value
	
	# def settings ( self, value )
	
	# --------------------------------------------------

	@property
	def dst_group ( self ):
		"""
		Group where augmentations are create (`pydataset.dataset.Group`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )

		return self._dst_group
	
	# def dst_group ( self )

	# --------------------------------------------------

	@property
	def src_group ( self ):
		"""
		Group where examples are mapped from (`pydataset.dataset.Group`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )

		return self._src_group
	
	# def src_group ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _get_or_create_example_parent_in_dst_group ( self, example, group=None,  ):
		
		# Split the url of the example and the one of the search pattern
		url_search_pattern_parts = self.settings.url_search_pattern.split( '/' )
		example_url_parts        = example.url.split( '/' )

		# Use the length to find the index to align them
		alignment_index = len(example_url_parts) - len(url_search_pattern_parts)
			
		# Create intermediate groups
		if group is None:
			group = self._dst_group
		
		for index in range( alignment_index, len(example_url_parts)-2, 2 ):

			item_family = example_url_parts[ index     ]
			item_name   = example_url_parts[ index + 1 ]

			if item_family != 'groups':
				break

			if group.contains_group( item_name ):
				group = group.get_group( item_name )

			else:
				ref_group_url = example_url_parts[ 1 : index + 2 ]
				ref_group_url = '/'.join( ref_group_url )
				ref_group     = example.dataset.get( ref_group_url )

				# Create the destination group
				group = group.create_group( item_name )

				# Copy the features of the source group
				group.copy_features( ref_group )
		
		# for index in range( alignment_index, len(example_url_parts), 2 )

		return group

	# def _get_or_create_augmentation_parent ( self, example )

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.Manipulator`): Manipulator used to manipulate the example.
			root_group      (`pydataset.dataset.Group`): Group containing the examples that will be manipulated.
			number_of_examples                  (`int`): Number of examples to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		# Save the SRC group
		self._src_group = root_group

		# Place result in a different group ?
		if self.settings.dst_group_url:

			# Get the dataset the augmenter works on
			dataset = root_group.dataset

			# If the dataset contains the dst group get it
			if dataset.contains( self.settings.dst_group_url ):

				# Get the group
				item = dataset.get( self.settings.dst_group_url )
				
				# Check that it is a group
				if not isinstance( item, pydataset.dataset.Group ):

					# Raise an error
					raise RuntimeError( 'The destination group URL "{}" point to a "", Group is required !'.format(
						self.settings.dst_group_url,
						type(item).__name__
						))

			# If not, create it
			else:

				# Create it
				item = dataset.create( self.settings.dst_group_url )
				
				# Check that it is a group
				if not isinstance( item, pydataset.dataset.Group ):

					# Raise an error
					raise RuntimeError( 'The destination group URL "{}" point to a "", Group is required !'.format(
						self.settings.dst_group_url,
						type(item).__name__
						))

				# Copy the features of the src group in the dst group
				item.copy_features( root_group )

			# if dataset.contains( self.settings.dst_group_url )
			
			# The group is OK
			self._dst_group = item

		else:
			self.settings.dst_group_url = root_group.url
			self._dst_group             = root_group

		# if self.settings.dst_group_url

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after manipulation append.

		Arguments:
			self (`pydataset.manipualtors.Manipulator`): Manipulator used to manipulate the example.
			root_group      (`pydataset.dataset.Group`): Group containing the examples that have been manipulated.
			number_of_examples                  (`int`): Number of examples manipulated.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

	# def end ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this manipulator.

		Arguments:
			self (`pydataset.manipualtors.Manipulator`): Manipulator used to manipulate the example.
			example       (`pydataset.dataset.Example`): Example to manipulate.
			index                               (`int`): Index of the example being manipulated.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )
		
		raise NotImplementedError()

	# def manipulate ( self, example, index )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, **kwargs ):
		"""
		Creates a new manipulator instance.

		Arguments:
			cls      (`type`): Type of the manipulator to create.
			**kwargs (`dict`): Arguments forwarded to the settings of the manipulator.

		Returns:
			`pydataset.manipulators.Manipulator`: Manipulator of type `cls`.
		"""

		# Create the settings
		settings = cls.create_settings( **kwargs )

		# Create the manipulator
		return cls( settings )

	# def create ( cls, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the manipulator type `cls`.

		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings of the manipulator.

		Returns:
			`pydataset.manipulators.ManipulatorSettings`: Settings for the manipulator of type `cls`.
		"""
		
		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )

# class Manipulator