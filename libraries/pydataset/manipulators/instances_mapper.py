# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pydataset
import pytools

# LOCALS
from .mapper                    import Mapper
from .instances_mapper_settings import InstancesMapperSettings

# ##################################################
# ###           CLASS INSTANCES-MAPPER           ###
# ##################################################

class InstancesMapper ( Mapper ):
	"""
	Base class for instances mappers.

	An instances mapper processes object instances examples children of an example describing an image.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.InstancesMapper`): Instances mapper instance to initialize.
			settings (`pydataset.manipualtors.InstancesMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapper )
		assert pytools.assertions.type_is_instance_of( settings, InstancesMapperSettings )

		super( InstancesMapper, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def create_instances_iterator ( self, example ):
		"""
		Creates the instances iterator.

		Arguments:
			self (`pydataset.manipualtors.InstancesMapper`): Mapper used to map the example.
			example           (`pydataset.dataset.Example`): Example being mapped.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Compose the absolute URL search pattern
		url = example.absolute_url + '/' + self.settings.instances_url_search_pattern
		
		# Create the iterator
		iterator = pydataset.dataset.Iterator( url )

		# Check it
		if len(iterator) == 0:
			raise ValueError( "Could not find object instances examples using url '{}'".format(url) )

		# Return it
		return iterator
	
	# def create_instances_iterator ( self, example )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this instances mapper.

		Arguments:
			self (`pydataset.manipualtors.InstancesMapper`): Mapper used to map the example.
			example           (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, InstancesMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		raise NotImplementedError()
	
	# def map ( self, example )
	
# class InstancesMapper ( Mapper )