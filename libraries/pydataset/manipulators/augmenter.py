# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .augmenter_settings import AugmenterSettings
from .manipulator        import Manipulator

# ##################################################
# ###              CLASS AUGMENTER               ###
# ##################################################

class Augmenter ( Manipulator ):
	"""
	Base class for example mappers.

	A mappers modifies an existing example.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.Augmenter` class.
		
		Arguments:
			self             (`pydataset.manipualtors.Augmenter`): Augmenter instance to initialize.
			settings (`pydataset.manipualtors.AugmenterSettings`): Settings of the augmenter.
		"""
		assert pytools.assertions.type_is_instance_of( self, Augmenter )
		assert pytools.assertions.type_is_instance_of( settings, AugmenterSettings )

		super( Augmenter, self ).__init__( settings )
		
	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def augment ( self, example, index ):
		"""
		Augment the given example using this augmenter.

		Arguments:
			self (`pydataset.manipualtors.Augmenter`): Augmenter used to augment the example.
			example     (`pydataset.dataset.Example`): Example to augment.
			index                             (`int`): Index of the example being augmented.
		"""
		assert pytools.assertions.type_is_instance_of( self, Augmenter )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )

		raise NotImplementedError()
	
	# def augment ( self, example )
	
	# --------------------------------------------------

	def create_augmentation ( self, example, augmentation_type='same', name_format='augmentation_{:02d}', copy_what=['all'], *args ):
		"""
		Creates a copy of the given example from the source group into the destination group.
		
		Depending on `type` the new copy can be an example or a group.

		Arguments:
			self (`pydataset.manipulators.Augmenter`): Augmenter.
			example     (`pydataset.dataset.Example`): Example to copy.
			augmentation_type                 (`str`): `'example'` or `'group'`.
			name_format                       (`str`): Name format used to create augmentations names.
		
		Returns:
			`pydataset.dataset.Example`: The augmentation.
		"""
		assert pytools.assertions.type_is_instance_of( self, Augmenter )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( augmentation_type, str )
		assert pytools.assertions.value_in( augmentation_type, ['example', 'group', 'same'] )
		assert pytools.assertions.type_is( name_format, str )
		assert pytools.assertions.type_is( copy_what, list )
		for item in copy_what:
			assert pytools.assertions.value_in( item, ['all', 'features', 'examples', 'groups'] )

		return self.create_augmentations( example, 1, augmentation_type, name_format, copy_what, *args )[ 0 ]

	# def create_augmentation ( self, example )

	# --------------------------------------------------

	def create_augmentations ( self, example_or_group, number_of_augmentations, augmentation_type='example', name_format='augmentation_{:02d}', copy_what=['all'], *args ):
		"""
		Creates copy of the given example from the source group into the destination group.
		
		Depending on `type` the new copy can be examples or groups.

		Arguments:
			self      (`pydataset.manipulators.Augmenter`): Augmenter.
			example_or_group (`pydataset.dataset.Example`): Example or group copy.
			augmentation_type                      (`str`): `'example'` or `'group'`.
			name_format                            (`str`): Name format used to create augmentations names.

		Returns:
			`list` of `pydataset.dataset.Example`: The augmentations.
		"""
		assert pytools.assertions.type_is_instance_of( self, Augmenter )
		assert pytools.assertions.type_is_instance_of( example_or_group, pydataset.dataset.Example )
		assert pytools.assertions.type_is( number_of_augmentations, int )
		assert pytools.assertions.type_is( augmentation_type, str )
		assert pytools.assertions.value_in( augmentation_type, ['example', 'group', 'same'] )
		assert pytools.assertions.type_is( name_format, str )
		assert pytools.assertions.type_is( copy_what, list )
		for item in copy_what:
			assert pytools.assertions.value_in( item, ['all', 'features', 'examples', 'groups'] )
		
		# Init result
		augmentations = []

		# Lock the destination group
		self._dst_group_lock.acquire()

		# Get/create the augmentations parent group
		parent_group = self._get_or_create_example_parent_in_dst_group( example_or_group )
		
		# Create the augmentations
		for i in range( number_of_augmentations ):

			# Create group augmentation
			if isinstance( example_or_group, pydataset.dataset.Group ):
				
				# Create a new exampe ?
				if augmentation_type == 'example':

					raise RuntimeError( "Cannot create an example augmentation of a group, sub-groups and sub-examples would be lost!" )

					# # Create a new exampe
					# augmentation_name = name_format.format( *args, parent_group.number_of_examples )
					# augmentation      = parent_group.create_example( augmentation_name )

					# # Copy the src example
					# augmentation.copy( example_or_group )

				# Create a new group ?
				elif augmentation_type == 'group' or augmentation_type == 'same':					
					
					# Create a new exampe
					augmentation_name = name_format.format( parent_group.number_of_groups, *args )
					augmentation      = parent_group.create_group( augmentation_name )

					# Copy the src example
					if 'all' in copy_what:
						augmentation.copy( example_or_group )
					else:
						if 'features' in copy_what:
							augmentation.copy_features( example_or_group )
						if 'examples' in copy_what:
							augmentation.copy_examples( example_or_group )
						if 'groups' in copy_what:
							augmentation.copy_groups( example_or_group )


				else:
					raise RuntimeError( "Invalid augmentation type {}, this should not have happend!".format(
						augmentation_type
						))
				
				# if augmentation_type == 'example'

			# Create example augmentation
			elif isinstance( example_or_group, pydataset.dataset.Example ):

				# Create a new exampe ?
				if augmentation_type == 'example' or augmentation_type == 'same':

					# Create a new exampe
					augmentation_name = name_format.format( parent_group.number_of_examples, *args )
					augmentation      = parent_group.create_example( augmentation_name )

					# Copy the src example
					augmentation.copy( example_or_group )

				# Create a new group ?
				elif augmentation_type == 'group':					
					
					# Create a new exampe
					augmentation_name = name_format.format( parent_group.number_of_groups, *args )
					augmentation      = parent_group.create_group( augmentation_name )

					# Copy the src example
					augmentation.copy( example_or_group )

				else:
					raise RuntimeError( "Invalid augmentation type {}, this should not have happend!".format(
						augmentation_type
						))
				
				# if augmentation_type == 'example' or augmentation_type == 'same'

			else:
				raise RuntimeError( 'Unsupported item type: {}'.format(type(example)) )
			
			# if isinstance( example_or_group, pydataset.dataset.Group )

			# Save the augmentation
			augmentations.append( augmentation )

		# for i in range( number_of_augmentations )

		# Unlock the destination group
		self._dst_group_lock.release()

		# Return the augmentations
		return augmentations

	# def create_augmentations ( ... )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after augmentation append.

		Arguments:
			self (`pydataset.manipualtors.Augmenter`): Augmenter used to augment the example.
			root_group    (`pydataset.dataset.Group`): Group containing the examples that where augmented.
			number_of_examples                (`int`): Number of examples that where augmented.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

	# def end ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this augmenter.

		Arguments:
			self (`pydataset.manipualtors.Augmenter`): Augmenter used to augment the example.
			example     (`pydataset.dataset.Example`): Example to augment.
			index                             (`int`): Index of the example being augmented.
		"""
		assert pytools.assertions.type_is_instance_of( self, Manipulator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )
		
		# Augment the example
		self.augment( example, index )

	# def manipulate ( self, example )

# class Augmenter ( Manipulator )