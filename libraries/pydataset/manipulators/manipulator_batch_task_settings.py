# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###   CLASS MANIPULATOR-BATCH-TASK-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ManipulatorBatchTaskSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = [
		'start_index',
		'end_index'
		]
	)
class ManipulatorBatchTaskSettings ( pytools.tasks.TaskSettings ):
	"""
	Manipualtors batch task settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, start_index=0, end_index=-1, name='Manipulator Batch Task' ):
		"""
		Initializes a new instance of the `pydataset.manipualtors.ManipulatorBatchTaskSettings` class.
		
		Arguments:
			self (`pydataset.manipualtors.ManipulatorBatchTaskSettings`): Instance to initialize.
			start_index                                          (`int`): Index of the first example to manipulate.
			end_index                                            (`int`): Index after the last example to manipulate.
			name                                                 (`str`): Name of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTaskSettings )
		assert pytools.assertions.type_is( start_index, int )
		assert pytools.assertions.type_is( end_index, int )
		assert pytools.assertions.type_is( name, str )

		super( ManipulatorBatchTaskSettings, self ).__init__( name )

		self._start_index = start_index
		self._end_index   = end_index

	# def __init__ ( self, start_index, end_index, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def start_index ( self ):
		"""
		Index of the first example to manipulate (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTaskSettings )

		return self._start_index
	
	# def start_index ( self )

	# --------------------------------------------------

	@start_index.setter
	def start_index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTaskSettings )
		assert pytools.assertions.type_is( value, int )

		self._start_index = value
	
	# def start_index ( self, value )
	
	# --------------------------------------------------

	@property
	def end_index ( self ):
		"""
		Name of the manipulator (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTaskSettings )

		return self._end_index
	
	# def end_index ( self )

	# --------------------------------------------------

	@end_index.setter
	def end_index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ManipulatorBatchTaskSettings )
		assert pytools.assertions.type_is( value, int )

		self._end_index = value
	
	# def end_index ( self, value )

# class ManipulatorBatchTaskSettings ( pytools.tasks.TaskSettings )