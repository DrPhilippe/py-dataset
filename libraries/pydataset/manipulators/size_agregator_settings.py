# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .agregator_settings import AgregatorSettings

# ##################################################
# ###       CLASS SIZE-AGREGATOR-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SizeAgregatorSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'bounding_rectangle_feature_name',
		'output_directory_path',
		'filename_format'
		]
	)
class SizeAgregatorSettings ( AgregatorSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		bounding_rectangle_feature_name = 'bounding_rectangle',
		          category_feature_name = 'category_name',
		          output_directory_path = 'sizes',
		                filename_format = 'sizes_{}.csv',
		**kwargs
		):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )
		assert pytools.assertions.type_is( category_feature_name, str )
		assert pytools.assertions.type_is_instance_of( output_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( filename_format, str )
		assert pytools.assertions.true( filename_format.endswith('.csv') )

		super( SizeAgregatorSettings, self ).__init__( **kwargs )

		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name
		self._category_feature_name           = category_feature_name
		self._output_directory_path           = pytools.path.DirectoryPath.ensure( output_directory_path )
		self._filename_format                 = filename_format

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )

		return self._category_feature_name

	# def category_feature_name ( self )

	# --------------------------------------------------

	@category_feature_name.setter
	def category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_feature_name = value
	
	# def category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def output_directory_path ( self ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )

		return self._output_directory_path

	# def output_directory_path ( self )

	# --------------------------------------------------

	@output_directory_path.setter
	def output_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._output_directory_path = pytools.path.DirectoryPath.ensure( value )

	# def output_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def filename_format ( self ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )

		return self._filename_format

	# def filename_format ( self )

	# --------------------------------------------------

	@filename_format.setter
	def filename_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SizeAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.true( '_' in value )
		assert pytools.assertions.true( value.endswith('.csv') )

		self._filename_format = value

	# def filename_format ( self, value )
	
# class SizeAgregatorSettings ( AgregatorSettings )