# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from .manipulator_settings import ManipulatorSettings

# ##################################################
# ###    CLASS MANIPULATION-SEQUENCE-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ManipulationSequenceSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = []
	)
class ManipulationSequenceSettings ( ManipulatorSettings ):
	"""
	Manipualtors settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, url_search_pattern='', dst_group_url='', name='' ):
		"""
		Initializes a new instance of the manipulator sequence settings class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to manipulate.

		Arguments:
			self (`pydataset.manipualtors.ManipulationSequenceSettings`): Instance to initialize.
			url_search_pattern                                   (`str`): URL used to search for example(s) to manipulate.
			dst_group_url                                        (`str`): URL of the group where to save the manipulated examples.
			name                                                 (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequenceSettings )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( ManipulationSequenceSettings, self ).__init__( url_search_pattern, dst_group_url, name )

	# def __init__ ( self, url_search_pattern, dst_group_url, name )

# class ManipulationSequenceSettings ( ManipulatorSettings )