# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools
import pydataset

# LOCALS
from ..agregator                         import Agregator
from .object_location_agregator_settings import ObjectLocationAgregatorSettings

# ##################################################
# ###      CLASS OBJECT-LOCATION-AGREGATOR       ###
# ##################################################

class ObjectLocationAgregator ( Agregator ):
	"""
	Image object location agregator.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Agregates on-screen object locations.
		
		Arguments:
			self             (`pydataset.manipualtors.pose.ObjectLocationAgregator`): Object locations agregator instance to initialize.
			settings (`pydataset.manipualtors.pose.ObjectLocationAgregatorSettings`): Settings of the object locations  agregator.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )
		assert pytools.assertions.type_is_instance_of( settings, ObjectLocationAgregatorSettings )

		# Init parent class
		super( ObjectLocationAgregator, self ).__init__( settings )

		# Init per-category agregated object locations
		self._locations = {}
		
	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def locations ( self ):
		"""
		Agregated per-category object locations.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )

		return self._locations
	
	# def locations ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.pose.ObjectLocationAgregator`): Agregator used to agregate object locations.
			example                        (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get features used to compute and agregate the point of view
		K              = example.get_feature( self.settings.camera_matrix_feature_name ).value
		category_index = example.get_feature( self.settings.category_index_feature_name ).value
		R              = example.get_feature( self.settings.rotation_matrix_feature_name ).value
		t              = example.get_feature( self.settings.translation_vector_feature_name ).value
		
		# FOR T-LESS WE OVERRIDE THE K MATRIX TO IGNORE CROPPING
		# K = numpy.asarray( [1076.74064739, 0.0, 373.98264967, 0.0, 1075.17825536, 241.59181836, 0.0, 0.0, 1.0], dtype=numpy.float32 )
		# K = numpy.reshape( K, [3, 3] )
		
		# Convert rotation to OpenCV Euler
		r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )

		# Create the camera space origine
		origine = numpy.zeros( [1, 3], dtype=numpy.float32 )
	
		# Project object scape origine onto image plane
		location, j = cv2.projectPoints( origine, r, t, K, numpy.zeros(5) )
		location = numpy.reshape( location, [1, 2] )

		# Agregate the point
		if category_index not in self._locations:
			self._locations[ category_index ] = location
		else:
			self._locations[ category_index ] = numpy.vstack( (self._locations[ category_index ], location) )

	# def agregate ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before agregation starts.

		Arguments:
			self (`pydataset.manipualtors.pose.ObjectLocationAgregator`): Agregator used to agregate object locations.
			root_group                       (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                                   (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self._locations = {}

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after manipulation append.

		Arguments:
			self (`pydataset.manipualtors.pose.ObjectLocationAgregator`): Agregator used to agregate object locations.
			root_group                       (`pydataset.dataset.Group`): Group containing the examples that have been agregated.
			number_of_examples                                   (`int`): Number of examples agregated.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self.save()
		
	# def end ( self, root_group, number_of_examples )
	
	# --------------------------------------------------

	def reload ( self ):
		"""
		Reloads any saved object locations.

		Arguments:
			self (`pydataset.manipulators.pose.ObjectLocationAgregator`): Object locations agregator
		"""
		# Reset the points dictionary
		self._locations = {}

		# Filename prefix
		prefix = self.settings.filename_format.split( '_' )[ 0 ]

		# Go though the output directory contents
		for path in self.settings.output_directory_path.list_contents():
			
			if path.is_a_file() and path.extension() == '.csv' and path.filename().startswith( prefix+'_' ):

				index = path.filename().split( '_' )[ 1 ]
				index = int( index )
				points = numpy.loadtxt( str(path),
					    dtype = numpy.float32,
					delimiter = ',',
					 skiprows = 1
					)
				self._locations[ index ] = points

			# if ...
		
		# for ...

	# def reload ( self )

	# --------------------------------------------------

	def save ( self ):
		"""
		Saves the agregated object locations.

		Arguments:
			self (`pydataset.manipulators.pose.ObjectLocationAgregator`): Object locations agregator
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregator )

		# Create output directory if necessary
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()

		# Save the points of views of each object
		for category_index in self._locations.keys():

			# Fetch points
			locations = self._locations[ category_index ]

			# Create the path of the file where to save the points of views
			filepath = self.settings.output_directory_path + pytools.path.FilePath.format( self.settings.filename_format, category_index )
			
			# Save location of this category
			numpy.savetxt(
				str(filepath),
				locations,
				delimiter=',',
				header='u,v',
				comments='',
				fmt='%010.4f'
				)
		
		# for ...

	# def save ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by an object location agregator.

		Arguments:
			cls               (`type`): Object location agregator type.
			**kwargs (`str`s to `any`): Forwarded to the settings constructor.

		Returns:
			`pydataset.manipulators.pose.ObjectLocationAgregatorSettings`: The settings.
		"""
		return ObjectLocationAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ObjectLocationAgregator ( Agregator )