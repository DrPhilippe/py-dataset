# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pydataset.data
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                       import Mapper
from .point_of_view_mapper_settings import PointOfViewMapperSettings

# ##################################################
# ###         CLASS POINT-OF-VIEW-MAPPER         ###
# ##################################################

class PointOfViewMapper ( Mapper ):
	"""
	Mapper that computes the position of the camera viewing the object in model space.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Computes the position in model space of the camera viewing the object.
		
		Arguments:
			self             (`pydataset.manipualtors.pose.PointOfViewMapper`): Point-Of-View mapper instance to initialize.
			settings (`pydataset.manipualtors.pose.PointOfViewMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewMapper )
		assert pytools.assertions.type_is_instance_of( settings, PointOfViewMapperSettings )

		super( PointOfViewMapper, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.pose.PointOfViewMapper`): Mapper used to map the example.
			example                  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get features
		rotation_feature    = example.get_feature( self.settings.rotation_matrix_feature_name )
		translation_feature = example.get_feature( self.settings.translation_vector_feature_name )
			
		# Check features
		assert pytools.assertions.type_is_instance_of( rotation_feature.data, pydataset.dataset.NDArrayFeatureData )
		assert pytools.assertions.type_is_instance_of( translation_feature.data, pydataset.dataset.NDArrayFeatureData )

		# Get features values
		R = rotation_feature.value
		t = translation_feature.value

		# Check features value
		assert pytools.assertions.equal( list(R.shape), [3,3] )
		assert pytools.assertions.equal( R.dtype, numpy.dtype('float32') )
		assert pytools.assertions.equal( list(t.shape), [3] )
		assert pytools.assertions.equal( t.dtype, numpy.dtype('float32') )

		# Model view (model space to camera space) in OpenCV space
		mat_view = numpy.eye( 4, dtype=numpy.float32 )
		mat_view[:3, :3] = R
		mat_view[:3, 3 ] = t

		# Invert it to get Camera to Model space
		mat_view = numpy.linalg.inv( mat_view )

		# OpenGL expects column-wise matrix format
		mat_view = mat_view.T

		# Create the camera space origine
		origine = numpy.asarray( [0, 0, 0, 1], dtype=numpy.float32 )

		# Apply inverse model view to camera space origine
		point_of_view = numpy.dot( origine, mat_view )
		point_of_view = point_of_view[ 0:3 ]
		
		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.point_of_view_feature_name,
			            shape = pydataset.data.ShapeData( *point_of_view.shape ),
			            value = point_of_view,
			            dtype = numpy.dtype('float32')
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return PointOfViewMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class PointOfViewMapper ( Mapper )