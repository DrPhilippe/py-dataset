# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import transforms3d.quaternions
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper                      import Mapper
from .matrix_2_quaternion_settings import Matrix2QuaternionSettings

# ##################################################
# ###    CLASS MATRIX-TO-QUATERNION-SETTINGS     ###
# ##################################################

class Matrix2Quaternion ( Mapper ):
	"""
	Mapper that converts a rotation matrix to a rotation vector or vice-versa.

	Rotation vectors must be / are in radians
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Converts a rotation matrix to a rotation quaternion.
		
		Arguments:
			self             (`pydataset.manipualtors.pose.Matrix2Quaternion`): Mapper instance to initialize.
			settings (`pydataset.manipualtors.pose.Matrix2QuaternionSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, Matrix2Quaternion )
		assert pytools.assertions.type_is_instance_of( settings, Matrix2QuaternionSettings )

		super( Matrix2Quaternion, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.pose.Matrix2Quaternion`): Mapper used to map the example.
			example                  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, Matrix2Quaternion )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get feature
		rotation_matrix_feature = example.get_feature( self.settings.input_matrix_feature_name )
			
		# Check feature
		if not isinstance( rotation_matrix_feature.data, pydataset.dataset.NDArrayFeatureData ):
			raise RuntimeError(
				'Unsupported feature data type: type={}, expected=NDArrayFeatureData'.format( type(rotation_matrix_feature.data) )
				)

		# Convert rotation
		R = rotation_matrix_feature.value
		q = transforms3d.quaternions.mat2quat( R )

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.output_quaternion_feature_name,
			            value = q
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.pose.Matrix2Quaternion` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.pose.Matrix2Quaternion`.

		Returns:
			`pydataset.manipulators.pose.Matrix2QuaternionSettings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return Matrix2QuaternionSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Matrix2Quaternion ( Mapper )