# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..mapper                             import Mapper
from .mesh_to_bounding_rectangle_settings import MeshToBoundingRectangleSettings

# ##################################################
# ###      CLASS MESH-TO-BOUNDING-RECTANGLE      ###
# ##################################################

class MeshToBoundingRectangle ( Mapper ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.MeshToBoundingRectangle` class.

		Arguments:
			self             (`pydataset.manipulators.pose.MeshToBoundingRectangle`): Instance to initialize.
			settings (`pydataset.manipulators.pose.MeshToBoundingRectangleSettings`): Settings used.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangle )
		assert pytools.assertions.type_is_instance_of( settings, MeshToBoundingRectangleSettings )

		super( MeshToBoundingRectangle, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_mesh_points ( self, example ):
		"""
		Returns the points of the mesh of the given example.
		
		Arguments:
			self (`pydataset.manipualtors.MeshToBoundingRectangle`): Mesh to bounding rectangle mapper.
			example                   (`pydataset.dataset.Example`): Example of which to compute the bounding rectangle.

		Returns:
			`numpy.ndarray`: Mesh points with shape `[-1, 3]` and dtype `float32`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangle )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Get the mesh points
		mesh_data = example.get_feature( self.settings.mesh_data_feature_name ).value

		# Return its points
		return mesh_data.points
	
	# def get_mesh_points ( self, example )

	# --------------------------------------------------

	def project_points ( self, example, points ):
		"""
		Projects the given points to the image plane.

		Arguments:
			self (`pydataset.manipulators.pose.MeshToBoundingBox`): Bounding box creator.
			example                  (`pydataset.dataset.Example`): Current example.
			points                               (`numpy.ndarray`): Set of 3D points with shape `[-1, 3]` and dtype `float32`.

		Returns:
			`numpy.ndarray`: Projected points with dtype `float32` and shape `[-1, 2]`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangle )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( points, numpy.ndarray )
		assert pytools.assertions.equal( points.dtype, numpy.float32 )
		assert pytools.assertions.equal( len(points.shape), 2 )
		assert pytools.assertions.equal( points.shape[1], 3 )

		# Get the number of points
		nb_points = points.shape[ 0 ]

		# Get required features
		r = example.get_feature( self.settings.rotation_feature_name ).value
		t = example.get_feature( self.settings.translation_feature_name ).value
		K = example.get_feature( self.settings.camera_k_feature_name ).value

		# Get optional camera dist coefficients
		if self.settings.camera_dist_coeffs_feature_name:
			dist_coeffs = example.get_feature( settings.camera_dist_coeffs_feature_name ).value
		else:
			dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

		# Convert rotation to euler angles if necessary
		r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( r )

		# Project points to image plane
		points, jacobian = cv2.projectPoints( points, r, t, K, dist_coeffs )
		points = numpy.reshape( points, [nb_points, 2] )

		return points

	# def project_points ( self, points )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Computes the bounding rectangle in the given example.

		Arguments:
			self (`pydataset.manipualtors.MeshToBoundingRectangle`): Mesh to bounding rectangle mapper.
			example                   (`pydataset.dataset.Example`): Example of which to compute the bounding rectangle.

		Returns:
			`pydataset.dataset.Example`: Example onced its bounding rectangle was computed.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangle )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the mesh points
		mesh_points = self.get_mesh_points( example )

		# Project points to image plane
		mesh_points = self.project_points( example, mesh_points )

		# Compute bounding rectangle
		br = numpy.zeros( [2, 2], dtype=numpy.float32 )
		br[ 0, 0 ] = numpy.amin( mesh_points[:, 0] )
		br[ 0, 1 ] = numpy.amin( mesh_points[:, 1] )
		br[ 1, 0 ] = numpy.amax( mesh_points[:, 0] )
		br[ 1, 1 ] = numpy.amax( mesh_points[:, 1] )

		# Save the bb
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bounding_rectangle_feature_name,
			            value = br
			)
		
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.pose.MeshToBoundingRectangle` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.pose.MeshToBoundingRectangle`.

		Returns:
			`pydataset.manipulators.pose.MeshToBoundingRectangleSettings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return MeshToBoundingRectangleSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MeshToBoundingRectangle ( Mapper )