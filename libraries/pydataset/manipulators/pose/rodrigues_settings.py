# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###          CLASS RODRIGUES-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RodriguesSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'input_rotation_feature_name',
		'output_rotation_feature_name'
		]
	)
class RodriguesSettings ( MapperSettings ):
	"""
	Converts a rotation matrix to a rotation vector or vice-versa.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, input_rotation_feature_name, output_rotation_feature_name, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.RodriguesSettings` class.
		
		Arguments:
			self (`pydataset.manipulators.image.RodriguesSettings`): Instance to initialize.
			input_rotation_feature_name                     (`str`): Name of the feature containing the input rotation.
			output_rotation_feature_name                    (`str`): Name of the feature where to save the output rotation.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, RodriguesSettings )
		assert pytools.assertions.type_is( input_rotation_feature_name, str )
		assert pytools.assertions.type_is( output_rotation_feature_name, str )

		super( RodriguesSettings, self ).__init__( **kwargs )

		self._input_rotation_feature_name  = input_rotation_feature_name
		self._output_rotation_feature_name = output_rotation_feature_name

	# def __init__ ( self, input_rotation_feature_name, output_rotation_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def input_rotation_feature_name ( self ):
		"""
		Name of the feature containing the input rotation (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RodriguesSettings )

		return self._input_rotation_feature_name

	# def input_rotation_feature_name ( self )

	# --------------------------------------------------

	@input_rotation_feature_name.setter
	def input_rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RodriguesSettings )
		assert pytools.assertions.type_is( value, str )

		self._input_rotation_feature_name = value
	
	# def input_rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def output_rotation_feature_name ( self ):
		"""
		Name of the feature where to save the output rotation (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RodriguesSettings )

		return self._output_rotation_feature_name

	# def output_rotation_feature_name ( self )

	# --------------------------------------------------

	@output_rotation_feature_name.setter
	def output_rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RodriguesSettings )
		assert pytools.assertions.type_is( value, str )

		self._output_rotation_feature_name = value
	
	# def output_rotation_feature_name ( self, value )

# class RodriguesSettings ( MapperSettings )