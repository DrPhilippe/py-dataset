# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import pandas
import threading

# INTERNALS
import pytools
import pydataset

# LOCALS
from ..agregator                    import Agregator
from .pose_error_agregator_settings import PoseErrorAgregatorSettings

# ##################################################
# ###         CLASS POSE-ERROR-AGREGATOR         ###
# ##################################################

class PoseErrorAgregator ( Agregator ):
	"""
	Agregate the pose error of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregator )
		assert pytools.assertions.type_is_instance_of( settings, PoseErrorAgregatorSettings )

		super( PoseErrorAgregator, self ).__init__( settings )

		# self._depth_differences      = {}
		self._depth_outlier_totals   = {}
		self._depth_outlier_dividers = {}
		self._master_lock            = None
		# self._sizes                  = {}

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _depth_image_to_distance_image ( self, depth, K ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregator )
		assert pytools.assertions.type_is_instance_of( depth, numpy.ndarray )
		assert pytools.assertions.type_is_instance_of( K, numpy.ndarray )
		
		xs, ys = numpy.meshgrid(
			numpy.arange( depth.shape[1] ),
			numpy.arange( depth.shape[0] )
			)
		Xs = (xs - K[0, 2]) / numpy.float64( K[0, 0] )
		Ys = (ys - K[1, 2]) / numpy.float64( K[1, 1] )

		distance_image = numpy.sqrt(
			numpy.multiply(Xs, depth)**2 +
			numpy.multiply(Ys, depth)**2 +
			depth.astype(numpy.float64)**2 )

		return distance_image

	# def _depth_image_to_distance_image ( depth, K )

	# --------------------------------------------------
	
	def agregate ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get mask
		mask = example.get_feature( self.settings.mask_feature_name ).value.astype( numpy.bool )
		
		# Check object is visible
		visible_pixels_count = numpy.sum( mask, axis=None, dtype=numpy.float64 )
		if visible_pixels_count == 0:
			return

		# Get features
		category             = example.get_feature( self.settings.category_feature_name ).value
		depth_map            = example.get_feature( self.settings.depth_feature_name ).value.astype( numpy.float16 )
		depth_scale          = example.get_feature( self.settings.depth_scale_feature_name ).value if self.settings.depth_scale_feature_name else float(1.)
		render_depth_map     = example.get_feature( self.settings.render_depth_feature_name ).value.astype( numpy.float16 )
		render_depth_scale   = example.get_feature( self.settings.render_depth_scale_feature_name ).value if self.settings.render_depth_scale_feature_name else float(1.)
		K                    = example.get_feature( self.settings.camera_k_feature_name ).value

		# Category
		category = '{:04d}'.format( category ) if isinstance( category, int ) else str(category).lower().replace(' ', '-')

		# Fetch array size
		H, W = mask.shape[0], mask.shape[1]

		# scale depthes maps
		depth_map        = numpy.multiply( depth_map, depth_scale )
		render_depth_map = numpy.multiply( render_depth_map, render_depth_scale )

		# Compute pixel to pixel differences
		depth_maps_differences     = numpy.subtract( depth_map, render_depth_map )
		abs_depth_maps_differences = numpy.abs( depth_maps_differences )

		# # Export differences maps?
		# if self.settings.output_directorypath:
		# 	# convert to absolute values and floats
		# 	abs_depth_maps_differences = numpy.abs( depth_maps_differences ).astype( numpy.float16 )
		# 	# Normalize
		# 	abs_depth_maps_differences = abs_depth_maps_differences / abs_depth_maps_differences.max() * 255.
		# 	# Create filepath
		# 	depth_filenpath = self.settings.output_directorypath + pytools.path.FilePath.format(
		# 		self.settings.output_filename_format,
		# 		'depth',
		# 		numpy.mean( abs_depth_maps_differences )
		# 		)
		# 	depth_filenpath.ensure_uniqueness()
		# 	# Save
		# 	cv2.imwrite( str(depth_filenpath), abs_depth_maps_differences.astype( numpy.uint8 ) )

		# Indexes of pixels whoes:
		# - Absolute depth error is less than the outlier threshol
		# - That are within the object mask
		depth_indexes = numpy.logical_and(
			numpy.less_equal( abs_depth_maps_differences, self.settings.outlier_threshold ),
			mask
			)
		depth_outlier_indexes = numpy.logical_and(
			numpy.greater( abs_depth_maps_differences, self.settings.outlier_threshold ),
			mask
			)

		# Percent of depth outlier
		depth_outliers_percent = numpy.sum( depth_outlier_indexes, axis=None, dtype=numpy.float64 ) / visible_pixels_count * 100.

		# Inliers
		depth_inliers = depth_maps_differences[ depth_indexes ].ravel()

		# Agregate
		self.agregate_depth( category, depth_inliers, depth_outliers_percent )
		self.agregate_depth( 'all', depth_inliers, depth_outliers_percent )

	# def agregate ( self, example )
	
	# --------------------------------------------------

	def agregate_depth ( self, category, inliers, outliers_percent ):
		# Master Lock
		self._master_lock.acquire()
		# Agregate errors using a buffer
		if category not in self._locks:
			# Create the lock for that category
			self._locks[ category ] = threading.Lock()
			# Size tracker
			# self._sizes[ category ] = inliers.size
			# Ignore category all
			# if category != 'all':
			# 	# Create the buffer for that category
			# 	tmp = numpy.memmap(
			# 		'C:\\buffers\\{}.array'.format( category ),
			# 		dtype=numpy.float32,
			# 		mode='w+',
			# 		shape=(self._sizes[ category ]),
			# 		order='C'
			# 		)
			# 	tmp[:] = inliers
			# 	tmp.flush()

			# Outliers
			self._depth_outlier_totals[ category ] = outliers_percent
			self._depth_outlier_dividers[ category ] = 1.0
			# Release master lock
			self._master_lock.release()
		
		else:
			# Use the cateogry specific lock
			self._locks[ category ].acquire()
			# Release master lock
			self._master_lock.release()
			# Size tracker
			# previous = self._sizes[ category ]
			# self._sizes[ category ] += inliers.size
			# Ignore category all
			# if category != 'all':
			# 	# Append to the buffer of that category
			# 	tmp = numpy.memmap(
			# 		'C:\\buffers\\{}.array'.format( category ),
			# 		dtype=numpy.float32,
			# 		mode='r+',
			# 		shape=(self._sizes[ category ]),
			# 		order='C'
			# 		)
			# 	tmp[ previous: ] = inliers

			# Outliers
			self._depth_outlier_totals[ category ] += outliers_percent
			self._depth_outlier_dividers[ category ] += 1.0
			# Done, release lock
			self._locks[ category ].release()

	# def agregate_depth ( self, category, inliers, outliers_percent )

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		# self._depth_differences      = {}
		self._depth_outlier_totals   = {}
		self._depth_outlier_dividers = {}
		# self._sizes                  = {}
		self._locks                  = {}
		self._master_lock            = threading.Lock()

		# if self.settings.output_directorypath and self.settings.output_directorypath.is_not_a_directory():
		# 	self.settings.output_directorypath.create()

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		# # Names of categories without 'all'
		# categories = [ c for c in sorted(self._locks.keys()) if c != 'all' ]

		# # Create the buffer for all
		# all_buffer = numpy.memmap(
		# 			'C:\\buffers\\all.array',
		# 			dtype=numpy.float32,
		# 			mode='w+',
		# 			shape=(self._sizes['all']),
		# 			order='C'
		# 			)
		# all_count = 0
		
		# # Apppend the data off all categories in it
		# for category in categories:
		# 	tmp_count  = self._sizes[ category ]
		# 	tmp_buffer = numpy.memmap(
		# 			'C:\\buffers\\{}.array'.format( category ),
		# 			dtype = numpy.float32,
		# 			mode  = 'r',
		# 			shape = (tmp_count),
		# 			order = 'C'
		# 			)
		# 	all_buffer[ all_count : all_count+tmp_count ] = tmp_buffer
		# 	all_count += tmp_count
		# 	del tmp_buffer

		# Names of categories with 'all'
		categories = list(sorted(self._locks.keys()))

		# Data
		# depth_means     = []
		# depth_stds      = []
		# abs_depth_means = []
		# abs_depth_stds  = []
		depth_outliers  = []

		# Go through each category
		for category in categories:

			# depth_differences = numpy.memmap(
			# 	'C:\\buffers\\{}.array'.format( category ),
			# 	dtype = numpy.float32,
			# 	mode  = 'r',
			# 	shape = (self._sizes[ category ]),
			# 	order = 'C'
			# 	)

			# # Compute mean and std depth error
			# depth_mean = numpy.mean( depth_differences, axis=None, dtype=numpy.float64 )
			# depth_std  = numpy.std( depth_differences, axis=None, dtype=numpy.float64 )

			# # Compute mean and std absolute depth error
			# abs_depth_mean = numpy.mean( numpy.abs(depth_differences), axis=None, dtype=numpy.float64 )
			# abs_depth_std  = numpy.std( numpy.abs(depth_differences), axis=None, dtype=numpy.float64 )

			# Append to data
			# depth_means.append( depth_mean )
			# depth_stds.append( depth_std )
			# abs_depth_means.append( abs_depth_mean )
			# abs_depth_stds.append( abs_depth_std )

			# Outliers percent
			depth_outlier_totals = self._depth_outlier_totals[ category ]
			depth_outlier_dividers = self._depth_outlier_dividers[ category ]
			depth_outliers.append( depth_outlier_totals / depth_outlier_dividers )

		# for category in categories

		data = pandas.DataFrame({
			'categories':     categories,
			# 'mean_depth':     depth_means,
			# 'std_depth':      depth_stds,
			# 'mean_abs_depth': abs_depth_means,
			# 'std_abs_depth':  abs_depth_stds,
			'depth_outliers': depth_outliers
			})

		# Create output directory if needed
		if self.settings.output_filepath.parent().is_not_a_directory():
			self.settings.output_filepath.parent().create()

		data.to_csv(
			str(self.settings.output_filepath),
			sep          = ',',
			na_rep       = 'nan',
			float_format = '%010.6f',
			index        = False
			)

	# def end ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return PoseErrorAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class PoseErrorAgregator ( Agregator )