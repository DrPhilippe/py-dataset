# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from ..mapper                            import Mapper
from .control_points_normalizer_settings import ControlPointsNormalizerSettings

# ##################################################
# ###  CLASS CONTROL-POINTS-NORMALIZER-SETTINGS  ###
# ##################################################

class ControlPointsNormalizer ( Mapper ):
	"""
	Mapper used to create BB8 control points out of the bounding box.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.ControlPointsNormalizer` class.

		Arguments:
			self             (`pydataset.manipulators.pose.ControlPointsNormalizer`): Instance to initialize.
			settings (`pydataset.manipulators.pose.ControlPointsNormalizerSettings`): Settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizer )
		assert pytools.assertions.type_is_instance_of( settings, ControlPointsNormalizerSettings )

		super( ControlPointsNormalizer, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Normalizes control points for the given example.
		
		This converts pixel coordinates to relative image coordinates.

		Arguments:
			self (`pydataset.manipulators.pose.ControlPointsNormalizer`): Control points normalizer instance.
			example                        (`pydataset.dataset.Example`): Example for which to normalize the control points.

		Returns:
			`pydataset.dataset.Example`: The example with normalized control points.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizer )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
			
		# Get features
		control_points = example.get_feature( self.settings.control_points_feature_name ).value.copy()
		image_shape    = example.get_feature( self.settings.image_feature_name ).data.shape
		
			
		# Get the image width and height
		width  = image_shape[ 0 ]
		height = image_shape[ 1 ]

		# Normalize the control points coordinates
		control_points[:, 0] /= width
		control_points[:, 1] /= height

		# Update the control points
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.control_points_feature_name,
			            shape = pydataset.data.ShapeData( control_points.shape[0], control_points.shape[1] ),
			            value = control_points,
			            dtype = numpy.dtype('float32')
			)
		
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		assert pytools.assertions.type_is( cls, type )

		return ControlPointsNormalizerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ControlPointsNormalizer ( Mapper )