# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import pandas
import threading

# INTERNALS
import pytools
import pydataset

# LOCALS
from ..agregator                    import Agregator
from .visibility_agregator_settings import VisibilityAgregatorSettings

# ##################################################
# ###         CLASS VISIBILITY-AGREGATOR         ###
# ##################################################

class VisibilityAgregator ( Agregator ):
	"""
	Agregate the visibility of objects.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregator )
		assert pytools.assertions.type_is_instance_of( settings, VisibilityAgregatorSettings )

		super( VisibilityAgregator, self ).__init__( settings )

		self._lock = threading.Lock()
		self._per_category_visibilities = {}
		self._per_category_in_frame = {}
		self._per_category_count = {}

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get features
		category   = example.get_feature( self.settings.category_feature_name ).value
		visibility = example.get_feature( self.settings.visibility_feature_name ).value
		mask       = example.get_feature( self.settings.mask_feature_name ).value.astype(numpy.bool)

		# Category to string
		category = '{:04d}'.format(category) if isinstance( category, int ) else str(category)

		# Reshape visibility
		visibility = numpy.asarray( [visibility], dtype=numpy.float32 )
		visibility = numpy.reshape( visibility, [1] )

		# Check if any pixels are visible and convert to int (0 or 1)
		in_frame = numpy.any( mask, axis=None )
		in_frame = int( in_frame )

		# self._lock.acquire()

		# Agregation for that category
		if category not in self._per_category_visibilities:
			# Set it as agregated visibilities
			self._per_category_visibilities[ category ] = visibility
		else:
			# Append to already agregated visibilities
			self._per_category_visibilities[ category ] = numpy.concatenate( [self._per_category_visibilities[ category ], visibility], axis=0 )

		# Agregation for all categories
		if 'all' not in self._per_category_visibilities:
			# Set it as agregated visibilities
			self._per_category_visibilities[ 'all' ] = visibility
		else:
			# Append to already agregated visibilities
			self._per_category_visibilities[ 'all' ] = numpy.concatenate( [self._per_category_visibilities[ 'all' ], visibility], axis=0 )

		# Add it to the agregate count of out of frame pictures
		if category not in self._per_category_in_frame:
			self._per_category_in_frame[ category ] = in_frame
		else:
			self._per_category_in_frame[ category ] += in_frame

		# Add it to the agregate count of out of frame pictures
		if 'all' not in self._per_category_in_frame:
			self._per_category_in_frame[ 'all' ] = in_frame
		else:
			self._per_category_in_frame[ 'all' ] += in_frame

		if category not in self._per_category_count:
			self._per_category_count[ category ] = 1
		else:
			self._per_category_count[ category ] += 1

		if 'all' not in self._per_category_count:
			self._per_category_count[ 'all' ] = 1
		else:
			self._per_category_count[ 'all' ] += 1

		# self._lock.release()

	# def agregate ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self._per_category_visibilities = {}
		self._per_category_in_frame = {}

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		categories = list( sorted(self._per_category_visibilities.keys()) )
		minimums   = []
		maximums   = []
		means      = []
		stds       = []
		ins        = []

		for category in categories:

			# Get the visibilities of that category
			visibilities = self._per_category_visibilities[ category ]

			# Compute minimum, mean and maximum error
			amin = numpy.amin( visibilities, axis=None )
			amax = numpy.amax( visibilities, axis=None )
			mean = numpy.mean( visibilities, axis=None, dtype=numpy.float64 )
			std  = numpy.std(  visibilities, axis=None, dtype=numpy.float64 )
			
			minimums.append( amin )
			maximums.append( amax )
			means.append( mean )
			stds.append( std )

			in_count = self._per_category_in_frame[ category ]
			count = self._per_category_count[ category ]
			ins.append( float(in_count)/float(count)*100. )

		data = {
			'categories': categories,
			'min': minimums,
			'max': maximums,
			'mean': means,
			'std': stds,
			'in': ins
		}

		if self.settings.output_filepath.parent().is_not_a_directory():
			self.settings.output_filepath.parent().create()

		data = pandas.DataFrame( data )
		data.to_csv(
			str(self.settings.output_filepath),
			sep=',',
			na_rep='nan',
			float_format='%010.4f',
			index=False
			)

	# def end ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return VisibilityAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class VisibilityAgregator ( Agregator )