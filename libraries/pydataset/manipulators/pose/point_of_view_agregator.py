# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import pandas

# INTERNALS
import pytools
import pydataset

# LOCALS
from ..agregator                       import Agregator
from .point_of_view_agregator_settings import PointOfViewAgregatorSettings

# ##################################################
# ###       CLASS POINT-OF-VIEW-AGREGATOR        ###
# ##################################################

class PointOfViewAgregator ( Agregator ):
	"""
	Point of view agregator.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Agregates point of views.
		
		Arguments:
			self             (`pydataset.manipualtors.pose.PointOfViewAgregator`): Point-Of-View agregator instance to initialize.
			settings (`pydataset.manipualtors.pose.PointOfViewAgregatorSettings`): Settings of the agregator.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )
		assert pytools.assertions.type_is_instance_of( settings, PointOfViewAgregatorSettings )

		super( PointOfViewAgregator, self ).__init__( settings )

		self._points_of_views = {}
		self._distances = {}
		self._azymuths = {}
		self._elevations = {}
		self._statistics = {}

	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def points_of_views ( self ):
		"""
		Agregated points of views.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )

		return self._points_of_views
	
	# def points_of_views ( self )

	# --------------------------------------------------
	
	@property
	def distances ( self ):
		"""
		Agregated distances
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )

		return self._distances
	
	# def distances ( self )

	# --------------------------------------------------
	
	@property
	def azymuths ( self ):
		"""
		Agregated azymuths
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )

		return self._azymuths
	
	# def azymuths ( self )

	# --------------------------------------------------
	
	@property
	def elevations ( self ):
		"""
		Agregated elevations
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )

		return self._elevations
	
	# def elevations ( self )

	# --------------------------------------------------
	
	@property
	def statistics ( self ):
		"""
		Agregated statistics
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )

		return self._statistics
	
	# def statistics ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.pose.PointOfViewAgregator`): Agregator used to agregate the point of views.
			example                     (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get features used to compute and agregate the point of view
		category = example.get_feature( self.settings.category_index_feature_name ).value
		R        = example.get_feature( self.settings.rotation_matrix_feature_name ).value
		t        = example.get_feature( self.settings.translation_vector_feature_name ).value

		# Convert category to str
		category = '{:06d}'.format(category) if isinstance(category, int) else str(category)

		# Model view (model space to camera space) in OpenCV space
		mat_view = numpy.eye( 4, dtype=numpy.float32 )
		mat_view[:3, :3] = R
		mat_view[:3, 3 ] = t

		# Invert it to get Camera to Model space
		inverse_mat_view = numpy.linalg.inv( mat_view )

		# OpenGL expects column-wise matrix format
		inverse_mat_view = inverse_mat_view.T

		# Create the camera space origine
		origine = numpy.asarray( [0, 0, 0, 1], dtype=numpy.float32 )

		# Apply inverse model view to camera space origine
		point_of_view = numpy.dot( origine, inverse_mat_view )
		point_of_view = point_of_view[ 0:3 ]
		point_of_view = numpy.reshape( point_of_view, [1, 3] )

		# Agregate the point of view
		if category not in self._points_of_views:
			self._points_of_views[ category ] = point_of_view
		else:
			self._points_of_views[ category ] = numpy.vstack( (self._points_of_views[ category ], point_of_view) )

	# def agregate ( self, example )
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before agregation starts.

		Arguments:
			self (`pydataset.manipualtors.pose.PointOfViewAgregator`): Agregator used to agregate the point of views.
			root_group                    (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                                (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		self._points_of_views = {}
		self._distances = {}
		self._azymuths = {}
		self._elevations = {}

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after manipulation append.

		Arguments:
			self (`pydataset.manipualtors.pose.PointOfViewAgregator`): Agregator used to agregate the point of views.
			root_group                    (`pydataset.dataset.Group`): Group containing the examples that have been agregated.
			number_of_examples                                (`int`): Number of examples agregated.
		"""
		assert pytools.assertions.type_is_instance_of( self, PointOfViewAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		# Create output directory if necessary
		if self.settings.output_directory_path.is_not_a_directory():
			self.settings.output_directory_path.create()

		# Concatenate everything in category all
		self._points_of_views[ 'all' ] = numpy.concatenate( list(self._points_of_views.values()), axis=0 )

		# Compute distances, azymuths and elevations
		for category in sorted(self._points_of_views.keys()):
			
			points_of_views = self._points_of_views[ category ]
			N = points_of_views.shape[ 0 ]
			
			# Compute distances to the object
			distances = numpy.linalg.norm( points_of_views, axis=-1 )
			
			# Azimuth from [0, 360].
			azimuths = numpy.arctan2( points_of_views[:,1], points_of_views[:,0] )
			azimuths = numpy.where( azimuths<0, azimuths+2.*numpy.pi, azimuths )
			azimuths = (180./numpy.pi) * azimuths
			
			# Elevation from [-90, 90].
			in_plane = points_of_views.copy()
			in_plane[:, 2] = 0.
			a = numpy.linalg.norm( points_of_views )
			b = numpy.linalg.norm( in_plane, axis=-1 )
			elevations = numpy.arccos( b / a )
			elevations = numpy.where( points_of_views[:,2]<0, -elevations, elevations )
			elevations = (180.0/numpy.pi) * elevations

			# Keep in memory
			self._distances[ category ] = distances
			self._azymuths[ category ] = azimuths
			self._elevations[ category ] = elevations

			# Concatenate distances, azimuths and elevations
			distances  = numpy.reshape( distances, [N, 1] )
			azimuths   = numpy.reshape( azimuths, [N, 1] )
			elevations = numpy.reshape( elevations, [N, 1] )
			everything = numpy.concatenate( [points_of_views, distances, azimuths, elevations], axis=1 )
			
			# Compute statistics
			amin = numpy.amin( everything, axis=0, keepdims=True )
			amax = numpy.amax( everything, axis=0, keepdims=True )
			mean = numpy.mean( everything, axis=0, dtype=numpy.float64, keepdims=True )
			std  = numpy.std( everything, axis=0, dtype=numpy.float64, keepdims=True )
			
			# Concatenate  statistics
			statistics = numpy.concatenate( [amin, amax, mean, std], axis=0 )
			
			# Keep in memory
			self._statistics[ category ] = statistics

			# Concatenate everything
			everything = numpy.concatenate( [everything, statistics], axis=0 )

			# Create the path of the file where to save everything
			filepath = self.settings.output_directory_path + pytools.path.FilePath.format( self.settings.filename_format, category )

			# Save
			# numpy.savetxt(
			# 	str(filepath),
			# 	everything,
			# 	delimiter=',',
			# 	header='x,y,z,distance,azimuth,elevation',
			# 	comments='',
			# 	fmt='%010.4f'
			# 	)

			df = pandas.DataFrame({
				        'x': everything[:,0],
				        'y': everything[:,1],
				        'z': everything[:,2],
				 'distance': everything[:,3],
				  'azimuth': everything[:,4],
				'elevation': everything[:,5],
				},
				index = ['{:010d}'.format(i) for i in range(N)] + ['min', 'max', 'mean', 'std']
				)
			df.to_csv( str(filepath),
				         sep = ',',
				      na_rep = 'nan',
				float_format = '%010.4f'
				)

		# for category in sorted(self._points_of_views.keys()):

	# def end ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return PointOfViewAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class PointOfViewAgregator ( Agregator )