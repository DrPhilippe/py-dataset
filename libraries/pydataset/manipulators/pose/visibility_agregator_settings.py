# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###    CLASS VISIBILITY-AGREGATOR-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'VisibilityAgregatorSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'category_feature_name',
		'mask_feature_name',
		'visibility_feature_name',
		'output_filepath'
		]
	)
class VisibilityAgregatorSettings ( AgregatorSettings ):
	"""
	Settings used to agregate the visibility of objects.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		category_feature_name = 'category_index',
		visibility_feature_name = 'visibility',
		mask_feature_name = 'mask',
		output_filepath = 'visibility.csv',
		**kwargs
		):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )
		assert pytools.assertions.type_is( category_feature_name, str )
		assert pytools.assertions.type_is( visibility_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is_instance_of( output_filepath, (str, pytools.path.FilePath) )
		output_filepath = pytools.path.FilePath.ensure( output_filepath )
		assert pytools.assertions.equal( output_filepath.extension(), '.csv' )

		super( VisibilityAgregatorSettings, self ).__init__( **kwargs )

		self._category_feature_name   = category_feature_name
		self._visibility_feature_name = visibility_feature_name
		self._mask_feature_name       = mask_feature_name
		self._output_filepath         = output_filepath

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def category_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )

		return self._category_feature_name

	# def category_feature_name ( self )

	# --------------------------------------------------

	@category_feature_name.setter
	def category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_feature_name = value

	# def category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def visibility_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )

		return self._visibility_feature_name

	# def visibility_feature_name ( self )

	# --------------------------------------------------

	@visibility_feature_name.setter
	def visibility_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._visibility_feature_name = value

	# def visibility_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )

		return self._mask_feature_name

	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._mask_feature_name = value

	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def output_filepath ( self ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )

		return self._output_filepath

	# def output_filepath ( self )

	# --------------------------------------------------

	@output_filepath.setter
	def output_filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VisibilityAgregatorSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.FilePath) )
		value = pytools.path.FilePath.ensure( value )
		assert pytools.assertions.equal( value.extension(), '.csv' )

		self._output_filepath = value

	# def output_filepath ( self, value )

# class VisibilityAgregatorSettings ( Agregator )