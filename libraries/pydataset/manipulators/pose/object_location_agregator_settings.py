# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools

# LOCALS
import pydataset.data
import pydataset.render
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###  CLASS OBJECT-LOCATION-AGREGATOR-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ObjectLocationAgregatorSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'camera_matrix_feature_name',
		'category_index_feature_name',
		'rotation_matrix_feature_name',
		'translation_vector_feature_name',
		'output_directory_path',
		'filename_format',
		]
	)
class ObjectLocationAgregatorSettings ( AgregatorSettings ):
	"""
	Computes for each object the view direction in camera space.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		     camera_matrix_feature_name = 'K',
		    category_index_feature_name = 'category_index',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = 'locations',
		                filename_format = 'locations_{:06d}.csv',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.ObjectLocationAgregatorSettings` class.
		
		Arguments:
			self     (`pydataset.manipulators.pose.ObjectLocationAgregatorSettings`): Instance to initialize.
			camera_matrix_feature_name                                       (`str`): Name of the feature containing the camera parameters.
			image_feature_name                                               (`str`): Name the feature containing the image.
			image_shape                          (`None`/`pydataset.data.ShapeData`): Shape of the image as `[height, width]`.
			category_index_feature_name                                      (`str`): Name of the feature containing the category index.
			rotation_matrix_feature_name                                     (`str`): Name of the feature containing the rotation matrix.
			translation_vector_feature_name                                  (`str`): Name of the feature containing the translation vector.
			output_directory_path               (`str`/`pytools.path.DirectoryPath`): Path of the directory where to save the results.
			filename_format                                                  (`str`): Format used to create the name of the files.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( category_index_feature_name, str )
		assert pytools.assertions.type_is( rotation_matrix_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is_instance_of( output_directory_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( filename_format, str )
		assert pytools.assertions.true( '_' in filename_format )
		assert pytools.assertions.true( filename_format.endswith('.csv') )

		super( ObjectLocationAgregatorSettings, self ).__init__( **kwargs )

		self._camera_matrix_feature_name      = camera_matrix_feature_name
		self._category_index_feature_name     = category_index_feature_name
		self._rotation_matrix_feature_name    = rotation_matrix_feature_name
		self._translation_vector_feature_name = translation_vector_feature_name
		self._output_directory_path           = pytools.path.DirectoryPath.ensure( output_directory_path )
		self._filename_format                 = filename_format

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		"""
		Name of the feature containing the camera parameters (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._camera_matrix_feature_name

	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def category_index_feature_name ( self ):
		"""
		Name of the feature containing the input category index (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._category_index_feature_name

	# def category_index_feature_name ( self )

	# --------------------------------------------------

	@category_index_feature_name.setter
	def category_index_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_index_feature_name = value
	
	# def category_index_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_matrix_feature_name ( self ):
		"""
		Name of the feature containing the rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._rotation_matrix_feature_name

	# def rotation_matrix_feature_name ( self )

	# --------------------------------------------------

	@rotation_matrix_feature_name.setter
	def rotation_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_matrix_feature_name = value
	
	# def rotation_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._translation_vector_feature_name

	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_vector_feature_name = value
	
	# def translation_vector_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def output_directory_path ( self ):
		"""
		Path of the directory where to save the results (`str`/`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._output_directory_path

	# def output_directory_path ( self )

	# --------------------------------------------------

	@output_directory_path.setter
	def output_directory_path ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._output_directory_path = pytools.path.DirectoryPath.ensure( value )

	# def output_directory_path ( self, value )

	# --------------------------------------------------

	@property
	def filename_format ( self ):
		"""
		Format used to create the name of the files (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )

		return self._filename_format

	# def filename_format ( self )

	# --------------------------------------------------

	@filename_format.setter
	def filename_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ObjectLocationAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.true( '_' in value )
		assert pytools.assertions.true( value.endswith('.csv') )

		self._filename_format = value

	# def filename_format ( self, value )

# class ObjectLocationAgregatorSettings ( AgregatorSettings )