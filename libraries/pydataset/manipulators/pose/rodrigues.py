# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..mapper            import Mapper
from .rodrigues_settings import RodriguesSettings

# ##################################################
# ###              CLASS RODRIGUES               ###
# ##################################################

class Rodrigues ( Mapper ):
	"""
	Mapper that converts a rotation matrix to a rotation vector or vice-versa.

	Rotation vectors must be / are in radians
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Converts a rotation matrix to a rotation vector or vice-versa.
		
		Arguments:
			self             (`pydataset.manipualtors.pose.Rodrigues`): Rodrigues mapper instance to initialize.
			settings (`pydataset.manipualtors.pose.RodriguesSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, Rodrigues )
		assert pytools.assertions.type_is_instance_of( settings, RodriguesSettings )

		super( Rodrigues, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.pose.Rodrigues`): Mapper used to map the example.
			example          (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, Rodrigues )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get feature
		feature = example.get_feature( self.settings.input_rotation_feature_name )
			
		# Check feature
		if not isinstance( feature.data, pydataset.dataset.NDArrayFeatureData ):
			raise RuntimeError(
				'Unsupported feature data type: type={}, expected=NDArrayFeatureData'.format( type(feature.data) )
				)

		# Convert rotation
		r, _ = cv2.Rodrigues( feature.value )
		
		# Reshape rotation
		if r.size == 3:
			r = numpy.reshape( r, [3] )
		else:
			r = numpy.reshape( r, [3, 3] )

		# Create the mask feature
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.output_rotation_feature_name,
			            value = r
			)

		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.pose.Rodrigues` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.pose.Rodrigues`.

		Returns:
			`pydataset.manipulators.pose.RodriguesSettings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return RodriguesSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Rodrigues ( Mapper )