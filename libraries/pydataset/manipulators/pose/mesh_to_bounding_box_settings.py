	# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###    CLASS MESH-TO-BOUNDING-BOX-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshToBoundingBoxSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		# in
		'mesh_data_feature_name',
		'rotation_feature_name',
		'translation_feature_name',
		'camera_k_feature_name',
		'camera_dist_coeffs_feature_name',
		# out
		'bounding_box_feature_name',
		]
	)
class MeshToBoundingBoxSettings ( MapperSettings ):
	"""
	Settings used to compute the bounding box of an object under a given pose.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		# in
		mesh_data_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		camera_k_feature_name = '',
		camera_dist_coeffs_feature_name = '',
		# out
		bounding_box_feature_name = '',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.MeshToBoundingBoxSettings` class.
	
		If only `mesh_data_feature_name` is specified, the bounding box is computed in the model space.
		If `rotation_feature_name` and `translation_feature_name` are specified it is computed in camera space.
		If `camera_k_feature_name` and `camera_dist_coeffs_feature_name` are specified it is computed in image space.

		Arguments:
			self (`pydataset.manipulators.pose.MeshToBoundingBoxSettings`): Instance to initialize.
			mesh_data_feature_name                                 (`str`): Name of the feature containing the mesh of the object.
			rotation_feature_name                                  (`str`): Name of the feature containing the rotation matrix, vector or quaternion.
			translation_feature_name                               (`str`): Name of the feature containing the translation vector.
			camera_k_feature_name                                  (`str`): Name of the feature containing the camera k matrix.
			camera_dist_coeffs_feature_name                        (`str`): Name of the feature containing the camera distortion coefficient vector.
			bounding_box_feature_name                              (`str`): Name of the feature where to save the bounding box.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( mesh_data_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( camera_k_feature_name, str )
		assert pytools.assertions.type_is( camera_dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( bounding_box_feature_name, str )

		super( MeshToBoundingBoxSettings, self ).__init__( **kwargs )

		self._mesh_data_feature_name          = mesh_data_feature_name
		self._rotation_feature_name           = rotation_feature_name
		self._translation_feature_name        = translation_feature_name
		self._camera_k_feature_name           = camera_k_feature_name
		self._camera_dist_coeffs_feature_name = camera_dist_coeffs_feature_name
		self._bounding_box_feature_name       = bounding_box_feature_name

	# def __init__ ( self, mesh_data_feature_name, bounding_box_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mesh_data_feature_name ( self ):
		"""
		Name of the feature containing the mesh of the object (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._mesh_data_feature_name

	# def mesh_data_feature_name ( self )

	# --------------------------------------------------

	@mesh_data_feature_name.setter
	def mesh_data_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._mesh_data_feature_name = value
	
	# def mesh_data_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation matrix, vector or quaternion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._rotation_feature_name

	# def rotation_feature_name ( self )

	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_feature_name = value
	
	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._translation_feature_name

	# def translation_feature_name ( self )

	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_feature_name = value
	
	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_k_feature_name ( self ):
		"""
		Name of the feature containing the camera k matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._camera_k_feature_name

	# def camera_k_feature_name ( self )

	# --------------------------------------------------

	@camera_k_feature_name.setter
	def camera_k_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_k_feature_name = value
	
	# def camera_k_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_dist_coeffs_feature_name ( self ):
		"""
		Name of the feature containing the camera distortion coefficient vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._camera_dist_coeffs_feature_name

	# def camera_dist_coeffs_feature_name ( self )

	# --------------------------------------------------

	@camera_dist_coeffs_feature_name.setter
	def camera_dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_dist_coeffs_feature_name = value
	
	# def camera_dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_box_feature_name ( self ):
		"""
		Name of the feature where to save the bounding box (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )

		return self._bounding_box_feature_name

	# def bounding_box_feature_name ( self )

	# --------------------------------------------------

	@bounding_box_feature_name.setter
	def bounding_box_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBoxSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_box_feature_name = value
	
	# def bounding_box_feature_name ( self, value )

# class MeshToBoundingBoxSettings ( MapperSettings )