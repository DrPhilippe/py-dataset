# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###    CLASS MATRIX-TO-QUATERNION-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'Matrix2QuaternionSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'input_matrix_feature_name',
		'output_quaternion_feature_name'
		]
	)
class Matrix2QuaternionSettings ( MapperSettings ):
	"""
	Converts rotation matrix to quaternion.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, input_matrix_feature_name, output_quaternion_feature_name, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.image.Matrix2QuaternionSettings` class.
		
		Arguments:
			self (`pydataset.manipulators.image.Matrix2QuaternionSettings`): Instance to initialize.
			input_matrix_feature_name                               (`str`): Name of the feature containing the input rotation matrix.
			output_quaternion_feature_name                          (`str`): Name of the feature where to save the output rotation quaternion.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, Matrix2QuaternionSettings )
		assert pytools.assertions.type_is( input_matrix_feature_name, str )
		assert pytools.assertions.type_is( output_quaternion_feature_name, str )

		super( Matrix2QuaternionSettings, self ).__init__( **kwargs )

		self._input_matrix_feature_name      = input_matrix_feature_name
		self._output_quaternion_feature_name = output_quaternion_feature_name

	# def __init__ ( self, input_matrix_feature_name, output_quaternion_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def input_matrix_feature_name ( self ):
		"""
		Name of the feature containing the input rotation matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Matrix2QuaternionSettings )

		return self._input_matrix_feature_name

	# def input_matrix_feature_name ( self )

	# --------------------------------------------------

	@input_matrix_feature_name.setter
	def input_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Matrix2QuaternionSettings )
		assert pytools.assertions.type_is( value, str )

		self._input_matrix_feature_name = value
	
	# def input_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def output_quaternion_feature_name ( self ):
		"""
		Name of the feature where to save the output rotation quaternion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Matrix2QuaternionSettings )

		return self._output_quaternion_feature_name

	# def output_quaternion_feature_name ( self )

	# --------------------------------------------------

	@output_quaternion_feature_name.setter
	def output_quaternion_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Matrix2QuaternionSettings )
		assert pytools.assertions.type_is( value, str )

		self._output_quaternion_feature_name = value
	
	# def output_quaternion_feature_name ( self, value )

# class Matrix2QuaternionSettings ( MapperSettings )