	# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ### CLASS BOUNDING-RECTANGLE-CREATOR-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MeshToBoundingRectangleSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		# in
		'mesh_data_feature_name',
		'rotation_feature_name',
		'translation_feature_name',
		'camera_k_feature_name',
		'camera_dist_coeffs_feature_name',
		# out
		'bounding_rectangle_feature_name',
		]
	)
class MeshToBoundingRectangleSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		# in
		mesh_data_feature_name = '',
		rotation_feature_name = '',
		translation_feature_name = '',
		camera_k_feature_name = '',
		camera_dist_coeffs_feature_name = '',
		# out
		bounding_rectangle_feature_name = '',
		**kwargs
		):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.MeshToBoundingRectangleSettings` class.
	
		Arguments:
			self (`pydataset.manipulators.pose.MeshToBoundingRectangleSettings`): Instance to initialize.
			mesh_data_feature_name                                       (`str`): Name of the feature containing the mesh of the object.
			rotation_feature_name                                        (`str`): Name of the feature containing the rotation matrix, vector or quaternion.
			translation_feature_name                                     (`str`): Name of the feature containing the translation vector.
			camera_k_feature_name                                        (`str`): Name of the feature containing the camera k matrix.
			camera_dist_coeffs_feature_name                              (`str`): Name of the feature containing the camera distortion coefficient vector.
			bounding_rectangle_feature_name                              (`str`): Name of the feature where to save the bounding rectangle.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( mesh_data_feature_name, str )
		assert pytools.assertions.type_is( rotation_feature_name, str )
		assert pytools.assertions.type_is( translation_feature_name, str )
		assert pytools.assertions.type_is( camera_k_feature_name, str )
		assert pytools.assertions.type_is( camera_dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( bounding_rectangle_feature_name, str )

		super( MeshToBoundingRectangleSettings, self ).__init__( **kwargs )

		self._mesh_data_feature_name          = mesh_data_feature_name
		self._rotation_feature_name           = rotation_feature_name
		self._translation_feature_name        = translation_feature_name
		self._camera_k_feature_name           = camera_k_feature_name
		self._camera_dist_coeffs_feature_name = camera_dist_coeffs_feature_name
		self._bounding_rectangle_feature_name = bounding_rectangle_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mesh_data_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._mesh_data_feature_name

	# def mesh_data_feature_name ( self )

	# --------------------------------------------------

	@mesh_data_feature_name.setter
	def mesh_data_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._mesh_data_feature_name = value
	
	# def mesh_data_feature_name ( self, value )
	
	# --------------------------------------------------

	@property
	def rotation_feature_name ( self ):
		"""
		Name of the feature containing the rotation matrix, vector or quaternion (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._rotation_feature_name

	# def rotation_feature_name ( self )

	# --------------------------------------------------

	@rotation_feature_name.setter
	def rotation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._rotation_feature_name = value
	
	# def rotation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_feature_name ( self ):
		"""
		Name of the feature containing the translation vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._translation_feature_name

	# def translation_feature_name ( self )

	# --------------------------------------------------

	@translation_feature_name.setter
	def translation_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._translation_feature_name = value
	
	# def translation_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_k_feature_name ( self ):
		"""
		Name of the feature containing the camera k matrix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._camera_k_feature_name

	# def camera_k_feature_name ( self )

	# --------------------------------------------------

	@camera_k_feature_name.setter
	def camera_k_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_k_feature_name = value
	
	# def camera_k_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_dist_coeffs_feature_name ( self ):
		"""
		Name of the feature containing the camera distortion coefficient vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._camera_dist_coeffs_feature_name

	# def camera_dist_coeffs_feature_name ( self )

	# --------------------------------------------------

	@camera_dist_coeffs_feature_name.setter
	def camera_dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_dist_coeffs_feature_name = value
	
	# def camera_dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def bounding_rectangle_feature_name ( self ):
		"""
		Name of the feature where to save the bounding rectangle (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )

		return self._bounding_rectangle_feature_name

	# def bounding_rectangle_feature_name ( self )

	# --------------------------------------------------

	@bounding_rectangle_feature_name.setter
	def bounding_rectangle_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingRectangleSettings )
		assert pytools.assertions.type_is( value, str )

		self._bounding_rectangle_feature_name = value
	
	# def bounding_rectangle_feature_name ( self, value )

# class MeshToBoundingRectangleSettings ( MapperSettings )