# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###  CLASS CONTROL-POINTS-NORMALIZER-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ControlPointsNormalizerSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'control_points_feature_name',
		'image_feature_name'
		]
	)
class ControlPointsNormalizerSettings ( MapperSettings ):
	"""
	Settings of the `pykeras.manipulators.pose.ControlPointsNormalizerSettings` class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		control_points_feature_name = 'bb8',
		image_feature_name = 'image',
		**kwargs
		):
		"""
		Creates a new instance of the `pydataset.manipulators.pose.ControlPointsNormalizerSettings` class.

		Arguments:
			control_points_feature_name (`str`): Name of the feature containing the control points.
			image_feature_name          (`str`): Name of the feature containing the image.
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizerSettings )
		assert pytools.assertions.type_is( control_points_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )

		super( ControlPointsNormalizerSettings, self ).__init__( **kwargs )

		self._control_points_feature_name = control_points_feature_name
		self._image_feature_name          = image_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def control_points_feature_name ( self ):
		"""
		Name of the feature containing the control points (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizerSettings )

		return self._control_points_feature_name

	# def control_points_feature_name ( self )

	# --------------------------------------------------

	@control_points_feature_name.setter
	def control_points_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._control_points_feature_name = value
	
	# def control_points_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the feature containing the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizerSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ControlPointsNormalizerSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
	
	# def image_feature_name ( self, value )

# class ControlPointsNormalizerSettings ( MapperSettings )