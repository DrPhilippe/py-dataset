# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.data
import pydataset.dataset
from ..mapper                       import Mapper
from .mesh_to_bounding_box_settings import MeshToBoundingBoxSettings

# ##################################################
# ###         CLASS MESH-TO-BOUNDING-BOX         ###
# ##################################################

class MeshToBoundingBox ( Mapper ):
	"""
	Mapper used to compute the bounding box of an object under a given pose.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the `pydataset.manipulators.pose.MeshToBoundingBox` class.

		Arguments:
			self             (`pydataset.manipulators.pose.MeshToBoundingBox`): Instance to initialize.
			settings (`pydataset.manipulators.pose.MeshToBoundingBoxSettings`): Settings used.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBox )
		assert pytools.assertions.type_is_instance_of( settings, MeshToBoundingBoxSettings )

		super( MeshToBoundingBox, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_bounding_box ( self, example ):
		"""
		Returns the bounding box of the mesh of the given example.
		
		Arguments:
			self (`pydataset.manipualtors.MeshToBoundingRectangle`): Mesh to bounding rectangle mapper.
			example                   (`pydataset.dataset.Example`): Example of which to compute the bounding rectangle.

		Returns:
			`numpy.ndarray`: Mesh bounding box with shape `[8, 3]` and dtype `float32`.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBox )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Get the mesh data
		mesh_data = example.get_feature( self.settings.mesh_data_feature_name ).value

		# Create bb
		bb = mesh_data.get_bounding_box()

		return bb
	
	# def get_bounding_box ( self, example )

	# --------------------------------------------------

	def project_points_if_needed ( self, example, points ):
		"""
		Projects the given points to model space, scene space or image plane depending on the settings.

		Arguments:
			self (`pydataset.manipulators.pose.MeshToBoundingBox`): Bounding box creator.
			example                  (`pydataset.dataset.Example`): Current example.
			points                               (`numpy.ndarray`): Set of 3D points with shape `[-1, 3]` and dtype `float32`.

		Returns:
			`numpy.ndarray`: Projected points with dtype `float32` and shape `[-1, 3]` or `[-1, 2]` in image plane.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBox )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( points, numpy.ndarray )
		assert pytools.assertions.equal( points.dtype, numpy.float32 )
		assert pytools.assertions.equal( len(points.shape), 2 )
		assert pytools.assertions.equal( points.shape[1], 3 )

		# Get the number of points
		nb_points = points.shape[ 0 ]

		# Project bounding box to camera space or image plane ?
		if self.settings.rotation_feature_name and self.settings.translation_feature_name:

			# get features
			r = example.get_feature( self.settings.rotation_feature_name ).value
			t = example.get_feature( self.settings.translation_feature_name ).value

			# Project to image plane ?
			if self.settings.camera_k_feature_name:
				# print( 'IMAGE PLANE' )

				# get features
				D = example.get_feature( self.settings.camera_k_feature_name ).value

				# Convert rotation to euler angles if necessary
				r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( r )

				# optional camera dist coefficients
				if self.settings.camera_dist_coeffs_feature_name:
					dist_coeffs = example.get_feature( settings.camera_dist_coeffs_feature_name ).value
				else:
					dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

				# Project points to image plane
				points, jacobian = cv2.projectPoints( points, r, t, D, dist_coeffs )
				points = numpy.reshape( points, [nb_points, 2] )

			# Project to camera space ?
			else:
				# Convert bounding box to homogeneous coordinates
				points = numpy.hstack(
					(points, numpy.ones([nb_points,1], dtype=numpy.float32 )),
					)

				# Convert rotation to a matrix if necessary
				R = pydataset.pose_utils.convert_rotation_to_matrix_if_needed( r )

				# Compose the affine transform
				T = pydataset.pose_utils.affine_transform( R, t )

				# Project bounding box in camera space
				points = numpy.matmul( points, T.transpose() )

				# Discar the homogeneous coordinates
				points = points[ :, 0:3 ]
				points = numpy.reshape( points, [nb_points, 3] )
				
			# if self.settings.camera_k_feature_name

		# if self.settings.rotation_feature_name and self.settings.translation_feature_name
		
		return points

	# def project_points_if_needed ( self, points )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Computes the bounding box in the given example.

		Arguments:
			self (`pydataset.manipualtors.MeshToBoundingBox`): Mesh to bounding box mapper.
			example             (`pydataset.dataset.Example`): Example of which to compute the bounding box.

		Returns:
			`pydataset.dataset.Example`: Example onced its bounding box was computed.
		"""
		assert pytools.assertions.type_is_instance_of( self, MeshToBoundingBox )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get bounding box
		bb = self.get_bounding_box( example )

		# Project to camera space or image plane ?
		bb = self.project_points_if_needed( example, bb )

		# Save the bb
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.bounding_box_feature_name,
			            value = bb
			)
		
		return example

	# def map ( self, example )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings used by a `pydataset.manipulators.pose.MeshToBoundingBox` mapper.

		Arguments:
			cls (`type`): The type `pydataset.manipulators.pose.MeshToBoundingBox`.

		Returns:
			`pydataset.manipulators.pose.MeshToBoundingBoxSettings`: The settings.
		"""
		assert pytools.assertions.type_is( cls, type )

		return MeshToBoundingBoxSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MeshToBoundingBox ( Mapper )