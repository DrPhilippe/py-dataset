# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# LOCALS
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###    CLASS POSE-ERROR-AGREGATOR-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PoseErrorAgregatorSettings',
	   namespace = 'pydataset.manipulators.pose',
	fields_names = [
		'category_feature_name',
		'depth_feature_name',
		'depth_scale_feature_name',
		'render_depth_feature_name',
		'render_depth_scale_feature_name',
		'mask_feature_name',
		'camera_k_feature_name',
		'outlier_threshold',
		'output_filepath',
		'output_directorypath',
		'output_filename_format'
		]
	)
class PoseErrorAgregatorSettings ( AgregatorSettings ):
	"""
	Settings used to agregate the pose error.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		category_feature_name = 'category_index',
		depth_feature_name = 'depth',
		depth_scale_feature_name = 'depth_scale',
		render_depth_feature_name = 'render_depth',
		render_depth_scale_feature_name = 'render_depth_scale',
		mask_feature_name = 'mask',
		camera_k_feature_name = 'K',
		outlier_threshold = 50., # 50 mm = 5 cm
		output_filepath = 'pose_error.csv',
		output_directorypath = 'pose_errors',
		output_filename_format = '{}_error_{:07.4f}.png',
		**kwargs
		):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( category_feature_name, str )
		assert pytools.assertions.type_is( depth_feature_name, str )
		assert pytools.assertions.type_is( depth_scale_feature_name, str )
		assert pytools.assertions.type_is( render_depth_feature_name, str )
		assert pytools.assertions.type_is( render_depth_scale_feature_name, str )
		assert pytools.assertions.type_is( mask_feature_name, str )
		assert pytools.assertions.type_is( camera_k_feature_name, str )
		assert pytools.assertions.type_is( outlier_threshold, float )
		assert pytools.assertions.type_is_instance_of( output_filepath, (str, pytools.path.FilePath) )
		output_filepath = pytools.path.FilePath.ensure( output_filepath )
		assert pytools.assertions.equal( output_filepath.extension(), '.csv' )
		assert pytools.assertions.type_is_instance_of( output_directorypath, (str, pytools.path.DirectoryPath) )
		output_directorypath = pytools.path.DirectoryPath.ensure( output_directorypath )
		assert pytools.assertions.type_is( output_filename_format, str )

		super( PoseErrorAgregatorSettings, self ).__init__( **kwargs )

		self._category_feature_name           = category_feature_name
		self._depth_feature_name              = depth_feature_name
		self._depth_scale_feature_name        = depth_scale_feature_name
		self._render_depth_feature_name       = render_depth_feature_name
		self._render_depth_scale_feature_name = render_depth_scale_feature_name
		self._mask_feature_name               = mask_feature_name
		self._camera_k_feature_name           = camera_k_feature_name
		self._outlier_threshold               = outlier_threshold
		self._output_filepath                 = output_filepath
		self._output_directorypath            = output_directorypath
		self._output_filename_format          = output_filename_format

	# def __init__ ( ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def category_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._category_feature_name
	# def category_feature_name ( self )

	# --------------------------------------------------

	@category_feature_name.setter
	def category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._category_feature_name = value
	# def category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._depth_feature_name
	# def depth_feature_name ( self )

	# --------------------------------------------------

	@depth_feature_name.setter
	def depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._depth_feature_name = value
	# def depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def depth_scale_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._depth_scale_feature_name
	# def depth_scale_feature_name ( self )

	# --------------------------------------------------

	@depth_scale_feature_name.setter
	def depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._depth_scale_feature_name = value
	# def depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_depth_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._render_depth_feature_name
	# def render_depth_feature_name ( self )

	# --------------------------------------------------

	@render_depth_feature_name.setter
	def render_depth_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._render_depth_feature_name = value
	# def render_depth_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def render_depth_scale_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._render_depth_scale_feature_name
	# def render_depth_scale_feature_name ( self )

	# --------------------------------------------------

	@render_depth_scale_feature_name.setter
	def render_depth_scale_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._render_depth_scale_feature_name = value
	# def render_depth_scale_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def mask_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._mask_feature_name
	# def mask_feature_name ( self )

	# --------------------------------------------------

	@mask_feature_name.setter
	def mask_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._mask_feature_name = value
	# def mask_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_k_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._camera_k_feature_name
	# def camera_k_feature_name ( self )

	# --------------------------------------------------

	@camera_k_feature_name.setter
	def camera_k_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._camera_k_feature_name = value
	# def camera_k_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def outlier_threshold ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._outlier_threshold
	# def outlier_threshold ( self )

	# --------------------------------------------------

	@outlier_threshold.setter
	def outlier_threshold ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, float )
		self._outlier_threshold = value
	# def outlier_threshold ( self, value )

	# --------------------------------------------------

	@property
	def output_filepath ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._output_filepath
	# def output_filepath ( self )

	# --------------------------------------------------

	@output_filepath.setter
	def output_filepath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.FilePath) )
		value = pytools.path.FilePath.ensure( value )
		assert pytools.assertions.equal( value.extension(), '.csv' )
		self._output_filepath = value
	# def output_filepath ( self, value )

	# --------------------------------------------------

	@property
	def output_directorypath ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._output_directorypath
	# def output_directorypath ( self )

	# --------------------------------------------------

	@output_directorypath.setter
	def output_directorypath ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )
		self._output_directorypath = pytools.path.DirectoryPath.ensure( value )
	# def output_directorypath ( self, value )

	# --------------------------------------------------

	@property
	def output_filename_format ( self ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		return self._output_filename_format
	# def output_filename_format ( self )

	# --------------------------------------------------

	@output_filename_format.setter
	def output_filename_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, PoseErrorAgregatorSettings )
		assert pytools.assertions.type_is( value, str )
		self._output_filename_format = value
	# def output_filename_format ( self, value )

# class PoseErrorAgregatorSettings ( Agregator )