# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
import pydataset.dataset
from .agregator                  import Agregator
from .ndarray_agregator_settings import NDArrayAgregatorSettings

# ##################################################
# ###          CLASS NDARRAY-AGREGATOR           ###
# ##################################################

class NDArrayAgregator ( Agregator ):
	"""
	Base class for agregators.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings, result_mapper=None ):
		"""
		Initializes a new instance of the manipulator class.

		Arguments:
			self             (`pydataset.manipulators.NDArrayAgregator`): Instance to initialize.
			settings (`pydataset.manipulators.AgregatorSettings`): NDArrayAgregator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )
		assert pytools.assertions.type_is_instance_of( settings, NDArrayAgregatorSettings )

		# Init parent class
		super( NDArrayAgregator, self ).__init__( settings )

		# Init fields
		self._example_index = 0
		self._number_of_examples = 0
		self._result = None
		self._result_mapper = result_mapper

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def result ( self ):
		"""
		Agregation result (`dict` of `str` to `numpy.ndarray` or `numpy.ndarray`).

		If categories are defiened using the category name feature name, the result is grouped in 
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )

		return self._result
	
	# def result ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.NDArrayAgregator`): Agregator.
			example            (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		value = example.get_feature( self.settings.ndarray_feature_name ).value
		
		# Are group used ?
		if self.settings.category_name_feature_name:
			
			# Get/Check the group name
			category_name = example.get_feature( self.settings.category_name_feature_name ).value
			assert isinstance( category_name, str )

			# Initialize the result ?
			if self._result is None:
				self._result = {}

			# Initialize the result for this category ?
			if category_name not in self._result:
				self._result[ category_name ] = []

			# Assign the value in the result
			self._result[ category_name ].append( value ) 

		else:
			# Is the result initialized ?
			if self._result is None:

				# If the value is a numpy ndarray ?
				if isinstance( value, numpy.ndarray ):

					# Get its shape and dtype
					shape = list( value.shape )
					dtype = value.dtype
				
				# otherwise
				else:

					# Shape is [1] qnd dtype is the type of the value
					shape = [1]
					dtype = type( value )

				# Initialize the result
				self._result = numpy.empty( [self._number_of_examples, *shape], dtype=dtype )
			
			# if self._result is None

			# Assign the value in the result
			self._result[ self._example_index ] = value

		# if self.settings.category_name_feature_name

		# Increment exqmple index
		self._example_index += 1

	# def agregate ( self, example )

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before agregation starts.

		Arguments:
			self (`pydataset.manipualtors.NDArrayAgregator`): Agregator.
			root_group           (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                       (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( NDArrayAgregator, self ).begin( root_group, number_of_examples )

		self._example_index      = 0
		self._number_of_examples = number_of_examples
		self._result             = None

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after agregation took place.

		Arguments:
			self (`pydataset.manipualtors.NDArrayAgregator`): Agregator.
			root_group           (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                       (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( NDArrayAgregator, self ).end( root_group, number_of_examples )
		
		# Are group used ?
		if self.settings.category_name_feature_name:

			# Convert the list of result in each category to numpy.ndarray
			for category_name in self._result.keys():

				self._result[ category_name ] = numpy.asarray( self._result[ category_name ] )

				if self._result_mapper:

					self._result[ category_name ] = self._result_mapper( self._result[ category_name ], category_name )

			# for category_name in self._result.keys()

		# Is a result mapper defined
		elif self._result_mapper:

			# Map the result
			self._result = self._result_mapper( self._result )

		# if self.settings.category_name_feature_name

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def save_result_as_text ( self, filepath, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayAgregator )
		assert pytools.assertions.type_is_instance_of( filepath, (str, pytools.path.FilePath) )

		# Are group used ?
		if self.settings.category_name_feature_name:

			# Check filepath
			assert pytools.assertions.type_is( filepath, str )

			# Save each group in its csv
			for category_name in self._result.keys():

				# Compose the path of the text file
				fp = pytools.path.FilePath.format( filepath, category_name )

				# Save it
				numpy.savetxt( str(fp), self._result[category_name], **kwargs )

			# for category_name in self._result.keys()

		else:
			# Save a single array in a single file
			numpy.savetxt( str(filepath), self._result, **kwargs )

		# if self.settings.category_name_feature_name

	# def save_result_as_text ( self, filepath, **kwargs )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, result_mapper=None, **kwargs ):
		"""
		Creates a new ndarray agregator instance.

		Arguments:
			cls               (`type`): Type of the manipulator to create.
			result_mapper (`callable`): function used to map the result(s).
		
		Returns:
			`pydataset.manipulators.NDArrayAgregator`: NDArrayAgregator of type `cls`.
		"""

		# Create the settings
		settings = cls.create_settings( **kwargs )

		# Create the manipulator
		return cls( settings, result_mapper )

	# def create ( cls, result_mapper, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return NDArrayAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class NDArrayAgregator ( Agregator )