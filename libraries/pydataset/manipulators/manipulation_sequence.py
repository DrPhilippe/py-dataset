# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pydataset
import pytools

# LOCALS
from .manipulator                    import Manipulator
from .manipulation_sequence_settings import ManipulationSequenceSettings

# ##################################################
# ###        CLASS MANIPULATION-SEQUENCE         ###
# ##################################################

class ManipulationSequence ( Manipulator ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, manipulators, settings ):
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		assert pytools.assertions.type_is( manipulators, list )
		assert pytools.assertions.list_items_type_is( manipulators, pydataset.manipulators.Manipulator )
		assert pytools.assertions.type_is_instance_of( settings, ManipulationSequenceSettings )

		super( ManipulationSequence, self ).__init__( settings )

		self._manipulators = [manipulator for manipulator in manipulators]

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def manipulators ( self ):
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		return self._manipulators

	# --------------------------------------------------

	@manipulators.setter
	def manipulators ( self ):
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		return self._manipulators

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.ManipulationSequence`): Manipulator sequence used to manipulate the example.
			root_group               (`pydataset.dataset.Group`): Group containing the examples that will be manipulated.
			number_of_examples                           (`int`): Number of examples to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( ManipulationSequence, self ).begin( root_group, number_of_examples )

		for manipulator in self.manipulators:

			# Warn
			if manipulator.settings.url_search_pattern:
				print( "WARNING: Overriding manipulator '{}' url search pattern ({}) with this sequence url search pattern ({})".format(
					manipulator.settings.name,
					manipulator.settings.url_search_pattern,
					self.settings.url_search_pattern
					))

			# Ovveride URL search pattern
			manipulator.settings.url_search_pattern = self.settings.url_search_pattern

			manipulator.begin( root_group, number_of_examples )

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this manipulator sequence.

		Arguments:
			self (`pydataset.manipualtors.ManipulationSequence`): Manipulator sequence to apply to the example.
			example                (`pydataset.dataset.Example`): Example to agregate.
			index                                        (`int`): Index of the example being agregated.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )
		
		for manipulator in self.manipulators:
			
			example = manipulator.manipulate( example, index )
			
			if not isinstance( example, pydataset.dataset.Example ):
				raise ValueError( "Manipulator name '{}' did not return an example, it instead returned a '{}'".format(
					manipulator.settings.name,
					type(example)
					))

		# Return the example
		return example

	# def manipulate ( self, example, index )
	
	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		Method called after manipulation append.

		Arguments:
			self (`pydataset.manipualtors.ManipulationSequence`): Manipulator sequence used to manipulate the example.
			root_group               (`pydataset.dataset.Group`): Group containing the examples that have been manipulated.
			number_of_examples                           (`int`): Number of examples manipulated.
		"""
		assert pytools.assertions.type_is_instance_of( self, ManipulationSequence )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		for manipulator in self.manipulators:
			manipulator.end( root_group, number_of_examples )

	# def end ( self, root_group, number_of_examples )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, *args, **kwargs ):
		"""
		Creates a new manipulator sequence.

		Arguments:
			cls                                  (`type`): Type of the manipulator sequence to create.
			*args (`pydataset.manipulators.Manipulator`s): Manipulators to apply in sequence.
			**kwargs                    (`str`s to `any`): Arguments forwarded to the settings of the manipulator.

		Returns:
			`pydataset.manipulators.ManipulatorSequence`: Manipulator sequence of type `cls`.
		"""
		assert pytools.assertions.type_is( cls, type )
		manipulators = list(args)
		assert pytools.assertions.list_items_type_is( manipulators, pydataset.manipulators.Manipulator )

		# Create the settings
		settings = cls.create_settings( **kwargs )

		# Create the manipulator
		return cls( manipulators, settings )

	# def create ( cls, manipulators, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the manipulator sequence type `cls`.

		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings of the manipulator sequence.

		Returns:
			`pydataset.manipulators.ManipulatorSequenceSettings`: Settings for the manipulator of type `cls`.
		"""
		
		return ManipulationSequenceSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ManipulationSequence ( Manipulator )