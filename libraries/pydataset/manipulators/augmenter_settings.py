# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .manipulator_settings import ManipulatorSettings

# ##################################################
# ###          CLASS AUGMENTER-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'AugmenterSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = []
	)
class AugmenterSettings ( ManipulatorSettings ):
	"""
	Augmenter settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the `pydataset.manipulators.AugmenterSettings` class.

		Arguments:
			self (`pydataset.manipualtors.AugmenterSettings`): Instance to initialize.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			dst_group_url      (`str`): URL of the group where to save the augmented examples.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, AugmenterSettings )

		super( AugmenterSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	

# class AugmenterSettings ( ManipulatorSettings )