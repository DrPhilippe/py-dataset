# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .manipulator_settings import ManipulatorSettings

# ##################################################
# ###           CLASS MAPPER-SETTINGS            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MapperSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = []
	)
class MapperSettings ( ManipulatorSettings ):
	"""
	Mapper settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, **kwargs ):
		"""
		Initializes a new instance of the mapper settings class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipualtors.MapperSettings`): Instance to initialize.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			dst_group_url      (`str`): URL of the group where to save the mapped examples.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, MapperSettings )

		super( MapperSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )
		
# class MapperSettings ( ManipulatorSettings )