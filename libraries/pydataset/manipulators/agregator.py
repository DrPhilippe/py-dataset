# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .agregator_settings import AgregatorSettings
from .manipulator        import Manipulator

# ##################################################
# ###              CLASS AGREGATOR               ###
# ##################################################

class Agregator ( Manipulator ):
	"""
	Base class for agregators.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the agregator class.

		Arguments:
			self             (`pydataset.manipulators.Agregator`): Instance to initialize.
			settings (`pydataset.manipulators.AgregatorSettings`): Agregator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, Agregator )
		assert pytools.assertions.type_is_instance_of( settings, AgregatorSettings )

		super( Agregator, self ).__init__( settings )

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.Agregator`): Agregator used to agregate the example.
			example     (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, Agregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		raise NotImplementedError()
		
	# def agregate ( self, example )

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.Agregator`): Agregator used to agregate the example.
			example     (`pydataset.dataset.Example`): Example to agregate.
			index                             (`int`): Index of the example being agregated.
		"""
		assert pytools.assertions.type_is_instance_of( self, Agregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		
		# Agregate this example
		self.agregate( example )

		# Return the example
		return example

	# def manipulate ( self, example, index )

# class Agregator ( Manipulator )