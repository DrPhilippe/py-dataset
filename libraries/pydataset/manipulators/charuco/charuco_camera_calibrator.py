# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy
import random

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper                            import Mapper
from .charuco_camera_calibrator_settings import CharucoCameraCalibratorSettings

# ##################################################
# ###            CLASS BOARD-DETECTOR            ###
# ##################################################

class CharucoCameraCalibrator ( Mapper ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the charuco camera calibrator class.
		
		Arguments:
			self             (`pydataset.aruco.CharucoCameraCalibrator`): Charuco camera calibrator instance to initialize.
			settings (`pydataset.aruco.CharucoCameraCalibratorSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCameraCalibrator )
		assert pytools.assertions.type_is_instance_of( settings, CharucoCameraCalibratorSettings )

		super( CharucoCameraCalibrator, self ).__init__( settings )

		self.all_corners = []
		self.all_ids     = []
		self.all_sizes   = []

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples, url_search_pattern_prefix ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.Manipulator`): Manipulator used to manipulate the example.
			root_group      (`pydataset.dataset.Group`): Group containing the examples that will be manipulated.
		"""
		
		self.all_corners = []
		self.all_ids     = []
		self.all_sizes   = []

	# def begin ( self, root_group )

	# --------------------------------------------------

	def end ( self, root_group, number_of_examples, url_search_pattern_prefix ):
		"""
		Method called after manipulation append.

		Arguments:
			self (`pydataset.manipualtors.Manipulator`): Manipulator used to manipulate the example.
			root_group      (`pydataset.dataset.Group`): Group containing the examples that have been manipulated.
		"""

		# Number of examples
		count = len(self.all_corners)
		
		# Check that all images have the same size
		image_size = self.all_sizes[ 0 ]
		for i in range( 1, count ):
			assert self.all_sizes[ i ] == image_size 

		# Get the charuco board
		board_config = self.settings.board_config.get_cv2_board()

		# Create an index table of all the considered examples
		indexes = list( range(count) )

		# Shuffle it and only keep the 100 first examples
		random.shuffle( indexes )
		final_corners = []
		final_ids     = []
		for i in range(min(count, 100)):
			index = indexes[ i ]
			final_corners.append( self.all_corners[index] )
			final_ids.append( self.all_ids[index] )
		final_corners = numpy.asarray(final_corners)
		final_corners = numpy.reshape(final_corners, [100, 49, 1, 2])
		final_ids = numpy.asarray(final_ids)

		# Intial camera parameters and distance coefficients
		K = numpy.array([
			[ 2000.0,    0.0, image_size[ 0 ]/2.0 ],
			[    0.0, 2000.0, image_size[ 1 ]/2.0 ],
			[    0.0,    0.0,                      1.0 ]
			])
		d = numpy.zeros( (5) )

		# Run camera parameter estimation
		ret, K, d, r, t, std1, std2, err = cv2.aruco.calibrateCameraCharucoExtended(
			final_corners,
			final_ids,
			board_config,
			image_size,
			K,
			d
			)
		print( K.shape, K.dtype )
		print( d.shape, d.dtype )

		K = numpy.reshape( K, [3, 3] ).astype(numpy.float32)
		d = numpy.reshape( d, [5] ).astype(numpy.float32)

		# Get the example where to save the camera matrix / dist coefs
		example = pydataset.dataset.get( url_search_pattern_prefix + self.settings.url_calibration_example )
		
		# Create or update the features
		example.create_or_update_feature(
			     feature_name = self.settings.camera_matrix_feature_name,
			feature_data_type = pydataset.data.NDArrayFeatureData,
			            shape = pydataset.data.ShapeData( 3, 3 ),
			            value = K,
			            dtype = numpy.dtype('float32')
			)
		example.create_or_update_feature(
			     feature_name = self.settings.dist_coeffs_feature_name,
			feature_data_type = pydataset.data.NDArrayFeatureData,
			            shape = pydataset.data.ShapeData( 5 ),
			            value = d,
			            dtype = numpy.dtype('float32')
			)

	# def end ( self, root_group )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper.

		Arguments:
			self (`pydataset.manipualtors.Mapper`): Mapper used to map the example.
			example  (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""

		# Get features
		image_shape     = example.get_feature( self.settings.image_feature_name ).shape
		charuco_corners = example.get_feature( self.settings.charuco_corners_feature_name ).value

		if not numpy.isnan( numpy.sum(charuco_corners) ):
			nb = charuco_corners.shape[0]
			self.all_corners.append( charuco_corners )
			self.all_ids.append( numpy.arange(nb, dtype=numpy.int32) )
			self.all_sizes.append( (image_shape[0], image_shape[1]) )
		
		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return CharucoCameraCalibratorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class CharucoCameraCalibrator ( pydataset.manipulators.Mapper )