# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from pydataset            import charuco
from pydataset.data       import ShapeData
from pydataset.dataset    import Example, NDArrayFeatureData
from ..mapper             import Mapper
from .calibrator_settings import CalibratorSettings

# ##################################################
# ###              CLASS CALIBRATOR              ###
# ##################################################

class Calibrator ( Mapper ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the class `pydataset.manipulators.charuco.Calibrator`.
		
		Arguments:
			self             (`pydataset.manipulators.charuco.Calibrator`): Calibrator instance to initialize.
			settings (`pydataset.manipulators.charuco.CalibratorSettings`): Calibrator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, Calibrator )
		assert pytools.assertions.type_is_instance_of( settings, CalibratorSettings )

		super( Calibrator, self ).__init__( settings )

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Prepares this calibrator before going through calibration examples.

		Arguments:
			self (`pydataset.manipulators.charuco.Calibrator`): Calibrator.
			root_group             (`pydataset.dataset.Group`): Group containing the calibration sequence.
		"""
		assert pytools.assertions.type_is_instance_of( self, Calibrator )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		self._board_data    = root_group.get_feature( self.settings.board_data_feature_name ).value
		self._board_corners = []
		self._board_ids     = []
		self._image_size    = None

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Use this example for calibration of the camera.

		Arguments:
			self (`pydataset.manipulators.charuco.Calibrator`): Calibrator.
			example              (`pydataset.dataset.Example`): Example used for calibration.

		Returns:
			`pydataset.dataset.Example`: Example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Calibrator )
		assert pytools.assertions.type_is_instance_of( example, Example )

		# Get the image on which to detect the board
		image = example.get_feature( self.settings.image_feature_name ).value

		# Convert image to grayscale
		grayscale_image = cv2.cvtColor( image, cv2.COLOR_BGR2GRAY )

		# Detect the markers corners
		markers_corners, markers_ids = charuco.detect_markers( self._board_data, grayscale_image )

		# Interpolate the position of the charuco board corners
		board_corners, board_ids = charuco.interpolate_corners( self._board_data, grayscale_image, markers_corners, markers_ids )
		
		# Save the markers corners in the example
		example.create_or_update_feature(
			     feature_name = self.settings.markers_corners_feature_name,
			feature_data_type = NDArrayFeatureData,
			            value = charuco.pack_detected_markers( markers_corners, markers_ids, self._board_data.number_of_markers )
			)

		# Save the charuco board corners in the example
		example.create_or_update_feature(
			     feature_name = self.settings.board_corners_feature_name,
			feature_data_type = NDArrayFeatureData,
			            value = charuco.pack_detected_corners( board_corners, board_ids, self._board_data.number_of_corners )
			)

		# Keep the charuco corners, id and image size for calibration

		if not numpy.isnan( numpy.sum( board_corners ) ):
			
			board_corners = numpy.asarray( board_corners, dtype=numpy.float32 )
			board_ids     = numpy.asarray( board_ids,     dtype=numpy.int32   )
			board_corners = numpy.reshape( board_corners, [-1, 1, 2]          )
			board_ids     = numpy.reshape( board_ids,     [-1]                )

			self._board_corners.append( board_corners )
			self._board_ids.append( board_ids )
			
			if not self._image_size:
				self._image_size = (image.shape[0], image.shape[1])

		return example

	# def map ( self, example )
	
	# --------------------------------------------------

	def end ( self, root_group, number_of_examples ):
		"""
		method called after calibration examples have been seen.

		Arguments:
			self (`pydataset.manipualtors.Calibrator`): Calibrator.
			root_group     (`pydataset.dataset.Group`): Group containing the calibration sequence.
		"""
		assert pytools.assertions.type_is_instance_of( self, Calibrator )
		assert pytools.assertions.type_is( number_of_examples, int )
		
		# Compute number of samples
		number_of_samples = len( self._board_corners )

		# Modify settings ?
		if number_of_samples < self.settings.maximum_number_of_samples:
			self.settings.maximum_number_of_samples = number_of_samples

		# Convert corners and ids to numpy
		# self._board_corners = numpy.asarray( self._board_corners, dtype=numpy.float32 )
		# self._board_ids     = numpy.asarray( self._board_ids,     dtype=numpy.int32   )
		# self._board_corners = numpy.reshape( self._board_corners, [number_of_samples, self._board_data.number_of_corners, 1, 2] )
		# self._board_ids     = numpy.reshape( self._board_ids,     [number_of_samples, self._board_data.number_of_corners] )

		# Calibrate camera
		ret, K, d = charuco.calibrate_camera(
			self._board_data,
			self._board_corners,
			self._board_ids,
			self._image_size,
			self.settings.maximum_number_of_samples
			)

		if not ret:
			raise RuntimeError( 'Failed to calibrate camera' )

		# Get the example where to save the camera matrix / dist coefs
		if self.settings.url_calibration_example:
			example = root_group.dataset.get( self.settings.url_calibration_example )
		else:
			example = root_group.dataset
		
		# Create or update the features
		example.create_or_update_feature(
			     feature_name = self.settings.camera_matrix_feature_name,
			feature_data_type = NDArrayFeatureData,
			      auto_update = False,
			            shape = ShapeData( 3, 3 ),
			            value = K,
			            dtype = numpy.dtype('float32')
			)
		example.create_or_update_feature(
			     feature_name = self.settings.dist_coeffs_feature_name,
			feature_data_type = NDArrayFeatureData,
			      auto_update = False,
			            shape = ShapeData( 5 ),
			            value = d,
			            dtype = numpy.dtype('float32')
			)
		example.update()

	# def end ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return CalibratorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class Calibrator ( pydataset.manipulators.Mapper )