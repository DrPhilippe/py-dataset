# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###         CLASS CALIBRATOR-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoCalibratorSettings',
	   namespace = 'pydataset.manipulators.charuco',
	fields_names = [
		'board_corners_feature_name',
		'board_data_feature_name',
		'camera_matrix_feature_name',
		'dist_coeffs_feature_name',
		'image_feature_name',
		'markers_corners_feature_name',
		'url_calibration_example',
		'maximum_number_of_samples'
		]
	)
class CalibratorSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, board_corners_feature_name='', board_data_feature_name='', camera_matrix_feature_name='',
		dist_coeffs_feature_name='', image_feature_name='', markers_corners_feature_name='', url_calibration_example='',
		maximum_number_of_samples=100, **kwargs ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( board_corners_feature_name, str )
		assert pytools.assertions.type_is( board_data_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( markers_corners_feature_name, str )
		assert pytools.assertions.type_is( url_calibration_example, str )
		assert pytools.assertions.type_is( maximum_number_of_samples, int )

		super( CalibratorSettings, self ).__init__( **kwargs )

		# input features
		self._board_data_feature_name       = board_data_feature_name
		self._image_feature_name            = image_feature_name

		# by-product features
		self._markers_corners_feature_name  = markers_corners_feature_name
		self._board_corners_feature_name    = board_corners_feature_name
		
		# producct features
		self._camera_matrix_feature_name    = camera_matrix_feature_name
		self._dist_coeffs_feature_name      = dist_coeffs_feature_name

		# other
		self._url_calibration_example       = url_calibration_example
		self._maximum_number_of_samples     = maximum_number_of_samples

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def board_data_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._board_data_feature_name

	# def board_data_feature_name ( self )

	# --------------------------------------------------
	
	@board_data_feature_name.setter
	def board_data_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._board_data_feature_name = value
		
	# def board_data_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def board_corners_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._board_corners_feature_name

	# def board_corners_feature_name ( self )

	# --------------------------------------------------

	@board_corners_feature_name.setter
	def board_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._board_corners_feature_name = value
		
	# def board_corners_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._camera_matrix_feature_name

	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------
	
	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._camera_matrix_feature_name = value
		
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._dist_coeffs_feature_name

	# def dist_coeffs_feature_name ( self )

	# --------------------------------------------------
	
	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._dist_coeffs_feature_name = value
		
	# def dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._image_feature_name

	# def image_feature_name ( self )

	# --------------------------------------------------
	
	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value
		
	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def markers_corners_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._markers_corners_feature_name

	# def markers_corners_feature_name ( self )

	# --------------------------------------------------
	
	@markers_corners_feature_name.setter
	def markers_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._markers_corners_feature_name = value
		
	# def markers_corners_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def url_calibration_example ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._url_calibration_example

	# def url_calibration_example ( self )

	# --------------------------------------------------
	
	@url_calibration_example.setter
	def url_calibration_example ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._url_calibration_example = value

	# def url_calibration_example ( self, value )

	# --------------------------------------------------

	@property
	def maximum_number_of_samples ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )

		return self._maximum_number_of_samples

	# def maximum_number_of_samples ( self )

	# --------------------------------------------------
	
	@maximum_number_of_samples.setter
	def maximum_number_of_samples ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CalibratorSettings )
		assert pytools.assertions.type_is( value, int )

		self._maximum_number_of_samples = value

	# def maximum_number_of_samples ( self, value )

# class CalibratorSettings ( MapperSettings )