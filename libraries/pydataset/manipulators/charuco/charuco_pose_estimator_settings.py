# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.manipulators
import pytools.assertions
import pytools.serialization

# LOCALS
from pydataset.data                         import CharucoBoardData
from ..mapper_settings                      import MapperSettings

# ##################################################
# ###   CLASS CHARUCO-POSE-ESTIMATOR-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoPoseEstimatorSettings',
	   namespace = 'pydataset.aruco',
	fields_names = [
		'charuco_board_data',
		'board_corners_feature_name',
		'camera_matrix_feature_name',
		'dist_coeffs_feature_name',
		'rotation_vector_feature_name',
		'translation_vector_feature_name'
		]
	)
class CharucoPoseEstimatorSettings ( MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		charuco_board_data,
		charuco_board_corners_feature_name,
		camera_matrix_feature_name,
		dist_coeffs_feature_name,
		rotation_vector_feature_name,
		translation_vector_feature_name,
		**kwargs
		):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
		assert pytools.assertions.type_is( charuco_board_corners_feature_name, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( rotation_vector_feature_name, str )
		assert pytools.assertions.type_is( translation_vector_feature_name, str )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( CharucoPoseEstimatorSettings, self ).__init__( **kwargs )

		self._charuco_board_data                 = charuco_board_data
		self._charuco_board_corners_feature_name = charuco_board_corners_feature_name
		self._camera_matrix_feature_name         = camera_matrix_feature_name
		self._dist_coeffs_feature_name           = dist_coeffs_feature_name
		self._rotation_vector_feature_name       = rotation_vector_feature_name
		self._translation_vector_feature_name    = translation_vector_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_board_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )

		return self._charuco_board_data
	
	# def charuco_board_data ( self )

	# --------------------------------------------------

	@charuco_board_data.setter
	def charuco_board_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is_instance_of( value, CharucoBoardData )

		self._charuco_board_data = value
		
	# def charuco_board_data ( self, value )

	# --------------------------------------------------

	@property
	def charuco_board_corners_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )

		return self._charuco_board_corners_feature_name
	
	# def charuco_board_corners_feature_name ( self )

	# --------------------------------------------------

	@charuco_board_corners_feature_name.setter
	def charuco_board_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._charuco_board_corners_feature_name = value
	
	# def charuco_board_corners_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		
		return self._camera_matrix_feature_name
	
	# def camera_matrix_feature_name ( self )

	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._camera_matrix_feature_name = value
	
	# def camera_matrix_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		
		return self._dist_coeffs_feature_name

	# def dist_coeffs_feature_name ( self )

	# --------------------------------------------------

	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._dist_coeffs_feature_name = value

	# def dist_coeffs_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def rotation_vector_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		
		return self._rotation_vector_feature_name

	# def rotation_vector_feature_name ( self )

	# --------------------------------------------------

	@rotation_vector_feature_name.setter
	def rotation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._rotation_vector_feature_name = value

	# def rotation_vector_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def translation_vector_feature_name ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		
		return self._translation_vector_feature_name

	# def translation_vector_feature_name ( self )

	# --------------------------------------------------

	@translation_vector_feature_name.setter
	def translation_vector_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimatorSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._translation_vector_feature_name = value

	# def translation_vector_feature_name ( self, value )

# class CharucoPoseEstimatorSettings ( MapperSettings )