# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy

# INTERNALS
import pydataset.manipulators
import pytools.assertions
import pytools.serialization

# LOCALS
from .charuco_pose_estimator_settings import CharucoPoseEstimatorSettings
from .import utils

# ##################################################
# ###        CLASS CHARUCO-POSE-ESTIMATOR        ###
# ##################################################

class CharucoPoseEstimator ( pydataset.manipulators.Mapper ):
	"""
	Estimates the 6-DOF pose of the charuco board.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the charuco pose estimator class.
		
		Arguments:
			self             (`pydataset.aruco.CharucoPoseEstimator`): Markers corners detector instance to initialize.
			settings (`pydataset.aruco.CharucoPoseEstimatorSettings`): Markers corners detector settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimator )
		assert pytools.assertions.type_is_instance_of( settings, CharucoPoseEstimatorSettings )

		super( CharucoPoseEstimator, self ).__init__( settings )

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def map ( self, example ):
		"""
		Estimate the 6-DOF pose estimation of the charuco board.

		Arguments:
			self (`pydataset.manipualtors.CharucoPoseEstimator`): Estimator used to estimate the pose of the charuco board.
			example                (`pydataset.dataset.Example`): Example for which to estimate the pose of the charuco board.

		Returns:
			`pydataset.dataset.Example`: Example onced the pose is estimated.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoPoseEstimator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the required features
		charuco_corners = example.get_feature( self.settings.charuco_corners_feature_name ).value
		camera_matrix   = example.get_feature( self.settings.camera_matrix_feature_name   ).value
		dist_coeffs     = example.get_feature( self.settings.dist_coeffs_feature_name     ).value
		nb_corners      = charuco_corners.shape[ 0 ]

		# Reshape charuco corners
		charuco_corners = numpy.reshape( charuco_corners, [nb_corners, 1, 2] )

		# Create dummy ids
		charuco_ids = numpy.arange( nb_corners, dtype=numpy.int32 )

		# Get the charuco board
		charuco = utils.get_cv2_board( self.settings.board_config )

		# Create initial R and t
		charuco_r = numpy.zeros( [3], dtype=numpy.float32 )
		charuco_t = numpy.zeros( [3], dtype=numpy.float32 )

		# Estimate pose
		ret, charuco_r, charuco_t = cv2.aruco.estimatePoseCharucoBoard( charuco_corners, charuco_ids, charuco, camera_matrix, dist_coeffs, charuco_r, charuco_t )

		# Reshape
		charuco_r = numpy.reshape( charuco_r, [3] ).astype( numpy.float32 )
		charuco_t = numpy.reshape( charuco_t, [3]    ).astype( numpy.float32 )

		# Save pose in the example
		example.create_or_update_feature(
			     feature_name = self.settings.rotation_vector_feature_name,
			feature_data_type = pydataset.data.NDArrayFeatureData,
			            value = charuco_r,
			            shape = pydataset.data.ShapeData( 3 ),
			            dtype = numpy.dtype( 'float32' )
			)
		example.create_or_update_feature(
			     feature_name = self.settings.translation_vector_feature_name,
			feature_data_type = pydataset.data.NDArrayFeatureData,
			            value = charuco_t,
			            shape = pydataset.data.ShapeData( 3 ),
			            dtype = numpy.dtype( 'float32' )
			)

		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Create the settings for a charuco pose estimators.

		Arguments:
			cls (`type`): Type of pose estimators for which to create settings.
		
		Returns:
			`pydataset.aruco.CharucoPoseEstimatorSettings`: The new settings.
		"""
		return CharucoPoseEstimatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class CharucoPoseEstimatorSettings ( pydataset.manipulators.Mapper )