# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pydataset.manipulators
import pytools.assertions
import pytools.serialization

# LOCALS
from pydataset.data import CharucoBoardData

# ##################################################
# ###   CLASS MARKERS-CORNERS-DETECTOR-SETTINGS  ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MarkersCornersDetectorSettings',
	   namespace = 'pydataset.manipulators.charuco',
	fields_names = [
		'charuco_board_data',
		'image_feature_name',
		'markers_corners_feature_name'
		]
	)
class MarkersCornersDetectorSettings ( pydataset.manipulators.MapperSettings ):
	"""
	Charuco markers corners detector settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, charuco_board_data, image_feature_name, markers_corners_feature_name, **kwargs ):
		"""
		Initializes a new instance of the board detector settings class.

		Arguments:
			self (`pydataset.dataset.MarkersCornersDetectorSettings`): Instance to initialize.
			charuco_board_data    (`pydataset.data.CharucoBoardData`): Charuco board configuration.
			image_feature_name                                (`str`): Name of the image feature where to detect the markers.
			markers_corners_feature_name                      (`str`): Name of the feature where to save the detected markers corners.
		
		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )
		assert pytools.assertions.type_is_instance_of( charuco_board_data, CharucoBoardData )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( markers_corners_feature_name, str )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( MarkersCornersDetectorSettings, self ).__init__( url_search_pattern, name )

		self._charuco_board_data           = charuco_board_data
		self._image_feature_name           = image_feature_name
		self._markers_corners_feature_name = markers_corners_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_board_data ( self ):
		"""
		Charuco board data (`pydataset.data.CharucoBoardData`)
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )

		return self._charuco_settings
	
	# def charuco_board_data ( self )

	# --------------------------------------------------

	@charuco_board_data.setter
	def charuco_board_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )
		assert pytools.assertions.type_is_instance_of( value, CharucoBoardData )
		
		self._charuco_settings = value

	# def charuco_board_data ( self, value )
		
	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		"""
		Name of the image feature where to detect the markers (`str`).	
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )

		return self._image_feature_name
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )
		assert pytools.assertions.type_is( value, str )

		self._image_feature_name = value

	# def image_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def markers_corners_feature_name ( self ):
		"""
		Name if the feature where to save the detected markers corners (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )

		return self._markers_corners_feature_name
	
	# def markers_corners_feature_name ( self )

	# --------------------------------------------------

	@markers_corners_feature_name.setter
	def markers_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetectorSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._markers_corners_feature_name = value

	# def markers_corners_feature_name ( self, value )

# class MarkersCornersDetectorSettings ( pydataset.manipulators.MapperSettings )