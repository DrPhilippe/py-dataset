# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy

# INTERNALS
import pydataset.manipulators
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper import Mapper
from .markers_corners_detector_settings import MarkersCornersDetectorSettings
from . import charuco
from . import utils

# ##################################################
# ###       CLASS MARKERS-CORNERS-DETECTOR       ###
# ##################################################

class MarkersCornersDetector ( Mapper ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the markers detector class.
		
		Arguments:
			self             (`pydataset.aruco.MarkersCornersDetector`): Markers corners detector instance to initialize.
			settings (`pydataset.aruco.MarkersCornersDetectorSettings`): Markers corners detector settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetector )
		assert pytools.assertions.type_is_instance_of( settings, MarkersCornersDetectorSettings )

		super( MarkersCornersDetector, self ).__init__( settings )

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def detect ( self, image ):
		"""
		Detects the markers of a chauco board on the given image.

		The charuco board is defined in the settings of this detector.
		
		Corners are returned as a numpy n-dimentional array of type `numpy.float32`.
		The shape of the array is Nx4x2 where N is the number of markers on the board,
		4 is the number of corners for each markers and 2 is the number of coordinates
		of each corner point (x and y).
		Non-detected markers are set to NaN values.

		Arguments:
			self (`pydataset.aruco.MarkersCornersDetector`): Markers corners detector.
			image                         (`numpy.ndarray`): Image on wich to detect the markers of a charuco board.

		Returns:
			`numpy.ndarray`: The corners of the board's markers.
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetector )
		assert pytools.assertions.type_is( image, numpy.ndarray )

		return chruco.detect_markers( image, self.settings.charuco_settings )
	
	# def detect ( self, image )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Detects the corners of the markers of a charuco board in the image of the given example using this detector.

		Arguments:
			self (`pydataset.manipualtors.MarkersCornersDetector`): Detector used to detect the markers corners.
			example                  (`pydataset.dataset.Example`): Example for which to detect the markers corners.

		Returns:
			`pydataset.dataset.Example`: Example onced the board is detected and saved.
		"""
		assert pytools.assertions.type_is_instance_of( self, MarkersCornersDetector )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the image on which to detect the board
		image = example.get_feature( self.settings.image_feature_name ).value

		# Detect the markers corners
		markers_corners = self.detect( image )
		
		# Save them in the example
		example.create_or_update_feature(
			     feature_name = self.settings.markers_corners_feature_name,
			feature_data_type = MarkersCornersFeatureData,
			            value = markers_corners
			)

		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Create the settings for a charuco markers corners detector.

		Arguments:
			cls (`type`): Type of detector for which to create settings.
		
		Named Arguments:
			charuco_settings (`pydataset.data.CharucoBoardData`): Charuco board configuration.
			image_feature_name                           (`str`): Name of the image feature where to detect the markers.
			markers_corners_feature_name                 (`str`): Name if the feature where to save the detected markers corners.
			url_search_pattern                           (`str`): URL used to search for example(s) to map.
			name                                         (`str`): Name of the mapper.

		Returns:
			`MarkersCornersDetectorSettings`: The new settings.
		"""
		return MarkersCornersDetectorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class MarkersCornersDetector ( Mapper )