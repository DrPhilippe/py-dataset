# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from pydataset.data                         import CharucoBoardData
from ..mapper_settings                      import MapperSettings
from .charuco_corners_interpolator_settings import CharucoCornersInterpolatorSettings

# ##################################################
# ###   CLASS BOARD-CORNERS-DETECTOR-SETTINGS    ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoCameraCalibratorSettings',
	   namespace = 'pydataset.manipulators.charuco',
	fields_names = [
		'charuco_settings',
		'board_corners_feature_name',
		'image_feature_name',
		'url_calibration_example',
		'camera_parameters_feature_name'
		]
	)
class CharucoCameraCalibratorSettings ( pydataset.manipulators.MapperSettings ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ (
		self,
		charuco_settings,
		image_feature_name,
		charuco_corners_feature_name,
		url_calibration_example,
		camera_matrix_feature_name,
		dist_coeffs_feature_name,
		url_search_pattern,
		name='Charuco Camera Calibrator'
		):
		"""
		Initializes a new instance of the charuco camera calibrator settings class.

		Arguments:
			self (`pydataset.dataset.CharucoCameraCalibratorSettings`): Instance to initialize.
			charuco_settings       (`pydataset.data.CharucoBoardData`): Charuco board configuration.
			image_feature_name                                 (`str`): Name of the feature containing the image of the charuco board.
			charuco_corners_feature_name                       (`str`): Name of the feature containing the detected charuco board corners.
			url_calibration_example                            (`str`): URL of the example where to save calibration features.
			camera_parameters_feature_name                     (`str`): Name of the feature create to save the camera parameters.
			url_search_pattern                                 (`str`): URL used to search for example(s) to map.
			name                                               (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCameraCalibratorSettings )
		assert pytools.assertions.type_is_instance_of( charuco_settings, CharucoBoardData )
		assert pytools.assertions.type_is( charuco_corners_feature_name, str )
		assert pytools.assertions.type_is( url_calibration_example, str )
		assert pytools.assertions.type_is( camera_matrix_feature_name, str )
		assert pytools.assertions.type_is( dist_coeffs_feature_name, str )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( CharucoCameraCalibratorSettings, self ).__init__(
			url_search_pattern,
			name
			)

		self.charuco_settings             = charuco_settings
		self.image_feature_name           = image_feature_name
		self.charuco_corners_feature_name = charuco_corners_feature_name
		self.url_calibration_example      = url_calibration_example
		self.camera_matrix_feature_name   = camera_matrix_feature_name
		self.dist_coeffs_feature_name     = dist_coeffs_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_settings ( self ):
		return self._charuco_settings
	
	# --------------------------------------------------

	@charuco_settings.setter
	def charuco_settings ( self, value ):
		self._charuco_settings = value

	# --------------------------------------------------

	@property
	def image_feature_name ( self ):
		return self._image_feature_name
	
	# --------------------------------------------------

	@image_feature_name.setter
	def image_feature_name ( self, value ):
		self._image_feature_name = value
		
	# --------------------------------------------------

	@property
	def charuco_corners_feature_name ( self ):
		return self._charuco_corners_feature_name
	
	# --------------------------------------------------

	@charuco_corners_feature_name.setter
	def charuco_corners_feature_name ( self, value ):
		self._charuco_corners_feature_name = value
	
	# --------------------------------------------------

	@property
	def url_calibration_example ( self ):
		return self._url_calibration_example
	
	# --------------------------------------------------

	@url_calibration_example.setter
	def url_calibration_example ( self, value ):
		self._url_calibration_example = value

	# --------------------------------------------------

	@property
	def camera_matrix_feature_name ( self ):
		return self._camera_matrix_feature_name
	
	# --------------------------------------------------

	@camera_matrix_feature_name.setter
	def camera_matrix_feature_name ( self, value ):
		self._camera_matrix_feature_name = value
	
	# --------------------------------------------------

	@property
	def dist_coeffs_feature_name ( self ):
		return self._dist_coeffs_feature_name

	# --------------------------------------------------

	@dist_coeffs_feature_name.setter
	def dist_coeffs_feature_name ( self, value ):
		self._dist_coeffs_feature_name = value

# class CharucoCameraCalibratorSettings ( pydataset.manipulators.MapperSettings )