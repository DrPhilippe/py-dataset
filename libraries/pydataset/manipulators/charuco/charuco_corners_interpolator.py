# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import cv2.aruco
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper                               import Mapper
from .charuco_corners_interpolator_settings import CharucoCornersInterpolatorSettings


# ##################################################
# ###       CLASS MARKERS-CORNERS-DETECTOR       ###
# ##################################################

class CharucoCornersInterpolator ( Mapper ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the charuco corners interpolator class.
		
		Arguments:
			self             (`pydataset.aruco.CharucoCornersInterpolator`): Markers corners detector instance to initialize.
			settings (`pydataset.aruco.CharucoCornersInterpolatorSettings`): Markers corners detector settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolator )
		assert pytools.assertions.type_is_instance_of( settings, CharucoCornersInterpolatorSettings )

		super( CharucoCornersInterpolator, self ).__init__( settings )

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def interpolate ( self, markers_corners, image ):
		
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolator )
		assert pytools.assertions.type_is( markers_corners, numpy.ndarray )
		assert pytools.assertions.type_is( image, numpy.ndarray )
		
		# Get the cv2 aruco board object and the number of markers
		charuco_board     = utils.get_cv2_board( self.settings.board_config )
		number_of_markers = self.settings.board_config.number_of_markers
		number_of_corners = len(charuco_board.chessboardCorners)

		# Create a dummy verctor for the ids
		markers_ids = numpy.arange( number_of_markers, dtype=numpy.int32 )
		
		# Convert image to grayscale
		gray_image = cv2.cvtColor( image, cv2.COLOR_BGR2GRAY )

		# Interpolate the corners of the charuco board
		nb_detected, charuco_corners, charuco_ids = cv2.aruco.interpolateCornersCharuco( markers_corners, markers_ids, gray_image, charuco_board )
		
		# Intialize the result array
		result      = numpy.empty( [number_of_corners, 2], dtype=numpy.float32 )
		result[ : ] = numpy.nan

		# If corners where detected
		if nb_detected > 0:

			# Convert the result to numpy ndarray
			charuco_corners = numpy.asarray( charuco_corners, dtype=numpy.float32 )
			charuco_ids     = numpy.asarray( charuco_ids, dtype=numpy.int32 )

			# Go through each detected corners
			for i in range(nb_detected):

				# Get the corner and its id
				charuco_corner = charuco_corners[ i, : ]
				charuco_id     = charuco_ids[ i ]

				# Set it in the result
				result[ charuco_id, : ] = charuco_corner
			
			# for i in range(nb_detected)

		# if nb_detected > 0

		return result

	# def interpolate ( self, markers_corners, image )

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Interpolates the corners of the charuco board from the corners of the markers in the image of the given example.

		Arguments:
			self (`pydataset.aruco.CharucoCornersInterpolator`): Interpolator.
			example               (`pydataset.dataset.Example`): Example in which to interpolate the charuco board corners.

		Returns:
			`pydataset.dataset.Example`: Example onced the board is detected and saved.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the image and the markers corners
		image           = example.get_feature( self.settings.image_feature_name ).value
		markers_corners = example.get_feature( self.settings.markers_corners_feature_name ).value

		# Interpolate the corners of the charuco board.
		charuco_corners = self.interpolate( markers_corners, image )
		
		# Save them in the example
		example.create_or_update_feature(
			     feature_name = self.settings.charuco_corners_feature_name,
			feature_data_type = CharucoCornersFeatureData,
			            value = charuco_corners
			)

		# Return the example
		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Create the settings for a charuco board corners interpolator.

		Arguments:
			cls (`type`): Type of interpolator for which to create settings.
		
		Named Arguments:
			board_config (`pydataset.data.CharucoBoardData`): Charuco board configuration.
			image_feature_name                       (`str`): Name of the image feature.
			markers_corners_feature_name             (`str`): Name of the markers corners feature.
			charuco_corners_feature_name             (`str`): Name of the feature where to save the interpolated board corners.
			url_search_pattern                       (`str`): URL used to search for example(s) to map.
			name                                     (`str`): Name of the mapper.

		Returns:
			`pydataset.manipulators.charuco.CharucoCornersInterpolatorSettings`: The new settings.
		"""
		return CharucoCornersInterpolatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class CharucoCornersInterpolator ( Mapper )