# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from pydataset.data                     import CharucoBoardData
from .markers_corners_detector_settings import MarkersCornersDetectorSettings

# ##################################################
# ## CLASS CHARUCO-CORNERS-INTERPOLATOR-SETTINGS  ##
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BoardCornersInterpolatorSettings',
	   namespace = 'pydataset.aruco',
	fields_names = [
		'charuco_corners_feature_name'
		]
	)
class CharucoCornersInterpolatorSettings ( MarkersCornersDetectorSettings ):
	"""
	Charuco board corners interpolator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, charuco_settings, image_feature_name, markers_corners_feature_name, charuco_corners_feature_name, url_search_pattern, name='Markers Corners Detector' ):
		"""
		Initializes a new instance of the board detector settings class.

		Arguments:
			self (`pydataset.dataset.CharucoCornersInterpolatorSettings`): Instance to initialize.
			charuco_settings          (`pydataset.data.CharucoBoardData`): Charuco board configuration.
			image_feature_name                                    (`str`): Name of the image feature.
			markers_corners_feature_name                          (`str`): Name of the markers corners feature.
			charuco_corners_feature_name                          (`str`): Name of the feature where to save the interpolated board corners.
			url_search_pattern                                    (`str`): URL used to search for example(s) to map.
			name                                                  (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolatorSettings )
		assert pytools.assertions.type_is_instance_of( charuco_settings, CharucoBoardData )
		assert pytools.assertions.type_is( image_feature_name, str )
		assert pytools.assertions.type_is( markers_corners_feature_name, str )
		assert pytools.assertions.type_is( charuco_corners_feature_name, str )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( CharucoCornersInterpolatorSettings, self ).__init__(
			charuco_settings,
			image_feature_name,
			markers_corners_feature_name,
			url_search_pattern,
			name
			)

		self.charuco_corners_feature_name = charuco_corners_feature_name

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_corners_feature_name ( self ):
		"""
		Name of the feature where to save the interpolated board corners (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolatorSettings )

		return self._charuco_corners_feature_name
	
	# def charuco_corners_feature_name ( self )

	# --------------------------------------------------

	@charuco_corners_feature_name.setter
	def charuco_corners_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoCornersInterpolatorSettings )
		assert pytools.assertions.type_is( value, str )
	
		self._charuco_corners_feature_name = value

	# def charuco_corners_feature_name ( self, value )

# class CharucoCornersInterpolatorSettings ( MarkersCornersDetectorSettings )