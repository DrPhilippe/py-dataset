# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .mapper_settings import MapperSettings

# ##################################################
# ###       CLASS CALLABLE-MAPPER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CallableMapperSettings',
	   namespace = 'pydataset.manipulators',
	fields_names = []
	)
class CallableMapperSettings ( MapperSettings ):
	"""
	Callable mapper settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, url_search_pattern='', name='Callable Mapper' ):
		"""
		Initializes a new instance of the callable mapper settings class.
		
		The url search pattern is used to configure an iterator,
		thus defining which example(s) to map.

		Arguments:
			self (`pydataset.manipualtors.CallableMapperSettings`): Instance to initialize.
			url_search_pattern                             (`str`): URL used to search for example(s) to map.
			name                                           (`str`): Name of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CallableMapperSettings )
		assert pytools.assertions.type_is( url_search_pattern, str )
		assert pytools.assertions.type_is( name, str )

		super( CallableMapperSettings, self ).__init__( url_search_pattern=url_search_pattern, name=name )

	# def __init__ ( self, url_search_pattern, name )

# class CallableMapperSettings ( MapperSettings )