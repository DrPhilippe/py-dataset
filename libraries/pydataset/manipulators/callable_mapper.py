# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from .callable_mapper_settings import CallableMapperSettings
from .mapper                   import Mapper

# ##################################################
# ###                CLASS MAPPER                ###
# ##################################################

class CallableMapper ( Mapper ):
	"""
	Maps example(s) using a callable object (callble object, function, lambda...).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, callable, settings ):
		"""
		Initializes a new instance of the callable mapper class.
		
		The signature of the callble must be the following:
		callable (`pydataset.dataset.Example`) -> pydataset.dataset.Example

		Arguments:
			self                     (`pydataset.manipualtors.Mapper`): Mapper instance to initialize.
			callable                                      (`callable`): Callable used to map the example(s).
			settings (`pydataset.manipualtors.CallableMapperSettings`): Settings of the callable mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, CallableMapper )
		assert pytools.assertions.type_is_instance_of( settings, CallableMapperSettings )

		super( CallableMapper, self ).__init__( settings )

		self._callable = callable

	# def __init__ ( self )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def callable ( self ):
		"""
		Callable object used to map the example(s) (`any`).

		The signature of the callble is the following:

		callable (`pydataset.dataset.Example`) -> pydataset.dataset.Example
		"""
		assert pytools.assertions.type_is_instance_of( self, CallableMapper )

		return self._callable

	# def callable ( self )
	
	# --------------------------------------------------

	@callable.setter
	def callable ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CallableMapper )
		
		self._callable = value
		
	# def callable ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this mapper's callable.

		Arguments:
			self (`pydataset.manipualtors.CallableMapper`): Mapper used to map the example.
			example          (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, Mapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Map the example
		example = self.callable( example )

		if example is None:
			raise RuntimeError( 'The callable did not return the example' )

		return example
	
	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, callable, **kwargs ):

		# Create the settings
		settings = cls.create_settings( **kwargs )

		# Create the manipulator
		return cls( callable, settings )

	# def create ( cls, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return CallableMapperSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class CallableMapper ( Mapper )