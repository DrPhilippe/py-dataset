# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..mapper_settings import MapperSettings

# ##################################################
# ###       CLASS ONE-HOT-MAPPER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OneHotMapperSettings',
	   namespace = 'pydataset.manipulators.classification',
	fields_names = [
		'sparse_category_feature_name',
		'one_hot_category_feature_name',
		'number_of_categories'
		]
	)
class OneHotMapperSettings ( MapperSettings ):
	"""
	On hot mapper settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, sparse_category_feature_name='', one_hot_category_feature_name='', number_of_categories=2, **kwargs ):
		"""
		Initializes a new instance of the one hot mapper settings class.
		
		Arguments:
			self (`pydataset.manipulators.classification.OneHotMapperSettings`): Instance to initialize.
			sparse_category_feature_name                                (`str`): Name of the aleardy existing feature defining to which category the example belongs to.
			one_hot_category_feature_name                               (`str`): Name of the feature created which will define the category the example belongs to using a one-hot vector.
			number_of_categories                                        (`int`): Total number of categories.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to map.
			name               (`str`): Name of the one hot mapper mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )
		assert pytools.assertions.type_is( sparse_category_feature_name, str )
		assert pytools.assertions.type_is( one_hot_category_feature_name, str )
		assert pytools.assertions.type_is( number_of_categories, int )

		super( OneHotMapperSettings, self ).__init__( **kwargs )

		self._sparse_category_feature_name  = sparse_category_feature_name
		self._one_hot_category_feature_name = one_hot_category_feature_name
		self._number_of_categories          = number_of_categories

	# def __init__ ( self, ... )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def sparse_category_feature_name ( self ):
		"""
		Name of the aleardy existing feature defining to which category the example belongs to (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )

		return self._sparse_category_feature_name
	
	# def sparse_category_feature_name ( self )

	# --------------------------------------------------

	@sparse_category_feature_name.setter
	def sparse_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._sparse_category_feature_name = value
	
	# def sparse_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def one_hot_category_feature_name ( self ):
		"""
		Name of the feature created which will define the category the example belongs to using a one-hot vector (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )

		return self._one_hot_category_feature_name
	
	# def one_hot_category_feature_name ( self )

	# --------------------------------------------------

	@one_hot_category_feature_name.setter
	def one_hot_category_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )
		assert pytools.assertions.type_is( value, str )

		self._one_hot_category_feature_name = value
	
	# def one_hot_category_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def number_of_categories ( self ):
		"""
		Total number of categories (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )

		return self._number_of_categories
	
	# def number_of_categories ( self )

	# --------------------------------------------------

	@number_of_categories.setter
	def number_of_categories ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OneHotMapperSettings )
		assert pytools.assertions.type_is( value, int )

		self._number_of_categories = value
	
	# def number_of_categories ( self, value )

# class OneHotMapperSettings ( MapperSettings )