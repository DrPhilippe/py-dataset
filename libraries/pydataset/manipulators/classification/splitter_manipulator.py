# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import random
# import threading

# INTERNALS
import pydataset.dataset
import pytools.assertions

# LOCALS
from ..manipulator                  import Manipulator
from .splitter_manipulator_settings import SplitterManipulatorSettings

# ##################################################
# ###         CLASS SPLITTER-MANIPULATOR         ###
# ##################################################

class SplitterManipulator ( Manipulator ):
	"""
	Example splitter.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.classification.SplitterManipulator`): Splitter instance to initialize.
			settings (`pydataset.manipualtors.classification.SplitterManipulatorSettings`): Settings of the splitter.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulator )
		assert pytools.assertions.type_is_instance_of( settings, SplitterManipulatorSettings )

		super( SplitterManipulator, self ).__init__( settings )

		self._splits = {}

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before manipulation starts.

		Arguments:
			self (`pydataset.manipualtors.classification.SplitterManipulator`): Splitter instance.
			root_group                             (`pydataset.dataset.Group`): Group containing the examples that will be manipulated.
			number_of_examples                                         (`int`): Number of examples to manipulate.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( SplitterManipulator, self ).begin( root_group, number_of_examples )

		# Check the splits settings
		if self.settings.splits_names is None and self.settings.splits_proportions is None:
			self.settings.splits_names       = [root_group.name+'_1', root_group.name+'_2']
			self.settings.splits_proportions = [0.75, 0.25]
			
		elif self.settings.splits_names is not None and self.settings.splits_proportions is not None:
			pass

		else:
			raise RuntimeError( 'Both or None of splits_names and splits_proportions must be provided' )

		# Create the splits
		self._splits = []
		for split_name in self.settings.splits_names:
			split = self.dst_group.create_group( split_name )
			self._splits.append( split )
		self._number_of_splits = len( self._splits )

		# Prepare cursors
		# Cursor define which example goes to which split
		# by means of indexing the data
		# for example if we have one category named 'foo' with data:
		# {'foo': [
		# 	'dataset/groups/g1/examples/example_1',
		# 	'dataset/groups/g1/examples/example_2',
		# 	'dataset/groups/g1/examples/example_3',
		# 	'dataset/groups/g1/examples/example_4'
		# ]}
		# And we want to split it in 3 split named 'bar_1', 'bar_2', 'bar_3'
		# with proportions [.5, .25, .25]
		# then cursors would be [2, 3, 4]
		# Meaning that bar_1 would receive examples 1 and 2,
		# bar_2 would receive example 3
		# and bar_3 would receive example 4
		self._cursors = {}
		for category_name in self.settings.data:

			# Shuffle the data
			random.shuffle( self.settings.data[ category_name ] )

			# Number of examples in the category
			nb_examples_in_category = len( self.settings.data[ category_name ] )

			# get proportions
			proportions = numpy.asarray( self.settings.splits_proportions, dtype=numpy.float32 )

			# Create the cursors
			count   = len( proportions )
			cursors = numpy.zeros( [count], dtype=numpy.float32 )
			total   = .0
			for i in range(count):
				proportion   = proportions[i]
				total        = total + proportion
				cursors[ i ] = total
			cursors = cursors * float(nb_examples_in_category)
			cursors = numpy.floor( cursors )
			cursors[ -1 ] = nb_examples_in_category
			cursors = cursors.astype(numpy.int32).tolist()
			self._cursors[ category_name ] = cursors

	# def begin ( self, root_group, number_of_examples )

	# --------------------------------------------------

	def manipulate ( self, example, index ):
		"""
		Manipulates the given example using this splitter.

		This method clones the example and any required parent into one of the created splits.

		Arguments:
			self (`pydataset.manipualtors.classification.SplitterManipulator`): Splitter instance.
			example                              (`pydataset.dataset.Example`): Example treated.
			index                                                      (`int`): Index of the example treated.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )
		assert pytools.assertions.type_is( index, int )

		if self.settings.category_name_feature_name:
			# Get the name of the category the example bolong to
			category_name = example.get_feature( self.settings.category_name_feature_name ).value
			category_name = str(category_name)
		else:
			category_name = 'all'

		# Get the index table of that category
		# and the cursors
		urls_table = self.settings.data[ category_name ]
		cursors    = self._cursors[ category_name ]

		# Get the index of the example in the table
		index = urls_table.index( example.url )

		# Find in which split the example goes to
		for i in range( self._number_of_splits ):
			cursor = cursors[ i ]
			split  = self._splits[ i ]
			if index < cursor:
				break

		# Get/Create example parent group in the split
		self._dst_group_lock.acquire()
		parent_group = self._get_or_create_example_parent_in_dst_group( example, split )
		self._dst_group_lock.release()
		
		# Create a new exampe in that group with the same name
		example_copy = parent_group.create_example( example.name )

		# Copy the src example in it
		example_copy.copy( example )

	# def manipulate ( self, example, index )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for the manipulator type `cls`.

		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings of the manipulator.

		Returns:
			`pydataset.manipulators.ManipulatorSettings`: Settings for the manipulator of type `cls`.
		"""
		
		return SplitterManipulatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SplitterManipulator ( Manipulator )