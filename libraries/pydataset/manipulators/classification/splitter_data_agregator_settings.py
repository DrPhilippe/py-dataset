# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..agregator_settings import AgregatorSettings

# ##################################################
# ###   CLASS SPLITTER-DATA-AGREGATOR-SETTINGS   ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SplitterDataAgregatorSettings',
	   namespace = 'pydataset.manipulators.classification',
	fields_names = [
		'category_name_feature_name'
		]
	)
class SplitterDataAgregatorSettings ( AgregatorSettings ):
	"""
	Splitter data-agregator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, category_name_feature_name='', **kwargs ):
		"""
		Initializes a new instance of the splitter data agregator settings class.
		
		Arguments:
			self (`pydataset.manipualtors.SplitterDataAgregatorSettings`): Instance to initialize.
			category_name_feature_name                            (`str`): Name of the feature containing the category name.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to split.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregatorSettings )
		assert pytools.assertions.type_is( category_name_feature_name, str )

		# Init parent class
		super( SplitterDataAgregatorSettings, self ).__init__( **kwargs )

		# Init properties
		self._category_name_feature_name = category_name_feature_name

	# def __init__ ( self, category_name_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregatorSettings )

		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

# class SplitterDataAgregatorSettings ( AgregatorSettings )