# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
import pydataset.dataset
from ..mapper import Mapper
from .one_hot_mapper_settings import OneHotMapperSettings

# ##################################################
# ###                CLASS MAPPER                ###
# ##################################################

class OneHotMapper ( Mapper ):
	"""
	The one hot mapper create a new feature in each mapped examples
	by converting a given sparse category feature to its one hot equivalent.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the mapper class.
		
		Arguments:
			self             (`pydataset.manipualtors.classification.OneHotMapper`): One hot mapper instance to initialize.
			settings (`pydataset.manipualtors.classification.OneHotMapperSettings`): Settings of the mapper.
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapper )
		assert pytools.assertions.type_is_instance_of( settings, OneHotMapperSettings )

		super( OneHotMapper, self ).__init__( settings )

	# def __init__ ( self )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def map ( self, example ):
		"""
		Maps the given example using this one hot mapper.

		Arguments:
			self (`pydataset.manipualtors.classification.OneHotMapper`): One hot apper used to map the example.
			example                       (`pydataset.dataset.Example`): Example to map.

		Returns:
			`pydataset.dataset.Example`: Example onced map.
		"""
		assert pytools.assertions.type_is_instance_of( self, OneHotMapper )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		# Get the sparse category value
		sparse_category = example.get_feature( self.settings.sparse_category_feature_name ).value

		# Create the one hot category value
		one_hot_category = numpy.zeros( (self.settings.number_of_categories), dtype=numpy.int32 )
		one_hot_category[ sparse_category ] = 1

		# Create the one hot category feature 
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			     feature_name = self.settings.one_hot_category_feature_name,
			            shape = pydataset.data.ShapeData( self.settings.number_of_categories ),
			            dtype = 'int32',
			            value = one_hot_category
			)

		# Return the example
		return example

	# def map ( self, example )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Create a new instance of the one hot mapper settings.
		
		Arguments:
			cls (`type`): `pydataset.manipualtors.classification.OneHotMapper` class

		Named Arguments:
			sparse_category_feature_name  (`str`): Name of the aleardy existing feature defining to which category the example belongs to.
			one_hot_category_feature_name (`str`): Name of the feature created which will define the category the example belongs to using a one-hot vector.
			number_of_categories          (`int`): Total number of categories.
			url_search_pattern            (`str`): URL used to search for example(s) to map.
			name                          (`str`): Name of the one hot mapper mapper.

		Returns:
			`pydataset.manipualtors.classification.OneHotMapperSettings`: The new one hot mapper settings instance.
		"""
		return OneHotMapperSettings( **kwargs)

	# def create_settings ( cls, **kwargs )

# class OneHotMapper ( Mapper )