# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..manipulator_settings import ManipulatorSettings

# ##################################################
# ###    CLASS SPLITTER-MANIPULATOR-SETTINGS     ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SplitterManipulatorSettings',
	   namespace = 'pydataset.manipulators.classification',
	fields_names = [
		'category_name_feature_name',
		'data',
		'splits_names',
		'splits_proportions'
		]
	)
class SplitterManipulatorSettings ( ManipulatorSettings ):
	"""
	Splitter manipulator settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, category_name_feature_name='', data={}, splits_names=None, splits_proportions=None, **kwargs ):
		"""
		Initializes a new instance of the splitter data agregator settings class.
		
		Their must be as many split names and splits proportions.
		Split proportions must sum to 1.0.
		
		If both are None then two splits are created by adding _1 and _2 to the source split name
		and their proportions are 0.75 and 0.25.

		Arguments:
			self  (`pydataset.manipualtors.SplitterManipulatorSettings`): Instance to initialize.
			category_name_feature_name                           (`str`): Name of the feature containing the category name.
			data                              (`dict` of `str` to `str`): Data agragated using the splitter data agregator.
			splits_names                        (`None`/`list` of `str`): Names of the split to create.
			splits_proportions                (`None`/`list` of `float`): Proportion of examples going to each split.

		Named Arguments:
			url_search_pattern (`str`): URL used to search for example(s) to manipulate.
			dst_group_url      (`str`): URL of the group where to create the splits.
			name               (`str`): Name of the manipulator.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )
		assert pytools.assertions.type_is( category_name_feature_name, str )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_in( splits_names, (list, type(None)) )
		assert pytools.assertions.type_in( splits_proportions, (list, type(None)) )

		if splits_names is not None or splits_proportions is not None:
			assert pytools.assertions.type_is( splits_names, list )
			assert pytools.assertions.list_items_type_is( splits_names, str )
			assert pytools.assertions.type_is( splits_proportions, list )
			assert pytools.assertions.list_items_type_is( splits_proportions, float )
			assert pytools.assertions.equal( len(splits_names), len(splits_proportions) )
			assert pytools.assertions.equal( numpy.sum(splits_proportions), 1.0 )


		# Init parent class
		super( SplitterManipulatorSettings, self ).__init__( **kwargs )

		# Init properties
		self._category_name_feature_name = category_name_feature_name
		self._data                       = data
		self._splits_names               = splits_names
		self._splits_proportions         = splits_proportions

	# def __init__ ( self, category_name_feature_name, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def category_name_feature_name ( self ):
		"""
		Name of the feature containing the category name (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )

		return self._category_name_feature_name
	
	# def category_name_feature_name ( self )

	# --------------------------------------------------

	@category_name_feature_name.setter
	def category_name_feature_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )
		assert pytools.assertions.type_is( value, str )

		self._category_name_feature_name = value
	
	# def category_name_feature_name ( self, value )

	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data agragated using the splitter data agregator (`dict` of `str` to `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )

		return self._data

	# def data ( self )

	# --------------------------------------------------

	@data.setter
	def data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )
		assert pytools.assertions.type_is( value, dict )

		self._data = value
	
	# def data ( self, value )

	# --------------------------------------------------

	@property
	def splits_names ( self ):
		"""
		Names of the split to create (`None`/`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )

		return self._splits_names
	
	# def splits_names ( self )

	# --------------------------------------------------

	@splits_names.setter
	def splits_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )
		assert pytools.assertions.type_in( value, (list, type(None)) )

		self._splits_names = value
	
	# def splits_names ( self, value )

	# --------------------------------------------------

	@property
	def splits_proportions ( self ):
		"""
		Proportion of examples going to each split. (`None`/`list` of `float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )

		return self._splits_proportions
	
	# def splits_proportions ( self )

	# --------------------------------------------------

	@splits_proportions.setter
	def splits_proportions ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SplitterManipulatorSettings )
		assert pytools.assertions.type_in( value, (list, type(None)) )

		self._splits_proportions = value
	
	# def splits_proportions ( self, value )

# class SplitterManipulatorSettings ( ManipulatorSettings )