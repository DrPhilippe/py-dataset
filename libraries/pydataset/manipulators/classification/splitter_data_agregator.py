# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import threading

# INTERNALS
import pytools.assertions
import pytools.path

# LOCALS
import pydataset.dataset
from ..agregator                       import Agregator
from .splitter_data_agregator_settings import SplitterDataAgregatorSettings

# ##################################################
# ###       CLASS SPLITTER-DATA-AGREGATOR        ###
# ##################################################

class SplitterDataAgregator ( Agregator ):
	"""
	Base class for agregators.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, settings ):
		"""
		Initializes a new instance of the manipulator class.

		Arguments:
			self             (`pydataset.manipulators.SplitterDataAgregator`): Instance to initialize.
			settings (`pydataset.manipulators.SplitterDataAgregatorSettings`): SplitterDataAgregator settings.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregator )
		assert pytools.assertions.type_is_instance_of( settings, SplitterDataAgregatorSettings )

		# Init parent class
		super( SplitterDataAgregator, self ).__init__( settings )

		# Init fields
		# self._lock          = threading.Lock()
		# self._example_index = 0
		self._result = {}

	# def __init__ ( self, settings )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def result ( self ):
		"""
		Agregation result (`dict` of `str` to `str`).

		Each entry in the result is a category name and the list
		of examples urls in  belonging to that category.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregator )

		return self._result
	
	# def result ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def agregate ( self, example ):
		"""
		Agregates the given example using this agregator.

		Arguments:
			self (`pydataset.manipualtors.SplitterDataAgregator`): Agregator.
			example                 (`pydataset.dataset.Example`): Example to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregator )
		assert pytools.assertions.type_is_instance_of( example, pydataset.dataset.Example )

		if self.settings.category_name_feature_name:		
			# Get the name of the category the example bolong to
			category_name = example.get_feature( self.settings.category_name_feature_name ).value
			category_name = str(category_name)
			
		else:
			category_name = 'all'

		# If this is the first example of this category
		# Initialize the list of url
		if category_name not in self._result:
			self._result[ category_name ] = []

		# Add the example url to its category table
		self._result[ category_name ].append( example.url )

		# self._lock.acquire()
		# self._lock.release()

	# def agregate ( self, example )

	# --------------------------------------------------

	def begin ( self, root_group, number_of_examples ):
		"""
		Method called before agregation starts.

		Arguments:
			self (`pydataset.manipualtors.SplitterDataAgregator`): Agregator.
			root_group                (`pydataset.dataset.Group`): Group containing the examples that will be agregated.
			number_of_examples                            (`int`): Number of examples to agregate.
		"""
		assert pytools.assertions.type_is_instance_of( self, SplitterDataAgregator )
		assert pytools.assertions.type_is_instance_of( root_group, pydataset.dataset.Group )
		assert pytools.assertions.type_is( number_of_examples, int )

		super( SplitterDataAgregator, self ).begin( root_group, number_of_examples )

		# self._example_index = 0
		self._result = {}

	# def begin ( self, root_group, number_of_examples )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):

		return SplitterDataAgregatorSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class SplitterDataAgregator ( Agregator )