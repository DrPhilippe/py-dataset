# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

FEET = 3.28084
INCH = 39.3701
DEG_TO_RAD = numpy.pi / 180.
RAD_TO_DEG = 180. / numpy.pi

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def meter_to_feet ( value ):
	"""
	Converts the given value in meters to feets.

	Arguments:
		value (`float`/`numpy.ndarray`): Value in meters.

	Returns:
		`float`/`numpy.ndarray`: value in feets.
	"""
	assert pytools.assertions.type_in( value, (float, numpy.ndarray) )
	if isinstance( value, numpy.ndarray ):
		assert pytools.assertions.equal( value.dtype, numpy.float32 )
	
	return value * FEET

# def meter_to_feet ( value )

# --------------------------------------------------

def feet_to_meter ( value ):
	"""
	Converts the given value in feets to meters.

	Arguments:
		value (`float`/`numpy.ndarray`): Value in feets.

	Returns:
		`float`/`numpy.ndarray`: value in meters.
	"""
	assert pytools.assertions.type_in( value, (float, numpy.ndarray) )
	if isinstance( value, numpy.ndarray ):
		assert pytools.assertions.equal( value.dtype, numpy.float32 )

	return value / FEET

# def feet_to_meter ( value )

# --------------------------------------------------

def meter_to_inch ( value ):
	"""
	Converts the given value in meters to inches.

	Arguments:
		value (`float`/`numpy.ndarray`): Value in meters.

	Returns:
		`float`/`numpy.ndarray`: value in inches.
	"""
	assert pytools.assertions.type_in( value, (float, numpy.ndarray) )
	if isinstance( value, numpy.ndarray ):
		assert pytools.assertions.equal( value.dtype, numpy.float32 )

	return value * INCH

# def meter_to_inch ( value )

# --------------------------------------------------

def inch_to_meter ( value ):
	"""
	Converts the given value in inches to meters.

	Arguments:
		value (`float`/`numpy.ndarray`): Value in inches.

	Returns:
		`float`/`numpy.ndarray`: value in meters.
	"""
	assert pytools.assertions.type_in( value, (float, numpy.ndarray) )
	if isinstance( value, numpy.ndarray ):
		assert pytools.assertions.equal( value.dtype, numpy.float32 )

	return value / INCH
	
# def inch_to_meter ( value )


# --------------------------------------------------

def degree_to_radian ( angle ):
	"""
	Converts the given agnle in degrees to radians.

	Arguments:
		angle (`float`/`numpy.ndarray`): Angle in degrees.

	Returns:
		`float`/`numpy.ndarray`: Angle in radians.
	"""
	assert pytools.assertions.type_in( angle, (float, numpy.ndarray) )
	if isinstance( angle, numpy.ndarray ):
		assert pytools.assertions.equal( angle.dtype, numpy.float32 )

	return angle * DEG_TO_RAD
	
# def degree_to_radian ( angle )

# --------------------------------------------------

def radian_to_degree ( angle ):
	"""
	Converts the given agnle in radians to degrees.

	Arguments:
		angle (`float`): Angle in radians.

	Returns:
		`float`/`numpy.ndarray`: Angle in degrees.
	"""
	assert pytools.assertions.type_in( angle, (float, numpy.ndarray) )
	if isinstance( angle, numpy.ndarray ):
		assert pytools.assertions.equal( angle.dtype, numpy.float32 )

	return angle * RAD_TO_DEG
	
# def radian_to_degree ( angle )