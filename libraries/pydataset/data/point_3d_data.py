# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .point_2d_data import Point2DData

# ##################################################
# ###            CLASS POINT-3D-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'Point3DData',
	   namespace = 'pydataset.data',
	fields_names = [ 'z' ]
	)
class Point3DData ( Point2DData ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, x=0, y=0, z=0 ):
		"""
		Initializes a new instance of the point 2d data class.

		Arguments:
			self (`pydataset.data.Point3DData`): Instance to initialize.
			x                           (`int`): Position along the X axis.
			y                           (`int`): Position along the Y axis.
			z                           (`int`): Position along the Z axis.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point3DData )
		assert pytools.assertions.type_is( x, int )
		assert pytools.assertions.type_is( y, int )
		assert pytools.assertions.type_is( x, int )

		super( Point3DData, self ).__init__( x, y )

		self._z = z

	# def __init__ ( self, x, y, z )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def z ( self ):
		"""
		Position along the Z axis (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point3DData )

		return self._z

	# def z ( self )

	# --------------------------------------------------

	@z.setter
	def z ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Point3DData )
		assert pytools.assertions.type_is( value, int )

		self._z = value

	# def z ( self, value )

# class Point3DData ( Point2DData )