# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###              CLASS SHAPE-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ShapeData',
	   namespace = 'pydataset.data',
	fields_names = [
		'dimensions'
		]
	)
class ShapeData ( pytools.serialization.Serializable ):
	"""
	The `pydataset.data.ShapeData` class is used to describe the n-dimensional size of features and tensors.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, *args ):
		"""
		Initializes a new instance of the `pydataset.data.ShapeData` class.

		Arguments:
			self (`pydataset.data.ShapeData`): Instance to initialize.
			*args          (`tuple` of `int`): The dimensions of the shape along each n-axis.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )
		assert pytools.assertions.type_is( args, tuple )
		assert pytools.assertions.tuple_items_type_is( args, int )
		
		super( ShapeData, self ).__init__()
		
		self.dimensions = list( args )

	# def __init__ ( self, *args )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def dimensions ( self ):
		"""
		Dimensions of the shape along each n-axis (`list` of `int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )

		return self._dimensions

	# def dimensions ( self )
	
	# --------------------------------------------------

	@dimensions.setter
	def dimensions ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ShapeData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, int )

		self._dimensions = value

	# def dimensions ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_area ( self ):
		"""
		Computes the area of this shape, i.e. the product of the dimensions of this shape.

		Arguments:
			self (`pydataset.data.ShapeData`): Shape of which to compute the area.

		Returns:
			`int`: Area of this shape
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )

		area = 1
		for dimension in self.dimension:
			area *= dimension
		return area

	# def get_area ( self )

	# --------------------------------------------------

	def get_rank ( self ):
		"""
		Returns the rank of this shape, i.e. the number of dimensions it spans on.

		Arguments:
			self (`pydataset.data.ShapeData`): Shape of which to compute the rank.

		Returns:
			`int`: Rank of this shape
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )

		return len(self.dimensions)

	# def get_rank ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def from_list ( cls, dimensions ):
		"""
		Initializes a new instance of the `cls` shape class with the given dimensions.

		Arguments:
			cls                 (`type`): Type of shape data to initialize.
			dimensions (`list` of `int`): The dimensions of the shape along each n-axis.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( dimensions, list )
		assert pytools.assertions.list_items_type_is( dimensions, int )
		
		return cls( *dimensions )

	# def from_list ( cls, dimensions )
	
	# --------------------------------------------------

	@classmethod
	def from_tuple ( cls, dimensions ):
		"""
		Initializes a new instance of the `cls` shape class with the given dimensions.

		Arguments:
			cls                  (`type`): Type of shape data to initialize.
			dimensions (`tuple` of `int`): The dimensions of the shape along each n-axis.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( dimensions, tuple )
		assert pytools.assertions.tuple_items_type_is( dimensions, int )
		
		return cls( *dimensions )

	# def from_tuple ( cls, dimensions )

	# ##################################################
	# ###                OPERATORS                   ###
	# ##################################################

	# --------------------------------------------------

	def __getitem__ ( self, index ):
		return self.dimensions[ index ]

	# --------------------------------------------------

	def __setitem__ ( self, index, value ):
		self.dimensions[ index ] = value

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Create a detail text representation of this shape.

		Arguments:
			self (`pydataset.data.ShapeData`): Shape of which to create a text representation.

		Returns:
			`str`: Text representation of this shape.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )

		return '[ShapeData: rank={}, dimensions={}]'.format(
			self.get_rank(),
			self.dimensions
			)

	# def __repr__ ( self )

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Create a text representation of this shape.

		Arguments:
			self (`pydataset.data.ShapeData`): Shape of which to create a text representation.

		Returns:
			`str`: Text representation of this shape.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )

		rank = self.get_rank()

		if ( rank == 0 ):
			return ''
			
		text = ''
		for i in range( rank-1 ):
			text = text + str( self.dimensions[ i ] ) + 'x'
		return text + str( self.dimensions[ rank-1 ] )

	# def __str__ ( self )

	# --------------------------------------------------

	def __eq__ ( self, other ):
		"""
		Checks if the the other value is a shape equal to this one.

		Arguments:
			self         (`pydataset.data.ShapeData`): Reference shape.
			other (`None`/`pydataset.data.ShapeData`): Shape to check.
		"""
		assert pytools.assertions.type_is_instance_of( self, ShapeData )
		assert pytools.assertions.type_is_instance_of( other, (type(None), ShapeData) )

		if isinstance( other, ShapeData ):
			return self.dimensions == other.dimensions
		return False

	# def __eq__ ( self, other )
	
	# --------------------------------------------------

	def __ne__ ( self, other ):
		"""
		Checks if the the other value is not a shape equal to this one.

		Arguments:
			self         (`pydataset.data.ShapeData`): Reference shape.
			other (`None`/`pydataset.data.ShapeData`): Shape to check.
		"""		
		assert pytools.assertions.type_is_instance_of( self, ShapeData )
		assert pytools.assertions.type_is_instance_of( other, (type(None), ShapeData) )

		if isinstance( other, ShapeData ):
			return self.dimensions != other.dimensions
		return True
	
	# def __ne__ ( self, other )

# class ShapeData ( pytools.serialization.Serializable )