# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###            CLASS POINT-2D-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'Point2DData',
	   namespace = 'pydataset.data',
	fields_names = [ 'x', 'y' ]
	)
class Point2DData ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, x=0, y=0 ):
		"""
		Initializes a new instance of the point 2d data class.

		Arguments:
			self (`pydataset.data.Point2DData`): Instance to initialize.
			x                           (`int`): Position along the X axis.
			y                           (`int`): Position along the Y axis.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DData )
		assert pytools.assertions.type_is( x, int )
		assert pytools.assertions.type_is( y, int )

		super( Point2DData, self ).__init__()
		
		self._x = x
		self._y = y

	# def __init__ ( self, x1, y1, x2, y2 )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def x ( self ):
		"""
		Position along the X axis (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DData )

		return self._x

	# def x ( self )

	# --------------------------------------------------

	@x.setter
	def x ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Point2DData )
		assert pytools.assertions.type_is( value, int )

		self._x = value

	# def x ( self, value )

	# --------------------------------------------------

	@property
	def y ( self ):
		"""
		Position along the Y axis (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DData )

		return self._y

	# def y ( self )

	# --------------------------------------------------

	@y.setter
	def y ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Point2DData )
		assert pytools.assertions.type_is( value, int )

		self._y = value

	# def y ( self, value )
	
# class Point2DData ( pytools.serialization.Serializable )