# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2.aruco

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###       CLASS CHARUCO-DICTIONARY-DATA        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoDictionaryData',
	   namespace = 'pydataset.data',
	fields_names = []
	)
class CharucoDictionaryData ( pytools.serialization.Enum ):
	"""
	Existing charuco dictionary enum.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	DICT_4X4_50         = None
	DICT_4X4_100        = None
	DICT_4X4_250        = None
	DICT_4X4_1000       = None
	DICT_5X5_50         = None
	DICT_5X5_100        = None
	DICT_5X5_250        = None
	DICT_5X5_1000       = None
	DICT_6X6_50         = None
	DICT_6X6_100        = None
	DICT_6X6_250        = None
	DICT_6X6_1000       = None
	DICT_7X7_50         = None
	DICT_7X7_100        = None
	DICT_7X7_250        = None
	DICT_7X7_1000       = None
	DICT_ARUCO_ORIGINAL = None
	DICT_APRILTAG_16h5  = None
	DICT_APRILTAG_25h9  = None
	DICT_APRILTAG_36h10 = None
	DICT_APRILTAG_36h11 = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='DICT_6X6_250' ):
		"""
		Initializes a new instance of the `pydataset.data.CharucoDictionaryData` enum.

		Arguments:
			self (`pydataset.data.CharucoDictionaryData`): Instance to initialize.
			value                                 (`str`): Name of the dictionary.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoDictionaryData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( CharucoDictionaryData, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	# --------------------------------------------------

	def get_cv2_dictionary ( self ):
		"""
		Gets the preset opencv aruco dictionay corresponding to this dictinary data.

		Arguments:
			self (`pydataset.aruco.CharucoDictionaryData`): The dictionay data.

		Returns:
			`cv2.aruco.Dictionary`: OpenCV aruco dictionary.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoDictionaryData )

		dict_id = getattr( cv2.aruco, str(self) )
		return cv2.aruco.Dictionary_get( dict_id )

	# def get_cv2_dictionary ( self )


	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible values for the `pydataset.data.CharucoDictionaryData` enum.

		Arguments:
			cls (`type`): The enum type.

		Returns:
			`list` of `str`: The list of possible values.
		"""
		assert pytools.assertions.type_is_instance_of( cls, type )

		return [
			'DICT_4X4_50',
			'DICT_4X4_100',
			'DICT_4X4_250',
			'DICT_4X4_1000',
			'DICT_5X5_50',
			'DICT_5X5_100',
			'DICT_5X5_250',
			'DICT_5X5_1000',
			'DICT_6X6_50',
			'DICT_6X6_100',
			'DICT_6X6_250',
			'DICT_6X6_1000',
			'DICT_7X7_50',
			'DICT_7X7_100',
			'DICT_7X7_250',
			'DICT_7X7_1000',
			'DICT_ARUCO_ORIGINAL',
			'DICT_APRILTAG_16h5',
			'DICT_APRILTAG_25h9',
			'DICT_APRILTAG_36h10',
			'DICT_APRILTAG_36h11'
			]
		
	# def get_possible_values ( cls )

# class TargetType ( pytools.serialization.Enum )
	
# ##################################################
# ###                CLASS-FIELDS                ###
# ##################################################

CharucoDictionaryData.DICT_4X4_50         = CharucoDictionaryData( 'DICT_4X4_50' )
CharucoDictionaryData.DICT_4X4_100        = CharucoDictionaryData( 'DICT_4X4_100' )
CharucoDictionaryData.DICT_4X4_250        = CharucoDictionaryData( 'DICT_4X4_250' )
CharucoDictionaryData.DICT_4X4_1000       = CharucoDictionaryData( 'DICT_4X4_1000' )
CharucoDictionaryData.DICT_5X5_50         = CharucoDictionaryData( 'DICT_5X5_50' )
CharucoDictionaryData.DICT_5X5_100        = CharucoDictionaryData( 'DICT_5X5_100' )
CharucoDictionaryData.DICT_5X5_250        = CharucoDictionaryData( 'DICT_5X5_250' )
CharucoDictionaryData.DICT_5X5_1000       = CharucoDictionaryData( 'DICT_5X5_1000' )
CharucoDictionaryData.DICT_6X6_50         = CharucoDictionaryData( 'DICT_6X6_50' )
CharucoDictionaryData.DICT_6X6_100        = CharucoDictionaryData( 'DICT_6X6_100' )
CharucoDictionaryData.DICT_6X6_250        = CharucoDictionaryData( 'DICT_6X6_250' )
CharucoDictionaryData.DICT_6X6_1000       = CharucoDictionaryData( 'DICT_6X6_1000' )
CharucoDictionaryData.DICT_7X7_50         = CharucoDictionaryData( 'DICT_7X7_50' )
CharucoDictionaryData.DICT_7X7_100        = CharucoDictionaryData( 'DICT_7X7_100' )
CharucoDictionaryData.DICT_7X7_250        = CharucoDictionaryData( 'DICT_7X7_250' )
CharucoDictionaryData.DICT_7X7_1000       = CharucoDictionaryData( 'DICT_7X7_1000' )
CharucoDictionaryData.DICT_ARUCO_ORIGINAL = CharucoDictionaryData( 'DICT_ARUCO_ORIGINAL' )
CharucoDictionaryData.DICT_APRILTAG_16h5  = CharucoDictionaryData( 'DICT_APRILTAG_16h5' )
CharucoDictionaryData.DICT_APRILTAG_25h9  = CharucoDictionaryData( 'DICT_APRILTAG_25h9' )
CharucoDictionaryData.DICT_APRILTAG_36h10 = CharucoDictionaryData( 'DICT_APRILTAG_36h10' )
CharucoDictionaryData.DICT_APRILTAG_36h11 = CharucoDictionaryData( 'DICT_APRILTAG_36h11' )
