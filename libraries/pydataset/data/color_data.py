# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import random

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###                CLASS COLOR                 ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ColorData',
	   namespace = 'pydataset.data',
	fields_names = [
		'red',
		'green',
		'blue',
		'alpha'
		]
	)
class ColorData ( pytools.serialization.Serializable ):
	"""
	The class `pydataset.data.ColorData` describes a RGBA color.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, red=0, green=0, blue=0, alpha=255 ):
		"""
		Initializes a new instance of the `pydataset.data.ColorData` class.

		Arguments:
			self (`pydataset.data.ColorData`): Instance to initilize.
			red                       (`int`): Red channel intensity.
			green                     (`int`): Green channel intensity.
			blue                      (`int`): Blue channel intensity.
			alpha                     (`int`): Alpha channel intensity.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( red, int )
		assert pytools.assertions.value_in( red, range(256) )
		assert pytools.assertions.type_is( green, int )
		assert pytools.assertions.value_in( green, range(256) )
		assert pytools.assertions.type_is( blue, int )
		assert pytools.assertions.value_in( blue, range(256) )
		assert pytools.assertions.type_is( alpha, int )
		assert pytools.assertions.value_in( alpha, range(256) )

		super( ColorData, self ).__init__()

		self._red   = red
		self._green = green
		self._blue  = blue
		self._alpha = alpha

	# def __init__ ( self, red, green, blue, alpha )

	# ##################################################
	# ###                CLASS-METHOD                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def zero ( cls ):
		"""
		Create a color with 0 intensity in all channels.

		Arguments:
			cls (`type`): Type of color to create.

		Returns:
			`cls`: The new color.
		"""
		assert pytools.assertions.type_is( cls, type )

		return cls( 0, 0, 0, 0 )

	# def zero ( cls )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def red ( self ):
		"""
		Red channel intensity (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return self._red

	# def red ( self )
	
	# --------------------------------------------------

	@red.setter
	def red ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(256) )

		self._red = value
		
	# def red ( self, value )

	# --------------------------------------------------

	@property
	def green ( self ):
		"""
		Green channel intensity (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return self._green

	# def green ( self )
	
	# --------------------------------------------------

	@green.setter
	def green ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(256) )

		self._green = value
		
	# def green ( self, value )

	# --------------------------------------------------

	@property
	def blue ( self ):
		"""
		Blue channel intensity (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return self._blue

	# def blue ( self )
	
	# --------------------------------------------------

	@blue.setter
	def blue ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(256) )

		self._blue = value
		
	# def blue ( self, value )

	# --------------------------------------------------

	@property
	def alpha ( self ):
		"""
		Alpha channel intensity (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return self._alpha

	# def alpha ( self )
	
	# --------------------------------------------------

	@alpha.setter
	def alpha ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( value, int )
		assert pytools.assertions.value_in( value, range(256) )

		self._alpha = value
		
	# def alpha ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def clone ( self ):
		"""
		Creates a clone of this color.

		Arguments:
			self (`pydataset.data.ColorData`): Color to clone.

		Returns:
			`pydataset.data.ColorData`: The clone.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( alpha, bool )
		
		return type(self)( self.red, self.green, self.blue, self.alpha )

	# def clone ( self )

	# --------------------------------------------------

	def to_array ( self, alpha=True ):
		"""
		Creates a numpy ndarray containing the intensities of this color.
	
		Arguments:
			self (`pydataset.data.ColorData`): Color to convert to a list.

		Returns:
			`numpy.ndarray`: The numpy ndarray containing the intensities of this color.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( alpha, bool )

		if alpha:
			return numpy.asarray( [ self.red, self.green, self.blue, self.alpha ], dtype=numpy.uint8 )
		else:
			return numpy.asarray( [ self.red, self.green, self.blue ], dtype=numpy.uint8 )

	# def to_array ( self )

	# --------------------------------------------------

	def to_hex ( self, alpha=True ):
		"""
		Returns the hexadecimal representation of this color.
	
		Arguments:
			self (`pydataset.data.ColorData`): Color to convert to hexadicimal.

		Returns:
			`str`: Exadecimal color
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( alpha, bool )

		if alpha:
			return '#{:02x}{:02x}{:02x}{:02x}'.format( self.red, self.green, self.blue, self.alpha )
		else:
			return '#{:02x}{:02x}{:02x}'.format( self.red, self.green, self.blue )

	# def to_array ( self )

	# --------------------------------------------------

	def to_list ( self, alpha=True ):
		"""
		Creates a list containing the intensities of this color.
		
		Arguments:
			self (`pydataset.data.ColorData`): Color to convert to a list.

		Returns:
			`list` of `int`: The list containing the intensities of this color.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( alpha, bool )

		if alpha:
			return [ self.red, self.green, self.blue, self.alpha ]
		else:
			return [ self.red, self.green, self.blue ]

	# def to_list ( self )

	# --------------------------------------------------

	def to_tuple ( self, alpha=True ):
		"""
		Creates a tuple containing the intensities of this color.
		
		Arguments:
			self (`pydataset.data.ColorData`): Color to convert to a list.

		Returns:
			`list` of `int`: The list containing the intensities of this color.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is( alpha, bool )

		if alpha:
			return ( self.red, self.green, self.blue, self.alpha )
		else:
			return ( self.red, self.green, self.blue )

	# def to_list ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a compact text representation of this color.

		Arguments:
			self (`pydataset.data.ColorData`): Color to represent as text.

		Returns:
			`str`: Text representation of this color.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return '({}, {}, {}, {})'.format(
			self.red,
			self.green,
			self.blue,
			self.alpha
			)

	# def __str__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Creates a detailed text representation of this color.

		Arguments:
			self (`pydataset.data.ColorData`): Color to represent as text.

		Returns:
			`str`: Text representation of this color.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )

		return '[Color: red={}, green={}, blue={}, alpha={}]'.format(
			self.red,
			self.green,
			self.blue,
			self.alpha
			)

	# def __repr__ ( self )
	
	# --------------------------------------------------

	def __eq__ ( self, other ):
		"""
		Checks if the other color has the same channel intensities as this one.

		Arguments:
			self         (`pydataset.data.ColorData`): Reference color.
			other (`None`/`pydataset.data.ColorData`): Color to check.

		Returns:
			`bool`: `True` if the two colors have the same channels intensities. Otherwise, `False`.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is_instance_of( other, (type(None), ColorData) )

		if isinstance( other, ColorData ):
			return self.red == other.red and self.green == other.green and self.blue == other.blue and self.alpha == other.alpha
		
		return False

	# def __eq__ ( self, other )

	# --------------------------------------------------

	def __ne__ ( self, other ):
		"""
		Checks if the other color has not the same channel intensities as this one.

		Arguments:
			self         (`pydataset.data.ColorData`): Reference color.
			other (`None`/`pydataset.data.ColorData`): Color to check.

		Returns:
			`bool`: `True` if the two colors do not have the same channels intensities. Otherwise, `False`.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorData )
		assert pytools.assertions.type_is_instance_of( other, (type(None), ColorData) )
		
		if isinstance( other, ColorData ):
			return self.red != other.red or self.green != other.green or self.blue != other.blue or self.alpha != other.alpha
		
		return True

	# def __ne__ ( self, other )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def from_hex ( cls, hex_color ):
		"""
		Creates a new color from an hexadecimal color code.

		Arguments:
			cls      (`type`): Type of the `ColorData` to create.
			hex_color (`str`): Hexadicimal color.

		Returns:
			`ColorData`: The new color with type `cls`.
		"""
		_hex_color = hex_color.lstrip( '#' )
		
		if len(_hex_color) == 6:
			rgb_color = tuple( int(_hex_color[i : i+2], 16 ) for i in (0, 2, 4) )
			return cls( rgb_color[0], rgb_color[1], rgb_color[2] )

		elif len(_hex_color) == 8:
			rgba_color = tuple( int(_hex_color[i : i+2], 16 ) for i in (0, 2, 4, 6) )
			return cls( rgba_color[0], rgba_color[1], rgba_color[2], rgba_color[3] )
		
		else:
			raise RuntimeError( 'Hex color {} has an unsupported format'.format(hex_color) )

	# def from_hex ( cls, hex_color )
	
	# ##################################################
	# ###                 CONSTANTS                  ###
	# ##################################################

	# --------------------------------------------------

	PREDEFINED_22 = []
	PREDEFINED_256 = []
	
# class ColorData ( pytools.serialization.Serializable )

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

# --------------------------------------------------

ColorData.PREDEFINED_22 = [ColorData.from_hex(h) for h in [
	'#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
	'#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
	'#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
	'#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
	'#ffffff', '#000000'
	]]

ColorData.PREDEFINED_256 = [ColorData.from_hex(h) for h in [
	'#B88183', '#922329', '#5A0007', '#D7BFC2',	'#D86A78', '#FF8A9A', '#3B000A', '#E20027',
	'#943A4D', '#5B4E51', '#B05B6F', '#FEB2C6', '#D83D66', '#895563', '#FF1A59', '#FFDBE5',
	'#CC0744', '#CB7E98', '#997D87', '#6A3A4C', '#FF2F80', '#6B002C', '#A74571', '#C6005A',
	'#FF5DA7', '#300018', '#B894A6', '#FF90C9', '#7C6571', '#A30059', '#DA007C', '#5B113C',
	'#402334', '#D157A0', '#DDB6D0', '#885578', '#962B75', '#A97399', '#D20096', '#E773CE',
	'#AA5199', '#E704C4', '#6B3A64', '#FFA0F2', '#6F0062', '#B903AA', '#C895C5', '#FF34FF',
	'#320033', '#DBD5DD', '#EEC3FF', '#BC23FF', '#671190', '#201625', '#F5E1FF', '#BC65E9',
	'#D790FF', '#72418F', '#4A3B53', '#9556BD', '#B4A8BD', '#7900D7', '#A079BF', '#958A9F',
	'#837393', '#64547B', '#3A2465', '#353339', '#BCB1E5', '#9F94F0', '#9695C5', '#0000A6',
	'#000035', '#636375', '#00005F', '#97979E', '#7A7BFF', '#3C3E6E', '#6367A9', '#494B5A',
	'#3B5DFF', '#C8D0F6', '#6D80BA', '#8FB0FF', '#0045D2', '#7A87A1', '#324E72', '#00489C',
	'#0060CD', '#789EC9', '#012C58', '#99ADC0', '#001325', '#DDEFFF', '#59738A', '#0086ED',
	'#75797C', '#BDC9D2', '#3E89BE', '#8CD0FF', '#0AA3F7', '#6B94AA', '#29607C', '#404E55',
	'#006FA6', '#013349', '#0AA6D8', '#658188', '#5EBCD1', '#456D75', '#0089A3', '#B5F4FF',
	'#02525F', '#1CE6FF', '#001C1E', '#203B3C', '#A3C8C9', '#00A6AA', '#00C6C8', '#006A66',
	'#518A87', '#E4FFFC', '#66E1D3', '#004D43', '#809693', '#15A08A', '#00846F', '#00C2A0',
	'#00FECF', '#78AFA1', '#02684E', '#C2FFED', '#47675D', '#00D891', '#004B28', '#8ADBB4',
	'#0CBD66', '#549E79', '#1A3A2A', '#6C8F7D', '#008941', '#63FFAC', '#1BE177', '#006C31',
	'#B5D6C3', '#3D4F44', '#4B8160', '#66796D', '#71BB8C', '#04F757', '#001E09', '#D2DCD5',
	'#00B433', '#9FB2A4', '#003109', '#A3F3AB', '#456648', '#51A058', '#83A485', '#7ED379',
	'#D1F7CE', '#A1C299', '#061203', '#1E6E00', '#5EFF03', '#55813B', '#3B9700', '#4FC601',
	'#1B4400', '#C2FF99', '#788D66', '#868E7E', '#83AB58', '#374527', '#98D058', '#C6DC99',
	'#A4E804', '#76912F', '#8BB400', '#34362D', '#4C6001', '#DFFB71', '#6A714A', '#222800',
	'#6B7900', '#3A3F00', '#BEC459', '#FEFFE6', '#A3A489', '#9FA064', '#FFFF00', '#61615A',
	'#FFFFFE', '#9B9700', '#CFCDAC', '#797868', '#575329', '#FFF69F', '#8D8546', '#F4D749',
	'#7E6405', '#1D1702', '#CCAA35', '#CCB87C', '#453C23', '#513A01', '#FFB500', '#A77500',
	'#D68E01', '#B79762', '#7A4900', '#372101', '#886F4C', '#A45B02', '#E7AB63', '#FAD09F',
	'#C0B9B2', '#938A81', '#A38469', '#D16100', '#A76F42', '#5B4534', '#5B3213', '#CA834E',
	'#FF913F', '#953F00', '#D0AC94', '#7D5A44', '#BE4700', '#FDE8DC', '#772600', '#A05837',
	'#EA8B66', '#391406', '#FF6832', '#C86240', '#29201D', '#B77B68', '#806C66', '#FFAA92',
	'#89412E', '#E83000', '#A88C85', '#F7C9BF', '#643127', '#E98176', '#7B4F4B', '#1E0200',
	'#9C6966', '#BF5650', '#BA0900', '#FF4A46', '#F4ABAA', '#000000', '#452C2C', '#C8A1A1'
	]]