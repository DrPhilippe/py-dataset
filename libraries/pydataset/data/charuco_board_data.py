# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2.aruco

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .charuco_dictionary_data import CharucoDictionaryData

# ##################################################
# ###         CLASS -CHARUCO-BOARD-DATA          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CharucoBoardData',
	   namespace = 'pydataset.data',
	fields_names = [
		'dictionary_data',
		'board_width',
		'board_height',
		'square_length',
		'marker_length',
		'border_length'
		]
	)
class CharucoBoardData ( pytools.serialization.Serializable ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, dictionary_data=CharucoDictionaryData.DICT_6X6_250, board_width=8, board_height=8, square_length=0.025, marker_length=0.02, border_length=0.025 ):
		"""
		Initializes a new instance of the `pydataset.data.CharucoBoardData` class.

		Arguments:
			self  (`pydataset.data.CharucoBoardData`): Instance to initialize.
			dictionary_data (`CharucoDictionaryData`): Dictionary used to fill the cell of the board.
			board_width                       (`int`): Number of cells along the width of the board.
			board_height                      (`int`): Number of cells along the height of the board.
			square_length                   (`float`): Length of the cells in meters.
			marker_length                   (`float`): Length of the markers inside the cells in meters.
			border_length                   (`float`): Width of the border around the board.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is_instance_of( dictionary_data, CharucoDictionaryData )
		assert pytools.assertions.type_is( board_width, int )
		assert pytools.assertions.type_is( board_height, int )
		assert pytools.assertions.type_is( square_length, float )
		assert pytools.assertions.type_is( marker_length, float )
		assert pytools.assertions.type_is( border_length, float )


		super( CharucoBoardData, self ).__init__()

		self._dictionary_data = dictionary_data
		self._board_width     = board_width
		self._board_height    = board_height
		self._square_length   = square_length
		self._marker_length   = marker_length
		self._border_length   = border_length

	# def __init__ ( self, ... )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dictionary_data ( self ):
		"""
		Dictionary used to fill the cell of the board (`CharucoDictionaryData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._dictionary_data

	# def dictionary_data ( self )
	
	# --------------------------------------------------

	@dictionary_data.setter
	def dictionary_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is_instance_of( value, CharucoDictionaryData )
		
		self._dictionary_data = value
		
	# def dictionary_data ( self, value )

	# --------------------------------------------------

	@property
	def board_width ( self ):
		"""
		Number of cells along the width of the board (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._board_width

	# def board_width ( self )
	
	# --------------------------------------------------

	@board_width.setter
	def board_width ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( value, int )

		self._board_width = value
		
	# def board_width ( self, value )

	# --------------------------------------------------

	@property
	def board_height ( self ):
		"""
		Number of cells along the height of the board (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._board_height

	# def board_height ( self )
	
	# --------------------------------------------------

	@board_height.setter
	def board_height ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( value, int )

		self._board_height = value
		
	# def board_height ( self, value )

	# --------------------------------------------------

	@property
	def number_of_cells ( self ):
		"""
		Number of cells on the board (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self.board_width * self.board_height

	# def number_of_cells ( self )

	# --------------------------------------------------

	@property
	def number_of_markers ( self ):
		"""
		Number of markers on the board (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self.number_of_cells // 2

	# def number_of_markers ( self )

	# --------------------------------------------------

	@property
	def number_of_corners ( self ):
		"""
		Number of corners on the board, excluding thoese on the sides (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return (self._board_width - 1) * (self._board_height -1)

	# def number_of_corners ( self )

	# --------------------------------------------------

	@property
	def square_length ( self ):
		"""
		Length of the cells in meters (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._square_length

	# def square_length ( self )
	
	# --------------------------------------------------

	@square_length.setter
	def square_length ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( value, float )

		self._square_length = value
		
	# def square_length ( self, value )
	
	# --------------------------------------------------

	@property
	def marker_length ( self ):
		"""
		Length of the markers inside the cells in meters (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._marker_length

	# def marker_length ( self )
	
	# --------------------------------------------------

	@marker_length.setter
	def marker_length ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( value, float )

		self._marker_length = value
		
	# def marker_length ( self, value )

	# --------------------------------------------------

	@property
	def border_length ( self ):
		"""
		Width of the border around the board (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return self._border_length

	# def border_length ( self )
	
	# --------------------------------------------------

	@border_length.setter
	def border_length ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( value, float )

		self._border_length = value
		
	# def border_length ( self, value )

	# ##################################################
	# ###                 METHODS                  ###
	# ##################################################

	# --------------------------------------------------

	def get_cv2_board ( self ):
		"""
		Gets the opencv charuco board corresponding to this board data.

		Arguments:
			self (`pydataset.data.CharucoBoardData`): Charuco board data.

		Retuens:
			`cv2.aruco.CharucoBaord`: OpenCV charuco board.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return cv2.aruco.CharucoBoard_create(
			self.board_width,
			self.board_height,
			self.square_length,
			self.marker_length,
			self.dictionary_data.get_cv2_dictionary()
			)
		
	# def get_cv2_board ( self )

	# --------------------------------------------------

	def get_marker_data ( self, marker_index ):
		"""
		Returns the data of a marker on this charuco board.

		Arguments:
			self (`pydataset.data.CharucoBoardData`): Board of which to inspect a marker.
			marker_index                     (`int`): Index of the marker to inspect.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )
		assert pytools.assertions.type_is( marker_index, int )
		
		charuco      = self.get_cv2_board()
		dictionary   = charuco.dictionary
		marker_size  = dictionary.markerSize
		marker_index = charuco.ids[ marker_index ]
		data         = dictionary.bytesList[ marker_index, :, : ]
		return cv2.aruco.Dictionary_getBitsFromByteList( data, marker_size )

	# def get_marker_data ( self, marker_index )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this charuco board data.

		Arguments:
			self (`pydataset.aruco.CharucoBoardData`): Charuco board data to represent.

		Returns:
			`str`: The text representation of this charuco board data.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return '[CharucoBoardData: dictionary={}, size={}x{}, square length={}m, marker length={}m, border length={}m]'.format(
			str(self.dictionary_data),
			self.board_width,
			self.board_height,
			self.square_length,
			self.marker_length,
			self.border_length
			)

	# def __str__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Creates a text representation of this charuco board data.

		Arguments:
			self (`pydataset.aruco.CharucoBoardData`): Charuco board data to represent.

		Returns:
			`str`: The text representation of this charuco board data.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoBoardData )

		return str(self)

	# def __repr__ ( self )

# class CharucoBoardData ( pytools.serialization.Serializable )