# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import numpy
import random

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###                 CONSTANTS                  ###
# ##################################################

DEFAULT_RNG = numpy.random.default_rng()

# ##################################################
# ###            CLASS RECTANGLE-DATA            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RectangleData',
	   namespace = 'pydataset.data',
	fields_names = [ 'x1', 'y1', 'x2', 'y2' ]
	)
class RectangleData ( pytools.serialization.Serializable ):

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, x1=0, y1=0, x2=0, y2=0 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( x1, (int, float) )
		assert pytools.assertions.type_in( y1, (int, float) )
		assert pytools.assertions.type_in( x2, (int, float) )
		assert pytools.assertions.type_in( y2, (int, float) )
		
		if ( x2 < x1 ):
			x2 = x1
		if ( y2 < y1 ):
			y2 = y1

		self.x1 = float(x1)
		self.y1 = float(y1)
		self.x2 = float(x2)
		self.y2 = float(y2)

	# def __init__ ( self, x1, y1, x2, y2 )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def x1 ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self._x1

	# def x1 ( self )

	# --------------------------------------------------

	@x1.setter
	def x1 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( value, (int, float) )

		self._x1 = float(value)

	# def x1 ( self, value )

	# --------------------------------------------------
	
	@property
	def y1 ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self._y1

	# def y1 ( self )

	# --------------------------------------------------

	@y1.setter
	def y1 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( value, (int, float) )

		self._y1 = float(value)

	# def y1 ( self, value )
	
	# --------------------------------------------------

	@property
	def x2 ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self._x2

	# def x2 ( self )
	
	# --------------------------------------------------
	
	@x2.setter
	def x2 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( value, (int, float) )

		self._x2 = float(value)

	# def x2 ( self, value )

	# --------------------------------------------------
	
	@property
	def y2 ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self._y2	

	# def y2 ( self )

	# --------------------------------------------------

	@y2.setter
	def y2 ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( value, (int, float) )

		self._y2 = float(value)

	# def y2 ( self, value )
	
	# --------------------------------------------------

	@property
	def area ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return float( max(self.width,0) ) * float( max(self.height,0) )

	# def area ( self )

	# --------------------------------------------------

	@property
	def center ( self ):
		
		x = float( self.x1 + self.x2 ) / 2.0
		y = float( self.y1 + self.y2 ) / 2.0
		return x, y

	# def center ( self )

	# --------------------------------------------------
	
	@property
	def height ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self.y2 - self.y1

	# def height ( self )

	# --------------------------------------------------
	
	@property
	def width ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )

		return self.x2 - self.x1
	
	# def width ( self )

	# --------------------------------------------------

	@property
	def top_left ( self ):
		return (self.x1, self.y1)
	
	# --------------------------------------------------

	@property
	def bottom_right ( self ):
		return (self.x2, self.y2)

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def center_on ( self, x, y ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( x, (float, int) )
		assert pytools.assertions.type_in( y, (float, int) )
		
		center_x, center_y = self.center
		shift_x = x - center_x
		shift_y = y - center_y
		self.shift( shift_x, shift_y )

	# def center_on ( self, x, y )

	# --------------------------------------------------

	def clone ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		
		return RectangleData( self.x1, self.y1, self.x2, self.y2 )
	
	# def clone ( self )

	# --------------------------------------------------

	def does_not_fit_in ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_is_instance_of( other, RectangleData )
		
		return self.width > other.width or self.height > other.height

	# def does_not_fit_in ( self, other )
	
	# --------------------------------------------------

	def fits_in ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_is_instance_of( other, RectangleData )
		
		return self.width <= other.width and self.height <= other.height

	# def fits_in ( self, other )

	# --------------------------------------------------

	def is_inside ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_is_instance_of( other, RectangleData )
		
		return self.x1 >= other.x1 and self.x2 <= other.x2 and self.y1 >= other.y1 and self.y2 <= other.y2

	# def is_inside ( self, other )

	# --------------------------------------------------

	def is_not_inside ( self, other ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_is_instance_of( other, RectangleData )
		
		return self.x1 < other.x1 or self.x2 > other.x2 or self.y1 < other.y1 or self.y2 > other.y2

	# def is_not_inside ( self, other )
	
	# --------------------------------------------------

	def get_overlap_with ( self, other, norm=1.0 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_is_instance_of( other, RectangleData )
		assert pytools.assertions.type_is( norm, float )

		a1 = RectangleData.intersection( self, other ).get_area()
		a2 = self.get_area()
		return (float(a1) / float(a2)) * norm

	# def get_overlap_with ( self, other )

	# --------------------------------------------------

	def shift ( self, dx, dy ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		assert pytools.assertions.type_in( dx, (int, float) )
		assert pytools.assertions.type_in( dy, (int, float) )

		self.x1 += float(dx)
		self.x2 += float(dx)
		self.y1 += float(dy)
		self.y2 += float(dy)

	# def sShift ( self, dx, dy )
	
	# --------------------------------------------------

	def to_list ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		
		return [ self.x1, self.y1, self.x2, self.y2 ]

	# def to_list ( self )
	
	# --------------------------------------------------

	def to_ndarray ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleData )
		
		return numpy.asarray( [[self.x1, self.y1], [self.x2, self.y2]], dtype=numpy.int32 )

	# def to_ndarray ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def copy ( cls, rectangle ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( rectangle, RectangleData )
		
		return cls( rectangle.x1, rectangle.y1, rectangle.x2, rectangle.y2 )

	# def copy ( cls, rectangle )
	
	# --------------------------------------------------

	@classmethod
	def from_ndarray ( cls, array ):
		assert pytools.assertions.type_is( array, numpy.ndarray )
		assert pytools.assertions.value_in( array.dtype, (numpy.int32, numpy.float32) )
		assert pytools.assertions.equal( array.size, 4 )
		
		array = array.ravel()

		return cls( float(array[0]), float(array[1]), float(array[2]), float(array[3]) )

	# def from_ndarray ( cls, array )

	# --------------------------------------------------

	@classmethod
	def from_list ( cls, lst ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( lst, list )
		assert pytools.assertions.list_items_type_is( lst, int )
		
		return cls( *lst )

	# def from_list ( cls, lst )

	# --------------------------------------------------
	
	@classmethod
	def from_points ( cls, x1, y1, x2, y2 ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_in( x1, (int, float) )
		assert pytools.assertions.type_in( y1, (int, float) )
		assert pytools.assertions.type_in( x2, (int, float) )
		assert pytools.assertions.type_in( y2, (int, float) )
		
		return cls( x1, y1, x2, y2 )

	# def from_points ( cls, x1, y1, x2, y2 )
	
	# --------------------------------------------------
	
	@classmethod
	def from_size ( cls, x, y, width, height ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_in( x, (int, float) )
		assert pytools.assertions.type_in( y, (int, float) )
		assert pytools.assertions.type_in( width, (int, float) )
		assert pytools.assertions.type_in( height, (int , float))
		
		return cls( x, y, x+width, y+height )

	# def from_size ( cls, x1, y1, x2, y2 )

	# --------------------------------------------------
	
	@classmethod
	def random_in ( cls, rectangle, width, height ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( rectangle, RectangleData )
		assert pytools.assertions.type_in( width, (int, float) )
		assert pytools.assertions.type_in( height, (int, float) )
		
		return RectangleData.randoms_in( rectangle, width, height, 1 )[ 0 ]
		
	# def random_in ( cls, rectangle, width, height )

	# --------------------------------------------------
	
	@classmethod
	def random_integer_in ( cls, rectangle, width, height ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( rectangle, RectangleData )
		assert pytools.assertions.type_in( width, (int, float) )
		assert pytools.assertions.type_in( height, (int, float) )
		
		return RectangleData.randoms_integer_in( rectangle, width, height, 1 )[ 0 ]
		
	# def random_integer_in ( cls, rectangle, width, height )

	# --------------------------------------------------
	
	@classmethod
	def randoms_in ( cls, rectangle, width, height, count ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( rectangle, RectangleData )
		assert pytools.assertions.type_in( width, (int, float) )
		assert pytools.assertions.type_in( height, (int, float) )
		assert pytools.assertions.type_is( count, int )
		
		# Check if the rectangles to create fit in the given rectangle
		if ( width > rectangle.width or height > rectangle.height ):
			raise ValueError(
				'Impossible to take a rectangle of size {}x{} in rectangle of size {}x{}'.format(
					width,
					height,
					rectangle.width,
					rectangle.height
					)
				)

		# if it is the same rectangle return it
		elif ( numpy.abs(width-rectangle.width)<numpy.finfo(float).eps and numpy.abs(height-rectangle.height)<numpy.finfo(float).eps ):
			return [ rectangle.clone() ]

		results = []
		while len(results) < count:

			x = DEFAULT_RNG.uniform( rectangle.x1, rectangle.x2 - width  ) # random.randrange( rectangle.x1, rectangle.x2 - width )
			y = DEFAULT_RNG.uniform( rectangle.y1, rectangle.y2 - height ) # random.randrange( rectangle.y1, rectangle.y2 - height )

			results.append(
				RectangleData.from_size( x, y, width, height )
				)

		return results
		
	# def randoms_in ( cls, rectangle, width, height, count )

	# --------------------------------------------------
	
	@classmethod
	def randoms_integer_in ( cls, rectangle, width, height, count ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( rectangle, RectangleData )
		assert pytools.assertions.type_in( width, (int, float) )
		assert pytools.assertions.type_in( height, (int, float) )
		assert pytools.assertions.type_is( count, int )
		
		# Check if the rectangles to create fit in the given rectangle
		if ( width > rectangle.width or height > rectangle.height ):
			raise ValueError(
				'Impossible to take a rectangle of size {}x{} in rectangle of size {}x{}'.format(
					width,
					height,
					rectangle.width,
					rectangle.height
					)
				)

		# if it is the same rectangle return it
		elif ( int(width) == int(rectangle.width) and int(height) == int(rectangle.height) ):
			return [ rectangle.clone() ]

		results = []
		while len(results) < count:

			x = random.randrange( int(rectangle.x1), int(rectangle.x2) - int(width ) )
			y = random.randrange( int(rectangle.y1), int(rectangle.y2) - int(height) )

			results.append(
				RectangleData.from_size( float(x), float(y), float(width), float(height) )
				)

		return results
		
	# def randoms_integer_in ( cls, rectangle, width, height, count )

	# --------------------------------------------------

	@classmethod
	def zero ( cls ):
		assert pytools.assertions.type_is( cls, type )
		
		return RectangleData()

	# def zero ( cls )

	# --------------------------------------------------
	
	@classmethod
	def intersection ( cls, lhs, rhs ):
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( lhs, RectangleData )
		assert pytools.assertions.type_is_instance_of( rhs, RectangleData )
		
		x1 = max( lhs.x1, rhs.x1 )
		y1 = max( lhs.y1, rhs.y1 )
		x2 = min( lhs.x2, rhs.x2 )
		y2 = min( lhs.y2, rhs.y2 )

		return cls( x1, y1, x2, y2 )

	# def intersection ( lhs, rhs )
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@staticmethod
	def intersection_over_union ( lhs, rhs ):
		"""
		Computes the instersection over union area ratio.
		"""
		assert pytools.assertions.type_is_instance_of( lhs, (RectangleData, numpy.ndarray) )
		assert pytools.assertions.type_is_instance_of( rhs, (RectangleData, numpy.ndarray) )

		if isinstance( lhs, numpy.ndarray ):
			lhs = RectangleData.from_ndarray( lhs )
		
		if isinstance( rhs, numpy.ndarray ):
			rhs = RectangleData.from_ndarray( rhs )

		intersection = RectangleData.intersection( lhs, rhs )


		return intersection.area / ( lhs.area + rhs.area - intersection.area + numpy.finfo(numpy.float32).eps )

	# def intersection_over_union ( lhs, rhs )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------
	
	def __repr__ ( self ):

		return '[Rectangle: ({}, {}), ({}, {}), {}x{}]'.format(
			self.x1, self.y1,
			self.x2, self.y2,
			self.width, self.height
			)

	# def __repr__ ( self )

	# --------------------------------------------------

	def __str__ ( self ):

		return '[({}, {}), ({}, {}), {}x{}]'.format(
			self.x1, self.y1,
			self.x2, self.y2,
			self.width, self.height
			)
	
	# def __str__ ( self )

	# --------------------------------------------------

	def __eq__ ( self, other ):

		if isinstance( other, RectangleData ):
			return self.x1 == other.x1 and self.y1 == other.y1 and self.x2 == other.x2 and self.y2 == other.y2
		
		return False

	# def __eq__ ( self, other )

	# --------------------------------------------------

	def __ne__ ( self, other ):
		
		if isinstance( other, RectangleData ):
			return self.x1 != other.x1 or self.y1 != other.y1 or self.x2 != other.x2 or self.y2 != other.y2
		
		return True

	# def __eq__ ( self, other )

# class RectangleData ( pytools.serialization.Serializable )