# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.text

# LOCALS
from .group_data import GroupData

# ##################################################
# ###             CLASS DATASET-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class DatasetData ( GroupData ):
	"""
	Informations used to describe a dataset and its contents.
	"""
	
	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	family_name = 'datasets'
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', groups_names=[], examples_names=[], features_names=[] ):
		"""
		Initializes a new instance of the `pydataset.data.DatasetData` class.
		
		Arguments:
			self (`pydataset.abc.DatasetData`): Instance to initialize.
			name                       (`str`): Name of the dataset.
			url                        (`str`): URL of the dataset.
			groups_names     (`list` of `str`): Names of the sub-groups of this dataset.
			examples_names   (`list` of `str`): examples of this dataset.
			features_names    (`list of `str`): Names of the features of this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( groups_names, list )
		assert pytools.assertions.list_items_type_is( groups_names, str )
		assert pytools.assertions.type_is( examples_names, list )
		assert pytools.assertions.list_items_type_is( examples_names, str )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		super( DatasetData, self ).__init__(
			          name = name,
			           url = url,
			    parent_url = '',
			  groups_names = groups_names,
			examples_names = examples_names,
			features_names = features_names
			)

	# def __init__ ( self, name, url, groups_names, examples_names, features_names )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this dataset data.

		Arguments:
			self (`pydataset.data.DatasetData`): Dataset data of which to create a text representation.

		Returns:
			`str`: Text representation of this dataset data.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetData )

		return "[pydataset.data.DatasetData: name='{}', url='{}', parent_url='{}', groups_names={}, examples_names={}, features_names={}]".format(
			self.name,
			self.url,
			self.parent_url,
			pytools.text.list_to_str( self.groups_names ),
			pytools.text.list_to_str( self.examples_names ),
			pytools.text.list_to_str( self.features_names )
			)

	# def __str__ ( self )

# class DatasetData ( GroupData )