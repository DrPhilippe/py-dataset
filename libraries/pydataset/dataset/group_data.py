# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.text

# LOCALS
from .example_data import ExampleData

# ##################################################
# ###              CLASS GROUP-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'GroupData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'groups_names',
		'examples_names'
		]
	)
class GroupData ( ExampleData ):
	"""
	Informations used to describe a group and its contents.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	family_name = 'groups'

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', groups_names=[], examples_names=[], features_names=[] ):
		"""
		Initializes a new instance of the `pydataset.data.GroupData` class.
		
		Group data are used to describe a group and its content.

		Arguments:
			self   (`pydataset.abc.GroupData`): Instance to initialize.
			name                       (`str`): Name of the group.
			url                        (`str`): URL of this group.
			parent_url          (`None`/`str`): URL of the parent group or `None`.
			groups_names     (`list` of `str`): Names of the children of this group.
			examples _names  (`list` of `str`): Names of the examples of this group.
			features_names    (`list of `str`): Names of the features of this group.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( groups_names, list )
		assert pytools.assertions.list_items_type_is( groups_names, str )
		assert pytools.assertions.type_is( examples_names, list )
		assert pytools.assertions.list_items_type_is( examples_names, str )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		super( GroupData, self ).__init__(
			          name = name,
			           url = url,
			    parent_url = parent_url,
			features_names = features_names
			)

		self.groups_names   = list(groups_names)
		self.examples_names = list(examples_names)

	# def __init__ ( self, name, url, parent_url, groups_names, examples_names, features_names )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def groups_names ( self ):
		"""
		Names of the sub-groups of this group (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )

		return self._groups_names

	# def groups_names ( self )
	
	# --------------------------------------------------

	@groups_names.setter
	def groups_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, GroupData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._groups_names = value
		
	# def groups_names ( self, value )

	# --------------------------------------------------

	@property
	def examples_names ( self ):
		"""
		Names of the examples of this group (`list` of ``).
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )

		return self._examples_names

	# def examples_names ( self )
	
	# --------------------------------------------------

	@examples_names.setter
	def examples_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, GroupData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._examples_names = value
		
	# def examples_names ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this group data (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )

		return len(self.examples_names)

	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def number_of_groups ( self ):
		"""
		Number of sub-groups in this group data (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )

		return len(self.groups_names)

	# def number_of_groups ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other group data into this group data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.data.GroupData`): Group data receiving the copy.
			other (`pydataset.data.GroupData`): The data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )
		assert pytools.assertions.type_is_instance_of( other, (GroupData, ExampleData) )

		super( GroupData, self ).copy( other )
		
		if isinstance( other, GroupData ):
			self._groups_names   = copy.deepcopy( other.groups_names )
			self._examples_names = copy.deepcopy( other.examples_names )

	# def copy ( self, other )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this group.

		Arguments:
			self (`pydataset.data.GroupData`): Group of which to create a text representation.

		Returns:
			`str`: Text representation of this group.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupData )

		return "[pydataset.data.GroupData: name='{}', url='{}', parent_url='{}', groups_names='{}', examples_names='{}', features_names='{}']".format(
			self.name,
			self.url,
			self.parent_url,
			pytools.text.list_to_str( self.groups_names ),
			pytools.text.list_to_str( self.examples_names ),
			pytools.text.list_to_str( self.features_names )
			)

	# def __str__ ( self )

# class GroupData ( ExampleData )