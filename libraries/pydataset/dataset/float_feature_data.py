# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###          CLASS FLOAT-FEATURE-DATA          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FlaotFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class FloatFeatureData ( FeatureData ):
	"""
	The class `pydataset.dataset.FloatFeatureData` stores `float` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=0.0 ):
		"""
		Initializes a new instance of the `pydataset.dataset.FloatFeatureData` class.

		Arguments:
			self (`pydataset.dataset.FloatFeatureData`): Instance to initialize.
			name                                (`str`): Name if the item.
			url                                 (`str`): URL if the item.
			parent_url                          (`str`): URL if the parent item.
			shape          (`pydataset.data.ShapeData`): Shape of this feature.
			value                               (`int`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, float )

		super( FloatFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureData )
		assert pytools.assertions.type_is( val, float )

		self._value = val
	
	# def value ( self, val )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this float feature data.

		Arguments:
			self (`pydataset.dataset.FloatFeatureData`): Float feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this float feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureData )

		return "[pydataset.dataset.FloatFeatureData: name='{}', url='{}', parent_url='{}', value={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class FloatFeatureData ( FeatureData )