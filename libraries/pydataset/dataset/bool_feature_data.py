# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###          CLASS BOOL-FEATURE-DATA           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BoolFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class BoolFeatureData ( FeatureData ):
	"""
	The class `pydataset.data.BoolFeatureData` stores `bool` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=False ):
		"""
		Initializes a new instance of the `pydataset.data.BoolFeatureData` class.

		Arguments:
			self (`pydataset.dataset.BoolFeatureData`): Instance to initialize.
			name                               (`str`): Name if the item.
			url                                (`str`): URL if the item.
			parent_url                         (`str`): URL if the parent item.
			value                             (`bool`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, bool )

		super( BoolFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureData )
		assert pytools.assertions.type_is( val, bool )

		self._value = val
	
	# def value ( self, value )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this bytes feature data.

		Arguments:
			self (`pydataset.dataset.BoolFeatureData`): Bool feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this bytes feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureData )

		return "[pydataset.dataset.BoolFeatureData: name='{}', url='{}', parent_url='{}', value={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class BoolFeatureData ( FeatureData )