# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pydataset.data

# ##################################################
# ###              CLASS A-BACKEND               ###
# ##################################################

class Backend:
	"""
	Base class for dataset storage implementations.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, url, name ):
		"""
		Initializes a new instance of the `pydataset.data.Backend` class.

		Arguments:
			url  (`str`): URL of the backend.
			name (`str`): Name of the backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( name, str )

		self._url  = url
		self._name = name

	# def __init__ ( self, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this backend (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )

		return self._name

	# def name ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this backend (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )

		return self._url

	# def url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def contains ( self, url, family_name='items' ):
		"""
		Checks if an item with the given url exists in the stored dataset.

		Arguments:
			self (`pydataset.dataset.Backend`): Backend that stores the item.
			url                        (`str`): URL of the item.
			family_name                (`str`): Family name of the item.

		Returns:
			`bool`: `True` if the item exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		raise NotImplementedError()

	# def contains ( self, url, family_name )

	# --------------------------------------------------

	def delete ( self, url, family_name='items' ):
		"""
		Deletes the item with the given URL from the stored dataset.

		Arguments:
			self (`pydataset.dataset.Backend`): Backend that stores the item.
			url                         (`str`): URL of the item to delete.
			family_name                 (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		raise NotImplementedError()

	# def delete ( self, url, family_name )

	# --------------------------------------------------

	def insert ( self, item_data, family_name='items' ):
		"""
		Inserts the given item in the stored dataset.

		Arguments:
			self       (`pydataset.dataset.Backend`): Backend that stores the item.
			item_data (`pydataset.dataset.ItemData`): Data of the item to insert.
			family_name                      (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )

		raise NotImplementedError()

	# def insert ( self, item_data, family_name )

	# --------------------------------------------------

	def find ( self, url, family_name='items' ):
		"""
		Searches for the item with the given URL in the stored dataset.

		Arguments:
			self (`pydataset.dataset.Backend`): Backend that stores the item.
			url                        (`str`): URL of the item to search for.
			family_name                (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		raise NotImplementedError()

	# def find ( self, url, family_name )
	
	# --------------------------------------------------

	def update ( self, item_data, family_name='items' ):
		"""
		Updates the given item in the stored dataset.

		Arguments:
			self       (`pydataset.dataset.Backend`): Backend that stores the item.
			item_data (`pydataset.dataset.ItemData`): Data of the item to update.
			family_name                      (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )

		raise NotImplementedError()

	# def update ( self, item_data, family_name )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this backend.

		Arguments:
			self (`pydataset.dataset.Backend`): Backend of which to create a text representation.

		Returns:
			`str`: Text representation of this backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, Backend )
		
		return "[pydataset.dataset.Backend: name='{}', url='{}']".format(
			self.name,
			self.url
			)

	# def __str__ ( self )

# class Backend