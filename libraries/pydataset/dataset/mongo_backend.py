# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import pymongo

# INTERNALS
import pytools.assertions
import pytools.serialization
import pydataset.data

# LOCALS
from .backend                    import Backend
from .item_data                  import ItemData
from .register_backend_attribute import RegisterBackendAttribute

# ##################################################
# ###               CLASS BACKEND                ###
# ##################################################

@RegisterBackendAttribute( 'mongodb' )
class MongoBackend ( Backend ):
	"""
	The mongo backend stores dataset in a mongodb dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, url='localhost:27017', database_name='PyDataset', name='mongo backend', **kwargs ):
		"""
		Initializes a new instance of the `pydataset.dataset.MongoBackend` class.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): Instance to initialize.
			url                             (`str`): URL to the mongodb dataset.
			database_name                   (`str`): Name of the database where to save the data.
			name                            (`str`): Name of the backend.
			**kwargs     (`dict` of `str` to `any`): Arguments forwarded to the mongodb client.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is( url, str )
		
		super( MongoBackend, self ).__init__( url, name )

		self._mongo_client   = pymongo.MongoClient( url, **kwargs )
		self._mongo_database = self.mongo_client[ 'PyDataset' ]

	# def __init__ ( self, url, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mongo_client ( self ):
		"""
		MongoDB client used to access the data (`pymongo.mongo_client.MongoClient`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )

		return self._mongo_client

	# def mongo_client ( self )
	
	# --------------------------------------------------

	@property
	def mongo_database ( self ):
		"""
		MongoDB database containing the data (`pymongo.database.Database`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )

		return self._mongo_database

	# def mongo_database ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------
	
	def get_collection ( self, family_name ):
		"""
		Returns the collection used to store the items family with the given name.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): MongoBackend that stores the family of items.
			family_name                     (`str`): Name of the family of item.

		Returns:
			`pymongo.collection.Collection`: The collection containing the items.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is( family_name, str )

		collection_name = family_name.capitalize()
		return self.mongo_database[ collection_name ]

	# def get_collection ( self, family_name )

	# --------------------------------------------------

	def contains ( self, url, family_name='items' ):
		"""
		Checks if an item with the given url exists in the stored dataset.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): MongoBackend that stores the item.
			url                             (`str`): URL of the item.
			family_name                     (`str`): Family name of the item.

		Returns:
			`bool`: `True` if the item exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		# Get the collection where to look for the item
		collection = self.get_collection( family_name )
		
		# Search for the item
		document = collection.find_one( {'url': url} )

		# Was it found ?
		return document is not None

	# def contains ( self, url, family_name )
	
	# --------------------------------------------------

	def delete ( self, url, family_name='items' ):
		"""
		Deletes the item with the given URL from the stored dataset.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): MongoBackend that stores the item.
			url                             (`str`): URL of the item to delete.
			family_name                     (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		# Get the collection where to delete the item
		collection = self.get_collection( family_name )

		# delete the item
		result = collection.delete_one( {'url': url} )

		return result.deleted_count == 1

	# def delete ( self, url, family_name )

	# --------------------------------------------------

	def insert ( self, item_data, family_name='items' ):
		"""
		Inserts the given item in the stored dataset.

		Arguments:
			self  (`pydataset.dataset.MongoBackend`): MongoBackend that stores the item.
			item_data (`pydataset.dataset.ItemData`): Data of the item to insert.
			family_name                      (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )

		# Serialize the item
		data = pytools.serialization.serialize( item_data )

		# Get the collection where to insert the item
		collection = self.get_collection( family_name )

		# Insert the item
		collection.insert_one( data )

	# def insert ( self, item_data, family_name )

	# --------------------------------------------------

	def find ( self, url, family_name='items' ):
		"""
		Searches for the item with the given URL in the stored dataset.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): MongoBackend that stores the item.
			url                             (`str`): URL of the item to search for.
			family_name                     (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		# Get the collection whre to look for the item
		collection = self.get_collection( family_name )
		
		# Find the item json data
		document = collection.find_one( {'url': url} )

		# Check it was found
		if ( document is None ):
			return None

		# Deserialize and return it
		return pytools.serialization.deserialize( document )

	# def find ( self, url, family_name )
	
	# --------------------------------------------------

	def update ( self, item_data, family_name='items' ):
		"""
		Updates the given item in the stored dataset.

		Arguments:
			self    (`pydataset.mongo.MongoBackend`): MongoBackend that stores the item.
			item_data (`pydataset.dataset.ItemData`): Data of the item to update.
			family_name                      (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )
		
		# Serialize the item
		data = pytools.serialization.serialize( item_data )

		# Get the collection where to update the item
		collection = self.get_collection( family_name )
		
		# Update the document
		result = collection.update_one( {'url': item_data.url}, {'$set': data} )

		return result.modified_count == 1

	# def update ( self, item_data, family_name )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this backend.

		Arguments:
			self (`pydataset.dataset.MongoBackend`): MongoBackend of which to create a text representation.

		Returns:
			`str`: Text representation of this backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, MongoBackend )
		
		return "[pydataset.dataset.MongoBackend: name='{}' url='{}']".format(
			self.name,
			self.url
			)

	# def __str__ ( self )

# class MongoBackend ( Backend )