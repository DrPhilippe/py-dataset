# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy
import base64
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data        import ShapeData
from .feature_data import FeatureData

# ##################################################
# ###         CLASS NDARRAY-FEATURE-DATA         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'NDArrayFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'shape',
		'dtype'
		]
	)
class NDArrayFeatureData ( FeatureData ):
	"""
	The class `pydataset.data.NDArrayFeatureData` stores `numpy.ndarray` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', shape=None, dtype=None, value=numpy.empty(0) ):
		"""
		Initializes a new instance of the `pydataset.data.NDArrayFeatureData` class.

		Arguments:
			self (`pydataset.data.NDArrayFeatureData`): Instance to initialize.
			name                               (`str`): Name if the item.
			url                                (`str`): URL if the item.
			parent_url                         (`str`): URL if the parent item.
			shape  (`None`/`pydataset.data.ShapeData`): Shape of this feature.
			dtype         (`None`/`str`/`numpy.dtype`): Data type of the nd-array .
			value                    (`numpy.ndarray`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is_instance_of( shape, (type(None), ShapeData) )
		assert pytools.assertions.type_in( dtype, (type(None), numpy.dtype, str) )
		assert pytools.assertions.type_is( value, numpy.ndarray )

		super( NDArrayFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

		self._shape = shape                if shape else ShapeData( *value.shape )
		self._dtype = numpy.dtype( dtype ) if dtype else value.dtype

	# def __init__ ( self, name, url, parent_url, shape, dtype, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is( val, numpy.ndarray )

		self._value = val
		self._dtype = val.dtype
		self._shape = ShapeData( *val.shape )
	
	# def value ( self, value )
	
	# --------------------------------------------------

	@property
	def shape ( self ):
		"""
		Shape of this feature (`pydataset.data.ShapeData`):
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )

		return self._shape
	
	# def shape ( self )
	
	# --------------------------------------------------

	@shape.setter
	def shape ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is_instance_of( value, ShapeData )

		self._shape = value
	
	# def shape ( self, value )

	# --------------------------------------------------

	@property
	def dtype ( self ):
		"""
		Data type of the nd-array (`numpy.dtype`).
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )

		return self._dtype

	# def dtype ( self )
	
	# --------------------------------------------------

	@dtype.setter
	def dtype ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_in( value, (numpy.dtype, str) )
		
		self._dtype = numpy.dtype(value) if type(value) == str else value

	# def dtype ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other ndarray feature data into this ndarray feature data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.dataset.NDArrayFeatureData`): NDArray feature data receiving the copy.
			other (`pydataset.dataset.NDArrayFeatureData`): The ndarray feature data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is_instance_of( other, NDArrayFeatureData )

		super( NDArrayFeatureData, self ).copy( other )
		
		self._shape = copy.deepcopy( other.shape )
		self._dtype = copy.deepcopy( other.dtype )

	# def copy ( self, other )

	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this ndarray feature to a python `dict`.

		Arguments:
			self (`pydataset.dataset.NDArrayFeatureData`): NDArray feature to serialize.
			ignores                     (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		
		# Run pydataset.data.ItemData serialize method, but skip value and dtype
		if ( 'value' not in ignores ):
			ignores.append( 'value' )
		if ( 'dtype' not in ignores ):
			ignores.append( 'dtype' )
		data = super( NDArrayFeatureData, self ).serialize( ignores )

		# serialize the value
		values, shape, dtype = pytools.serialization.serialize_ndarray( self.value )
		data[ 'dtype' ] = dtype
		data[ 'value' ] = values

		# Return the data
		return data

	# def serialize ( self )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this ndarray feature from a python `dict`.

		Arguments:
			self (`pydataset.dataset.NDArrayFeatureData`): NDArray feature to deserialize.
			data                                 (`dict`): The serialized data.
			ignores                     (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
	
		# for key, value in data.items():
		# 	print( '{}: {}'.format(key, value if key!='value' else '...') )

		# Run default deserialize method, but skip value
		if 'value' not in ignores:
			ignores.append( 'value' )
		if ( 'dtype' not in ignores ):
			ignores.append( 'dtype' )
		super( NDArrayFeatureData, self ).deserialize( data, ignores )

		# Deserialzize the dtype value as str
		dtype = data[ 'dtype' ]
		value = data[ 'value' ]
		value = pytools.serialization.deserialize_ndarray( value, self.shape.dimensions, dtype )
		self._dtype = dtype
		self._value = value
		
	# def __deserialize__ ( self, data )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this ndarray feature data.

		Arguments:
			self (`pydataset.dataset.NDArrayFeatureData`): NDArray feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this ndarray feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureData )

		return "[pydataset.dataset.NDArrayFeatureData: name='{}', url='{}', parent_url='{}', shape={}, dtype={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.shape,
			self.dtype.name
			)

	# def __str__ ( self )

# class NDArrayFeatureData ( FeatureData )