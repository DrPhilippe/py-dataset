# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###          CLASS BYTES-FEATURE-DATA          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'BytesFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class BytesFeatureData ( FeatureData ):
	"""
	The class `pydataset.dataset.BytesFeatureData` stores `bytes` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=b'' ):
		"""
		Initializes a new instance of the `pydataset.dataset.BytesFeatureData` class.

		Arguments:
			self (`pydataset.dataset.BytesFeatureData`): Instance to initialize.
			name                                (`str`): Name if the item.
			url                                 (`str`): URL if the item.
			parent_url                          (`str`): URL if the parent item.
			value                             (`bytes`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, bytes )

		super( BytesFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureData )
		assert pytools.assertions.type_is( val, bytes )

		self._value = val
	
	# def value ( self, value )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this bytes feature to a python `dict`.

		Arguments:
			self (`pydataset.dataset.BytesFeatureData`): Bytes feature to serialize.
			ignores                   (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		
		# Run pydataset.data.ItemData serialize method, but skip value
		if ( 'value' not in ignores ):
			ignores.append( 'value' )
		data = super( FeatureData, self ).serialize( ignores )

		# Serialzize the bytes value as str
		data[ 'value' ] = str( self.value, 'UTF-8' )

		# Return the data
		return data

	# def __serialize__ ( self )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this bytes feature from a python `dict`.

		Arguments:
			self (`pydataset.dataset.BytesFeatureData`): Bytes feature to deserialize.
			data                               (`dict`): The serialized data.
			ignores                   (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureData )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
	
		# Run default deserialize method, but skip value
		if 'value' not in ignores:
			ignores.append( 'value' )
		super( BytesFeatureData, self ).deserialize( data, ignores )

		# Deserialzize the str value as bytes
		self.value = bytes( data[ 'value' ], 'UTF-8' )

	# def __deserialize__ ( self, data )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this bytes feature data.

		Arguments:
			self (`pydataset.dataset.BytesFeatureData`): Bytes feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this bytes feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureData )

		return "[pydataset.dataset.BytesFeatureData: name='{}', url='{}', parent_url='{}', value={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class BytesFeatureData ( FeatureData )