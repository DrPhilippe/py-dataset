# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.text

# LOCALS
from .item_data import ItemData

# ##################################################
# ###              CLASS GROUP-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ExampleData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'features_names'
		]
	)
class ExampleData ( ItemData ):
	"""
	Informations used to describe an example and its contents.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	family_name = 'examples'

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', features_names=[] ):
		"""
		Initializes a new instance of the `pydataset.data.ExampleData` class.
		
		Example data are used to describe an example and its content.

		Arguments:
			self (`pydataset.dataset.ExampleData`): Instance to initialize.
			name                           (`str`): Name of the example.
			url                            (`str`): URL of this example.
			parent_url              (`None`/`str`): URL of the parent group or `None`.
			features_names        (`list of `str`): Names of the features of this example.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_in( parent_url, (type(None), str) )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )

		super( ExampleData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			)

		self.features_names = features_names

	# def __init__ ( self, name, url, parent_url, features_names )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features of this example (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleData )

		return self._features_names

	# def features_names ( self )
	
	# --------------------------------------------------

	@features_names.setter
	def features_names ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ExampleData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._features_names = value
		
	# def features_names ( self, value )

	# --------------------------------------------------

	@property
	def number_of_features ( self ):
		"""
		Number of features in this example data (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleData )

		return len(self.features_names)

	# def number_of_features ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other example data into this example data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.data.ExampleData`): Example data receiving the copy.
			other (`pydataset.data.ExampleData`): The data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleData )
		assert pytools.assertions.type_is_instance_of( other, ExampleData )

		super( ExampleData, self ).copy( other )
		
		self._features_names = copy.deepcopy( other.features_names )

	# def copy ( self, other )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this example data.

		Arguments:
			self (`pydataset.data.ExampleData`): Example data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this example data.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleData )

		return "[pydataset.data.ExampleData: name='{}', url='{}', parent_url='{}', features_names={}]".format(
			self.name,
			self.url,
			self.parent_url,
			pytools.text.list_to_str( self.features_names )
			)

	# def __str__ ( self )

# class ExampleData ( ItemData )