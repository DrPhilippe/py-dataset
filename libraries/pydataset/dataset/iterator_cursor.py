# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .example import Example
from .group   import Group

# ##################################################
# ###               CLASS ITERATOR               ###
# ##################################################

class IteratorCursor:
	"""
	Cursor used by iterators to keep track of the iteration over multiple groups and examples.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, root, pattern='*', what='examples' ):
		"""
		Initializes a new instance of the `pydataset.dataset.IteratorCursor` class.

		Arguments:
			self (`pydataset.dataset.IteratorCursor`): Instance to initialize.
			root        (`pydataset.dataset.Example`): Example or group this cursor iterates on.
			pattern                           (`str`): Pattern defining what this cursor iterates on.
			what                              (`str`): Names of the item family iterated over.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )
		assert pytools.assertions.type_is_instance_of( root, Example )
		assert pytools.assertions.type_is( pattern, str )
		assert pytools.assertions.type_is( what, str )
		assert pytools.assertions.value_in( what, ('examples', 'groups', 'self') )

		# Set fields
		self._root    = root
		self._pattern = pattern
		self._what    = what
		self._index   = 0

		# Does this cursor only consider one item ?
		if ( pattern != '*' ):
			self._count = 1

		# This cursor consideres all items of the group:
		else:
			# The items are example:
			if what == 'examples':
				self._count = root.number_of_examples

			# The items are group:
			elif what == 'groups':
				self._count = root.number_of_groups

			# The item is self
			elif what == 'self':
				self._count = 1

			# error
			else:
				return RuntimeError( 'Unsupported iterator target' )

		# if ( pattern != '*' )

	# def __init__ ( self, root, pattern, what )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def index ( self ):
		"""
		Current position of this cursor (`int`).
		"""
		return self._index

	# def index ( self )

	# --------------------------------------------------

	@property
	def count ( self ):
		"""
		Number of items the cursor iterates on (`int`).
		"""
		return self._count
	
	# def count ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_at ( self, index ):
		"""
		Returns the example at the given index in this cursor.
		
		Arguments:
			self (`pydataset.dataset.IteratorCursor`): The iterator cursor of wich to get and example.
			index                             (`int`): Index of the example to get.

		Returns:
			`pydataset.dataset.Example`: The example.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )
		assert pytools.assertions.type_is( index, int )

		# Check index
		if ( index >= self._count ):
			raise IndexError( "Invalid index: {}/{} {}".format(index, self._count, self._what) )

		# Get example
		if self._what == 'examples':
			return self._root.get_example_at( index )
		
		# Get group (groups are examples after all)
		elif self._what == 'groups':
			return self._root.get_group( index )
		
		# Get the group
		elif self._what == 'self':
			return self._root

		else:
			return RuntimeError( 'Unsupported iterator target' )

	# --------------------------------------------------

	def get_current ( self ):
		"""
		Returns the example corresponding to this cursor current position.
		
		Arguments:
			self (`pydataset.dataset.IteratorCursor`): The iterator cursor of wich to get the current example.

		Returns:
			`pydataset.dataset.Example`: Current example.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )

		return self.get_at( self._index )
		
	# def get_current ( self )

	# --------------------------------------------------

	def next ( self ):
		"""
		Move this cursor index by one.

		Arguments:
			self (`pydataset.dataset.IteratorCursor`): The cursor to move.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )

		if ( self._index >= self._count ):
			raise IndexError( 'Cursor already on the last item: {}/{}', self._index, self._count )

		self._index += 1

	# def next ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------
	
	def __str__ ( self ):
		"""
		Create a text representation of this cursor.

		Arguments:
			self (`pydataset.dataset.IteratorCursor`): The cursor to represent.

		Returns:
			`str`: Text representation of the cursor.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )
		
		return "[IteratorCursor '{}': {}/{} {}]".format( self._root.name, self._index, self._count, self._what )
	
	# def __str__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Create a text representation of this cursor.

		Arguments:
			self (`pydataset.dataset.IteratorCursor`): The cursor to represent.

		Returns:
			`str`: Text representation of the cursor.
		"""
		assert pytools.assertions.type_is_instance_of( self, IteratorCursor )

		return "[IteratorCursor '{}': {}/{} {}]".format( self._root.name, self._index, self._count, self._what )
	
	# def __repr__ ( self )

# class IteratorCursor