# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import json

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .backend                    import Backend
from .item_data                  import ItemData
from .register_backend_attribute import RegisterBackendAttribute

# ##################################################
# ###              CLASS A-BACKEND               ###
# ##################################################

@RegisterBackendAttribute( 'file' )
class FileBackend ( Backend ):
	"""
	The file backend stores dataset on local drives.

	Note:
		File backend may not be case sensitive on cerain operating systems (includin Windows).
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, url, name='file' ):
		"""
		Initializes a new instance of the `FileBackend` class.

		Arguments:
			url  (`str`/`pytools.path.DirectoryPath`): URL of the folder where to store data.
			name                              (`str`): Name of the backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is_instance_of( url, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( name, str )

		super( FileBackend, self ).__init__( str(url), name )

		self._path = url if isinstance(url, pytools.path.DirectoryPath) else pytools.path.DirectoryPath(url)
		
		if self.path.is_not_a_directory():
			self.path.create()

	# def __init__ ( self, path, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def path ( self ):
		"""
		Path of the folder where to store data (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )

		return self._path

	# def path ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_filepath ( self, url, family_name='items' ):
		"""
		Returns the path of the directory where to save the item
		and the full path to the item file in it.

		Arguments:
			self (`pydataset.dataset.FileBackend`): The file backend that stores the item.
			url                            (`str`): URL of the item.
			family_name                    (`str`): Name of the family the item is part of.

		Returns:
			path     (`str`): Path of the directory where to save the item
			filepath (`str`): Full path to the item file in it
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		# Path of the directory containing the item
		path = self.path + pytools.path.DirectoryPath( url )

		# The name of the item file is the family name minus the 's'
		filename = pytools.path.FilePath.format( '{}.json', family_name[0:-1] )
				
		# Create the final filepath
		filepath = path + filename

		# return the directory tree and the filepath
		return path, filepath

	# def get_filepath ( url, family_name )

	# --------------------------------------------------

	def contains ( self, url, family_name='items' ):
		"""
		Checks if an item with the given url exists in the stored dataset.

		Arguments:
			self (`pydataset.dataset.FileBackend`): FileBackend that stores the item.
			url                            (`str`): URL of the item.
			family_name                    (`str`): Family name of the item.

		Returns:
			`bool`: `True` if the item exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		path, filepath = self.get_filepath( url, family_name )
		return filepath.exists() and filepath.is_a_file()

	# def contains ( self, url, family_name )
	
	# --------------------------------------------------

	def delete ( self, url, family_name='items' ):
		"""
		Deletes the item with the given URL from the stored dataset.

		Arguments:
			self (`pydataset.dataset.FileBackend`): FileBackend that stores the item.
			url                            (`str`): URL of the item to delete.
			family_name                    (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		path, filepath = self.get_filepath( url, family_name )
		path.delete()

	# def delete ( self, url, family_name )

	# --------------------------------------------------

	def insert ( self, item_data, family_name='items' ):
		"""
		Inserts the given item in the stored dataset.

		Arguments:
			self    (`pydataset.dataset.FileBackend`): FileBackend that stores the item.
			item_data  (`pydataset.dataset.ItemData`): Data of the item to insert.
			family_name                       (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )

		path, filepath = self.get_filepath( item_data.url, family_name )
		if path.is_not_a_directory():
			path.create()
		filepath.serialize_json( item_data, ignores=['parent_url', 'url'] )

	# def insert ( self, item_data, family_name )

	# --------------------------------------------------

	def find ( self, url, family_name='items' ):
		"""
		Searches for the item with the given URL in the stored dataset.

		Arguments:
			self (`pydataset.dataset.FileBackend`): FileBackend that stores the item.
			url                            (`str`): URL of the item to search for.
			family_name                    (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( family_name, str )

		# Get the path of the file containing the item
		path, filepath = self.get_filepath( url, family_name )

		# Check it exists
		if ( not filepath.exists() or not filepath.is_a_file() ):
			return None
		
		# Deserialize the item
		item = filepath.deserialize_json( ignores=['parent_url', 'url'] )

		# Set its url
		item.url = url

		# Set its parent url
		parts = url.split( '/' )
		if len( parts ) >= 3:
			item.parent_url = '/'.join( parts[0:-2] )
		else:
			item.parent_url = ''

		# Return the item
		return item

	# def find ( self, url, family_name )
	
	# --------------------------------------------------

	def update ( self, item_data, family_name='items' ):
		"""
		Updates the given item in the stored dataset.

		Arguments:
			self    (`pydataset.dataset.FileBackend`): FileBackend that stores the item.
			item_data  (`pydataset.dataset.ItemData`): Data of the item to update.
			family_name                       (`str`): Family name of the item.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		assert pytools.assertions.type_is_instance_of( item_data, ItemData )
		assert pytools.assertions.type_is( family_name, str )

		return self.insert( item_data, family_name )

	# def update ( self, item_data, family_name )


	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this backend.

		Arguments:
			self (`pydataset.dataset.FileBackend`): FileBackend of which to create a text representation.

		Returns:
			`str`: Text representation of this backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileBackend )
		
		return "[pydataset.dataset.FileBackend: name='{}', url='{}']".format(
			self.name,
			self.url
			)

	# def __str__ ( self )

# class FileBackend ( Backend )