# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .dataset_error import DatasetError

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def split ( url ):
	"""
	Splits the given url in 4: backend_name, backent_url, dataset_name, content_url.

	Arguments:
		url (`str`): URL to split

	Returns:
		backend_name (`str`): Name of the backend.
		backend_url  (`str`): Backend internal URL.
		dataset_name (`str`): Name of the dataset.
		content_url  (`str`): URL of the content inside the dataset (if any).
	"""
	assert pytools.assertions.type_is( url, str )

	# Extract the name of the backend first
	parts = url.split( '//' )
	if len(parts) != 2:
		raise DatasetError.format( 'Invalid URL: "{}"', url )
	backend_name = parts[ 0 ][0:-1]

	# Split the rest
	parts = parts[ 1 ].split( '/' )
	count = len(parts)
	if count < 2:
		raise DatasetError.format( 'Invalid URL: "{}"', url )

	backend_url  = []
	dataset_name = None
	content_url  = []

	# Resolve the name of the dataset and its contents url
	# Starting from the end
	index = count-1
	previous = None
	while index >= 0:

		# Get the current part
		part = parts[ index ]

		# Is previous part a part of the content url
		# a group, an example or a feature ?
		if part in ['groups', 'examples', 'features']:
			
			if previous is None:
				raise DatasetError( 'Invalid url: {}'.format(url) )

			content_url.insert( 0, previous )
			content_url.insert( 0, part )
			dataset_name = None

		# Is the part the name of the dataset ?
		elif dataset_name is None:

			dataset_name = part

		# Then it is part of the backend URL
		# And we are done reading the contents URL
		else:
			break

		previous = part
		index -= 1

	# while index >= 0
	
	# Merge the contents url
	content_url = '/'.join( content_url )

	# If no more part is left the backend was not specified
	if ( index < 0 ):
		return backend_name, backend_url, dataset_name, content_url

	# Now the rest is easy
	# backend_name = parts[ 0 ][0:-1]
	backend_url  = parts[ 0:index+1 ]
	backend_url  = '/'.join( backend_url )

	return backend_name, backend_url, dataset_name, content_url

# def split ( url )
