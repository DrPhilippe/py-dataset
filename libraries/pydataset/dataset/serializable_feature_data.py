# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###      CLASS SERIALIZABLE-FEATURE-DATA       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'SerializableFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class SerializableFeatureData ( FeatureData ):
	"""
	The class `pydataset.data.SerializableFeatureData` stores `pytools.serialization.Serializable` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=None ):
		"""
		Initializes a new instance of the `pydataset.dataset.SerializableFeatureData` class.

		Arguments:
			self  (`pydataset.dataset.SerializableFeatureData`): Instance to initialize.
			name                                        (`str`): Name if the item.
			url                                         (`str`): URL if the item.
			parent_url                                  (`str`): URL if the parent item.
			value (`None`/`pytools.serialization.Serializable`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, SerializableFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pytools.serialization.Serializable) )

		super( SerializableFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, shape, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, SerializableFeatureData )
		assert pytools.assertions.type_is_instance_of( val, (type(None), pytools.serialization.Serializable) )

		self._value = val
	
	# def value ( self, val )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this serializable feature data.

		Arguments:
			self (`pydataset.dataset.SerializableFeatureData`): Serializable feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this serializable feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, SerializableFeatureData )

		return "[pydataset.data.SerializableFeatureData: name='{}', url='{}', parent_url='{}']".format(
			self.name,
			self.url,
			self.parent_url
			)

	# def __str__ ( self )

# class SerializableFeatureData ( FeatureData )