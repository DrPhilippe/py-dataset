# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .backend_factory import BackendFactory

# ##################################################
# ###      CLASS REGISTER-BACKEND-ATTRIBUTE      ###
# ##################################################

class RegisterBackendAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Attribute used to register a `pydataset.dataset.Backend` class.
	"""

	# ##################################################
	# ###                 CONSTRUCTOR                ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name ):
		"""
		Initializes a new instance of the `pydataset.dataset.RegisterBackendAttribute` class.

		Arguments:
			self (`pydataset.dataset.RegisterBackendAttribute`): The attribute to initialize.
			name                                        (`str`): Name of the backend to register.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterBackendAttribute )
		assert pytools.assertions.type_is( name, str )
		
		super( RegisterBackendAttribute, self ).__init__(
			   group = BackendFactory.group_name,
			typename = name
			)

	# def __init__ ( self, name )

# class RegisterBackendAttribute