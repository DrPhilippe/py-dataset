# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# ##################################################
# ###               DATASET-ERROR                ###
# ##################################################

class DatasetError ( pytools.Error ):
	"""
	Custom error type used to describe errors when using a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, message='' ):
		"""
		Initializes a new instance of the `pydataset.dataset.DatasetError` class.

		Arguments:
			self (`pydataset.dataset.DatasetError`): Instance to initialize.
			message                         (`str`): Error message.
		"""
		super( DatasetError, self ).__init__(
			message = message
			)

	# def __init__ ( self, message )
	
# class DatasetError ( pytools.Error )