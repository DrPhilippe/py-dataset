# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .item_data  import ItemData

# ##################################################
# ###             CLASS FEATURE-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'value'
		]
	)
class FeatureData ( ItemData ):
	"""
	The class `pydataset.dataset.FeatureData` is the base class of all specific feature data implementations.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	family_name = 'features'

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=None ):
		"""
		Initializes a new instance of the `pydataset.dataset.FeatureData` class.

		Arguments:
			self (`pydataset.dataset.FeatureData`): Instance to initialize.
			name                           (`str`): Name if the item.
			url                            (`str`): URL if the item.
			parent_url                     (`str`): URL if the parent item.
			value                          (`any`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )

		super( FeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url
			)

		self._value = value

	# def __init__ ( self, name, url, parent_url, value )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		Value of this feature (`any`):
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureData )

		return self._value
	
	# def _value ( self )
	
	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, FeatureData )

		self._value = val
	
	# def value ( self, val )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other feature data into this feature data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.dataset.FeatureData`): Feature data receiving the copy.
			other (`pydataset.dataset.FeatureData`): The data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureData )
		assert pytools.assertions.type_is_instance_of( other, FeatureData )

		super( FeatureData, self ).copy( other )
		
		self._value = copy.deepcopy( other.value )

	# def copy ( self, other )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this feature data.

		Arguments:
			self (`pydataset.dataset.FeatureData`): Feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureData )

		return "[pydataset.dataset.FeatureData: name='{}', url='{}', parent_url='{}']".format(
			self.name,
			self.url,
			self.parent_url
			)

	# def __str__ ( self )

# class FeatureData ( ItemData )