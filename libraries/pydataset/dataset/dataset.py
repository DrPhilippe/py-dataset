# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.text

# LOCALS
from .backend      import Backend
from .dataset_data import DatasetData
from .group        import Group

# ##################################################
# ###               CLASS DATASET                ###
# ##################################################

class Dataset ( Group ):
	"""
	A dataset is the top-level group that contains all the groups, examples and features of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, backend ):
		"""
		Initializes a new instance of the `pydataset.dataset.Dataset` class.

		Arguments:
			self    (`pydataset.dataset.Dataset`): Instance to initialize.
			data   (`pydataset.data.DatasetData`): Data of the dataset.
			backend (`pydataset.dataset.Backend`): Backend stroring the dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )
		assert pytools.assertions.type_is_instance_of( data, DatasetData )
		assert pytools.assertions.type_is_instance_of( backend, Backend )

		super( Dataset, self ).__init__(
			   data = data,
			 parent = None,
			backend = backend
			)
	
	# def __init__ ( self, data, backend )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this dataset.

		Arguments:
			self (`pydataset.dataset.Dataset`): Dataset of which to create a text representation.

		Returns:
			`str`: Text representation of this dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, Dataset )

		return "[pydataset.dataset.Dataset: name={}, features_names={}, groups_names={}, examples_names={}]".format(
			self.name,
			pytools.text.list_to_str( self.data.features_names ),
			pytools.text.list_to_str( self.data.groups_names ),
			pytools.text.list_to_str( self.data.examples_names )
			)
		
	# def __str__ ( self )

# class Dataset ( Group )