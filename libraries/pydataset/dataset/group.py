# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.tasks
import pytools.text

# LOCALS
from .backend       import Backend
from .backend_error import BackendError
from .dataset_error import DatasetError
from .example       import Example
from .example_data  import ExampleData
from .group_data    import GroupData
from .item          import Item

# ##################################################
# ###                CLASS GROUP                 ###
# ##################################################

class Group ( Example ):
	"""
	Group can regroup example as well as having features.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent, backend ):
		"""
		Initializes a new instance of the `pydataset.dataset.Group` class.

		Arguments:
			self      (`pydataset.dataset.Group`): Instance to initialize.
			data  (`pydataset.dataset.GroupData`): Data describing the group and its contents.
			parent     (`pydataset.dataset.Item`): Parent of this group.
			backend (`pydataset.dataset.Backend`): Backend that stores this group.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( data, GroupData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Item) )
		assert pytools.assertions.type_is_instance_of( backend, Backend )

		if ( parent is not None and parent.url != data.parent_url ):
			
			raise DatasetError.format(
				'The given parent is not the one reference in the group data: {} v/s {}',
				parent.url,
				data.parent
				)
			
		# if ( parent is not None and parent.url != data.parent_url )

		super( Group, self ).__init__(
			   data = data,
			 parent = parent,
			backend = backend
			)

	# def __init__ ( self, data, parent, backend )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def groups_names ( self ):
		"""
		Names of the sub-groups of this group (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		
		return self.data.groups_names
	
	# def groups_names ( self )

	# --------------------------------------------------

	@property
	def examples_names ( self ):
		"""
		Names of the examples of this group (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		
		return self.data.examples_names
	
	# def examples_names ( self )

	# --------------------------------------------------

	@property
	def number_of_examples ( self ):
		"""
		Number of examples in this group (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )

		return self.data.number_of_examples

	# def number_of_examples ( self )

	# --------------------------------------------------

	@property
	def number_of_groups ( self ):
		"""
		Number of sub-groups in this group (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )

		return self.data.number_of_groups

	# def number_of_groups ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def contains ( self, item_url ):
		"""
		Checks if this group contains a sub-item with the given url.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to search.
			item_url                 (`str`): URL of the item search for.

		Returns:
			`bool`: `True` if the sub-item exists, `False` otherwise.

		Raises:
			`pydataset.dataset.DatasetError`: If the URL is not valid.if the item does not exist.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( item_url, str )
		
		# Split the url
		parts = item_url.split( '/' )
		parts_count = len(parts)

		# Check the url
		if parts_count < 2:
			raise DatasetError.format( "Invalid URL (too short): '{}'", item_url )

		# Get the two first parts
		item_family = parts[ 0 ]
		item_name   = parts[ 1 ]

		# Rest of the url
		if parts_count > 2:
			url_rest = '/'.join( parts[2:] )
		else:
			url_rest = None

		# Search for a sub-example ?
		if item_family == 'examples':

			# If the example does not exist, done
			if not self.contains_example( item_name ):
				return False

			# If it does and we need to look deeper:
			elif url_rest:
				example = self.get_example( item_name )
				return example.contains( url_rest )

			# Otherwise, also done
			else:
				return True

		# Search for a sub-feature ?
		elif item_family == 'features':

			# Ask the deeper-self example
			return super( Group, self ).get( item_url )

		# Search for a sub-group ?
		elif item_family == 'groups':
			
			# If the group does not exist, done
			if not self.contains_group( item_name ):
				return False

			# If it does and we need to look deeper:
			elif url_rest:
				group = self.get_group( item_name )
				return group.contains( url_rest )

			# Otherwise, also done
			else:
				return True

		# Error
		else:
			raise DatasetError.format( "Invalid URL (unknown family): '{}'", item_url )

	# def contains ( self, item_url )

	# --------------------------------------------------

	def contains_example ( self, index_or_name, prefix='example', name_format='{}_{}' ):
		"""
		Checks if this group has an example with the given index or name.

		Additionaly a prefix can be specified (for augmentation and temporary examples)
		and a format to specify how to compose the name (and url) of the example
		using the given prefix and index.
		Unused if a name is provided.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to search for the example.
			index_or_name      (`int`/`str`): Index of name of the example.
			prefix                   (`str`): Name prefix of the example.
			name_format              (`str`): Name format string (with two placeholders for the prefix and the index).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_in( index_or_name, (int, str) )
		assert pytools.assertions.type_is( prefix, str )
		assert pytools.assertions.type_is( name_format, str )

		# compose the name of the example
		example_name = name_format.format( prefix, index_or_name ) if isinstance(index_or_name, int) else index_or_name
		example_url  = self.data.url + '/examples/' + example_name

		# Check if the example is referenced in this group
		if ( example_name in self.data.examples_names ):
			
			# If the example is stored in the backend, everything is fine
			if ( self.backend.contains( example_url, 'examples' ) ):
				return True

			# If not, the dataset is broken
			else:
				raise BackendError.format(
					"BROKEN DATASET: The example '{}' could not be found but it is referenced in group '{}'",
					example_url,
					self.data.url
					)

		# If the example is not referenced in this group
		else:

			# If the example is stored in the backend, dataset is broken
			if ( self.backend.contains( example_url, 'examples' ) ):

				raise BackendError.format(
					"BROKEN DATASET: The example '{}' exists but is not referenced in the group '{}'",
					example_url,
					self.data.url
					)

			# If not, it just doesnt exist
			else:
				return False

		# if ( example_name in self.data.examples_names )

	# def contains_example ( self, index_or_name, prefix, name_format )
	
	# --------------------------------------------------

	def contains_group ( self, group_name ):
		"""
		Checks if this group has a sub-group with the given name.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to search for the group.
			group_name               (`str`): Name of the group.

		Returns:
			`bool`: `True` if the groups exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( group_name, str )

		# compose the url of the group
		group_url  = self.data.url + '/groups/' + group_name

		# Check if the group is referenced in this group
		if ( group_name in self.data.groups_names ):
			
			# If the group is stored in the backend, everything is fine
			if ( self.backend.contains( group_url, 'groups' ) ):
				return True

			# If not, the dataset is broken
			else:
				raise BackendError.format(
					"BROKEN DATASET: The group '{}' could not be found but it is referenced in group '{}'",
					group_url,
					self.data.url
					)

		# If the group is not referenced in this group
		else:

			# If the group is stored in the backend, dataset is broken
			if ( self.backend.contains( group_url, 'groups' ) ):

				raise BackendError.format(
					"BROKEN DATASET: The group '{}' exists but is not referenced in the group '{}'",
					group_url,
					self.data.url
					)

			# If not, it just doesnt exist
			else:
				return False

		# if ( group_name in self.data.groups_names )

	# def contains_group ( self, group_name )
	
	# --------------------------------------------------

	def copy ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the other group into this group.

		Arguments:
			self                              (`pydataset.dataset.Group`): Group receiving copied data.
			other (`pydataset.dataset.Group`/`pydataset.dataset.Example`): The group or example to copy.
			auto_update                                          (`bool`): If `True` updates this group after the data are copied.
			progress_tracker     (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( other, (Group, Example) )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Sub progress-trackers for eqch set of items
		if progress_tracker:
			super_tracker = progress_tracker.create_child()
		else:
			super_tracker = None

		# If the other is a group
		if isinstance( other, Group ):
			
			# Sub progress-trackers for eqch set of items
			if progress_tracker:
				groups_tracker   = progress_tracker.create_child()
				examples_tracker = progress_tracker.create_child()
			else:
				groups_tracker   = None
				examples_tracker = None

			# Copy the groups of the other group
			self.copy_groups( other, False, groups_tracker )

			# Copy the examples of the other group
			self.copy_examples( other, False, examples_tracker )
		
		# Copy the data of the other example
		super( Group, self ).copy( other, auto_update, super_tracker )

		# Report progress
		if ( progress_tracker ):
			progress_tracker.update( 1.0 )

		return self

	# def copy ( self, other, auto_update, progress_tracker )
	
	# --------------------------------------------------

	def copy_example ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the other example into this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): Group receiving copied sub-examples.
			other                       (`pydataset.dataset.Example`): The example to copy.
			auto_update                                      (`bool`): If `True` updates this group after the example is copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.

		Returns:
			`pydataset.dataset.Example`: The new copy.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( other, Example )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Create an example of the same type in this group
		example_copy = self.create_example( other.data.name, auto_update=auto_update )

		# Use the Example.copy() method to copy the contents of the sub-example
		example_copy.copy( other, True, progress_tracker )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

		return example_copy

	# def copy_example ( self, other, auto_update, progress_tracker ):

	# --------------------------------------------------

	def copy_examples ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the sub-examples of the other group into this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): Group receiving copied sub-examples.
			other                         (`pydataset.dataset.Group`): The group of which to copy the sub-examples.
			auto_update                                      (`bool`): If `True` updates this group after the data are copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( other, Group )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Number of examples to copy
		number_of_examples = other.number_of_examples

		# Create sub-trackers for each sub-example
		sub_trackers = [None] * number_of_examples
		if progress_tracker:
			for example_index in range( number_of_examples ):
				sub_trackers[ example_index ] = progress_tracker.create_child()

		# Get through each sub-example to copy
		for example_index in range( number_of_examples ):

			# Get the sub-example from the other group
			example = other.get_example_at( example_index )
		
			# Copy the example
			self.copy_example( example, False, sub_trackers[ example_index ] )

			# Report progress
			if progress_tracker:
				progress_tracker.update( float(example_index)/float(number_of_examples) )

		# for example_index in range( number_of_examples )

		# Update this example ?
		if auto_update:
			self.update()

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

		return self

	# def copy_examples ( self, other, auto_update, progress_tracker )

	# --------------------------------------------------
	
	def copy_group ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the other group into this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): Group receiving copied sub-groups.
			other                         (`pydataset.dataset.Group`): The group to copy.
			auto_update                                      (`bool`): If `True` updates this group after the groups is copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( other, Group )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )
			
		# Create a group of the same type in this example
		group_copy = self.create_group( other.data.name )

		# Use the Group.copy() method to copy the contents of the sub-group
		group_copy.copy( other, auto_update, progress_tracker )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

		return group_copy

	# def copy_group ( self, other, auto_update, progress_tracker )

	# --------------------------------------------------

	def copy_groups ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the sub-groups of the other group into this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): Group receiving copied sub-groups.
			other                         (`pydataset.dataset.Group`): The group of which to copy the sub-groups.
			auto_update                                      (`bool`): If `True` updates this group after the data are copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( other, Group )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Number of groups to copy
		number_of_groups = other.number_of_groups

		# Create sub-trackers for each sub-group
		sub_trackers = [None] * number_of_groups
		if progress_tracker:
			for group_index in range( number_of_groups ):
				sub_trackers[ group_index ] = progress_tracker.create_child()

		# Get through each group to copy
		for group_index in range( number_of_groups ):

			# Get the group from the other group
			sub_group = other.get_group( group_index )
			
			# Copy the group
			self.copy_group( sub_group, False, sub_trackers[ group_index ] )

			# Report progress
			if progress_tracker:
				progress_tracker.update( float(group_index)/float(number_of_groups) )

		# for group_index in range( number_of_groups )

		# Update this example ?
		if auto_update:
			self.update()

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

		return self
		
	# def copy_groups ( self, other, auto_update, progress_tracker )

	# --------------------------------------------------

	def create ( self, item_url, auto_update=True ):
		"""
		Creates a sub-item with the given url in this group and all its parents.
		
		Only groups and examples can be created with this method.
		Features cannot be created with this method as the type of the feature data is required to create it.

		Arguments:
			self (`pydataset.dataset.Item`): Item in which to search.
			url                     (`str`): URL of the item search for.
			auto_update            (`bool`): If `True` this group is update after creating the item.

		Returns:
			`pydataset.dataset.Item`: The sub-item once created (the deepest one).

		Raises:
			`pydataset.dataset.DatasetError`: If the URL is not validi.
			`pydataset.dataset.DatasetError`: If the URL points to a feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( item_url, str )
		assert pytools.assertions.type_is( auto_update, bool )
		
		# Split the url
		parts = item_url.split( '/' )
		parts_count = len(parts)

		# Check the url
		if parts_count < 2:
			raise DatasetError.format( "Invalid URL (too short): '{}'", item_url )

		# Get the two first parts
		item_family = parts[ 0 ]
		item_name   = parts[ 1 ]

		# Rest of the url
		if parts_count > 2:
			# Check it the user is trying to create a feature
			if parts[ -2 ] == 'features':
				raise DatasetError.format( "Rejected URL '{}': cannot create features", item_url )
			url_rest = '/'.join( parts[2:] )
		else:
			url_rest = None

		# Create an example ?
		if item_family == 'examples':
			
			
			# More to create ?
			if url_rest:
				raise DatasetError.format( "Cannot create examples sub-item because a type is required" )

			# Create it
			else:
				return self.create_example( item_name, auto_update=auto_update )

		# Create a group ?
		elif item_family == 'groups':
			
			# More to create ?
			if url_rest:
				
				# its not an error if the groups exists here
				group = self.get_or_create_group( item_name, auto_update=auto_update )

				# Create the sub-item
				return group.create( url_rest, auto_update=True )

			# Return the group
			else:
				return self.create_group( item_name, auto_update=auto_update )

		# Create a feature or somethong else ?
		else:
			# Nope
			raise DatasetError.format( "Rejected URL '{}': cannot create {}", item_family )

	# def create ( self, url )

	# --------------------------------------------------

	def create_example ( self, index_or_name=None, prefix='example', name_format='{}_{}', auto_update=True ):
		"""
		Creates an example.
		
		By default this method composes the name of the example using the current example count
		and the given prefix and a name-format (that can be customized).
		But you can direcly specify the name of the example as well.

		Arguments:
			self   (`pydataset.dataset.Group`): Group in which to search for the example.
			index_or_name (`None`/`int`/`str`): Index or name of the example to create.
			prefix                     (`str`): Name prefix of the example.
			name_format                (`str`): Name format string (with two placeholders for the prefix and the index).
			auto_update               (`bool`): If `True` this group is update after the example is created.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( prefix, str )
		assert pytools.assertions.type_is( name_format, str )
		assert pytools.assertions.type_is( auto_update, bool )


		# Compose the index, name and url of the example
		# if None use the example count
		if index_or_name is None:
			index_or_name = self.data.number_of_examples
			while self.contains_example( index_or_name, prefix, name_format ):
				index_or_name += 1
		# If int then use the prefix and format to get the name
		if type(index_or_name) == int:
			index_or_name = name_format.format( prefix, index_or_name )
		# Create url
		example_url = self.data.url + '/examples/' + index_or_name

		if self.contains_example( index_or_name, prefix, name_format ):
			raise DatasetError.format( 'Cannot create example {} in group {} because is already exists', example_url, self._data.url ) 

		# Create the example data
		example_data = ExampleData(
			          name = index_or_name,
			           url = example_url,
			    parent_url = self.data.url,
			features_names = []
			)

		# Insert it in the backend
		self.backend.insert( example_data, 'examples' )

		# Add it to this group
		self.data.examples_names.append( index_or_name )

		# Update this group in the backend
		if ( auto_update ):
			self.update()

		# Create and return the example wrapper
		return Example(
			   data = example_data,
			 parent = self,
			backend = self.backend
			)

	# def create_example ( self, index_or_name, prefix, name_format )

	# --------------------------------------------------

	def create_group ( self, group_name, auto_update=True ):
		"""
		Creates a new group in this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to create the group.
			group_name               (`str`): Name of the group.
			auto_update             (`bool`): If `True` this group is update after the group is created.

		Returns:
			`pydataset.dataset.AGroup`: The new group.

		Raises:
			`pydataset.DatasetError`: If the group alread exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( auto_update, bool )

		# Check if the group already exists
		if self.contains_group( group_name ):

			raise DatasetError.format(
					"Cannot create group named '{}' because it already exists",
					self.absolute_url + '/groups/' + group_name
					)

		# Create the group data
		group_data = GroupData(
			          name = group_name,
			           url = self.data.url + '/groups/' + group_name,
			    parent_url = self.data.url,
			features_names = [],
			  groups_names = [],
			examples_names = []
			)

		# Save the group data in the backend
		self.backend.insert( group_data, 'groups' )

		# Add the group to this group
		self.data.groups_names.append( group_name )

		# Update this group
		if ( auto_update ):
			self.update()

		# Create and return the group wrapper
		return Group(
			   data = group_data,
			 parent = self,
			backend = self.backend
			)

	# def create_group ( self, group_name )

	# --------------------------------------------------

	def delete ( self, progress_tracker=None ):
		"""
		Deletes this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): Group to delete.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Sub progress-trackers for eqch set of items
		if progress_tracker:
			groups_tracker   = progress_tracker.create_child()
			examples_tracker = progress_tracker.create_child()
			example_tracker  = progress_tracker.create_child()
		else:
			groups_tracker   = None
			examples_tracker = None
			example_tracker  = None

		# Delete the groups
		self.delete_groups( auto_update=False, progress_tracker=groups_tracker )

		# Delete the examples
		self.delete_examples( auto_update=False, progress_tracker=examples_tracker )

		# Delete this group
		super( Group, self ).delete( progress_tracker=example_tracker )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete ( self, progress_tracker )

	# --------------------------------------------------

	def delete_example ( self, index_or_name, auto_update=True, progress_tracker=None ):
		"""
		Deletes an example of this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): The example.
			index_or_name                               (`int`/`str`): Index or name of the example to delete.
			auto_update                                      (`bool`): If `True` this group is updated after the example is deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_in( index_or_name, (int, str) )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		if isinstance( index_or_name, int ):
			example_name = self.examples_names[ index_or_name ]
		else:
			example_name = index_or_name

		# Get the example
		example = self.get_example( example_name )

		# Delete it
		example.delete( progress_tracker )

		# Remove it from this group
		self.data.examples_names.remove( example.data.name )

		# Update this group
		if ( auto_update ):
			self.update()

	# def delete_example ( self, index_or_name, auto_update, progress_tracker )

	# --------------------------------------------------

	def delete_examples ( self, auto_update=True, progress_tracker=None ):
		"""
		Deletes all the examples of this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): The group.
			auto_update                                      (`bool`): If `True` this group is updated after the examples are deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progression
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Get the number of examples
		number_of_examples = self.number_of_examples

		# Create a sub-tracker for each example
		trackers = [None] * number_of_examples
		for example_index in reversed(range( number_of_examples )):
			
			# Create it
			if progress_tracker:
				start       = float( example_index   )/float( number_of_examples )
				end         = float( example_index+1 )/float( number_of_examples )
				sub_tracker = progress_tracker.create_child( start, end )
			else:
				sub_tracker = None

			# Save it
			trackers[ example_index ] = sub_tracker

		# Go through each example and delete it
		for example_index in reversed(range( number_of_examples )):

			# Delete the example
			self.delete_example( example_index, False, trackers[ example_index ] )

		# Update this group
		if ( auto_update ):
			self.update()

		# Report progression
		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete_examples ( self, auto_update, progress_tracker )

	# --------------------------------------------------

	def delete_group ( self, index_or_name, auto_update=True, progress_tracker=None ):
		"""
		Deletes a group of this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): The example.
			index_or_name                               (`int`/`str`): Index or name of the group to delete.
			auto_update                                      (`bool`): If `True` this group is updated after the group is deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_in( index_or_name, (int, str) )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Get the group
		group = self.get_group( index_or_name )

		# Delete it
		group.delete( progress_tracker )

		# Remove it from this group
		self.data.groups_names.remove( group.data.name )

		# Update this group
		if ( auto_update ):
			self.update()

	# def delete_group ( self, index_or_name, progress_tracker, auto_update )
	
	# --------------------------------------------------

	def delete_groups ( self, auto_update=True, progress_tracker=None ):
		"""
		Deletes all the groups of this group.

		Arguments:
			self                          (`pydataset.dataset.Group`): The group.
			auto_update                                      (`bool`): If `True` this group is updated after the groups are deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progression
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Get the number of groups
		number_of_groups = self.number_of_groups

		# Create a sub-tracker for each group
		trackers = [None] * number_of_groups
		for group_index in reversed(range( number_of_groups )):
			
			# Create it
			if progress_tracker:
				start       = float( group_index   )/float( number_of_groups )
				end         = float( group_index+1 )/float( number_of_groups )
				sub_tracker = progress_tracker.create_child( start, end )
			else:
				sub_tracker = None

			# Save it
			trackers[ group_index ] = sub_tracker

		# Go through each group and delete it
		for group_index in reversed(range( number_of_groups )):

			# Delete the group
			self.delete_group( group_index, False, trackers[ group_index ] )

		# Update the group
		if ( auto_update ):
			self.update()

		# Report progression
		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete_groups ( self, auto_update, progress_tracker )

	# --------------------------------------------------

	def get ( self, item_url ):
		"""
		Searches for a sub-item with the given url in this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to search.
			item_url                 (`str`): URL of the item search for.

		Returns:
			`pydataset.dataset.Item`: The sub-item if found.

		Raises:
			`pydataset.dataset.DatasetError`: If the URL is not validif the item does not exist.
			`pydataset.dataset.DatasetError`: If the item does not exist.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( item_url, str )
		
		# Split the url
		parts = item_url.split( '/' )
		parts_count = len(parts)

		# Check the url
		if parts_count < 2:
			raise DatasetError.format( "Invalid URL (too short): '{}'", item_url )

		# Get the two first parts
		item_family = parts[ 0 ]
		item_name   = parts[ 1 ]

		# Get a sub-example ?
		if item_family == 'examples':
			item = self.get_example( item_name )

		# Get a sub-feature ?
		elif item_family == 'features':
			return super( Group, self ).get( item_url )

		# Get a sub-group ?
		elif item_family == 'groups':
			item = self.get_group( item_name )

		# Error
		else:
			raise DatasetError.format( "Invalid URL (unknown family): '{}', family: '{}'", item_url, item_family )

		# Search deeper ?
		if parts_count > 2:
			return item.get( '/'.join(parts[2:]) )

		# Return the item
		return item

	# def contains ( self, item_url )

	# --------------------------------------------------

	def get_example ( self, index_or_name, prefix='example', name_format='{}_{}' ):
		"""
		Retreives the example with the given index in this group.

		Additionaly a prefix can be specified (for augmentation and temporary examples)
		and a format to specify how to compose the name (and url) of the example
		using the given prefix and index.
		Unused if a name is provided.

		Arguments:
			self (`pydataset.dataset.Group`): Group in which to search for the example.
			index_or_name      (`int`/`str`): Index or name of the example.
			prefix                   (`str`): Name prefix of the example.
			name_format              (`str`): Name format string (with two placeholders for the prefix and the index).
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_in( index_or_name, (int, str) )
		assert pytools.assertions.type_is( prefix, str )
		assert pytools.assertions.type_is( name_format, str )
	
		# Create/get the name of the example
		if isinstance( index_or_name, int ):
			example_name = name_format.format( prefix, index_or_name )
		else:
			example_name = index_or_name

		#  Create the url of the example
		example_url  = self.data.url + '/examples/' + example_name

		# Check if the example exists
		if not self.contains_example( index_or_name, prefix, name_format ):

			raise DatasetError.format(
					"Cannot get example '{}' because it does not exist",
					example_url
					)
			
		# Find the example data in the backend
		example_data = self.backend.find( example_url, 'examples' )
		
		# Check the example data
		if example_data is None:

			raise BackendError.format(
					"BROKEN DATASET: Failed get example '{}' in the backend.",
					example_url
					)

		# Create and return the example wrapper
		return Example(
			   data = example_data,
			 parent = self,
			backend = self.backend
			)

	# def get_example ( self, index_or_name, prefix, name_format )

	# --------------------------------------------------

	def get_example_at ( self, index ):

		return self.get_example( self.data.examples_names[ index ] )

	# def get_example_at ( self, index )

	# --------------------------------------------------

	def get_group ( self, name_or_index ):
		"""
		Retreives the sub-group with the given name in this group.

		Arguments:
			self (`pydataset.dataset.AGroup`): Group from which to retreive the sub-group.
			name_or_index             (`str`): Name or index of the sub-group.

		Returns:
			`pydataset.dataset.Group`: The group if it exists.
			
		Raises:
			`pydataset.DatasetError`: If the group does not exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_in( name_or_index, (str, int) )

		# Get the name of the group if an index is given
		if isinstance( name_or_index, int ):
			group_name = self.groups_names[ name_or_index ]
		else:
			group_name = name_or_index

		# Create the group url
		group_url = self.data.url + '/groups/' + group_name

		# Check if the group exists
		if not self.contains_group( group_name ):

			raise DatasetError.format(
					"Cannot get group '{}' because it does not exist",
					group_url
					)
			
		# Find the group data in the backend
		group_data = self.backend.find( group_url, 'groups' )
		
		# Check the group data
		if group_data is None:
			
			raise BackendError.format(
					"BROKEN DATASET: Failed get group named '{}' in the backend.",
					group_name
					)

		# Create and return the group wrapper
		group = Group(
			   data = group_data,
			 parent = self,
			backend = self.backend
			)
		return group

	# def get_group ( self, group_name )

	# --------------------------------------------------

	def get_group_at ( self, index ):

		return self.get_group( self.data.groups_names[ index ] )

	# def get_group_at ( self, index )

	# --------------------------------------------------

	def get_or_create ( self, item_url, auto_update=True ):
		"""
		Retreives or creates the sub-item with the given url in this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group from which to retreive the item.
			item_url                 (`str`): URL of the item.
			auto_update             (`bool`): If `True` this group is updated after the example is created.

		Returns:
			`pydataset.dataset.Item`: The item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( item_url, str )

		if self.contains( item_url ):
			return self.get( item_url )
		else:
			return self.create( item_url, auto_update=auto_update )

	# --------------------------------------------------

	def get_or_create_example ( self, index_or_name, prefix='example', name_format='{}_{}', auto_update=True ):
		"""
		Retreives or creates the sub-example with the given index or name in this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group from which to retreive the example.
			index_or_name      (`int`/`str`): Index or name of the example.
			prefix                   (`str`): Name prefix of the example.
			name_format              (`str`): Name format string (with two placeholders for the prefix and the index).
			auto_update              (`bool`): If `True` this group is updated after the example is created.

		Returns:
			`pydataset.dataset.Group`: The group.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_in( index_or_name, (str, int) )
		assert pytools.assertions.type_is( prefix, str )
		assert pytools.assertions.type_is( name_format, str )
		assert pytools.assertions.type_is( auto_update, bool )

		if self.contains_example( index_or_name, prefix, name_format ):
			return self.get_example( index_or_name, prefix, name_format )
		else:
			return self.create_example( index_or_name, prefix, name_format, auto_update )

	# def get_or_create_group ( self, group_name )
	
	# --------------------------------------------------

	def get_or_create_group ( self, group_name, auto_update=True ):
		"""
		Retreives or creates the sub-group with the given name in this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group from which to retreive the group.
			group_name               (`str`): Name of the group.
			auto_update             (`bool`): If `True` this group is updated after the group is created.

		Returns:
			`pydataset.dataset.Group`: The group.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )
		assert pytools.assertions.type_is( group_name, str )
		assert pytools.assertions.type_is( auto_update, bool )

		if self.contains_group( group_name ):
			return self.get_group( group_name )
		else:
			return self.create_group( group_name, auto_update )

	# def get_or_create_group ( self, group_name, auto_update )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )

		# Sort features by name
		self._data._groups_names   = sorted( self._data._groups_names )
		self._data._examples_names = sorted( self._data._examples_names )

		# Update the example
		super( Group, self ).update()

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this group.

		Arguments:
			self (`pydataset.dataset.Group`): Group of which to create a text representation.

		Returns:
			`str`: Text representation of this group.
		"""
		assert pytools.assertions.type_is_instance_of( self, Group )

		return "[pydataset.dataset.Group: name='{}', url='{}', parent_url='{}', features_names={}, groups_names={}, examples_names={}]".format(
			self.data.name,
			self.data.url,
			self.data.parent_url,
			pytools.text.list_to_str( self.data.features_names ),
			pytools.text.list_to_str( self.data.groups_names ),
			pytools.text.list_to_str( self.data.examples_names )
			)
		
	# def __str__ ( self )

# class Group ( Example )