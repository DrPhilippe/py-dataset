# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .backend_error import BackendError

# ##################################################
# ###           CLASS BACKEND-FACTORY            ###
# ##################################################

class BackendFactory ( pytools.factory.ClassFactory ):
	"""
	Factory dedicated to intantiating backends.
	"""

	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------

	"""
	Name of the groups containing backends in the class factory (`str`).
	"""
	group_name = 'backends'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_backend ( cls, name ):
		"""
		Returns the backend with the given name.

		Arguments:
			cls (`type`): Backend factory type.
			name (`str`): Name of the factory to get.

		Returns:
			`type`: The backend type.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		# Check that backends exists
		if cls.get_number_of_backends() == 0:
			raise BackendError( "0 backends found ! thats realy strange, did you do something bad ?" )

		# Check that the backend exists
		if not cls.has_backend( backend_name ):
			raise BackendError.format(
				"Backend named '{}' does not exists !",
				backend_name
				)


		return cls.get_type( cls.group_name, name )

	# def get_backend ( cls, name )

	# --------------------------------------------------

	@classmethod
	def get_backends_names ( cls ):
		"""
		Returns the list of existing backends.

		Arguments:
			cls (`type`): Backend factory type.

		Returns:
			`list` of `str`: The names of the backends.
		"""
		assert pytools.assertions.type_is( cls, type )

		groups_names = cls.get_group_names()
		
		if groups_names and cls.group_name in groups_names:
			return cls.get_typenames_of_group( cls.group_name )
		
		else:
			return []

	# def get_backends_names ( cls )

	# --------------------------------------------------

	@classmethod
	def get_number_of_backends ( cls ):
		"""
		Returns the nuber of existing backends.

		Arguments:
			cls (`type`): Backend factory type.

		Returns:
			`int`: The number of the backends.
		"""
		assert pytools.assertions.type_is( cls, type )

		len( cls.get_backends_names() )

	# def get_number_of_backends ( cls )

	# --------------------------------------------------

	@classmethod
	def has_backend ( cls, name ):
		"""
		Checks if the backend with the given name exists.

		Arguments:
			cls (`type`): Backend factory type.

		Returns:
			`bool`: `True` if the backend exists, `False` otherwise.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		return name in cls.get_backends_names()

	# def has_backend ( cls, name )

	# --------------------------------------------------

	@classmethod
	def instantiate_backend ( cls, name, **kwargs ):
		"""
		Instantiates the backend with the given name.

		Arguments:
			cls                        (`type`): Backend factory type.
			name                        (`str`): Name of the factory to get.
			**kwargs (`dict` of `str` to `any`): Arguments forwarded to the backend constructor.

		Returns:
			`type`: The backend type.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		return cls.instantiate( cls.group_name, name, **kwargs )

	# def instantiate_widget_for ( cls, name, **kwargs )

# class BackendFactory ( pytools.factory.ClassFactory )