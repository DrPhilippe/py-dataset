# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.text

# LOCALS
from .backend          import Backend
from .dataset_error    import DatasetError
from .feature_data     import FeatureData
from .feature_ref_data import FeatureRefData
from .item             import Item

# ##################################################
# ###               CLASS FEATURE                ###
# ##################################################

class Feature ( Item ):
	"""
	Feature store values of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent, backend ):
		"""
		Initializes a new instance of the `pydataset.dataset.Feature` class.

		Arguments:
			self       (`pydataset.dataset.Feature`): Instance to initialize.
			data      (`pydataset.data.FeatureData`): Data describing the feature and its contents.
			parent (`None`/`pydataset.dataset.Item`): Parent of this feature.
			backend    (`pydataset.dataset.Backend`): Backend that stores this example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Feature )
		assert pytools.assertions.type_is_instance_of( data, FeatureData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Item) )
		assert pytools.assertions.type_is_instance_of( backend, Backend )

		if ( parent is not None and parent.url != data.parent_url ):
			raise DatasetError.format(
				'The given parent is not the one reference in the feature data: {} v/s {}',
				parent.url,
				data.parent_url
				)

		super( Feature, self ).__init__(
			   data = data,
			 parent = parent,
			backend = backend
			)

		if self.is_reference:
			try:
				self._reference = self.dataset.get( self._data.value )
			
			except DatasetError as e:
				raise DatasetError.format(
					"Failed to get referenced feature: '{}'. error: {}",
					self._data.value,
					str(e)
					)

	# def __init__ ( self, data, parent, backend )

	# ##################################################
	# ###                PROPERTIES                  ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing this item and its contents (`pydataset.data.ItemData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		if not self.is_reference:
			return self._data
		
		else:
			return self._reference._data

	# def data ( self )

	# --------------------------------------------------

	@property
	def is_reference ( self ):
		"""
		Boolean indicating if this feature is a reference (`bool`).
		"""
		return isinstance( self._data, FeatureRefData )

	# def is_reference ( self )
	
	# --------------------------------------------------

	@property
	def value ( self ):
		"""
		Value of this feature (`any`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Feature )

		if not self.is_reference:
			return self._data.value

		else:
			return self._reference._data.value

	# def value( self )
	
	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, Feature )

		if not self.is_reference:
			self._data.value = val
		
		else:
			self._reference._data.value = val
		
	# def value ( self, val )

	# ##################################################
	# ###                  MERHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this feature.

		Arguments:
			self (`pydataset.dataset.Feature`): Feature to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Feature )

		super( Feature, self ).update()

		if self.is_reference:
			self._reference.update()

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this example.

		Arguments:
			self (`pydataset.dataset.Example`): Example of which to create a text representation.

		Returns:
			`str`: Text representation of this example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Feature )
		
		return "[pydataset.dataset.Feature: name='{}', url='{}', parent_url='{}', is_reference='{}']".format(
			self.data.name,
			self.data.url,
			self.data.parent_url,
			self.is_reference
			)

	# def __str__ ( self )

# class Feature ( Item )