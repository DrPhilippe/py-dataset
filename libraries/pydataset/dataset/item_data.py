# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###              CLASS ITEM-DATA               ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ItemData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'name',
		'url',
		'parent_url'
		]
	)
class ItemData ( pytools.serialization.Serializable ):
	"""
	The class `ItemData` is the base class of all the contents of a dataset.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	family_name = 'items'

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='' ):
		"""
		Initializes a new instance of the `pydataset.dataset.ItemData` class.

		Arguments:
			self (`pydataset.dataset.ItemData`): Instance to initialize.
			name                        (`str`): Name if the item.
			url                         (`str`): URL if the item.
			parent_url                  (`str`): URL if the parent item.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )

		super( ItemData, self ).__init__()

		self.name       = name
		self.url        = url
		self.parent_url = parent_url

	# def __init__ ( self, name, url )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this item (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )

		return self._name

	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemData )
		assert pytools.assertions.type_is( value, str )

		self._name = value
		
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this item (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )

		return self._url

	# def url ( self )
	
	# --------------------------------------------------

	@url.setter
	def url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemData )
		assert pytools.assertions.type_is( value, str )

		self._url = value
		
	# def url ( self, value )
	
	# --------------------------------------------------

	@property
	def parent_url ( self ):
		"""
		URL of the parent of this item (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )

		return self._parent_url

	# def parent_url ( self )
	
	# --------------------------------------------------

	@parent_url.setter
	def parent_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ItemData )
		assert pytools.assertions.type_in( value, (type(None), str) )

		self._parent_url = value
		
	# def parent_url ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other item data into this item data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.dataset.ItemData`): Data receiving the copy.
			other (`pydataset.dataset.ItemData`): The data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )
		assert pytools.assertions.type_is_instance_of( other, ItemData )

	# def copy ( self, other )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this item data.
		
		Arguments:
			self (`pydataset.dataset.ItemData`): Item data of which to create a text representation.

		Returns:
			`str`: Text representation of this item data.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemData )

		return "[pydataset.dataset.ItemData: name='{}', url='{}', parent_url='{}']".format(
			self.name,
			self.url,
			self.parent_url
			)

	# def __str__ ( self )

# class ItemData ( pytools.serialization.Serializable )