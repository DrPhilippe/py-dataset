# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.text
import pydataset.data

# LOCALS
from .backend          import Backend
from .backend_error    import BackendError
from .dataset_error    import DatasetError
from .example_data     import ExampleData
from .feature          import Feature
from .feature_ref_data import FeatureRefData
from .item             import Item

# ##################################################
# ###               CLASS EXAMPLE                ###
# ##################################################

class Example ( Item ):
	"""
	Examples are used to store features.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent, backend ):
		"""
		Initializes a new instance of the `pydataset.dataset.Example` class.

		Arguments:
			self       (`pydataset.dataset.Example`): Instance to initialize.
			data      (`pydataset.data.ExampleData`): Data describing the example and its contents.
			parent (`None`/`pydataset.dataset.Item`): Parent of this example.
			backend    (`pydataset.dataset.Backend`): Backend that stores this example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( data, ExampleData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Item) )
		assert pytools.assertions.type_is_instance_of( backend, Backend )

		if ( parent is not None and parent.url != data.parent_url ):

			raise DatasetError.format(
				'The given parent is not the one reference in the example data: {} v/s {}',
				parent.url,
				data.parent_url
				)

		# if ( parent is not None and parent.url != data.parent_url )

		super( Example, self ).__init__(
			   data = data,
			 parent = parent,
			backend = backend
			)

	# def __init__ ( self, data, parent, backend )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def features_names ( self ):
		"""
		Names of the features of this example (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )

		return self.data.features_names

	# def features_names ( self )

	# --------------------------------------------------

	@property
	def number_of_features ( self ):
		"""
		Number of features in this example (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )

		return self.data.number_of_features

	# def number_of_features ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def contains ( self, feature_url ):
		"""
		Checks if this example contains a feature with the given url.

		The URL must point to a feature: "features/<feature name>":
		- two parts only, separated by a forward slash
		- the first part must be exactly 'features'
		- the second part is the name of the feature.
		If not a dataset error is raised.
		
		This method is used internaly when querying features at the dataset, or group level.
		To check if a feature exists at the example level use `Example.contains_feature` instead.

		Arguments:
			self (`pydataset.dataset.Example`): Example in which to search.
			feature_url                (`str`): URL of the feature search for.

		Returns:
			`bool`: `True` if the fature exists, `False` otherwise.

		Raises:
			`DatasetError`: If the url is not a feature URL.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( feature_url, str )
		
		# Split the URL
		parts = list( filter(None, feature_url.split('/')) )
		
		# Check the URL
		if len(parts) != 2 and parts[0] != 'features':
			raise DatasetError.format( "Invalid feature URL: '{}'", feature_url )

		# Look for the feature
		return self.contains_feature( parts[1] )

	# def contains ( self, url )

	# --------------------------------------------------

	def contains_feature ( self, feature_name, feature_data_type=None, inherited=True ):
		"""
		Checks if this example contains a feature with the given name.

		Arguments:
			self (`pydataset.dataset.Example`): Example in which to seach for the feature.
			feature_name               (`str`): Name of the feature.
			feature_data_type  (`None`/`type`): If a type is given, check that the feature is an instance of that type.
			inherited                 (`bool`): If `True` searches for the feature in the ancestors as well.

		Returns:
			`bool`: `True` if the feature exists, `False` otherwise.

		Raises:
			`pydataset.DatasetError`: If the dataset is broken (feature in example but not in backend, or the opposite).
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( feature_name, str )

		# compose the url of the feature
		feature_url  = self.data.url + '/features/' + feature_name

		# Check if the feature is referenced in this example
		if ( feature_name in self.data.features_names ):
			
			# If the feature is not stored in the backend, the dataset is broken
			if not self.backend.contains( feature_url, 'features' ):
				raise BackendError.format(
					"BROKEN DATASET: The feature '{}' could not be found but it is referenced in example '{}'",
					feature_url,
					self.data.url
					)

			# The feature exists, check the type of the feature ?
			if feature_data_type is not None:

				# Get the feature
				feature = self.get_feature( feature_name )

				# Check the type
				return isinstance( feature.data, feature_data_type )

			# The feature exists
			return True

		# If the feature is not referenced in this example
		# but the feature is stored in the backend, dataset is broken
		elif ( self.backend.contains( feature_url, 'features' ) ):

				raise BackendError.format(
					"BROKEN DATASET: The feature '{}' exists but is not referenced in the example '{}'",
					feature_url,
					self.data.url
					)

		# Maybe the feature is in a parent group ?
		elif self.parent is not None and inherited:

			# Search in the parent(s)
			return self.parent.contains_feature( feature_name, feature_data_type )

		# It definetly doesnt extist
		else:
			return False

		# if ( feature_name in self.data.features_names )
	
	# def contains_feature ( self, feature_name )

	# --------------------------------------------------

	def copy ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the other example into this example.

		Arguments:
			self                        (`pydataset.dataset.Example`): Example receiving copied data.
			other                       (`pydataset.dataset.Example`): The example to copy.
			auto_update                                      (`bool`): If `True` updates this example after the data are copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( other, Example )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Subprogress trackers
		if progress_tracker:
			features_tracker = progress_tracker.create_child()
			super_tracker    = progress_tracker.create_child()
		else:
			features_tracker = None
			super_tracker    = None

		# Copy the features of the other example
		self.copy_features( other, False, features_tracker )
		
		# Copy the data of the other example
		super( Example, self ).copy( other, auto_update, super_tracker )

		# Report progress
		if ( progress_tracker ):
			progress_tracker.update( 1.0 )

		return self

	# def copy ( self, other, auto_update, progress_tracker )

	# --------------------------------------------------

	def copy_feature ( self, other, auto_update=True ):
		"""
		Copy the other feature and adds it to this example.

		Arguments:
			self  (`pydataset.dataset.Example`): Example receiving copied features.
			other (`pydataset.dataset.Feature`): The feature to copy.
			auto_update                (`bool`): If `True` updates this example after the feature is copy.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( other, Feature )
		assert pytools.assertions.type_is( auto_update, bool )
		
		# Get the data of the feature
		# using ._data instead of .data ensures ref are copy as is
		other_feature_data = other._data

		# If indeed its a reference copy the reference directly
		if isinstance( other_feature_data, FeatureRefData ):
			
			# Create a feature of the same type in this example
			feature_copy = self.create_feature(
				     feature_name = other_feature_data.name,
				feature_data_type = FeatureRefData,
				      auto_update = auto_update,
				            value = other._data.value
				)
		
		# Otherwise copy feature data afterward
		else:
			# Create a feature of the same type in this example
			feature_copy = self.create_feature(
				     feature_name = other_feature_data.name,
				feature_data_type = type( other_feature_data ),
				      auto_update = auto_update
				)

			# Use the Feature.copy() method to copy the properties of the feature
			feature_copy.copy( other )

			# The feature has to be updated
			feature_copy.update()

		return feature_copy

	# def copy_feature ( self, other, auto_update )

	# --------------------------------------------------

	def copy_features ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the features of the other example into this example.

		Arguments:
			self                        (`pydataset.dataset.Example`): Example receiving copied features.
			other                       (`pydataset.dataset.Example`): The example of which to copy the feature.
			auto_update                                      (`bool`): If `True` updates this example after the data are copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( other, Example )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Number of features to copy
		number_of_features = other.number_of_features

		# Get through each feature to copy
		for feature_index in range( number_of_features ):

			# Get the feature for the other example
			feature = other.get_feature( feature_index )
			
			# Copy the feature
			self.copy_feature( feature, False )
			
			# Report progress
			if progress_tracker:
				progress_tracker.update( float(feature_index)/float(number_of_features) )

		# for feature_index in range( number_of_features )

		# Update this example ?
		if auto_update:
			self.update()

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

		return self
		
	# def copy_features ( self, other, progress_tracker )

	# --------------------------------------------------

	def create_feature ( self, feature_data_type, feature_name, auto_update=True, **kwargs ):
		"""
		Creates a new feature in this example.

		Arguments:
			self (`pydataset.dataset.Example`): Example in which to create the feature.
			feature_data_type         (`type`): Type of the feature data.
			feature_name               (`str`): Name to give to the feature (forwarded to the feature data constructor).
			auto_update               (`bool`): If `True` this example is updated after the feature is created.
			kwargs                    (`dict`): Arguments to forward to the feature data constructor.

		Returns:
			`pydataset.dataset.Feature`: The new feature.

		Raises:
			`pydataset.DatasetError`: If the feature already exists.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( feature_data_type, type )
		assert pytools.assertions.type_is( feature_name, str )
		assert pytools.assertions.type_is( auto_update, bool )

		# Create the URL of the feature
		feature_url = self.data.url + '/features/' + feature_name

		# Check if a feature with the same name already exist
		if self.contains_feature( feature_name, inherited=False ):

			raise DatasetError.format(
				"Cannot create feature '{}' because it already exists in this example '{}'",
				feature_url,
				self.data.url
				)

		# Create the feature data
		feature_data = feature_data_type(
			      name = feature_name,
			       url = feature_url,
			parent_url = self.data.url,
			**kwargs
			)

		# Save the feature data
		self.backend.insert( feature_data, 'features' )

		# Save the feature in the example
		self.data.features_names.append( feature_name )

		# Save this example
		if ( auto_update ):
			self.update()

		# Create and return the feature wrapper
		return Feature(
			   data = feature_data,
			 parent = self,
			backend = self.backend
			)

	# def create_feature ( self, feature_data_type, feature_name, **kwargs )
	
	# --------------------------------------------------

	def create_or_update_feature ( self, feature_name, feature_data_type, auto_update=True, **kwargs ):
		"""
		Creates or update a feature with the given name and data type.
	
		If the feature exists, it must have the same data type of the given data type.
		Named arguments (`**kwargs`) are forwarded to the feature data constructor if
		the feature does not already exists. If it exists then each item in it is considered
		as a pair property name - property value that is update in the existing feature data.
		
		Arguments:
			feature_name              (`str`): Name of the feature to create or update.
			feature_data_type        (`type`): Type of feature data.
			auto_update              (`bool`): If `True` this example is updated after the feature is created.
			**kwargs (`dict` of `str` to any): Properies name and value of the feature data.
		
		Returns:
			`pydataset.dataset.Feature`: The feature once created or updated.
		"""
		# If the feature exists
		if self.contains_feature( feature_name, inherited=False ):

			# Get the feature
			feature = self.get_feature( feature_name )

			# Check the type
			if type(feature.data) != feature_data_type:
				raise DatasetError.format(
					'Cannot update the existing feature "{}" of example "{}" because it does not have the expected type: existing={}, expected={}',
					feature_name,
					self.absolute_url,
					type(feature.data),
					feature_data_type
					)

			# Set the properties in the feature data
			for property_name, property_value in kwargs.items():
				setattr( feature.data, property_name, property_value )
	
			# Save the changes
			feature.update()

		# Otherwise
		else:
			# Create the feature
			feature = self.create_feature(
				feature_data_type,
				feature_name,
				auto_update,
				# Forward the properties
				**kwargs
				)

		return feature

	# def create_or_update_feature ( self, feature_name, feature_data_type, **kwargs )
	
	# --------------------------------------------------

	def delete ( self, progress_tracker=None ):
		"""
		Deletes this example.

		Arguments:
			self                        (`pydataset.dataset.Example`): Example to delete.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 0.0 )
		
		# Subprogress trackers
		if progress_tracker:
			features_tracker = progress_tracker.create_child()
			example_tracker  = progress_tracker.create_child()
		else:
			features_tracker = None
			example_tracker  = None

		# Delete the features of this example
		self.delete_features( False, features_tracker )

		# Delete the example itself
		super( Example, self ).delete( example_tracker )

		# Report progress
		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete ( self )

	# --------------------------------------------------

	def delete_feature ( self, index_or_name, auto_update=True, progress_tracker=None ):
		"""
		Deletes a feature of this example.

		Arguments:
			self                        (`pydataset.dataset.Example`): The example.
			index_or_name                               (`int`/`str`): Index or name of the feature to delete.
			auto_update                                      (`bool`): If `True` this example is updated after the feature is deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_in( index_or_name, (int, str) )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Get the feature
		feature = self.get_feature( index_or_name )

		# Delete it in the backend
		feature.delete( progress_tracker )

		# Unreference it from this example
		self.data.features_names.remove( feature.data.name )

		# Update this example
		if ( auto_update ):
			self.update()

	# def delete_feature ( self, index_or_name, progress_tracker )

	# --------------------------------------------------

	def delete_features ( self, auto_update=True, progress_tracker=None ):
		"""
		Deletes all the features of this example.

		Arguments:
			self                        (`pydataset.dataset.Example`): The example.
			auto_update                                      (`bool`): If `True` this example is updated after the features are deleted.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Report progression
		if progress_tracker:
			progress_tracker.update( 0.0 )

		# Get the number of features
		number_of_features = self.number_of_features

		# Create a sub-tracker for each feature
		trackers = [None] * number_of_features
		for feature_index in reversed(range( number_of_features )):
			
			# Create it
			if progress_tracker:
				sub_tracker = progress_tracker.create_child()
			else:
				sub_tracker = None

			# Save it
			trackers[ feature_index ] = sub_tracker

		# Go through each feature and delete it
		for feature_index in reversed(range( number_of_features )):

			# Delete the feature
			self.delete_feature( feature_index, False, trackers[ feature_index ] )

		# Update this example
		if ( auto_update ):
			self.update()

		# Report progression
		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete_features ( self, progress_tracker )

	# --------------------------------------------------

	def get ( self, url ):
		"""
		Searches for a feature with the given url in this example.
		
		The URL must point to a feature: "features/<feature name>":
		- two parts only, separated by a forward slash
		- the first part must be exactly 'features'
		- the second partis the name of the feature.
		If not a dataset error is raised.
		
		This method is used internaly when querying features at the dataset, or group level.
		To get features at the example level use `Example.get_feature` instead.
		
		Arguments:
			self (`pydataset.dataset.Example`): Example in which to search.
			url                        (`str`): URL of the feature to search for.

		Returns:
			`pydataset.dataset.Feature`: The feature if found.

		Raises:
			`DatasetError`: If the url is not a feature URL.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( url, str )
		
		# Split the URL
		parts = list( filter(None, url.split('/')) )
		
		# Check the URL
		if len(parts) != 2 and parts[0] != 'features':
			raise DatasetError.format( "Invalid feature URL: '{}'", url )

		# Look for the feature
		return self.get_feature( parts[1] )

	# def contains ( self, url )

	# --------------------------------------------------

	def get_feature ( self, index_or_name ):
		"""
		Retreives a feature in this example.

		Arguments:
			self (`pydataset.dataset.Example`): Example in which to seach for the feature.
			index_or_name        (`int`/`str`): Index or name of the feature to find.

		Returns:
			`pydataset.dataset.Feature`: The feature.

		Raises:
			`pydataset.DatasetError`: If the feature does not exist.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_in( index_or_name, (int, str) )

		# get feature name
		if isinstance( index_or_name, int ):

			# Check index			
			if index_or_name < 0 or index_or_name > self.number_of_features:
				raise IndexError( 'Index {} is out of bounds of this example features [0, {}].'.format(
					index_or_name,
					self.number_of_features
					))
			
			feature_name = self.data.features_names[ index_or_name ]
		
		else:
			feature_name = index_or_name

		# Check if the feature exists in this example
		if not self.contains_feature( feature_name ):

			raise DatasetError.format(
				"Cannot get feature '{}' because it does not exist in this example '{}'",
				feature_name,
				self.data.url
				)

		# If the feature is in this example return it
		if feature_name in self.data.features_names:

			# Create the URL of the feature
			feature_url = self.data.url + '/features/' + feature_name

			# Find the feature data in the backend
			feature_data = self.backend.find( feature_url, 'features' )
			
			# Check
			if feature_data is None:

				raise BackendError.format(
					"BROKEN DATASET: Failed to get feature '{}' from the backend",
					feature_url,
					self.data.url
					)

			# Create and return the feature wrapper
			return Feature(
				   data = feature_data,
				 parent = self,
				backend = self.backend
				)

		# Otherwise it is in one of the parent
		elif self.parent is not None:
			
			# Get it from the parent
			return self.parent.get_feature( feature_name )

		# this should not happend
		else:
			raise DatasetError(
				"BROKEN DATASET: Failed to get feature '{}' from example '{}' although the feature exists",
				feature_name,
				self.data.url
				)

	# def get_feature ( self, feature_name )

	# --------------------------------------------------

	def get_features ( self, features_names=[], inherited=True ):
		"""
		Creates and returns a dictionary containing the features of this example by name.

		If inherited is set to `True` features of the parents (groups up to the dataset)
		are also added to the result dictionary.

		Arguments:
			self (`pydataset.dataset.Example`): Example of which to get the features.
			features_names   (`list` of `str`): Names of the features to retreive.
			inherited                 (`bool`): If `True` includes the features of the parents.

		Returns:
			`dict` of `str` to `pydataset.dataset.Feature`: The features of the examples.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		assert pytools.assertions.type_is( features_names, list )
		assert pytools.assertions.list_items_type_is( features_names, str )
		assert pytools.assertions.type_is( inherited, bool )

		# Are features names provided
		# If they are not, all features are retreived
		names_are_not_specified = not bool(features_names)
		
		# Initialize the result
		features = {}

		# Add the features of this example
		for feature_name in self.features_names:

			# If names are not specified, or the feature is among the requested ones
			if names_are_not_specified or feature_name in features_names:
				
				# Get it
				features[ feature_name ] = self.get_feature( feature_name )
			
			# if not names_are_specified or feature_name in features_names

		# for feature_name in self.features_names

		# Include inherited features ?
		if ( inherited ):

			# Start whith this example's parent
			node = self.parent

			# As long as we havent reached the dataset level (root node)
			while ( node is not None ):

				# Look the features of the parent
				for feature_name in node.features_names:
	
					# If names are not specified, or the feature is among the requested ones
					if names_are_not_specified or feature_name in features_names:
						
						# Features of the child override parent feature
						# So ignore features that have already been found
						if ( feature_name not in features.keys() ):

							# Get the feature
							features[ feature_name ] = node.get_feature( feature_name )
						
						# if ( feature_name not in features.keys() )

					# if not names_are_specified or feature_name in features_names

				# for feature_name in node.features_names

				# Move on tho the next parent
				node = node.parent

			# while ( node is not None )

		# if ( inherited )

		return features
		
	# def get_features ( self, features_names, inherited )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this example.

		Arguments:
			self (`pydataset.dataset.Example`): Example to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )

		# Sort features by name
		self._data._features_names = sorted( self._data._features_names )

		# Update the item
		super( Example, self ).update()

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this example.

		Arguments:
			self (`pydataset.dataset.Example`): Example of which to create a text representation.

		Returns:
			`str`: Text representation of this example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Example )
		
		return "[pydataset.dataset.Example: name='{}', url='{}', parent_url='{}', features_names={}]".format(
			self.data.name,
			self.data.url,
			self.data.parent_url,
			pytools.text.list_to_str( self.data.features_names )
			)

	# def __str__ ( self )

# class Example ( Item )