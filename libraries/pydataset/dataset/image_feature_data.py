# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
import base64
import copy
import cv2
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from ..data                import ShapeData
from .ndarray_feature_data import NDArrayFeatureData

# ##################################################
# ###          CLASS IMAGE-FEATURE-DATA          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ImageFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = [
		'extension',
		'param'
		]
	)
class ImageFeatureData ( NDArrayFeatureData ):
	"""
	The class `pydataset.dataset.ImageFeatureData` stores `numpy.ndarray` image values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', shape=None, dtype=None, value=numpy.empty(0, dtype=numpy.uint8), extension='.jpeg', param=90 ):
		"""
		Initializes a new instance of the `pydataset.dataset.ImageFeatureData` class.
		
		The value `extension` must be one of '.jpeg', '.jpg', '.jpe', '.png' and param must be set according to the chosen extension.
		See opencv imwrite documentation for more informations:
		https://docs.opencv.org/master/d4/da8/group__imgcodecs.html

		Arguments:
			self (`pydataset.dataset.ImageFeatureData`): Instance to initialize.
			name                                (`str`): Name if the item.
			url                                 (`str`): URL if the item.
			parent_url                          (`str`): URL if the parent item.
			shape          (`pydataset.data.ShapeData`): Shape of this feature.
			dtype                 (`str`/`numpy.dtype`): Data type of the nd-array .
			value                     (`numpy.ndarray`): The value of the feature.
			extension                           (`str`): Format used to encode and compress the image.
			param                               (`int`): Encoding parameter.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is_instance_of( shape, (type(None), ShapeData) )
		assert pytools.assertions.type_in( dtype, (type(None), numpy.dtype, str) )
		assert pytools.assertions.type_is( value, numpy.ndarray )
		assert pytools.assertions.type_is( extension, str )
		assert pytools.assertions.value_in( extension.lower(), ['.jpeg', '.jpg', '.jpe', '.png'] )
		assert pytools.assertions.type_is( param, int )

		super( ImageFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     shape = shape,
			     dtype = dtype,
			     value = value
			)

		self.extension = extension.lower()
		self.param     = param

	# def __init__ ( self, name, url, parent_url, shape, dtype, value, extension, param )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def extension ( self ):
		"""
		Format used to encode and compress the image (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )

		return self._extension
	
	# def extension ( self )

	# --------------------------------------------------

	@extension.setter
	def extension ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value.lower(), ['.jpeg', '.jpg', '.jpe', '.png'] )

		self._extension = value.lower()
	
	# def extension ( self, value )

	# --------------------------------------------------

	@property
	def param ( self ):
		"""
		Encoding parameter (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )

		return self._param
	
	# def param ( self )

	# --------------------------------------------------

	@param.setter
	def param ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is( value, int )

		self._param = value
	
	# def param ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, other ):
		"""
		Copy the properties of the other image feature data into this image feature data.
		
		The `name`, `parent_url` and `url` are not modified.

		Arguments:
			self  (`pydataset.dataset.ImageFeatureData`): Image feature data receiving the copy.
			other (`pydataset.dataset.ImageFeatureData`): The image feature data to copy
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is_instance_of( other, ImageFeatureData )

		super( ImageFeatureData, self ).copy( other )
		
		self._extension = copy.deepcopy( other.extension )
		self._param     = copy.deepcopy( other.param )

	# def copy ( self, other )

	# --------------------------------------------------

	def serialize ( self, ignores=[] ):
		"""
		Serializes the fields of this image feature to a python `dict`.

		Arguments:
			self (`pydataset.dataset.ImageFeatureData`): Image feature to serialize.
			ignores                   (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict`: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
		

		if self.extension in ['.jpeg', '.jpg', '.jpe'] and self.shape.get_rank() > 2 and self.shape[2] == 4:
			print(
				'WARNING: ImageFeatureData.extension is {}: Dropping image alpha channel'.format(self.extension)
				)


		# Run pydataset.data.ItemData serialize method, but skip value
		if ( 'value' not in ignores ):
			ignores.append( 'value' )
		if ( 'dtype' not in ignores ):
			ignores.append( 'dtype' )
		data = super( NDArrayFeatureData, self ).serialize( ignores )

		# Decifer what flag to use to compress the image
		if self.extension in ['.jpeg', '.jpg', '.jpe']:
			flags = [cv2.IMWRITE_JPEG_QUALITY, self.param]
		elif self.extension in ['.png']:
			flags = [cv2.IMWRITE_PNG_COMPRESSION , self.param]
		else:
			raise ValueError( 'Inrecognized extension: {}'.format(self.extension) )

		# Serialzize the bytes value as str
		value = self.value
		if value.size != 0:
			ret, value = cv2.imencode( self.extension, value, flags )
			if ( not ret ):
				raise RuntimeError( 'Failed to encode image feature {}'.format(self.name) )
		value = value.tostring()
		value = base64.encodebytes( value )
		value = str( value, 'utf8' )
		data[ 'value' ] = value

		# Serialzize the dtype value as str
		data[ 'dtype' ] = self.dtype.name

		# Return the data
		return data

	# def __serialize__ ( self )

	# --------------------------------------------------

	def deserialize ( self, data, ignores=[] ):
		"""
		Deserializes the fields of this image feature from a python `dict`.

		Arguments:
			self (`pydataset.dataset.ImageFeatureData`): Image feature to deserialize.
			data                               (`dict`): The serialized data.
			ignores                   (`list` of `str`): Names of fields that should be ignored.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )
		assert pytools.assertions.type_is( data, dict )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )
	
		# Run default deserialize method, but skip value
		if 'value' not in ignores:
			ignores.append( 'value' )
		if ( 'dtype' not in ignores ):
			ignores.append( 'dtype' )
		super( NDArrayFeatureData, self ).deserialize( data, ignores )

		# Deserialzize the dtype value as str
		dtype = data[ 'dtype' ]
		self.dtype = numpy.dtype(dtype)

		# Deserialzize the str value as bytes
		value = data[ 'value' ]
		value = bytes( value, 'utf8' )
		value = base64.decodebytes( value )
		value = numpy.fromstring( value, dtype=numpy.uint8 )
		value = cv2.imdecode( value, cv2.IMREAD_UNCHANGED )
		# value = numpy.reshape( value, self.shape.dimensions )
		self.value = value
		
	# def __deserialize__ ( self, data )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this image feature data.

		Arguments:
			self (`pydataset.dataset.ImageFeatureData`): Image feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this image feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureData )

		return "[pydataset.dataset.ImageFeatureData: name='{}', url='{}', parent_url='{}', shape={}, extension={}, param={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.shape,
			self.extension,
			self.param
			)

	# def __str__ ( self )

# class ImageFeatureData ( NDArrayFeatureData )