# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .text_feature_data import TextFeatureData

# ##################################################
# ###           CLASS FEATURE-REF-DATA           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FeatureRefData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class FeatureRefData ( TextFeatureData ):
	"""
	The class `pydataset.dataset.FeatureRefData` stores `str` feature references values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value='' ):
		"""
		Initializes a new instance of the `pydataset.dataset.FeatureRefData` class.

		Arguments:
			self (`pydataset.dataset.FeatureRefData`): Instance to initialize.
			name                              (`str`): Name if the item.
			url                               (`str`): URL if the item.
			parent_url                        (`str`): URL if the parent item.
			value                             (`str`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureRefData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, str )

		super( FeatureRefData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, shape, value )
	
	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this int feature data.

		Arguments:
			self (`pydataset.dataset.FeatureRefData`): Text feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this int feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureRefData )

		return "[pydataset.dataset.FeatureRefData: name='{}', url='{}', parent_url='{}', value='{}']".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class FeatureRefData ( FeatureData )