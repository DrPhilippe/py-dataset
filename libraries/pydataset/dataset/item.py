# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pydataset.data

# LOCALS
from .backend       import Backend
from .dataset_error import DatasetError
from .item_data     import ItemData

# ##################################################
# ###                 CLASS ITEM                 ###
# ##################################################

class Item:
	"""
	The `Item` class is the base wrapper for dataset contents.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, data, parent, backend ):
		"""
		Initializes a new instance of the `pydataset.dataset.Item` class.

		Arguments:
			self          (`pydataset.dataset.Item`): Instance to initialize.
			item_data    (`pydataset.data.ItemData`): Item data describing its contents.
			parent (`None`/`pydataset.dataset.Item`): Parent of this item.
			backend    (`pydataset.dataset.Backend`): Backend that stores this item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )
		assert pytools.assertions.type_is_instance_of( data, ItemData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), Item) )
		assert pytools.assertions.type_is_instance_of( backend, Backend )
		
		if ( parent is not None and parent.url != data.parent_url ):
			
			raise DatasetError.format(
				'The given parent is not the one reference in the item data: {} v/s {}',
				parent.url,
				data.parent
				)
			
		# if ( parent is not None and parent.url != data.parent_url )

		self._data    = data
		self._parent  = parent
		self._backend = backend

	# def __init__ ( self, data, parent, backend )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def absolute_url ( self ):
		"""
		Absolute URL of this item (`str`).

		The absolute URL goes back up to the backend.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self.backend.name + '://' + self.backend.url + '/' + self.url

	# def absolute_url ( self )
	
	# --------------------------------------------------

	@property
	def data ( self ):
		"""
		Data describing this item and its contents (`pydataset.data.ItemData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self._data

	# def data ( self )

	# --------------------------------------------------

	@property
	def dataset ( self ):
		"""
		Dataset this item belongs to (`pydataset.data.Item`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		dataset = self
		while ( dataset.parent ):
			dataset = dataset.parent
		return dataset

	# def dataset ( self )

	# --------------------------------------------------

	@property
	def parent ( self ):
		"""
		Parent of this item (`pydataset.dataset.Item`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self._parent

	# def parent ( self )

	# --------------------------------------------------

	@property
	def backend ( self ):
		"""
		Backend that stores this group (`pydataset.dataset.Backend`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )
		
		return self._backend

	# def backend ( self )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of this item (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self._data.name

	# def name ( self )

	# --------------------------------------------------

	@property
	def url ( self ):
		"""
		URL of this item (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self._data.url

	# def url ( self )
	
	# --------------------------------------------------

	@property
	def parent_url ( self ):
		"""
		URL of the parent group of this example (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		return self.data.parent_url

	# def parent_url ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def copy ( self, other, auto_update=True, progress_tracker=None ):
		"""
		Copy the data of the other item into this item.

		Arguments:
			self                           (`pydataset.dataset.Item`): Item receiving copied data.
			other                          (`pydataset.dataset.Item`): The imem to copy.
			auto_update                                      (`bool`): If `True` updates this item after the data are copied.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )
		assert pytools.assertions.type_is_instance_of( other, Item )
		assert pytools.assertions.type_is( auto_update, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		if ( progress_tracker ):
			progress_tracker.update( 0.0 )

		self._data.copy( other._data )

		if ( progress_tracker ):
			progress_tracker.update( 0.5 )

		if auto_update:
			self.update()

		if ( progress_tracker ):
			progress_tracker.update( 1.0 )

	# def copy ( self, other, auto_update, progress_tracker )

	# --------------------------------------------------

	def delete ( self, progress_tracker=None ):
		"""
		Deletes this item.

		Arguments:
			self                           (`pydataset.dataset.Item`): Item to delete.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		if progress_tracker:
			progress_tracker.update( 0.0 )

		self.backend.delete( self._data.url, type(self._data).family_name )

		if progress_tracker:
			progress_tracker.update( 1.0 )

	# def delete ( self )

	# --------------------------------------------------

	def reload ( self ):
		"""
		Reload this item.

		Arguments:
			self (`pydataset.dataset.Item`): Item to reload.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		self._data = self.backend.find( self._data.url, type(self._data).family_name )

	# def update ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this item.

		Arguments:
			self (`pydataset.dataset.Item`): Item to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )

		self.backend.update( self._data, type(self._data).family_name )

	# def update ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this item.

		Arguments:
			self (`pydataset.dataset.Item`): Item of which to create a text representation.

		Returns:
			`str`: Text representation of this item.
		"""
		assert pytools.assertions.type_is_instance_of( self, Item )
		
		return "[pydataset.dataset.Item: name='{}', url='{}', parent_url='{}']".format(
			self._data.name,
			self._data.url,
			self._data.parent_url
			)

	# def __str__ ( self )

# class Item