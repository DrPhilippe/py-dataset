# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools

# ##################################################
# ###               BACKEND-ERROR                ###
# ##################################################

class BackendError ( pytools.Error ):
	"""
	Custom error type used to describe backend errors.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, message='' ):
		"""
		Initializes a new instance of the `pydataset.dataset.BackendError` class.

		Arguments:
			self (`pydataset.dataset.BackendError`): Instance to initialize.
			message                         (`str`): Error message.
		"""
		super( BackendError, self ).__init__(
			message = message
			)

	# def __init__ ( self, message )
	
# class BackendError ( pytools.Error )