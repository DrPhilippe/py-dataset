# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions

# LOCALS
from .                import functions
from .                import url
from .example         import Example
from .group           import Group
from .iterator_cursor import IteratorCursor

# ##################################################
# ###               CLASS ITERATOR               ###
# ##################################################

class Iterator:
	"""
	Iterator are used to iterate over examples of a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, url_search_pattern ):
		"""
		Initiamizes a new instance of the iterator class.

		Arguments:
			self (`pydataset.dataset.Iterator`): Instance to initialize.
			url_search_pattern          (`str`): URL search pattern used to select iterated examples.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )
		assert pytools.assertions.type_is( url_search_pattern, str )

		# Save the url
		self._url_search_pattern = url_search_pattern

		# Split the URL search pattern
		backend_name, backend_url, dataset_name, content_url = url.split( url_search_pattern )

		# Split the content URL
		fixed_url, patterns = Iterator._split_contents_url( content_url )

		# Get the dataset
		dataset_url = '{}://{}/{}'.format( backend_name, backend_url, dataset_name )
		dataset     = functions.get( dataset_url )

		# Append the fixed part of the contents url to get the root group url
		if fixed_url:
			root_group_url = '{}://{}/{}/{}'.format( backend_name, backend_url, dataset_name, fixed_url )
			root_group = functions.get( root_group_url )
		
		# Otherwise the root group is the dataset
		else:
			root_group = dataset

		# Init fields
		self._dataset         = dataset
		self._root_group      = root_group
		self._cursors         = self._create_cursors( root_group, patterns )
		self._cursor_index    = 0
		self._count           = self._get_count()

	# def __init__ ( self, url_search_pattern )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def count ( self ):
		"""
		Total number of examples iterated over by this iterator (`int`).
		"""
		return self._count

	# def count ( self )

	# --------------------------------------------------

	@property
	def dataset ( self ):
		"""
		Dataset containing the examples/groups being iterated (`pydataset.dataset.Group`).
		"""
		return self._dataset
	
	# def dataset ( self )

	# --------------------------------------------------

	@property
	def root_group ( self ):
		"""
		Group containing the examples/groups being iterated (`pydataset.dataset.Group`).
		"""
		return self._root_group
	
	# def root_group ( self )
	
	# --------------------------------------------------

	@property
	def url_search_pattern ( self ):
		"""
		URL search pattern used by the iterator to locate items to iterate on (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )

		return self._url_search_pattern
	
	# def url_search_pattern ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _create_cursors ( self, root, patterns, depth=0 ):
		"""
		Creates the cursors used by this iterator to iterate over the examples.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator of which to create the cursors.
			root  (`pydataset.dataset.Example`): Example or group of which to iterate the examples.
			patterns          (`list` of `str`): Patterns used to select the examples to considere during iteration.
			depth                       (`int`): Current search depth.

		Returns:
			`list` of `pydataset.dataset.IteratorCursor`: The cursors.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )
		assert pytools.assertions.type_is_instance_of( root, Example )
		assert pytools.assertions.type_is( patterns, list )
		assert pytools.assertions.list_items_type_is( patterns, str )
		assert pytools.assertions.type_is( depth, int )

		# Init the result
		cursors = []

		# If only the root is considered
		if not patterns:
			cursor = IteratorCursor( root, '', 'self' )
			cursors.append( cursor )

		else:
			search_depth = len(patterns)//2
			family       = patterns[ depth*2 ]
			pattern      = patterns[ depth*2+1 ]

			# If we are not on the max saerch depth
			# Recurse on the sub groups
			if ( depth < search_depth-1 ):

				# Cursor iterates on examples
				if ( family == 'examples' ):
					raise RuntimeError( "The search pattern 'examples' appeares in the middle of the search parttern {}".format(self._patterns) )

				# If the partter is a named group, only consider this group
				if ( pattern != '*' ):
					sub_group = root.get_group( pattern )
					cursors  += self._create_cursors( sub_group, patterns, depth+1 )
				
				# Otherwise considere all the groups
				else:
					for group_index in range( root.number_of_groups ):
						sub_group = root.get_group( group_index )
						cursors  += self._create_cursors( sub_group, patterns, depth+1 )

			# If we are at the max saerch depth,
			# Create the cursors
			else:
				cursor = IteratorCursor( root, pattern, family )
				cursors.append( cursor )
		
		# if not patterns

		return cursors

	# def _create_cursors ( self, group, patterns, depth )

	# --------------------------------------------------

	def _get_current_cursor ( self ):
		"""
		Returns the current of this iterator.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator.

		Returns:
			`pydataset.dataset.IteratorCursor`: The current cursor.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )

		return self._cursors[ self._cursor_index ]
	
	# def _get_current_cursor ( self )

	# --------------------------------------------------
	
	def _get_count ( self ):
		"""
		Computes the total number of item iterated by this iterator.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator.

		Returns:
			`int`: count.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )
		
		total = 0
		for cursor in self._cursors:
			total += cursor.count
		return total

	# def _get_count ( self )

	# --------------------------------------------------

	def at ( self, index ):
		"""
		Returns the example at the given index in this iterator references.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator.
			index                     (`index`): Index of the example to get in this iterator.

		Returns:
			`pydatatset.dataset.Example`: Current example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )
		assert pytools.assertions.type_is( index, int )
		
		# Check index
		if ( index < 0 or index >= self.count ):
			raise IndexError( "Invalid index: {}/{}".format(index, self.count) )

		offset = 0
		for cursor in self._cursors:

			cursor_index = index - offset
			cursor_count = cursor.count

			if ( cursor_index < cursor_count ):
				return cursor.get_at( cursor_index )

			offset += cursor_count

		# for cursor in self._cursors

	# def at ( self, index )

	# --------------------------------------------------

	def current ( self ):
		"""
		Returns the current example this iterator references.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator.

		Returns:
			`pydatatset.dataset.Example`: Current example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )
		
		# Get the current example from the cursor
		return self._get_current_cursor().get_current()

	# def current ( self )

	# --------------------------------------------------

	def next ( self ):
		"""
		Returns the current example and move this iterator on the next one.
		
		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator.

		Returns:
			`pydatatset.dataset.Example`: Current example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )

		# Check if the iterator already went through all the cursors
		if ( self._cursor_index == len(self._cursors) ):
			raise StopIteration()

		cursor = self._get_current_cursor()

		if ( cursor.index == cursor.count ):
			raise StopIteration()

		value  = self.current()
		cursor.next()

		# self._cursor_index +=1
		# if ( self._cursor_index >= len(self._cursors) ):
		# 	self._cursor_index = 0
		
		if ( cursor.index == cursor.count ):
			self._cursor_index +=1

		return value
		
	# def next ( self )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------
	
	def __iter__( self ):
		"""
		Initialize the iterator.

		Arguments:
			self (`pydataset.dataset.Iterator`): Iterator to initialize.

		Returns:
			`pydataset.dataset.Iterator`: Tterator once initialized.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )

		return self

	# def __iter__( self )

	# --------------------------------------------------

	def __len__ ( self ):
		"""
		Returns the total number of examples iterated over by this iterator.

		Arguments:
			self (`pydataset.dataset.Iterator`): Iterator.

		Returns:
			`int`: Number of examples iterated over by this iterator
		"""
		return self.count

	# def __len__ ( self )

	# --------------------------------------------------

	def __next__( self ):
		"""
		Returns the current example and move this iterator on the next example.

		Arguments:
			self (`pydataset.dataset.Iterator`): The iterator to move.

		Returns:
			`pydataset.dataset.Example`: Current example.
		"""
		assert pytools.assertions.type_is_instance_of( self, Iterator )

		return self.next()

	# def __next__( self )

	# ##################################################
	# ###               STATIC METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@staticmethod
	def _split_contents_url ( contents_url ):
		assert pytools.assertions.type_is( contents_url, str )
		
		# Iterators only works on contents of a datasets (examples/groups)
		# Not on the dataset itself, for which a simple call to `Dataset.get_features_values()` would be anough.
		if not contents_url:
			raise RuntimeError( "Invalid contents url: '{}': Nothing to search for.".format(contents_url) )
		
		# Split the contents URL and locate the first asterix
		contents            = contents_url.split( '/' )
		index_first_asterix = contents.index( '*' ) if '*' in contents else -1

		# Check if the asterix is badly placed in the URL
		if ( (index_first_asterix % 2) == 0 ):
			raise RuntimeError( "Invalid contents url: '{}': Badly placed '*'.".format(contents_url) )

		# If all the contents are variable:
		if ( index_first_asterix == -1 ):
			root_url = contents_url
			patterns = []
		
		# If some the contents are variable:
		else:
			root_url = '/'.join( contents[ : index_first_asterix-1 ] )
			patterns = contents[ index_first_asterix-1 : ]
		
		return root_url, patterns

	# def _split_contents_url ( url )

# class Iterator