# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .backend         import Backend
from .backend_error   import BackendError
from .backend_factory import BackendFactory
from .dataset         import Dataset
from .dataset_data    import DatasetData
from .dataset_error   import DatasetError
from .group           import Group
from . import url

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def create ( item_url ):
	"""
	Creates a new dataset item.

	Arguments:
		item_url (`str`): URL of the dataset item to create.

	Returns:
		`pydataset.dataset.Item`: The new dataset item.

	Raises:
		`pydataset.BackendError`: If the backend does not exist.
		`pydataset.DatasetError`: If the item already exist or cannot be created.
	"""
	assert pytools.assertions.type_is( item_url, str )

	# Split the URL
	backend_name, backend_url, dataset_name, contents_url = url.split( item_url )
	
	# Check that the backend is specified
	if not backend_name or not backend_url:
		raise DatasetError.format( "Cannot get backend '{}://{}'", backend_name, backend_url )
	
	# Check that the dataset name is specified
	if not dataset_name:
		raise DatasetError( "Dataset name is None." )

	# Instantiate the backend
	backend = BackendFactory.instantiate_backend( backend_name, url=backend_url )

	# If the dataset exist
	if backend.contains( dataset_name, 'datasets' ):
		
		# If the url does not specify any contents to create, its an error
		if not contents_url:
			raise DatasetError.format( "Cannot create dataset named '{}' because it already exists", dataset_name )
		
		# If we have contents to create, fetch the dataset
		else:
			dataset_data = backend.find( dataset_name, 'datasets' )

	# If the dataset does not exist
	else:		
		# Create the dataset data
		dataset_data = DatasetData(
			          name = dataset_name,
			           url = dataset_name,
			features_names = [],
			  groups_names = [],
			examples_names = []
			)

		# Save the dataset data
		backend.insert( dataset_data, 'datasets' )

	# Create the dataset wrapper
	dataset = Dataset( dataset_data, backend )
	
	# Stop here if we do not hae to create any contents:
	if not contents_url:
		return dataset

	# Ask the dataset to create the contents:
	return dataset.create( contents_url )

# def create ( dataset_name, backend )

# --------------------------------------------------

def exists ( item_url ):
	"""
	Checks if item with the given name exists.

	Arguments:
		item_url (`str`): URL of the item.

	Returns:
		`bool`: `True` if the item exists, `False` otherwise.
	"""
	assert pytools.assertions.type_is( item_url, str )

	# Split the URL
	backend_name, backend_url, dataset_name, contents_url = url.split( item_url )
	
	# Check that the backend is specified
	if not backend_name or not backend_url:
		raise DatasetError.format( "Cannot get backend '{}://{}'", backend_name, backend_url )
	
	# Check that the dataset name is specified
	if not dataset_name:
		raise DatasetError( "Dataset name is None." )

	# Instantiate the backend
	backend = BackendFactory.instantiate_backend( backend_name, url=backend_url )

	# Check if the dataset exists
	if backend.contains( dataset_name, 'datasets' ):

		# If no contents is specified, done
		if not contents_url:
			return True

		# Otherwise get the dataset and search in it
		else:
			dataset_data = backend.find( dataset_name, 'datasets' )
			dataset = Dataset( dataset_data, backend )
			return dataset.contains( contents_url )

	# Done
	else:
		return False

# def exists ( dataset_name, backend )

# --------------------------------------------------

def get ( item_url ):
	"""
	Retreives the item with the given URL.

	Arguments:
		item_url (`str`): URL of the item to retreive.

	Returns:
		`pydataset.dataset.Dataset`: The item if it exists.

	Raises:
		`pydataset.DatasetError`: If the item does not exist.
	"""
	assert pytools.assertions.type_is( item_url, str )
	
	# Split the URL to get the name of the backend, the backend url and the dataset name
	backend_name, backend_url, dataset_name, contents_url = url.split( item_url )
		
	# Check that the backend is specified
	if not backend_name or not backend_url:
		raise DatasetError.format( "Cannot get backend '{}://{}'", backend_name, backend_url )

	# Check that the dataset name is specified
	if not dataset_name:
		raise DatasetError( "Dataset name is None." )

	# Instantiate the backend
	backend = BackendFactory.instantiate_backend( backend_name, url=backend_url )

	# Check if the dataset already exists
	if not backend.contains( dataset_name, 'datasets' ):
		raise DatasetError.format( "Cannot get dataset named '{}' because it does not exist", dataset_name )

	# Load the dataset data
	dataset_data = backend.find( dataset_name, 'datasets' )
	
	# Create and return the dataset wrapper
	dataset = Dataset( dataset_data, backend )

	if contents_url:
		return dataset.get( contents_url )

	return dataset

# def get ( item_url )

# --------------------------------------------------

def get_or_create ( item_url ):
	"""
	Retreives or create the item with the given url.

	Arguments:
		item_url (`str`): URL of the item.

	Returns:
		`pydataset.dataset.Item`: The item.

	Raises:
		`pydataset.BackendError`: If the backend does not exist.
		`pydataset.DatasetError`: If the item cannot be created.
	"""
	assert pytools.assertions.type_is( item_url, str )
	
	if exists( item_url ):
		return get( item_url )
	else:
		return create( item_url )

# def get_or_create ( item_url )

# --------------------------------------------------

def resolve_group ( group, url ):
	"""
	Resolve the group from the url if it is None.	

	Arguments:
		group (`None`/`pydataset.dataset.Group`): Group 
		url                              (`str`): URL
	"""
	assert pytools.assertions.type_is_instance_of( group, (type(None), Group) )
	assert pytools.assertions.type_in( url, (type(None), str) )

	if ( group is None ):
		
		if url is None:
			raise ArgumentError( "Both group and url are None" )
		
		group = get_or_create( url )
		
		if not isinstance( group, Group ):
			raise RuntimeError( "The url '{}' does not point to a group" )

	else:
		url = group.url

	return group, url

# def resolve_group ( group, url )

# --------------------------------------------------

def update ( dataset ):
	"""
	Updates the given dataset.

	Arguments:
		dataset (`pydataset.dataset.Dataset`): Dataset to update.
	"""
	dataset.backend.update( dataset.data, 'datasets' )

# def update ( dataset, backend )