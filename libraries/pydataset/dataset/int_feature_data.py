# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###           CLASS INT-FEATURE-DATA           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'IntFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class IntFeatureData ( FeatureData ):
	"""
	The class `pydataset.dataset.IntFeatureData` stores `int` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value=0 ):
		"""
		Initializes a new instance of the `pydataset.dataset.IntFeatureData` class.

		Arguments:
			self (`pydataset.dataset.IntFeatureData`): Instance to initialize.
			name                              (`str`): Name if the item.
			url                               (`str`): URL if the item.
			parent_url                        (`str`): URL if the parent item.
			value                             (`int`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, int )

		super( IntFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, IntFeatureData )
		assert pytools.assertions.type_is( val, int )

		self._value = val
	
	# def value ( self, val )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this int feature data.

		Arguments:
			self (`pydataset.dataset.features.IntFeatureData`): Int feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this int feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureData )

		return "[pydataset.dataset.IntFeatureData: name='{}', url='{}', parent_url='{}', value={}]".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class IntFeatureData ( FeatureData )