# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .feature_data import FeatureData

# ##################################################
# ###          CLASS TEXT-FEATURE-DATA           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TextFeatureData',
	   namespace = 'pydataset.dataset',
	fields_names = []
	)
class TextFeatureData ( FeatureData ):
	"""
	The class `pydataset.dataset.TextFeatureData` stores `str` values.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='', url='', parent_url='', value='' ):
		"""
		Initializes a new instance of the `pydataset.dataset.TextFeatureData` class.

		Arguments:
			self (`pydataset.dataset.TextFeatureData`): Instance to initialize.
			name                               (`str`): Name if the item.
			url                                (`str`): URL if the item.
			parent_url                         (`str`): URL if the parent item.
			value                              (`str`): The value of the feature.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( url, str )
		assert pytools.assertions.type_is( parent_url, str )
		assert pytools.assertions.type_is( value, str )

		super( TextFeatureData, self ).__init__(
			      name = name,
			       url = url,
			parent_url = parent_url,
			     value = value
			)

	# def __init__ ( self, name, url, parent_url, shape, value )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@FeatureData.value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, TextFeatureData )
		assert pytools.assertions.type_is( val, str )

		self._value = val
	
	# def value ( self, val )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this int feature data.

		Arguments:
			self (`pydataset.dataset.TextFeatureData`): Text feature data of which to create a text representation.

		Returns:
			`str`: Text reprensentation of this int feature data.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureData )

		return "[pydataset.dataset.TextFeatureData: name='{}', url='{}', parent_url='{}', value='{}']".format(
			self.name,
			self.url,
			self.parent_url,
			self.value
			)

	# def __str__ ( self )

# class TextFeatureData ( FeatureData )