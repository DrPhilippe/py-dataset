# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from ..data                     import ShapeData
from ..dataset                  import Group, ImageFeatureData, Item, NDArrayFeatureData
from .camera                    import Camera
from .recorder                  import Recorder
from .dataset_recorder_settings import DatasetRecorderSettings
from .target_type               import TargetType

# ##################################################
# ###           CLASS DATASET-RECORDER           ###
# ##################################################

class DatasetRecorder ( Recorder ):
	"""
	Recorder used to record camera frames into a dataset.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, camera=None, dst_group=None, logger=None, progress_tracker=None ):
		"""
		Initialize a new instance of the file recorder class.

		Arguments:
			self                (`pydataset.cameras.DatasetRecorder`): Instance to initialize.
			settings    (`pydataset.cameras.DatasetRecorderSettings`): Settings of the recorder.
			camera                       (`pydataset.cameras.Camera`): Camera to record.
			dst_group              (`None`/`pydataset.dataset.Group`): Group in which to record frames.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )
		assert pytools.assertions.type_is_instance_of( settings, DatasetRecorderSettings )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( dst_group, (type(None), Group) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( DatasetRecorder, self ).__init__( settings, camera, logger, progress_tracker )
			
		self.dst_group    = dst_group
		self.target_group = dst_group
		self.records      = []

	# def __init__ ( self, settings, camera, logger, progress_tracker )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def dst_group ( self ):
		"""
		Group where to record frames to (`None`/`pydataset.dataset.Group`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )

		return self._dst_group

	# def dst_group ( self )

	# --------------------------------------------------
	
	@dst_group.setter
	def dst_group ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Group) )

		self._dst_group = value
		
	# def dst_group ( self, value )

	# --------------------------------------------------
	
	@property
	def target_group ( self ):
		"""
		Target group where to record frames to (`None`/`pydataset.dataset.Group`).

		This is either equal to `dst_group` of a sub group of it.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )

		return self._target_group

	# def target_group ( self )

	# --------------------------------------------------
	
	@target_group.setter
	def target_group ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Group) )

		self._target_group = value
		
	# def target_group ( self, value )

	# --------------------------------------------------

	@property
	def records ( self ):
		"""
		Items recorded by this recorder (`list` of `pydataset.dataset.Item`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )

		return self._records

	# def records ( self )
	
	# --------------------------------------------------

	@records.setter
	def records ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, Item )

		self._records = value
		
	# def records ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def record ( self, index, frames ):
		"""
		Record the given set of frames.

		Arguments:
			self  (`pydataset.cameras.DatasetRecorder`): Recorder.
			index                               (`int`): Index of the set of frames.
			frames (`dict` of `str` to `numpy.ndarray`): Set of frames.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )
		assert pytools.assertions.type_is_instance_of( frames, dict )
		assert pytools.assertions.dictionary_types_are( frames, str, numpy.ndarray )
		
		# Fetch target type / group
		target_type  = self.settings.target_type
		target_group = self.target_group
		
		# Create the receiving dataset item
		target = None

		# It should be a new example ?
		if target_type == TargetType.examples:
			target = target_group.create_example()
		
		# It shoul be a group ?
		elif target_type == TargetType.groups:
			
			# Get a unique name for the new group
			i = index
			group_name = 'frame_{}'.format( i )
			if not self.settings.override:
				while target_group.contains_group( group_name ):
					i += 1
					group_name = 'frame_{}'.format( i )

			# Create the new group
			target = target_group.get_or_create_group( group_name )

		# it features of the target group ?
		elif target_type == TargetType.features:
			target = target_group

		# Save each frame as an independant feature
		for key in frames.keys():

			# Get frame data
			frame = frames[ key ]
			dtype = frame.dtype
			shape = frame.shape
			rank  = len(shape)
			depth = shape[2] if rank > 2 else 1

			# Create unique feature name
			i = 1
			feature_name = '{}{}'.format( self.settings.feature_name_prefix, key )
			while not self.settings.override and target.contains_feature( feature_name ):
				i += 1
				feature_name = '{}{}_{}'.format( self.settings.feature_name_prefix, key, i )

			# The array is an image
			if ( dtype == numpy.uint8 and rank in [2, 3] and depth < 5 ):
				target.create_or_update_feature(
					feature_data_type = ImageFeatureData,
					     feature_name = feature_name,
					            shape = ShapeData( *shape ),
					            dtype = 'uint8',
					            value = frame,
					        extension = '.jpeg',
					            param = 90
					)
			
			# The array is something else
			else:
				target.create_or_update_feature(
					feature_data_type = NDArrayFeatureData,
					     feature_name = feature_name,
					            shape = ShapeData( *shape ),
					            dtype = dtype.name,
					            value = frame
					)
			
			# if ( dtype == numpy.uint8 and rank in [2, 3] and depth < 5 )

		# for key in frames.keys()
			
		self.records.append( target )
			
	# def record ( self, index, frames )
		
	# --------------------------------------------------

	def resolve_group ( self ):
		"""
		If a groups is not already specified, resolves the group where to import data to using the provided settings.
		
		Arguments:
			self (`pydataset.cameras.DatasetRecorder`): The recorder of which to resolve the destination group.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorder )

		if ( self.dst_group is None ):

			if not self.settings.dst_group_url:
				raise ValueError( 'Destination group URL is empty. Please provided a group directly or its url' )
			
			dst_group = pydataset.dataset.get_or_create( self.settings.dst_group_url )
			
			if not isinstance( dst_group, pydataset.dataset.Group ):
				raise RuntimeError( "The url '{}' does not point to a group" )

			self.dst_group = dst_group

		else:
			self.settings.dst_group_url = self.dst_group.url

	# def resolve_group ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this recorder.

		Arguments:
			self (`pydataset.cameras.DatasetRecorder`): The recorder to start.
		"""		
		super( DatasetRecorder, self ).start()

		# log
		if self.logger:
			self.logger.log( 'Dataset Recorder Settings:' )
			self.logger.log( '    dst_group_url:      {}', self.settings.dst_group_url      )
			self.logger.log( '    records_group_name: {}', self.settings.records_group_name )
			self.logger.log( '    target_type:        {}', self.settings.target_type        )
			self.logger.end_line()
			self.logger.flush()

		# Resolve the destination group
		self.resolve_group()

		# Place examples in a sub group ?
		if self.settings.records_group_name:
			self.target_group = self.dst_group.get_or_create_group( self.settings.records_group_name )
		else:
			self.target_group = self.dst_group

		# log
		if self.logger:
			self.logger.log( 'Resolved destination groups:')
			self.logger.log( '    dst_group:    {}', self.dst_group.url )
			self.logger.log( '    target_group: {}', self.target_group.url )
			self.logger.end_line()
			self.logger.flush()

	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, camera, dst_group=None, logger=None, progress_tracker=None, **kwargs ):
		"""
		Creates a new instance of the task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of recorder.
			camera                (`None`/`pydataset.cameras.Camera`): Camera to record.
			dst_group              (`None`/`pydataset.dataset.Group`): Group in which to record frames.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			**kwargs                                         (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, camera, dst_group, logger, progress_tracker )

	# def create ( cls, camera, logger, progress_tracker, **kwargs )
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a dataset recorder.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pydataset.cameras.DatasetRecorderSettings`: The settings.
		"""
		return DatasetRecorderSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class DatasetRecorder ( Recorder )