# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###               CLASS RECORDER               ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'RecorderSettings',
	   namespace = 'pycameras',
	fields_names = [
		'maximum_number_of_records'
		]
	)
class RecorderSettings ( pytools.tasks.TaskSettings ):
	"""
	Base class for recorder settings classes.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, maximum_number_of_records=-1, name='Recorder' ):
		"""
		Initializes a new instance of the recorder settings class.

		Arguments:
			self (`pydataset.cameras.RecorderSettings`): Instance to initialize.
			maximum_number_of_records           (`int`): Maximum number of records taken by the recorder.
			name                                (`str`): Name of the recorder.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecorderSettings )
		assert pytools.assertions.type_is( maximum_number_of_records, int )
		assert pytools.assertions.type_is( name, str )

		super( RecorderSettings, self ).__init__( name )

		self.maximum_number_of_records = maximum_number_of_records

	# def __init__ ( self, maximum_number_of_records, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def maximum_number_of_records ( self ):
		"""
		Maximum number of records taken by this recorder (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, RecorderSettings )

		return self._maximum_number_of_records
	
	# def maximum_number_of_records ( self )

	# --------------------------------------------------

	@maximum_number_of_records.setter
	def maximum_number_of_records ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, RecorderSettings )
		assert pytools.assertions.type_is( value, int )
		
		self._maximum_number_of_records = value
	
	# def maximum_number_of_records ( self, value )
	
# class RecorderSettings ( pytools.tasks.TaskSettings )