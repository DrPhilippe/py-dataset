# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .recorder_settings import RecorderSettings
from .target_type       import TargetType

# ##################################################
# ###      CLASS DATASET-RECORDER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DatasetRecorderSettings',
	   namespace = 'pydataset.cameras',
	fields_names = [
		'dst_group_url',
		'records_group_name',
		'target_type',
		'override',
		'feature_name_prefix'
		]
	)
class DatasetRecorderSettings ( RecorderSettings ):
	"""
	Settings for the dataset recorder.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self,
		            dst_group_url = '',
		maximum_number_of_records = -1,
		       records_group_name = '',
		              target_type = TargetType.examples,
		                 override = False,
		      feature_name_prefix = '',
		                     name = 'Dataset Recorder'
		):
		"""
		Initializes a new instance of the dataset recorder settings class.

		Arguments:
			self (`pydataset.cameras.DatasetRecorderSettings`): Instance to initialize.
			dst_group_url                              (`str`): URL of the group where to save record to.
			maximum_number_of_records                  (`int`): Maximum number of records taken by the recorder.
			records_group_name                         (`str`): If specified recordes are place in a sub-group.
			target_type                         (`TargetType`): Type of dataset item to create to save each frame.
			name                                       (`str`): Name of the recorder.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( dst_group_url, str )
		assert pytools.assertions.type_is( maximum_number_of_records, int )
		assert pytools.assertions.type_is( override, bool )
		assert pytools.assertions.type_is( feature_name_prefix, str )
		assert pytools.assertions.type_is( name, str )

		super( DatasetRecorderSettings, self ).__init__( maximum_number_of_records, name )

		self._dst_group_url       = dst_group_url
		self._records_group_name  = records_group_name
		self._target_type         = target_type
		self._override            = override
		self._feature_name_prefix = feature_name_prefix

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def dst_group_url ( self ):
		"""
		URL of the group where to import example to (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )

		return self._dst_group_url
	
	# def dst_group_url ( self )
	
	# --------------------------------------------------

	@dst_group_url.setter
	def dst_group_url ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( value, str )

		self._dst_group_url = value
	
	# def dst_group_url ( self, value )

	# --------------------------------------------------

	@property
	def records_group_name ( self ):
		"""
		Name of the group where to save the records (`str`).

		If a group name is provided, records are place in a
		sub-group of the destination group with the given name.
		If the name is empty records are directly added to
		the destination group.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		
		return self._records_group_name

	# def records_group_name ( self )
	
	# --------------------------------------------------

	@records_group_name.setter
	def records_group_name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( value, str )

		self._records_group_name = value

	# def records_group_name ( self, value )

	# --------------------------------------------------

	@property
	def target_type ( self ):
		"""
		Type of dataset item to create to save each frame (`TargetType`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )

		return self._target_type
	
	# def target_type ( self )

	# --------------------------------------------------

	@target_type.setter
	def target_type ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( value, TargetType )

		self._target_type = value

	# def target_type ( self, value )

	# --------------------------------------------------

	@property
	def override ( self ):
		"""
		If `True` overrides existing features (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )

		return self._override
	
	# def override ( self )

	# --------------------------------------------------

	@override.setter
	def override ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( value, bool )

		self._override = value

	# def override ( self, value )

	# --------------------------------------------------

	@property
	def feature_name_prefix ( self ):
		"""
		If `True` overrides existing features (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )

		return self._feature_name_prefix
	
	# def feature_name_prefix ( self )

	# --------------------------------------------------

	@feature_name_prefix.setter
	def feature_name_prefix ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, DatasetRecorderSettings )
		assert pytools.assertions.type_is( value, str )

		self._feature_name_prefix = value

	# def feature_name_prefix ( self, value )

# class DatasetRecorderSettings ( RecorderSettings )