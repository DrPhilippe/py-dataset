# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from .camera            import Camera
from .recorder_settings import RecorderSettings

# ##################################################
# ###               CLASS RECORDER               ###
# ##################################################

class Recorder ( pytools.tasks.Task ):
	"""
	Recorder are used to record camrea frames.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, camera=None, logger=None, progress_tracker=None ):
		"""
		Initialize a new instance of the recorder class.

		Arguments:
			self                       (`pydataset.cameras.Recorder`): Instance to initialize.
			settings           (`pydataset.cameras.RecorderSettings`): Settings of the recorder.
			camera                       (`pydataset.cameras.Camera`): Camera to record.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is_instance_of( settings, RecorderSettings )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( Recorder, self ).__init__( settings, logger, progress_tracker )

		self.camera                   = camera
		self.current_frames_index     = -1
		self.current_frames           = {}
		self.current_frames_timestamp = 0.0

	# def __init__ ( self, settings, camera, logger, progress_tracker )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def camera ( self ):
		"""
		The camera this recorder record frames from (`pydataset.cameras.Camera`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		return self._camera
	
	# def camera ( self )

	# --------------------------------------------------

	@camera.setter
	def camera ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Camera) )

		self._camera = value

	# def camera ( self, value )
	
	# --------------------------------------------------

	@property
	def current_frames ( self ):
		"""
		Last frames recoreded by this recorder (`dict` of `str` to `numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		return self._current_frames

	# def current_frames ( self )
	
	# --------------------------------------------------

	@current_frames.setter
	def current_frames ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is( value, dict )
		assert pytools.assertions.dictionary_types_are( value, str, numpy.ndarray )

		self._current_frames = value
		
	# def current_frames ( self, value )

	# --------------------------------------------------

	@property
	def current_frames_timestamp ( self ):
		"""
		Index of the last frames in the camera (`float`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		return self._current_frames_timestamp

	# def current_frames_timestamp ( self )
	
	# --------------------------------------------------

	@current_frames_timestamp.setter
	def current_frames_timestamp ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is( value, float )

		self._current_frames_timestamp = value
		
	# def current_frames_timestamp ( self, value )

	# --------------------------------------------------

	@property
	def current_frames_index ( self ):
		"""
		Index of the last frames recoreded by this recorder (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		return self._current_frames_index

	# def current_frames_index ( self )
	
	# --------------------------------------------------

	@current_frames_index.setter
	def current_frames_index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is( value, int )

		self._current_frames_index = value
		
	# def current_frames_index ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def record ( self, index, frames ):
		"""
		Record the given set of frames.

		Arguments:
			self         (`pydataset.cameras.Recorder`): Recorder.
			index                               (`int`): Index of the set of frames.
			frames (`dict` of `str` to `numpy.ndarray`): Set of frames.
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )
		assert pytools.assertions.type_is_instance_of( frames, dict )
		assert pytools.assertions.dictionary_types_are( frames, str, numpy.ndarray )
		
		raise NotImplementError()

	# def record ( self, index, frames )

	# --------------------------------------------------

	def run ( self ):
		"""
		Run recording.

		Arguments:
			self (`pydataset.cameras.Recorder`): The recorder run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		# Log
		if self.logger:
			self.logger.log( 'Recorder Running:' )
			self.logger.flush()

		if self.settings.maximum_number_of_records == 0:
			self.stop()
			if self.logger:
				self.logger.log( '    Nothing to record.' )
				self.logger.flush()
			return

		while self.is_running():

			# Update the task state
			self.update()

			# Check if we recorder enough frames
			if ( self.settings.maximum_number_of_records > 0 and self.current_frames_index >= self.settings.maximum_number_of_records ):
				self.stop()
				break

			# Get the frames from the camera
			timestamp, frames = self.camera.get_frames()

			# Check they are new
			if ( timestamp > self.current_frames_timestamp ):
				
				# Record them
				self.record( self.current_frames_index, frames )

				# Save them in this recorder
				self.current_frames_timestamp = timestamp
				self.current_frames           = frames
				self.current_frames_index    += 1

				# Log
				if self.logger:
					self.logger.log(
						'    recorded frame(s) {} ({}/{})',
						self.current_frames_timestamp,
						self.current_frames_index,
						self.settings.maximum_number_of_records
						)
					self.logger.flush()

				# Report progress
				if ( self.settings.maximum_number_of_records > 0 and self.progress_tracker ):
					self.progress_tracker.update( float(self.current_frames_index)/float(self.settings.maximum_number_of_records) )

			# if ( timestamp > self.current_frames_timestamp )

		# while self.is_running()

		# Log
		if self.logger:
			self.logger.log( '    Done' )
			self.logger.end_line()
			self.logger.flush()

		# Report progress
		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def run ( self )

	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this recorder.

		Arguments:
			self (`pydataset.cameras.Recorder`): The recorder to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Recorder )

		super( Recorder, self ).start()

		# Log
		if self.logger:
			self.logger.log( 'Recorder Starting' )
			self.logger.log( '    name: {}', self.settings.name )
			self.logger.log( '    maximum_number_of_records: {}', self.settings.maximum_number_of_records )
			self.logger.end_line()
			self.logger.flush()

		self.current_frames_timestamp = 0.0
		self.current_frames_index     = 0
		self.current_frames           = {}
				
	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create ( cls, camera, logger=None, progress_tracker=None, **kwargs ):
		"""
		Creates a new instance of the task of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of recorder.
			camera                (`None`/`pydataset.cameras.Camera`): Camera to record.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
			**kwargs                                         (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pydataset.cameras.Recorder`: The new recorder
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, camera, logger, progress_tracker )

	# def create ( cls, camera, logger, progress_tracker, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a recorder.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pydataset.cameras.RecorderSettings`: The new settings.
		"""
		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )

# class Recorder ( pytools.tasks.Task )