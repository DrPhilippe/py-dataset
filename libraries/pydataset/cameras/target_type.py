# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###             CLASS TARGET-TYPE              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'TargetType',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class TargetType ( pytools.serialization.Enum ):
	"""
	The targer type enum is used to define what kind of dataset items
	are create for each frame when recording a camera using a dataset recorder.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------
	
	"""
	The dataset recorder records images as feature of new examples.
	"""
	examples = None	

	"""
	The dataset recorder records images as features of an existing example or group.
	"""
	features = None

	"""
	The dataset recorder records images as feature of new groups.
	"""
	groups   = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='examples' ):
		"""
		Creates a new instance of the target type enum with the given textual value.

		Arguments:
			self (`pydataset.cameras.TargetType`): Instance to initialize.
			value                         (`str`): Textual value of the target type.
		"""
		assert pytools.assertions.type_is_instance_of( self, TargetType )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( TargetType, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible textual values for the target type enum.

		Arguments:
			cls (`type`): The target type class.

		Returns:
			`list` of `str`: The list of possible textual values of the target type enum.
		"""
		return [
			'examples',
			'features',
			'groups'
			]
		
	# def get_possible_values ( cls )

# class TargetType ( pytools.serialization.Enum )

TargetType.examples = TargetType( 'examples' )
TargetType.features = TargetType( 'features' )
TargetType.groups   = TargetType( 'groups'   )