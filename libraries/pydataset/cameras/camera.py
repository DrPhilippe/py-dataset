# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import numpy
import threading

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from .camera_settings import CameraSettings

# ##################################################
# ###                CLASS CAMERA                ###
# ##################################################

class Camera ( pytools.tasks.Task ):
	"""
	Base camera class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, logger=None ):
		"""
		Initializes a new instance of the camera class.

		Arguments:
			self             (`pydataset.cameras.Camera`): Camera to initialize.
			settings (`pydataset.cameras.CameraSettings`): Settings of the camera.
			logger               (`pytools.tasks.Logger`): Logger used to log the activity of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )
		assert pytools.assertions.type_is_instance_of( settings, CameraSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		super( Camera, self ).__init__( settings, logger, None ) # cameras do not track progression
		
		self._lock      = threading.Lock()
		self._frames    = {}
		self._counter   = 0
		self._timestamp = 0
		
	# def __init__ ( self, settings, logger )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def counter ( self ):
		"""
		Current frames counter (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )

		self._lock.acquire()
		counter = self._counter
		self._lock.release()
		return counter

	# def counter ( self )

	# --------------------------------------------------

	@property
	def frames ( self ):
		"""
		Current frames of the camera (`dict` of` str` to `numpy.ndarray`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )

		self._lock.acquire()
		frames = copy.deepcopy( self._frames )
		self._lock.release()
		return frames
	
	# def frames ( self )
	
	# --------------------------------------------------

	@property
	def timestamp ( self ):
		"""
		Current frames timestamp (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )

		self._lock.acquire()
		timestamp = self._timestamp
		self._lock.release()
		return timestamp

	# def timestamp ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_description ( self ):
		"""
		Returns a textual description of this camera.

		Arguments:
			self (`pydataset.cameras.Camera`): Camera to describe.

		Returns:
			`str`: Description of this camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )

		raise NotImplementedError()
		
	# def get_description ( self )

	# --------------------------------------------------

	def get_frames ( self ):
		"""
		Returns the current timestamp and set of frames of this camera
		
		Arguments:
			self (`pydataset.cameras.Camera`): The camera of which to get the current timestamp and frames.

		Returns:
			timestamp                           (`int`): Index of the current frames
			frames (`dict` of `str` to `numpy.ndarray`): Index of the current frames
		"""
		self._lock.acquire()
		timestamp = self._timestamp
		frames    = self._frames
		self._lock.release()
		return timestamp, frames

	# def get_frames ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this camera.

		Arguments:
			self (`pydataset.cameras.Camera`): The camera to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )

		raise NotImplementedError()
		
	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this camera.

		Arguments:
			self (`pydataset.cameras.Camera`): The camera to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, Camera )
		
		super( Camera, self ).start()

		self._counter   = 0
		self._timestamp = 0
		self._frames    = {}
		
	# def start ( self )

	# --------------------------------------------------

	def _set_frames ( self, frames ):
		"""
		Sets the current timestamp and set of frames of this camera
		
		Arguments:
			self           (`pydataset.cameras.Camera`): The camera of which to set the current timestamp and frames.
			frames (`dict` of `str` to `numpy.ndarray`): Index of the current frames
		"""
		self._lock.acquire()
		self._timestamp = self.delta_time
		self._frames    = frames
		self._counter  += 1
		self._lock.release()

	# def _set_frames ( self, timestamp, frames )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create ( cls, logger=None, **kwargs ):
		"""
		Creates a new instance of the camera of type `cls`.
		
		Arguments:
			cls                           (`type`): The type of camera.
			logger (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the camera.
			**kwargs                      (`dict`): Arguments forwarded to the camera settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		# Create the camera settings
		settings = cls.create_settings( **kwargs )

		# Create the camera
		return cls( settings, logger )

	# def create ( cls, logger, progress_tracker, **kwargs )

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a camera.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the camera settings constructor.

		Returns:
			`pydataset.cameras.CameraSettings`: The camera settings.
		"""
		raise NotImplementedError()

	# def create_settings ( cls, **kwargs )
	
# class Camera ( pytools.tasks.Task )