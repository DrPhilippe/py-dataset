# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###            CLASS CAMERA-FACTORY            ###
# ##################################################

class CameraFactory ( pytools.factory.ClassFactory ):
	"""
	Factory dedicated to cameras.
	"""

	# ##################################################
	# ###               CLASS-FIELDS                 ###
	# ##################################################

	# --------------------------------------------------
	
	"""
	Name of the group in the class factory for cameras
	"""
	group_name = 'cameras'

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_cameras_names ( cls ):
		"""
		Returns the names of all registered cameras in the class factory.

		Arguments:
			cls (`type`): The camera factory class.

		Returns:
			`list` of `str`: The names of the caleras.
		"""
		assert pytools.assertions.type_is( cls, type )

		if ( cls.group_name in cls.get_group_names() ):
			return cls.get_typenames_of_group( cls.group_name )
		
		else:
			return []

	# def get_cameras_names ( cls )

	# --------------------------------------------------

	@classmethod
	def has_camera ( cls, name ):
		"""
		Checks if a camera with the given name is registered in the class factory.

		Arguments:
			cls (`type`): The camera factory class.
			name (`str`): Name of the camera.

		Returns:
			`bool`: `True` if the camera is registered, `False` otherwise.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( name, str )

		if ( cls.group_name in cls.get_group_names() ):
			return cls.typename_is_used( cls.group_name, name )

		return False

	# def has_camera ( cls, name )

	# --------------------------------------------------

	@classmethod
	def instantiate_camera ( cls, name, **kwargs ):
		"""
		Instantiate the camera with the given name.

		Arguments:
			cls      (`type`): The camera factory class.
			name      (`str`): Name of the camera.
			**kwargs (`dict`): Named arguments formwarded to the camera constructor.

		Returns:
			`pydataset.cameras.Camera`: The new camera instance.
		"""
		return cls.instantiate( cls.group_name, name, **kwargs )

	# def instantiate_camera ( cls, name, **kwargs )

# class CameraFactory ( pytools.factory.ClassFactory )