# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .stream_type       import StreamType
from .stream_resolution import StreamResolution
from .stream_format     import StreamFormat

# ##################################################
# ###           CLASS STREAM-SETTINGS            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'StreamSettings',
	   namespace = 'pydataset.cameras',
	fields_names = [
		'name',
		'enabled',
		'stream_type',
		'index',
		'framerate',
		'resolution',
		'stream_format'
		]
	)
class StreamSettings ( pytools.serialization.Serializable ):
	"""
	Settings for individual stream of an intel camera.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, enabled=True, stream_type=StreamType.color, index=-1, framerate=None, resolution=None, stream_format=None, name='' ):
		"""
		Initialize a nez instance of the `pycameras.intel.StreamSettings` class.

		Arguments:
			self                (`pydataset.cameras.StreamSettings`): Instance to intialize.
			enabled                                         (`bool`): `True` if the stream being recorded, `False` if it isn't.
			stream_type             (`pydataset.cameras.StreamType`): Type of stream this settings apply to.
			index                                            (`int`): Index of the stream used to identify multiple instance of the same stream-type (example: left-right infrared).
			framerate                                 (`None`/`int`): Number of frame per seconds to record.
			resolution (`None`/`pydataset.cameras.StreamResolution`): Resolution of the stream.
			stream_format  (`None`/`pydataset.cameras.StreamFormat`): Data format of the stream.
			name                                             (`str`): Name of the stream.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_is( enabled, bool )
		assert pytools.assertions.type_is( stream_type, StreamType )
		assert pytools.assertions.type_is( index, int )
		assert pytools.assertions.type_in( framerate, (type(None), int) )
		assert pytools.assertions.type_in( resolution, (type(None), StreamResolution) )
		assert pytools.assertions.type_in( stream_format, (type(None), StreamFormat) )
		assert pytools.assertions.type_is( name, str )
		
		super( StreamSettings, self ).__init__()
		
		self.enabled       = enabled
		self.stream_type   = stream_type
		self.index         = index
		self.framerate     = framerate
		self.resolution    = resolution
		self.stream_format = stream_format
		self.name          = name if name else str(stream_type)

	# def __init__ ( self, ... )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def enabled ( self ):
		"""
		`True` if the stream being recorded, `False` if it isn't (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._enabled

	# def enabled ( self )
	
	# --------------------------------------------------

	@enabled.setter
	def enabled ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_is( value, bool )

		self._enabled = value
		
	# def enabled ( self, value )

	# --------------------------------------------------

	@property
	def name ( self ):
		"""
		Name of the stream (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._name

	# def name ( self )
	
	# --------------------------------------------------

	@name.setter
	def name ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
		
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def stream_type ( self ):
		"""
		Type of stream this settings apply to (`pydataset.cameras.StreamType`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._stream_type

	# def stream_type ( self )
	
	# --------------------------------------------------

	@stream_type.setter
	def stream_type ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_is( value, StreamType )

		self._stream_type = value
		
	# def stream_type ( self, value )

	# --------------------------------------------------

	@property
	def index ( self ):
		"""
		Index of the stream used to identify multiple instance of the same stream-type (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._index

	# def index ( self )
	
	# --------------------------------------------------

	@index.setter
	def index ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_is( value, int )

		self._index = value
		
	# def index ( self, value )

	# --------------------------------------------------

	@property
	def framerate ( self ):
		"""
		Number of frame per seconds to record (`None`/`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._framerate

	# def framerate ( self )
	
	# --------------------------------------------------

	@framerate.setter
	def framerate ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_in( value, (type(None), int) )

		self._framerate = value
		
	# def framerate ( self, value )

	# --------------------------------------------------

	@property
	def resolution ( self ):
		"""
		Resolution of the stream (`None`/`pydataset.cameras.StreamResolution`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._resolution

	# def resolution ( self )
	
	# --------------------------------------------------

	@resolution.setter
	def resolution ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_in( value, (type(None), StreamResolution) )

		self._resolution = value
		
	# def resolution ( self, value )

	# --------------------------------------------------

	@property
	def stream_format ( self ):
		"""
		Data format of the stream (`None`/`pydataset.cameras.StreamFormat`).
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return self._stream_format

	# def stream_format ( self )
	
	# --------------------------------------------------

	@stream_format.setter
	def stream_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )
		assert pytools.assertions.type_in( value, (type(None), StreamFormat) )

		self._stream_format = value
		
	# def stream_format ( self, value )

	# ##################################################
	# ###                 OPERATORS                  ###
	# ##################################################

	# --------------------------------------------------

	def __str__ ( self ):
		"""
		Creates a text representation of this stream settings.

		Arguments:
			self (`pydataset.cameras.StreamSettings`): Stream settings to represent as text.

		Returns:
			`str`: Text representation.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return repr(self)
	
	# def __str__ ( self )

	# --------------------------------------------------

	def __repr__ ( self ):
		"""
		Creates a text representation of this stream settings.

		Arguments:
			self (`pydataset.cameras.StreamSettings`): Stream settings to represent as text.

		Returns:
			`str`: Text representation.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamSettings )

		return '[StreamSettings: enabled={}, stream_type={}, index={}, framerate={}, resolution={}, stream_format={}]'.format(
			self.enabled,
			self.stream_type,
			self.index,
			self.framerate,
			self.resolution,
			self.stream_format
			)

	# def __repr__ ( self )

# class StreamSettings ( pytools.serialization.Serializable )