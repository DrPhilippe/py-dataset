# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###           CLASS CAMERA-SETTINGS            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'CameraSettings',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class CameraSettings ( pytools.tasks.TaskSettings ):
	"""
	Base class for custom camera settings.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='camera' ):
		"""
		Initializes a new instance of the camera settings class.

		Arguments:
			self (`pydataset.cameras.CameraSettings`): Instance to initialize.
			name                              (`str`): Name of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraSettings )
		assert pytools.assertions.type_is( name, str )
		
		super( CameraSettings, self ).__init__( name )

	# def __init__ ( self )

# class CameraSettings ( pytools.tasks.TaskSettings )