# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import io
import os
import numpy
import time

# INTERNALS
import pytools.assertions
import pytools.tasks
import pytools.text

# LOCALS
from .camera                    import Camera
from .register_camera_attribute import RegisterCameraAttribute
from .intel_camera_settings     import IntelCameraSettings
from .stream_type               import StreamType

# ##################################################
# ###             CLASS INTEL-CAMERA             ###
# ##################################################

@RegisterCameraAttribute( 'Intel Camera' )
class IntelCamera ( Camera ):
	"""
	Intel RealSens D435i of compatible intel camera.
	
	https://github.com/IntelRealSense/librealsense/wiki
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings=IntelCameraSettings(), logger=None ):
		"""
		Initialize a new instance of the `pycameras.intel.IntelCamera` class.

		Arguments:
			self             (`pydataset.cameras.IntelCamera`): Instance to initialize.
			settings (`pydataset.cameras.IntelCameraSettings`): Settings of the camera.
			logger             (`None`/`pytools.tasks.Logger`): Logger used by the camera to log errors.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )
		assert pytools.assertions.type_is_instance_of( settings, IntelCameraSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		super( IntelCamera, self ).__init__( settings, logger )

		self._device   = None
		self._config   = None
		self._pipeline = None

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def frame_aligner ( self ):
		"""
		Frame aligner that realigns depth and color frame (`pyrealsense2.align`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )
		
		return self._frame_aligner

	# def frame_aligner ( self )
	
	# --------------------------------------------------
	
	@frame_aligner.setter
	def frame_aligner ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )
		
		self._frame_aligner = value

	# def frame_aligner ( self, value )

	# --------------------------------------------------

	@property
	def device ( self ):
		"""
		Intel camera device (`pyrealsense2.device`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		return self._device

	# def device ( self )

	# --------------------------------------------------

	@device.setter
	def device ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		self._device = value

	# def device ( self, value )

	# --------------------------------------------------

	@property
	def config ( self ):
		"""
		Configuration of the device (`pyrealsense2.config`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		return self._config

	# def config ( self )

	# --------------------------------------------------

	@config.setter
	def config ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		self._config = value

	# def config ( self, value )

	# --------------------------------------------------

	@property
	def pipeline ( self ):
		"""
		Video pipeline used to access the frame of the device (`pyrealsense2.pipeline`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		return self._pipeline

	# def pipeline ( self )

	# --------------------------------------------------

	@pipeline.setter
	def pipeline ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		self._pipeline = value

	# def pipeline ( self, value )

	# ##################################################
	# ###                   METHOD                   ###
	# ##################################################

	# --------------------------------------------------

	def find_device ( self ):
		"""
		Finds the device plug to the computer.

		Arguments:
			self (`pydataset.cameras.IntelCamera`): Camera that search the device.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		import pyrealsense2
		context       = pyrealsense2.context()
		devices_list  = context.devices
		devices_count = devices_list.size()
		device_names  = []
		self.device   = None

		if devices_count == 0:
			raise RuntimeError( 'Intel RealSense D435I not connected (0 device found).' )

		for device_index in range(devices_count):			
			device      = devices_list[ device_index ]
			device_name = device.get_info( pyrealsense2.camera_info.name )
			
			device_names.append( device_name )

			if ( device_name == self.settings.name ):				
				self.device = device
				return

			# if ( device_name == 'Intel RealSense D435I' )
		
		# for device_index in range(devices_count)

		raise RuntimeError( 'Device "{}" not found in {} available devices: {}.'.format(self.settings.name, devices_count, device_names) )

	# def find_device ( self )
	
	# --------------------------------------------------

	def get_config ( self ):
		"""
		Create intel real sense pipeline configuration from this camera settings.

		Arguments:
			self (`pydataset.camras.IntelCamera`): Camera of which to create the pipeline config.

		Returns:
			`pyrealsense2.config`: The pipeline configuration.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )
		
		if ( self.logger ):
			self.logger.log( 'Creating configuration:' )
			self.logger.flush()

		import pyrealsense2
		config = pyrealsense2.config()
		
		for stream_settings in self.settings.streams:

			if ( self.logger ):
				self.logger.log(
					'    {}: enabled={}, index={}, framerate={}, resolution={}, stream_format={}',
					stream_settings.stream_type,
					stream_settings.enabled,
					stream_settings.index,
					stream_settings.framerate,
					stream_settings.resolution,
					stream_settings.stream_format
					)
				self.logger.flush()

			if stream_settings.enabled:

				stream_type   = getattr( pyrealsense2.stream, stream_settings.stream_type.value )
				stream_index  = stream_settings.index
				framerate     = stream_settings.framerate
				resolution    = stream_settings.resolution
				stream_format = getattr( pyrealsense2.format, stream_settings.stream_format.value )

				if resolution is not None:
					width, height = resolution.value.split(' x ')
					width  = int(width)
					height = int(height)
				else:
					width  = None
					height = None

				if ( stream_index == -1 and stream_format is None and width is None and height is None and framerate is None ):
					config.enable_stream( stream_type )

				elif ( stream_format is None and width is None and height is None and framerate is None ):
					config.enable_stream( stream_type, stream_index )

				elif ( stream_index == -1 and width is None and height is None and framerate is None ):
					config.enable_stream( stream_type, stream_format )

				elif ( stream_index == -1 and width is None and height is None ):
					config.enable_stream( stream_type, stream_format, framerate )

				elif ( stream_index == -1 and stream_format is None and framerate is None ):
					config.enable_stream( stream_type, width, height )

				elif ( stream_index == -1 and framerate is None ):
					config.enable_stream( stream_type, width, height, stream_format )
				
				elif ( stream_index == -1 ):
					config.enable_stream( stream_type, width, height, stream_format, framerate )

				elif ( width is None and height is None and framerate is None ):
					config.enable_stream( stream_type, stream_index, stream_format )
				
				elif ( width is None and height is None ):
					config.enable_stream( stream_type, stream_index, stream_format, framerate )

				else:
					config.enable_stream( stream_type, stream_index, width, height, stream_format, framerate )

			# if stream_settings.enabled

		# for stream_settings in self.settings.streams
		
		if ( self.logger ):
			self.logger.log( '    Done.' )
			self.logger.end_line()
			self.logger.flush()

		return config

	# def get_config ( self )

	# --------------------------------------------------

	def get_description ( self ):
		"""
		Returns a textual description of this intel camera.

		Arguments:
			self (`pydataset.cameras.IntelCamera`): Intel camera to describe.

		Returns:
			`str`: Description of this intel camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		import pyrealsense2

		advanced_mode                = self.device.get_info( pyrealsense2.camera_info.advanced_mode )
		asic_serial_number           = self.device.get_info( pyrealsense2.camera_info.asic_serial_number )
		camera_locked                = self.device.get_info( pyrealsense2.camera_info.camera_locked )
		debug_op_code                = self.device.get_info( pyrealsense2.camera_info.debug_op_code )
		firmware_update_id           = self.device.get_info( pyrealsense2.camera_info.firmware_update_id )
		firmware_version             = self.device.get_info( pyrealsense2.camera_info.firmware_version )
		name                         = self.device.get_info( pyrealsense2.camera_info.name )
		physical_port                = self.device.get_info( pyrealsense2.camera_info.physical_port )
		product_id                   = self.device.get_info( pyrealsense2.camera_info.product_id )
		product_line                 = self.device.get_info( pyrealsense2.camera_info.product_line )
		recommended_firmware_version = self.device.get_info( pyrealsense2.camera_info.recommended_firmware_version )
		serial_number                = self.device.get_info( pyrealsense2.camera_info.serial_number )
		usb_type_descriptor          = self.device.get_info( pyrealsense2.camera_info.usb_type_descriptor )

		writer = io.StringIO( newline=os.linesep )

		writer.write( 'Name: ' )
		writer.write( name )
		writer.write( '\n' )
		
		writer.write( 'Product Line: ' )
		writer.write( product_line )
		writer.write( '\n' )

		writer.write( 'Product ID: ' )
		writer.write( product_id )
		writer.write( '\n' )

		writer.write( 'Serial Number: ' )
		writer.write( serial_number )
		writer.write( '\n' )

		writer.write( 'ASIC Serial Number: ' )
		writer.write( asic_serial_number )
		writer.write( '\n' )

		writer.write( 'Camera Locked: ' )
		writer.write( camera_locked )
		writer.write( '\n' )

		writer.write( 'Debug OP Code: ' )
		writer.write( debug_op_code )

		writer.write( 'Firmware Update ID: ' )
		writer.write( firmware_update_id )
		writer.write( '\n' )

		writer.write( 'Firmware Version: ' )
		writer.write( firmware_version )
		writer.write( '\n' )

		writer.write( 'Recommended Firmware Version: ' )
		writer.write( recommended_firmware_version )
		writer.write( '\n' )

		writer.write( 'USB Type Descriptor: ' )
		writer.write( usb_type_descriptor )
		writer.write( '\n' )

		writer.write( 'Physical Port: ' )
		writer.write( physical_port )
		writer.write( '\n' )

		writer.write( 'Advanced Mode: ' )
		writer.write( advanced_mode )
		writer.write( '\n' )

		return writer.getvalue()

	# def get_informations ( self )

	# --------------------------------------------------
	
	def get_depth_scale ( self ):
		return self.device.get_depth_scale()

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this camera.

		Arguments:
			self (`pydataset.cameras.IntelCamera`): The camera to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )
		
		import pyrealsense2

		if ( self.logger ):
			self.logger.log( 'Running:' )
			self.logger.flush()

		while self.is_running():

			# Update the task
			self.update()

			# Get the composite frames
			frames = self.pipeline.wait_for_frames()

			# Check the frames
			if frames is None:
				raise RuntimeError( 'Could not get any frames' )

			# Align frames
			frames = self.frame_aligner.process( frames )

			# Unpack the frames
			frames = self.unpack_frames( frames )
			
			# Save the frames
			self._set_frames( frames )

			if ( self.logger ):
				self.logger.log(
					'{} ms: {}',
					self.timestamp,
					pytools.text.list_to_str( list(frames.keys()) )
					)
				self.logger.flush()

			time.sleep( 1.0/60.0 ) # 60 FPS max

		# while self.is_running()
		
		self.pipeline.stop()

		if ( self.logger ):
			self.logger.log( '    Done.' )
			self.logger.end_line()
			self.logger.flush()

	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this intel camera.

		Arguments:
			self (`pydataset.cameras.IntelCamera`): The intel camera to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCamera )

		super( IntelCamera, self ).start()
		
		import pyrealsense2

		self.find_device()
		self.config = self.get_config()
		self.frame_aligner = pyrealsense2.align( pyrealsense2.stream.color )

		if ( self.logger ):
			self.logger.log( 'Creating and starting pipeline:' )
			self.logger.flush()

		self.pipeline = pyrealsense2.pipeline()
		profile = self.pipeline.start( self.config )
		
		# Uncoment to find intrinsic parameters
		video_stream_profile = profile.get_stream( pyrealsense2.stream.color ).as_video_stream_profile()
		intrinsics = video_stream_profile.get_intrinsics()
		
		if ( self.logger ):
			self.logger.log( '    Done.' )
			self.logger.end_line()
			self.logger.log( 'Intrinsic parameters:' )
			self.logger.log( '    width: {}', intrinsics.width )
			self.logger.log( '    height: {}', intrinsics.height )
			self.logger.log( '    fx: {}', intrinsics.fx )
			self.logger.log( '    fy: {}', intrinsics.fy )
			self.logger.log( '    ppx: {}', intrinsics.ppx )
			self.logger.log( '    ppy: {}', intrinsics.ppy )
			self.logger.log( '    coeffs: {}', intrinsics.coeffs )
			self.logger.flush()

	# def start ( self )

	# --------------------------------------------------

	def unpack_frames ( self, frames ):
		"""
		Unpacks the realsense composite frames into numpy ndarray(s).

		Arguments:
			self (`pydataset.cameras.IntelCamera`): The intel camera of which to unpack frames.
		"""
		import pyrealsense2

		result = {}	

		for stream_settings in self.settings.streams:

			if not stream_settings.enabled:
				continue

			stream_type  = stream_settings.stream_type
			stream_index = stream_settings.index
			stream_name  = stream_settings.name
			stream_frame = None

			# Color frame
			if stream_type == StreamType.color:
				frame        = frames.get_color_frame()
				stream_frame = numpy.asanyarray( frame.get_data() )

			# Depth frame
			elif stream_type == StreamType.depth:
				frame        = frames.get_depth_frame()
				stream_frame = numpy.asanyarray( frame.get_data() )

			# Fisheye Frame
			elif stream_type == StreamType.fisheye:
				stream_frame = numpy.asanyarray( frames.get_fisheye_frame().get_data() )

			# Infrared left/right
			elif stream_type == StreamType.infrared:
					
				# Default
				if stream_index is None:
					pass
				# Left
				elif ( stream_index == 1 ):
					stream_name += 'left'
				# Right
				elif ( stream_index == 2 ):
					stream_name += '_right'
				else:
					raise RuntimeError( 'Unknown infrared stream index: {}', stream_index )

				stream_frame = numpy.asanyarray( frames.get_infrared_frame( stream_index ).get_data() )
			
			elif stream_type == StreamType.pose:
				
				stream_name += '_{}'.format(stream_index)
				stream_frame = numpy.asanyarray( frames.get_pose_frame( stream_index ).get_data() )

			else:
				self.fail( RuntimeError, 'Unsupported stream type' )
				return

			result[ stream_name ] = stream_frame

		# for stream_config in self.settings.streams

		return result

	# def unpack_frames ( self, frames )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings of an intel camera.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the intel camera settings constructor.

		Returns:
			`pydataset.cameras.IntelCameraSettings`: The intel camera settings.
		"""
		return IntelCameraSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class IntelCamera ( Camera )