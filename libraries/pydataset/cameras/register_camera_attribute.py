# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.factory

# LOCALS
from .camera_factory import CameraFactory

# ##################################################
# ###      CLASS REGISTER-CAMERA-ATTRIBUTE       ###
# ##################################################

class RegisterCameraAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Attribute used to register a camera by name in the camera factory.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name ):
		"""
		Initializes a new instance of the register camera attribute.

		Arguments:
			self (`pydataset.cameras.RegisterCameraAttribute`): Instance to initialize.
			name                                       (`str`): Name of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterCameraAttribute )
		assert pytools.assertions.type_is( name, str )

		super( RegisterCameraAttribute, self ).__init__(
			   group = CameraFactory.group_name,
			typename = name
			)

	# def __init__ ( self, name )

# class RegisterCameraAttribute ( pytools.factory.RegisterClassAttribute )