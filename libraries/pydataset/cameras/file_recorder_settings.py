# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .recorder_settings import RecorderSettings

# ##################################################
# ###        CLASS FILE-RECORDER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FileRecorderSettings',
	   namespace = 'pydataset.cameras',
	fields_names = [
		'directory',
		'filename_format',
		'filename_prefix'
		]
	)
class FileRecorderSettings ( RecorderSettings ):
	"""
	Settings for the file recorder.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self, directory=pytools.path.DirectoryPath(), filename_format='{}_{:010d}_{}.{}', filename_prefix='record', maximum_number_of_records=-1, name='FileRecorder' ):
		"""
		Initializes a new instance of the file recorder settings class.

		Arguments:
			self (`pydataset.cameras.FileRecorderSettings`): Instance to initialize.
			directory  (`str`/`pytools.path.DirectoryPath`): Path of the directory where to save the records.
			filename_format                         (`str`): Text format used to construct the name of record files.
			filename_prefix                         (`str`): Record file prefix.
			maximum_number_of_records               (`int`): Maximum number of records taken by the recorder.
			name                                    (`str`): Name of the recorder.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )
		assert pytools.assertions.type_is_instance_of( directory, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( filename_format, str )
		assert pytools.assertions.type_is( filename_prefix, str )
		assert pytools.assertions.type_is( maximum_number_of_records, int )
		assert pytools.assertions.type_is( name, str )

		super( FileRecorderSettings, self ).__init__( maximum_number_of_records, name )

		self.directory       = directory
		self.filename_format = filename_format
		self.filename_prefix = filename_prefix

	# def __init__ ( self, directory, filename_format, filename_prefix, maximum_number_of_records, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def directory ( self ):
		"""
		Path of the directory where to save the records (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )

		return self._directory
	
	# def directory ( self )
	
	# --------------------------------------------------

	@directory.setter
	def directory ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		if isinstance( value, str ):
			value = pytools.path.DirectoryPath( value )

		self._directory = value
	
	# def directory ( self, value )

	# --------------------------------------------------

	@property
	def filename_format ( self ):
		"""
		Text format used to construct the name of record files (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )

		return self._filename_format
	
	# def filename_format ( self )

	# --------------------------------------------------

	@filename_format.setter
	def filename_format ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._filename_format = value
	
	# def filename_format ( self, value )
	
	# --------------------------------------------------

	@property
	def filename_prefix ( self ):
		"""
		Record file prefix (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )

		return self._filename_prefix
	
	# def filename_prefix ( self )

	# --------------------------------------------------

	@filename_prefix.setter
	def filename_prefix ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FileRecorderSettings )
		assert pytools.assertions.type_is( value, str )
		
		self._filename_prefix = value
	
	# def filename_prefix ( self, value )

# class FileRecorderSettings ( RecorderSettings )