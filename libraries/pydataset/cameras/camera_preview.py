# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.tasks
import pyui.widgets

# LOCALS
from .camera import Camera
from .frame_display_widget import FrameDisplayWidget

# ##################################################
# ###            CLASS CAMERA-PREVIEW            ###
# ##################################################

class CameraPreview ( PyQt5.QtWidgets.QWidget ):

	# ##################################################
	# ###                 CONSTANTS                  ###
	# ##################################################

	# --------------------------------------------------

	DEFAULT_BUTTON_STYLE = '''
		font-style: bold;
		font-size: 8pt;
		color: white;
		'''
	
	GREEN_BUTTON_STYLE = '''
		font-style: bold;
		font-size: 8pt;
		color: white;
		'''
	
	RED_BUTTON_STYLE = '''
		font-style: bold;
		font-size: 8pt;
		color: red;
		'''
	
	ORANGE_BUTTON_STYLE = '''
		font-style: bold;
		font-size: 8pt;
		color: orange;
		'''

	# ##################################################
	# ###                   SIGNAL                   ###
	# ##################################################

	# --------------------------------------------------

	camera_started = PyQt5.QtCore.pyqtSignal()

	# --------------------------------------------------

	camera_stopped = PyQt5.QtCore.pyqtSignal()

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, camera=None, fps=15, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		super( CameraPreview, self ).__init__( parent )

		# Load icons
		self._icon_camera = pyui.widgets.Application.instance().assets.icon( 'icon_camera.png' )
		self._icon_play   = pyui.widgets.Application.instance().assets.icon( 'icon_play.png' )
		self._icon_stop   = pyui.widgets.Application.instance().assets.icon( 'icon_stop.png' )

		# Fields
		self._camera         = None
		self._camera_manager = None
		self._frame_mapper   = None
		self._timer_delay    = 1000.0 / float(fps)

		# Setup UI
		self.setup()

		# Set camera
		self.set_camera( camera )

	# def __init__ ( self, camera, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def display_size ( self ):

		return self._frame_preview.size()

	# def display_size ( self )

	# --------------------------------------------------

	@property
	def fps ( self ):

		return 1000.0 / float( self._timer.inerval() )
	
	# def fps ( self )

	# --------------------------------------------------

	@fps.setter
	def fps ( self, value ):

		self._timer.setInerval( float(value) / 1000.0 )

	# def fps ( self, value )
	
	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def get_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		return self._camera

	# def get_camera ( self )

	# --------------------------------------------------

	def is_playing ( self ):

		return (self._camera_manager is not None) and (self._camera is not None) and (self._camera.is_running())

	# def is_playing ( self )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_camera ( self, camera ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		# Set the camera fields
		self._camera = camera
		
		# Clear stream combo-box that may have been populated by a previous camera
		self._combobox_stream.blockSignals( True )
		self._combobox_stream.clear()
		self._combobox_stream.blockSignals( False )

		# Enable the play button if the camera is not None
		if ( camera ):

			# Update UI according to the camera
			self.update_ui()

		# Or disable it
		else:
			self._button_play.setText( 'No Camera' )
			self._button_play.setEnabled( False )

	# def set_camera ( self, camera )

	# --------------------------------------------------

	def set_frame_mapper ( self, frame_mapper ):

		self._frame_mapper = frame_mapper

	# def set_frame_mapper ( self, frame_mapper )

	# --------------------------------------------------

	def set_frame ( self, frame, stream_name, map_frame=True ):

		# Deep copy the frame
		frame = copy.deepcopy( frame )

		# Map the frame ?
		if map_frame and self._frame_mapper:
			frame = self._frame_mapper( stream_name, frame )
		
		# Set the pixmap in the label
		self._frame_preview.frame = frame

	# def set_frame ( self, frame, stream_name, map_frame )
	
	# --------------------------------------------------

	def set_raw_frame ( self, frame ):
		
		# Set the pixmap in the label
		self._frame_preview.frame = frame

	# def set_raw_frame ( self, frame )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------
	
	def clear_preview ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )
		
		self._frame_preview.frame = None

	# def clear_preview ( self ):
	
	# --------------------------------------------------

	def closeEvent ( self, event ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		# Stop the update timer
		if self._timer.isActive():
			self._timer.stop()
		self._timer = None

		# Stop the camera if its running
		if self._camera_manager:
			if self._camera.is_running():
				self.stop_camera()
			self._camera_manager = None
			self._camera = None

		# Close the window
		super( CameraPreview, self ).closeEvent( event )
	
	# def closeEvent ( self, event )
	
	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )
		
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Toolbar
		toolbar_widget = PyQt5.QtWidgets.QWidget( self )
		toolbar_layout = PyQt5.QtWidgets.QHBoxLayout( toolbar_widget )
		toolbar_layout.setContentsMargins( 1, 1, 1, 1 )
		toolbar_layout.setSpacing( 2 )
		toolbar_widget.setLayout( toolbar_layout )
		layout.addWidget( toolbar_widget, 0 )

		# Toolbar / Button Play
		self._button_play = PyQt5.QtWidgets.QPushButton( 'No Camera' )
		self._button_play.setMinimumSize( 128, 22 )
		self._button_play.setMaximumSize( 128, 22 )
		self._button_play.setIcon( self._icon_play )
		self._button_play.setCheckable( True )
		self._button_play.setEnabled( False )
		self._button_play.clicked.connect( self.on_button_play_clicked )
		toolbar_layout.addWidget( self._button_play, 0 )

		# Toolbar / ComboBox Stream
		self._combobox_stream = PyQt5.QtWidgets.QComboBox()
		self._combobox_stream.setMinimumSize( 128, 22 )
		self._combobox_stream.setMaximumSize( 128, 22 )
		self._combobox_stream.setEnabled( False )
		toolbar_layout.addWidget( self._combobox_stream, 0 )

		# Toolbar / Spacer
		toolbar_layout.addStretch()
		
		# Label Preview
		self._frame_preview = FrameDisplayWidget( None, self )
		layout.addWidget( self._frame_preview, 1 )

		# Update Timer
		self._timer = PyQt5.QtCore.QTimer()
		self._timer.setInterval( self._timer_delay )
		self._timer.timeout.connect( self.update_ui )

	# def setup ( self )
		
	# --------------------------------------------------

	def start_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		# Reset the camera if it previously failed
		if ( self._camera.state == pytools.tasks.TaskState.failed ):
			self._camera.state = pytools.tasks.TaskState.idle

		# Start the camera
		self._camera_manager = pytools.tasks.Manager( self._camera )
		self._camera_manager.start_tasks()
		self._timer.start()

	# def start_camera ( self )

	# --------------------------------------------------

	def stop_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		if self._camera_manager:
			# Stop the camera
			self._camera_manager.stop_tasks( wait_for_stop=True )
			self._camera_manager.join_tasks()
			self._timer.stop()

	# def stop_camera ( self )

	# --------------------------------------------------

	def update_preview ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraPreview )

		timestamp, frames = self._camera.get_frames()

		if frames:

			# Get the stream combo box to select a stream
			stream_name = self._combobox_stream.currentText()
			
			# If its empty set its items
			if not stream_name:
				self._combobox_stream.blockSignals( True )
				self._combobox_stream.clear()
				self._combobox_stream.addItems( frames.keys() )
				self._combobox_stream.blockSignals( False )
				stream_name = self._combobox_stream.currentText()

			# Get the selected stream
			frame = frames[ stream_name ]

			self.set_frame( frame, stream_name, True )

		# if frames

	# def update_preview ( self )
	
	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( bool )
	def on_button_play_clicked ( self, checked ):

		if ( checked ):
			self.start_camera()
			self.camera_started.emit()

		else:
			self.stop_camera()
			self.update_ui()
			self.camera_stopped.emit()

	# def on_button_preview_clicked ( self, checked )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def update_ui ( self ):

		# Get the state of the camera
		state = self._camera.state
		
		# Set the text of the button play
		self._button_play.setText( str(state).upper() )

		if state == pytools.tasks.TaskState.idle:
			# button play
			self._button_play.setEnabled( True )
			self._button_play.setIcon( self._icon_play )
			self._button_play.setStyleSheet( CameraPreview.DEFAULT_BUTTON_STYLE )
			# combobox stream
			self._combobox_stream.setEnabled( False )
			# label preview
			self.clear_preview()
			
		elif state == pytools.tasks.TaskState.starting:
			self._button_play.setEnabled( False )
			self._button_play.setIcon( self._icon_play )
			self._button_play.setStyleSheet( CameraPreview.GREEN_BUTTON_STYLE )
			self._combobox_stream.setEnabled( False )

		elif state == pytools.tasks.TaskState.running:
			self._button_play.setEnabled( True )
			self._button_play.setIcon( self._icon_stop )
			self._button_play.setStyleSheet( CameraPreview.GREEN_BUTTON_STYLE )
			self._combobox_stream.setEnabled( True )
			self.update_preview()

		elif state == pytools.tasks.TaskState.stopping:
			self._button_play.setEnabled( False )
			self._button_play.setIcon( self._icon_stop )
			self._button_play.setStyleSheet( CameraPreview.GREEN_BUTTON_STYLE )
			self._combobox_stream.setEnabled( False )

		elif state == pytools.tasks.TaskState.stopped:
			self._button_play.setEnabled( True )
			self._button_play.setIcon( self._icon_play )
			self._combobox_stream.setEnabled( False )
			self._combobox_stream.clear()
			self.clear_preview()

		elif state == pytools.tasks.TaskState.failed:
			self._button_play.blockSignals( True )
			self._button_play.setChecked( False )
			self._button_play.setEnabled( True )
			self._button_play.setIcon( self._icon_play )
			self._button_play.blockSignals( False )
			self._button_play.setStyleSheet( CameraPreview.RED_BUTTON_STYLE )
			self._combobox_stream.setEnabled( False )
			self.clear_preview()

	# def update_ui ( self )

# class CameraPreview ( PyQt5.QtWidgets.QWidget )