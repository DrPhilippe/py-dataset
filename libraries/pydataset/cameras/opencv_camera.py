# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import io
import os
import time

# INTERNALS
import pytools.assertions
import pytools.tasks
import pytools.text

# LOCALS
from .camera                    import Camera
from .register_camera_attribute import RegisterCameraAttribute
from .opencv_camera_settings    import OpenCVCameraSettings

# ##################################################
# ###            CLASS OPENCV-CAMERA             ###
# ##################################################

@RegisterCameraAttribute( 'OpenCV Camera' )
class OpenCVCamera ( Camera ):
	"""
	Generic camera based on OpenCV video capture.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings=OpenCVCameraSettings(), logger=None ):
		"""
		Initializes a new instance of the `pydataset.cameras.OpenCVCamera` class.

		Arguments:
			self             (`pydataset.cameras.OpenCVCamera`): Instance to initialize.
			settings (`pydataset.cameras.OpenCVCameraSettings`): Settings of the camera.
			logger                     (`pytools.tasks.Logger`): Logger used to log the activity of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )
		assert pytools.assertions.type_is_instance_of( settings, OpenCVCameraSettings )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )

		super( OpenCVCamera, self ).__init__( settings, logger )

	# def __init__ ( self, settings, logger )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def capture ( self ):
		"""
		OpenCV video capture used to access the physical camera (`cv2.VideoCapture`).
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )

		return self._capture

	# def capture ( self )

	# --------------------------------------------------

	@capture.setter
	def capture ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )
		assert pytools.assertions.type_is_instance_of( value, cv2.VideoCapture )

		self._capture = value

	# def capture ( self, value )

	# ##################################################
	# ###                   METHOD                   ###
	# ##################################################
	
	# --------------------------------------------------

	def get_description ( self ):
		"""
		Returns a textual description of this OpenCV camera.

		Arguments:
			self (`pydataset.cameras.OpenCVCamera`): OpenCV camera to describe.

		Returns:
			`str`: Description of this camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )

		return 'OpenCV Camera: {}'.format(self.settings.name)

	# def get_informations ( self )

	# --------------------------------------------------

	def run ( self ):
		"""
		Runs this OpenCV camera.

		Arguments:
			self (`pydataset.cameras.OpenCVCamera`): The OpenCV camera to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )
		
		if ( self.logger ):
			self.logger.log( 'Running:' )
			self.logger.flush()

		while self.is_running():

			# Update the task
			self.update()

			# Check if the camera is still connected
			if not self.capture.isOpened():
				raise RuntimeError( 'Camera disconnected' )

			# Get the frame set
			ret, frame = self.capture.read()

			# Check the frames
			if not ret:
				raise RuntimeError( 'Could not get any frames' )
			
			# Save the frames
			frames = { 'color': frame }
			self._set_frames( frames )
			
			# Log
			if ( self.logger ):
				self.logger.log(
					'{} ms: {}',
					self.timestamp,
					pytools.text.list_to_str( list(frames.keys()) )
					)
				self.logger.flush()

		# while self.status in [CameraStatus.running, CameraStatus.paused, CameraStatus.resuming]
		
		if ( self.logger ):
			self.logger.log( '    Done.' )
			self.logger.end_line()
			self.logger.flush()

		self.capture.release()
		
	# def run ( self )
	
	# --------------------------------------------------

	def start ( self ):
		"""
		Starts this OpenCV camera and then runs it.

		Arguments:
			self (`pydataset.cameras.OpenCVCamera`): The OpenCV camera to start.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCamera )

		super( OpenCVCamera, self ).start()

		if ( self.logger ):
			self.logger.log( 'Creating video capture:' )
			self.logger.flush()

		self.capture = cv2.VideoCapture()
		self.capture.open( 0 )
	
		if not self.capture.isOpened():
			raise RuntimeError( 'Fail to connect to camera' )

		if ( self.logger ):
			self.logger.log( '    Done.' )
			self.logger.end_line()
			self.logger.flush()

	# def start ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings of a generic camera.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the generic camera settings constructor.

		Returns:
			`pydataset.cameras.OpenCVCameraSettings`: The camera settings.
		"""
		raise pycameras.OpenCVCameraSettings( **kwargs )

	# def create_settings ( cls, **kwargs )
	
# class GenericCamera ( Camera )