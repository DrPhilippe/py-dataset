# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###            CLASS STREAM-FORMAT             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'StreamFormat',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class StreamFormat ( pytools.serialization.Enum ):
	"""
	Enum defining the type of the data in a given retreived frame. 
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	any           = None
	bgr8          = None
	bgra8         = None
	disparity16   = None
	disparity32   = None
	distance      = None
	gpio_raw      = None
	invi          = None
	inzi          = None
	mjpeg         = None
	motion_raw    = None
	motion_xyz32f = None
	raw10         = None
	raw16         = None
	raw8          = None
	rgb8          = None
	rgba8         = None
	six_dof       = None
	uyvy          = None
	w10           = None
	xyz32f        = None
	y10bpack      = None
	y12i          = None
	y16           = None
	y8            = None
	y8i           = None
	yuyv          = None
	z16           = None
	z16h          = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='bgr8' ):
		"""
		Initializes a new instance of the `pydataset.cameras.StreamFormat` enum.

		Arguments:
			self (`pydataset.cameras.StreamFormat`): Instance to initialize.
			value                           (`str`): Value of the enum.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamFormat )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( StreamFormat, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible values that an enum of type `cls` can take.

		Arguments:
			cls (`type`): Type of `pydataset.cameras.StreamFormat`.

		Returns:
			`list` of `str`: The list of possible values.
		"""
		return [
			'any',
			'bgr8',
			'bgra8',
			'disparity16',
			'disparity32',
			'distance',
			'gpio_raw',
			'invi',
			'inzi',
			'mjpeg',
			'motion_raw',
			'motion_xyz32f',
			'raw10',
			'raw16',
			'raw8',
			'rgb8',
			'rgba8',
			'six_dof',
			'uyvy',
			'w10',
			'xyz32f',
			'y10bpack',
			'y12i',
			'y16',
			'y8',
			'y8i',
			'yuyv',
			'z16',
			'z16h'
			]
		
	# def get_possible_values ( cls )

# class StreamFormat

StreamFormat.any           = StreamFormat( 'any' )
StreamFormat.bgr8          = StreamFormat( 'bgr8' )
StreamFormat.bgra8         = StreamFormat( 'bgra8' )
StreamFormat.disparity16   = StreamFormat( 'disparity16' )
StreamFormat.disparity32   = StreamFormat( 'disparity32' )
StreamFormat.distance      = StreamFormat( 'distance' )
StreamFormat.gpio_raw      = StreamFormat( 'gpio_raw' )
StreamFormat.invi          = StreamFormat( 'invi' )
StreamFormat.inzi          = StreamFormat( 'inzi' )
StreamFormat.mjpeg         = StreamFormat( 'mjpeg' )
StreamFormat.motion_raw    = StreamFormat( 'motion_raw' )
StreamFormat.motion_xyz32f = StreamFormat( 'motion_xyz32f' )
StreamFormat.raw10         = StreamFormat( 'raw10' )
StreamFormat.raw16         = StreamFormat( 'raw16' )
StreamFormat.raw8          = StreamFormat( 'raw8' )
StreamFormat.rgb8          = StreamFormat( 'rgb8' )
StreamFormat.rgba8         = StreamFormat( 'rgba8' )
StreamFormat.six_dof       = StreamFormat( 'six_dof' )
StreamFormat.uyvy          = StreamFormat( 'uyvy' )
StreamFormat.w10           = StreamFormat( 'w10' )
StreamFormat.xyz32f        = StreamFormat( 'xyz32f' )
StreamFormat.y10bpack      = StreamFormat( 'y10bpack' )
StreamFormat.y12i          = StreamFormat( 'y12i' )
StreamFormat.y16           = StreamFormat( 'y16' )
StreamFormat.y8            = StreamFormat( 'y8' )
StreamFormat.y8i           = StreamFormat( 'y8i' )
StreamFormat.yuyv          = StreamFormat( 'yuyv' )
StreamFormat.z16           = StreamFormat( 'z16' )
StreamFormat.z16h          = StreamFormat( 'z16h' )