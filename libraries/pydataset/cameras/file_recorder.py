# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from .camera import Camera
from .recorder import Recorder
from .file_recorder_settings import FileRecorderSettings

# ##################################################
# ###            CLASS FILE-RECORDER             ###
# ##################################################

class FileRecorder ( Recorder ):
	"""
	Recorder used to record camrea frames in image files.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings, camera=None, logger=None, progress_tracker=None ):
		"""
		Initialize a new instance of the file recorder class.

		Arguments:
			self                   (`pydataset.cameras.FileRecorder`): Instance to initialize.
			settings       (`pydataset.cameras.FileRecorderSettings`): Settings of the recorder.
			camera                       (`pydataset.cameras.Camera`): Camera to record.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the task.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the task.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorder )
		assert pytools.assertions.type_is_instance_of( settings, FileRecorderSettings )
		assert pytools.assertions.type_is_instance_of( camera, (type(None), Camera) )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), pytools.tasks.Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		super( FileRecorder, self ).__init__( settings, camera, logger, progress_tracker )
		
	# def __init__ ( self, settings, camera, logger, progress_tracker )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def record ( self, index, frames ):
		"""
		Record the given set of frames in files.

		Arguments:
			self     (`pydataset.cameras.FileRecorder`): Recorder.
			index                               (`int`): Index of the set of frames.
			frames (`dict` of `str` to `numpy.ndarray`): Set of frames.
		"""
		assert pytools.assertions.type_is_instance_of( self, FileRecorder )
		assert pytools.assertions.type_is_instance_of( frames, dict )
		assert pytools.assertions.dictionary_types_are( frames, str, numpy.ndarray )
		
		directory       = self.settings.directory
		filename_format = self.settings.filename_format
		filename_prefix = self.settings.filename_prefix

		for key in frames.keys():

			frame = frames[ key ]
			dtype = frame.dtype
			shape = frame.shape
			rank  = len(shape)
			depth = shape[2] if rank > 2 else 1

			# The array is an image
			if ( dtype == numpy.uint8 and rank in [2, 3] and depth < 5 ):
					
				# Create the jpg filepath
				filepath = directory + pytools.path.FilePath.format( filename_format, filename_prefix, index, key, 'jpg' )

				# Save the image
				cv2.imwrite( str(filepath), frame )
			
			# The array is something else
			else:

				# Create the npy filepath
				filepath = directory + pytools.path.FilePath.format( filename_format, filename_prefix, index, key, 'npy' )

				# Save the image
				numpy.save( str(filepath), frame )

			# for key in frames.keys()
			
	# def record ( self, index, frames )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a file recorder.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`pydataset.cameras.FileRecorderSettings`: The settings.
		"""
		return FileRecorderSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class FileRecorder ( Recorder )