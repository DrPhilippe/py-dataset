# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###             CLASS STREAM-TYPE              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'StreamType',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class StreamType ( pytools.serialization.Enum ):
	"""
	Intel realsense stream type enumeration.
	"""

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	accel      = None
	any        = None
	color      = None
	confidence = None
	depth      = None
	fisheye    = None
	gpio       = None
	gyro       = None
	infrared   = None
	pose       = None

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value='color' ):
		"""
		Initializes a new instance of the stream type enum.

		Arguments:
			self (`pydataset.cameras.StreamType`): Instance to initialize.
			value                         (`str`): Value of the enum.
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamType )
		assert pytools.assertions.type_is( value, str )
		assert pytools.assertions.value_in( value, type(self).get_possible_values() )

		super( StreamType, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible values the stream type enum can take.

		Arguments:
			cls (`type`): StreamType

		Returns:
			`list` of `str`: The list of possible values.
		"""
		return [
			'accel',
			'any',
			'color',
			'confidence',
			'depth',
			'fisheye',
			'gpio',
			'gyro',
			'infrared',
			'pose'
			]
		
	# def get_possible_values ( cls )

# class StreamType ( pytools.serialization.Enum )

StreamType.accel      = StreamType( 'accel' )
StreamType.any        = StreamType( 'any' )
StreamType.color      = StreamType( 'color' )
StreamType.confidence = StreamType( 'confidence' )
StreamType.depth      = StreamType( 'depth' )
StreamType.fisheye    = StreamType( 'fisheye' )
StreamType.gpio       = StreamType( 'gpio' )
StreamType.gyro       = StreamType( 'gyro' )
StreamType.infrared   = StreamType( 'infrared' )
StreamType.pose       = StreamType( 'pose' )