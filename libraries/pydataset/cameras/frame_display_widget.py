# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.widgets

# ##################################################
# ###         CLASS FRAME-DISPLAY-WIDGET         ###
# ##################################################

class FrameDisplayWidget ( PyQt5.QtWidgets.QFrame ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, frame, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, FrameDisplayWidget )

		super( FrameDisplayWidget, self ).__init__( parent )

		self._frame = frame

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------
	
	@property
	def frame ( self ):
		assert pytools.assertions.type_is_instance_of( self, FrameDisplayWidget )

		return self._frame
	
	# def frame ( self )

	# --------------------------------------------------

	@frame.setter
	def frame ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, FrameDisplayWidget )

		self._frame = value
		self.repaint()

	# def frame ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def paintEvent ( self, event ):
		"""
		Paints this camera preview frame.

		Arguments:
			self (`pydataset.inspector.FrameDisplayWidget`): Widget to paint.
			event                   (`PyQt5.QtCore.QEvent`): Paint event infos
		"""
		assert pytools.assertions.type_is_instance_of( self, FrameDisplayWidget )
		assert pytools.assertions.type_is_instance_of( event, PyQt5.QtCore.QEvent )

		option = PyQt5.QtWidgets.QStyleOption()
		option.initFrom( self )

		rect = self.rect()

		# Begin painting
		painter = PyQt5.QtGui.QPainter()
		painter.begin( self )

		# Fill self with black
		black  = PyQt5.QtGui.QColor.fromRgb( 0, 0, 0, 255 )
		painter.fillRect( rect.x(), rect.y(), rect.width(), rect.height(), black )

		# Is there a frame to paint ?
		if self._frame is not None:
			pixmap = pyui.widgets.images_utils.image_to_qpixmap( self._frame )
			painter.drawPixmap( rect, pixmap )

		# End painting
		painter.end()

		# Accept the event
		event.accept()

	# def paintEvent ( self, event )

# class FrameDisplayWidget ( PyQt5.QtWidgets.QFrame )