# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .camera_settings import CameraSettings

# ##################################################
# ###        CLASS OPENCV-CAMERA-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'OpenCVCameraSettings',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class OpenCVCameraSettings ( CameraSettings ):
	"""
	Generic camera settings.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='OpenCV Camera' ):
		"""
		Initializes a new instance of the generic camera settings class.

		Arguments:
			self (`pydataset.cameras.OpenCVCameraSettings`): Instance to initialize.
			name                                    (`str`): Name of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, OpenCVCameraSettings )
		assert pytools.assertions.type_is( name, str )
		
		super( OpenCVCameraSettings, self ).__init__( name )

	# def __init__ ( self )

# class OpenCVCameraSettings ( CameraSettings )