# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###          CLASS CAMERA-RESOLUTION           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'StreamResolution',
	   namespace = 'pydataset.cameras',
	fields_names = []
	)
class StreamResolution ( pytools.serialization.Enum ):
	"""
	Resolutions supported by intel cameras.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, width=1280, height=720 ):
		"""
		Initializes a new instance of the stream resolution with the given width and height.
		
		Arguments:
			self (`pydataset.cameras.StreamResolution`): Instance to initialize.
			width                               (`int`): Width
			height                              (`int`): Height
		"""
		assert pytools.assertions.type_is_instance_of( self, StreamResolution )
		assert pytools.assertions.type_is( width, int )
		assert pytools.assertions.type_is( height, int )

		value = '{} x {}'.format(width, height)

		super( StreamResolution, self ).__init__( value )

	# def __init__ ( self, value )
	
	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------
	
	@classmethod
	def get_possible_values ( cls ):
		"""
		Returns the list of possible values the stream resolution enum can take.

		Arguments:
			cls (`type`): StreamResolution

		Returns:
			`list` of `str`: The list of possible values.
		"""
		return [
			'1920 x 1080',
			'1280 x 800',
			'1280 x 720',
			'960 x 540',
			'848 x 480',
			'848 x 100',
			'640 x 480',
			'640 x 400',
			'640 x 360',
			'480 x 270',
			'424 x 240',
			'320 x 240',
			'320 x 180',
			'256 x 144'
			]
		
	# def get_possible_values ( cls )

# class StreamResolution