# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .camera_settings   import CameraSettings
from .stream_format     import StreamFormat
from .stream_resolution import StreamResolution
from .stream_settings   import StreamSettings
from .stream_type       import StreamType

# ##################################################
# ###        CLASS INTEL-CAMERA-SETTINGS         ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'IntelCameraSettings',
	   namespace = 'pydataset.cameras',
	fields_names = [
		'streams',
		]
	)
class IntelCameraSettings ( CameraSettings ):
	"""
	Settings for intel cameras.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='Intel RealSense D435I' ):
		"""
		Initializes a new instance of the `pydataset.cameras.CameraSettings` class.

		Arguments:
			self (`pydataset.cameras.CameraSettings`): Instance to initialize.
			name                              (`str`): Name of the camera.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCameraSettings )
		assert pytools.assertions.type_is( name, str )
		
		super( IntelCameraSettings, self ).__init__( name )
		
		self._streams = [
			StreamSettings( True, StreamType.color, -1, 30, StreamResolution(1280, 720), StreamFormat.bgr8 ),
			StreamSettings( True, StreamType.depth, -1, 30, StreamResolution(1280, 720), StreamFormat.z16  )
			]
		
	# def __init__ ( self )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def streams ( self ):
		"""
		Individual stream settings (`list` of `pydataset.cameras.StreamSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, IntelCameraSettings )

		return self._streams

	# def streams ( self )
	
	# --------------------------------------------------

	@streams.setter
	def streams ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, IntelCameraSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, StreamSettings )

		self._streams = value
		
	# def streams ( self, value )

# class IntelCameraSettings ( CameraSettings )