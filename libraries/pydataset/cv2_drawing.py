# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def draw_bounding_box ( image, bb, edges_color=(0,0,0), edges_thickness=1, points_color=(0,0,0), points_radius=1, points_thickness=1 ):
	"""
	Draws a bounding box on the given image.

	Arguments:
		image                     (`numpy.ndarray`): Image to draw on.
		bb                        (`numpy.ndarray`): Bounding box.
		edges_color         (`tuple` of `3` `int`s): Color used to draw the box edges.
		edges_thickness                     (`int`): Edges line thickness.
		points_color (`None`/`tuple` of `3` `int`s): Color used to draw the box corners.
		points_radius                     (`float`): Radius of the points.
		points_thickness                    (`int`): Point permiter thickness.

	Returns:
		`numpy.ndarray`: The image.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.equal( len(image.shape), 3 )
	assert pytools.assertions.type_is_instance_of( bb, numpy.ndarray )
	assert pytools.assertions.equal( bb.dtype, numpy.float32 )
	assert pytools.assertions.equal( list(bb.shape), [8, 2] )
	assert pytools.assertions.type_is( edges_color, tuple )
	assert pytools.assertions.equal( len(edges_color), 3 )
	assert pytools.assertions.tuple_items_type_is( edges_color, int )
	assert pytools.assertions.type_is( edges_thickness, int )
	assert pytools.assertions.type_in( points_color, (type(None), tuple) )
	if isinstance( points_color, tuple ):
		assert pytools.assertions.equal( len(points_color), 3 )
		assert pytools.assertions.tuple_items_type_is( points_color, int )
	assert pytools.assertions.type_is( points_radius, int )
	assert pytools.assertions.type_is( points_thickness, int )

	# Bottom
	image = draw_bb_edge( image, bb, 0, 1, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 1, 2, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 2, 3, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 3, 0, edges_color, edges_thickness )
	# Top
	image = draw_bb_edge( image, bb, 4, 5, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 5, 6, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 6, 7, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 7, 4, edges_color, edges_thickness )
	# Connecting lines
	image = draw_bb_edge( image, bb, 0, 4, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 1, 5, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 2, 6, edges_color, edges_thickness )
	image = draw_bb_edge( image, bb, 3, 7, edges_color, edges_thickness )
	# Points
	if points_color is not None:
		image = draw_points( image, bb, points_color, points_radius, points_thickness )
	# Done
	return image

# def draw_bounding_box ( image, bb, edges_color, edges_thickness, points_color, points_radius, points_thickness )

# --------------------------------------------------

def draw_bounding_rectangle ( image, rect, rect_color=(255,0,0), rect_thickness=1, points_color=(255,0,0), points_radius=1, points_thickness=1 ):
	"""
	Draws a bounding box on the given image.

	Arguments:
		image                     (`numpy.ndarray`): Image to draw on.
		rect                      (`numpy.ndarray`): Bounding rectangle.
		rect_color          (`tuple` of `3` `int`s): Color used to draw the rectangle's edges.
		rect_thickness                      (`int`): Edges line thickness.
		points_color (`None`/`tuple` of `3` `int`s): Color used to draw the rectangle's corners.
		points_radius                     (`float`): Radius of the points.
		points_thickness                    (`int`): Point permiter thickness.

	Returns:
		`numpy.ndarray`: The image.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.equal( len(image.shape), 3 )
	assert pytools.assertions.type_is_instance_of( rect, numpy.ndarray )
	assert pytools.assertions.equal( rect.dtype, numpy.int32 )
	assert pytools.assertions.equal( list(rect.shape), [2, 2] )
	assert pytools.assertions.type_is( rect_color, tuple )
	assert pytools.assertions.equal( len(rect_color), 3 )
	assert pytools.assertions.tuple_items_type_is( rect_color, int )
	assert pytools.assertions.type_is( rect_thickness, int )
	assert pytools.assertions.type_in( points_color, (type(None), tuple) )
	if isinstance( points_color, tuple ):
		assert pytools.assertions.equal( len(points_color), 3 )
		assert pytools.assertions.tuple_items_type_is( points_color, int )
	assert pytools.assertions.type_is( points_radius, int )
	assert pytools.assertions.type_is( points_thickness, int )


	# Rect
	image = cv2.rectangle( image, (rect[0,0], rect[0,1]), (rect[1,0], rect[1,1]), rect_color, rect_thickness )
	
	# Points
	if points_color is not None:
		image = draw_points( image, rect.astype(numpy.float32), points_color, points_radius, points_thickness )
	
	# Done
	return image

# def draw_bounding_rectangle ( image, rect, rect_color, rect_thickness, points_color, points_radius, points_thickness )

# --------------------------------------------------

def draw_bb_edge ( image, bb, index_p1, index_p2, color, thickness=1 ):
	"""
	Draws the edge connection the two points of a bounding box with given indexes.

	Arguments:
		image        (`numpy.ndarray`): Image to draw on.
		bb           (`numpy.ndarray`): Bounding box.
		index_p1               (`int`): Index of the first point.
		index_p2               (`int`): Index of the second point.
		color  (`tuple` of `3` `int`s): Color used to draw the box edge.
		thickness              (`int`): Edge line thickness.

	Returns:
		`numpy.ndarray`: The image.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.equal( len(image.shape), 3 )
	assert pytools.assertions.type_is_instance_of( bb, numpy.ndarray )
	assert pytools.assertions.equal( bb.dtype, numpy.float32 )
	assert pytools.assertions.equal( list(bb.shape), [8, 2] )
	assert pytools.assertions.type_is( index_p1, int )
	assert pytools.assertions.value_in( index_p1, range(8) )
	assert pytools.assertions.type_is( index_p2, int )
	assert pytools.assertions.value_in( index_p2, range(8) )
	assert pytools.assertions.type_is( color, tuple )
	assert pytools.assertions.equal( len(color), 3 )
	assert pytools.assertions.tuple_items_type_is( color, int )
	assert pytools.assertions.type_is( thickness, int )

	return draw_edge( image, bb[index_p1, :], bb[index_p2, :], color, thickness )

# def draw_bb_edge ( image, bb, index_p1, index_p2, color, thickness )
	
# --------------------------------------------------

def draw_edge ( image, p1, p2, color, thickness=1 ):
	"""
	Draws an edge.

	Arguments:
		image        (`numpy.ndarray`): Image to draw on.
		p1           (`numpy.ndarray`): First point delimiting the edge.
		p2           (`numpy.ndarray`): Second point delimiting the edge.
		color  (`tuple` of `3` `int`s): Color used to draw the edge.
		thickness              (`int`): Edge line thickness.

	Returns:
		`numpy.ndarray`: The image.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.equal( len(image.shape), 3 )
	assert pytools.assertions.type_is_instance_of( p1, numpy.ndarray )
	assert pytools.assertions.equal( p1.dtype, numpy.float32 )
	assert pytools.assertions.equal( list(p1.shape), [2] )
	assert pytools.assertions.type_is_instance_of( p2, numpy.ndarray )
	assert pytools.assertions.equal( p2.dtype, numpy.float32 )
	assert pytools.assertions.equal( list(p2.shape), [2] )
	assert pytools.assertions.type_is( color, tuple )
	assert pytools.assertions.equal( len(color), 3 )
	assert pytools.assertions.tuple_items_type_is( color, int )
	assert pytools.assertions.type_is( thickness, int )

	return cv2.line( image, ( int(p1[0]), int(p1[1]) ), ( int(p2[0]), int(p2[1]) ), color, thickness )

# def draw_edge ( image, bb, index_p1, index_p2, color, thickness )

# --------------------------------------------------

def draw_point ( image, point, color, radius=1, thickness=1 ):
	"""
	Draws a point on an image.
	"""
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( point, numpy.ndarray )
	assert pytools.assertions.equal( point.dtype, numpy.float32 )
	assert pytools.assertions.equal( len(point.shape), 1 )
	assert pytools.assertions.equal( point.shape[0], 2 )
	assert pytools.assertions.type_is( color, tuple )
	assert pytools.assertions.equal( len(color), 3 )
	assert pytools.assertions.tuple_items_type_is( color, int )
	assert pytools.assertions.type_is( radius, int )
	assert pytools.assertions.type_is( thickness, int )
	
	return cv2.circle( image, (int(point[0]), int(point[1])), radius, color, thickness )

# def draw_point ( image, point, color, radius, thickness )

# --------------------------------------------------

def draw_points ( image, points, color, radius=1, thickness=1 ):
	assert pytools.assertions.type_is_instance_of( image, numpy.ndarray )
	assert pytools.assertions.type_is_instance_of( points, numpy.ndarray )
	assert pytools.assertions.equal( points.dtype, numpy.float32 )
	assert pytools.assertions.equal( len(points.shape), 2 )
	assert pytools.assertions.equal( points.shape[1], 2 )
	assert pytools.assertions.type_is( color, tuple )
	assert pytools.assertions.equal( len(color), 3 )
	assert pytools.assertions.tuple_items_type_is( color, int )
	assert pytools.assertions.type_is( radius, int )
	assert pytools.assertions.type_is( thickness, int )
	
	for i in range( points.shape[0] ):
		image = draw_point( image, points[i], color, radius, thickness )
	return image

# def draw_points ( image, points, color, radius, thickness )