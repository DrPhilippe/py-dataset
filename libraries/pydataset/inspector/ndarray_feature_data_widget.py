# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.widgets

# LOCALS
from ..dataset               import NDArrayFeatureData
from .feature_data_widget    import FeatureDataWidget
from .shape_data_item_widget import ShapeDataItemWidget

# ##################################################
# ###       CLASS TEXT-FEATURE-DATA-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'NDArrayFeatureData' )
class NDArrayFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.NDArrayFeatureDataWidget` allows to display/edit `pydataset.dataset.NDArrayFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.NDArrayFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.NDArrayFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.NDArrayFeatureData`): Float feature data to display/edit.
			is_editable                                  (`bool`): If `True` value can be edited using the item widget.
			parent             (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), NDArrayFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( NDArrayFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this ndarray feature data widget.

		Arguments:
			self (`pydataset.inspector.NDArrayFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, NDArrayFeatureDataWidget )

		# Setup the parent class
		super( NDArrayFeatureDataWidget, self ).setup()

		# Get the feature data and its value
		feature_data  = self.value
		feature_value = feature_data.value if feature_data else None
		feature_shape = feature_data.shape if feature_data else None
		
		# Display/edit the shape
		self._shape_itemwidget = ShapeDataItemWidget(
			feature_shape,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Shape', self._shape_itemwidget )

		# Display/edit the ndarray
		self._value_itemwidget = pyui.inspector.NDArrayItemWidget(
			feature_value,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

# class NDArrayFeatureDataWidget ( FeatureDataWidget )