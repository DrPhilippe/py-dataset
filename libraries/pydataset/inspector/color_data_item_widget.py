# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..data import ColorData

# ##################################################
# ###        CLASS COLOR-DATA-ITEM-WIDGET        ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'ColorData' )
class ColorDataItemWidget ( pyui.inspector.ItemWidget ):
	"""
	The class `pydataset.inspector.ColorDataItemWidget` allows to display/edit `pydataset.data.ColorData`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.ColorDataItemWidget`.

		Arguments:
			self (`pydataset.inspector.ColorDataItemWidget`): Instance to initialize.
			value        (`None`/`pydataset.data.ColorData`): Color data to display/edit.
			is_editable                             (`bool`): If `True` value can be edited using the item widget.
			parent        (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorDataItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ColorData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Initializes the parent class
		super( ColorDataItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def paintEvent ( self, event ):
		"""
		Paints this color data item widget.

		Arguments:
			self (`pydataset.inspector.ColorDataItemWidget`): Color data item widget to paint.
			event (`PyQt5.QtCore.QEvent`)
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorDataItemWidget )
		assert pytools.assertions.type_is_instance_of( event, PyQt5.QtCore.QEvent )

		option = PyQt5.QtWidgets.QStyleOption()
		option.initFrom( self )

		rect = self.rect()

		# Begin painting
		painter = PyQt5.QtGui.QPainter()
		painter.begin( self )

		# draw the border
		black  = PyQt5.QtGui.QColor.fromRgb( 0,   0, 0, 255 )
		red    = PyQt5.QtGui.QColor.fromRgb( 255, 0, 0, 255 )
		painter.fillRect( rect.x(), rect.y(), rect.width(), rect.height(), black )

		if ( self.value is not None ):

			# Draw the RGB and alpha bellow
			offset = int( rect.height() / 8 * 6 )
			rest   = rect.height()-offset
			color  = PyQt5.QtGui.QColor.fromRgb( self.value.red,   self.value.green, self.value.blue,  255 )
			alpha  = PyQt5.QtGui.QColor.fromRgb( self.value.alpha, self.value.alpha, self.value.alpha, 255 )

			painter.fillRect( rect.x()+1, rect.y()+1, rect.width()-2, offset, color )
			painter.fillRect( rect.x()+1, offset,     rect.width()-2, rest-1, alpha )

		else:
			# Draw a black square crossed out in red
			painter.fillRect( rect.x()+1, rect.y()+1, rect.width()-2, rect.height()-2, black )

			painter.setBrush( PyQt5.QtGui.QBrush(red, PyQt5.QtCore.Qt.SolidPattern) )
			painter.drawLine( rect.x()+1, rect.y()+1, rect.width()-2, rect.height()-2 )
			painter.drawLine( rect.x()+1, rect.height()-2, rect.width()-2, rect.y()+1 )

		# if ( self.get_value() is not None )

		# End painting
		painter.end()

		# Accept the event
		event.accept()

	# def paintEvent ( self, event )
	
	# --------------------------------------------------

	def mousePressEvent ( self, event ):
		"""
		Method used to listen to mouse press event.

		Arguments:
			self (`pydataset.inspector.ColorDataItemWidget`): Color data item widget.
			event                    (`PyQt5.QtCore.QEvent`): Event.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorDataItemWidget )
		assert pytools.assertions.type_is_instance_of( event, PyQt5.QtCore.QEvent )
		
		# Only look at left button presses
		if ( event.button() == PyQt5.QtCore.Qt.LeftButton and self.is_editable ):

			# Convert the color to a qt color
			if ( self.value is not None ):
				color = PyQt5.QtGui.QColor(
					self.value.red,
					self.value.green,
					self.value.blue,
					self.value.alpha
					)
			else:
				color =  PyQt5.QtGui.QColor( 0, 0, 0, 0 )

			# Ask for a new color using QColorDialog
			color = PyQt5.QtWidgets.QColorDialog.getColor(
				initial = color,
				 parent = self,
				  title = 'Pick Color',
				options = PyQt5.QtWidgets.QColorDialog.ShowAlphaChannel
				)


			# If the color didi change
			if ( color.isValid() ):

				# Update internal copy for repainting
				self._value = ColorData( color.red(), color.green(), color.blue(), color.alpha() )

				# Notify color changed
				self.value_changed.emit()

				# Redraw this item widget
				self.repaint()
			
			# if ( color.isValid() )

			# Accept the event
			event.accept()

		else:
			super( ColorDataItemWidget, self ).mousePressEvent( event )

		# if ( event.button() == Qt.LeftButton )

	# def mousePressEvent ( event )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this color data item widget.

		Arguments:
			self (`pydataset.inspector.ColorDataItemWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorDataItemWidget )

		self.setMinimumSize( ColorDataItemWidget.height, ColorDataItemWidget.height )
		self.setMaximumSize( self.maximumWidth(), ColorDataItemWidget.height )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Updates this color data item widget.

		Arguments:
			self (`pydataset.inspector.ColorDataItemWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ColorDataItemWidget )

		# Repaint the widget
		self.repaint()

		# Update the parent class
		super( ColorDataItemWidget, self ).update()

	# def update ( self )	

# class ColorDataItemWidget ( ItemWidget )
