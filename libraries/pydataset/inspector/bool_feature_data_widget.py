# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import BoolFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###       CLASS BOOL-FEATURE-DATA-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'BoolFeatureData' )
class BoolFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.BoolFeatureDataWidget` allows to display/edit `pydataset.dataset.BoolFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.BoolFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.BoolFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.BoolFeatureData`): Bytes feature data to display/edit.
			is_editable                               (`bool`): If `True` value can be edited using the item widget.
			parent          (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), BoolFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( BoolFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this bool feature data widget.

		Arguments:
			self (`pydataset.inspector.BoolFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureDataWidget )

		# Setup the parent class
		super( BoolFeatureDataWidget, self ).setup()

		# Fetch value
		feature_data  = self.value
		feature_value = feature_data.value if feature_data else None

		# Create the bytes item widget to edit/display the value
		self._value_itemwidget = pyui.inspector.BoolItemWidget(
			feature_value,
			False, # Cannot edit dataset items
			self
			)
		# self._value_itemwidget.value_changed.connect( self.value_changed )
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this bytes feature data widget.

		Arguments:
			self (`pydataset.inspector.BoolFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, BoolFeatureDataWidget )

		# Fetch value
		feature_data = self.value
		feature_value = feature_data.value if feature_data else None,

		# Update the value item widget
		self._value_itemwidget.value = feature_value

		# Update the parent class
		super( BoolFeatureDataWidget, self ).update()

	# def update ( self )

# class BoolFeatureDataWidget ( FeatureDataWidget )