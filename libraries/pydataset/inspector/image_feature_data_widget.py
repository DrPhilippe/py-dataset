# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui
import pyui.inspector

# LOCALS
from ..dataset               import ImageFeatureData
from .feature_data_widget    import FeatureDataWidget
from .shape_data_item_widget import ShapeDataItemWidget

# ##################################################
# ###      CLASS IMAGE-FEATURE-DATA-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'ImageFeatureData' )
class ImageFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.ImageFeatureDataWidget` allows to display/edit `pydataset.dataset.ImageFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.ImageFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.ImageFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.ImageFeatureData`): Float feature data to display/edit.
			is_editable                                (`bool`): If `True` value can be edited using the item widget.
			parent           (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ImageFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( ImageFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this image feature data widget.

		Arguments:
			self (`pydataset.inspector.ImageFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureDataWidget )

		# Setup the parent class
		super( ImageFeatureDataWidget, self ).setup()

		# Get the feature data and its value (image, shape, extension+param)
		feature_data = self.value
		image        = feature_data.value     if feature_data else None
		shape        = feature_data.shape     if feature_data else None
		extension    = feature_data.extension if feature_data else None
		param        = feature_data.param     if feature_data else None

		# Resize the image
		image   = pyui.widgets.images_utils.clamp_image_to_size( image, (128, 128) )
		qpixmap = pyui.widgets.images_utils.image_to_qpixmap( image )

		# Display/edit the shape
		self._shape_itemwidget = ShapeDataItemWidget( shape, False, self )
		self.form_layout.addRow( 'Shape', self._shape_itemwidget )

		# Display the image
		self._value_label = PyQt5.QtWidgets.QLabel( self )
		self._value_label.setPixmap( qpixmap )
		self.form_layout.addRow( 'Value', self._value_label )

		# Display the extension
		self._extension_label = PyQt5.QtWidgets.QLabel( str(extension), self )
		self.form_layout.addRow( 'Extension', self._extension_label )

		# Display the param
		self._param_label = PyQt5.QtWidgets.QLabel( str(param), self )
		self.form_layout.addRow( 'Param', self._param_label )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Setup the widgets of used by this image feature data widget.

		Arguments:
			self (`pydataset.inspector.ImageFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ImageFeatureDataWidget )

		# Get the feature data and its value (image, shape, extension+param)
		feature_data = self.value
		image        = feature_data.value     if feature_data else None
		shape        = feature_data.shape     if feature_data else None
		extension    = feature_data.extension if feature_data else None
		param        = feature_data.param     if feature_data else None

		# Resize the image
		image   = pyui.widgets.images_utils.clamp_image_to_size( image, (128, 128) )
		qpixmap = pyui.widgets.images_utils.image_to_qpixmap( image )

		# Update widgets
		self._shape_itemwidget.value = shape
		self._value_label.setPixmap( qpixmap )
		self._extension_label.setText( str(extension) )
		self._param_label.setText( str(param) )

		# Setup the parent class
		super( ImageFeatureDataWidget, self ).update()

	# def setup ( self )

# class ImageFeatureDataWidget ( FeatureDataWidget )