# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..data import RectangleData

# ##################################################
# ###      CLASS RECTANGLE-DATA-ITEM-WIDGET      ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'RectangleData' )
class RectangleDataItemWidget ( pyui.inspector.ItemWidget ):
	"""
	The class `pydataset.inspector.RectangleDataItemWidget` allows to display/edit `pydataset.data.RectangleData`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.Point2DDataItemWidget`.

		Arguments:
			self (`pydataset.inspector.RectangleDataItemWidget`): Instance to initialize.
			value        (`None`/`pydataset.data.RectangleData`): Rectangle data to display/edit.
			is_editable                                 (`bool`): If `True` value can be edited using the item widget.
			parent            (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), RectangleData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Initializes the parent class
		super( RectangleDataItemWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def height_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._height_label

	# def width_label ( self )

	# --------------------------------------------------

	@property
	def width_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._height_spinbox

	# def height_spinbox ( self )

	# --------------------------------------------------

	@property
	def height_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.height

	# def height_value ( self )

	# --------------------------------------------------

	@property
	def rectangle_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.value if self.value else pydataset.data.RectangleData()

	# def rectangle_value ( self )

	# --------------------------------------------------

	@property
	def width_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._width_label

	# def width_label ( self )

	# --------------------------------------------------

	@property
	def width_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._width_spinbox

	# def width_spinbox ( self )

	# --------------------------------------------------

	@property
	def width_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.width

	# def width_value ( self )

	# --------------------------------------------------

	@property
	def x1_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._x1_label

	# def x1_label ( self )

	# --------------------------------------------------

	@property
	def x1_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._x1_spinbox

	# def x1_spinbox ( self )

	# --------------------------------------------------

	@property
	def x1_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.x1

	# def x1_value ( self )

	# --------------------------------------------------

	@property
	def x2_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._x2_label

	# def x2_label ( self )

	# --------------------------------------------------

	@property
	def x2_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._x2_spinbox

	# def x2_spinbox ( self )

	# --------------------------------------------------

	@property
	def x2_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.x2

	# def x2_value ( self )

	# --------------------------------------------------

	@property
	def y1_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._y1_label

	# def y1_label ( self )

	# --------------------------------------------------

	@property
	def y1_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._y1_spinbox

	# def x1_spinbox ( self )

	# --------------------------------------------------

	@property
	def y1_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.y1

	# def y1_value ( self )

	# --------------------------------------------------

	@property
	def y2_label ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._y2_label

	# def y2_label ( self )

	# --------------------------------------------------

	@property
	def y2_spinbox ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self._y2_spinbox

	# def x2_spinbox ( self )

	# --------------------------------------------------

	@property
	def y2_value ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		return self.rectangle_value.y2

	# def y2_value ( self )
	
	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################
	
	# --------------------------------------------------

	def create_label ( self, text ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( text, str )

		label = PyQt5.QtWidgets.QLabel( text, self )
		label.setSizePolicy(
			PyQt5.QtWidgets.QSizePolicy.Preferred,
			PyQt5.QtWidgets.QSizePolicy.Preferred
			)
		label.setMinimumSize( RectangleDataItemWidget.height, RectangleDataItemWidget.height )
		label.setMaximumSize( label.maximumWidth(), RectangleDataItemWidget.height )
		return label
	
	# def create_label ( sefl, text )

	# --------------------------------------------------

	def create_spin_box ( self, value, slot=None ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( value, int )

		spin = PyQt5.QtWidgets.QSpinBox( self )
		spin.setSizePolicy(
			PyQt5.QtWidgets.QSizePolicy.Expanding,
			PyQt5.QtWidgets.QSizePolicy.Preferred
			)
		spin.setMinimumSize( RectangleDataItemWidget.height, RectangleDataItemWidget.height )
		spin.setMaximumSize( spin.maximumWidth(), RectangleDataItemWidget.height )
		spin.setMinimum( 0 )
		spin.setMaximum( 2147483647 )
		spin.setValue( value )

		if ( slot is not None ):
			spin.valueChanged.connect( slot )
			spin.setReadOnly( self.is_not_editable )

		else:
			spin.setReadOnly( True )

		return spin

	# def create_spin_box ( sefl, value, slot )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		layout = PyQt5.QtWidgets.QGridLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# x1 label
		self._x1_label = self.create_label( 'x1' )
		self.layout().addWidget( self._x1_label, 0, 0 )

		# x1 spinbox
		self._x1_spinbox = self.create_spin_box( self.x1_value, self.on_x1_changed )
		self.layout().addWidget( self._x1_spinbox,  0, 1 )

		# x2 label
		self._x2_label = self.create_label( 'x2' )
		self.layout().addWidget( self._x2_label, 0, 2 )

		# x2 spinbox
		self._x2_spinbox = self.create_spin_box( self.x2_value, self.on_x2_changed )
		self.layout().addWidget( self._x2_spinbox,  0, 3 )

		# width label
		self._width_label = self.create_label( 'width' )
		self.layout().addWidget( self._width_label, 0, 4 )

		# width spinbox
		self._width_spinbox = self.create_spin_box( self.width_value )
		self.layout().addWidget( self._width_spinbox, 0, 5 )
		
		# y1 label
		self._y1_label = self.create_label( 'y1' )
		self.layout().addWidget( self._y1_label, 1, 0 )
		
		# y1 spinbox
		self._y1_spinbox = self.create_spin_box( self.y1_value, self.on_y1_changed )
		self.layout().addWidget( self._y1_spinbox,  1, 1 )

		# y2 label
		self._y2_label = self.create_label( 'y2' )
		self.layout().addWidget( self._y2_label, 1, 2 )

		# y2 spinbox
		self._y2_spinbox = self.create_spin_box( self.y2_value, self.on_y2_changed )
		self.layout().addWidget( self._y2_spinbox,  1, 3 )

		# height label
		self._height_label = self.create_label( 'height' )
		self.layout().addWidget( self._height_label, 1, 4 )

		# height spinbox
		self._height_spinbox = self.create_spin_box( self.height_value )
		self.layout().addWidget( self._height_spinbox, 1, 5 )
	
	# def setup ( self )

	# --------------------------------------------------
	
	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )

		self._x1_spinbox.setValue( self.x1_value )
		self._x1_spinbox.setReadOnly( self.is_not_editable )
		self._x2_spinbox.setValue( self.x2_value )
		self._x2_spinbox.setReadOnly( self.is_not_editable )
		self._y1_spinbox.setValue( self.y1_value )
		self._y1_spinbox.setReadOnly( self.is_not_editable )
		self._y2_spinbox.setValue( self.y2_value )
		self._y2_spinbox.setReadOnly( self.is_not_editable )
		self._width_spinbox.setValue( self.width_value )
		self._height_spinbox.setValue( self.height_value )
	
		super( RectangleDataItemWidget, self ).update()
		
	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_x1_changed ( self, x1 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( x1, int )

		# If rectanle is None, create a new rectangle
		if ( self.value is None ):

			self.value = RectangleData( x1, 0, 0, 0 )

		# Otherwise set its properties
		else:			
			self.value.x1 = x1

			# If x1 is greater than x2, also update x2
			if( x1 > self.value.x2 ):

				# in the rectange
				self.value.x2 = x1

				# the display value
				self._x2_spinbox.blockSignals( True )
				self._x2_spinbox.setValue( x1 )
				self._x2_spinbox.blockSignals( False )

			# Update the displayed width
			self._width_spinbox.setValue( self.value.width )

		self.value_changed.emit()

	# def on_x1_changed ( self, x1 )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_y1_changed ( self, y1 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( y1, int )

		# If rectangle is None, create a new rectangle
		if ( self.value is None ):

			self.value = RectangleData( 0, y1, 0, 0 )

		# Otherwise set its properties
		else:			
			self.value.y1 = y1

			# If x1 is greater than x2, also update x2
			if( y1 > self.value.y2 ):

				# in the rectange
				self.value.y2 = y1

				# the display value
				self._y2_spinbox.blockSignals( True )
				self._y2_spinbox.setValue( y1 )
				self._y2_spinbox.blockSignals( False )

			# Update the displayed width
			self._height_spinbox.setValue( self.value.get_height() )
		
		self.value_changed.emit()

	# def on_y1_changed ( self, x1 )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_x2_changed ( self, x2 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( x2, int )

		# If rectangle is None, create a new rectangle
		if ( self.value is None ):

			self.value = RectangleData( 0, 0, x2, 0 )

		# Otherwise set its properties
		else:			
			self.value.x2 = x2

			# If x2 is lower than x1, also update x1
			if( x2 < self.value.x1 ):

				# in the rectange
				self.value.x1 = x2

				# the display value
				self.self._x1_spinbox.blockSignals( True )
				self.self._x1_spinbox.setValue( x2 )
				self.self._x1_spinbox.blockSignals( False )

			# Update the displayed width
			self._width_spinbox.setValue( rect.width )
		
		self.value_changed.emit()

	# def on_x2_changed ( self, x2 )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_y2_changed ( self, y2 ):
		assert pytools.assertions.type_is_instance_of( self, RectangleDataItemWidget )
		assert pytools.assertions.type_is( y2, int )

		# If rectangle is None, create a new rectangle
		if ( self.value is None ):

			self.value = RectangleData( 0, 0, 0, y2 )

		# Otherwise set its properties
		else:			
			self.value.y2 = y2

			# If y2 is lower than y1, also update y1
			if( y2 < self.value.y1 ):

				# in the rectange
				self.value.y1 = y2

				# the display value
				self._y1_spinbox.blockSignals( True )
				self._y1_spinbox.setValue( y2 )
				self._y1_spinbox.blockSignals( False )

			# Update the displayed width
			self._height_spinbox.setValue( self.value.height )
		

		self.value_changed.emit()

	# def on_y2_changed ( self, x1 )

# class RectangleDataItemWidget ( ItemWidget )