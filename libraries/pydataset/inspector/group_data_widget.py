# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import GroupData
from .example_data_widget import ExampleDataWidget

# ##################################################
# ###          CLASS GROUP-DATA-WIDGET           ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'GroupData' )
class GroupDataWidget ( ExampleDataWidget ):
	"""
	The class `pydataset.inspector.GroupDataWidget` allows to display/edit `pydataset.dataset.GroupData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.GroupDataWidget`.

		Arguments:
			self (`pydataset.inspector.GroupDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.GroupData`): Example data to display/edit.
			is_editable                         (`bool`): If `True` value can be edited using the item widget.
			parent    (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), GroupData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( GroupDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this group data widget.

		Arguments:
			self (`pydataset.inspector.GroupDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupDataWidget )

		# Setup the parent class
		super( GroupDataWidget, self ).setup()

		# Fetch value
		group_data     = self.value
		groups_names   = group_data.groups_names   if group_data else None
		examples_names = group_data.examples_names if group_data else None

		# Edit/display groups names
		self._groups_names_itemwidget = pyui.inspector.ListItemWidget(
			groups_names,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Groups', self._groups_names_itemwidget )

		# Edit/display examples names
		self._examples_names_itemwidget = pyui.inspector.ListItemWidget(
			examples_names,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Examples', self._examples_names_itemwidget )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		"""
		Update this group data widget.

		Arguments:
			self (`pydataset.inspector.GroupDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, GroupDataWidget )

		# Fetch value
		group_data     = self.value
		groups_names   = group_data.groups_names   if group_data else None
		examples_names = group_data.examples_names if group_data else None

		# Update the item widgets
		self._groups_names_itemwidget.value   = groups_names
		self._examples_names_itemwidget.value = examples_names

		# Update the parent class
		super( GroupDataWidget, self ).update()

	# def update ( self )

# class GroupDataItemWidget ( ExampleDataWidget )