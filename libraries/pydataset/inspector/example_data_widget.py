# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset         import ExampleData
from .item_data_widget import ItemDataWidget

# ##################################################
# ###         CLASS EXAMPLE-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'ExampleData' )
class ExampleDataWidget ( ItemDataWidget ):
	"""
	The class `pydataset.inspector.ExampleDataWidget` allows to display/edit `pydataset.dataset.ExampleData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.ExampleDataWidget`.

		Arguments:
			self (`pydataset.inspector.ExampleDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.ExampleData`): Example data to display/edit.
			is_editable                           (`bool`): If `True` value can be edited using the item widget.
			parent      (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ExampleData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( ExampleDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this example data widget.

		Arguments:
			self (`pydataset.inspector.ExampleDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleDataWidget )

		# Setup the parent class
		super( ExampleDataWidget, self ).setup()

		# Fetch value
		example_data = self.value
		features_names = example_data.features_names if example_data else None

		# Edit/display features names
		self._features_names_itemwidget = pyui.inspector.ListItemWidget(
			features_names,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Features', self._features_names_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this example data widget.

		Arguments:
			self (`pydataset.inspector.ExampleDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ExampleDataWidget )

		# Fetch value
		example_data = self.value
		features_names = example_data.features_names if example_data else None,

		# Update the features names item widget
		self._features_names_itemwidget.value = features_names

		# Update the parent class
		super( ExampleDataWidget, self ).update()

	# def update ( self )

# class ExampleDataWidget ( ItemWidget )