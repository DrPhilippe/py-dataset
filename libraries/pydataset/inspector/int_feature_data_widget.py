# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.widgets

# LOCALS
from ..dataset            import IntFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###         CLASS FEATURE-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'IntFeatureData' )
class IntFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.IntFeatureDataWidget` allows to display/edit `pydataset.dataset.IntFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.IntFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.IntFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.IntFeatureData`): Float feature data to display/edit.
			is_editable                              (`bool`): If `True` value can be edited using the item widget.
			parent         (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), IntFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( IntFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this int feature data widget.

		Arguments:
			self (`pydataset.inspector.IntFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureDataWidget )

		# Setup the parent class
		super( IntFeatureDataWidget, self ).setup()
		
		# Fetch value
		feature_data = self.value
		feature_value = feature_data.value if feature_data else None

		# Create the int item widget to edit/display the value
		self._value_itemwidget = pyui.inspector.IntItemWidget(
			feature_value,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this int feature data widget.

		Arguments:
			self (`pydataset.inspector.IntFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, IntFeatureDataWidget )

		# Fetch value
		feature_data = self.value
		feature_value = feature_data.value if feature_data else None
		
		# Update the value item widget
		self._value_itemwidget.value = feature_value

		# Update the parent class
		super( IntFeatureDataWidget, self ).update()

	# def update ( self )

# class IntFeatureDataWidget ( FeatureDataWidget )