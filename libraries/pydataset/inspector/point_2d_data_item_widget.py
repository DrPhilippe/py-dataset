# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..data import Point2DData

# ##################################################
# ###      CLASS POINT-2D-DATA-ITEM-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'Point2DData' )
class Point2DDataItemWidget ( pyui.inspector.ItemWidget ):
	"""
	The class `pydataset.inspector.Point2DDataItemWidget` allows to display/edit `pydataset.data.Point2DData`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.Point2DDataItemWidget`.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Instance to initialize.
			value        (`None`/`pydataset.data.Point2DData`): Color data to display/edit.
			is_editable                               (`bool`): If `True` value can be edited using the item widget.
			parent          (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), Point2DData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )
		
		# Initializes the parent class
		super( Point2DDataItemWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def point_2d_value ( self ):
		"""
		Point2D value editied/displayed by this widget (`pydataset.data.Point2DData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		# If None, create a new Point2D
		return self.value if self.value else Point2DData()

	# def point_2d_value ( self )

	# --------------------------------------------------

	@property
	def x_label ( self ):
		"""
		Label used to name X value spinbox (`PyQt5.QtWidgets.QLabel`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self._x_label

	# def x_label ( self )

	# --------------------------------------------------

	@property
	def x_spinbox ( self ):
		"""
		Spin-box used to edit the X value (`PyQt5.QtWidgets.QSpinBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self._x_spinbox

	# def x_spinbox ( self )

	# --------------------------------------------------

	@property
	def x_value ( self ):
		"""
		Point2D X value editied/displayed by this widget (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self.point_2d_value.x

	# def x_value ( self )

	# --------------------------------------------------

	@property
	def y_label ( self ):
		"""
		Label used to name Y value spinbox (`PyQt5.QtWidgets.QLabel`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self._y_label

	# def y_label ( self )

	# --------------------------------------------------
	
	@property
	def y_spinbox ( self ):
		"""
		Spin-box used to edit the Y value (`PyQt5.QtWidgets.QSpinBox`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self._y_spinbox

	# def y_spinbox ( self )

	# --------------------------------------------------

	@property
	def y_value ( self ):
		"""
		Point2D Y value editied/displayed by this widget (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		return self.point_2d_value.y

	# def y_value ( self )
	
	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################
	
	# --------------------------------------------------

	def create_label ( self, text ):
		"""
		Creates a new label to be used in this `pydataset.inspector.Point2DDataItemWidget`.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Widget using the label.
			text                                       (`str`): Text displayed in the label.

		Returns:
			`PyQt5.QtWidgets.QLabel`: The new label.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )
		assert pytools.assertions.type_is( text, str )

		label = PyQt5.QtWidgets.QLabel( text, self )
		label.setMinimumSize( label.minimumWidth(), Point2DDataItemWidget.height )
		label.setMaximumSize( Point2DDataItemWidget.height, Point2DDataItemWidget.height )
		return label
	
	# def create_label ( sefl, text )

	# --------------------------------------------------

	def create_spin_box ( self, value, slot=None ):
		"""
		Creates a new spin-box to be used in this `pydataset.inspector.Point2DDataItemWidget`.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Widget using the spin-box.
			value                                      (`int`): Value edited/displayed in the spin-box.
			slot                                  (`callable`): Slot invoked when the value is edited.

		Returns:
			`PyQt5.QtWidgets.QSpinBox`: The new spin-box.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )
		assert pytools.assertions.type_is( value, int )

		spin = PyQt5.QtWidgets.QSpinBox( self )
		spin.setMinimumSize( Point2DDataItemWidget.height, Point2DDataItemWidget.height )
		spin.setMaximumSize( spin.maximumWidth(), Point2DDataItemWidget.height )
		spin.setMinimum( -2147483648 )
		spin.setMaximum( 2147483647 )
		spin.setValue( value )
		spin.setReadOnly( self.is_not_editable )

		if ( slot is not None ):
			spin.valueChanged.connect( slot )
		else:
			spin.setEnabled( False )

		return spin

	# def create_spin_box ( sefl, value, slot )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this point2D data item widget.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		# Layout
		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		self.setLayout( layout )

		# Layout / X label
		self._x_label = self.create_label( 'x' )
		layout.addWidget( self._x_label, 0 )

		# Layout / X spin-box
		self._x_spinbox = self.create_spin_box( self.x_value, self.on_x_changed )
		layout.addWidget( self._x_spinbox, 1 )

		# Layout / Y label
		self._y_label = self.create_label( 'y' )
		layout.addWidget( self._y_label, 0 )

		# Layout / Y spin-box
		self._y_spinbox = self.create_spin_box( self.y_value, self.on_y_changed )
		layout.addWidget( self._y_spinbox, 1 )

	# def setup ( self )

	# --------------------------------------------------
	
	def update ( self ):
		"""
		Update this point2D data item widget.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )

		# Set the value of both spin box
		self._x_spinbox.setValue( self.x_value )
		self._x_spinbox.setReadOnly( self.is_not_editable )
		self._y_spinbox.setValue( self.y_value )
		self._y_spinbox.setReadOnly( self.is_not_editable )

		# Update the parent class
		super( Point2DDataItemWidget, self ).update()
	
	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_x_changed ( self, x ):
		"""
		Slot invoked when the value of X is edited.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Point2DDataItewWidget instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )
		assert pytools.assertions.type_is( x, int )

		# Fetch the point 2D
		point_2d = self.value 

		# If its None, create a new point 2D
		if ( point_2d is None ):

			self.value = Point2DData( x, 0, 0, 0 )

		# Otherwise set its properties
		else:			
			point_2d.x = x	

		self.value_changed.emit()

	# def on_x_changed ( self, x )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_y_changed ( self, y ):
		"""
		Slot invoked when the value of Y is edited.

		Arguments:
			self (`pydataset.inspector.Point2DDataItemWidget`): Point2DDataItewWidget instance.
		"""
		assert pytools.assertions.type_is_instance_of( self, Point2DDataItemWidget )
		assert pytools.assertions.type_is( y, int )

		# Fetch the point 2D
		point_2d = self.value 

		# If its None, create a new point 2D
		if ( point_2d is None ):

			self.value = Point2DData( 0, y, 0, 0 )

		# Otherwise set its properties
		else:			
			point_2d.y = y

		self.value_changed.emit()

	# def on_y_changed ( self, x )

# class Point2DDataItemWidget ( ItemWidget )