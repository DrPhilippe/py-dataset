# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset         import FeatureData
from .item_data_widget import ItemDataWidget

# ##################################################
# ###         CLASS FEATURE-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'FeatureData' )
class FeatureDataWidget ( ItemDataWidget ):
	"""
	The class `pydataset.inspector.BytesFeatureDataWidget` allows to display/edit `pydataset.dataset.BytesFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.FeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.FeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.FeatureData`): Feature data to display/edit.
			is_editable                           (`bool`): If `True` value can be edited using the item widget.
			parent      (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, FeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), FeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( FeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

# class FeatureDataWidget ( ItemDataWidget )