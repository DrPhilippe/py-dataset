# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import TextFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###       CLASS TEXT-FEATURE-DATA-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'TextFeatureData' )
class TextFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.TextFeatureDataWidget` allows to display/edit `pydataset.dataset.TextFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.TextFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.TextFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.TextFeatureData`): Float feature data to display/edit.
			is_editable                               (`bool`): If `True` value can be edited using the item widget.
			parent          (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), TextFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( TextFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this float feature data widget.

		Arguments:
			self (`pydataset.inspector.TextFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureDataWidget )

		# Setup the parent class
		super( TextFeatureDataWidget, self ).setup()

		# Create the text item widget to edit/display the value
		self._value_itemwidget = pyui.inspector.TextItemWidget(
			self.value.value if self.value is not None else None,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this float feature data widget.

		Arguments:
			self (`pydataset.inspector.TextFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, TextFeatureDataWidget )

		# Update the value item widget
		self._value_itemwidget.value = self.value.value if self.value is not None else None

		# Update the parent class
		super( TextFeatureDataWidget, self ).update()

	# def update ( self )

# class TextFeatureDataWidget ( FeatureDataWidget )