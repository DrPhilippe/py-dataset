# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets
import re

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..data import ShapeData

# ##################################################
# ###        CLASS SHAPE-DATA-ITEM-WIDGET        ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'ShapeData' )
class ShapeDataItemWidget ( pyui.inspector.ItemWidget ):

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################
	
	# --------------------------------------------------
	
	_regex = re.compile( r"([0-9]+(x[0-9]+)*)|None" )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ShapeData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( ShapeDataItemWidget, self ).__init__( value, is_editable, parent )

	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def lineedit ( self ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )

		return self._lineedit

	# def lineedit ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )

		# create the layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 );
		self.setLayout( layout )

		# Create the line edit to display the path
		self._lineedit = PyQt5.QtWidgets.QLineEdit( self.text_value )
		self._lineedit.setMinimumSize( ShapeDataItemWidget.height, ShapeDataItemWidget.height )
		self._lineedit.setMaximumSize( self._lineedit.maximumWidth(), ShapeDataItemWidget.height )
		self._lineedit.textChanged.connect( self.on_text_changed )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )
		layout.addWidget( self._lineedit )

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )

		self._lineedit.setText( self.text_value )
		self._lineedit.setReadOnly( not self.isEnabled() or self.is_not_editable )

		super( ShapeDataItemWidget, self ).update()
		
	# def update ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_text_changed ( self, text ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )
		assert pytools.assertions.type_is( text, str )

		# reset the text color to black
		self.setStyleSheet( '' )
	
		# if the text is empty
		if ( not text ):

			self._value = ShapeData()
			self.value_changed.emit()

		# if dimensions are specified
		elif ( ShapeDataItemWidget._regex.fullmatch( text ) != None ):
			
			if ( text == 'None' ):
				self._value = None
				self.value_changed.emit()

			else:
				# split the txt
				txt_dims = text.split( 'x' )

				# parse each dim
				dims = []
				for txt_dim in txt_dims:

					if ( len( txt_dim ) > 0 ):
					
						dim = int( txt_dim )
						dims.append( dim )

					# if ( len( txt_dim ) > 0 )

				# for txt_dim in txt_dims:

				self._value = ShapeData.from_list( dims )

				# notify of the modification
				self.value_changed.emit()

		# unrecognized shape
		else:
			self.setStyleSheet( 'color: red' )
		
		# if ( ShapeDataItemWidget._regex.fullmatch( text ) != None )

	# def on_text_changed ( self, text )

# class ShapeItemWidget ( ItemWidget )
