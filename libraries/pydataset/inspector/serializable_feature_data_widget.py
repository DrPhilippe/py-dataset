# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import SerializableFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###         CLASS FEATURE-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'SerializableFeatureData' )
class SerializableFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.SerializableFeatureDataWidget` allows to display/edit `pydataset.dataset.SerializableFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.SerializableFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.SerializableFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.SerializableFeatureData`): Float feature data to display/edit.
			is_editable                                       (`bool`): If `True` value can be edited using the item widget.
			parent                  (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, SerializableFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), SerializableFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( SerializableFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def clear ( self ):

		if self._value_itemwidget:
			self.form_layout.removeRow( self._value_itemwidget )
			self._value_itemwidget.deleteLater()
			self._value_itemwidget = None

	# def clear ( self )

	# --------------------------------------------------

	def populate ( self ):
		
		# Get the feature data and its value
		feature_data = self.value
		value        = feature_data.value if feature_data is not None else None

		# Create the widget used to display the value
		self._value_itemwidget = pyui.inspector.ItemWidgetFactory.instantiate_widget( type(value), value=value, is_editable=self.is_editable, parent=self )
		
		# If no widget exists
		if not self._value_itemwidget:

			# Replace it by an info label
			self._value_itemwidget = PyQt5.QtWidgets.QLabel( '{} (cannot display this)'.format(type(value).__name__) )

		# Add the widget to the form layout
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def populate ( self ):

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this serializable feature data widget.

		Arguments:
			self (`pydataset.inspector.SerializableFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, SerializableFeatureDataWidget )

		# Setup the parent class
		super( SerializableFeatureDataWidget, self ).setup()

		self.populate()

	# def setup ( self )
	
	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, ShapeDataItemWidget )

		self.clear()
		self.populate()

		super( ShapeDataItemWidget, self ).update()
		
	# def update ( self )

# class SerializableFeatureDataWidget ( FeatureDataWidget )