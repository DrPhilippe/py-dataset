# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset          import DatasetData
from .group_data_widget import GroupDataWidget

# ##################################################
# ###         CLASS DATASET-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'DatasetData' )
class DatasetDataWidget ( GroupDataWidget ):
	"""
	The class `pydataset.inspector.DatasetDataWidget` allows to display/edit `pydataset.dataset.DatasetData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.DatasetDataWidget`.

		Arguments:
			self (`pydataset.inspector.DatasetDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.DatasetData`): Dataset data to display/edit.
			is_editable                           (`bool`): If `True` value can be edited using the item widget.
			parent      (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, DatasetDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), DatasetData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( DatasetDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )
	
# class DatasetDataWidget ( ItemWidget )