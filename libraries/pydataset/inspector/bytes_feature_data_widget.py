# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import BytesFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###      CLASS BYTES-FEATURE-DATA-WIDGET       ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'BytesFeatureData' )
class BytesFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.BytesFeatureDataWidget` allows to display/edit `pydataset.dataset.BytesFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.BytesFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.BytesFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.BytesFeatureData`): Bytes feature data to display/edit.
			is_editable                                (`bool`): If `True` value can be edited using the item widget.
			parent           (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), BytesFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( BytesFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the widgets of used by this bytes feature data widget.

		Arguments:
			self (`pydataset.inspector.BytesFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureDataWidget )

		# Setup the parent class
		super( BytesFeatureDataWidget, self ).setup()

		# Fetch value
		feature_data  = self.value
		feature_value = feature_data.value if feature_data else None

		# Create the bytes item widget to edit/display the value
		self._value_itemwidget = pyui.inspector.BytesItemWidget(
			feature_value,
			False, # Cannot edit dataset items
			self
			)
		self._value_itemwidget.value_changed.connect( self.value_changed )
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this bytes feature data widget.

		Arguments:
			self (`pydataset.inspector.BytesFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, BytesFeatureDataWidget )

		# Fetch value
		feature_data = self.value
		feature_value = feature_data.value if feature_data else None,

		# Update the value item widget
		self._value_itemwidget.value = feature_value

		# Update the parent class
		super( BytesFeatureDataWidget, self ).update()

	# def update ( self )

# class BytesFeatureDataWidget ( FeatureDataWidget )