# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset import ItemData

# ##################################################
# ###           CLASS ITEM-DATA-WIDGET           ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'ItemData' )
class ItemDataWidget ( pyui.inspector.ItemWidget ):
	"""
	The class `pydataset.inspector.ItemDataWidget` allows to display/edit `pydataset.dataset.ItemData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.ItemDataWidget`.

		Arguments:
			self (`pydataset.inspector.ItemDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.ItemData`): Example data to display/edit.
			is_editable                        (`bool`): If `True` value can be edited using the item widget.
			parent   (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), ItemData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the parent class
		super( ItemDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def form_layout ( self ):
		"""
		Form layout used layout the widgets of this item data widget (`PyQt5.QtWidgets.QFormLayout`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		return self._form_layout

	# def form_layout ( self )

	# --------------------------------------------------

	@property
	def name_lineedit ( self ):
		"""
		Line-edit used to display the name of the item (`PyQt5.QtWidgets.QLineEdit`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		return self._name_lineedit

	# def name_lineedit ( self )

	# --------------------------------------------------

	@property
	def parent_url_lineedit ( self ):
		"""
		Line-edit used to display the parent url of the item (`PyQt5.QtWidgets.QLineEdit`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		return self._parent_url_lineedit

	# def parent_url_lineedit ( self )

	# --------------------------------------------------

	@property
	def url_title_label ( self ):
		"""
		Title label used to display the url of the item (`pyui.widgets.TitleLabel`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		return self._url_title_label

	# def url_title_label ( self )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup this item data widget.

		Arguments:
			self (`pydataset.inspector.ItemDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		# Setup parent
		super( ItemDataWidget, self ).setup()

		# Fetch values
		item_data       = self.value
		item_url        = item_data.url        if item_data else ''
		item_name       = item_data.name       if item_data else ''
		item_parent_url = item_data.parent_url if item_data else ''

		# Layout of this item widget
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Display the URL in a title label
		self._url_title_label = pyui.widgets.TitleLabel( item_url, self )
		layout.addWidget( self._url_title_label )

		# Form layout used to display other properties
		self._form_layout = PyQt5.QtWidgets.QFormLayout()
		self._form_layout.setContentsMargins( 5, 5, 5, 5 )
		self._form_layout.setSpacing( 5 )
		self._form_layout.setHorizontalSpacing( 5 )
		layout.addLayout( self._form_layout )

		# Form layout / Name line-edit
		self._name_lineedit = PyQt5.QtWidgets.QLineEdit( item_name, self )
		self._name_lineedit.setReadOnly( True ) # Cannot edit dataset items
		self._form_layout.addRow( 'Name', self._name_lineedit )

		# Form layout / Parent URL line-edit
		self._parent_url_lineedit = PyQt5.QtWidgets.QLineEdit( item_parent_url, self )
		self._parent_url_lineedit.setReadOnly( True ) # Cannot edit dataset items
		self._form_layout.addRow( 'Parent URL', self._parent_url_lineedit )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this item data widget.

		Arguments:
			self (`pydataset.inspector.ItemDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, ItemDataWidget )

		# Fetch values
		item_data       = self.value
		item_url        = item_data.url        if item_data else ''
		item_name       = item_data.name       if item_data else ''
		item_parent_url = item_data.parent_url if item_data else ''

		# Update the widget
		self._url_title_label.setText( item_url )
		self._name_lineedit.setText( item_name )
		self._parent_url_lineedit.setText( item_parent_url )

		# Update the parent class
		super( ItemDataWidget, self ).update()

	# def update ( self )

# class ItemDataWidget ( ItemWidget )