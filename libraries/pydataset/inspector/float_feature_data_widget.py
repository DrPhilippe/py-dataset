# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.inspector

# LOCALS
from ..dataset            import FloatFeatureData
from .feature_data_widget import FeatureDataWidget

# ##################################################
# ###         CLASS FEATURE-DATA-WIDGET          ###
# ##################################################

@pyui.inspector.RegisterItemWidgetAttribute( 'FloatFeatureData' )
class FloatFeatureDataWidget ( FeatureDataWidget ):
	"""
	The class `pydataset.inspector.FloatFeatureDataWidget` allows to display/edit `pydataset.dataset.FloatFeatureData`.
	"""

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, is_editable=True, parent=None ):
		"""
		Initializes a new instance of the class `pydataset.inspector.FloatFeatureDataWidget`.

		Arguments:
			self (`pydataset.inspector.FloatFeatureDataWidget`): Instance to initialize.
			value (`None`/`pydataset.dataset.FloatFeatureData`): Float feature data to display/edit.
			is_editable                                (`bool`): If `True` value can be edited using the item widget.
			parent           (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureDataWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), FloatFeatureData) )
		assert pytools.assertions.type_is( is_editable, bool )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initializes the parent class
		super( FloatFeatureDataWidget, self ).__init__( value, is_editable, parent )
	
	# def __init__ ( self, value, is_editable, parent )

	# ##################################################
	# ###                 METHODS                    ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the inspector of used by this float feature data widget.

		Arguments:
			self (`pydataset.inspector.FloatFeatureDataWidget`): Instance to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureDataWidget )

		# Setup the parent class
		super( FloatFeatureDataWidget, self ).setup()

		# Fetch value
		feature_data  = self.value
		feature_value = feature_data.value if feature_data else None
		
		# Create the float item widget to edit/display the value
		self._value_itemwidget = pyui.inspector.FloatItemWidget(
			feature_value,
			False, # Cannot edit dataset items
			self
			)
		self.form_layout.addRow( 'Value', self._value_itemwidget )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		"""
		Update this float feature data widget.

		Arguments:
			self (`pydataset.inspector.FloatFeatureDataWidget`): Instance to update.
		"""
		assert pytools.assertions.type_is_instance_of( self, FloatFeatureDataWidget )

		# Fetch value
		feature_data  = self.value
		feature_value = feature_data.value if feature_data else None
		
		# Update the value item widget
		self._value_itemwidget.value = feature_value

		# Update the parent class
		super( FloatFeatureDataWidget, self ).update()

	# def update ( self )

# class FloatFeatureDataWidget ( FeatureDataWidget )