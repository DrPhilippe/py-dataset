# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy

# INTERNALS
import pytools.assertions

# LOCALS
from .data import RectangleData

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def crop_image ( image, rectangle ):
	"""
	Crops out the given rectangle in the given image.
	
	The image must be an array with either 2 or 3 dimensions.

	Arguments:
		image      (`numpy.ndarray`): Image to crop.
		rectangle (`Data.Rectangle`): Rectangle to crop out.

	Returns:
		`numpy.ndarray`: The crop.

	Raises:
		`ValueError`: If the image shape is invalid.
	"""
	assert pytools.assertions.type_is( image, numpy.ndarray )
	assert pytools.assertions.type_is( rectangle, RectangleData )

	rank = len( image.shape )

	if ( rank == 2 ):
		return image[ int(rectangle.y1) : int(rectangle.y2), int(rectangle.x1) : int(rectangle.x2) ]

	elif ( rank == 3 ):
		return image[ int(rectangle.y1) : int(rectangle.y2), int(rectangle.x1) : int(rectangle.x2), : ]

	else:
		raise ValueError( 'Cannot crop images of rank {} with a Rectangle'.format(rank) )

# def crop_image ( image, rectangle )

# --------------------------------------------------

def mask_image ( image, mask, method='linear' ):
	"""
	Masks the pixels of the image with the given mask.
	
	The image must be of type uint8 and the mask float32
	and they must have the same width and height.
	
	Arguments:
		image (`numpy.ndarray`): Image to mask.
		mask  (`numpy.ndarray`): Mask to apply.
		method          (`str`): Masking method {'linear', 'binaray'}.

	Returns:
		`numpy.ndarray`: The masked image.
	"""
	assert pytools.assertions.type_is( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )
	assert pytools.assertions.type_is( mask, numpy.ndarray )
	assert pytools.assertions.equal( mask.dtype, numpy.float32 )
	assert pytools.assertions.equal( image.shape[1], mask.shape[1] )
	assert pytools.assertions.equal( image.shape[0], mask.shape[0] )
	assert pytools.assertions.type_is( method, str )
	assert pytools.assertions.value_in( method, ['linear', 'binary'] )

	shape = image.shape
	image = image.astype( numpy.float32 )

	# Normalize the mask ?
	if ( numpy.amax(mask) > 1.0 ):
		mask = mask / numpy.amax(mask)
	
	# Binarize mask ?
	if ( method == 'binary' ):
		mask = numpy.where( mask != 0.0, numpy.ones(mask.shape, numpy.float32), numpy.zeros(mask.shape, numpy.float32) )

	# Repeat mask for each channel of the image
	if ( len(shape) == 3 ):
		mask = numpy.reshape( mask, [shape[0], shape[1], 1] )
		mask = numpy.repeat( mask, shape[2], axis=2 )

	# Apply mask
	image = image * mask

	# Return image
	return image.astype( numpy.uint8 )

# def mask_image ( image, mask )

# --------------------------------------------------

def past_image ( foreground, background, mask, method='linear' ):
	"""
	Pasts the pixels of foreground image onto the backgound.

	Arguments:
		foreground (`numpy.ndarray`): Foreground image.
		background (`numpy.ndarray`): Background image.
		mask       (`numpy.ndarray`): Foreground mask.
		method               (`str`): Masking method {'linear', 'binaray'}.

	Returns:
		`numpy.ndarray`: The new image.
	"""
	assert pytools.assertions.type_is( foreground, numpy.ndarray )
	assert pytools.assertions.equal(  foreground.dtype, numpy.uint8 )
	assert pytools.assertions.type_is( background, numpy.ndarray )
	assert pytools.assertions.equal( background.dtype, numpy.uint8 )
	assert pytools.assertions.type_is( mask, numpy.ndarray )
	assert pytools.assertions.equal( mask.dtype, numpy.float32 )
	assert pytools.assertions.equal( foreground.shape[1], mask.shape[1] )
	assert pytools.assertions.equal( foreground.shape[0], mask.shape[0] )
	assert pytools.assertions.equal( background.shape[1], mask.shape[1] )
	assert pytools.assertions.equal( background.shape[0], mask.shape[0] )
	assert pytools.assertions.type_is( method, str )
	assert pytools.assertions.value_in( method, ['linear', 'binary'] )

	depth = foreground.shape[ 2 ] if len( foreground.shape ) > 2 else 1
	dtype = foreground.dtype

	# Convert all images to rgba and to float32
	foreground = to_rgba( foreground ).astype( numpy.float32 )
	background = to_rgba( background ).astype( numpy.float32 )

	# Normalize the mask ?
	if ( numpy.amax(mask) > 1.0 ):
		mask = mask / numpy.amax(mask)
	
	# Repeat mask for each channel of the image
	mask = numpy.reshape( mask, [mask.shape[0], mask.shape[1], 1] )
	mask = numpy.repeat( mask, 4, axis=2 )

	# find non-zero mask pixels
	indexes = mask != 0.0

	# Past
	if ( method == 'binary' ):

		# Binarize mask
		mask = numpy.where( mask != 0.0, numpy.ones(mask.shape, numpy.float32), numpy.zeros(mask.shape, numpy.float32) )

		# Copy image
		image = numpy.copy( background )

		# Past foreground in it
		image[ indexes ] = foreground[ indexes ]

	else:
		# Copy image
		image = numpy.copy( background )

		# Create negative mask for background
		neg_mask = numpy.ones( [mask.shape[0], mask.shape[1], 4], dtype=numpy.float32 ) - mask

		# Past mask*foreground + neg_mask*background in it
		image[ indexes ] = image[ indexes ] * neg_mask[ indexes ] + foreground[ indexes ] * mask[ indexes ]


	# Convert back the image in the foreground depth and type
	if ( depth == 1 ):
		image = image[ :, :, 0 ]
	elif ( depth == 3 ):
		image = image[ :, :, 0:3 ]
	image = image.astype( dtype )
	
	# Return image
	return image

# def past_image ( image, mask )

# --------------------------------------------------

def to_rgba ( image ):
	"""
	Converts the image to RGBA.

	Arguments:
		image (`numpy.ndarray`): Image to convert.

	Returns:
		`numpy.ndarray`: The new RGBA image.
	"""
	assert pytools.assertions.type_is( image, numpy.ndarray )
	assert pytools.assertions.equal( image.dtype, numpy.uint8 )

	if ( len(image.shape) == 3 and image.shape[2] == 4 ):
		return image

	# grayscale HxW ?
	if ( len(image.shape) == 2 ):

		# HxW -> HxWx0
		image = numpy.reshape( image, (image.shape[0], image.shape[1], 1) )

	# grayscale HxWx1 ?
	if ( image.shape[2] == 1 ):

		# HxWx1 -> HxWx3
		image = numpy.repeat( image, 3, axis=2 )


	# grayscale HxWx3 ?
	if ( image.shape[2] == 3 ):

		# Create alpha channel
		one  = 255.0 if numpy.amax(image) > 1.0 else 1.0
		ones = numpy.ones( (image.shape[0], image.shape[1], 1), dtype=image.dtype ) * one

		# HxWx3 -> HxWx4
		image = numpy.concatenate( (image, ones), axis=2 )

	return image
	
# def to_rgba ( image )