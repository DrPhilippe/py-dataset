# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import transforms3d

# INTERNALS
import pytools.assertions

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def affine_transform ( R, t=numpy.zeros([3],dtype=numpy.float32) ):
	"""
	Converts the given `numpy.ndarray` to a `PyQt5.QtGui.QMatrix4x4`.
	
	The numpy ndarray must be of dtype float32 and shape [3, 3] and [3] respectively.
	The first is a rotation matrix and the second a translation vector.

	Arguments:
		ndarray (`numpy.ndarray`): NumPy n-dimensional array to convert.

	Returns:
		`numpy.ndarrary`: The matrix.
	"""
	T = numpy.eye( 4, dtype=numpy.float32 )
	T[:3, :3] = R
	T[:3,  3] = t.ravel()
	return T

# def affine_transform ( R, t )

# --------------------------------------------------

def invert_xy ():
	"""
	Transform that invberts the X and Y axis.

	Returns:
		`numpy.ndarrary`: The transform matrix.
	"""
	T = numpy.eye( 4, dtype=numpy.float32 )
	T[1, 1] = -1
	T[2, 2] = -1
	return T

# def invert_xy ()

# --------------------------------------------------

def project_points ( points, K=None, dist_coeffs=None, r=None, t=None ):
	assert pytools.assertions.type_is_instance_of( points, numpy.ndarray )

	# Do we need to project ?
	if K is not None:
				
		# Optional values
		if dist_coeffs is None:
			dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )
		if r is None:
			r = numpy.zeros( [3], dtype=numpy.float32 )
		if t is None:
			t = numpy.zeros( [3], dtype=numpy.float32 )

		# Convert rotation to euler angles
		r = convert_rotation_to_eulers_if_needed( r )
		
		# Project the axes points
		points, jacobian = cv2.projectPoints( points, r, t, K, dist_coeffs )
		points = numpy.reshape( points, [-1, 2] )
	
	# if K is not None

	return points

# def project_points ( ... )

# --------------------------------------------------

def place_points ( points, r, t ):

	count = points.shape[ 0 ]

	# Convert points to homogeneous coordinates
	points = numpy.hstack(
		(points, numpy.ones([count,1], dtype=numpy.float32 )),
		)

	# Convert rotation to a matrix if necessary
	R = convert_rotation_to_matrix_if_needed( r )

	# Compose the affine transform
	T = affine_transform( R, t )

	# Project points in camera space
	points = numpy.matmul( points, T.transpose() )

	# Discar the homogeneous coordinates
	points = points[ :, 0:3 ]
	points = numpy.reshape( points, [count, 3] )
	return points

# --------------------------------------------------

def convert_rotation_to_eulers_if_needed ( rotation ):
	"""
	Converts any rotation to an euler angle.
	
	Input rotation can be:
		3   values vector for an euler angle
		4   values vector for a quaternion
		3x3 values matrix for a rotation matrix

	Arguments:
		rotation (`numpy.ndarray`): Input rotation

	Returns:
		`numpy.ndarray`: Euler angle.
	"""
	assert pytools.assertions.type_is( rotation, numpy.ndarray )
	
	shape = list(rotation.shape)

	# Euler angle
	if shape == [3] or shape == [1, 3] or shape == [3, 1]:
		return rotation
	
	# Quaternion angle
	elif shape == [4] or shape == [1, 4] or shape == [4, 1]:
		rotation    = transforms3d.quaternions.quat2mat( rotation )
		rotation, j = cv2.Rodrigues( rotation )
		rotation    = numpy.reshape( rotation, [3] ).astype(numpy.float32)
		return rotation
	
	# Rotation matrix angle
	elif shape == [3, 3]:
		rotation, j = cv2.Rodrigues( rotation )
		rotation    = numpy.reshape( rotation, [3] ).astype(numpy.float32)
		return rotation

	else:
		raise ValueError( 'Unsupported rotation shape: {}'.format(shape) )

# def convert_rotation_to_eulers_if_needed ( rotation )

# --------------------------------------------------

def convert_rotation_to_matrix_if_needed ( rotation ):
	"""
	Converts any rotation to a rotation matrix.
	
	Input rotation can be:
		3   values vector for an euler angle
		4   values vector for a quaternion
		3x3 values matrix for a rotation matrix

	Arguments:
		rotation (`numpy.ndarray`): Input rotation

	Returns:
		`numpy.ndarray`: Rotation matrix.
	"""
	assert pytools.assertions.type_is( rotation, numpy.ndarray )
	
	shape = list(rotation.shape)

	# Euler angle
	if shape == [3] or shape == [1, 3] or shape == [3, 1]:
		rotation, j = cv2.Rodrigues( rotation )
		return rotation
	
	# Quaternion angle
	elif shape == [4] or shape == [1, 4] or shape == [4, 1]:
		rotation = transforms3d.quaternions.quat2mat( rotation )
		rotation = numpy.reshape( rotation, [3, 3] )
		return rotation
	
	# Rotation matrix angle
	elif shape == [3, 3]:
		return rotation

	else:
		raise ValueError( 'Unsupported rotation shape: {}'.format(shape) )

# def convert_rotation_to_matrix_if_needed ( rotation )

# --------------------------------------------------

def normalize_images_points ( points, image_width, image_height, target_min, target_max ):
	assert pytools.assertions.type_is( points, numpy.ndarray )
	assert pytools.assertions.equal( points.dtype, numpy.float32 )
	assert pytools.assertions.type_in( image_width, (int, float) )
	assert pytools.assertions.type_in( image_height,(int,  float) )
	assert pytools.assertions.type_in( target_min, (int, float) )
	assert pytools.assertions.type_in( target_max,(int,  float) )
	image_width  = float(image_width)
	image_height = float(image_height)
	target_min   = float(target_min)
	target_max   = float(target_max)

	target_len = target_max - target_min

	points = numpy.math.divide( points, numpy.asarray([float(image_width), float(image_height)], dtype=numpy.float32) )
	points = numpy.math.multiply( points, numpy.asarray([target_len, target_len], dtype=numpy.float32) )
	points = numpy.math.add( points, numpy.asarray([target_min, target_min], dtype=numpy.float32) )
	
	return points
	
# def normalize_images_points ( ... )

# --------------------------------------------------

def rescale_image_points ( points, image_width, image_height, target_min, target_max ):
	assert pytools.assertions.type_is( points, numpy.ndarray )
	assert pytools.assertions.equal( points.dtype, numpy.float32 )
	assert pytools.assertions.type_in( image_width, (int, float) )
	assert pytools.assertions.type_in( image_height,(int,  float) )
	assert pytools.assertions.type_in( target_min, (int, float) )
	assert pytools.assertions.type_in( target_max,(int,  float) )
	image_width  = float(image_width)
	image_height = float(image_height)
	target_min   = float(target_min)
	target_max   = float(target_max)

	target_len = target_max - target_min

	points = numpy.subtract( points, numpy.asarray([target_min, target_min], dtype=numpy.float32) )
	points = numpy.divide( points, numpy.asarray([target_len, target_len], dtype=numpy.float32) )
	points = numpy.multiply( points, numpy.asarray([image_width, image_height], dtype=numpy.float32) )

	return points

# def rescale_image_points ( points, image_shape )

# --------------------------------------------------

def normalize_control_points ( points, image_width, image_height ):
	assert pytools.assertions.type_is( points, numpy.ndarray )
	assert pytools.assertions.type_is( image_width, int )
	assert pytools.assertions.type_is( image_height, int )
	
	points = numpy.divide( points, numpy.asarray([image_width, image_height], dtype=numpy.float32) )
	points = numpy.multiply( points, numpy.asarray([2.0, 2.0], dtype=numpy.float32) )
	points = numpy.subtract( points, numpy.asarray([1.0, 1.0], dtype=numpy.float32) )
	return points

# def normalize_control_points ( points, image_shape )

# --------------------------------------------------

def rescale_control_points ( points, image_width, image_height ):
	assert pytools.assertions.type_is( points, numpy.ndarray )
	assert pytools.assertions.equal( points.dtype, numpy.float32 )
	assert pytools.assertions.equal( points.shape[-1], 2 )
	assert pytools.assertions.type_is( image_width, int )
	assert pytools.assertions.type_is( image_height, int )

	points = numpy.add( points, numpy.asarray([1.0, 1.0], dtype=numpy.float32) )
	points = numpy.divide( points, numpy.asarray([2.0, 2.0], dtype=numpy.float32) )
	points = numpy.multiply( points, numpy.asarray([image_width, image_height], dtype=numpy.float32) )
	return points

# def rescale_control_points ( points, image_shape )

# --------------------------------------------------

def mesh_points_to_bounding_rect ( points, r, t, K ):
	# Project point cloud into image
	points, j = cv2.projectPoints( mesh, r, t, K, numpy.zeros(5,dtype=numpy.float32) )
	points = numpy.reshape( points, (-1,2) ).astype( numpy.float32 )

	# Compute bounding rect
	br00 = numpy.min( points[:, 0] )
	br01 = numpy.min( points[:, 1] )
	br10 = numpy.max( points[:, 0] )
	br11 = numpy.max( points[:, 1] )
	return numpy.asarray( [[br00, br01], [br10, br11]], dtype=numpy.float32 )

# --------------------------------------------------

def mask_to_bounding_rectangle ( mask ):
	# vertical projection, this gives a horizontal line of cells
	v_proj = numpy.any( mask, axis=0 )

	# horizontal projection, this gives a vertical line of cells
	h_proj = numpy.any( mask, axis=1 )

	# Find non-zeros raws and columns
	h_proj_indexes = numpy.nonzero( h_proj )[ 0 ]
	v_proj_indexes = numpy.nonzero( v_proj )[ 0 ]

	# Compute bounding rect
	br00 = v_proj_indexes[  0 ] # first true in h
	br01 = h_proj_indexes[  0 ] # first true in v
	br10 = v_proj_indexes[ -1 ] # last true in h
	br11 = h_proj_indexes[ -1 ] # last true in v
	return numpy.asarray( [[br00, br01], [br10, br11]], dtype=numpy.int32 )

# --------------------------------------------------

def render_to_bounding_rect ( render ):
	alpha = render[ :, : ,3 ]
	mask  = alpha > 0
	return mask_to_bounding_rectangle( mask )