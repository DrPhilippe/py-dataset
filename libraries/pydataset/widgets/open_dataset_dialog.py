# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets
import re

# INTERNALS
import pytools.assertions
import pyui.widgets

# ##################################################
# ###        CLASS OPEN-DATASET-DIALOG           ###
# ##################################################

class OpenDatasetDialog ( pyui.widgets.Dialog ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):

		# Init the properties
		self._url = ''

		# Init the dialog
		super( OpenDatasetDialog, self ).__init__( 'Open a dataset', 'Ok', 'Cancel', parent )

	# def __init__ ( self, parent )

	# ##################################################
	# ###                CLASS-FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	regex = re.compile( r'(?P<backend_name>[a-z]+)(?:\:\/\/)(?P<backend_url>[a-zA-Z0-9:\/]+)\/(?P<dataset_name>[a-zA-Z0-9]+)' )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def url ( self ):

		return self._url

	# def url ( self )

	# --------------------------------------------------

	@property
	def url_label ( self ):

		return self._url_label

	# def url_label ( self )

	# --------------------------------------------------

	@property
	def url_lineedit ( self ):

		return self._url_lineedit

	# def url_lineedit ( self )

	# --------------------------------------------------

	@property
	def pick_button ( self ):

		return self._pick_button

	# def pick_button ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self, title, text_ok, text_cancel ):

		self.setMinimumSize( 400, 60 )
		super( OpenDatasetDialog, self ).setup( title, text_ok, text_cancel )

	# def setup ( self, title, text_ok, text_cancel )

	# --------------------------------------------------

	def setup_form ( self ):

		box_layout = PyQt5.QtWidgets.QHBoxLayout()
		self.layout().addLayout(box_layout)

		self._url_label = PyQt5.QtWidgets.QLabel( 'URL', self )
		box_layout.addWidget( self._url_label )

		# Create a row to display the path
		self._url_lineedit = PyQt5.QtWidgets.QLineEdit( '', self )
		self._url_lineedit.textChanged.connect( self.on_line_edit_url_changed )
		box_layout.addWidget( self._url_lineedit )

		# Create a button to open file dataset
		self._pick_button = PyQt5.QtWidgets.QPushButton( '...', self )
		self._pick_button.setMaximumSize( 24, self._pick_button.maximumHeight() )
		self._pick_button.clicked.connect( self.on_button_pick_clicked )
		box_layout.addWidget( self._pick_button )

	# def setup_form ( self )

	# ##################################################
	# ###                  SLOTS                     ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_line_edit_url_changed ( self, text ):

		self._url_lineedit.setStyleSheet( 'color: white;' )

		if ( text ):
			match = OpenDatasetDialog.regex.match( text )

			if match is not None:
				self.ok_button.setEnabled( True )
				self._url = text

			else:
				self._url_lineedit.setStyleSheet( 'color: red;' )
				self._url = ''

		else:
			self._url_lineedit.setStyleSheet( 'color: red;' )
			self._url = ''

	# def on_line_edit_url_changed ( self )

	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot()
	def on_button_pick_clicked ( self ):

		# fetch the selection
		dirpath = PyQt5.QtWidgets.QFileDialog.getExistingDirectory( self, 'Select the directory containing the dataset to open' )

		# Check selection
		if ( dirpath ):

			# Get the chosen directory
			url = 'file://' + dirpath
			
			self._url_lineedit.blockSignals( True )
			self._url_lineedit.setText( url )	
			self._url_lineedit.blockSignals( False )

			self._url = url

			self.ok_button.setEnabled( True )

		# if ( dirpath )

	# def on_pick_button_clicked ( self )
	
# class OpenDatasetDialog ( pyui.widgets.Dialog )