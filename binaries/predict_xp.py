# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import sys

# INTERNALS
import pykeras
import pytools

# ##################################################
# ###                    MAIN                    ###
# ##################################################

if __name__ == '__main__':
	progress_logger = pytools.tasks.console_progress_logger( 'PREDICT XP' )
	xp = pykeras.experiments.load_experiment( sys.argv[1:2], progress_logger )
	xp.predict( sys.argv[2:], progress_logger, progress_logger )