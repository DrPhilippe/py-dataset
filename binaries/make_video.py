import cv2
import numpy
import pydataset

REP = 1

subscene_group = pydataset.dataset.get( 'file://E:/Datasets/nema/groups/scenes/groups/scene_1/groups/subscene_217' )
background_color = subscene_group.get_feature( 'background_color' ).value
background_depth = subscene_group.get_feature( 'background_depth' ).value

fourcc = cv2.VideoWriter_fourcc('H','2','6','4')   
writer = cv2.VideoWriter( 'preview.mp4', fourcc, 30, (1280, 720), True )

for group_name in subscene_group.groups_names:
	print( 'frame {}'.format(group_name) )

	frame_group = subscene_group.get_group( group_name )

	foreground_color = frame_group.get_feature( 'foreground_color' ).value
	foreground_depth = frame_group.get_feature( 'foreground_depth' ).value

	for i in range( REP ):
		writer.write( foreground_color )

writer.release()