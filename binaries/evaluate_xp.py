# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import sys

# INTERNALS
import pykeras
import pytools

# ##################################################
# ###                    MAIN                    ###
# ##################################################

if __name__ == '__main__':
	logger = pytools.tasks.console_logger()
	xp = pykeras.experiments.load_experiment( sys.argv[1:2], logger )
	xp.evaluate( sys.argv[2:], logger )