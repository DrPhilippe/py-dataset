# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pydataset.widgets
import pytools.assertions
import pyui.widgets

# LOCALS
from .mesh_widget import MeshWidget

# ##################################################
# ###             CLASS SCENE-WIDGET             ###
# ##################################################

class SceneWidget ( PyQt5.QtWidgets.QFrame ):

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	scene_modified = PyQt5.QtCore.pyqtSignal()

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, scene_data=None, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )
		assert pytools.assertions.type_is_instance_of( scene_data, (type(None), pydataset.render.SceneData) )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Init the parent class
		super( SceneWidget, self ).__init__( parent )

		# Init properties
		self._scene_data = scene_data

		# Init widgets
		self.setup()
		self._populate()

	# def __init__ ( self, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def scene_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )

		return self._scene_data
	
	# def scene_data ( self )

	# --------------------------------------------------

	@scene_data.setter
	def scene_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pydataset.render.SceneData) )

		self._scene_data = value
		self.update()
	
	# def scene_data ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _clear ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )

		while self._meshs_layout.count() > 0:
		
			item = self._meshs_layout.itemAt( 0 )
			self._meshs_layout.removeItem( item )
		
			if item.widget():
				item.widget().deleteLater()

		# while self._meshs_layout.count() > 0

	# def _clear ( self )

	# --------------------------------------------------

	def _populate ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )

		if self._scene_data:
			
			opencv_data        = self._scene_data.get_child( 'OpenCV' )
			charuco_board_data = opencv_data.get_child( 'ChArUcO Board' )
			
			for entity_data in charuco_board_data.children_data:

				if isinstance ( entity_data, pydataset.render.MeshData ):

					mesh_widget = MeshWidget( entity_data, self )
					mesh_widget.changed.connect( self.on_mesh_changed )
					mesh_widget.deleted.connect( self.on_mesh_deleted )
					self._meshs_layout.addWidget( mesh_widget )

				# if isintance ( entity_data, pydataset.render.MeshData )

			# for entity_data in self._scene_data.children_data

		# if self._scene_data

	# def _populate ( self )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )

		# Layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		self.setLayout( layout )

		# Name label
		name = self._scene_data.name if self._scene_data else ''
		self._name_label = PyQt5.QtWidgets.QLabel( name, self )
		layout.addWidget( self._name_label )

		# Scroll Area
		self._meshs_scrollarea = PyQt5.QtWidgets.QScrollArea( self )
		self._meshs_scrollarea.setWidgetResizable( True )
		layout.addWidget( self._meshs_scrollarea )

		# Scroll Area / Meshs Frame
		self._meshs_frame  = PyQt5.QtWidgets.QFrame()
		self._meshs_layout = PyQt5.QtWidgets.QVBoxLayout( self._meshs_frame )
		self._meshs_frame.setLayout( self._meshs_layout )
		self._meshs_scrollarea.setWidget( self._meshs_frame )

		# Button Add
		self._button_add = PyQt5.QtWidgets.QPushButton( '+', self )
		self._button_add.clicked.connect( self.on_button_add_clicked )
		layout.addWidget( self._button_add )

	# def setup ( self )

	# --------------------------------------------------

	def update ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )
		
		self._name_label.setText( self._scene_data.name if self._scene_data else '' )
		self._clear()
		self._populate()

	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot( pydataset.render.MeshData )
	def on_mesh_changed ( self, mesh_data ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )
		assert pytools.assertions.type_is_instance_of( mesh_data, pydataset.render.MeshData )

		self.scene_modified.emit()

	# def on_mesh_changed ( self, mesh_data )

	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot( pydataset.render.MeshData )
	def on_mesh_deleted ( self, mesh_data ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )
		assert pytools.assertions.type_is_instance_of( mesh_data, pydataset.render.MeshData )
		
		# Get the charuco baord data
		opencv_entity_data = self._scene_data.get_child( 'OpenCV' )
		charuco_board_data = opencv_entity_data.get_child( 'ChArUcO Board' )

		# Remove the mesh data from the baord
		charuco_board_data.remove_child( mesh_data )

		# Refresh list
		self._clear()
		self._populate()
		
		# Notify
		self.scene_modified.emit()

	# def on_mesh_deleted ( self, mesh_data )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_add_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneWidget )

		filepath, extension_filter = PyQt5.QtWidgets.QFileDialog.getOpenFileName(
			pyui.widgets.Application.instance().main_window,
			'Select a mesh file',
			'',
			'Meshs (*.ply)'
			)

		if filepath:
			# Get the charuco baord data
			opencv_entity_data = self._scene_data.get_child( 'OpenCV' )
			charuco_board_data = opencv_entity_data.get_child( 'ChArUcO Board' )

			# Load the mesh data
			mesh_data = pydataset.render.MeshData.load_ply( filepath )

			# Add the mesh data on the baord
			charuco_board_data.add_child( mesh_data )

			# Create the widget to edit it
			mesh_widget = MeshWidget( mesh_data, self )
			self._meshs_layout.addWidget( mesh_widget )

		# if filepath

		self.scene_modified.emit()

	# def on_button_add_clicked ( self )

# class SceneWidget ( PyQt5.QtWidgets.QFrame )