# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pytools.assertions
import pyui

# LOCALS

# ##################################################
# ###                 CLASS TAB                  ###
# ##################################################

class Tab ( PyQt5.QtWidgets.QWidget ):
	"""
	Base class for tabs of the recorder app main window.
	"""

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	"""
	Signals used by tabs to notify the mainwindow that the dataset was modified.
	"""
	dataset_modified = PyQt5.QtCore.pyqtSignal()
	
	# --------------------------------------------------

	"""
	Signals used by tabs to notify the mainwindow that this tab is now enabled.
	"""
	enabled = PyQt5.QtCore.pyqtSignal( object, bool )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='', parent=None ):
		"""
		Initialize a new isntance of the `recorder.widgets.Tab` class.

		Arguments:
			self             (`recorder.widgets.Tab`): Tab instance to intialize.
			title                             (`str`): Title of the tab>
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )
		assert pytools.assertions.type_is( title, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Set properties
		self._title = title

		# Initialize the widget
		super( Tab, self ).__init__( parent )
		
		self.setup()
		self._edition_widget = self.setup_edition_widget()
		self._home_button    = self.setup_home_button()

	# def __init__ ( self, title, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def title ( self ):
		"""
		Title of the tab (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )
	
		return self._title

	# def title ( self )

	# --------------------------------------------------

	@property
	def edition_widget ( self ):
		"""
		Edition widget displayed in the right section of the UI (`PyQt5.QtWidgets.QWidget`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		return self._edition_widget

	# def edition_widget ( self )

	# --------------------------------------------------

	@property
	def home_button ( self ):
		"""
		Button displayed in the home tab's edition widget used to select this tab (`PyQt5.QtWidgets.QPushButton`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		return self._home_button

	# def home_button ( self )

	# --------------------------------------------------

	@property
	def target_dataset ( self ):
		"""
		Dataset this tab works on (`None`/`pydataset.dataset.Dataset`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		return self._target_dataset

	# def target_dataset ( self )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def setEnabled ( self, enabled ):
		"""
		Enables/Disables this tab.

		Arguments:
			self (`recorder.widgets.Tab`): Tab to enable/disable.
			enabled              (`bool`): `True` enables the tab, `False` disables it.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		super( Tab, self ).setEnabled( enabled )
		
		self.enabled.emit( self, enabled )

	# def setEnabled ( self, enabled )

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		"""
		Sets the dataset this tab works on.

		Arguments:
			self                (`recorder.widgets.Tab`): Tab of which to set the dataset.
			dataset (`pydataset.dataset.Dataset`/`None`): Dataset the tab must work on.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )

		self._target_dataset = dataset

	# def set_target_dataset ( self, dataset )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this tab widgets.

		Arguments:
			self (`recorder.widgets.Tab`): Tab to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		super( Tab, self ).close()

	# def close ( self )
	
	# --------------------------------------------------

	def setup ( self ):
		"""
		Setups the widgets of this tab.

		Arguments:
			self (`recorder.widgets.Tab`): Tab of which to setup the widgets.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		"""
		Setups the edition widget of this tab.

		Arguments:
			self (`recorder.widgets.Tab`): Tab of which to setup the edition widget.

		Returns:
			`None`/`PyQt5.QtWidgets.QWidget`: Edition widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		return None

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		"""
		Setups the home button of this tab.

		Arguments:
			self (`recorder.widgets.Tab`): Tab of which to setup the home button.

		Returns:
			`None`/`PyQt5.QtWidgets.QPushButton`: Home button.
		"""
		assert pytools.assertions.type_is_instance_of( self, Tab )

		return None

	# def setup_home_button ( self )

# class Tab ( PyQt5.QtWidgets.QWidget )