# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import cv2
import numpy
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets
import transforms3d.affines
import transforms3d.euler
import time

# INTERNALS
import pydataset.charuco
import pydataset.dataset
import pytools.assertions
import pytools.tasks
import pyturntable
import pyui.widgets

# LOCALS
from .tab import Tab
from .choose_scene_dialog import ChooseSceneDialog

# ##################################################
# ###                 CLASS TAB                  ###
# ##################################################

class RecordTab ( Tab ):
	"""
	Tab used to record examples of a scene.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='', parent=None ):
		"""
		Initialize a new isntance of the `recorder.widgets.RecordTab` class.

		Arguments:
			self       (`recorder.widgets.RecordTab`): Tab instance to intialize.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the widget
		super( RecordTab, self ).__init__( 'Record', parent )
		
		# Init fields
		self._board_data      = None
		self._camera          = None
		self._camera_name     = ''
		self._camera_settings = None
		self._cam_K           = None
		self._cam_dist_coeffs = None
		self._scene_group     = None
		self._subscene_group  = None
		self._turn_table      = pyturntable.TurnTable()
		
	# def __init__ ( self, parent )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		"""
		Sets the dataset this tab works on.

		Arguments:
			self          (`recorder.widgets.RecordTab`): Tab of which to set the dataset.
			dataset (`pydataset.dataset.Dataset`/`None`): Dataset the tab must work on.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )

		# Set in the parent class
		super( RecordTab, self ).set_target_dataset( dataset )

		# Reset fields
		self._board_data      = None
		self._camera          = None
		self._camera_name     = ''
		self._camera_settings = None
		self._cam_K           = None
		self._cam_dist_coeffs = None
		self._scene_group     = None
		self._subscene_group  = None
		self._example_listwidget.clear()
		self._background_label.setPixmap( PyQt5.QtGui.QPixmap() )

		# Reset UI
		self.setEnabled( False )
		self._home_button.setEnabled( False )
		self._scene_name_lineedit.setText( '' )
		self._scene_choose_button.setEnabled( False )
		self._subscene_name_lineedit.setText( '' )
		self._subscene_choose_button.setEnabled( False )

		# If dataset is not None
		if dataset is None:
			
			# Set None the camera in the UI
			self._set_camera()
			return
		
		# if dataset is None

		# Check that the charuco board has been setup
		if dataset.contains_feature( 'charuco_board', pydataset.dataset.SerializableFeatureData ):
			self._board_data = dataset.get_feature( 'charuco_board' ).value
			if not self._board_data:
				return
		else:
			return

		# Check that the camera name is setup
		if dataset.contains_feature( 'camera_name', pydataset.dataset.TextFeatureData ):
			self._camera_name = dataset.get_feature( 'camera_name' ).value
			if not self._camera_name:
				return
		else:
			return

		# Check that the camera settings are setup
		if dataset.contains_feature( 'camera_settings', pydataset.dataset.SerializableFeatureData ):
			self._camera_settings = dataset.get_feature( 'camera_settings' ).value
			if not self._camera_settings:
				return
		else:
			return

		# Check that the calibrator settings are defined
		if dataset.contains_feature( 'calibrator_settings', pydataset.dataset.SerializableFeatureData ):
			calibrator_settings = dataset.get_feature( 'calibrator_settings' ).value
			if not calibrator_settings:
				return
		else:
			return

		# Check that the cam K is computed
		if dataset.contains_feature( calibrator_settings.camera_matrix_feature_name, pydataset.dataset.NDArrayFeatureData ):
			self._cam_K = dataset.get_feature( calibrator_settings.camera_matrix_feature_name ).value
			if self._cam_K is None:
				return

		else:
			return

		# Check that the cam dist_coeffs is computed
		if dataset.contains_feature( calibrator_settings.dist_coeffs_feature_name, pydataset.dataset.NDArrayFeatureData ):
			self._cam_dist_coeffs = dataset.get_feature( calibrator_settings.dist_coeffs_feature_name ).value
			if self._cam_dist_coeffs is None:
				return
		else:
			return
	
		# Set the camera in the UI
		self._set_camera()

		# Enable UI
		self._scene_choose_button.setEnabled( True )
		self._home_button.setEnabled( True )
		self.setEnabled( True )

	# def set_target_dataset ( self, dataset )
	
	# --------------------------------------------------

	def _set_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		
		cameras_names = pydataset.cameras.CameraFactory.get_cameras_names()
		
		# Stop previous camera
		if self._camera_preview.is_playing():
			self._camera_preview.stop_camera()
			self._camera_preview.set_camera( None )
		
		# Delete previous camera
		self._camera = None
		
		# If not camera name/settings are given
		if not self._camera_name or not self._camera_settings:
			
			# Display error
			popup = pyui.dialogs.InfoDialog(
				 title = 'Camera is not setup',
				  text = 'The camera is not setup, this should not have happend.',
				parent = self
				)
			popup.exec()
			return

		# If the camera is not supported
		if self._camera_name not in cameras_names:

			# Display error
			popup = pyui.dialogs.InfoDialog(
				 title = 'Unsupported Camera',
				  text = 'The camera named "{}" is not supported'.format( self._camera_name ),
				parent = self
				)
			popup.exec()
			return

		# If the camera is supported
		else:		
			# Log file
			logger = pytools.tasks.file_logger( 'logs/recorder.record_tab.camera.log' )

			# Instantiate the camera
			self._camera = pydataset.cameras.CameraFactory.instantiate_camera(
				self._camera_name,
				settings = self._camera_settings,
				  logger = logger
				)
				
			# Update UI
			self._camera_preview.set_camera( self._camera )

		# if not self._camera_name or not self._camera_settings

	# def _set_camera ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this tab widgets.

		Arguments:
			self (`recorder.widgets.RecordTab`): Tab to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		
		if self._camera_preview.is_playing:
			self._camera_preview.stop_camera()

		self._turn_table.close()

		super( RecordTab, self ).close()

	# def close ( self )
	
	# --------------------------------------------------

	def setup ( self ):
		"""
		Setups the widgets of this tab.

		Arguments:
			self (`recorder.widgets.RecordTab`): Tab of which to setup the widgets.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		# Layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Camera Preview
		self._camera_preview = pydataset.cameras.CameraPreview( None, 15, self )
		self._camera_preview.camera_started.connect( self.on_camera_started )
		self._camera_preview.camera_stopped.connect( self.on_camera_stopped )
		self._camera_preview.set_frame_mapper( self.on_preview_frame )
		layout.addWidget( self._camera_preview )

		# Status Bar
		self._status_bar = PyQt5.QtWidgets.QLabel( '', self )
		self._status_bar.setStyleSheet( 'font-size: 18px' )
		layout.addWidget( self._status_bar )

	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		"""
		Setups the edition widget of this tab.

		Arguments:
			self (`recorder.widgets.RecordTab`): Tab of which to setup the edition widget.

		Returns:
			`None`/`PyQt5.QtWidgets.QWidget`: Edition widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_widget.setLayout( edition_layout )
		
		# Edition Widget / Scene TitleLabel
		label = pyui.widgets.TitleLabel( 'Scene', edition_widget )
		edition_layout.addWidget( label )
		
		# Edition Widget / Scene Widget
		scene_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		scene_layout = PyQt5.QtWidgets.QHBoxLayout( scene_widget )
		scene_layout.setContentsMargins( 0, 0, 0, 0 )
		scene_widget.setLayout( scene_layout )
		edition_layout.addWidget( scene_widget )

		# Edition Widget / Scene Widget / Scene-Name Line-Edit
		self._scene_name_lineedit = PyQt5.QtWidgets.QLineEdit( '', scene_widget )
		self._scene_name_lineedit.setReadOnly( True )
		scene_layout.addWidget( self._scene_name_lineedit, 1 )

		# Edition Widget / Scene Widget / Scene-Name Button
		self._scene_choose_button = PyQt5.QtWidgets.QPushButton( '...', scene_widget )
		self._scene_choose_button.clicked.connect( self.on_choose_scene_clicked )
		self._scene_choose_button.setEnabled( False )
		scene_layout.addWidget( self._scene_choose_button, 0 )
		
		# Edition Widget / Sub-Scene TitleLabel
		label = pyui.widgets.TitleLabel( 'Sub-Scene', edition_widget )
		edition_layout.addWidget( label )
		
		# Edition Widget / Sub-Scene Widget
		subscene_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		subscene_layout = PyQt5.QtWidgets.QHBoxLayout( subscene_widget )
		subscene_layout.setContentsMargins( 0, 0, 0, 0 )
		subscene_widget.setLayout( subscene_layout )
		edition_layout.addWidget( subscene_widget )

		# Edition Widget / Scene Widget / Scene-Name Line-Edit
		self._subscene_name_lineedit = PyQt5.QtWidgets.QLineEdit( '', subscene_widget )
		self._subscene_name_lineedit.setReadOnly( True )
		subscene_layout.addWidget( self._subscene_name_lineedit, 1 )

		# Edition Widget / Scene Widget / Scene-Name Button
		self._subscene_choose_button = PyQt5.QtWidgets.QPushButton( '...', subscene_widget )
		self._subscene_choose_button.clicked.connect( self.on_choose_subscene_clicked )
		self._subscene_choose_button.setEnabled( False )
		subscene_layout.addWidget( self._subscene_choose_button, 0 )

		# Edition Widget / Background TitleLabel
		label = pyui.widgets.TitleLabel( 'Background', edition_widget )
		edition_layout.addWidget( label )


		# Edition Widget / Background Label
		self._background_label = PyQt5.QtWidgets.QLabel( edition_widget )
		self._background_label.setScaledContents( True )
		edition_layout.addWidget( self._background_label )

		# Edition Widget / Background Capture Button
		self._backgroung_button = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_camera.png' ),
			'IDLE',
			edition_widget
			)
		self._backgroung_button.clicked.connect( self.on_button_record_background_clicked )
		self._backgroung_button.setEnabled( False )
		edition_layout.addWidget( self._backgroung_button )

		# Edition Widget / Examples TitleLabel
		label = pyui.widgets.TitleLabel( 'Examples', edition_widget )
		edition_layout.addWidget( label )

		# Edition Widget / Examples ListWidget
		self._example_listwidget = PyQt5.QtWidgets.QListWidget( edition_widget )
		edition_layout.addWidget( self._example_listwidget )
		
		# Edition Widget / Example Clear Button
		self._clear_button = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_trash.png' ),
			'CLEAR',
			edition_widget
			)
		self._clear_button.clicked.connect( self.on_button_clear_clicked )
		# self._clear_button.setEnabled( False )
		edition_layout.addWidget( self._clear_button )

		# Edition Widget / Example Capture Button
		self._example_button = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_camera.png' ),
			'IDLE',
			edition_widget
			)
		self._example_button.clicked.connect( self.on_button_record_example_clicked )
		self._example_button.setEnabled( False )
		edition_layout.addWidget( self._example_button )

		self._progress_bar = PyQt5.QtWidgets.QProgressBar( edition_widget )
		self._progress_bar.setRange( 0, 360 )
		self._progress_bar.setValue( 0 )
		edition_layout.addWidget( self._progress_bar )

		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		"""
		Setups the home button of this tab.

		Arguments:
			self (`recorder.widgets.RecordTab`): Tab of which to setup the home button.

		Returns:
			`None`/`PyQt5.QtWidgets.QPushButton`: Home button.
		"""
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_record.png' ),
			'Record examples of a scene',
			)

	# def setup_home_button ( self )

	# --------------------------------------------------

	def _prepare_record ( self, frames ):

		# Get the foreground
		color_image = frames[ 'color' ]
		depth_image = frames[ 'depth' ]

		# Convert image to grayscale
		grayscale = cv2.cvtColor( color_image, cv2.COLOR_BGR2GRAY  )

		# Detect charuco markers
		markers_corners, markers_ids = pydataset.charuco.detect_markers(
			self._board_data,
			grayscale
			)
		if markers_corners is None:
			self._status_bar.setText( 'ChArUcO marker detection failed, record deleted.' )
			return None

		# Interpolate charuco board corners
		charuco_corners, charuco_ids = pydataset.charuco.interpolate_corners(
			self._board_data,
			grayscale,
			markers_corners,
			markers_ids,
			self._cam_K,
			self._cam_dist_coeffs
			)
		if charuco_corners is None:
			self._status_bar.setText( 'ChArUcO corner interpolation failed, record deleted.' )
			return None
			
		# Estimate charuco board pose
		ok, charuco_r, charuco_t = pydataset.charuco.estimate_pose(
			self._board_data,
			charuco_corners,
			charuco_ids,
			self._cam_K,
			self._cam_dist_coeffs
			)
		if not ok:
			self._status_bar.setText( 'ChArUcO pose estimation failed, record deleted.' )
			return None			
		
		# Compute rotation matrix
		charuco_R, j = cv2.Rodrigues( charuco_r )

		# Cast pose
		charuco_R = numpy.asarray( charuco_R, dtype=numpy.float32 )
		charuco_R = numpy.reshape( charuco_R, [3, 3] )
		charuco_t = numpy.asarray( charuco_t, dtype=numpy.float32 )
		charuco_t = numpy.reshape( charuco_t, [3] )
		charuco_T = numpy.eye( 4, dtype=numpy.float32 )
		charuco_T[:3, :3] = charuco_R
		charuco_T[:3,  3] = charuco_t

		# Create record
		record = self._subscene_group.create_group(
			'frame_{}'.format( self._subscene_group.number_of_groups )
			)

		# Save features
		record.create_or_update_feature(
			     feature_name = 'foreground_color',
			feature_data_type = pydataset.dataset.ImageFeatureData,
			            value = color_image
			)
		record.create_or_update_feature(
			     feature_name = 'foreground_depth',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = depth_image
			)
		record.create_or_update_feature(
			     feature_name = 'markers_corners',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = markers_corners
			)
		record.create_or_update_feature(
			     feature_name = 'markers_ids',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = markers_ids
			)
		record.create_or_update_feature(
			     feature_name = 'charuco_corners',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = charuco_corners
			)
		record.create_or_update_feature(
			     feature_name = 'charuco_ids',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = charuco_ids
			)
		record.create_or_update_feature(
			     feature_name = 'R',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = charuco_R
			)
		record.create_or_update_feature(
			     feature_name = 't',
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = charuco_t
			)

		# Load scene data
		scene_data         = self._scene_group.get_feature( 'scene_data' ).value
		opencv_data        = scene_data.get_child( 'OpenCV' )
		charuco_board_data = opencv_data.get_child( 'ChArUcO Board' )

		# Go though mesh on the board
		for entity_data in charuco_board_data.children_data:

			# Is the entity a mesh
			if isinstance ( entity_data, pydataset.render.MeshData ):

				# Mesh local transform
				mesh_T = numpy.asarray( entity_data.local_transform, dtype=numpy.float32 )
				mesh_T = numpy.reshape( mesh_T, [4, 4] )

				# Mesh global transform
				T = numpy.dot( charuco_T, mesh_T )

				# decompose
				mesh_t, mesh_R, Z, S = transforms3d.affines.decompose( T )

				# Create an example for this object
				example = record.create_example( entity_data.name )

				# Save its pose
				example.create_or_update_feature(
					     feature_name = 'R',
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = mesh_R,
					      auto_update = False
					)
				example.create_or_update_feature(
					     feature_name = 't',
					feature_data_type = pydataset.dataset.NDArrayFeatureData,
					            value = mesh_t,
					      auto_update = False
					)
				example.update()

			# if isintance ( entity_data, pydataset.render.MeshData )

		# for entity_data in self._scene_data.children_data

		self._status_bar.setText( 'Record successfull.' )
		
		return record

	# def _prepare_record ( self, record )
	
	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_preview_frame ( self, stream_name, frame ):

		if stream_name != 'color':
			return frame
			
		width  = frame.shape[ 1 ]
		height = frame.shape[ 0 ]

		# Convert the image to grayscale
		gray_frame = cv2.cvtColor( frame, cv2.COLOR_BGR2GRAY )

		# Draw scope pointer
		frame = cv2.line( frame, (width//2-10,height//2), (width//2+10,height//2), (255,0,0), 2 )
		frame = cv2.line( frame, (width//2,height//2-10), (width//2,height//2+10), (255,0,0), 2 )

		if self._board_data:

			# Detect charuco markers
			markers_corners, markers_ids = pydataset.charuco.detect_markers(
				self._board_data,
				gray_frame
				)
			if markers_corners is None:
				return frame

			# Interpolate charuco board corners
			charuco_corners, charuco_ids = pydataset.charuco.interpolate_corners(
				self._board_data,
				gray_frame,
				markers_corners,
				markers_ids,
				self._cam_K,
				self._cam_dist_coeffs
				)
			if charuco_corners is None:
				return frame
				
			# Estimate charuco board pose
			ok, r, t = pydataset.charuco.estimate_pose( self._board_data, charuco_corners, charuco_ids, self._cam_K, self._cam_dist_coeffs )
			if not ok:
				return frame

			# Draw baord axis
			frame = pydataset.charuco.draw_axis( frame, self._cam_K, self._cam_dist_coeffs, r, t, 0.2 )
			
			r = numpy.reshape( numpy.array( r, dtype=numpy.float32 ), [3] )
			t = numpy.reshape( numpy.array( t, dtype=numpy.float32 ), [3] )	
			r = r * 180.0 / numpy.pi
			frame = cv2.putText(
				frame,
				'r: {:07.3f}, {:07.3f}, {:07.3f}'.format( r[0], r[1], r[2] ),
				(2, 15),
				cv2.FONT_HERSHEY_SIMPLEX,
				0.5,
				(0,0,0),
				1,
				cv2.LINE_AA
				)
			frame = cv2.putText(
				frame,
				't: {:07.3f}, {:07.3f}, {:07.3f}'.format( t[0], t[1], t[2] ),
				(2, 30),
				cv2.FONT_HERSHEY_SIMPLEX,
				0.5,
				(0,0,0),
				1,
				cv2.LINE_AA
				)

		# if self._board_data

		return frame

	# def on_preview_frame ( self, stream_name, frame )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_started ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		if self._subscene_group is not None:
			self._backgroung_button.setEnabled( True )
			self._example_button.setEnabled( True )

	# def on_camera_started ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_stopped ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		self._backgroung_button.setEnabled( False )
		self._example_button.setEnabled( False )
		
	# def on_camera_stopped ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_choose_scene_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		# Load scene group parent
		scenes_group = self.target_dataset.get_or_create_group( 'scenes' )

		# Create and run selection dialog
		dialog = ChooseSceneDialog( scenes_group.groups_names, self )
		if dialog.exec():
			
			# Get the scene group
			self._scene_group = scenes_group.get_or_create_group( dialog.scene_name )
			
			# Display the scene name in the UI
			self._scene_name_lineedit.setText( self._scene_group.name )
			
			# Enable the button to pick the sub-scene
			self._subscene_choose_button.setEnabled( True )		

		# if dialog.exec():

	# def on_choose_scene_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_choose_subscene_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )

		# Create and run selection dialog
		dialog = ChooseSceneDialog( self._scene_group.groups_names, self )
		if dialog.exec():
			
			# Get the scene group
			self._subscene_group = self._scene_group.get_or_create_group( dialog.scene_name )
			
			# Display the scene name in the UI
			self._subscene_name_lineedit.setText( self._subscene_group.name )
			
			# Load background image ?
			if self._subscene_group.contains_feature( 'background_color', pydataset.dataset.ImageFeatureData ):
				image  = self._subscene_group.get_feature( 'background_color' ).value
				image  = pyui.widgets.images_utils.clamp_image_to_size( image, (self._background_label.width(), self._background_label.width()) )
				pixmap = pyui.widgets.images_utils.image_to_qpixmap( image )
				self._background_label.setPixmap( pixmap )
			else:
				self._background_label.setPixmap( PyQt5.QtGui.QPixmap() )


			# Load existing record
			self._example_listwidget.clear()
			self._example_listwidget.addItems( self._subscene_group.groups_names )
			self._example_listwidget.scrollToBottom()

		# if dialog.exec():

	# def on_choose_subscene_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_record_background_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		
		# Create an run a recorder to record one frame
		recorder = pydataset.cameras.DatasetRecorder.create(
			                   camera = self._camera,
			                dst_group = self._subscene_group,
			maximum_number_of_records = 1,
			       records_group_name = '',
			              target_type = pydataset.cameras.TargetType.features,
			                 override = True,
			      feature_name_prefix = 'background_',
			                   logger = pytools.tasks.file_logger( 'logs/recorder.record_tab.background_recorder.log' )
			)
		recorder.execute()

		# Check the recorder did not failed
		if recorder.is_failed():
			dialog = pyui.widgets.InfoDialog(
				  title = 'Error',
				   text = 'Recorder failed: {}\ncheck the log for more details.'.format(recorder.error),
				text_ok = 'Ok',
				 parent = None
				)
			dialog.exec()
			return

		# Reload the subscene group to include the new record
		self._subscene_group.reload()

		# Display the background	
		if self._subscene_group.contains_feature( 'background_color', pydataset.dataset.ImageFeatureData ):
			image  = self._subscene_group.get_feature( 'background_color' ).value
			image  = pyui.widgets.images_utils.clamp_image_to_size( image, (self._background_label.width(), self._background_label.width()) )
			pixmap = pyui.widgets.images_utils.image_to_qpixmap( image )
			self._background_label.setPixmap( pixmap )

	# def on_button_record_background_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_record_example_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, RecordTab )
		
		self._backgroung_button.setEnabled( False )
		self._example_button.setEnabled( False )

		# Update the progress bar
		self._progress_bar.setValue( 0 )

		# Home the turn table
		UNIT_ANGLE = 1.0
		self._turn_table.set_velocity( 9, True )
		self._turn_table.set_unit_angle( UNIT_ANGLE, True )
		self._turn_table.home( True )

		angle = 0.0
		nb_attempts = 0
		while ( angle < 360.0 and nb_attempts < 500 ):

			# Let Qt process events
			PyQt5.QtWidgets.QApplication.processEvents()

			# Check that the camera did not stop
			if not self._camera_preview.is_playing:
				self._status_bar.setText( 'Camera failed: check log for details.' )
				break

			# Grab current frames
			t, frames = self._camera.get_frames()

			# Prepare the record
			record = self._prepare_record( frames )

			# If the record was deleted (because not satifying)
			if record is None:
				
				# Increment atemps counter
				nb_attempts += 1
			
			else:
				# Reset atemps counter
				nb_attempts = 0

				# Display the frame
				self._camera_preview.set_raw_frame( record.get_feature( 'foreground_color' ).value )

				# Reload the subscene group to include the new record
				self._subscene_group.reload()

				# Move on to the next turn table position
				self._turn_table.turn( True )
				angle += UNIT_ANGLE

				# Update the progress bar
				self._progress_bar.setValue( angle )			

			# Let Qt process events
			PyQt5.QtWidgets.QApplication.processEvents()

		# while ( angle < 360.0 or nb_attempts < 100 )

		# Reload the subscene group to include the new records
		self._subscene_group.reload()

		# Refresh record list
		self._example_listwidget.clear()
		self._example_listwidget.addItems( self._subscene_group.groups_names )
		self._example_listwidget.scrollToBottom()

		self._example_button.setEnabled( True )
		self._backgroung_button.setEnabled( True )
		
	# def on_button_record_example_clicked ( self )

	def on_button_clear_clicked ( self ):

		# Reload the subscene group to include the new records
		self._subscene_group.reload()

		# Delete frames
		self._subscene_group.delete_groups()

		# Refresh record list
		self._example_listwidget.clear()

	# def on_button_clear_clicked ( self )

# class RecordTab ( Tab )