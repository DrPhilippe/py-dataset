# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset
import pydataset.cameras
import pytools.assertions
import pyui.inspector
import pyui.widgets

# LOCALS
from .tab import Tab

# ##################################################
# ###              CLASS CAMERA-TAB              ###
# ##################################################

class CameraTab ( Tab ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		"""
		Initialize a new isntance of the `recorder.widgets.CameraTab` class.

		Arguments:
			self       (`recorder.widgets.CameraTab`): Instance to intialize.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Camera properties
		self._camera_name_feature     = None
		self._camera_settings_feature = None
		self._camera_name             = ''
		self._camera_settings         = None
		self._camera                  = None

		# Init the tab
		super( CameraTab, self ).__init__( 'Camera', parent )
		
	# def __init__ ( self, camera_name, camera_settings, parent )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )
		
		# Set in the parent class
		super( CameraTab, self ).set_target_dataset( dataset )

		# If the dataset exists
		if dataset:
			
			# If the camera name feature exists
			if dataset.contains_feature( 'camera_name', pydataset.dataset.TextFeatureData ):

				# Load it
				self._camera_name_feature = dataset.get_feature( 'camera_name' )

			# If the camera name feature does not exist
			else:

				# Create the camera name feature
				self._camera_name_feature = dataset.create_feature(
					     feature_name = 'camera_name',
					feature_data_type = pydataset.dataset.TextFeatureData,
					      auto_update = True,
					            value = ''
					)
			
			# Create a local copy of the camera name
			self._camera_name = copy.deepcopy( self._camera_name_feature.value )

			# If the camera settings feature exists
			if dataset.contains_feature( 'camera_settings', pydataset.dataset.SerializableFeatureData ):

				# Load it
				self._camera_settings_feature = dataset.get_feature( 'camera_settings' )

			# If the camera settings feature does not exist
			else:
				
				# Create the camera settings feature
				self._camera_settings_feature = dataset.create_feature(
					     feature_name = 'camera_settings',
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = None
					)

			# Create a local copy of the camera settings
			self._camera_settings = copy.deepcopy( self._camera_settings_feature.value )

			# Set the camera in the UI
			self._set_camera()

			# Enable UI
			self._home_button.setEnabled( True )
			self.setEnabled( True )

		# If the dataset does not exist
		else:
			# Set the features to None
			self._camera_name_feature = None
			self._camera_settings_feature = None
			self._camera_name = ''
			self._camera_settings = None

			# Set the edited feature to None
			self._set_camera()

			# Disable UI
			self._home_button.setEnabled( False )
			self.setEnabled( False )

	# def set_target_dataset ( self, dataset )

	# --------------------------------------------------

	def _set_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		
		cameras_names = pydataset.cameras.CameraFactory.get_cameras_names()
		
		# Stop previous camera
		if self._camera_preview.is_playing():
			self._camera_preview.stop_camera()
			self._camera_preview.set_camera( None )
		
		# Delete previous camera
		self._camera = None
		
		# If not camera is given
		if not self._camera_name:
			
			# Update UI
			self._name_combobox.blockSignals( True )
			self._name_combobox.setCurrentText( '<nothing selected>' )
			self._name_combobox.blockSignals( False )
			self._settings_inspector.value = None
			self._button_save_changes.setEnabled( False )
			self._button_discard_changes.setEnabled( False )
			self._camera_preview.set_camera( None )

		# If the camera is not supported
		elif self._camera_name not in cameras_names:

			# Display error
			popup = pyui.widgets.InfoDialog(
				 title = 'Unsupported Camera',
				  text = 'The camera named "{}" is not supported'.format( self._camera_name ),
				parent = self
				)
			popup.exec()

			# Default camera name/settings
			self._camera_name = ''
			self._camera_settings = None

			# Update UI
			self._name_combobox.blockSignals( True )
			self._name_combobox.setCurrentText( '<nothing selected>' )
			self._name_combobox.blockSignals( False )
			self._settings_inspector.value = None
			self._button_save_changes.setEnabled( True )
			self._button_discard_changes.setEnabled( True )
			self._camera_preview.set_camera( None )

		# If the camera is supported
		else:		
			# Log file
			logger = pytools.tasks.file_logger( 'logs/recorder.camera_tab.camera.log' )

			# Instantiate the camera with defined settings
			if self._camera_settings:

				# Instantiate
				self._camera = pydataset.cameras.CameraFactory.instantiate_camera( self._camera_name, settings=self._camera_settings, logger=logger )
				
				# Update UI
				self._button_save_changes.setEnabled( False )
				self._button_discard_changes.setEnabled( False )

			# Instantiate the camera with default settings
			else:

				# Instantiate
				self._camera = pydataset.cameras.CameraFactory.instantiate_camera( self._camera_name, logger=logger )

				# Keep a copy of the settings
				self._camera_settings = self._camera.settings
				
				# Update UI
				self._button_save_changes.setEnabled( True )
				self._button_discard_changes.setEnabled( True )

			# Update UI
			self._name_combobox.blockSignals( True )
			self._name_combobox.setCurrentText( self._camera_name )
			self._name_combobox.blockSignals( False )
			self._settings_inspector.value = self._camera_settings
			self._camera_preview.set_camera( self._camera )

		# if not self._camera_name

	# def _set_camera ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this tab widgets.

		Arguments:
			self (`recorder.widgets.CameraTab`): CameraTab to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraTab )

		if self._camera_preview.is_playing:
			self._camera_preview.stop_camera()
			
		super( CameraTab, self ).close()

	# def close ( self )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the contents of this tab.

		Arguments:
			self (`recorder.ui.CameraTab`): Tab to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Camera Preview
		self._camera_preview = pydataset.cameras.CameraPreview( None, 15, self )
		self._camera_preview.camera_started.connect( self.on_camera_started )
		self._camera_preview.camera_stopped.connect( self.on_camera_stopped )
		layout.addWidget( self._camera_preview, 0 )

	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_widget.setLayout( edition_layout )
	
		# Edition Widget / Camera Name Label
		name_label = pyui.widgets.TitleLabel( 'Camera Name', edition_widget )
		edition_layout.addWidget( name_label, 0 )

		# Edition Widget / Camera Name ComboBox
		self._name_combobox = PyQt5.QtWidgets.QComboBox( edition_widget )
		self._name_combobox.currentTextChanged.connect( self.on_camera_name_changed )
		edition_layout.addWidget( self._name_combobox, 0 )

		# Edition Widget / Camera Settings Label
		settings_label = pyui.widgets.TitleLabel( 'Camera Settings', edition_widget )
		edition_layout.addWidget( settings_label, 0 )

		# Edition Widget / Settings Inspector
		self._settings_inspector = pyui.inspector.InspectorWidget(
			     value = None,
			is_editable = True,
			    is_root = True,
			    ignores = [ 'name' ],
			     parent = edition_widget
			)
		self._settings_inspector.value_changed.connect( self.on_camera_settings_changed )
		edition_layout.addWidget( self._settings_inspector, 1 )
		
		# Edition Widget / Button Box
		button_box_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		button_box_layout = PyQt5.QtWidgets.QHBoxLayout( button_box_widget )
		button_box_layout.setContentsMargins( 2, 2, 2, 2 )
		button_box_layout.setSpacing( 2 )
		button_box_widget.setLayout( button_box_layout )
		edition_layout.addWidget( button_box_widget, 0 )

		# Edition Widget / Button Box / Button Keep Changes
		self._button_save_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogApplyButton ),
			'Save Changes',
			button_box_widget
			)
		self._button_save_changes.setEnabled( False )
		self._button_save_changes.clicked.connect( self.on_save_changes )
		button_box_layout.addWidget( self._button_save_changes, 1 )

		# Edition Widget / Button Box / Button Discard Changes
		self._button_discard_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogCancelButton ),
			'Discard Changes',
			button_box_widget
			)
		self._button_discard_changes.setEnabled( False )
		self._button_discard_changes.clicked.connect( self.on_discard_changes )
		button_box_layout.addWidget( self._button_discard_changes, 1 )

		# Populate the combo-box
		cameras_names = pydataset.cameras.CameraFactory.get_cameras_names()
		if cameras_names:
			self._name_combobox.blockSignals( True )
			self._name_combobox.addItem( '<nothing selected>' )
			for name in cameras_names:
				self._name_combobox.addItem( name )
			self._name_combobox.blockSignals( False )
		else:
			self._name_combobox.addItem( '<empty>' )
			self._name_combobox.setEnabled( False )
		# if cameras_names

		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		
		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_camera_gear.png' ),
			'Setup the camera used for the dataset',
			)

	# def setup_home_button ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_camera_name_changed ( self, camera_name ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )
		assert pytools.assertions.type_is( camera_name, str )

		# Set the camera name
		self._camera_name = camera_name

		# Discard current camera settings
		self._camera_settings = None

		# Change the camera
		self._set_camera()

		# Enable the buttons
		self._button_discard_changes.setEnabled( True )
		self._button_save_changes.setEnabled( True )

	# def on_camera_name_changed ( self, camera_name )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_settings_changed ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )

		# Save the camera settings
		self._camera_settings = self._settings_inspector.get_value()

		# Set the settings in the camera
		self._camera.settings = self._camera_settings

		# Enable the buttons
		self._button_discard_changes.setEnabled( True )
		self._button_save_changes.setEnabled( True )

	# def on_camera_settings_changed ( self, )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_started ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )

		self._name_combobox.setEnabled( False )
		self._settings_inspector.is_editable = False

	# def on_camera_started ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_stopped ( self ):
		assert pytools.assertions.type_is_instance_of( self, CameraTab )

		self._name_combobox.setEnabled( True )
		self._settings_inspector.is_editable = True

	# def on_camera_stopped ( self )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_save_changes ( self ):

		# Save the camera name
		self._camera_name_feature.value = copy.deepcopy( self._camera_name )
		self._camera_name_feature.update()

		# Save the camera settings
		self._camera_settings_feature.value = copy.deepcopy( self._camera_settings )
		self._camera_settings_feature.update()

		# Update UI
		self._button_discard_changes.setEnabled( False )
		self._button_save_changes.setEnabled( False )

		# Nodify of dataset modification
		self.dataset_modified.emit()

	# def on_save_changes ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_discard_changes ( self ):

		# Reset camera name
		self._camera_name = copy.deepcopy( self._camera_name_feature.value )

		# Reset camera settings
		self._camera_settings = copy.deepcopy( self._camera_settings_feature.value )

		# Change the camera
		self._set_camera()

	# def on_discard_changes ( self )

# class CameraTab ( Tab ) 