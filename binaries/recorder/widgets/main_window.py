# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets
import traceback

# INTERNALS
import pydataset.dataset
import pydataset.widgets
import pytools.assertions
import pyui.widgets

# LOCALS
from .tab             import Tab
from .home_tab        import HomeTab
from .charuco_tab     import CharucoTab
from .camera_tab      import CameraTab
from .calibration_tab import CalibrationTab
from .record_tab      import RecordTab
from .scene_tab       import SceneTab

# ##################################################
# ###            CLASS MAIN-WINDOW               ###
# ##################################################

class MainWindow ( PyQt5.QtWidgets.QMainWindow ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='PyRecorder' ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is( title, str )

		self._dataset = None

		super( MainWindow, self ).__init__()

		# Set window properties
		self.setWindowTitle( title )
		self.setup()
		self.setWindowState( self.windowState() ^ PyQt5.QtCore.Qt.WindowMaximized )

	# def __init__ ( self )

	# ##################################################
	# ###                   GETTER                   ###
	# ##################################################

	# --------------------------------------------------

	def get_dataset ( self ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		return self._dataset

	# def get_dataset ( self )

	# ##################################################
	# ###                   SETTER                   ###
	# ##################################################

	# --------------------------------------------------

	def set_dataset ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pydataset.dataset.Dataset) )

		self._dataset = value

		# set the dataset in each tab
		self._home_tab.set_target_dataset( value )
		self._charuco_tab.set_target_dataset( value )
		self._camera_tab.set_target_dataset( value )
		self._calibration_tab.set_target_dataset( value )
		self._scene_tab.set_target_dataset( value )
		self._record_tab.set_target_dataset( value )

	# def set_dataset ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_tab ( self, tab, enabled=True ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is_instance_of( tab, Tab )
		assert pytools.assertions.type_is( enabled, bool )

		# Get the current number of tabs
		index = self._tab_widget.count()

		# Add the tab in the tabwidget
		self._tab_widget.addTab( tab, tab.title )

		# Add the button of the tab in the home tab
		button = tab.home_button
		if button:
			self._home_tab.add_button( button, index, enabled )

		# Enable the tab
		self._tab_widget.setTabEnabled( index, enabled )

		# Connect enabled signal
		tab.enabled.connect( self.on_tab_enabled )
		tab.dataset_modified.connect( self.on_dataset_modified )

	# def add_tab ( self, tab, enabled )
	
	# --------------------------------------------------

	def open_dataset ( self ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		dialog = pydataset.widgets.OpenDatasetDialog( parent=self )
	
		if ( dialog.exec() ):

			# Attempt to open/create the dataset
			try:
				item = pydataset.dataset.get_or_create( dialog.url )
			
				# Check type of the item
				if not isinstance( item, pydataset.dataset.Dataset ):
					popup = pyui.widgets.InfoDialog(
						 title = 'Error',
						  text = 'The entered URL does not point to a dataset but a {}'.format( type(item).__name__ ),
						parent = self
						)
					popup.exec()
					return

				self.set_dataset( item )

			# Display exception if any
			except Exception as e:
				popup = pyui.widgets.InfoDialog(
					 title = 'Exception occured',
					  text = str(e)+'\n\nStack Trace:\n'+str(traceback.format_exc()),
					parent = self
					)
				popup.exec()

			# try

		# if ( dialog.exec() )

	# def open_dataset ( self )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Menu Bar
		menu_bar = pyui.widgets.MenuBar()
		self.setMenuBar( menu_bar )
		
		# Central Widget
		self._tab_widget = PyQt5.QtWidgets.QTabWidget()
		self._tab_widget.currentChanged.connect( self.on_tab_changed )
		self.setCentralWidget( self._tab_widget )

		# Right dock
		self._edit_dock = PyQt5.QtWidgets.QDockWidget( 'Edit' )
		self._edit_dock.setMinimumSize( 400, self._edit_dock.minimumHeight() )
		self.addDockWidget( PyQt5.QtCore.Qt.RightDockWidgetArea, self._edit_dock )

		# Home tab
		self._home_tab = HomeTab()
		self._home_tab.home_button_clicked.connect( self.on_home_button_clicked )
		self.add_tab( self._home_tab, True )

		# Charuco tab
		self._charuco_tab = CharucoTab()
		self.add_tab( self._charuco_tab, False )
		
		# Camera tab
		self._camera_tab = CameraTab()
		self.add_tab( self._camera_tab, False )

		# Calibration tab
		self._calibration_tab = CalibrationTab()
		self.add_tab( self._calibration_tab, False )

		# Scene tab
		self._scene_tab = SceneTab()
		self.add_tab( self._scene_tab, False )

		# Record tab
		self._record_tab = RecordTab()
		self.add_tab( self._record_tab, False )

	# def setup ( self )

	# --------------------------------------------------

	def closeEvent ( self, event ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Close all tabs
		self._home_tab.close()
		self._charuco_tab.close()
		self._camera_tab.close()
		self._calibration_tab.close()
		self._scene_tab.close()
		self._record_tab.close()

		# Close the window
		super( MainWindow, self ).closeEvent( event )
	
	# def closeEvent ( self, event )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_tab_changed ( self, index ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Check if index is not -1 (no tab)
		if ( index >= 0 ):

			# Get the new current tab
			tab = self._tab_widget.widget( index )

			# Get the edition widget
			edition_widget = tab.edition_widget

			# Set it in the edition dock
			self._edit_dock.setWidget( edition_widget )
			
			# Show it if any
			if edition_widget:
				edition_widget.show()

		# index is -1 (no tab)
		else:
			self._edit_dock.setWidget( None )

	# def on_tab_changed ( self, index )
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( object, bool )
	def on_tab_enabled( self, tab, enabled ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is_instance_of( tab, Tab )
		assert pytools.assertions.type_is( enabled, bool )

		tab_index = self._tab_widget.indexOf( tab )
		self._tab_widget.setTabEnabled( tab_index, enabled )

	# def on_tab_changed ( self, index )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_dataset_modified ( self ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Save the current tab index
		index = self._tab_widget.currentIndex()

		# Reload the dataset
		self._dataset.reload()

		# set the dataset in each tab
		self._home_tab.set_target_dataset( self._dataset )
		self._charuco_tab.set_target_dataset( self._dataset )
		self._camera_tab.set_target_dataset( self._dataset )
		self._calibration_tab.set_target_dataset( self._dataset )
		self._scene_tab.set_target_dataset( self._dataset )
		self._record_tab.set_target_dataset( self._dataset )
		
		# Set the current tab index
		self._tab_widget.setCurrentIndex( index )

	# def on_dataset_modified ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( int )
	def on_home_button_clicked ( self, tab_index ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is( tab_index, int )

		if tab_index == 0:
			self.open_dataset()
		
		elif tab_index > 0:
			self._tab_widget.setCurrentIndex( tab_index )
		
		else:
			pass

	# def on_home_button_clicked ( self, tab_index )

# class MainWindow ( PyQt5.QtWidgets.QMainWindow )