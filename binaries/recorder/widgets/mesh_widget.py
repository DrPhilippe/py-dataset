# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.render
import pytools.assertions
import pyui.widgets

# LOCALS
from .transform_widget import TransformWidget

# ##################################################
# ###             CLASS MESH-WIDGET              ###
# ##################################################

class MeshWidget ( PyQt5.QtWidgets.QFrame ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, mesh_data=None, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )
		assert pytools.assertions.type_is_instance_of( mesh_data, pydataset.render.MeshData )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( MeshWidget, self ).__init__( parent )
		self._mesh_data = mesh_data
		self.setup()
		
	# def __init__ ( self, mesh_data, parent )

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	changed = PyQt5.QtCore.pyqtSignal( pydataset.render.MeshData )

	# --------------------------------------------------

	deleted = PyQt5.QtCore.pyqtSignal( pydataset.render.MeshData )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def mesh_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )

		return self._mesh_data

	# def mesh_data ( self )

	# --------------------------------------------------

	@mesh_data.setter
	def mesh_data ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )
		assert pytools.assertions.type_is_instance_of( value, (type(None), pydataset.render.MeshData) )

		self._mesh_data = value

		name       = value.name
		primitives = '{} {}'.format( value.get_number_of_primitives(), value.primitive )
		vertices   = '{}'.format( value.get_number_of_vertices() )
		colors     = 'yes' if value.colors is not None else 'no'
		normals    = 'yes' if value.normals is not None else 'no'
		transform  = value.local_transform

		self._name_lineedit.setText( name )
		self._primitives_label.setText( primitives )
		self._vertices_label.setText( primitives )
		self._colors_label.setText( primitives )
		self._normals_label.setText( primitives )
		self._transform_widget.value = transform

	# def mesh_data ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )

		
		name       = self._mesh_data.name
		primitives = '{} {}'.format( self._mesh_data.get_number_of_primitives(), self._mesh_data.primitive )
		vertices   = '{}'.format( self._mesh_data.get_number_of_vertices() )
		colors     = 'yes' if self._mesh_data.colors is not None else 'no'
		normals    = 'yes' if self._mesh_data.normals is not None else 'no'
		transform  = self._mesh_data.local_transform


		stylesheet = pyui.widgets.Application.instance().assets.text( 'mesh_widget.css' )
		self.setStyleSheet( stylesheet )

		layout = PyQt5.QtWidgets.QFormLayout( self )
		self.setLayout( layout )

		self._name_lineedit = PyQt5.QtWidgets.QLineEdit( name, self )
		self._name_lineedit.textChanged.connect( self.on_name_changed )
		layout.addRow( 'Name', self._name_lineedit )

		self._primitives_label = PyQt5.QtWidgets.QLabel( primitives, self )
		layout.addRow( 'Primitives', self._primitives_label )

		self._vertices_label = PyQt5.QtWidgets.QLabel( vertices, self )
		layout.addRow( 'Vertices', self._vertices_label )
		
		self._colors_label = PyQt5.QtWidgets.QLabel( colors, self )
		layout.addRow( 'Colors', self._colors_label )

		self._normals_label = PyQt5.QtWidgets.QLabel( normals, self )
		layout.addRow( 'Normals', self._normals_label )

		self._transform_widget = TransformWidget( transform, self )
		self._transform_widget.value_changed.connect( self.on_transform_changed )
		layout.addRow( self._transform_widget )

		self._button_set_color = PyQt5.QtWidgets.QPushButton(
			'Set Color',
			self
			)
		self._button_set_color.clicked.connect( self.on_button_set_color_clicked )
		layout.addRow( self._button_set_color )

		self._button_delete = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_trash.png' ),
			'',
			self
			)
		self._button_delete.clicked.connect( self.on_button_delete_clicked )
		layout.addRow( self._button_delete )

	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################
	
	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot( str )
	def on_name_changed ( self, name ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )
		assert pytools.assertions.type_is( name, str )

		self._mesh_data.name = name
		self.changed.emit( self._mesh_data )

	# def on_name_changed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_transform_changed ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )

		self._mesh_data.local_transform = self._transform_widget.value
		self.changed.emit( self._mesh_data )
		
	# def on_transform_changed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_delete_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )

		self.deleted.emit( self._mesh_data )
		
	# def on_button_delete_clicked ( self )

	# --------------------------------------------------

	def on_button_set_color_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, MeshWidget )

		# Get the color
		color = PyQt5.QtWidgets.QColorDialog.getColor( parent=self, title='Choose Mesh Color', options=PyQt5.QtWidgets.QColorDialog.ShowAlphaChannel )

		# if dialog not canceled
		if color.isValid():

			# Cast it
			color = pydataset.data.ColorData( color.red(), color.green(), color.blue(), color.alpha() )

			# Assign it to the mesh vertices
			self._mesh_data.set_color( color )

			# Notify of change
			self.changed.emit( self._mesh_data )
		
		# if color_dialog.exec()

	# def on_button_set_color_clicked ( self )

# class MeshWidget ( PyQt5.QtWidgets.QFrame )