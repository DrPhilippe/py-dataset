# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pyui.widgets
import pytools.assertions

# ##################################################
# ###         CLASS CHOOSE-SCENE-DIALOG          ###
# ##################################################

class ChooseSceneDialog ( pyui.widgets.Dialog ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, scenes_names=[], parent=None ):
		"""
		Initialize a new isntance of the `pyui.widgets.ChooseSceneDialog` class.

		Arguments:
			self   (`pyui.widgets.ChooseSceneDialog`): Instance to intialize.
			scenes_names            (`list` of `str`): Names of the scenes.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, ChooseSceneDialog )
		assert pytools.assertions.type_is( scenes_names, list )
		assert pytools.assertions.list_items_type_is( scenes_names, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# set properties
		self._scenes_names = list(scenes_names)
		count = len(scenes_names)
		self._scene_name = scenes_names[ count-1 ] if count > 0 else ''

		# init the dialog
		super( ChooseSceneDialog, self ).__init__( 'Choose Scene', 'Load', 'Cancel', parent )
		self.ok_button.setEnabled( count > 0 )

	# def __init__ ( self, parent )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def scene_name ( self ):
		"""
		Name of the scene to load/create (`str`)
		"""
		return self._scene_name

	# --------------------------------------------------

	@property
	def scenes_names ( self ):
		return self._scenes_names
	
	# --------------------------------------------------

	@property
	def name_lineedit ( self ):
		return self._name_lineedit

	# --------------------------------------------------

	@property
	def scenes_names_listwidget ( self ):
		return self._scenes_names_listwidget

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup_form ( self ):
		"""
		Setup the contents of this dialog window.

		Arguments:
			self (`pyui.widgets.ChooseSceneDialog`): Dialog window to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, ChooseSceneDialog )

		self._name_lineedit = PyQt5.QtWidgets.QLineEdit( self._scene_name, self )
		self._name_lineedit.textEdited.connect( self.on_scene_name_edited )
		self.layout().addWidget( self._name_lineedit )

		self._scenes_names_listwidget = PyQt5.QtWidgets.QListWidget( self )
		self._scenes_names_listwidget.addItems( self._scenes_names )
		self._scenes_names_listwidget.currentTextChanged.connect( self.on_scene_name_selected )
		self.layout().addWidget( self._scenes_names_listwidget )

	# def setup_form ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_scene_name_edited ( self, scene_name ):
		assert pytools.assertions.type_is_instance_of( self, ChooseSceneDialog )
		assert pytools.assertions.type_is( scene_name, str )

		self._scene_name = scene_name

		if scene_name in self._scenes_names:
			scene_index = self._scenes_names.index( scene_name )
			self._scenes_names_listwidget.blockSignals( True )
			self._scenes_names_listwidget.setCurrentRow( scene_index )
			self._scenes_names_listwidget.blockSignals( False )
			self.ok_button.setText( 'Load' )

		else:
			self.ok_button.setText( 'Create' )

		self.ok_button.setEnabled( True )


	# def on_scene_name_edited ( self, str )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_scene_name_selected ( self, scene_name ):
		assert pytools.assertions.type_is_instance_of( self, ChooseSceneDialog )
		assert pytools.assertions.type_is( scene_name, str )

		self._scene_name = scene_name

		self._name_lineedit.blockSignals( True )
		self._name_lineedit.setText( scene_name )
		self._name_lineedit.blockSignals( False )
		self.ok_button.setText( 'Load' )
		self.ok_button.setEnabled( True )

	# def on_scene_name_selected ( self, scene_name )

# class ChooseSceneDialog ( pyui.widgets.Dialog )