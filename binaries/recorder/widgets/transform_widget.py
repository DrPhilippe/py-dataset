# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets
import transforms3d

# INTERNALS
import pytools.assertions
import pyui.widgets

# LOCALS
from .vector_widget import VectorWidget

# ##################################################
# ###           CLASS TRANSFORM-WIDGET           ###
# ##################################################

class TransformWidget ( PyQt5.QtWidgets.QFrame ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, value=None, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, TransformWidget )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.equal( len(value), 16 )
		assert pytools.assertions.list_items_type_is( value, float )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( TransformWidget, self ).__init__( parent )
		self._value = value
		self.setup()

	# def __init__ ( self, pixmap, value, unit, parent )
	
	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	value_changed = PyQt5.QtCore.pyqtSignal()
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def value ( self ):
		assert pytools.assertions.type_is_instance_of( self, TransformWidget )

		return self._value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, TransformWidget )
		assert pytools.assertions.type_is( val, list )
		assert pytools.assertions.list_items_type_is( list(val), float )
		assert pytools.assertions.equal( len(val), 16 )

		self._value = val

		t, r, z = self._decompose( val )
		self._t_vectorwidget.value = t
		self._r_vectorwidget.value = r
		self._z_vectorwidget.value = z

	# def value ( self, val )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def _compose ( self, t, r, z ):

		t = numpy.asarray( t, dtype=numpy.float32 )
		t = numpy.reshape( t, [3] )
		r = numpy.asarray( r, dtype=numpy.float32 )
		r = numpy.reshape( r, [3] )
		z = numpy.asarray( z, dtype=numpy.float32 )
		z = numpy.reshape( z, [3] )
		

		r = r * (numpy.pi / 180.0)
		R = transforms3d.euler.euler2mat( *r.ravel().tolist() )

		T = transforms3d.affines.compose( t, R, z )

		return T.ravel().tolist()

	# def _decompose ( self, value )

	# --------------------------------------------------

	def _decompose ( self, value ):

		T = numpy.asarray( value, dtype=numpy.float32 )
		T = numpy.reshape( T, [4, 4] )

		# decompose into tranlation / rotation / scale / sheer
		t, R, z, s = transforms3d.affines.decompose( T )
		
		# Convert rotation to euler in degrees
		r = transforms3d.euler.mat2euler( R )
		r = numpy.asarray(r) * (180.0 / numpy.pi)

		return t.ravel().tolist(), r.ravel().tolist(), z.ravel().tolist()

	# def _decompose ( self, value )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, TransformWidget )

		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 4 )
		self.setLayout( layout )

		t, r, z = self._decompose( self._value )

		self._t_vectorwidget = VectorWidget(
			pyui.widgets.Application.instance().assets.pixmap( 'icon_translate.png' ),
			t,
			'm',
			self
			)
		self._t_vectorwidget.value_changed.connect( self.on_vector_changed )
		layout.addWidget( self._t_vectorwidget )

		self._r_vectorwidget = VectorWidget(
			pyui.widgets.Application.instance().assets.pixmap( 'icon_rotate.png' ),
			r,
			'°',
			self
			)
		self._r_vectorwidget.value_changed.connect( self.on_vector_changed )
		layout.addWidget( self._r_vectorwidget )

		self._z_vectorwidget = VectorWidget(
			pyui.widgets.Application.instance().assets.pixmap( 'icon_scale.png' ),
			z,
			'/1',
			self
			)
		self._z_vectorwidget.value_changed.connect( self.on_vector_changed )
		layout.addWidget( self._z_vectorwidget )

	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_vector_changed ( self ):
		assert pytools.assertions.type_is_instance_of( self, TransformWidget )

		t = self._t_vectorwidget.value
		r = self._r_vectorwidget.value
		z = self._z_vectorwidget.value
		self._value = self._compose( t, r, z )
		self.value_changed.emit()

	# def on_vector_changed ( self )

# class TransformWidget ( PyQt5.QtWidgets.QFrame )