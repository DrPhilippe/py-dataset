# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import cv2
import numpy
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.data
import pydataset.dataset
import pydataset.cameras
import pytools.assertions
import pyui.inspector
import pyui.widgets

# LOCALS
from .tab import Tab

# ##################################################
# ###           CLASS CALIBRATION-TAB            ###
# ##################################################

class CalibrationTab ( Tab ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		"""
		Initialize a new isntance of the `recorder.widgets.CalibrationTab` class.

		Arguments:
			self  (`recorder.widgets.CalibrationTab`): Instance to intialize.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Init fields
		self._dataset             = None
		self._target_group        = None
		self._board_data          = None
		self._camera_name         = None
		self._camera_settings     = None
		self._calibrator_settings = None
		self._cam_k               = None
		self._cam_dist_coef       = None

		# Init the tab
		super( CalibrationTab, self ).__init__( 'Calibration', parent )

	# def __init__ ( self, title, text_ok, text_cancel, parent )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )
		
		# Set in the parent class
		super( CalibrationTab, self ).set_target_dataset( dataset )
		
		# Reset fields
		self._dataset             = dataset
		self._target_group        = None
		self._board_data          = None
		self._camera_name         = None
		self._camera_settings     = None
		self._calibrator_settings = None
		self._cam_k               = None
		self._cam_dist_coeffs     = None

		# Disable the UI
		self._home_button.setEnabled( False )
		self.setEnabled( False )

		# If the dataset is not None
		if dataset is None:
			return

		# Check that the charuco board has been setup
		if dataset.contains_feature( 'charuco_board', pydataset.dataset.SerializableFeatureData ):
			self._board_data = dataset.get_feature( 'charuco_board' ).value
			if not self._board_data:
				return
		else:
			return

		# Check that the camera name is setup
		if dataset.contains_feature( 'camera_name', pydataset.dataset.TextFeatureData ):
			self._camera_name = dataset.get_feature( 'camera_name' ).value
			if not self._camera_name:
				return
		else:
			return

		# Check that the camera settings are setup
		if dataset.contains_feature( 'camera_settings', pydataset.dataset.SerializableFeatureData ):
			self._camera_settings = dataset.get_feature( 'camera_settings' ).value
			if not self._camera_settings:
				return
		else:
			return
		
		# Get / Create calibration settings feature
		if dataset.contains_feature( 'calibrator_settings', pydataset.dataset.SerializableFeatureData ):
			self._calibrator_settings = dataset.get_feature( 'calibrator_settings' ).value
		
		else:
			self._calibrator_settings = pydataset.manipulators.charuco.CalibratorSettings(
				      board_data_feature_name = 'charuco_board',
				           image_feature_name = 'color',
				 markers_corners_feature_name = 'markers_corners',
				   board_corners_feature_name = 'board_corners',
				           url_search_pattern = 'groups/calibration/examples/*',
				    maximum_number_of_samples = 100,
				      url_calibration_example = '', # empty means to use the dataset
				   camera_matrix_feature_name = 'camera_K',
				     dist_coeffs_feature_name = 'camera_dist_coeffs',
				                         name = 'charuco-calibrator'
				  	)
			dataset.create_feature(
				     feature_name = 'calibrator_settings',
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				            value = self._calibrator_settings
				)

		# Get/Create the target group
		self._target_group = dataset.get_or_create_group( 'calibration', auto_update=True )

		# Enable UI
		self._set_camera()
		self._update_results_ui() # K, coeff loaded here
		self._update_examples_listwidget()
		self._button_calibrate.setEnabled( self._target_group.number_of_examples > 10 )
		self._calibration_settings_inspector.set_value( self._calibrator_settings )
		self._home_button.setEnabled( True )
		self.setEnabled( True )

		# if dataset

	# def set_target_dataset ( self, dataset )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def _set_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )
		
		cameras_names = pydataset.cameras.CameraFactory.get_cameras_names()
		
		# Stop previous camera
		if self._camera_preview.is_playing():
			self._camera_preview.stop_camera()
			self._camera_preview.set_camera( None )
		
		# Delete previous camera
		self._camera = None
		
		# If not camera name/settings are given
		if not self._camera_name or not self._camera_settings:
			
			# Display error
			popup = pyui.widgets.InfoDialog(
				 title = 'Camera is not setup',
				  text = 'The camera is not setup, this should not have happend.',
				parent = self
				)
			popup.exec()
			return

		# If the camera is not supported
		if self._camera_name not in cameras_names:

			# Display error
			popup = pyui.widgets.InfoDialog(
				 title = 'Unsupported Camera',
				  text = 'The camera named "{}" is not supported'.format( self._camera_name ),
				parent = self
				)
			popup.exec()
			return

		# If the camera is supported
		else:		
			# Log file
			logger = pytools.tasks.file_logger( 'logs/recorder.calibration_tab.camera.log' )

			# Instantiate the camera
			self._camera = pydataset.cameras.CameraFactory.instantiate_camera( self._camera_name, settings=self._camera_settings, logger=logger )
				
			# Update UI
			self._camera_preview.set_camera( self._camera )

		# if not self._camera_name or not self._camera_settings

	# def _set_camera ( self )
	
	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this tab widgets.

		Arguments:
			self (`recorder.widgets.CalibrationTab`): CalibrationTab to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		if self._camera_preview.is_playing:
			self._camera_preview.stop_camera()
			
		super( CalibrationTab, self ).close()

	# def close ( self )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the contents of this dialog window.

		Arguments:
			self (`recorder.widgets.CalibrationTab`): Tab to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )
		
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Camera Preview
		self._camera_preview = pydataset.cameras.CameraPreview( None, 15, self )
		self._camera_preview.camera_started.connect( self.on_camera_started )
		self._camera_preview.camera_stopped.connect( self.on_camera_stopped )
		self._camera_preview.set_frame_mapper( self.on_preview_frame )
		layout.addWidget( self._camera_preview, 0 )

	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_widget.setLayout( edition_layout )

		# Edition Widget / Calibration Examples Label
		label = pyui.widgets.TitleLabel( 'Calibration Examples', edition_widget )
		edition_layout.addWidget( label, 0 )

		# Edition Widget / Examples ListWidget
		self._examples_listwidget = PyQt5.QtWidgets.QListWidget( edition_widget )
		self._examples_listwidget.currentTextChanged.connect( self.on_example_clicked )
		edition_layout.addWidget( self._examples_listwidget, 0 )

		# Edition Widget / Button Delete
		self._button_delete = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_trash.png' ),
			'',
			edition_widget
			)
		self._button_delete.setEnabled( False )
		self._button_delete.clicked.connect( self.on_button_delete_clicked )
		edition_layout.addWidget( self._button_delete, 0 )

		# Edition Widget / Button Capture
		self._button_capture = PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_camera.png' ),
			'',
			edition_widget
			)
		self._button_capture.setEnabled( False )
		self._button_capture.clicked.connect( self.on_button_capture_clicked )
		edition_layout.addWidget( self._button_capture, 0 )
		
		# Edition Widget / Calibration Settings Label
		label = pyui.widgets.TitleLabel( 'Calibration Settings', edition_widget )
		edition_layout.addWidget( label, 0 )
		
		# Edition Widget / Calibration Settings Inspector
		self._calibration_settings_inspector = pyui.inspector.InspectorWidget(
			      value = self._calibrator_settings,
			is_editable = True,
			    is_root = True,
			    ignores = ['url_search_pattern', 'url_calibration_example', 'name'],
			     parent = edition_widget
			)
		edition_layout.addWidget( self._calibration_settings_inspector, 0 )
		
		# Edition Widget / Calibrate Label
		label = pyui.widgets.TitleLabel( 'Run Calibration', edition_widget )
		edition_layout.addWidget( label, 0 )

		# Edition Widget / Button Calibrate
		self._button_calibrate = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogApplyButton ),
			'Calibrate',
			edition_widget
			)
		self._button_calibrate.setEnabled( False )
		self._button_calibrate.clicked.connect( self.on_button_calibrate_clicked )
		edition_layout.addWidget( self._button_calibrate, 0 )		
		
		# Edition Widget / Progress Bar
		self._progress_bar = PyQt5.QtWidgets.QProgressBar( edition_widget )
		self._progress_bar.setRange( 0, 100 )
		self._progress_bar.setValue( 0 )
		edition_layout.addWidget( self._progress_bar, 0 )

		# Strech
		edition_layout.addStretch()

		# Edition Widget / Results
		label = pyui.widgets.TitleLabel( 'Calibration Results', edition_widget )
		edition_layout.addWidget( label, 0 )

		result_layout = PyQt5.QtWidgets.QFormLayout()
		edition_layout.addLayout( result_layout, 0 )

		# Edition Widget / Results / K
		self._k_item_widget = pyui.inspector.NDArrayItemWidget( value=numpy.zeros( [3,3], dtype=numpy.float32 ), is_editable=True, parent=edition_widget )
		self._k_item_widget.value_changed.connect( self.on_value_changed )
		result_layout.addRow( 'K', self._k_item_widget )

		# Edition Widget / Results / dist_coeffs
		self._d_item_widget = pyui.inspector.NDArrayItemWidget( value=numpy.zeros( [5], dtype=numpy.float32 ), is_editable=True, parent=edition_widget )
		self._d_item_widget.value_changed.connect( self.on_value_changed )
		result_layout.addRow( 'dist_coeffs', self._d_item_widget )

		# Edition Widget / Button Box
		button_box_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		button_box_layout = PyQt5.QtWidgets.QHBoxLayout( button_box_widget )
		button_box_layout.setContentsMargins( 2, 2, 2, 2 )
		button_box_layout.setSpacing( 2 )
		button_box_widget.setLayout( button_box_layout )
		edition_layout.addWidget( button_box_widget, 0 )

		# Edition Widget / Button Box / Button Keep Changes
		self._button_save_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogApplyButton ),
			'Save Changes',
			button_box_widget
			)
		self._button_save_changes.setEnabled( False )
		self._button_save_changes.clicked.connect( self.on_save_changes )
		button_box_layout.addWidget( self._button_save_changes, 1 )

		# Edition Widget / Button Box / Button Discard Changes
		self._button_discard_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogCancelButton ),
			'Discard Changes',
			button_box_widget
			)
		self._button_discard_changes.setEnabled( False )
		self._button_discard_changes.clicked.connect( self.on_discard_changes )
		button_box_layout.addWidget( self._button_discard_changes, 1 )

		# Update list of examples
		self._update_examples_listwidget()

		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_calibrate.png' ),
			'Record the calibration sequence'
			)

	# def setup_home_button ( self )

	# --------------------------------------------------

	def _update_examples_listwidget ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		self._examples_listwidget.blockSignals( True )
		self._examples_listwidget.clear()
		if self._target_group:
			self._examples_listwidget.addItems( self._target_group.examples_names )
		self._examples_listwidget.blockSignals( False )
		
		self._button_delete.setEnabled( False )

	# def _update_examples_listwidget ( self )

	# --------------------------------------------------

	def _update_results_ui ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		self._cam_k = numpy.zeros( [3,3], dtype=numpy.float32 )
		self._cam_dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )

		if self._dataset:
			
			# Get K if it exists
			if self._dataset.contains_feature( self._calibrator_settings.camera_matrix_feature_name, pydataset.dataset.NDArrayFeatureData ):
				self._cam_k = self._dataset.get_feature( self._calibrator_settings.camera_matrix_feature_name ).value

			# Get Dist Coeffs if they exists
			if self._dataset.contains_feature( self._calibrator_settings.dist_coeffs_feature_name, pydataset.dataset.NDArrayFeatureData ):
				self._cam_dist_coeffs = self._dataset.get_feature( self._calibrator_settings.dist_coeffs_feature_name ).value

		self._k_item_widget.value = copy.deepcopy( self._cam_k )
		self._d_item_widget.value = copy.deepcopy( self._cam_dist_coeffs )

		self._button_discard_changes.setEnabled( False )
		self._button_save_changes.setEnabled( False )

	# def _update_results_ui ( self )

	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_preview_frame ( self, stream_name, frame ):

		if self._board_data:

			# Convert the image to grayscale
			gray_frame = cv2.cvtColor( frame, cv2.COLOR_BGR2GRAY )

			# Detect charuco markers
			markers_corners, markers_ids = pydataset.charuco.detect_markers(
				self._board_data,
				gray_frame
				)
			if markers_corners is None:
				return frame

			# Draw markers
			frame = pydataset.charuco.draw_markers( frame, markers_corners )

			# Interpolate charuco board corners
			charuco_corners, charuco_ids = pydataset.charuco.interpolate_corners(
				self._board_data,
				gray_frame,
				markers_corners,
				markers_ids,
				self._cam_k,
				self._cam_dist_coeffs
				)
			if charuco_corners is None:
				return frame

			# Draw charuco markers
			frame = pydataset.charuco.draw_board_corners( frame, charuco_corners )

			# charuco pose
			if self._cam_k is None or self._cam_dist_coeffs is None:
				return frame
			
			# Estimate charuco board pose
			ok, r, t = pydataset.charuco.estimate_pose( self._board_data, charuco_corners, charuco_ids, self._cam_k, self._cam_dist_coeffs )
			if not ok:
				return frame

			# Draw baord axis
			frame = pydataset.charuco.draw_axis( frame, self._cam_k, self._cam_dist_coeffs, r, t, 0.2 )
		
		return frame

	# def on_preview_frame ( self, stream_name, frame )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_started ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		self._button_capture.setEnabled( True )
		self._examples_listwidget.setEnabled( False )

	# def on_camera_started ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_stopped ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		self._button_capture.setEnabled( False )
		self._examples_listwidget.setEnabled( True )

	# def on_camera_stopped ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( str )
	def on_example_clicked ( self, example_name ):

		if example_name:
			# Get the example
			example = self._target_group.get_example( example_name )

			# Get its image
			image = example.get_feature( self._calibrator_settings.image_feature_name ).value

			# Preview it
			self._camera_preview.set_frame( image, '' )

			# Enable the button to delete the example
			self._button_delete.setEnabled( True )

	# def on_example_clicked ( self, example_name )

	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot()
	def on_button_delete_clicked ( self ):

		# Get the name of the example to delete
		example_name = self._examples_listwidget.currentItem().text()

		# Delete it
		self._target_group.delete_example( example_name )

		# Clear the camera preview
		self._camera_preview.clear_preview()

		# Update the list of examples
		self._update_examples_listwidget()

	# def on_button_delete_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_capture_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		# Create an run a recorder to record one frame
		recorder = pydataset.cameras.DatasetRecorder.create(
			                   camera = self._camera,
			                dst_group = self._target_group,
			maximum_number_of_records = 1,
			       records_group_name = '',
			              target_type = pydataset.cameras.TargetType.examples
			)
		recorder.execute()

		self._button_calibrate.setEnabled( self._target_group.number_of_examples > 10 )
		self._update_examples_listwidget()
		
	# def on_button_capture_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_button_calibrate_clicked ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		# Save the calibration settings
		feature = self._target_group.get_feature( 'calibrator_settings' )
		feature.value = self._calibrator_settings
		feature.update()

		# Create the charuco calibrator
		calibrator = pydataset.manipulators.charuco.Calibrator( self._calibrator_settings )

		# Apply it
		self._manager = pydataset.manipulators.apply_async(
			              manipulator = calibrator,
			url_search_pattern_prefix = self._dataset.absolute_url,
			         progress_tracker = pytools.tasks.ProgressTracker(),
			     wait_for_application = False
			)

		# Update UI while its running
		self._timer = PyQt5.QtCore.QTimer()
		self._timer.timeout.connect( self.update_ui_while_calibrating )
		self._timer.start(30)

	# def on_button_calibrate_clicked ( self )

	# --------------------------------------------------


	@PyQt5.QtCore.pyqtSlot()
	def on_value_changed ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )
		
		self._cam_k           = copy.deepcopy( self._k_item_widget.value )
		self._cam_dist_coeffs = copy.deepcopy( self._d_item_widget.value )

		self._button_discard_changes.setEnabled( True )
		self._button_save_changes.setEnabled( True )

	# def on_value_changed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_discard_changes ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		self._update_results_ui()

	# def on_discard_changes ( self )

	# --------------------------------------------------
	
	@PyQt5.QtCore.pyqtSlot()
	def on_save_changes ( self ):
		assert pytools.assertions.type_is_instance_of( self, CalibrationTab )

		# Save parameters
		self._dataset.create_or_update_feature(
			     feature_name = self._calibrator_settings.camera_matrix_feature_name,
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = self._cam_k

			)
		self._dataset.create_or_update_feature(
			     feature_name = self._calibrator_settings.dist_coeffs_feature_name,
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			            value = self._cam_dist_coeffs
			)

		self._button_discard_changes.setEnabled( False )
		self._button_save_changes.setEnabled( False )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def update_ui_while_calibrating ( self ):

		task = self._manager.tasks[ 0 ]

		self._progress_bar.setValue( int(task.progress_tracker.progress*100) )

		if task.state == pytools.tasks.TaskState.running:
			self._button_capture.setEnabled( False )
			self._button_calibrate.setEnabled( False )

		elif task.state == pytools.tasks.TaskState.stopped:
			self._button_capture.setEnabled( True )
			self._button_calibrate.setEnabled( True )
			self._manager.join_tasks()
			self._timer.stop()
			self.dataset_modified.emit()

		elif task.state == pytools.tasks.TaskState.failed:
			self._button_capture.setEnabled( True )
			self._button_calibrate.setEnabled( True )
			self._manager.join_tasks()
			self._timer.stop()

	# def update_ui_while_calibrating ( self )

# class CharucoTab ( CalibrationTab )