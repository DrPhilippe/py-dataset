# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.widgets

# LOCALS
from .tab import Tab

# ##################################################
# ###               CLASS HOME-TAB               ###
# ##################################################

class HomeTab ( Tab ):

	# ##################################################
	# ###                 CONSTATNTS                 ###
	# ##################################################

	# --------------------------------------------------

	BUTTON_STYLESHEET = '''
		text-align: left;
		padding: 4px;
		'''

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	home_button_clicked = PyQt5.QtCore.pyqtSignal( int )

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, HomeTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Initialize the tab
		super( HomeTab, self ).__init__( 'Home', parent )
		
	# def __init__ ( parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def add_button ( self, button, tab_index, enabled ):
		assert pytools.assertions.type_is_instance_of( self, HomeTab )
		assert pytools.assertions.type_is_instance_of( button, PyQt5.QtWidgets.QPushButton )
		assert pytools.assertions.type_is( tab_index, int )
		assert pytools.assertions.type_is( enabled, bool )

		# Prepare the button
		button.setMinimumSize( button.minimumWidth(), 40 )
		button.setIconSize( PyQt5.QtCore.QSize(32, 32) )
		button.setStyleSheet( HomeTab.BUTTON_STYLESHEET )
		button.clicked.connect( lambda: self.home_button_clicked.emit(tab_index) )
		button.setEnabled( enabled )

		# Set the edition widget of the home tab as parent of the button
		button.setParent( self.edition_widget )

		# Add the button in the edition widget layout
		edition_layout = self.edition_widget.layout()
		edition_layout.insertWidget( edition_layout.count()-1, button )

	# def add_button ( self, button )

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, HomeTab )

		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		layout.addStretch( 1 )

		label = PyQt5.QtWidgets.QLabel( self )
		label.setPixmap( pyui.widgets.Application.instance().assets.pixmap( 'logo_iteca.png' ) )
		label.setScaledContents( False )
		layout.addWidget( label )

		layout.addStretch( 1 )
		
	# def setup ( self )
		
	# --------------------------------------------------

	def setup_edition_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, HomeTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_layout.setContentsMargins( 8, 8, 8, 8 )
		edition_layout.setSpacing( 8 )
		edition_widget.setLayout( edition_layout )
		
		# Edition Widget / Spacer
		edition_layout.addStretch( 1 )

		# Edition Widget / Spacer
		edition_layout.addStretch( 2 )

		# Return it
		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		
		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_new.png' ),
			'Create/Open a dataset'
			)

	# def setup_home_button ( self )

# class HomeTab ( Tab )