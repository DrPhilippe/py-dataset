# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import numpy
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions

# ##################################################
# ###            CLASS VECTOR-WIDGET             ###
# ##################################################

class VectorWidget ( PyQt5.QtWidgets.QFrame ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, pixmap=None, value=None, unit='m', parent=None ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )
		assert pytools.assertions.type_is_instance_of( pixmap, PyQt5.QtGui.QPixmap )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, float )
		assert pytools.assertions.equal( len(value), 3 )
		assert pytools.assertions.type_is( unit, str )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		super( VectorWidget, self ).__init__( parent )

		self._pixmap = pixmap
		self._value  = value
		self._unit   = unit

		self.setup()

	# def __init__ ( self, pixmap, value, unit, parent )

	# ##################################################
	# ###                  SIGNALS                   ###
	# ##################################################

	# --------------------------------------------------

	value_changed = PyQt5.QtCore.pyqtSignal()

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def pixmap ( self ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )

		return self._pixmap

	# def pixmap ( self )

	# --------------------------------------------------

	@pixmap.setter
	def pixmap ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )
		assert pytools.assertions.type_is_instance_of( val, (PyQt5.QtGui.QPixmap) )

		self._pixmap = value

		self._pixmap_label.setPixmap( value )

	# def pixmap ( self, value )

	# --------------------------------------------------

	@property
	def value ( self ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )

		return self._value

	# def value ( self )

	# --------------------------------------------------

	@value.setter
	def value ( self, val ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )
		assert pytools.assertions.type_is( val, list )
		assert pytools.assertions.list_items_type_is( val, float )
		assert pytools.assertions.equal( len(val), 3 )

		self._value = val

		self._spin_box_0.setValue( val[ 0 ] )
		self._spin_box_1.setValue( val[ 1 ] )
		self._spin_box_2.setValue( val[ 2 ] )

	# def value ( self, val )
	
	# --------------------------------------------------

	@property
	def unit ( self ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )

		return self._unit

	# def unit ( self )

	# --------------------------------------------------

	@unit.setter
	def unit ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )
		assert pytools.assertions.type_is( value, str )

		self._unit = value
		self._spin_box_0.setSuffix( ' '+value )
		self._spin_box_1.setSuffix( ' '+value )
		self._spin_box_2.setSuffix( ' '+value )

	# def unit ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )

		layout = PyQt5.QtWidgets.QHBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 4 )
		self.setLayout( layout )

		self._pixmap_label = PyQt5.QtWidgets.QLabel( self )
		self._pixmap_label.setPixmap( self._pixmap )
		self._pixmap_label.setScaledContents( True )
		self._pixmap_label.setMinimumSize( 22, 22 )
		self._pixmap_label.setMaximumSize( 22, 22 )
		layout.addWidget( self._pixmap_label, 0 )
		
		self._spin_box_0 = PyQt5.QtWidgets.QDoubleSpinBox( self )
		self._spin_box_0.setMinimum( -1_000_000.0 )
		self._spin_box_0.setMaximum( 1_000_000.0 )
		self._spin_box_0.setDecimals( 4 )
		self._spin_box_0.setSuffix( ' '+self._unit )
		self._spin_box_0.setValue( self._value[ 0 ] )
		self._spin_box_0.setMinimumSize( 40, 22 )
		self._spin_box_0.setMaximumSize( self._spin_box_0.maximumWidth(), 22 )
		self._spin_box_0.valueChanged.connect( self.on_value_changed )
		layout.addWidget( self._spin_box_0, 1 )
		
		self._spin_box_1 = PyQt5.QtWidgets.QDoubleSpinBox( self )
		self._spin_box_1.setMinimum( -1_000_000.0 )
		self._spin_box_1.setMaximum( 1_000_000.0 )
		self._spin_box_1.setDecimals( 4 )
		self._spin_box_1.setSuffix( ' '+self._unit )
		self._spin_box_1.setValue( self._value[ 1 ] )
		self._spin_box_1.setMinimumSize( 40, 22 )
		self._spin_box_1.setMaximumSize( self._spin_box_1.maximumWidth(), 22 )
		self._spin_box_1.valueChanged.connect( self.on_value_changed )
		layout.addWidget( self._spin_box_1, 1 )
		
		self._spin_box_2 = PyQt5.QtWidgets.QDoubleSpinBox( self )
		self._spin_box_2.setMinimum( -1_000_000.0 )
		self._spin_box_2.setMaximum( 1_000_000.0 )
		self._spin_box_2.setDecimals( 4 )
		self._spin_box_2.setSuffix( ' '+self._unit )
		self._spin_box_2.setValue( self._value[ 2 ] )
		self._spin_box_2.setMinimumSize( 40, 22 )
		self._spin_box_2.setMaximumSize( self._spin_box_2.maximumWidth(), 22 )
		self._spin_box_2.valueChanged.connect( self.on_value_changed )
		layout.addWidget( self._spin_box_2, 1 )

	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( float )
	def on_value_changed ( self, value ):
		assert pytools.assertions.type_is_instance_of( self, VectorWidget )
		assert pytools.assertions.type_is( value, float )
		self._value[ 0 ] = self._spin_box_0.value()
		self._value[ 1 ] = self._spin_box_1.value()
		self._value[ 2 ] = self._spin_box_2.value()
		self.value_changed.emit()
	# def on_value_changed ( self, value )

# class VectorWidget ( PyQt5.QtWidgets.QFrame )