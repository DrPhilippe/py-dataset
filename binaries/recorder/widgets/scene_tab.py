# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import cv2
import numpy
import PyQt5.QtCore
import PyQt5.QtGui
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pydataset.cameras
import pydataset.charuco
import pytools.assertions
import pyui.widgets

# LOCALS
from .choose_scene_dialog import ChooseSceneDialog
from .scene_widget        import SceneWidget
from .tab                 import Tab

# ##################################################
# ###              CLASS SCENE-TAB               ###
# ##################################################

class SceneTab ( Tab ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		"""
		Initialize a new isntance of the `recorder.widgets.SceneTab` class.

		Arguments:
			self        (`recorder.widgets.SceneTab`): Instance to intialize.
			parent (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		
		# Dataset properties
		self._dataset      = None
		self._scene_group  = None
		self._scene        = None
		self._render       = None
		self._charuco_mesh = None

		# Camera properties
		self._camera_name     = ''
		self._camera_settings = None
		self._camera          = None
		self._cam_K           = None
		self._cam_dist_coeffs = None

		# Charuco properties
		self._board_data = None

		# Init the tab
		super( SceneTab, self ).__init__( 'Scene', parent )
		
	# def __init__ ( self, camera_name, camera_settings, parent )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )
		
		# Set in the parent class
		super( SceneTab, self ).set_target_dataset( dataset )

		# Dataset properties
		self._dataset        = dataset
		self._scene_group    = None
		self._scene          = None
		self._viewport.scene = None
		self._charuco_mesh   = None

		# Camera properties
		self._camera_name     = ''
		self._camera_settings = None
		self._camera          = None
		self._cam_K           = None
		self._cam_dist_coeffs = None

		# Charuco properties
		self._board_data = None

		# If dataset is not None
		if dataset:
			
			# Check that the charuco board has been setup
			if dataset.contains_feature( 'charuco_board', pydataset.dataset.SerializableFeatureData ):
				self._board_data = dataset.get_feature( 'charuco_board' ).value
				if not self._board_data:
					return
			else:
				return

			# Check that the camera name is setup
			if dataset.contains_feature( 'camera_name', pydataset.dataset.TextFeatureData ):
				self._camera_name = dataset.get_feature( 'camera_name' ).value
				if not self._camera_name:
					return
			else:
				return

			# Check that the camera settings are setup
			if dataset.contains_feature( 'camera_settings', pydataset.dataset.SerializableFeatureData ):
				self._camera_settings = dataset.get_feature( 'camera_settings' ).value
				if not self._camera_settings:
					return
			else:
				return

			# Check that the calibrator settings are defined
			if dataset.contains_feature( 'calibrator_settings', pydataset.dataset.SerializableFeatureData ):
				self._calibrator_settings = dataset.get_feature( 'calibrator_settings' ).value
				if not self._calibrator_settings:
					return
			else:
				return

			# Check that the cam K is computed
			if dataset.contains_feature( self._calibrator_settings.camera_matrix_feature_name, pydataset.dataset.NDArrayFeatureData ):
				self._cam_K = dataset.get_feature( self._calibrator_settings.camera_matrix_feature_name ).value
				if self._cam_K is None:
					return

			else:
				return

			# Check that the cam dist_coeffs is computed
			if dataset.contains_feature( self._calibrator_settings.dist_coeffs_feature_name, pydataset.dataset.NDArrayFeatureData ):
				self._cam_dist_coeffs = dataset.get_feature( self._calibrator_settings.dist_coeffs_feature_name ).value
				if self._cam_dist_coeffs is None:
					return
			else:
				return

			# Set the camera in the UI
			self._set_camera()

			# Enable UI
			self._button_choose.setEnabled( True )
			self._home_button.setEnabled( True )
			self.setEnabled( True )

		# If the dataset is None
		else:
			# Set the camera in the UI
			self._set_camera()

			# Disable UI
			self._button_choose.setEnabled( False )
			self._home_button.setEnabled( False )
			self.setEnabled( False )

	# def set_target_dataset ( self, dataset )

	# --------------------------------------------------

	def _set_camera ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		
		cameras_names = pydataset.cameras.CameraFactory.get_cameras_names()
		
		# Stop previous camera
		if self._camera_preview.is_playing():
			self._camera_preview.stop_camera()
			self._camera_preview.set_camera( None )
		
		# Delete previous camera
		self._camera = None
		
		# If not camera name/settings are given
		if not self._camera_name or not self._camera_settings:
			
			# Display error
			popup = pyui.dialogs.InfoDialog(
				 title = 'Camera is not setup',
				  text = 'The camera is not setup, this should not have happend.',
				parent = self
				)
			popup.exec()
			return

		# If the camera is not supported
		if self._camera_name not in cameras_names:

			# Display error
			popup = pyui.dialogs.InfoDialog(
				 title = 'Unsupported Camera',
				  text = 'The camera named "{}" is not supported'.format( self._camera_name ),
				parent = self
				)
			popup.exec()
			return

		# If the camera is supported
		else:		
			# Log file
			logger = pytools.tasks.file_logger( 'logs/recorder.scene_tab.camera.log' )

			# Instantiate the camera
			self._camera = pydataset.cameras.CameraFactory.instantiate_camera( self._camera_name, settings=self._camera_settings, logger=logger )
				
			# Update UI
			self._camera_preview.set_camera( self._camera )

		# if not self._camera_name or not self._camera_settings

	# def _set_camera ( self )

	# --------------------------------------------------

	def _update_scene ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		# Create the scene
		self._scene = pydataset.render.Scene.create( name=self._scene_group.name )
		self._scene.camera = pydataset.render.cameras.OpenCVCamera.create(
			                 K = self._cam_K.ravel().tolist(),
			                x1 = 0.0,    # left
			                y1 = 0.0,    # bottom
			                x2 = 1280.0, # right
			                y2 = 720.0,  # top
			         near_clip = 0.001,
			          far_clip = 1000.0,
			              name = 'OpenCV Camera',
			            parent = self._scene
			)
		opencv = pydataset.render.Entity.create(
			  name = 'OpenCV',
			parent = self._scene
			)
		opencv.local_transform = pydataset.render.numpy_utils.invert_xy()
		self._charuco_mesh = pydataset.render.CharucoMesh.from_charuco_data(
			charuco_data = self._board_data,
			        name = 'ChArUcO Board',
			 white_color = pydataset.data.ColorData( 255, 255, 255, 64 ),
			 black_color = pydataset.data.ColorData(   0,   0,   0, 64 ),
			      parent = opencv
			)

		# If the scene data is defined
		if self._scene_group.contains_feature( 'scene_data' ):

			# Reload existing meshs on the board
			scene_data  = self._scene_group.get_feature( 'scene_data' ).value
			opencv_data = scene_data.get_child( 'OpenCV' )
			board_data  = opencv_data.get_child( 'ChArUcO Board' )

			for mesh_data in board_data.children_data:

				mesh_data = copy.deepcopy( mesh_data )
				mesh      = pydataset.render.Mesh( mesh_data, parent=self._charuco_mesh )
			
			# for mesh_data in board_data.children_data

		# if self._scene_group.contains_feature( 'scene_data' )

		# def log ( e, depth=0 ):
		# 	print( '  '*depth + e.name + ', ' + str(e.local_transform) )
		# 	for c in e.children_data:
		# 		log( c, depth+1 )
		# log( self._scene.data )

		# Assign scene to viewport and scene widget
		self._viewport.scene = self._scene
		self._scene_widget.scene_data = copy.deepcopy( self._scene.data )
		self._scene_widget.setEnabled( True )

	# def _update_scene ( self )

	# --------------------------------------------------

	def close ( self ):
		"""
		Closes this tab widgets.

		Arguments:
			self (`recorder.widgets.SceneTab`): SceneTab to close.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		if self._camera_preview.is_playing:
			self._camera_preview.stop_camera()

		self._viewport.close()
			
		super( SceneTab, self ).close()

	# def close ( self )

	# --------------------------------------------------

	def resizeEvent ( self, event ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		size = self._camera_preview.display_size
		self._viewport.setMinimumSize( size )
		self._viewport.setMaximumSize( size )
		self._viewport.resize( size )
		event.accept()

	# def resizeEvent ( self, event )

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the contents of this tab.

		Arguments:
			self (`recorder.ui.SceneTab`): Tab to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		
		# Layout
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )
		
		# Camera Preview
		self._camera_preview = pydataset.cameras.CameraPreview( None, 15, self )
		self._camera_preview.setSizePolicy( PyQt5.QtWidgets.QSizePolicy.Expanding, PyQt5.QtWidgets.QSizePolicy.Preferred )
		self._camera_preview.camera_started.connect( self.on_camera_started )
		self._camera_preview.camera_stopped.connect( self.on_camera_stopped )
		self._camera_preview.set_frame_mapper( self.on_preview_frame )
		layout.addWidget( self._camera_preview )

		# Viewport
		self._viewport = pydataset.render.Viewport( None, None )
		self._viewport.setMinimumSize( self._camera_preview.display_size )
		self._viewport.setMaximumSize( self._camera_preview.display_size )
		self._viewport.resize( self._camera_preview.display_size )
		# self._viewport.show()
	
	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_widget.setLayout( edition_layout )
		
		# Edition Widget / Scene TitleLabel
		label = pyui.widgets.TitleLabel( 'Scene', edition_widget )
		edition_layout.addWidget( label, 0 )

		# Edition Widget / Button Create-Load
		self._button_choose = PyQt5.QtWidgets.QPushButton(
			'Choose Scene',
			edition_widget
			)
		self._button_choose.clicked.connect( self.on_choose_scene_clicked )
		self._button_choose.setEnabled( False )
		edition_layout.addWidget( self._button_choose )
		
		# Edition Widget / Scene Widget
		self._scene_widget = SceneWidget( None, edition_widget )
		self._scene_widget.layout().setContentsMargins( 0, 0, 0, 0 )
		self._scene_widget.setEnabled( False )
		self._scene_widget.scene_modified.connect( self.on_scene_modified )
		edition_layout.addWidget( self._scene_widget, 1 )

		# Edition Widget / Button Box
		button_box_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		button_box_layout = PyQt5.QtWidgets.QHBoxLayout( button_box_widget )
		button_box_layout.setContentsMargins( 2, 2, 2, 2 )
		button_box_layout.setSpacing( 2 )
		button_box_widget.setLayout( button_box_layout )
		edition_layout.addWidget( button_box_widget, 0 )

		# Edition Widget / Button Box / Button Keep Changes
		self._button_save_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogApplyButton ),
			'Save Changes',
			button_box_widget
			)
		self._button_save_changes.setEnabled( False )
		self._button_save_changes.clicked.connect( self.on_save_changes )
		button_box_layout.addWidget( self._button_save_changes, 1 )

		# Edition Widget / Button Box / Button Discard Changes
		self._button_discard_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogCancelButton ),
			'Discard Changes',
			button_box_widget
			)
		self._button_discard_changes.setEnabled( False )
		self._button_discard_changes.clicked.connect( self.on_discard_changes )
		button_box_layout.addWidget( self._button_discard_changes, 1 )

		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		
		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_objects.png' ),
			'Setup a scene',
			)

	# def setup_home_button ( self )

	# ##################################################
	# ###                 CALLBACKS                  ###
	# ##################################################

	# --------------------------------------------------

	def on_preview_frame ( self, stream_name, frame ):

		if stream_name != 'color':
			return frame
			
		if self._board_data and self._charuco_mesh:

			# Convert the image to grayscale
			gray_frame = cv2.cvtColor( frame, cv2.COLOR_BGR2GRAY )

			# Detect charuco markers
			markers_corners, markers_ids = pydataset.charuco.detect_markers(
				self._board_data,
				gray_frame
				)
			if markers_corners is None:
				return frame

			# Interpolate charuco board corners
			charuco_corners, charuco_ids = pydataset.charuco.interpolate_corners(
				self._board_data,
				gray_frame,
				markers_corners,
				markers_ids,
				self._cam_K,
				self._cam_dist_coeffs
				)
			if charuco_corners is None:
				return frame
				
			# Estimate charuco board pose
			ok, r, t = pydataset.charuco.estimate_pose( self._board_data, charuco_corners, charuco_ids, self._cam_K, self._cam_dist_coeffs )
			if not ok:
				return frame				
			
			# Set pose on mesh
			R, j = cv2.Rodrigues( r )
			self._charuco_mesh.local_transform = pydataset.render.numpy_utils.affine_transform( R, t )
			
			# Repaint viewport
			self._viewport.update()
			
			# Grab render
			self._render = self._viewport.grab_image( self._viewport.width(), self._viewport.height(), 4 ) # SLOW
			if self._render is None:
				return frame

			background = cv2.resize( frame, (self._render.shape[1], self._render.shape[0]) )
			foreground = self._render
			mask       = self._render[ :, :, 3 ].astype( numpy.float32 ) / 255.0

			# frame = pydataset.images_utils.past_image( foreground, background, mask, 'linear' )  # SLOW

			background = cv2.cvtColor( background, cv2.COLOR_BGR2BGRA ).astype( numpy.float32 )
			foreground = foreground.astype( numpy.float32 )
			mask       = numpy.reshape( mask, [mask.shape[0], mask.shape[1], 1] )
			mask       = numpy.repeat( mask, 4, axis=2 )
			neg_mask   = numpy.ones( mask.shape, dtype=numpy.float32 ) - mask
			frame = ((background*neg_mask) + (foreground*mask)).astype(numpy.uint8)

		return frame

	# def on_preview_frame ( self, stream_name, frame )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_started ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		size = self._camera_preview.display_size
		self._viewport.setMinimumSize( size )
		self._viewport.setMaximumSize( size )
		self._viewport.resize( size )
		self._viewport.show()

	# def on_camera_started ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_camera_stopped ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		self._viewport.hide()
		
	# def on_camera_stopped ( self )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_choose_scene_clicked ( self ):

		scenes_group = self._dataset.get_or_create_group( 'scenes' )

		dialog = ChooseSceneDialog( scenes_group.groups_names, self )
		if dialog.exec():
			self._scene_group = scenes_group.get_or_create_group( dialog.scene_name )
			self._update_scene()

	# def on_choose_scene_clicked ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_scene_modified ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		self._button_save_changes.setEnabled( True )
		self._button_discard_changes.setEnabled( True )

	# def on_scene_modified ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_save_changes ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )

		if self._scene_group.contains_feature( 'scene_data' ):
			feature = self._scene_group.get_feature( 'scene_data' )
			feature.value = self._scene_widget.scene_data
			feature.update()

		else:
			self._scene_group.create_feature(
				     feature_name = 'scene_data',
				feature_data_type = pydataset.dataset.SerializableFeatureData,
				            value = self._scene_widget.scene_data
				)

		self._update_scene()
		self._button_save_changes.setEnabled( False )
		self._button_discard_changes.setEnabled( False )

	# def on_save_changes ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_discard_changes ( self ):
		assert pytools.assertions.type_is_instance_of( self, SceneTab )
		
		self._update_scene()
		self._button_save_changes.setEnabled( False )
		self._button_discard_changes.setEnabled( False )

	# def on_discard_changes ( self )

# class SceneTab ( Tab )