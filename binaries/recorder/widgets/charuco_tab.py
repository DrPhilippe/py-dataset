# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import copy
import PyQt5.QtCore
import PyQt5.QtWidgets
import matplotlib.figure
import matplotlib.backends.backend_qt5agg # FigureCanvas, NavigationToolbar2QT

# INTERNALS
import pydataset
import pytools.assertions
import pyui.inspector
import pyui.widgets

# LOCALS
from .tab import Tab

# ##################################################
# ###             CLASS CHARUCO-TAB              ###
# ##################################################

class CharucoTab ( Tab ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		"""
		Initialize a new isntance of the `recorder.ui.CharucoTab` class.

		Arguments:
			self                      (`recorder.ui.CharucoTab`): Instance to intialize.
			parent            (`None`/`PyQt5.QtWidgets.QWidget`): Parent widget.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Set properties
		self._charuco_feature = None

		# Init the tab
		super( CharucoTab, self ).__init__( 'Charuco', parent )

		# Update charuco plot
		self.update_plot()

	# def __init__ ( self, title, text_ok, text_cancel, parent )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def charuco_board_feature ( self ):
		assert pytools.assertions.type_is_instance_of( self, Tab )
	
		return self._charuco_board_feature

	# def charuco_board_feature ( self )

	# --------------------------------------------------

	@property
	def charuco_board_data ( self ):
		assert pytools.assertions.type_is_instance_of( self, Tab )
		
		if self._charuco_board_feature:
			return copy.deepcopy( self._charuco_board_feature.value )
		else:
			return None

	# def charuco_board_data ( self )

	# ##################################################
	# ###                  SETTERS                   ###
	# ##################################################

	# --------------------------------------------------

	def set_target_dataset ( self, dataset ):
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )
		assert pytools.assertions.type_is_instance_of( dataset, (type(None), pydataset.dataset.Dataset) )
		
		# Set in the parent class
		super( CharucoTab, self ).set_target_dataset( dataset )

		# If the dataset exists
		if dataset:
			
			# If the charuco board settings feature exists
			if dataset.contains_feature( 'charuco_board', pydataset.dataset.SerializableFeatureData ):
				
				# Load it
				feature = dataset.get_feature( 'charuco_board' )

			# If the charuco board settings feature does not exist
			else:
				# Create it
				feature = dataset.create_feature(
					     feature_name = 'charuco_board',
					feature_data_type = pydataset.dataset.SerializableFeatureData,
					      auto_update = True,
					            value = pydataset.data.CharucoBoardData()
					)

			# Set the edited feature
			self._set_charuco_board_feature( feature )

			# Enable the home button			
			self._home_button.setEnabled( True )

			# Enable the tab (self)
			self.setEnabled( True )

		# If the dataset does not exist
		else:
			# Set the edited feature to None
			self._set_charuco_board_feature( None )

			# Disable the home button
			self._home_button.setEnabled( False )

			# Disable the tab (self)
			self.setEnabled( False )

	# def set_target_dataset ( self, dataset )

	# --------------------------------------------------

	def _set_charuco_board_feature ( self, feature ):
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )
		assert pytools.assertions.type_is_instance_of( feature, (type(None), pydataset.dataset.Feature) )

		# Save the feature
		self._charuco_board_feature = feature

		# Set its value in the inspector
		charuco_board_data = self.charuco_board_data
		self._inspector.set_value( charuco_board_data, charuco_board_data is not None )
		self.update_plot()

		# Reset button
		self._button_save_changes.setEnabled( False )
		self._button_discard_changes.setEnabled( False )
		self._button_update.setEnabled( False )

	# def _set_charuco_board_feature ( self, feature )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Setup the contents of this dialog window.

		Arguments:
			self (`recorder.ui.CharucoTab`): Tab to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )
		
		layout = PyQt5.QtWidgets.QVBoxLayout( self )
		layout.setContentsMargins( 0, 0, 0, 0 )
		layout.setSpacing( 0 )
		self.setLayout( layout )

		# Scroll Area
		scroll_area = PyQt5.QtWidgets.QScrollArea( self )
		layout.addWidget( scroll_area )

		# Scroll Area / Canvas
		self._figure = matplotlib.figure.Figure( figsize=(8.27, 11.69), dpi=200 )
		self._axes   = self._figure.add_subplot( ylim=(1, 0) )
		self._canvas = matplotlib.backends.backend_qt5agg.FigureCanvas( self._figure )
		scroll_area.setWidget( self._canvas )	

	# def setup ( self )

	# --------------------------------------------------

	def setup_edition_widget ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )

		# Edition Widget
		edition_widget = PyQt5.QtWidgets.QWidget()
		edition_layout = PyQt5.QtWidgets.QVBoxLayout( edition_widget )
		edition_widget.setLayout( edition_layout )

		# Edition Widget / Title
		label = pyui.widgets.TitleLabel( 'Charuco Board', edition_widget )
		edition_layout.addWidget( label, 0 )

		# Edition Widget / Inspector
		self._inspector = pyui.inspector.InspectorWidget( value=None, is_editable=True, is_root=True, ignores=[], parent=edition_widget )
		self._inspector.value_changed.connect( self.on_settings_changed )
		edition_layout.addWidget( self._inspector, 1 )

		# Edition Widget / Button Update
		self._button_update = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_BrowserReload ),
			'Update',
			edition_widget
			)
		self._button_update.clicked.connect( self.on_update )
		self._button_update.setEnabled( False )
		edition_layout.addWidget( self._button_update, 0 )

		# Edition Widget / Button Export PDF
		self._button_export_pdf = PyQt5.QtWidgets.QPushButton( 
			pyui.widgets.Application.instance().assets.icon( 'icon_save.png' ),
			'Export PDF',
			edition_widget
			)
		self._button_export_pdf.clicked.connect( self.on_export_pdf )
		edition_layout.addWidget( self._button_export_pdf, 0 )

		# Edition Widget / Button Box
		button_box_widget = PyQt5.QtWidgets.QWidget( edition_widget )
		button_box_layout = PyQt5.QtWidgets.QHBoxLayout( button_box_widget )
		button_box_layout.setContentsMargins( 2, 2, 2, 2 )
		button_box_layout.setSpacing( 2 )
		button_box_widget.setLayout( button_box_layout )
		edition_layout.addWidget( button_box_widget, 0 )

		# Edition Widget / Button Box / Button Keep Changes
		self._button_save_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogApplyButton ),
			'Save Changes',
			button_box_widget
			)
		self._button_save_changes.setEnabled( False )
		self._button_save_changes.clicked.connect( self.on_save_changes )
		button_box_layout.addWidget( self._button_save_changes, 1 )

		# Edition Widget / Button Box / Button Discard Changes
		self._button_discard_changes = PyQt5.QtWidgets.QPushButton(
			self.style().standardIcon( PyQt5.QtWidgets.QStyle.SP_DialogCancelButton ),
			'Discard Changes',
			button_box_widget
			)
		self._button_discard_changes.setEnabled( False )
		self._button_discard_changes.clicked.connect( self.on_discard_changes )
		button_box_layout.addWidget( self._button_discard_changes, 1 )
		
		return edition_widget

	# def setup_edition_widget ( self )

	# --------------------------------------------------

	def setup_home_button ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )

		return PyQt5.QtWidgets.QPushButton(
			pyui.widgets.Application.instance().assets.icon( 'icon_charuco.png' ),
			'Setup the Charuco calibration board'
			)

	# def setup_home_button ( self )

	# --------------------------------------------------

	def update_plot ( self ):
		assert pytools.assertions.type_is_instance_of( self, CharucoTab )

		self._axes.clear()

		charuco_board_data = self._inspector.value
		
		if charuco_board_data:
			pydataset.charuco.plot_charuco_board( self._axes, charuco_board_data )
		
		self._axes.axis( 'equal' )
		self._axes.axis( 'off' )
		self._axes.axis( [0.0, 8.27, 11.69, 0.0] )
		self._canvas.draw()

	# def update_plot ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_settings_changed ( self ):

		self._button_update.setEnabled( True )
		self._button_save_changes.setEnabled( True )
		self._button_discard_changes.setEnabled( True )

	# def on_settings_changed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_update ( self ):

		self.update_plot()
		self._button_update.setEnabled( False )

	# def on_update ( self )
	
	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_export_pdf ( self ):

		# Ask the filepath
		filepath, filefilter = PyQt5.QtWidgets.QFileDialog.getSaveFileName(
			self,
			'Export Charuco Board',
			'charuco.pdf',
			'Portable Document File (*.pdf)'
			)

		# Check if dialog was accepted
		if filepath:

			# Save the charuco board
			pydataset.charuco.save_charuco_board( self._inspector.value, filepath )

	# def on_export_pdf ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_save_changes ( self ):

		# Set the charuco feature value from the one in the inspector
		self._charuco_board_feature.value = copy.deepcopy( self._inspector.value )

		# Update/Save the feature
		self._charuco_board_feature.update()

		# Nodify of dataset modification
		self.dataset_modified.emit()

	# def on_save_changes ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_discard_changes ( self ):

		# Reset all the UI
		self._set_charuco_board_feature( self._charuco_board_feature )

	# def on_discard_changes ( self )

# class CharucoTab ( Tab )