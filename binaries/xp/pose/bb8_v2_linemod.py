# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.PoseExperiment ):
	"""
	This experiment applies BB8Net to estimate the pose of LINEMOD objects though control points.
	"""

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# Get a batch of input images
		inputs_image = inputs[ arguments.image_feature_name ]
		
		# Get the corresponding batch of labels
		labels_k   = labels[ arguments.camera_matrix_feature_name  ]
		labels_bb  = labels[ arguments.bounding_box_feature_name   ]
		labels_r   = labels[ arguments.rotation_feature_name       ]
		labels_t   = labels[ arguments.translation_feature_name    ]
		labels_bb8 = labels[ arguments.control_points_feature_name ]

		# Get the corresponding predicted pose
		preds_r, preds_t  = cls.get_pose( predictions, labels, arguments )
		preds_bb8 = predictions[ arguments.control_points_feature_name ]

		# Create drawings buffer
		shape        = list(inputs_image.shape)
		new_shape    = list(shape)
		new_shape[1] = 512
		new_shape[2] = 512
		drawings     = numpy.empty( new_shape, numpy.uint8 )
		scores       = numpy.empty( new_shape[1], numpy.float32 )

		# Ratio used to scale the bounsung boxes
		x_ratio  = float(new_shape[ 1 ]) / float(shape[ 1 ])
		y_ratio  = float(new_shape[ 2 ]) / float(shape[ 2 ])
		
		label_color = (112, 255, 112)
		pred_color = (112, 112, 255)

		# Draw each images
		batch_size = shape[ 0 ]
		for i in range( batch_size ):
				
			# Get the input image
			image = inputs_image[ i ]

			# Get the associated label
			label_k   =   labels_k[ i ]
			label_r   =   labels_r[ i ]
			label_t   =   labels_t[ i ]
			label_bb8 = labels_bb8[ i ]
			bb        =   labels_bb[ i ]

			# Get the associated prediction
			pred_r   = preds_r[ i ]
			pred_t   = preds_t[ i ]
			pred_bb8 = preds_bb8[ i ]

			# Get the associated result
			result = results[ i ]

			# If control point are normlize, rescale them to the image plane
			if arguments.normalize:
				label_bb8 = pydataset.pose_utils.rescale_control_points( label_bb8, 224, 224 )
				pred_bb8  = pydataset.pose_utils.rescale_control_points( pred_bb8, 224, 224 )
			
			# Convert rotation if needed
			label_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( label_r )
			pred_r  = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( pred_r )

			# Resize drawing
			image = cv2.resize( image, (512, 512) )

			# Draw label bounding box
			label_bb, j     = cv2.projectPoints( bb, label_r, label_t, label_k, numpy.zeros([5]) )
			label_bb        = numpy.reshape( label_bb, [8, 2] )
			label_bb[:, 0] *= x_ratio
			label_bb[:, 1] *= y_ratio
			image           = pydataset.cv2_drawing.draw_bounding_box( image, label_bb, label_color, 2, None )

			# Draw label control points
			label_bb8[:, 0] *= x_ratio
			label_bb8[:, 1] *= y_ratio
			image = pydataset.cv2_drawing.draw_points( image, label_bb8, (0,255,0), radius=3, thickness=2 )

			# Draw predicted bounding box
			pred_bb, j     = cv2.projectPoints( bb, pred_r, pred_t, label_k, numpy.zeros([5]) )
			pred_bb        = numpy.reshape( pred_bb, [8, 2] )
			pred_bb[:, 0] *= x_ratio
			pred_bb[:, 1] *= y_ratio
			image          = pydataset.cv2_drawing.draw_bounding_box( image, pred_bb, pred_color, 2, None )

			# Draw predicted control points
			pred_bb8[:, 0] *= x_ratio
			pred_bb8[:, 1] *= y_ratio
			image = pydataset.cv2_drawing.draw_points( image, pred_bb8, (0,0,255), radius=3, thickness=2 )

			# Save drawing
			drawings[ i ] = image

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, inputs, labels, predictions, arguments )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.
	
		Arguments:
			cls (`type`): The experiment class.
		
		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""
		parser = super( XP, cls ).get_argument_parser()
		parser.add_argument(
			'--backbone',
			   type = str,
			default = '',
			   help = 'Name of the backbone network.'
			)
		parser.add_argument(
			'--category',
			   type = str,
			default = 'all',
			   help = 'Name of the object category to consider.'
			)
		parser.add_argument(
			'--control_points_feature_name',
			   type = str,
			default = 'bb8',
			   help = 'Name of the feature containing the control points'
			)
		parser.add_argument(
			'--normalize',
			action = 'store_true',
			  help = 'If set, control point are computed in normalized image coordinates.'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.BB8NetV2(
			          include_lrn = True,
			          include_top = True,
			         input_tensor = None,
			          input_dtype = 'float32',
			          input_shape = (224, 224, 3),
			           input_name = arguments.image_feature_name,
			classifier_activation = 'linear',
			         output_shape = (8, 2),
			          output_name = arguments.control_points_feature_name
			)

		
		# 5 pixels threshold
		if arguments.normalize:
			threshold = (5.0 / 224.0) * 2.0
		else:
			threshold = 5.0

		# Compile model
		model.compile(
			        loss = pykeras.losses.ControlPointsDistances( distance_scaling_factor=10.0 ),
			   optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			     metrics = pykeras.metrics.ControlPointsAccuracy( threshold=threshold, name='add' )
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
	
		name = 'pose/bb8_v2_linemod/sgd.{}'.format( arguments.category )

		if arguments.normalize:
			name += '.normalized'

		return name

	# def get_name ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_pose ( cls, predictions_batch, labels_batch, arguments ):
		"""
		Returns the pose of the examples whoes features are found in the given batch.

		This method is used both on labels and predictions to get the pose during prediction.
		
		Arguments:
			cls                                           (`type`): Experiment class type.
			predictions_batch (`dict` of `str` to `numpy.ndarray`): Batch of features.
			labels_batch      (`dict` of `str` to `numpy.ndarray`): Batch of label features.

		Returns:
			euler_batch       (`numpy.ndarray`): Euler angles of each example in the batch.
			translation_batch (`numpy.ndarray`): Translation vector of each example in the batch.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions_batch, dict )
		assert pytools.assertions.dictionary_types_are( predictions_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels_batch, dict )
		assert pytools.assertions.dictionary_types_are( labels_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		# Get features
		points_batch = predictions_batch[ arguments.control_points_feature_name ]
		k_batch      =      labels_batch[ arguments.camera_matrix_feature_name  ] 
		bb_batch     =      labels_batch[ arguments.bounding_box_feature_name   ] 

		# Get batch size
		batch_size  = k_batch.shape[ 0 ]

		# Get camera dist coefs if provided
		if arguments.camera_dist_coeffs_feature_name:
			coeffs_batch = labels[ arguments.camera_dist_coeffs_feature_name ]
		else:
			coeffs_batch = numpy.zeros( [batch_size, 5] )

		# Initialize resut buffers
		e_batch = numpy.empty( [batch_size, 3], dtype=numpy.float32 )
		t_batch = numpy.empty( [batch_size, 3], dtype=numpy.float32 )

		for i in range(batch_size):

			points = points_batch[ i ].copy()
			bb     =     bb_batch[ i ].copy()
			k      =      k_batch[ i ].copy()
			coeffs = coeffs_batch[ i ].copy()

			if arguments.normalize:
				points  = pydataset.pose_utils.rescale_control_points( points, 224, 224 )

			retval, e, t = cv2.solvePnP( bb, points, k, coeffs )

			e_batch[ i ] = e.ravel()
			t_batch[ i ] = t.ravel()

		return e_batch, t_batch

	# def get_pose ( cls, features_batch, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		labels_features_names = [
				arguments.control_points_feature_name,
				arguments.translation_feature_name,
				arguments.rotation_feature_name,
				arguments.camera_matrix_feature_name,
				arguments.bounding_box_feature_name
				]
		if arguments.camera_dist_coeffs_feature_name:
			labels_features_names.append( arguments.camera_dist_coeffs_feature_name )

		testing = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename ='testing-{}.tfrecords'.format( arguments.category ),
			   blueprint_filename ='testing-{}.json'.format( arguments.category ),
			                 name = 'linemod-testing-{}'.format( arguments.category ),
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = labels_features_names,
			              shuffle = False,
			  shuffle_buffer_size = -1,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = False,
			        prefetch_size = tensorflow.data.AUTOTUNE
			)

		if arguments.normalize:
			testing.map( pykeras.inputs.OnlinePointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.control_points_feature_name
				)

		return testing

	# def get_testing_dataset ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename = 'training-{}.tfrecords'.format( arguments.category ),
			   blueprint_filename = 'training-{}.json'.format( arguments.category ),
			                 name = 'linemod-training-{}'.format( arguments.category ),
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = [ arguments.control_points_feature_name ],
			              shuffle = arguments.shuffle,
			  shuffle_buffer_size = arguments.shuffle_buffer_size,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = arguments.drop_batch_reminder,
			        prefetch_size = arguments.prefetch_size
			)

		if arguments.normalize:
			training.map( pykeras.inputs.OnlinePointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.control_points_feature_name
				)

		return training

	# def get_training_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_validation_dataset ( cls, arguments ):
		"""
		Returns the dataset used to validate the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The validation dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		validation = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename = 'testing-{}.tfrecords'.format( arguments.category ),
			   blueprint_filename = 'testing-{}.json'.format( arguments.category ),
			                 name = 'linemod-validation-{}'.format( arguments.category ),
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = [ arguments.control_points_feature_name ],
			              shuffle = arguments.shuffle,
			  shuffle_buffer_size = arguments.shuffle_buffer_size,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = arguments.drop_batch_reminder,
			        prefetch_size = arguments.prefetch_size
			)
		
		if arguments.normalize:
			validation.map( pykeras.inputs.OnlinePointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.control_points_feature_name
				)

		return validation

	# def get_validation_dataset ( cls, arguments )

# class XP ( pykeras.experiment.PoseExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
