# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.PoseExperiment ):
	"""
	This experiment applies BB8Net to estimate the pose of LINEMOD objects though control points.
	"""

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def compute_metrics ( cls, all_labels, all_predictions, arguments ):

		# Constants
		COEFFS = numpy.zeros( [5], dtype=numpy.float32 )

		# Fetch all the labels
		labels_k        =      all_labels[ arguments.camera_k_feature_name   ] # [COUNT, 3, 3]
		labels_bb8      =      all_labels[ arguments.bb8_feature_name        ] # [COUNT, C, 8, 2]
		labels_bb8_ref  =      all_labels[ arguments.bb8_feature_name+'_ref' ] # [COUNT, C, 3*3+3+8*3]
		predictions_bb8 = all_predictions[ arguments.bb8_feature_name        ] # [COUNT, C, 8, 2]
		image_count     = labels_k.shape[0]

		# Count non NaN bb8 labels
		non_nan = numpy.sum( labels_bb8, axis=(-2,-1) ) # [COUNT, C]
		non_nan = numpy.logical_not( numpy.isnan(non_nan) ) # [COUNT, C]
		instance_count = numpy.sum( non_nan.astype(numpy.int32), axis=None ) # [1]
		
		# threashold
		taus = numpy.arange( 0., 52.5, 2.5 )
		nb   = taus.size
		
		# Per object instance metrics results
		five_degree                     = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		five_centimeter                 = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		five_degree_and_five_centimeter = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		add_3D                          = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		add_2D                          = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		add_2D_BB8                      = numpy.empty( [instance_count, nb], dtype=numpy.bool )
		index  = 0
		
		# Go through images
		for i in range( image_count ):

			# Fetch labels and predictions for that image
			label_k        =        labels_k[ i ] # [3, 3]
			label_bb8      =      labels_bb8[ i ] # [C, 8, 2]
			label_bb8_ref  =  labels_bb8_ref[ i ] # [C, 3*3+3+8*3]
			prediction_bb8 = predictions_bb8[ i ] # [C, 8, 2]

			# If BB8 labels and predictions are normalized
			# Convert them back to pixel units
			# note: BB8 works on images resized to 224x224
			if arguments.normalize:
				label_bb8      = pydataset.pose_utils.rescale_image_points( label_bb8,      224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
				prediction_bb8 = pydataset.pose_utils.rescale_image_points( prediction_bb8, 224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]

			# Go through possible objets
			for j in range( 15 ):
				
				# Fetch labels and predictions for that object
				lab_bb8     =      label_bb8[ j ]  # [8, 2]
				lab_bb8_ref =  label_bb8_ref[ j ]  # [3*3+3+8*3]
				pred_bb8    = prediction_bb8[ j ]  # [8, 2]

				# Unpack labels in references
				lab_R      = numpy.reshape( lab_bb8_ref[0: 9], [3,3]  )
				lab_t      = numpy.reshape( lab_bb8_ref[9:12], [3]    )
				lab_points = numpy.reshape( lab_bb8_ref[12:],  [-1,3] )

				# is the object there ?
				if not numpy.isnan( numpy.sum(lab_bb8) ):
					
					# Convert label rotation to Euler angles
					lab_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( lab_R ).ravel()

					# Compute PnP to get the predicted pose
					retval, pred_r, pred_t = cv2.solvePnP( lab_points, pred_bb8, label_k, COEFFS )
					pred_r = pred_r.astype( numpy.float32 ).ravel()
					pred_t = pred_t.astype( numpy.float32 ).ravel()

					# 5 degree
					distance_r = numpy.linalg.norm(
						pydataset.units.radian_to_degree(lab_r) - pydataset.units.radian_to_degree(pred_r)
						)

					# 5 centimeter
					distance_t = numpy.linalg.norm( lab_t* 100. - pred_t* 100. )

					# ADD 3D bb
					label_bb_3D       = pydataset.pose_utils.place_points( lab_points, lab_r,  lab_t  )
					prediction_bb_3D  = pydataset.pose_utils.place_points( lab_points, pred_r, pred_t )
					distance_bb_3D    = numpy.linalg.norm( label_bb_3D - prediction_bb_3D, axis=-1 )
					distance_bb_3D    = distance_bb_3D * 100. # meter to centimeter
					distance_bb_3D    = numpy.mean( distance_bb_3D, axis=-1 )

					# ADD 2D bb
					label_bb_2D      = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, lab_r,  lab_t  )
					prediction_bb_2D = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, pred_r, pred_t )
					distance_bb_2D   = numpy.linalg.norm( label_bb_2D - prediction_bb_2D, axis=-1 )
					distance_bb_2D   = numpy.mean( distance_bb_2D, axis=-1 )
					# print( distance_bb_2D )

					# ADD 2D BB8
					distance_bb8_2D = numpy.linalg.norm( lab_bb8 - pred_bb8, axis=-1 ) # [8]
					distance_bb8_2D = numpy.mean( distance_bb8_2D, axis=-1 ) # [1]

					# for each threshold
					for k in range( nb ):

						# fetch threshold
						tau = taus[ k ]

						# Compare
						five_degree[ index, k ]                     = distance_r <= tau
						five_centimeter[ index, k ]                 = distance_t <= tau
						five_degree_and_five_centimeter[ index, k ] = five_degree[ index, k ] and five_centimeter[ index, k ]
						add_3D[ index, k ]                          = distance_bb_3D <= tau
						add_2D[ index, k ]                          = distance_bb_2D <= tau
						add_2D_BB8[ index, k ]                      = distance_bb8_2D <= tau

					# for k in range( nb ):
				
					# next object instance
					index += 1

				# if not numpy.isnan( numpy.sum(lab_bb8) )

			# for j in range( 15 )

		# for i in range(count)

		assert index == instance_count

		
		# Cast metrics per images results to floats
		five_degree                     = five_degree.astype( numpy.float32 )
		five_centimeter                 = five_centimeter.astype( numpy.float32 )
		five_degree_and_five_centimeter = five_degree_and_five_centimeter.astype( numpy.float32 )
		add_3D                          = add_3D.astype( numpy.float32 )
		add_2D                          = add_2D.astype( numpy.float32 )
		add_2D_BB8                      = add_2D_BB8.astype( numpy.float32 )

		# Compute mean average of each metric
		
		five_degree                     = numpy.asarray([ numpy.sum(                     five_degree[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )
		five_centimeter                 = numpy.asarray([ numpy.sum(                 five_centimeter[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )
		five_degree_and_five_centimeter = numpy.asarray([ numpy.sum( five_degree_and_five_centimeter[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )
		add_3D                          = numpy.asarray([ numpy.sum(                          add_3D[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )
		add_2D                          = numpy.asarray([ numpy.sum(                          add_2D[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )
		add_2D_BB8                      = numpy.asarray([ numpy.sum(                      add_2D_BB8[ :, j ] ) / float(instance_count) for j in range(nb) ], dtype=numpy.float32 )

		return {
			'tau': taus,
			'degree': five_degree,
			'cm': five_centimeter,
			'degree_cm': five_degree_and_five_centimeter,
			'add_3d': add_3D,
			'add_2d': add_2D,
			'add_2d_bb8': add_2D_BB8
			}

	# --------------------------------------------------

	@classmethod
	def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments ):
		"""
		Draw predictions made by this experiment over the given inputs.

		Arguments:
			cls                                     (`type`): Experiment class type.
			inputs      (`dict` of `str` to `numpy.ndarray`): The inputs of the model.
			predictions (`dict` of `str` to `numpy.ndarray`): The predictions made on the inputs by the model.
			labels      (`dict` of `str` to `numpy.ndarray`): The labels associated to the inputs.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Batch of drawings (images).
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( inputs, dict )
		assert pytools.assertions.dictionary_types_are( inputs, str, numpy.ndarray )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		# Constants
		COEFFS = numpy.zeros( [5], dtype=numpy.float32 )

		# Get a batch of features
		batch_input_images    =      inputs[ arguments.image_feature_name      ] # [BATCH, 224, 224, 3]
		batch_k               =      labels[ arguments.camera_k_feature_name   ] # [BATCH, 3, 3]
		batch_labels_bb8      =      labels[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]
		batch_labels_bb8_ref  =      labels[ arguments.bb8_feature_name+'_ref' ] # [BATCH, C, 8, 2]
		batch_predictions_bb8 = predictions[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]

		# Get batch size
		batch_size = batch_input_images.shape[ 0 ] # [1]	

		# Create drawings buffer
		shape        = list(batch_input_images.shape)
		new_shape    = list(shape)
		new_shape[1] = 512
		new_shape[2] = 512
		drawings     = numpy.empty( new_shape, batch_input_images.dtype )

		# Ratio used to scale the bounsung boxes
		x_ratio  = float(new_shape[ 1 ]) / float(shape[ 1 ])
		y_ratio  = float(new_shape[ 2 ]) / float(shape[ 2 ])
		
		# Colors
		label_color = (83, 222, 73)
		good_color  = (255, 111, 28)
		bad_color   = (28, 111, 255)

		results

		# Draw each images
		for i in range( batch_size ):
			
			pred_color = good_color if results[i] else bad_color

			# Get the predictions and labels for one image
			input_image    = batch_input_images[ i ] # [224, 224, 3]
			prediction_bb8 = batch_predictions_bb8[ i ] # [C, 8, 2]
			label_k        = batch_k[ i ]
			label_bb8      = batch_labels_bb8[ i ]      # [C, 8, 2]
			label_bb8_ref  = batch_labels_bb8_ref[ i ]

			# Resize image
			drawing = cv2.resize( input_image.copy(), (512, 512) )

			# If BB8 labels and predictions are normalized
			# Convert them back to pixel units
			# note: BB8 works on images resized to 224x224
			if arguments.normalize:
				prediction_bb8 = pydataset.pose_utils.rescale_image_points( prediction_bb8, 224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
				label_bb8      = pydataset.pose_utils.rescale_image_points( label_bb8,      224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
			
			# Draw each bounding box prediction
			for j in range( 15 ):

				# fetch one bounding box
				lab_bb8      = label_bb8[ j ]
				lab_bb8_ref  = label_bb8_ref[ j ]
				pred_bb8     = prediction_bb8[ j ]


				# Unpack labels in references
				lab_R      = numpy.reshape( lab_bb8_ref[0: 9], [3,3]  )
				lab_t      = numpy.reshape( lab_bb8_ref[9:12], [3]    )
				lab_points = numpy.reshape( lab_bb8_ref[12:],  [-1,3] )

				# If the bb8 label is not populated
				# I.E. no obect of category "j" was present,
				# ignore this label and prediction
				if numpy.isnan( numpy.sum(lab_bb8) ):
					continue

				# Convert label rotation to Euler angles
				lab_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( lab_R )

				# Compute PnP to get the predicted pose
				retval, pred_r, pred_t = cv2.solvePnP( lab_points, pred_bb8, label_k, COEFFS )

				# Project bounding box
				lab_bb8_2D  = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, lab_r,  lab_t  )
				pred_bb8_2D = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, pred_r, pred_t )

				# Rescale and draw label
				lab_bb8_2D[:, 0] *= x_ratio
				lab_bb8_2D[:, 1] *= y_ratio
				drawing = pydataset.cv2_drawing.draw_bounding_box( drawing, lab_bb8_2D, label_color, 2, label_color, 3, 3 )

				# Rescale and draw prediction
				pred_bb8_2D[:, 0] *= x_ratio
				pred_bb8_2D[:, 1] *= y_ratio
				pred_bb8[:, 0] *= x_ratio
				pred_bb8[:, 1] *= y_ratio
				drawing = pydataset.cv2_drawing.draw_bounding_box( drawing, pred_bb8_2D, pred_color, 2, pred_color, 3, 3 )
				drawing = pydataset.cv2_drawing.draw_points( drawing, pred_bb8, pred_color, radius=3, thickness=2 )

			# for j in range( 15 )

			drawings[ i ] = drawing

		# for i in range( batch_size )

		return drawings

	# def draw_predictions ( cls, inputs, predictions, labels, scores, results, batch_size, arguments )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.
	
		Arguments:
			cls (`type`): The experiment class.
		
		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""
		parser = argparse.ArgumentParser()
		parser.add_argument(
			'--dataset_path',
			   type = str,
			   help = 'Path of the directory containing the dataset.',
			default = 'D:/Datasets/bop_ycb_video'
			)
		parser.add_argument(
			'--batch_size',
			   type = int,
			default = 64,
			   help = 'Datasets batch size.'
			)
		parser.add_argument(
			'--drop_batch_reminder',
			action = 'store_true',
			  help = 'Should the batch reminder be dropped?'
			)
		parser.add_argument(
			'--prefetch_size',
			   type = int,
			default = tensorflow.data.AUTOTUNE,
			   help = 'Training and validation dataset prefetch buffer size.'
			   )
		parser.add_argument(
			'--shuffle',
			   type = bool,
			default = True,
			   help = 'Should the training dataset be shuffled?'
			   )
		parser.add_argument(
			'--shuffle_buffer_size',
			   type = int,
			default = 640,
			   help = 'Training dataset shuffle buffer size.'
			)
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image'
			)
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'visible_mask',
			   help = 'Name of the feature containing the foreground mask'
			)
		parser.add_argument(
			'--camera_k_feature_name',
			   type = str,
			default = 'K',
			   help = 'Name of the feature containing the camera intrinsic parameters'
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the camera intrinsic parameters'
			)
		parser.add_argument(
			'--bb8_feature_name',
			   type = str,
			default = 'bb8_v1',
			   help = 'Name of the feature containing the control points'
			)
		parser.add_argument(
			'--normalize',
			action = 'store_true',
			  help = 'If set, control point are computed in normalized image coordinates.'
			)
		parser.add_argument(
			'--normalize_min',
			   type = float,
			default = 0.,
			   help = 'Normalized control points min value'
			)
		parser.add_argument(
			'--normalize_max',
			   type = float,
			default = 1.,
			   help = 'Normalized control points max value'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.BB8NetV1(
			          include_lrn = True,
			          include_top = True,
			         input_tensor = None,
			          input_dtype = 'float32',
			          input_shape = (224, 224, 3),
			           input_name = arguments.image_feature_name,
			classifier_activation = 'linear',
			         output_shape = (15, 8, 2),
			          output_name = arguments.bb8_feature_name
			)

		# 5 pixels threshold
		if arguments.normalize:
			normalize_length = arguments.normalize_max - arguments.normalize_min
			threshold = (5.0 / 224.0) * normalize_length
		else:
			threshold = 5.0

		# Compile model
		model.compile(
			        loss = pykeras.losses.ControlPointsDistancesV1( distance_scaling_factor=1.0 ),
			   optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			     metrics = [
			     	pykeras.metrics.ControlPointsAccuracyV1( threshold=threshold*1, name='add05' ),
			     	pykeras.metrics.ControlPointsAccuracyV1( threshold=threshold*2, name='add10' ),
			     	pykeras.metrics.ControlPointsAccuracyV1( threshold=threshold*3, name='add15' ),
			     	pykeras.metrics.ControlPointsAccuracyV1( threshold=threshold*4, name='add20' )
			     	]
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		name = 'pose/bb8_v1_linemod/sgd'
		
		if arguments.normalize:
			name += '_normalized_{}_{}'.format( arguments.normalize_min, arguments.normalize_max  )

		return name

	# def get_name ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    # tf_record_filename = 'training_instancewise.tfrecords',
			    # blueprint_filename = 'training_instancewise.json',
			    tf_record_filename = 'validation_instancewise.tfrecords',
			    blueprint_filename = 'validation_instancewise.json',
			                  name = 'testing',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.camera_k_feature_name,
			                         arguments.bounding_rectangle_feature_name,
			                         arguments.bb8_feature_name,
			                         arguments.bb8_feature_name+'_ref',
			                         ],
			               shuffle = False,
			   shuffle_buffer_size = -1,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = False,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'bilinear',
			affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		if arguments.normalize:
			testing.add_mapper( pykeras.inputs_v2.mappers.PointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.bb8_feature_name,
				            new_min = arguments.normalize_min,
				            new_max = arguments.normalize_max,
				)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = {
				arguments.bb8_feature_name: arguments.bb8_feature_name,
				arguments.bb8_feature_name+'_ref': arguments.bb8_feature_name+'_ref',
				arguments.camera_k_feature_name: arguments.camera_k_feature_name,
				arguments.bounding_rectangle_feature_name: arguments.bounding_rectangle_feature_name
				}
			)
		return testing

	# def get_testing_dataset ( ... )
	
	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training_instancewise.tfrecords',
			    blueprint_filename = 'training_instancewise.json',
			                  name = 'training',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.bounding_rectangle_feature_name,
			                         arguments.bb8_feature_name,
			                         arguments.camera_k_feature_name
			                         ],
			               shuffle = arguments.shuffle,
			   shuffle_buffer_size = arguments.shuffle_buffer_size,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'bilinear',
			affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomBrightness,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomContrast,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomHue,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.05
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomSaturation,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.0
			)
		if arguments.normalize:
			training.add_mapper( pykeras.inputs_v2.mappers.PointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.bb8_feature_name,
				            new_min = arguments.normalize_min,
				            new_max = arguments.normalize_max,
				)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = { arguments.bb8_feature_name: arguments.bb8_feature_name }
			)
		return training

	# def get_training_dataset ( ... )
	
	# --------------------------------------------------

	@classmethod
	def get_validation_dataset ( cls, arguments ):
		"""
		Returns the dataset used to validate the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The validation dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		validation = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'validation_instancewise.tfrecords',
			    blueprint_filename = 'validation_instancewise.json',
			                  name = 'validation',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.camera_k_feature_name,
			                         arguments.bounding_rectangle_feature_name,
			                         arguments.bb8_feature_name,
			                         arguments.bb8_feature_name+'_ref',
			                         ],
			               shuffle = False,
			   shuffle_buffer_size = -1,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = False,
			         prefetch_size = arguments.prefetch_size
			)
		validation.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		validation.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'bilinear',
			affected_features_names = [ arguments.bounding_rectangle_feature_name, arguments.bb8_feature_name, arguments.camera_k_feature_name ]
			)
		validation.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		if arguments.normalize:
			validation.add_mapper( pykeras.inputs_v2.mappers.PointsNormalizer,
				 image_feature_name = arguments.image_feature_name,
				points_feature_name = arguments.bb8_feature_name,
				            new_min = arguments.normalize_min,
				            new_max = arguments.normalize_max,
				)
		validation.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = {
				arguments.bb8_feature_name: arguments.bb8_feature_name#,
				# arguments.camera_k_feature_name: arguments.camera_k_feature_name,
				# arguments.bounding_rectangle_feature_name: arguments.bounding_rectangle_feature_name
				}
			)
		return validation

	# def get_validation_dataset ( ... )

	# --------------------------------------------------

	@classmethod
	def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):
		"""
		Identify correct predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			scores                         (`numpy.ndarray`): Scores of the predictions.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Booleans indicating correct precitions with shape [batch_size].
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( scores, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		# Constants
		COEFFS = numpy.zeros( [5], dtype=numpy.float32 )

		# Get a batch of features
		batch_k               =      labels[ arguments.camera_k_feature_name   ] # [BATCH, 3, 3]
		batch_labels_bb8      =      labels[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]
		batch_labels_bb8_ref  =      labels[ arguments.bb8_feature_name+'_ref' ] # [BATCH, C, 8, 2]
		batch_predictions_bb8 = predictions[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]

		# Get batch size
		batch_size = batch_k.shape[ 0 ] # [1]	

		# Create resut buffer
		results = numpy.empty( [batch_size, 1], numpy.bool )

		# Draw each images
		for i in range( batch_size ):
			
			# Get the predictions and labels for one image
			prediction_bb8 = batch_predictions_bb8[ i ] # [C, 8, 2]
			label_k        = batch_k[ i ]
			label_bb8      = batch_labels_bb8[ i ]      # [C, 8, 2]
			label_bb8_ref  = batch_labels_bb8_ref[ i ]

			# If BB8 labels and predictions are normalized
			# Convert them back to pixel units
			# note: BB8 works on images resized to 224x224
			if arguments.normalize:
				prediction_bb8 = pydataset.pose_utils.rescale_image_points( prediction_bb8, 224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
				label_bb8      = pydataset.pose_utils.rescale_image_points( label_bb8,      224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
			
			# Draw each bounding box prediction
			adds = numpy.zeros( [15], dtype=numpy.float32 )
			for j in range( 15 ):

				# fetch one bounding box
				lab_bb8      = label_bb8[ j ]
				lab_bb8_ref  = label_bb8_ref[ j ]
				pred_bb8     = prediction_bb8[ j ]

				# Unpack labels in references
				lab_R      = numpy.reshape( lab_bb8_ref[0: 9], [3,3]  )
				lab_t      = numpy.reshape( lab_bb8_ref[9:12], [3]    )
				lab_points = numpy.reshape( lab_bb8_ref[12:],  [-1,3] )

				# If the bb8 label is not populated
				# I.E. no obect of category "j" was present,
				# ignore this label and prediction
				if numpy.isnan( numpy.sum(lab_bb8) ):
					continue

				# Convert label rotation to Euler angles
				lab_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( lab_R )

				# Compute PnP to get the predicted pose
				retval, pred_r, pred_t = cv2.solvePnP( lab_points, pred_bb8, label_k, COEFFS )

				# Project bounding box
				lab_bb8_2D  = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, lab_r,  lab_t  )
				pred_bb8_2D = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, pred_r, pred_t )

				# Compute add 2D
				distances = numpy.linalg.norm( lab_bb8_2D - pred_bb8_2D, axis=-1 )
				distance  = numpy.mean( distances, axis=-1 )
				adds[ j ] = distance

			# for j in range( 21 )

			results[ i ] = numpy.max( adds ) < 5. # zeros for all but the object seen in the picture

		# for i in range( batch_size )

		return results

	# def idenfify_correct_predictions ( cls, predictions, labels, scores, batch_size, arguments ):

	# --------------------------------------------------

	@classmethod
	def score_predictions ( cls, predictions, labels, batch_size, arguments ):
		"""
		Compute the scores of predictions.

		Arguments:
			cls                                     (`type`): Experiment class type.
			predictions (`dict` of `str` to `numpy.ndarray`): Model predictions.
			labels      (`dict` of `str` to `numpy.ndarray`): Ground truth labels.
			batch_size                               (`int`): Current batch size.
			arguments                 (`argparse.Namespace`): Arguments of the experiment.
		
		Returns:
			`numpy.ndarray`: Scores of the given precitions.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( batch_size, int )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

				# Constants
		COEFFS = numpy.zeros( [5], dtype=numpy.float32 )

		# Get a batch of features
		batch_k               =      labels[ arguments.camera_k_feature_name   ] # [BATCH, 3, 3]
		batch_labels_bb8      =      labels[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]
		batch_labels_bb8_ref  =      labels[ arguments.bb8_feature_name+'_ref' ] # [BATCH, C, 8, 2]
		batch_predictions_bb8 = predictions[ arguments.bb8_feature_name        ] # [BATCH, C, 8, 2]

		# Get batch size
		batch_size = batch_k.shape[ 0 ] # [1]	

		# Create resut buffer
		scores = numpy.empty( [batch_size, 1], numpy.float32 )

		# Draw each images
		for i in range( batch_size ):
			
			# Get the predictions and labels for one image
			prediction_bb8 = batch_predictions_bb8[ i ] # [C, 8, 2]
			label_k        = batch_k[ i ]
			label_bb8      = batch_labels_bb8[ i ]      # [C, 8, 2]
			label_bb8_ref  = batch_labels_bb8_ref[ i ]

			# If BB8 labels and predictions are normalized
			# Convert them back to pixel units
			# note: BB8 works on images resized to 224x224
			if arguments.normalize:
				prediction_bb8 = pydataset.pose_utils.rescale_image_points( prediction_bb8, 224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
				label_bb8      = pydataset.pose_utils.rescale_image_points( label_bb8,      224.0, 224.0, arguments.normalize_min, arguments.normalize_max ) # [C, 8, 2]
			
			# Draw each bounding box prediction
			adds = numpy.zeros( [15], dtype=numpy.float32 )
			for j in range( 15 ):

				# fetch one bounding box
				lab_bb8      = label_bb8[ j ]
				lab_bb8_ref  = label_bb8_ref[ j ]
				pred_bb8     = prediction_bb8[ j ]

				# Unpack labels in references
				lab_R      = numpy.reshape( lab_bb8_ref[0: 9], [3,3]  )
				lab_t      = numpy.reshape( lab_bb8_ref[9:12], [3]    )
				lab_points = numpy.reshape( lab_bb8_ref[12:],  [-1,3] )

				# If the bb8 label is not populated
				# I.E. no obect of category "j" was present,
				# ignore this label and prediction
				if numpy.isnan( numpy.sum(lab_bb8) ):
					continue

				# Convert label rotation to Euler angles
				lab_r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( lab_R )

				# Compute PnP to get the predicted pose
				retval, pred_r, pred_t = cv2.solvePnP( lab_points, pred_bb8, label_k, COEFFS )

				# Project bounding box
				lab_bb8_2D  = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, lab_r,  lab_t  )
				pred_bb8_2D = pydataset.pose_utils.project_points( lab_points, label_k, COEFFS, pred_r, pred_t )

				# Compute add 2D
				distances = numpy.linalg.norm( lab_bb8_2D - pred_bb8_2D, axis=-1 )
				distance  = numpy.mean( distances, axis=-1 )
				adds[ j ] = distance

			# for j in range( 21 )

			scores[ i ] = numpy.max( adds ) # zeros for all but the object seen in the picture

		# for i in range( batch_size )

		return scores

	# def score_predictions ( cls, ... )

# class XP ( pykeras.experiment.PoseExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
