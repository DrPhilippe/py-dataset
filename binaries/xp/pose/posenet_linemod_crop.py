# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import keras
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.PoseExperiment ):
	"""
	This experiment applies PoseNet to estimate the pose of LINEMOD objects.
	"""

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.
	
		Arguments:
			cls (`type`): The experiment class.
		
		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""
		parser = super( XP, cls ).get_argument_parser()
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle'
			)
		parser.add_argument(
			'--visible_mask_feature_name',
			   type = str,
			default = 'visible_mask',
			   help = 'Name of the feature containing the visible mask'
			)
		parser.add_argument(
			'--rotation_type',
			   type = str,
			default = 'q',
			choices = ['e', 'q', 'R'],
			   help = 'Type of rotation'
			)
		parser.add_argument(
			'--lambda_q',
			   type = float,
			default = 0.5,
			   help = 'Weight of the rotation terms in the loss'
			   )
		parser.add_argument(
			'--lambda_t',
			   type = float,
			default = 0.075,
			help = 'Weight of the translation terms in the loss'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.posenet.PoseNet(
				  include_lrn = True,
				  include_top = True,
				 input_tensor = None,
				  input_shape = (227, 227, 3),
				  input_dtype = 'float32',
				   input_name = arguments.image_feature_name,
				rotation_type = arguments.rotation_type,
				outputs_names = ['r', 't', 'r1', 't1', 'r2', 't2']
			)

		# Compile model
		model.compile(
			        loss = pykeras.applications.posenet.get_losses( arguments.rotation_type, outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] ),
			loss_weights = pykeras.applications.posenet.get_losses_weights( arguments.lambda_q, arguments.lambda_t, outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] ),
			   optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			     metrics = pykeras.applications.posenet.get_metrics( arguments.rotation_type, outputs_names=['r', 't', 'r1', 't1', 'r2', 't2'] )
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'pose/posenet_linemod/{0:}t_{1:5.3f}_{2:5.3f}_crop'.format( arguments.rotation_type, arguments.lambda_q, arguments.lambda_t )

	# def get_name ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_pose ( cls, predictions_batch, labels_batch, arguments ):
		"""
		Returns the pose of the examples whoes features are found in the given batch.

		This method is used both on labels and predictions to get the pose during prediction.
		
		Arguments:
			cls                                        (`type`): Experiment class type.
			predictions_batch (`dict` of `str` to `numpy.ndarray`): Batch of features.
			labels_batch      (`dict` of `str` to `numpy.ndarray`): Batch of label features.

		Returns:
			euler_batch       (`numpy.ndarray`): Euler angles of each example in the batch.
			translation_batch (`numpy.ndarray`): Translation vector of each example in the batch.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( predictions_batch, dict )
		assert pytools.assertions.dictionary_types_are( predictions_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( labels_batch, dict )
		assert pytools.assertions.dictionary_types_are( labels_batch, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		# Get rotation and translations
		r_batch = predictions_batch[ 'r' ] 
		t_batch = predictions_batch[ 't' ]
		
		# normalize quaternions:
		if arguments.rotation_type == 'q':
			r_norms = numpy.linalg.norm(r_batch, axis=-1, keepdims=True)
			r_norms = numpy.tile( r_norms, [1,4] )
			r_batch = numpy.divide( r_batch, r_norms )

		# Convert rotation if needed
		batch_size  = r_batch.shape[ 0 ]
		euler_batch = numpy.empty( [batch_size, 3], dtype=numpy.float32 )
		for i in range(batch_size):
			euler_batch[ i ] = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( r_batch[ i ] )

		return euler_batch, t_batch

	# def get_pose ( cls, predictions_batch, labels_batch, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    # tf_record_filename = 'training_instancewise.tfrecords',
			    # blueprint_filename = 'training_instancewise.json',
			    tf_record_filename = 'validation_instancewise.tfrecords',
			    blueprint_filename = 'validation_instancewise.json',
			                  name = 'testing',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.camera_matrix_feature_name,
			    arguments.visible_mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.rotation_feature_name,
			    arguments.translation_feature_name,
			    arguments.bounding_box_feature_name#,
			    # 'mesh'
			    ],
			                repeat = False,
			               shuffle = False,
			   shuffle_buffer_size = 0,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.visible_mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.camera_matrix_feature_name, arguments.bounding_rectangle_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.visible_mask_feature_name
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = {
				arguments.camera_matrix_feature_name: arguments.camera_matrix_feature_name,
			    arguments.bounding_box_feature_name: arguments.bounding_box_feature_name,
				arguments.rotation_feature_name: arguments.rotation_feature_name,
				arguments.translation_feature_name: arguments.translation_feature_name#,
				# 'mesh':  'mesh'
				}
			)
		return testing

	# def get_testing_dataset ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training_instancewise.tfrecords',
			    blueprint_filename = 'training_instancewise.json',
			                  name = 'training',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.visible_mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.rotation_feature_name,
			    arguments.translation_feature_name
			    ],
			                repeat = False,
			               shuffle = False,
			   shuffle_buffer_size = 0,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.visible_mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.visible_mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomBrightness,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomContrast,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomHue,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomSaturation,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.8,
			                upper = 1.0
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = {
				'r': arguments.rotation_feature_name,
				't': arguments.translation_feature_name,
				'r1': arguments.rotation_feature_name,
				't1': arguments.translation_feature_name,
				'r2': arguments.rotation_feature_name,
				't2': arguments.translation_feature_name
				}
			)
		return training

	# def get_training_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_validation_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'validation_instancewise.tfrecords',
			    blueprint_filename = 'validation_instancewise.json',
			                  name = 'validation',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.visible_mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.rotation_feature_name,
			    arguments.translation_feature_name,
			    ],
			                repeat = False,
			               shuffle = False,
			   shuffle_buffer_size = 0,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.visible_mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (227, 227),
			                       pad_size = (0, 0),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.visible_mask_feature_name
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = {
				'r': arguments.rotation_feature_name,
				't': arguments.translation_feature_name,
				'r1': arguments.rotation_feature_name,
				't1': arguments.translation_feature_name,
				'r2': arguments.rotation_feature_name,
				't2': arguments.translation_feature_name
				}
			)
		return testing

	# def get_validation_dataset ( cls, arguments )

# class XP ( pykeras.experiment.PoseExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
