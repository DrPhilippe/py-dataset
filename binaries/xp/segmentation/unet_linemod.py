# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pylinemod
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.SegmentationExperiment ):
	"""
	This experiment applies UNet to segment LINEMOD images.
	"""

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		number_of_categories = len(pylinemod.constants.CATEGORIES) + int(arguments.dedicated_background)
		
		# Create model
		model = pykeras.applications.UNet(
			          include_top = True,
			         input_tensor = None,
			          input_shape = (240, 240, 3),
			          input_dtype = 'float32',
			           input_name = arguments.image_feature_name,
			              classes = 1,
			classifier_activation = 'sigmoid',
			          output_name = arguments.segmentation_feature_name
			)

		# Compile model
		model.compile(
			        loss = keras.losses.CategoricalCrossentropy( from_logits=False, name='loss' ),
			   optimizer = keras.optimizers.SGD( learning_rate=0.0001, momentum=0.5 ),
			     metrics = [
			         keras.metrics.MeanIoU( num_classes=number_of_categories, name='iou' ),
			         keras.metrics.Accuracy( name='accuracy' )
			         ]
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'segmentation/unet.linemod/sgd'

	# def get_name ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to testing the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The validation dataset.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'testing-all.tfrecords',
			    blueprint_filename = 'testing-all.json',
			                  name = 'linemod-testing-all',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.segmentation_feature_name,
			                         arguments.bounding_rectangle_feature_name
			                         ],
			               shuffle = False,
			   shuffle_buffer_size = -1,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = False,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name, arguments.segmentation_feature_name ],
			                 size = (240, 240),
			               method = 'bilinear',
			preserve_aspect_ratio = False,
			            antialias = False
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			         inputs = { arguments.image_feature_name: arguments.image_feature_name },
			         labels = { arguments.segmentation_feature_name: arguments.mask_feature_name }
			)
		return testing

	# def get_testing_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training-all.tfrecords',
			    blueprint_filename = 'training-all.json',
			                  name = 'linemod-training-all',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.segmentation_feature_name,
			                         arguments.bounding_rectangle_feature_name
			                         ],
			               shuffle = True,
			   shuffle_buffer_size = 512,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = False,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name, arguments.segmentation_feature_name ],
			                 size = (240, 240),
			               method = 'bilinear',
			preserve_aspect_ratio = False,
			            antialias = False
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			         inputs = { arguments.image_feature_name: arguments.image_feature_name },
			         labels = { arguments.segmentation_feature_name: arguments.mask_feature_name }
			)
		return training

	# def get_training_dataset ( cls, arguments )

# class XP ( pykeras.experiment.SegmentationExperiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
