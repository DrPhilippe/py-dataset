# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import cv2
import numpy
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pykeras
import pylinemod
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.LocalizationExperiment ):
	"""
	This experiment applies Yolo V1 to estimate the bounding rectangle and class of LINEMOD objects though control points.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def draw_rectangle ( cls, image, rect, color, thickness, arguments, label_category ):
		"""
		Draws a rectangle on the given image.

		Arguments:
			cls                     (`type`): Experiment class.
			image          (`numpy.ndarray`): Image on which to draw with shape [height, width, channels] and dtype uint8.
			rect           (`numpy.ndarray`): Rectangle formatted as [x1, y1, x2, y2, category index] and dtype float32.
			color        (`tuple` of `int`s): Color used to draw the rectangle, must be the same length as the image number of channels.
			thickness                (`int`): Thickness of the rectangle lines.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`numpy.ndarray`: The image with the drawn rectangle.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image, numpy.ndarray )
		assert pytools.assertions.equal( image.dtype, numpy.uint8 )
		assert pytools.assertions.type_is( rect, numpy.ndarray )
		assert pytools.assertions.equal( rect.dtype, numpy.float32 )
		assert pytools.assertions.type_is( color, tuple )
		assert pytools.assertions.tuple_items_type_is( color, int )
		assert pytools.assertions.type_is( thickness, int )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		# Get image dimensions
		height             = float( image.shape[ 0 ] )
		width              = float( image.shape[ 1 ] )
		number_of_channels = image.shape[ 2 ]
		
		# Check
		if ( len(color) != number_of_channels ):
			raise RuntimeError(
				"The color used to draw the rectangle must have the same amount of coordinates as image channels: {} != {}".format(
				len(color), number_of_channels
				))

		# Convert rectangle to pixels
		x1       = int(width  * rect[ 0 ])
		y1       = int(height * rect[ 1 ])
		x2       = int(width  * rect[ 2 ])
		y2       = int(height * rect[ 3 ])

		# Draw the rectangle
		image = cv2.rectangle( image, (x1, y1), (x2, y2), color, thickness )

		# get the category and replace it by the category name if possible
		category      = int( rect[ 4 ] )
		classes_names = cls.get_classes_names( arguments )
		if classes_names:
			category = classes_names[ category ]

		# get the condicence
		confidence = float( rect[ 5 ] )

		# Display the category
		if label_category:
			image = cv2.putText( image, '{} ({:04.2f})'.format( category, confidence ), (x1+8, y1+24), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1 )

		return image

	# def draw_rectangle ( cls, ... )

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( XP, cls ).get_argument_parser()
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'visible_mask',
			   help = 'Name of the feature containing the object foreground mask.'
			)
		parser.add_argument(
			'--number_of_detectors_per_cell',
			   type = int,
			default = 2,
			   help = 'Nunmber of predicted bounding rectangles in each cell.'
			)
		parser.add_argument(
			'--yolo_v1_feature_name',
			   type = str,
			default = 'yolo_v1',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--category',
			   type = str,
			default = 'all',
			   help = 'Name of the category for which to train.'
			)
		parser.add_argument(
			'--confidence_threshold',
			   type = float,
			default = 0.5,
			   help = 'Predictions with a confidence lower than this threshold are ignored.'
			)
		return parser

	# def get_argument_parser ( cls )
	
	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories classified in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return [
			'Coffee can',
			'Cracker box',
			'Sugar box',
			'Tomato soup can',
			'Mustard container',
			'Tuna fish can',
			'Chocolate pudding box',
			'Gelatin box',
			'Potted meat can',
			'Banana',
			'Pitcher',
			'Bleach cleanser',
			'Metal bowl',
			'Metal mug',
			'Power drill',
			'Wood block',
			'Scissors',
			'Marker',
			'Spring clamp (small)',
			'Spring clamp (large)',
			'Foam brick'
			]

	# def get_classes_names ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Constants
		S = 5
		B = arguments.number_of_detectors_per_cell
		C = 21

		# Create model
		model = pykeras.applications.YoloV1(
			# Architecture options
			include_lrn = False,
			include_top = True,
			# Input
			input_tensor = None,
			input_shape = (480, 640, 3), # images get resized to 448x448 in the model
			input_dtype = 'float32',
			input_name = arguments.image_feature_name,
			# Output
			number_of_cells = S,
			number_of_detectors_per_cell = B,
			number_of_classes = C,
			classifier_activation = 'linear',
			output_name = arguments.yolo_v1_feature_name
			)

		W = 1.
		H = 1.

		# Compile model
		model.compile(
			loss = pykeras.losses.yolo_v1.YoloLoss(
				                      image_size = (W, H),
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'loss'
					),
			optimizer = keras.optimizers.SGD( learning_rate=0.0001 ),
			metrics = [
				pykeras.losses.yolo_v1.XYLoss(
					                  image_size = (W, H),
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'xy_loss'
					),
				pykeras.losses.yolo_v1.WHLoss(
					                  image_size = (W, H),
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'wh_loss'
					),
				pykeras.losses.yolo_v1.OBJLoss(
					                  image_size = (W, H),
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'obj_loss'
					),
				pykeras.losses.yolo_v1.NOOBJLoss(
					                  image_size = (W, H),
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'noobj_loss'
					),
				pykeras.losses.yolo_v1.ClassificationLoss(
					             number_of_cells = S,
					number_of_detectors_per_cell = B,
					           number_of_classes = C,
					                        name = 'c_loss'
					)
				]
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'loc/yolo-v1.ycb_video/sgd'

	# def get_name ( . )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'testing_2d.tfrecords',
			    blueprint_filename = 'testing_2d.json',
			                  name = 'ycb_video_testing_2d',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.yolo_v1_feature_name
			                         ],
			               shuffle = False,
			   shuffle_buffer_size = -1,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = False,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = { arguments.yolo_v1_feature_name: arguments.yolo_v1_feature_name }
			)
		return testing

	# def get_testing_dataset ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training_2d.tfrecords',
			    blueprint_filename = 'training_2d.json',
			                  name = 'ycb_video_training_2d',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.mask_feature_name,
			                         arguments.yolo_v1_feature_name
			                         ],
			               shuffle = arguments.shuffle,
			   shuffle_buffer_size = arguments.shuffle_buffer_size,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomBrightness,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomContrast,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomHue,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomSaturation,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.8,
			                upper = 1.0
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = { arguments.yolo_v1_feature_name: arguments.yolo_v1_feature_name }
			)
		return training

	# def get_training_dataset ( cls, arguments )
		
	# --------------------------------------------------

	@classmethod
	def get_labels_bounding_rectangles ( cls, image_index, labels, arguments ):
		"""
		Returns the bounding rectangles corresponding to the given labels in the image with the given index.

		This method is used on one label, and not a batch.
		It returns an array of bounging rectangles, one for each object instance.
		
		The returned bounding rectangle must be in the given format with all coordinates in image relative coordinates:
		[x1, y1, x2, y2, category index]
		
		Arguments:
			cls                                (`type`): Experiment class type.
			image_index                         (`int`): Index of the image.
			labels (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments            (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets localizations and categories.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( labels, dict )
		assert pytools.assertions.dictionary_types_are( labels, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		# Fetch the yolo labels
		yolo = labels[ arguments.yolo_v1_feature_name ][ image_index ]

		# Get the yolo dimensions
		S = yolo.shape[ 0 ]
		B = 1  # in the labels B is alwayse 1
		C = 21

		# Rectangle buffer
		rectangles = []

		# Go trough each cell and decode the bounding rectangle
		for i in range(S):
			for j in range(S):

				# Get the cell data
				cell = yolo[ i, j ]

				# Split rectangle and category
				rect    = cell[ 0:4 ]
				conf    = cell[ 4 ]
				one_hot = cell[ 5: ]
				
				if conf == 0.:
					continue

				# Convert category
				category = numpy.argmax( one_hot, axis=-1 )

				# Convert rect
				rect = numpy.asarray([
					rect[0],
					rect[1],
					rect[2],
					rect[3],
					category,
					1.0
					], dtype=numpy.float32)
				rectangles.append( rect )

			# for j in range(S)
		# for i in range(S)

		return numpy.asarray( rectangles, dtype=numpy.float32 )

	# def get_labels_bounding_rectangles ( cls, image_index, labels, arguments )

	# --------------------------------------------------

	@classmethod
	def get_predictions_bounding_rectangles ( cls, image_index, predictions, arguments ):
		"""
		Returns the bounding rectangles corresponding to the given labels in the image with the given index.

		The returned bounding rectangle must be in the given format with all coordinates in image relative coordinates:
		[x1, y1, x2, y2, category index]
		
		Arguments:
			cls                                (`type`): Experiment class type.
			image_index                         (`int`): Index of the image.
			labels (`dict` of `str` to `numpy.ndarray`): Ground truth features.
			arguments            (`argparse.Namespace`): Command line arguments.

		Returns:
			`numpy.ndarray`: Objets localizations and categories.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( image_index, int )
		assert pytools.assertions.type_is( predictions, dict )
		assert pytools.assertions.dictionary_types_are( predictions, str, numpy.ndarray )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )
		
		# Fetch the yolo predictions
		yolo = predictions[ arguments.yolo_v1_feature_name ][ image_index ]

		# Get the yolo dimensions
		S = yolo.shape[ 0 ]
		B = arguments.number_of_detectors_per_cell
		C = 21

		# Cell dimensions
		image_width  = 1.
		image_height = 1.
		cell_width   = image_width  / float(S)
		cell_height  = image_height / float(S)

		# Rectangle buffer
		rectangles = []

		# Go trough each cell and decode the bounding rectangles
		for i in range(S):
			for j in range(S):

				# Compute cell position
				cell_x = cell_width  * float(j)
				cell_y = cell_height * float(i)

				# Get the cell data
				cell = yolo[ i, j ]

				# Get the predicted logits
				one_hot  = cell[ (B*5): ]
				category = numpy.argmax( one_hot, axis=-1 )

				# Go through detectors
				for b in range(B):
					
					# Get the b'th rectangle
					rect = cell[ (b+0)*5:(b+1)*5 ]
					cx   = rect[ 0 ]
					cy   = rect[ 1 ]
					w    = rect[ 2 ]
					h    = rect[ 3 ]
					conf = rect[ 4 ]

					# Take the square of the width and height
					w    = numpy.square( w ) * image_width
					h    = numpy.square( h ) * image_height

					# Do not ignore this rectangle ?
					if conf >= arguments.confidence_threshold:
						
						# Convert rect
						cx = cx * cell_width  + cell_x
						cy = cy * cell_height + cell_y
						x1 = cx - w / 2.
						y1 = cy - h / 2.
						x2 = cx + w / 2.
						y2 = cy + h / 2.

						rect = numpy.asarray([
							x1 / image_width,
							y1 / image_height,
							x2 / image_width,
							y2 / image_height,
							float(category),
							conf
							], dtype=numpy.float32 )
						rectangles.append( rect )

					# if conf >= 0.5
				# for b in range(B)
			# for j in range(S)
		# for i in range(S)

		return numpy.asarray( rectangles, dtype=numpy.float32 )

	# def get_predictions_bounding_rectangles ( cls, image_index, prediction, arguments )

# class XP ( pykeras.experiment.LocalizationExperiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
