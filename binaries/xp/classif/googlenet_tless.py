# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import tensorflow
from tensorflow import keras

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.ClassificationExperiment ):
	"""
	This experiment applies GoogleNet to classify LINEMOD images.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( pykeras.experiments.ClassificationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'visible_mask',
			   help = 'Name of the feature containing the object foreground mask.'
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle of the object.'
			)
		parser.add_argument(
			'--one_hot_category_feature_name',
			   type = str,
			default = 'one_hot_category',
			   help = 'Name of the feature containing the one-hot category.'
			)
		parser.add_argument(
			'--top',
			   type = int,
			default = 1,
			   help = 'A result is correst if it is amoungst the top-n best scoring classes'
			)
		return parser

	# def get_argument_parser ( cls )

	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories classified in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return [ '{:02d}'.format(i) for i in range(30) ]

	# def get_classes_names ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.googlenet.GoogLeNet(
			          include_lrn = True,
			          include_top = True,
			         input_tensor = None,
			          input_shape = (224, 224, 3),
			          input_dtype = 'float32',
			           input_name = arguments.image_feature_name,
			              classes = 30,
			classifier_activation = 'softmax',
			        outputs_names = [ arguments.one_hot_category_feature_name, 'aux1', 'aux2' ]
			)

		# Compile model
		model.compile(
			        loss = pykeras.applications.googlenet.get_losses(),
			loss_weights = pykeras.applications.googlenet.get_losses_weights(),
			   optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			     metrics = {
				arguments.one_hot_category_feature_name: [
					pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map' ),
					pykeras.metrics.LogitsTruePositives( threshold=0.75, normalized=True, name='tp' ),
					pykeras.metrics.LogitsTrueNegatives( threshold=0.25, normalized=True, name='tn' ),
					pykeras.metrics.LogitsFalsePositives( threshold=0.75, normalized=True, name='fp' ),
					pykeras.metrics.LogitsFalseNegatives( threshold=0.25, normalized=True, name='fn' ),
					pykeras.metrics.LogitsPrecision( threshold=0.75, name='p' ),
					pykeras.metrics.LogitsRecall( threshold=0.75, name='r' )
					],
				'aux1': [
					pykeras.metrics.LogitsPrecision( threshold=0.75, name='p1' ),
					pykeras.metrics.LogitsRecall( threshold=0.75, name='r1' )
					],
				'aux2': [
					pykeras.metrics.LogitsPrecision( threshold=0.75, name='p2' ),
					pykeras.metrics.LogitsRecall( threshold=0.75, name='r2' )
					]
				}
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'classification/googlenet.tless/sgd'

	# def get_name ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'testing_v2.tfrecords',
			    blueprint_filename = 'testing_v2.json',
			                  name = 'testing',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.one_hot_category_feature_name
			    ],
			                repeat = False,
			               shuffle = False,
			   shuffle_buffer_size = 0,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = ( 0,  0),
			                       pad_size = (16, 16),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'nearest',
			affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		# testing.add_mapper(
		# 	pykeras.inputs_v2.mappers.BackgroundExchanger,
		# 	backgrounds_directory_path = 'resources/sun2012',
		# 	        image_feature_name = arguments.image_feature_name,
		# 	         mask_feature_name = arguments.mask_feature_name
		# 	)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = {
			'one_hot_category': arguments.one_hot_category_feature_name,
			'aux1': arguments.one_hot_category_feature_name,
			'aux2': arguments.one_hot_category_feature_name
			}
			)
		return testing

	# def get_testing_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training.tfrecords',
			    blueprint_filename = 'training.json',
			                  name = 'training',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.one_hot_category_feature_name
			    ],
			                repeat = False,
			               shuffle = True,
			   shuffle_buffer_size = arguments.shuffle_buffer_size,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.RandomScaleBoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = ( 0,  0),
			                       pad_size = (16, 16),
			                      min_scale = 0.9,
			                      max_scale = 1.1,
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'nearest',
			affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomBrightness,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomContrast,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomHue,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomSaturation,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.8,
			                upper = 1.0
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = {
			'one_hot_category': arguments.one_hot_category_feature_name,
			'aux1': arguments.one_hot_category_feature_name,
			'aux2': arguments.one_hot_category_feature_name
			}
			)
		return training

	# def get_training_dataset ( cls, arguments )

# class XP ( pykeras.experiment.ClassificationExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
