# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import keras
import tensorflow

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.ClassificationExperiment ):
	"""
	This experiment applies GoogleNet to classify LINEMOD images.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories classified in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return [
			'ape',
			'benchviseblue',
			'can',
			'cat',
			'driller',
			'duck',
			'glue',
			'holepuncher',
			'iron',
			'lamp',
			'phone'
			]

	# def get_classes_names ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.googlenet.GoogLeNet(
			          include_lrn = True,
			          include_top = True,
			         input_tensor = None,
			          input_shape = (224, 224, 3),
			          input_dtype = 'float32',
			           input_name = arguments.image_feature_name,
			              classes = 11,
			classifier_activation = 'softmax',
			        outputs_names = [ arguments.one_hot_category_feature_name, 'aux1', 'aux2' ]
			)

		# Compile model
		model.compile(
			        loss = pykeras.applications.googlenet.get_losses(),
			loss_weights = pykeras.applications.googlenet.get_losses_weights(),
			   optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			     metrics = {
				arguments.one_hot_category_feature_name: [
					pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map' ),
					pykeras.metrics.LogitsTruePositives( threshold=0.75, normalized=True, name='tp' ),
					pykeras.metrics.LogitsTrueNegatives( threshold=0.25, normalized=True, name='tn' ),
					pykeras.metrics.LogitsFalsePositives( threshold=0.75, normalized=True, name='fp' ),
					pykeras.metrics.LogitsFalseNegatives( threshold=0.25, normalized=True, name='fn' ),
					pykeras.metrics.LogitsPrecision( threshold=0.75, name='p' ),
					pykeras.metrics.LogitsRecall( threshold=0.75, name='r' ),
					pykeras.metrics.LogitsConfusionMatrix( number_of_classes=11, name='cm' )
					],
				'aux1': [
					pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map1' ),
					],
				'aux2': [
					pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map2' ),
					]
				}
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'classification/googlenet.linemod/sgd'

	# def get_name ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		testing = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename ='testing.tfrecords',
			   blueprint_filename ='testing.json',
			                 name = 'linemod-testing',
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = [ arguments.one_hot_category_feature_name ],
			              shuffle = False,
			  shuffle_buffer_size = -1,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = False,
			        prefetch_size = tensorflow.data.AUTOTUNE
			)

		testing.map( pykeras.inputs.OnlineFeatureAdapter,
			              name = 'feature-adapter',
			inputs_adaptations = {},
			labels_adaptations = {
			    arguments.one_hot_category_feature_name: arguments.one_hot_category_feature_name,
			    'aux1': arguments.one_hot_category_feature_name,
			    'aux2': arguments.one_hot_category_feature_name
			    }
			)
		
		return testing

	# def get_testing_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename = 'training.tfrecords',
			   blueprint_filename = 'training.json',
			                 name = 'linemod-training',
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = [ arguments.one_hot_category_feature_name ],
			              shuffle = arguments.shuffle,
			  shuffle_buffer_size = arguments.shuffle_buffer_size,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = arguments.drop_batch_reminder,
			        prefetch_size = arguments.prefetch_size
			)

		training.map( pykeras.inputs.OnlineFeatureAdapter,
			              name = 'feature-adapter',
			inputs_adaptations = {},
			labels_adaptations = {
			    arguments.one_hot_category_feature_name: arguments.one_hot_category_feature_name,
			    'aux1': arguments.one_hot_category_feature_name,
			    'aux2': arguments.one_hot_category_feature_name
			    }
			)

		return training

	# def get_training_dataset ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_validation_dataset ( cls, arguments ):
		"""
		Returns the dataset used to validate the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The validation dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		validation = pykeras.inputs.TFDataset.open(
			       directory_path = arguments.dataset_path,
			   tf_record_filename = 'testing.tfrecords',
			   blueprint_filename = 'testing.json',
			                 name = 'linemod-validation',
			inputs_features_names = [ arguments.image_feature_name ],
			labels_features_names = [ arguments.one_hot_category_feature_name ],
			              shuffle = arguments.shuffle,
			  shuffle_buffer_size = arguments.shuffle_buffer_size,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = arguments.drop_batch_reminder,
			        prefetch_size = arguments.prefetch_size
			)

		validation.map( pykeras.inputs.OnlineFeatureAdapter,
			              name = 'feature-adapter',
			inputs_adaptations = {},
			labels_adaptations = {
			    arguments.one_hot_category_feature_name: arguments.one_hot_category_feature_name,
			    'aux1': arguments.one_hot_category_feature_name,
			    'aux2': arguments.one_hot_category_feature_name
			    }
			)
		
		return validation

	# def get_validation_dataset ( cls, arguments )

# class XP ( pykeras.experiment.ClassificationExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
