# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import numpy
import pandas
from tensorflow import keras
import tensorflow

# INTERNALS
import pydataset
import pykeras
import pytools

class LogitsMapper ( pykeras.inputs_v2.mappers.Mapper ):
	def __init__ ( self, table, settings=pykeras.inputs_v2.mappers.MapperSettings() ):
		settings.name = 'logits-mapper'
		super( LogitsMapper, self ).__init__( settings )
		self.table = table

	def map_features ( self, features ):
		one_hot_category = features[ 'one_hot_category' ]
		category_index   = tensorflow.argmax( one_hot_category )
		features[ 'one_hot_category' ] = self.table[ category_index, : ]
		return features

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.ClassificationExperiment ):
	"""
	This experiment applies GoogleNet to classify LINEMOD images.
	"""



	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.

		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""		
		parser = super( pykeras.experiments.ClassificationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--mask_feature_name',
			   type = str,
			default = 'visible_mask',
			   help = 'Name of the feature containing the object foreground mask.'
			)
		parser.add_argument(
			'--bounding_rectangle_feature_name',
			   type = str,
			default = 'bounding_rectangle',
			   help = 'Name of the feature containing the bounding rectangle of the object.'
			)
		parser.add_argument(
			'--one_hot_category_feature_name',
			   type = str,
			default = 'one_hot_category',
			   help = 'Name of the feature containing the one-hot category.'
			)
		return parser

	# def get_argument_parser ( cls )

	# --------------------------------------------------

	@classmethod
	def get_classes_names ( cls, arguments ):
		"""
		Returns the names of the categories classified in this experiment.

		Arguments:
			cls                     (`type`): Experiment class type.
			arguments (`argparse.Namespace`): Arguments of the experiment.
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is( arguments, argparse.Namespace )

		return cls.CLASSES_NAMES

	# def get_classes_names ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		class WordtreeSoftmax ( keras.layers.Activation ):
			def __init__ ( self, **kwargs ):
				super( WordtreeSoftmax, self ).__init__( activation=None, **kwargs )
				# Build layers for every slices
				for slice_index in range( cls.NUMBER_OF_SLICES ):
					# i'th slice layer
					slice_name   = 'slice_{:d}'.format(slice_index)
					slice_start  = int( cls.SLICES[ slice_index ][ 0 ] )
					slice_end    = int( cls.SLICES[ slice_index ][ 1 ] + 1 )
					# print( slice_name, ':', slice_start, slice_end )
					slice_layer  = pykeras.layers.Slice( slice_start, slice_end, name=slice_name )
					setattr( self, slice_name, slice_layer )
					# i'th softmaw
					softmax_name  = 'softmax_{:d}'.format(slice_index)
					softmax_layer = keras.layers.Softmax( name=softmax_name )
					setattr( self, softmax_name, softmax_layer )
					
				# Concatenation layer
				self.concat_layer = keras.layers.Concatenate( axis=-1 )
			def __call__ ( self, inputs ):
				slices = []
				for slice_index in range( cls.NUMBER_OF_SLICES ):
					# i'th slice layer
					slice_name  = 'slice_{:d}'.format(slice_index)
					slice_layer = getattr( self, slice_name )
					# i'th softmaw
					softmax_name  = 'softmax_{:d}'.format(slice_index)
					softmax_layer = getattr( self, softmax_name )
					# apply
					x = slice_layer( inputs )
					x = softmax_layer( x )
					slices.append( x )
				return self.concat_layer( slices )

		# Create model
		model = pykeras.applications.vgg19net.VGG19Net(
			          include_top = True,
			         input_tensor = None,
			          input_shape = (224, 224, 3),
			          input_dtype = 'float32',
			           input_name = arguments.image_feature_name,
			              classes = cls.NUMBER_OF_CLASSES,
			classifier_activation = WordtreeSoftmax(),
			          output_name = arguments.one_hot_category_feature_name
			)

		# Compile model
		model.compile(
			     loss = keras.losses.CategoricalCrossentropy( from_logits=False ),
			optimizer = keras.optimizers.SGD( learning_rate=0.001 ),
			  metrics = [
				pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map' ),
				pykeras.metrics.LogitsTruePositives( threshold=0.75, normalized=True, name='tp' ),
				pykeras.metrics.LogitsTrueNegatives( threshold=0.25, normalized=True, name='tn' ),
				pykeras.metrics.LogitsFalsePositives( threshold=0.75, normalized=True, name='fp' ),
				pykeras.metrics.LogitsFalseNegatives( threshold=0.25, normalized=True, name='fn' ),
				pykeras.metrics.LogitsPrecision( threshold=0.75, name='p' ),
				pykeras.metrics.LogitsRecall( threshold=0.75, name='r' )
				]
			)

		# Return model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'classification/vgg19net.tless/sgd.v5'

	# def get_name ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )
		
		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'testing_v2.tfrecords',
			    blueprint_filename = 'testing_v2.json',
			                  name = 'testing',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.one_hot_category_feature_name
			    ],
			                repeat = False,
			               shuffle = False,
			   shuffle_buffer_size = 0,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (384, 384),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'nearest',
			affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		testing.add_mapper( LogitsMapper( tensorflow.constant(XP.TABLE) ) )
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = { 'one_hot_category': arguments.one_hot_category_feature_name }
			)
		return testing

	# def get_testing_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename = 'training.tfrecords',
			    blueprint_filename = 'training.json',
			                  name = 'training',
			initial_features_names = [
			    arguments.image_feature_name,
			    arguments.mask_feature_name,
			    arguments.bounding_rectangle_feature_name,
			    arguments.one_hot_category_feature_name
			    ],
			                repeat = False,
			               shuffle = True,
			   shuffle_buffer_size = arguments.shuffle_buffer_size,
			            batch_size = arguments.batch_size,
			   drop_batch_reminder = arguments.drop_batch_reminder,
			         prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BoundingRectangleCropper,
			          images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
			                      crop_size = (384, 384),
			        affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			  images_features_names = [ arguments.image_feature_name, arguments.mask_feature_name ],
			                   size = (224, 224),
			                 method = 'nearest',
			affected_features_names = [ arguments.bounding_rectangle_feature_name ]
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.BackgroundExchanger,
			backgrounds_directory_path = 'resources/sun2012',
			        image_feature_name = arguments.image_feature_name,
			         mask_feature_name = arguments.mask_feature_name
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomBrightness,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomContrast,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.9,
			                upper = 1.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomHue,
			images_features_names = [ arguments.image_feature_name ],
			            max_delta = 0.1
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesRandomSaturation,
			images_features_names = [ arguments.image_feature_name ],
			                lower = 0.8,
			                upper = 1.0
			)
		training.add_mapper( LogitsMapper( tensorflow.constant(XP.TABLE) ) )
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { 'image': arguments.image_feature_name },
			labels = { 'one_hot_category': arguments.one_hot_category_feature_name }
			)
		return training

	# def get_training_dataset ( cls, arguments )

# class XP ( pykeras.experiment.ClassificationExperiemnt )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	XP.CLASSES_NAMES     = numpy.loadtxt( 'data/t_less/tless_names.csv', delimiter=',', dtype=str, skiprows=1 )[1:].tolist()
	XP.NUMBER_OF_CLASSES = len( XP.CLASSES_NAMES )
	XP.SLICES            = numpy.loadtxt( 'data/t_less/tless_slices.csv', delimiter=',', dtype=int, skiprows=1 )
	XP.NUMBER_OF_SLICES  = XP.SLICES.shape[ 0 ]
	XP.TABLE             = pandas.read_csv( 'data/t_less/tless_lexique.csv', index_col=0 )
	# print( XP.TABLE )
	XP.TABLE = XP.TABLE.to_numpy( dtype=int, copy=True )[:, 1:]
	return XP

# def get_experiment ()
