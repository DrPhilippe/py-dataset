# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import keras

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  CLASS XP                  ###
# ##################################################

class XP ( pykeras.experiments.ClassificationExperiment ):
	"""
	This experiment applies AlexNet to classify MNIST images.
	"""
	
	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def get_argument_parser ( cls ):
		"""
		Returns the argument parser used in this experiment.
	
		Arguments:
			cls (`type`): The experiment class.
		
		Returns:
			`argparse.ArgumentParser`: The argument parser.
		"""
		parser = super( pykeras.experiments.ClassificationExperiment, cls ).get_argument_parser()
		parser.add_argument(
			'--image_feature_name',
			   type = str,
			default = 'image',
			   help = 'Name of the feature containing the image to classify.'
			)
		parser.add_argument(
			'--digit_feature_name',
			    type = str,
			 default = 'digit',
			    help = 'Name of the feature containing to the digit.'
			)
		parser.add_argument(
			'--one_hot_digit_feature_name',
			   type = str,
			default = 'one_hot_digit',
			   help = 'Name of the feature containing to the one-hot digit.'
			)
		parser.add_argument(
			'--category_index_feature_name',
			   type = str,
			default = 'digit',
			   help = 'Name of the feature containing the category index.'
			)
		parser.add_argument(
			'--one_hot_category_feature_name',
			   type = str,
			default = 'one_hot_digit',
			   help = 'Name of the feature containing the one-hot category.'
			)
		return parser

	# def get_argument_parser ( cls )

	# --------------------------------------------------

	@classmethod
	def get_model ( cls, arguments ):
		"""
		Returns the compiled model used in this experiment.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`keras.Model`: The model.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Create model
		model = pykeras.applications.alexnet.AlexNet(
			          include_lrn = True,
			          include_top = True,
			         input_tensor = None,
			          input_shape = (227, 227, 1),
			          input_dtype = 'float32',
			           input_name = arguments.image_feature_name,
			              classes = 10,
			classifier_activation = 'softmax',
			          output_name = arguments.one_hot_digit_feature_name
			)

		# Compile model
		model.compile(
			     loss = keras.losses.CategoricalCrossentropy( from_logits=False ),
			optimizer = keras.optimizers.Adam(),
			  metrics = [
				pykeras.metrics.LogitsAccuracy( threshold=0.75, name='map' ),
				pykeras.metrics.LogitsTruePositives( threshold=0.75, normalized=True, name='tp' ),
				pykeras.metrics.LogitsTrueNegatives( threshold=0.25, normalized=True, name='tn' ),
				pykeras.metrics.LogitsFalsePositives( threshold=0.75, normalized=True, name='fp' ),
				pykeras.metrics.LogitsFalseNegatives( threshold=0.25, normalized=True, name='fn' ),
				pykeras.metrics.LogitsPrecision( threshold=0.75, name='p' ),
				pykeras.metrics.LogitsRecall( threshold=0.75, name='r' ),
				pykeras.metrics.LogitsConfusionMatrix( number_of_classes=10, name='cm' )
				]
			)

		# Return the model
		return model

	# def get_model ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_name ( cls, arguments ):
		"""
		Retuns the name of this experiment.
		
		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`str`: Name of the experiment.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		return 'classification/alexnet.mnist/sgd'

	# def get_name ( cls, arguments )
	
	# --------------------------------------------------

	@classmethod
	def get_testing_dataset ( cls, arguments ):
		"""
		Returns the dataset used to test the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The testing dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Load the dataset
		testing = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename ='testing.tfrecords',
			    blueprint_filename ='testing.json',
			                  name = 'mnist-testing',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.one_hot_digit_feature_name
			                         ],
			              shuffle = False,
			  shuffle_buffer_size = -1,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = False,
			        prefetch_size = arguments.prefetch_size
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			images_features_names = [ 'image' ],
			                 size = (227, 227),
			               method = 'bilinear'
			)
		testing.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = { arguments.one_hot_digit_feature_name: arguments.one_hot_digit_feature_name }
			)

		# Return the dataset
		return testing

	# def get_testing_dataset ( cls, arguments )

	# --------------------------------------------------

	@classmethod
	def get_training_dataset ( cls, arguments ):
		"""
		Returns the dataset used to train the model.

		Arguments:
			cls                     (`type`): The experiment class.
			arguments (`argparse.Namespace`): Arguments of the experiment.

		Returns:
			`pykeras.inputs.Dataset`: The training dataset.
		"""
		assert pytools.assertions.type_is_instance_of( arguments, argparse.Namespace )

		# Load the dataset
		training = pykeras.inputs_v2.TFRecordsDataset.open(
			        directory_path = arguments.dataset_path,
			    tf_record_filename ='training.tfrecords',
			    blueprint_filename ='training.json',
			                  name = 'mnist-training',
			initial_features_names = [
			                         arguments.image_feature_name,
			                         arguments.one_hot_digit_feature_name
			                         ],
			              shuffle = arguments.shuffle,
			  shuffle_buffer_size = arguments.shuffle_buffer_size,
			           batch_size = arguments.batch_size,
			  drop_batch_reminder = arguments.drop_batch_reminder,
			        prefetch_size = arguments.prefetch_size
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.ImagesResizer,
			images_features_names = [ 'image' ],
			                 size = (227, 227),
			               method = 'bilinear'
			)
		training.add_mapper(
			pykeras.inputs_v2.mappers.FeaturesMapper,
			inputs = { arguments.image_feature_name: arguments.image_feature_name },
			labels = { arguments.one_hot_digit_feature_name: arguments.one_hot_digit_feature_name }
			)

		# Return the dataset
		return training

	# def get_training_dataset ( cls, arguments )
	
# class XP ( pykeras.experiments.ClassificationExperiment )

# ##################################################
# ###                 FUNCTIONS                  ###
# ##################################################

# --------------------------------------------------

def get_experiment ():
	"""
	Entry point of the module used to get the experiment to run.

	Returns:
		`type`: The experiment class.
	"""
	return XP

# def get_experiment ()
