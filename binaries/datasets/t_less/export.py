# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pykeras
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/t_less'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL where to import T-LESS dataset.', default=DATASET_URL )
arguments = parser.parse_args()

##################################################
###      EXPORT INSTANCE-WISE TF-RECORDS       ###
##################################################

def visibility_filter ( example ):
	visibility = example.get_feature( 'visibility' ).value
	return visibility < 0.4

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Training TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	            logger = pytools.tasks.file_logger( 'logs/tless_train_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'training_instancewise.tfrecords',
	blueprint_filename = 'training_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/sensors/groups/kinect/groups/train/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		# 'mesh',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'bb8_v1',
		'bb8_v1_ref',
		'bb8_v2',
		'bb8_v2_ref',
		'R',
		'q',
		't'
		]
	)
exporter.execute()

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Test TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	         filter_fn = visibility_filter,
	            logger = pytools.tasks.file_logger( 'logs/tless_test_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'testing_instancewise.tfrecords',
	blueprint_filename = 'testing_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/sensors/groups/kinect/groups/test/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		# 'mesh',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'bb8_v1',
		'bb8_v1_ref',
		'bb8_v2',
		'bb8_v2_ref',
		'R',
		'q',
		't'
		]
	)
exporter.execute()
