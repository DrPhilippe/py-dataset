
import cv2
import numpy
import pydataset

# for i in range( 1213 ):
scene_index = 15
image_index = 324
image_url = 'file://D:/Datasets/tless/groups/sensors/groups/kinect/groups/test/groups/{:02d}/groups/{:04d}'.format(
	scene_index,
	image_index
	)
image_group = pydataset.dataset.get( image_url )
image       = image_group.get_feature( 'image' ).value
K           = image_group.get_feature( 'K').value
dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )
H, W        = image.shape[0], image.shape[1]

display_mask = image.copy()
display_lcs  = image.copy()

count = len(image_group.examples_names)

COLORS = [
	(128, 128, 255),
	(128, 255, 255),
	(128, 255, 128),
	(255, 255, 128),
	(255, 128,   0),
	(192, 128, 255),
	(225,   0,   0),
	(  0, 255,   0),
	(0,     0, 255)
	]

for instance_index in range(count):

	instance_example = image_group.get_example( instance_index, name_format='{}{:04d}', prefix='' )
	mesh  = instance_example.get_feature( 'mesh' ).value
	R     = instance_example.get_feature( 'R'    ).value
	t     = instance_example.get_feature( 't'    ).value
	mask  = instance_example.get_feature( 'mask' ).value
	r     = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )
	color = COLORS[ instance_index ]


	# BB
	# bb    = mesh.get_bounding_box()
	# bb, _ = cv2.projectPoints( bb, r, t, K, dist_coeffs )
	# bb    = numpy.reshape( bb, [-1,2] )
	# image = pydataset.cv2_drawing.draw_bounding_box( image, bb,
	# 	edges_color      = color.to_tuple(False),
	# 	edges_thickness  = 1,
	# 	points_color     = color.to_tuple(False),
	# 	points_radius    = 1,
	# 	points_thickness = 1
	# 	)

	# MESH CLOUD
	# points, _    = cv2.projectPoints( mesh.points, r, t, K, dist_coeffs )
	# points       = numpy.reshape( points, [-1,2] )
	# display_mask = pydataset.cv2_drawing.draw_points( display_mask, points, color=color, radius=1, thickness=1 )

	# # MASK
	mask   = numpy.reshape( mask, [H, W, 1] )
	mask   = numpy.tile( mask, [1, 1, 3] )
	colors = numpy.reshape( numpy.asarray(color, dtype=numpy.uint8), [1, 1, 3] )
	colors = numpy.tile( colors, [H, W, 1] )
	display_mask = numpy.where( mask, colors, display_mask )

	# LCS
	display_lcs = cv2.drawFrameAxes( display_lcs, K, dist_coeffs, r, t, 0.1, 2 )


cv2.imshow( 'mask', display_mask )
cv2.imshow( 'lcs', display_lcs )

if cv2.waitKey(0) == ord('q'):
	exit()

cv2.imwrite( 'image.png', image )
cv2.imwrite( 'mask.png', display_mask )
cv2.imwrite( 'lcs.png', display_lcs )
