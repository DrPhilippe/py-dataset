import json
import numpy
import pandas

# ##################################################
# original number of categories
old_count = 30

# ##################################################
# read new lexique
with open( 'data/t_less/tless_lexique.json', 'rt' ) as file:
	lexique   = json.load( file )
	new_count = len( lexique )
	file.close()

# ##################################################
# Helper func
def propagate_associations ( lexique, index=-1, depth=0 ):
	"""
	Propagates to parent the indexes of tless category in the lexique tree.
	"""
	current = lexique[ index+1 ]

	if current[ "children" ]:
		indexes = []
		for child_index in range( current[ "children" ][0], current[ "children" ][1]+1 ):
			indexes += propagate_associations( lexique, child_index, depth+1 )
			indexes = list(dict.fromkeys(indexes))
			indexes = sorted( indexes )
		current[ "tless_index" ] = indexes
		return indexes
	
	else:
		return current[ "tless_index" ]

# Apply helper
propagate_associations( lexique )

# write new lexique
with open( 'data/t_less/tless_lexique.json', 'wt' ) as file:
	json.dump( lexique, file, indent=4, sort_keys = True, separators = (',', ': ') )
	file.close()

# ##################################################
# Log lexique as tree
def log ( lexique, file, index=-1, depth=0 ):
	word = lexique[ index+1 ]
	file.write( "    "*depth + "{} {}: {}\n".format(
		word[ "wordnet_index" ],
		word[ "name" ],
		word[ "tless_index" ]
		))
	if word["children"]:
		for child_index in range( word["children"][0], word["children"][1]+1 ):
			log( lexique, file, child_index, depth+1 )

with open( 'data/t_less/tless_lexique.txt', 'wt' ) as file:
	log( lexique, file )

# ##################################################
# Create final table
table   = numpy.zeros( [old_count, new_count], dtype=int )
names = [word["name"] for word in lexique]

# Fill the table
for tless_index in range( old_count ):
	for wordnet_index in range( new_count ):
		if (tless_index+1) in lexique[ wordnet_index ][ "tless_index" ]:
			table[ tless_index, wordnet_index ] = 1

# Save
table = pandas.DataFrame( table, columns=names, dtype=int )
table.to_csv( 'data/t_less/tless_lexique.csv', float_format='%01d', columns=names )

# ##################################################
# Create softmax slices indexes
slices = []
for word in lexique:
	if word[ "children" ]:
		slices.append([
			word[ "children" ][0],
			word[ "children" ][1]
			])
slices = numpy.asarray( slices, dtype=int )
slices = numpy.reshape( slices, [-1, 2] )
numpy.savetxt( 'data/t_less/tless_slices.csv', slices, fmt='%01d', header='start,end', delimiter=',' )

# ##################################################
# names of the categories
names = numpy.asarray( names, dtype=str )
numpy.savetxt( 'data/t_less/tless_names.csv', names, fmt='%s', header='name', delimiter=',' )
