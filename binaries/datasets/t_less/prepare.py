# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/t_less'
NUMBER_OF_THREADS = 24

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL where to create the imported BOP-LM dataset.', default=DATASET_URL )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to import the dataset.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP-YCB-VIDEO PREPARE' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    number_of_threads:  {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###        COMPUTE INSTANCE-WISE LABELS        ###
# ##################################################

mapper = pydataset.manipulators.ManipulationSequence.create(
	# Agregate objects sizes
	pydataset.manipulators.SizeAgregator.create(
		bounding_rectangle_feature_name = 'bounding_rectangle',
			      category_feature_name = 'category_index',
			      output_directory_path = 'data/bop_ycb_video/sizes',
			            filename_format = 'sizes_{}.csv',
			                       name = 'tless.size_agregator'
		),
	# PoseNet quaternion labels
	pydataset.manipulators.pose.Matrix2Quaternion.create(
		     input_matrix_feature_name = 'R',
		output_quaternion_feature_name = 'q',
		                          name = 'tless.quaternion'
		),
	# Bounding boxes for PnP computations
	pydataset.manipulators.pose.MeshToBoundingBox.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = '', # bounding boxes are computed in model space
		       translation_feature_name = '', # bounding boxes are computed in model space
		          camera_k_feature_name = '', # bounding boxes are computed in model space
		camera_dist_coeffs_feature_name = '', # bounding boxes are computed in model space
		      bounding_box_feature_name = 'bounding_box',
		                           name = 'tless.bounding_box'
		),
	# BB8 V1 labels
	pydataset.manipulators.bb8.MeshToBB8V1.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		    category_index_feature_name = 'category_index',
		           number_of_categories = 30,
		   instances_url_search_pattern = '',
		               bb8_feature_name = 'bb8_v1',
		                           name = 'tless.bb8_v1'
		),
	# BB8 V2 labels
	pydataset.manipulators.bb8.MeshToBB8V2.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		               bb8_feature_name = 'bb8_v2',
		                           name = 'tless.bb8_v2'
		),
	# Common settings
	url_search_pattern = 'groups/sensors/groups/kinect/groups/*/groups/*/groups/*/examples/*',
	              name = 'tless.instance_wise'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Instance-Wise Labels' ),
	     wait_for_application = True
	)