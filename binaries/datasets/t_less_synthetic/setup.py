# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pykeras
import pystless
import pylinemod
import pytools.tasks

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
# Locations
parser.add_argument( 'dataset_path', type=str, help='Path of the directory containg the raw S-TLESS dataset' )
parser.add_argument( 'models_path',  type=str, help='Path of the directory containg the T-LESS models' )
parser.add_argument( 'dataset_url',  type=str, help='URL of the -TLESS dataset to create' )
parser.add_argument( '--number_of_threads', type=int, default=32, help='Number of threads used to map the examples.')
arguments = parser.parse_args()

# Echo arguments
print( '=================================================' )
print( 'SYNTHETIC T-LESS SETUP:' )
print( '=================================================' )
print( 'dataset_path      :', arguments.dataset_path )
print( 'models_path       :', arguments.models_path  )
print( 'dataset_url       :', arguments.dataset_url  )
print( '-------------------------------------------------' )
print( 'number_of_threads :', arguments.number_of_threads  )
print( '=================================================' )
print( '' )

# ##################################################
# ###                   IMPORT                   ###
# ##################################################

# Import the raw dataset
importer = pystless.Importer.create(
	# task debug
	             progress_tracker = pytools.tasks.console_progress_logger( 'Importing SYNTHETIC T-LESS Dataset' ),
	                       logger = pytools.tasks.file_logger( 'logs/stless-setup.importer.log' ),
	# dst group
	                dst_group_url = arguments.dataset_url,
	        models_directory_path = arguments.models_path,
	# directories
	   raw_dataset_directory_path = arguments.dataset_path,
	  	        models_group_name = 'objects',
	  models_examples_name_format = 'obj_{:02d}',
	# common
	   camera_matrix_feature_name = 'K',
	            mesh_feature_name = 'mesh',
	# per image
	  contours_image_feature_name = '',#'contours',
	     depth_image_feature_name = '',#'depth',
	     color_image_feature_name = 'image',
	 instances_image_feature_name = 'instances',
	      mask_image_feature_name = '', #'mask',
	   normals_image_feature_name = '', #'normals',
	# per object instance
	  instance_index_feature_name = 'instance_index',
	  category_index_feature_name = 'category_index',
	   category_name_feature_name = 'category_name',
	one_hot_category_feature_name = 'one_hot_category',
	     translation_feature_name = 't',
	        rotation_feature_name = 'R',
	      visibility_feature_name = 'visibility',
	# other
	name = 'stless-importer'
	)
importer.execute()

# # ##################################################
# # ###               COMPUTE MASKS                ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.segmentation.InstancesMapToMask.create(
# 		 instances_map_feature_name = 'instances',
# 		instance_index_feature_name = '',
# 		          mask_feature_name = 'mask',
# 		               url_search_pattern = 'groups/{}/groups/*'.format( split_name ),
# 		                             name = 'stless-setup.masks'
# 		)
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Masks of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###            COMPUTE SEGMENTATION            ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.segmentation.MaskToSegmentationMap.create(
# 		     instances_url_search_pattern = 'examples/*',
# 		                mask_feature_name = 'mask',
# 		      category_index_feature_name = 'category_index',
# 		                number_of_classes = 30,
# 		             dedicated_background = True,
# 		        segmentation_feature_name = 'segmentation',
# 		               url_search_pattern = 'groups/{}/groups/*'.format( split_name ),
# 		                             name = 'stless-setup.segmentation'
# 		)
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Segmentations of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###            COMPUTE QUATERNIONS             ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.pose.Matrix2Quaternion.create(
# 		     input_matrix_feature_name = 'R',
# 		output_quaternion_feature_name = 'q',
# 		            url_search_pattern = 'groups/{}/groups/*/examples/*'.format( split_name ),
# 		                          name = 'stless-setup.quaternions'
# 		)
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Quaternions Angles of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###         COMPUTE BOUNDING RECTANGLE         ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.pose.MeshToBoundingRectangle.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R',
# 		       translation_feature_name = 't',
# 		          camera_k_feature_name = 'K',
# 		camera_dist_coeffs_feature_name = '',
# 		bounding_rectangle_feature_name = 'bounding_rectangle',
# 		             url_search_pattern = 'groups/{}/groups/*/examples/*'.format( split_name ),
# 		                           name = 'stless-setup.bounding-rectangles'
# 		)
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Bounding Rectangles of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###            COMPUTE BOUNDING BOX            ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.pose.MeshToBoundingBox.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R',
# 		       translation_feature_name = 't',
# 		          camera_k_feature_name = '',#'K',
# 		camera_dist_coeffs_feature_name = '',
# 		      bounding_box_feature_name = 'bounding_box',
# 		             url_search_pattern = 'groups/{}/groups/*/examples/*'.format( split_name ),
# 		                           name = 'stless-setup.bounding-boxes'
# 		)
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Bounding Boxes of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###           COMPUTE BB8 V1 LABELS            ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.bb8.MeshToBB8V1.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R',
# 		       translation_feature_name = 't',
# 		          camera_k_feature_name = 'K',
# 		camera_dist_coeffs_feature_name = '',
# 		   instances_url_search_pattern = 'examples/*',
# 			       number_of_categories = 30,
# 			category_index_feature_name = 'category_index',
# 			           bb8_feature_name = 'bb8_v1',
# 		             url_search_pattern = 'groups/{}/groups/*'.format( split_name ),
# 		                           name = 'stless-setup.bb8-v1'
# 		)  
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Creating BB8 V1 Labels of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###           COMPUTE BB8 V2 LABELS            ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.bb8.MeshToBB8V2.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R',
# 		       translation_feature_name = 't',
# 		          camera_k_feature_name = 'K',
# 		camera_dist_coeffs_feature_name = '',
# 			           bb8_feature_name = 'bb8_v2',
# 		             url_search_pattern = 'groups/{}/groups/*/examples/*'.format( split_name ),
# 		                           name = 'stless-setup.bb8-v2'
# 		)  
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Creating BB8 V2 Labels of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)

# # ##################################################
# # ###           COMPUTE YOLO V1 LABELS           ###
# # ##################################################

# for split_name in ['training', 'validation']:
# 	mapper = pydataset.manipulators.yolo_v1.Labeler.create(
# 		                number_of_cells = 3,
# 		              number_of_classes = 30,
# 		   instances_url_search_pattern = 'examples/*',
# 		             image_feature_name = 'image',
# 		bounding_rectangle_feature_name = 'bounding_rectangle',
# 		  one_hot_category_feature_name = 'one_hot_category',
# 		              yolo_feature_name = 'yolo_v1',
# 		                      normalize = True,
# 		             url_search_pattern = 'groups/{}/groups/*'.format( split_name ),
# 		                           name = 'stless-setup.yolo-v1'
# 		)  
# 	pydataset.manipulators.apply_async(
# 		              manipulator = mapper,
# 		url_search_pattern_prefix = arguments.dataset_url,
# 		        number_of_threads = arguments.number_of_threads,
# 		         progress_tracker = pytools.tasks.console_progress_logger( 'Creating YOLO V1 labels of Split "{}"'.format(split_name) ),
# 		     wait_for_application = True
# 		)