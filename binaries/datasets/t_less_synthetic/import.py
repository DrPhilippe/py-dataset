# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytless
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_DIR = 'C:/Datasets/T-LESS'
DATASET_URL = 'file://D:/Datasets/t_less'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_path', type=pytools.path.DirectoryPath, help='Path of the directory containg the T-LESS dataset to import.', default=DATASET_DIR )
parser.add_argument( '--dataset_url',  type=str, help='URL where to create the imported T-LESS dataset.', default=DATASET_URL )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'T-LESS IMPORT' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_path: {}', arguments.dataset_path )
pt.log( '    dataset_url:  {}', arguments.dataset_url )
pt.end_line()
pt.flush()

# ##################################################
# ###                   IMPORT                   ###
# ##################################################

# Import the raw dataset
importer = pytless.Importer.create(
	# dst group
	                  dst_group_url = arguments.dataset_url,
	# directories
	     raw_dataset_directory_path = arguments.dataset_path,
	                  sensors_names = [ 'kinect' ], # ['canon', 'kinect', 'primesense']
	# models
	              models_group_name = 'objects',
	    models_examples_name_format = 'obj_{:02d}',
	              mesh_feature_name = '',#'mesh',
	   mesh_subdivided_feature_name = 'mesh',#'mesh_subdivided',
	mesh_reconstructed_feature_name = '',#'mesh_reconst',
	# per image
	     camera_matrix_feature_name = 'K',
	       depth_image_feature_name = '',#'depth',
	       depth_scale_feature_name = '',#'depth_scale',
	       color_image_feature_name = 'image',
	              mask_feature_name = '',#'mask',
	      visible_mask_feature_name = 'visible_mask',
	render_depth_image_feature_name = '',#'render_depth',
	render_color_image_feature_name = '',#'render_image',
	# per object instance
	    instance_index_feature_name = 'instance_index',
	    category_index_feature_name = 'category_index',
	  one_hot_category_feature_name = 'one_hot_category',
	       translation_feature_name = 't',
	          rotation_feature_name = 'R',
	bounding_rectangle_feature_name = 'bounding_rectangle',
		    visibility_feature_name = 'visibility',
	# other
	                           name = 't_less_importer',
	# task debug
	               progress_tracker = pt,
	                         logger = pytools.tasks.file_logger( 'logs/t_less_importer.log' )
	)
importer.execute()