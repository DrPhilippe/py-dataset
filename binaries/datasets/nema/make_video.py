import cv2
import numpy
import pydataset

# REP = 1

# OPEN VIDEO
fourcc = cv2.VideoWriter_fourcc('H','2','6','4')   
writer = cv2.VideoWriter( 'preview.mp4', fourcc, 30, (1280, 720), True )

# COLORED FRAMES
red   = numpy.zeros( [720, 1280, 3], dtype=numpy.uint8 )
red[:, :, 2] = 255
blue  = numpy.zeros( [720, 1280, 3], dtype=numpy.uint8 )
blue[:, :, 0] = 255
green = numpy.zeros( [720, 1280, 3], dtype=numpy.uint8 )
green[:, :, 1] = 255
cyan = numpy.zeros( [720, 1280, 3], dtype=numpy.uint8 )
cyan[:, :, 0] = 255
cyan[:, :, 1] = 255
colors = [red, green, blue, cyan]
COLORS = [
	(0,0,255),
	(255,0,0),
	(0,255,0),
	(255,255,0)
]
# GET SCENE
subscene_group = pydataset.dataset.get( 'file://D:/Datasets/nema_v2/groups/scenes/groups/000000/groups/000217' )
background_color = subscene_group.get_feature( 'background_color' ).value
background_depth = subscene_group.get_feature( 'background_depth' ).value
K = subscene_group.get_feature( 'K' ).value
dist_coeffs = numpy.zeros([5], dtype=numpy.float32)

def put_centered_text ( image, text, font=cv2.FONT_HERSHEY_SIMPLEX, font_scale=3, color=(255,255,255), thickness=3, aliasing=cv2.LINE_AA ):
	# get boundary of this text
	textsize = cv2.getTextSize( text, font, font_scale, thickness )[0]
	# get coords based on boundary
	text_x = (image.shape[1] - textsize[0]) // 2
	text_y = (image.shape[0] + textsize[1]) // 2
	# add text centered on image
	return cv2.putText( image, text, (text_x, text_y), font, font_scale, color, thickness, aliasing )

# INTERLUDE
for i in range(30*3):
	frame = numpy.zeros( [720,1280,3],dtype=numpy.uint8 )
	frame = put_centered_text( frame, "IMAGES" )
	# cv2.imshow( 'display', frame )
	# cv2.waitKey(1)
	writer.write( frame )

# PASS 1 COLOR
for group_name in subscene_group.groups_names:
	print( 'frame {}'.format(group_name) )

	frame_group      = subscene_group.get_group( group_name )
	foreground_color = frame_group.get_feature( 'foreground_color' ).value
	
	# REPLACE ALL THE BACKGROUND
	mask = numpy.zeros( [720, 1280], dtype=numpy.bool )
	for example_name in frame_group.examples_names[1:]:
		example      = frame_group.get_example( example_name )
		current_mask = example.get_feature( 'mask' ).value
		mask = numpy.logical_or( mask, current_mask )
	mask = numpy.reshape( mask, [720, 1280, 1] )
	mask = numpy.tile( mask, [1, 1, 3] )
	mask = cv2.erode( mask.astype(numpy.uint8), numpy.ones( [3, 3], numpy.uint8) ).astype(numpy.bool)
	display = numpy.where( mask, foreground_color, background_color )

	# cv2.imshow( 'display', display )
	# cv2.waitKey(1)
	writer.write( display )

# INTERLUDE
for i in range(30*3):
	frame = numpy.zeros( [720,1280,3],dtype=numpy.uint8 )
	frame = put_centered_text( frame, "MASKS" )
	# cv2.imshow( 'display', frame )
	# cv2.waitKey(1)
	writer.write( frame )

# PASS 2 MASKS
for group_name in subscene_group.groups_names:
	print( 'frame {}'.format(group_name) )

	frame_group      = subscene_group.get_group( group_name )
	foreground_color = frame_group.get_feature( 'foreground_color' ).value
	
	# REPLACE ALL THE BACKGROUND
	mask = numpy.zeros( [720, 1280], dtype=numpy.bool )
	for example_name in frame_group.examples_names[1:]:
		example      = frame_group.get_example( example_name )
		current_mask = example.get_feature( 'mask' ).value
		mask = numpy.logical_or( mask, current_mask )
	mask = numpy.reshape( mask, [720, 1280, 1] )
	mask = numpy.tile( mask, [1, 1, 3] )
	mask = cv2.erode( mask.astype(numpy.uint8), numpy.ones( [3, 3], numpy.uint8) ).astype(numpy.bool)
	display = numpy.where( mask, foreground_color, background_color )

	# Draw the mask of each object
	color_index = 0
	for example_name in frame_group.examples_names[1:]:
		example      = frame_group.get_example( example_name )
		current_mask = example.get_feature( 'visible_mask' ).value
		current_mask = cv2.erode( current_mask.astype(numpy.uint8), numpy.ones( [3, 3], numpy.uint8) ).astype(numpy.bool)
		current_mask = numpy.reshape( current_mask, [720, 1280, 1] )
		current_mask = numpy.tile( current_mask, [1, 1, 3] )

		color = colors[ color_index ]
		color_index += 1
		display = numpy.where(
			current_mask,
			cv2.addWeighted( foreground_color, 0.8, color, 0.2, 0. ),
			display
			)

	# cv2.imshow( 'display', display )
	# cv2.waitKey(1)
	writer.write( display )

# INTERLUDE
for i in range(30*3):
	frame = numpy.zeros( [720,1280,3],dtype=numpy.uint8 )
	frame = put_centered_text( frame, "BOUNDING BOXES" )
	# cv2.imshow( 'display', frame )
	# cv2.waitKey(1)
	writer.write( frame )

# PASS 2 MASKS
for group_name in subscene_group.groups_names:
	print( 'frame {}'.format(group_name) )

	frame_group      = subscene_group.get_group( group_name )
	foreground_color = frame_group.get_feature( 'foreground_color' ).value
	
	# REPLACE ALL THE BACKGROUND
	mask = numpy.zeros( [720, 1280], dtype=numpy.bool )
	for example_name in frame_group.examples_names[1:]:
		example      = frame_group.get_example( example_name )
		current_mask = example.get_feature( 'mask' ).value
		mask = numpy.logical_or( mask, current_mask )
	mask = numpy.reshape( mask, [720, 1280, 1] )
	mask = numpy.tile( mask, [1, 1, 3] )
	mask = cv2.erode( mask.astype(numpy.uint8), numpy.ones( [3, 3], numpy.uint8) ).astype(numpy.bool)
	display = numpy.where( mask, foreground_color, background_color )

	# Draw the mask of each object
	color_index = 0
	for example_name in frame_group.examples_names[1:]:
		example = frame_group.get_example( example_name )
		bb      = example.get_feature( 'mesh' ).value.get_bounding_box()
		R       = example.get_feature( 'R' ).value
		t       = example.get_feature( 't' ).value
		color = COLORS[ color_index ]
		color_index += 1

		bb = pydataset.pose_utils.project_points( bb, K, dist_coeffs, R, t )
		display = pydataset.cv2_drawing.draw_bounding_box( display, bb, edges_color=color, edges_thickness=1, points_color=color, points_radius=1, points_thickness=2 )

	# cv2.imshow( 'display', display )
	# cv2.waitKey(1)
	writer.write( display )

writer.release()