# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL                  = 'file://D:/Datasets/nema_v2'
MODELS_URL_SEARCH_PATTERN    = 'groups/models/examples/*'
IMAGES_URL_SEARCH_PATTERN    = 'groups/scenes/groups/000000/groups/*/groups/*'
INSTANCES_URL_SEARCH_PATTERN = 'examples/*'
NUMBER_OF_THREADS            = 24

# ##################################################
# ###                 ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL of the NEMA dataset.', default=DATASET_URL )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to agregate statistics.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'NEMA PREPARATION' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:       {}', arguments.dataset_url )
pt.log( '    number_of_threads: {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###         COMPUTE IMAGE-WISE LABELS          ###
# ##################################################

mapper = pydataset.manipulators.ManipulationSequence.create(
	# # Compute segmentation masks from rendered depth maps
	# pydataset.manipulators.image.DepthMapToMaskMapper.create(
	# 		instances_url_search_pattern = INSTANCES_URL_SEARCH_PATTERN,
	# 		      depth_map_feature_name = 'render_foreground_depth',
	# 		           mask_feature_name = 'mask',
	# 		   visible_mask_feature_name = 'visible_mask',
	# 		     visibility_feature_name = 'visibility',
	# 		                        name = 'nema.depth-to-mask'
	# 	),
	# # Compute the mask of all the foreground objects but the charuco board
	# pydataset.manipulators.image.FullMaskCreator.create(
	# 	instances_url_search_pattern = 'examples/*',
	# 	           mask_feature_name = 'visible_mask',
	# 	      full_mask_feature_name = 'full_visible_mask',
	# 	                skip_indexes = [0],
	# 		                    name = 'nema.full-mask'
	# 	),
	# Compute bounding rectangles from segmentation masks
	# pydataset.manipulators.image.MaskToBoundingRectangleMapper.create(
	# 	              mask_feature_name = 'full_visible_mask',
	# 	bounding_rectangle_feature_name = 'full_bounding_rectangle',
	# 	                           name = 'nema.mask-to-bounding-rectangle'
	# 	),
	# PoseNet quaternion labels
	pydataset.manipulators.pose.Matrix2Quaternion.create(
		     input_matrix_feature_name = 'R',
		output_quaternion_feature_name = 'q',
		                          name = 'nema.quaternion'
		),
	# BB8 labels with a single category
	pydataset.manipulators.bb8.MeshToBB8V2.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		               bb8_feature_name = 'bb8_v1',
		                           name = 'nema.bb8-v1'
		),
	url_search_pattern = IMAGES_URL_SEARCH_PATTERN,
	              name = 'nema.image-wize'
	)

pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Image-Wise Labels' ),
	     wait_for_application = True
	)

# # ##################################################
# # ###        COMPUTE INSTANCE-WISE LABELS        ###
# # ##################################################

# mapper = pydataset.manipulators.ManipulationSequence.create(
# 	# Compute bounding rectangles from segmentation masks
# 	pydataset.manipulators.image.MaskToBoundingRectangleMapper.create(
# 		              mask_feature_name = 'visible_mask',
# 		bounding_rectangle_feature_name = 'bounding_rectangle',
# 		                           name = 'nema.mask-to-bounding-rectangle'
# 		),
# 	# Agregate objects sizes
# 	pydataset.manipulators.SizeAgregator.create(
# 		bounding_rectangle_feature_name = 'bounding_rectangle',
# 		          category_feature_name = 'category_name',
# 		          output_directory_path = 'data/nema/sizes',
# 		                filename_format = 'sizes_{}.csv',
# 		                           name = 'nema.size-agregator'
# 		),
# 	# PoseNet quaternion labels
# 	pydataset.manipulators.pose.Matrix2Quaternion.create(
# 		     input_matrix_feature_name = 'R',
# 		output_quaternion_feature_name = 'q',
# 		                          name = 'nema.quaternion'
# 		),
# 	# Bounding boxes for PnP computations
# 	pydataset.manipulators.pose.MeshToBoundingBox.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = '', # bounding boxes are computed in model space
# 		       translation_feature_name = '', # bounding boxes are computed in model space
# 		          camera_k_feature_name = '', # bounding boxes are computed in model space
# 		camera_dist_coeffs_feature_name = '', # bounding boxes are computed in model space
# 		      bounding_box_feature_name = 'bounding_box',
# 		                           name = 'nema.bounding-box'
# 		),
# 	# BB8 V1 labels
# 	pydataset.manipulators.bb8.MeshToBB8V1.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R', # BB8 labels are computed in image space
# 		       translation_feature_name = 't', # BB8 labels are computed in image space
# 		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
# 		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
# 		    category_index_feature_name = 'category_index',
# 		           number_of_categories = 7,
# 		   instances_url_search_pattern = '',
# 		               bb8_feature_name = 'bb8_v1',
# 		                           name = 'nema.bb8-v1'
# 		),
# 	# BB8 V2 labels
# 	pydataset.manipulators.bb8.MeshToBB8V2.create(
# 		         mesh_data_feature_name = 'mesh',
# 		          rotation_feature_name = 'R', # BB8 labels are computed in image space
# 		       translation_feature_name = 't', # BB8 labels are computed in image space
# 		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
# 		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
# 		               bb8_feature_name = 'bb8_v2',
# 		                           name = 'nema.bb8-v2'
# 		),
# 	# Common settings
# 	url_search_pattern = IMAGES_URL_SEARCH_PATTERN+'/'+INSTANCES_URL_SEARCH_PATTERN,
# 	              name = 'nema.instance-wise'
# 	)
# pydataset.manipulators.apply_async(
# 	              manipulator = mapper,
# 	url_search_pattern_prefix = arguments.dataset_url,
# 	        number_of_threads = arguments.number_of_threads,
# 	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Instance-Wise Labels' ),
# 	     wait_for_application = True
# 	)