# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import numpy

# INTERNALS
import pydataset
import pykeras
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/nema_v2'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL where to import NEMA dataset.', default=DATASET_URL )
arguments = parser.parse_args()

# ##################################################
# ###             PREPARE SPLITTING              ###
# ##################################################

# nema_v2/groups/scenes/groups/<scene 0>/groups/<all sequences>/groups/<all frames>
frames_iterator = pydataset.dataset.Iterator( 'file://D:/Datasets/nema_v2/groups/scenes/groups/000000/groups/*/groups/*' )
frames_count    = frames_iterator.count

training_start_index = 0 
training_end_index   = int(float(frames_count)*0.75)
testing_start_index  = training_end_index
testing_end_index    = frames_count

# frames_indexes  = numpy.arange( frames_count, dtype=int )
# numpy.random.shuffle( frames_indexes )
# training_indexes = numpy.sort( frames_indexes[ training_start_index:training_end_index ] )
# testing_indexes  = numpy.sort( frames_indexes[  testing_start_index:testing_end_index  ] )
# numpy.savetxt( 'D:/Datasets/nema_v2/training_indexes.txt', training_indexes, fmt='%06d' )
# numpy.savetxt( 'D:/Datasets/nema_v2/testing_indexes.txt',  testing_indexes,  fmt='%06d' )
training_indexes = numpy.loadtxt( 'D:/Datasets/nema_v2/training_indexes.txt', dtype=int )
testing_indexes  = numpy.loadtxt( 'D:/Datasets/nema_v2/testing_indexes.txt',  dtype=int )

pt = pytools.tasks.console_progress_logger( 'Splitting Informations' )
pt.log( '        number_of_frames: {}', frames_count )
pt.log( '                training: {:06d}>{:06d} ({:06d})', training_start_index, training_end_index, training_indexes.size )
pt.log( '                 testing: {:06d}>{:06d} ({:06d})', testing_start_index,   testing_end_index, testing_indexes.size  )
pt.end_line()
pt.flush()

def filter_in_training( example ):
	global training_indexes
	# file://D:/Datasets/nema_v2/groups/scenes/groups/<scene 0>/groups/<sequence ID>/groups/<frame ID>
	frame_index = int( example.url.split( '/' )[-1] )
	if ( frame_index in training_indexes ):
		return False
	else:
		return True

def filter_in_testing( example ):
	global testing_indexes
	# file://D:/Datasets/nema_v2/groups/scenes/groups/<scene 0>/groups/<sequence ID>/groups/<frame ID>
	frame_index = int( example.url.split( '/' )[-1] )
	if ( frame_index in testing_indexes ):
		return False
	else:
		return True

##################################################
###             EXPORT TF-RECORDS              ###
##################################################

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Training TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	         filter_fn = filter_in_training,
	            logger = pytools.tasks.file_logger( 'logs/nema-export-training.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'training.tfrecords',
	blueprint_filename = 'training.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/000000/groups/*/groups/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'foreground_color',
		'background_color',
		'full_visible_mask',
		'K',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref'
		]
	)
exporter.execute()

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Validation TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	         filter_fn = filter_in_testing,
	            logger = pytools.tasks.file_logger( 'logs/nema-export-validation.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'validation.tfrecords',
	blueprint_filename = 'validation.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/000000/groups/*/groups/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'foreground_color',
		'background_color',
		'full_visible_mask',
		'K',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref'
		]
	)
exporter.execute()
