import pynema
import pytools

importer = pynema.io.DatasetImporter.create(
	                logger = pytools.tasks.file_logger( 'logs/nema_importer.log' ),
	      progress_tracker = pytools.tasks.console_progress_logger( 'Importing NEMA dataset:' ),
	dataset_directory_path = 'F:/Datasets/NEMA EXPORT',
	         dst_group_url = 'file://D:/Datasets/nema_v2',
	                  name = 'nema_importer'
	)
importer.execute()