# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL           = 'file://D:/Datasets/nema_v2'
OUTPUT_DIRECTORY_PATH = 'stats/nema'
NUMBER_OF_THREADS     = 8
URL_SEARCH_PATTERN    = 'groups/scenes/groups/000000/groups/*/groups/*/examples/*'

# ##################################################
# ###                  ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL of the NEMA dataset.', default=DATASET_URL )
parser.add_argument( '--url_search_pattern', type=str, help='URL seatch pattern used to find object instances.', default=URL_SEARCH_PATTERN )
parser.add_argument( '--output_directory_path', type=pytools.path.DirectoryPath, help='Directory where to save the statistics.', default=OUTPUT_DIRECTORY_PATH )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to agregate statistics.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'NEMA STATS' )

# Echo arguments
pt.log( 'Arguments:' )
pt.log( '    dataset_url:           {}', arguments.dataset_url )
pt.log( '    url_search_pattern:    {}', arguments.url_search_pattern )
pt.log( '    output_directory_path: {}', arguments.output_directory_path )
pt.log( '    number_of_threads:     {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###                DIRECTORIES                 ###
# ##################################################

# Output directory tree
locs_dir  = arguments.output_directory_path + pytools.path.DirectoryPath( 'locations' )
povs_dir  = arguments.output_directory_path + pytools.path.DirectoryPath( 'points_of_views' )

# Echo directories
pt.log( 'Directories:' )
pt.log( '    locations:       {}', locs_dir )
pt.log( '    points_of_views: {}', povs_dir )
pt.end_line()
pt.flush()

# ##################################################
# ###             COMPUTE STATISTICS             ###
# ##################################################


sequence = pydataset.manipulators.ManipulationSequence.create(
	# # Point Of Views
	# pydataset.manipulators.pose.PointOfViewAgregator.create(
	# 	    category_index_feature_name = 'category_name',
	# 	   rotation_matrix_feature_name = 'R',
	# 	translation_vector_feature_name = 't',
	# 	          output_directory_path = povs_dir,
	# 	                filename_format = 'points_of_views_{}.csv',
	# 	                           name = 'points_of_views_agregator'
	# 	),
	# # Screen Object Locations
	# pydataset.manipulators.pose.ObjectLocationAgregator.create(
	# 	     camera_matrix_feature_name = 'K',
	# 	    category_index_feature_name = 'category_name',
	# 	   rotation_matrix_feature_name = 'R',
	# 	translation_vector_feature_name = 't',
	# 	          output_directory_path = locs_dir,
	# 	                filename_format = 'locations_{}.csv',
	# 	                           name = 'objects_locations_agregator'
	# 	),
	# # Visibility
	# pydataset.manipulators.pose.VisibilityAgregator.create(
	# 	  category_feature_name = 'category_name',
	# 	visibility_feature_name = 'visibility',
	# 	      mask_feature_name = 'mask',
	# 	        output_filepath = arguments.output_directory_path + pytools.path.FilePath( 'visibility.csv' ),
	# 	                   name = 'visiblity_agregator'
	# 	),
	# Pose Error
	pydataset.manipulators.pose.PoseErrorAgregator.create(
		          category_feature_name = 'category_name',
		             depth_feature_name = 'foreground_depth',
		       depth_scale_feature_name = 'depth_scale',
		      render_depth_feature_name = 'render_foreground_depth',
		render_depth_scale_feature_name = 'render_foreground_depth_scale',
		              mask_feature_name = 'visible_mask',
		              outlier_threshold = 50.0,
		                output_filepath = arguments.output_directory_path + pytools.path.FilePath( 'pose_error.csv' ),
		           output_directorypath = '', #test_dir + pytools.path.DirectoryPath( 'pose_errors' ),
		         output_filename_format = '', #'{}_error_{:07.4f}.png',
		                           name = 'pose_error_agregator'
		),
	# Sequence manipulation settings
	url_search_pattern = arguments.url_search_pattern,
	              name = 'nema_stats'
	)
pydataset.manipulators.apply_async(
	              manipulator = sequence,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'NEMA STATS' ),
	     wait_for_application = True
	)