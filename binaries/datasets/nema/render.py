# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pylinemod
import pynema
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL                  = 'file://D:/Datasets/nema_v2'
MODELS_URL_SEARCH_PATTERN    = 'groups/models/examples/*'
IMAGES_URL_SEARCH_PATTERN    = 'groups/scenes/groups/{}/groups/{}/groups/*'
INSTANCES_URL_SEARCH_PATTERN = 'examples/*'

# ##################################################
# ###                 ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL of the NEMA dataset.', default=DATASET_URL )
parser.add_argument( '--scene', type=str, help='Scene to render', default='0' )
parser.add_argument( '--sequence', type=str, help='Sequence to render', default='*' )
arguments = parser.parse_args()

IMAGES_URL_SEARCH_PATTERN = IMAGES_URL_SEARCH_PATTERN.format(
	'*' if arguments.scene    == '*' else '{:06d}'.format( int(arguments.scene)    ),
	'*' if arguments.sequence == '*' else '{:06d}'.format( int(arguments.sequence) )
	)

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'NEMA RENDER' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    url_search_pattern: {}', IMAGES_URL_SEARCH_PATTERN )
pt.end_line()
pt.flush()

# ##################################################
# ###                   RENDER                   ###
# ##################################################

log_prefix = 'logs/nema_renderer_{}_{}'.format(
	'all' if arguments.scene    == '*' else '{:06d}'.format( int(arguments.scene)    ),
	'all' if arguments.sequence == '*' else '{:06d}'.format( int(arguments.sequence) )
	)

renderer = pylinemod.Renderer.create(
	               progress_tracker = pytools.tasks.console_progress_logger( 'Rendering Depth (all object)' ),
	                         logger = pytools.tasks.file_logger( log_prefix+'_depth.log' ),
	                           size = pydataset.data.ShapeData( 1280, 720 ),
	                    dataset_url = arguments.dataset_url,
	     objects_url_search_pattern = MODELS_URL_SEARCH_PATTERN,
	      images_url_search_pattern = IMAGES_URL_SEARCH_PATTERN,
	   instances_url_search_pattern = INSTANCES_URL_SEARCH_PATTERN,
	              mesh_feature_name = 'mesh',
	     category_name_feature_name = 'category_index',
	     camera_matrix_feature_name = 'K',
	   rotation_matrix_feature_name = 'R',
	translation_vector_feature_name = 't',
	            render_feature_name = 'render_foreground_depth',
	                         shader = 'depth',
	                 is_dry_running = False
	)
renderer.execute()

renderer = pylinemod.Renderer.create(
	               progress_tracker = pytools.tasks.console_progress_logger( 'Rendering Depth (individual object)' ),
	                         logger = pytools.tasks.file_logger( log_prefix+'_depth.log' ),
	                           size = pydataset.data.ShapeData( 1280, 720 ),
	                    dataset_url = arguments.dataset_url,
	     objects_url_search_pattern = MODELS_URL_SEARCH_PATTERN,
	      images_url_search_pattern = IMAGES_URL_SEARCH_PATTERN+'/'+INSTANCES_URL_SEARCH_PATTERN,
	   instances_url_search_pattern = '',
	              mesh_feature_name = 'mesh',
	     category_name_feature_name = 'category_index',
	     camera_matrix_feature_name = 'K',
	   rotation_matrix_feature_name = 'R',
	translation_vector_feature_name = 't',
	            render_feature_name = 'render_foreground_depth',
	                         shader = 'depth',
	                 is_dry_running = False
	)
renderer.execute()