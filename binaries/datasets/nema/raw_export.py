
import pydataset
import cv2
import numpy
import pynema
import pytools

exporter = pynema.raw.RawDatasetExporter.create(
	               logger = pytools.tasks.file_logger( 'logs/nema_dataset_exporter.log' ),
	     progress_tracker = pytools.tasks.console_progress_logger( 'Exporting NEMA dataset:' ),
	          dataset_url = 'file://D:/Datasets/nema',
	models_directory_path = 'F:/NEMA/models/PLY (to display)',
	output_directory_path = 'C:/Datasets/NEMA-EXPORT'
	)
exporter.execute()


# PATH  = 'F:/Datasets/NEMA EXPORT/scenes/000000/subscenes/{:06d}/frames/{:06d}_{:06d}'
# PATHC = 'F:/Datasets/NEMA EXPORT/scenes/000000/subscenes/{:06d}/frames/{:06d}'
# FLAGS = [cv2.IMWRITE_PNG_COMPRESSION, 9]

# sequences_iterator = pydataset.dataset.Iterator( 'file://D:/Datasets/nema_v2/groups/scenes/groups/000000/groups/*' )
# for sequence_index in range( sequences_iterator.count ):
# 	sequence = sequences_iterator.at( sequence_index )
# 	for frame_index in range( sequence.number_of_groups ):
# 		print( '{:03d}/{:03d}-{:03d}/{:03d}'.format(sequence_index+1, sequences_iterator.count, frame_index+1, sequence.number_of_groups ))
# 		frame = sequence.get_group( frame_index )
# 		full_mask = numpy.zeros( [720,1280], dtype=bool )
# 		for object_index in range( frame.number_of_examples ):
# 			# print( '{:03d}/{:03d}-{:03d}/{:03d}-{:1d}/{:1d}'.format(sequence_index+1, sequences_iterator.count, frame_index+1, sequence.number_of_groups, object_index+1, frame.number_of_examples ))
# 			obj = frame.get_example( '{:06d}'.format(object_index) )
# 			mask         = obj.get_feature( 'mask' ).value
# 			visible_mask = obj.get_feature( 'visible_mask' ).value
# 			# cv2.imwrite( PATH.format(sequence_index, frame_index, object_index)+'_mask.png',         mask.astype(numpy.uint8)*255,         FLAGS )
# 			# cv2.imwrite( PATH.format(sequence_index, frame_index, object_index)+'_mask.png', visible_mask.astype(numpy.uint8)*255, FLAGS )
# 			if ( object_index > 0 ):
# 				full_mask = numpy.logical_or( full_mask, visible_mask )
# 		cv2.imwrite( PATHC.format(sequence_index, frame_index)+'_mask.png', full_mask.astype(numpy.uint8)*255, FLAGS )