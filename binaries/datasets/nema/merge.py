# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import numpy

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/nema_v2'
REFERENCE_URL_FORMAT = 'groups/models/examples/{:06d}/features/{}'
IMAGES_URL_SEARCH_PATTERN = 'groups/scenes/groups/000000/groups/*/groups/*'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL where to import NEMA dataset.', default=DATASET_URL )
arguments = parser.parse_args()

# ##################################################
# ###                 LOAD MESHS                 ###
# ##################################################

meshs = []
indexes = [1, 3, 4, 6]
colors = [
	[ 32, 32, 32,255],
	[192,192,192,255],
	[128,128,128,255],
	[192,192,192,255],
	]
	
for i in range(4):
	index = indexes[ i ]
	mesh = pydataset.render.MeshData.load_ply( 'F:/Datasets/NEMA EXPORT/models/ply/{:06d}.ply'.format(index) )
	mesh.set_color( pydataset.data.ColorData( *colors[i] ) )
	meshs.append( mesh )

# ##################################################
# ###                 MERGE MESHS                ###
# ##################################################

merge = pydataset.render.MeshData.merge( meshs, name='merge' )
merge.save_ply( 'F:/Datasets/NEMA EXPORT/models/ply/merge.ply' )

# ##################################################
# ###         ADD MERGED MESH TO DATASET         ###
# ##################################################

group   = pydataset.dataset.get( 'file://D:/Datasets/nema_v2/groups/models' )
example = group.get_or_create_example( '000007' )
example.create_or_update_feature(
	feature_data_type = pydataset.dataset.TextFeatureData,
	feature_name      = 'category_name',
	value             = 'merge'
	)
example.create_or_update_feature(
	feature_data_type = pydataset.dataset.SerializableFeatureData,
	feature_name      = 'mesh',
	value             = merge
	)
example.create_or_update_feature(
	feature_data_type = pydataset.dataset.NDArrayFeatureData,
	feature_name      = 'bounding_box',
	value             = merge.get_bounding_box()
	)

# ##################################################
# ###    UPDATE ALL THE FRAMES IN THE DATASET    ###
# ##################################################

class MergeMapperSettings ( pydataset.manipulators.MapperSettings ):
	pass

class MergeMapper ( pydataset.manipulators.Mapper ):

	@classmethod
	def create_settings( cls, **kwargs ):
		return MergeMapperSettings( **kwargs )

	def map ( self, example ):
		child = example.get_example( '000001' )
		R = child.get_feature( 'R' ).value
		t = child.get_feature( 't' ).value

		if not example.contains_feature( 'category_name' ):
			example.create_feature(
				feature_data_type = pydataset.dataset.FeatureRefData,
				feature_name      = 'category_name',
				value             = REFERENCE_URL_FORMAT.format( 7, 'category_name' )
				)
		if not example.contains_feature( 'mesh' ):
			example.create_or_update_feature(
				feature_data_type = pydataset.dataset.FeatureRefData,
				feature_name      = 'mesh',
				value             = REFERENCE_URL_FORMAT.format( 7, 'mesh' )
				)
		if not example.contains_feature( 'bounding_box' ):
			example.create_or_update_feature(
				feature_data_type = pydataset.dataset.FeatureRefData,
				feature_name      = 'bounding_box',
				value             = REFERENCE_URL_FORMAT.format( 7, 'bounding_box' )
				)
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			feature_name      = 'R',
			value             =  R
			)
		example.create_or_update_feature(
			feature_data_type = pydataset.dataset.NDArrayFeatureData,
			feature_name      = 't',
			value             =  t
			)
		return example

mapper = MergeMapper.create(
	url_search_pattern = IMAGES_URL_SEARCH_PATTERN,
	name='merge_mapper'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = 16,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Merging labels at image level' ),
	     wait_for_application = True
	)