# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pykeras
import pylinemod
import pytools

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
# Locations
parser.add_argument( 'dataset_path',                 type=str, help='Path of the directory containg the raw LINEMOD dataset' )
parser.add_argument( 'dataset_url',                  type=str, help='URL of the LINEMOD dataset to create' )
parser.add_argument( '--backgrounds_directory_path', type=str, default='resources/sun2012', help='Directory containing background images' )
# Other arguments
parser.add_argument( '--number_of_threads', type=int, default=32, help='Number of threads used to map the examples.')
arguments = parser.parse_args()

# Echo arguments
print( 'LINEMOD SETUP:' )
print( '    Arguments:' )
print( '    -------------------------------------------------------------------------------' )
print( '    dataset_path                   :', arguments.dataset_path                        )
print( '    dataset_url                    :', arguments.dataset_url                         )
print( '    backgrounds_directory_path     :', arguments.backgrounds_directory_path          )
print( '    -------------------------------------------------------------------------------' )
print( '    number_of_threads              :', arguments.number_of_threads                   )
print( '    -------------------------------------------------------------------------------' )
print( '' )

# ##################################################
# ###                   IMPORT                   ###
# ##################################################

importer = pylinemod.Importer.create(
	               progress_tracker = pytools.tasks.console_progress_logger( 'Importing LINEMOD dataset' ),
	                         logger = pytools.tasks.file_logger( 'logs/linemod-setup.importer.log' ),
	                  dst_group_url = arguments.dataset_url,
	     raw_dataset_directory_path = arguments.dataset_path,
	             objects_group_name = 'objects',
	     category_name_feature_name = 'category_name',
	    category_index_feature_name = 'category_index',
	  one_hot_category_feature_name = 'one_hot_category',
	    category_color_feature_name = '',#'category_color',
	          ply_mesh_feature_name = 'mesh',
	          xyz_mesh_feature_name = '',#'xyz_mesh',
	      old_ply_mesh_feature_name = '',#'old_ply_mesh',
	   bounding_radius_feature_name = '',#'bounding_radius',
	    mesh_transform_feature_name = '',#'mesh_transform',
		          scenes_group_name = 'groups/raw/groups/all',
	     camera_matrix_feature_name = 'K',
	             image_feature_name = 'image',
	             depth_feature_name = '',#'depth',
	   rotation_matrix_feature_name = 'R',
	translation_vector_feature_name = 't'
	)
importer.execute()

##################################################
###                RENDER MASKS                ###
##################################################

renderer = pylinemod.Renderer.create(
	               progress_tracker = pytools.tasks.console_progress_logger( 'Rendering Masks' ),
	                         logger = pytools.tasks.file_logger( 'logs/linemode-setup.mask-renderer.log' ),
	                    dataset_url = arguments.dataset_url,
	     objects_url_search_pattern = 'groups/objects/examples/*',
	      images_url_search_pattern = 'groups/raw/groups/all/groups/*/groups/*',
	   instances_url_search_pattern = 'examples/*',
	              mesh_feature_name = 'mesh',
	     category_name_feature_name = 'category_name',
	     camera_matrix_feature_name = 'K',
	   rotation_matrix_feature_name = 'R',
	translation_vector_feature_name = 't',
	            render_feature_name = 'mask',
	                         shader = 'mask',
	                 # is_dry_running = True
	)
renderer.execute()

##################################################
###         COMPUTE SEGMENTATIONS MAPS         ###
##################################################

mapper = pydataset.manipulators.segmentation.MaskToSegmentationMap.create(
	     instances_url_search_pattern = 'examples/*',
	                mask_feature_name = arguments.mask_feature_name,
	      category_index_feature_name = arguments.category_index_feature_name,
	                number_of_classes = len(pylinemod.constants.CATEGORIES),
	             dedicated_background = True,
	        segmentation_feature_name = arguments.segmentation_feature_name,
	               url_search_pattern = 'groups/raw/groups/all/groups/*/groups/*',
	                             name = 'linemod-setup.segmentation'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Segmentations From Masks' ),
	     wait_for_application = True
	)

##################################################
###            COMPUTE QUATERNIONS             ###
##################################################

mapper = pydataset.manipulators.pose.Matrix2Quaternion.create(
	     input_matrix_feature_name = arguments.rotation_matrix_feature_name,
	output_quaternion_feature_name = arguments.quaternion_feature_name,
	            url_search_pattern = 'groups/raw/groups/all/groups/*/groups/*/examples/*',
	                          name = 'linemod-setup.quaternion'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Quaternions Angles' ),
	     wait_for_application = True
	)

##################################################
###        COMPUTE BOUNDING RECTANGLES         ###
##################################################

mapper = pydataset.manipulators.pose.MeshToBoundingRectangle.create(
	         mesh_data_feature_name = arguments.ply_mesh_feature_name,
	          rotation_feature_name = arguments.rotation_matrix_feature_name,
	       translation_feature_name = arguments.translation_vector_feature_name,
	          camera_k_feature_name = arguments.camera_matrix_feature_name,
	camera_dist_coeffs_feature_name = '',
	bounding_rectangle_feature_name = arguments.bounding_rectangle_feature_name,
	             url_search_pattern = 'groups/raw/groups/all/groups/*/groups/*/examples/*',
	                           name = 'linemod-setup.bounding-rectangle'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Bounding-Rectangle' ),
	     wait_for_application = True
	)

##################################################
###           COMPUTE BOUNDING BOXES           ###
##################################################

mapper = pydataset.manipulators.pose.MeshToBoundingBox.create(
	         mesh_data_feature_name = arguments.ply_mesh_feature_name,
	          rotation_feature_name = arguments.rotation_matrix_feature_name,
	       translation_feature_name = arguments.translation_vector_feature_name,
	          camera_k_feature_name = '',#arguments.camera_matrix_feature_name,
	camera_dist_coeffs_feature_name = '',
	      bounding_box_feature_name = arguments.bounding_box_feature_name,
	             url_search_pattern = 'groups/raw/groups/all/groups/*/groups/*/examples/*',
	                           name = 'linemod-setup.bounding-box'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Bounding-Box' ),
	     wait_for_application = True
	)

##################################################
###           COMPUTE BB8 V1 LABELS            ###
##################################################

mapper = pydataset.manipulators.bb8.MeshToBB8V1.create(
	         mesh_data_feature_name = arguments.ply_mesh_feature_name,
	          rotation_feature_name = arguments.rotation_matrix_feature_name,
	       translation_feature_name = arguments.translation_vector_feature_name,
	          camera_k_feature_name = arguments.camera_matrix_feature_name,
	camera_dist_coeffs_feature_name = '',
	   instances_url_search_pattern = 'examples/*',
		       number_of_categories = len(pylinemod.constants.CATEGORIES),
		category_index_feature_name = arguments.category_index_feature_name,
		           bb8_feature_name = arguments.bb8_v1_feature_name,
	             url_search_pattern = 'groups/*/groups/*',
	                           name = 'linemod-setup.bb8-v1'
	)  
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Creating BB8 V1 Labels' ),
	     wait_for_application = True
	)

##################################################
###           COMPUTE BB8 V2 LABELS            ###
##################################################

# Create BB8 V2 labels
mapper = pydataset.manipulators.bb8.MeshToBB8V2.create(
	         mesh_data_feature_name = arguments.ply_mesh_feature_name,
	          rotation_feature_name = arguments.rotation_matrix_feature_name,
	       translation_feature_name = arguments.translation_vector_feature_name,
	          camera_k_feature_name = arguments.camera_matrix_feature_name,
	camera_dist_coeffs_feature_name = '',
		           bb8_feature_name = arguments.bb8_v2_feature_name,
	             url_search_pattern = 'groups/*/groups/*/examples/*',
	                           name = 'linemod-setup.bb8-v2'
	)  
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Creating BB8 V2 Labels' ),
	     wait_for_application = True
	)

##################################################
###           COMPUTE YOLO V1 LABELS           ###
##################################################

mapper = pydataset.manipulators.yolo_v1.Labeler.create(
	                number_of_cells = 1,
	              number_of_classes = len(pylinemod.constants.CATEGORIES),
	   instances_url_search_pattern = 'examples/*',
	             image_feature_name = 'image',
	bounding_rectangle_feature_name = 'bounding_rectangle',
	  one_hot_category_feature_name = 'one_hot_category',
	              yolo_feature_name = arguments.yolo_v1_feature_name,
	                      normalize = True,
	             url_search_pattern = 'groups/*/groups/*',
	                           name = 'linemod-setup.yolo-v1'
	)  
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Creating YOLO V1 labels' ),
	     wait_for_application = True
	)

##################################################
###          COMPUTE YOLO V2 ANCHORS           ###
##################################################

anchors_agregator = pydataset.manipulators.yolo_v2.AnchorsAgregator.create(
	              number_of_anchors = 5,
	             image_feature_name = 'image',
	bounding_rectangle_feature_name = 'bounding_rectangle',
	           anchors_feature_name = 'yolo_v2_anchors',
	               anchors_filepath = 'D:\\Datasets\\linemod\\yolo_v2_anchors.txt',
	             url_search_pattern = 'groups/*/groups/*/examples/*',
	                           name = 'linemod-setup.bb8-creator-v2'
	)
pydataset.manipulators.apply_async(
	              manipulator = anchors_agregator,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing YOLO V2 Anchors' ),
	     wait_for_application = True
	)

##################################################
###           COMPUTE YOLO V2 LABELS           ###
##################################################

mapper = pydataset.manipulators.yolo_v2.Labeler.create(
	                number_of_cells = 1,
	              number_of_classes = len(pylinemod.constants.CATEGORIES),
	   instances_url_search_pattern = 'examples/*',
	           anchors_feature_name = 'yolo_v2_anchors',
	             image_feature_name = 'image',
	bounding_rectangle_feature_name = 'bounding_rectangle',
	  one_hot_category_feature_name = 'one_hot_category',
	              yolo_feature_name = arguments.yolo_v2_feature_name,
	             url_search_pattern = 'groups/*/groups/*',
	                           name = 'linemod-setup.bb8-creator-v2'
	)  
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Creating YOLO V2 labels' ),
	     wait_for_application = True
	)

#################################################
##                SPLIT IMAGES                ###
#################################################

# Compute splitting informations
agregator = pydataset.manipulators.classification.SplitterDataAgregator.create(
	category_name_feature_name = arguments.category_name_feature_name,
	        url_search_pattern = 'groups/*/groups/*/examples/*',
	                      name = 'linemod-setup.splitter-data'
	)
pydataset.manipulators.apply_async(
	              manipulator = agregator,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Splitting Informations' ),
	     wait_for_application = True
	)

# Split
splitter = pydataset.manipulators.classification.SplitterManipulator.create(
	category_name_feature_name = arguments.category_name_feature_name,
	                      data = agregator.result,
	              splits_names = ['training', 'testing'],
	        splits_proportions = [0.75, 0.25],
	        url_search_pattern = 'groups/*/groups/*/examples/*',
	             dst_group_url = 'groups/raw',
	                      name = 'linemod_setup.splitter'
	)
pydataset.manipulators.apply_async(
	              manipulator = splitter,
	url_search_pattern_prefix = arguments.dataset_url + '/groups/raw/groups/all',
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Splitting Examples' ),
	     wait_for_application = True
	)

#################################################
##             EXPORT TF-RECORDS              ###
#################################################

# categories_names  = [ category['name'] for category in pylinemod.constants.categories ]
categories_names = [ '*' ]

for category_name in categories_names:

	display_name = 'all' if category_name == '*' else category_name

	# Export training tf-records
	pt = pytools.tasks.console_progress_logger( 'Exporting Training TF-Records For Category "{}":'.format(display_name) )
	exporter = pykeras.records.RecordsExporter.create(
		            logger = pytools.tasks.file_logger( 'logs/linemode-setup.training-tfrecords-{}.log'.format(display_name) ),
		  progress_tracker = pt,
		         directory = '',
		  records_filename = 'training-{}.tfrecords'.format(display_name),
		blueprint_filename = 'training-{}.json'.format(display_name),
		url_search_pattern = arguments.dataset_url+'/groups/raw/groups/training/groups/{}/groups/*/examples/*'.format(category_name),
		  compression_type = 'GZIP',
		 compression_level = 8,
		           shuffle = True,
		    features_names = [
			arguments.camera_matrix_feature_name,
			# arguments.image_mean_feature_name,
			# arguments.category_name_feature_name,
			arguments.category_index_feature_name,
			arguments.one_hot_category_feature_name,
			# arguments.category_color_feature_name,
			# arguments.ply_mesh_feature_name,
			# arguments.xyz_mesh_feature_name,
			# arguments.old_ply_mesh_feature_name,
			# arguments.bounding_radius_feature_name,
			# arguments.mesh_transform_feature_name,
			arguments.image_feature_name,
			# arguments.depth_feature_name,
			arguments.rotation_matrix_feature_name,
			arguments.translation_vector_feature_name,
			arguments.quaternion_feature_name,
			# arguments.render_image_feature_name,
			# arguments.render_depth_feature_name,
			arguments.mask_feature_name,
			arguments.segmentation_feature_name,
			arguments.bounding_rectangle_feature_name,
			arguments.bounding_box_feature_name,
			arguments.bb8_v1_feature_name,
			arguments.bb8_v2_feature_name,
			arguments.yolo_v1_feature_name
			]
		)
	exporter.execute()

	# Export testing tf-records
	pt = pytools.tasks.console_progress_logger( 'Exporting Testing TF-Records For Category "{}":'.format(display_name) )
	exporter = pykeras.records.RecordsExporter.create(
		            logger = pytools.tasks.file_logger( 'logs/linemode-setup.testing-tfrecords-{}.log'.format(display_name) ),
		  progress_tracker = pt,
		         directory = '',
		  records_filename = 'testing-{}.tfrecords'.format( display_name ),
		blueprint_filename = 'testing-{}.json'.format( display_name ),
		url_search_pattern = arguments.dataset_url+'/groups/raw/groups/testing/groups/{}/groups/*/examples/*'.format(category_name),
		  compression_type = 'GZIP',
		 compression_level = 8,
		           shuffle = True,
		    features_names = [
			arguments.camera_matrix_feature_name,
			# arguments.image_mean_feature_name,
			# arguments.category_name_feature_name,
			arguments.category_index_feature_name,
			arguments.one_hot_category_feature_name,
			# arguments.category_color_feature_name,
			# arguments.ply_mesh_feature_name,
			# arguments.xyz_mesh_feature_name,
			# arguments.old_ply_mesh_feature_name,
			# arguments.bounding_radius_feature_name,
			# arguments.mesh_transform_feature_name,
			arguments.image_feature_name,
			# arguments.depth_feature_name,
			arguments.rotation_matrix_feature_name,
			arguments.translation_vector_feature_name,
			arguments.quaternion_feature_name,
			# arguments.render_image_feature_name,
			# arguments.render_depth_feature_name,
			arguments.mask_feature_name,
			arguments.segmentation_feature_name,
			arguments.bounding_rectangle_feature_name,
			arguments.bounding_box_feature_name,
			arguments.bb8_v1_feature_name,
			arguments.bb8_v2_feature_name,
			arguments.yolo_v1_feature_name
			]
		)
	exporter.execute()

# for category_name in categories_names
