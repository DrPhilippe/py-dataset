import cv2
import numpy
import sys
import tensorflow

blueprint = {
	'image': tensorflow.io.FixedLenFeature( [], tensorflow.string ),
	'digit': tensorflow.io.FixedLenFeature( [], tensorflow.int64 ),
	'one_hot_digit': tensorflow.io.FixedLenFeature( [], tensorflow.string )
}

def parser ( example ):
	return tensorflow.io.parse_single_example( example, blueprint )

dataset = tensorflow.data.TFRecordDataset( sys.argv[1], compression_type='ZLIB', buffer_size=10, num_parallel_reads=4 )
dataset = dataset.map( parser )

for example in dataset:

	image = example[ 'image' ].numpy()
	image = numpy.frombuffer( image, dtype=numpy.byte )
	image = cv2.imdecode( image, cv2.IMREAD_COLOR )
	image = cv2.cvtColor( image, cv2.COLOR_RGB2BGR )

	digit = example[ 'digit' ].numpy()

	one_hot_digit = example[ 'one_hot_digit' ].numpy()
	one_hot_digit = numpy.frombuffer( one_hot_digit, dtype=numpy.int32 )


	image = cv2.resize( image, (512, 512) )
	image = cv2.putText( image, 'digit: {}'.format(digit), (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,255,255), 2, cv2.LINE_AA )
	image = cv2.putText( image, 'onehot: {}'.format(one_hot_digit), (20,40), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255,255,255), 2, cv2.LINE_AA )
	cv2.imshow( 'image', image )
	if cv2.waitKey(500) == ord('q'):
		break