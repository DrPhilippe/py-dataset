# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset.manipulators.classification
import pykeras.records
import pymnist
import pytools.tasks

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

parser = argparse.ArgumentParser()
parser.add_argument( 'dataset_path',                 type=str, help='Path of the directory containg the raw MNIST dataset' )
parser.add_argument( 'dataset_url',                  type=str, help='URL where to import the mnist dataset' )
parser.add_argument( '--training_images_filename',   type=str, default='train-images.idx3-ubyte', help='Nane of the binary file containing the training images'      )
parser.add_argument( '--training_labels_filename',   type=str, default='train-labels.idx1-ubyte', help='Nane of the binary file containing the training labels'      )
parser.add_argument( '--testing_images_filename',    type=str, default='t10k-images.idx3-ubyte',  help='Nane of the binary file containing the testing images'       )
parser.add_argument( '--testing_labels_filename',    type=str, default='t10k-labels.idx1-ubyte',  help='Nane of the binary file containing the testing labels'       )
parser.add_argument( '--training_group_name',        type=str, default='training',                help='Name given to the group where training examples are created' )
parser.add_argument( '--testing_group_name',         type=str, default='testing',                 help='Name given to the group where testing examples are created'  )
parser.add_argument( '--image_feature_name',         type=str, default='image',                   help='Name given to the image feature'                             )
parser.add_argument( '--digit_feature_name',         type=str, default='digit',                   help='Name given to the digit feature'                             )
parser.add_argument( '--one_hot_digit_feature_name', type=str, default='one_hot_digit',           help='Name given to the one hot digit feature'                     )
parser.add_argument( '--number_of_threads', type=int, default=8, help='Number of threads used to map the examples.')
arguments = parser.parse_args()

print( 'LINEMOD SETUP:' )
print( '    Arguments:' )
print( '    ---------------------------------------------------------------------------' )
print( '    dataset_path              :', arguments.dataset_path                    )
print( '    dataset_url               :', arguments.dataset_url                     )
print( '    training_images_filename  :', arguments.training_images_filename        )
print( '    training_labels_filename  :', arguments.training_labels_filename        )
print( '    testing_images_filename   :', arguments.testing_images_filename         )
print( '    testing_labels_filename   :', arguments.testing_labels_filename         )
print( '    training_group_name       :', arguments.training_group_name             )
print( '    testing_group_name        :', arguments.testing_group_name              )
print( '    image_feature_name        :', arguments.image_feature_name              )
print( '    digit_feature_name        :', arguments.digit_feature_name              )
print( '    one_hot_digit_feature_name:', arguments.one_hot_digit_feature_name      )
print( '    number_of_threads         :', arguments.number_of_threads               )
print( '' )

# ##################################################
# ###                   IMPORT                   ###
# ##################################################

# Import the dataset
importer = pymnist.Importer.create(
	             dst_group_url = arguments.dataset_url + '/groups/raw',
	raw_dataset_directory_path = arguments.dataset_path,
	  training_images_filename = arguments.training_images_filename,
	  training_labels_filename = arguments.training_labels_filename,
	   testing_images_filename = arguments.testing_images_filename,
	   testing_labels_filename = arguments.testing_labels_filename,
	       training_group_name = arguments.training_group_name,
	        testing_group_name = arguments.testing_group_name,
	        image_feature_name = arguments.image_feature_name,
	        digit_feature_name = arguments.digit_feature_name,
	                    logger = pytools.tasks.file_logger( 'logs/mnist_setup.importer.log' ),
	          progress_tracker = pytools.tasks.console_progress_logger( 'Importing MNIST dataset' )
	)
importer.execute()

# ##################################################
# ###           COMPUTE ONE HOT LABELS           ###
# ##################################################

# Create one-hot labels
on_hot_mapper = pydataset.manipulators.classification.OneHotMapper.create(
	 sparse_category_feature_name = arguments.digit_feature_name,
	one_hot_category_feature_name = arguments.one_hot_digit_feature_name,
	         number_of_categories = 10,
	           url_search_pattern = 'groups/raw/groups/*/groups/*', # digit feature is common to all examples of these groups
	                         name = 'one-hot-mapper'
	)
pydataset.manipulators.apply_async(
	              manipulator = on_hot_mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing One-Hot category labels' ),
	     wait_for_application = True
	)

# ##################################################
# ###             EXPORT TF-RECORDS              ###
# ##################################################

# Export training tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting training tf-records:' )
exporter = pykeras.records.RecordsExporter.create(
		        logger = pytools.tasks.file_logger( 'logs/mnist_setup.tf-records-exporter.training.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = '{}.tfrecords',
	blueprint_filename = '{}.json',
	url_search_pattern = arguments.dataset_url+'/groups/raw/groups/training/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 9,
	           shuffle = True,
	    features_names = [
		arguments.image_feature_name,
		arguments.digit_feature_name,
		arguments.one_hot_digit_feature_name
		]
	)
exporter.execute()

# Export testing tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting testing tf-records:' )
exporter = pykeras.records.RecordsExporter.create(
		        logger = pytools.tasks.file_logger( 'logs/mnist_setup.tf-records-exporter.testing.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = '{}.tfrecords',
	blueprint_filename = '{}.json',
	url_search_pattern = arguments.dataset_url+'/groups/raw/groups/testing/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 9,
	           shuffle = True,
	    features_names = [
		arguments.image_feature_name,
		arguments.digit_feature_name,
		arguments.one_hot_digit_feature_name
		]
	)
exporter.execute()