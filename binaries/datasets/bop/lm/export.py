# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pykeras
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/bop_lm'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL where to import BOP-LM dataset.', default=DATASET_URL )
arguments = parser.parse_args()

##################################################
###        EXPORT IMAGE-WISE TF-RECORDS        ###
##################################################

# Export training tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Image-Wise Training TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	            logger = pytools.tasks.file_logger( 'logs/bop_ycb.imagewise_training_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'training_imagewise.tfrecords',
	blueprint_filename = 'training_imagewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/train/groups/*/groups/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		'yolo_v1',
		'bb8_v1',
		'bb8_v1_ref',
		]
	)
exporter.execute()

# Export testing tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Image-Wise Testing TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	            logger = pytools.tasks.file_logger( 'logs/bop_ycb.imagewise_testing_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'testing_imagewise.tfrecords',
	blueprint_filename = 'testing_imagewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/val/groups/*/groups/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = False,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		'yolo_v1',
		'bb8_v1',
		'bb8_v1_ref',
		]
	)
exporter.execute()

# ##################################################
# ###      EXPORT INSTANCE-WISE TF-RECORDS       ###
# ##################################################

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Training TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	            logger = pytools.tasks.file_logger( 'logs/bop_lm_train_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'training_instancewise.tfrecords',
	blueprint_filename = 'training_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/train/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		# 'mesh',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref',
		'bb8_v2',
		'bb8_v2_ref'
		]
	)
exporter.execute()

# Export tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Validation TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	            logger = pytools.tasks.file_logger( 'logs/bop_lm_val_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'validation_instancewise.tfrecords',
	blueprint_filename = 'validation_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/val/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		# 'mesh',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref',
		'bb8_v2',
		'bb8_v2_ref'
		]
	)
exporter.execute()
