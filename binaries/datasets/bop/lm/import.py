# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pybop
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_DIR = 'D:/LM'
DATASET_URL = 'file://D:/Datasets/bop_lm'
MODE = 'create'
CATEGORY_NAMES = [
	'ape',
	'benchviseblue',
	'bowl',
	'cam',
	'can',
	'cat',
	'cup',
	'driller',
	'duck',
	'eggbox',
	'glue',
	'holepuncher',
	'iron',
	'lamp',
	'phone'
	]
NUMBER_OF_THREADS = 24

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_path', type=pytools.path.DirectoryPath, help='Path of the directory containg the BOP-LM dataset to import.', default=DATASET_DIR )
parser.add_argument( '--dataset_url',  type=str, help='URL where to create the imported BOP-LM dataset.', default=DATASET_URL )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to import the dataset.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP-LM IMPORT' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_path:       {}', arguments.dataset_path )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    number_of_threads:  {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###             IMPORT THE DATASET             ###
# ##################################################

importer = pybop.DatasetImporter.create(
	                 dataset_directory_path = arguments.dataset_path,
	                          dst_group_url = arguments.dataset_url,
	                                   mode = MODE,
	                     cameras_group_name = 'cameras',
	                     objects_group_name = 'objects',
	                      scenes_group_name = 'scenes',
	            category_index_feature_name = 'category_index',
	          one_hot_category_feature_name = 'one_hot_category',
	                      mesh_feature_name = 'mesh',
	             mesh_diameter_feature_name = 'mesh_diameter',
	                     ignore_fine_models = True,
	                         category_names = CATEGORY_NAMES,
	             category_name_feature_name = 'category_name',
	             camera_matrix_feature_name = 'K',
	               depth_scale_feature_name = '',#'depth_scale',
	                image_size_feature_name = '',#'size',
	                     image_feature_name = 'image',
	                     depth_feature_name = '',#'depth',
	                      mask_feature_name = '',#'mask',
	              visible_mask_feature_name = 'visible_mask',
	           rotation_matrix_feature_name = 'R',
	        translation_vector_feature_name = 't',
	        bounding_rectangle_feature_name = '',#'bounding_rectangle',
	visible_bounding_rectangle_feature_name = 'bounding_rectangle',#'visible_bounding_rectangle',
	          number_of_pixels_feature_name = '',#'number_of_pixels',
	    number_of_valid_pixels_feature_name = '',#'number_of_valid_pixels',
	  number_of_visible_pixels_feature_name = '',#'number_of_visible_pixels',
	                visibility_feature_name = 'visibility',
	                                   name = 'bop_lm_importer',
	                       progress_tracker = pt,
	                                 logger = pytools.tasks.file_logger( 'logs/bop_lm_importer.log' )
	)
importer.execute()