
import cv2
import numpy
import pydataset

# for i in range( 1213 ):
i = 66
group = pydataset.dataset.get( 'file://D:/Datasets/bop_lm/groups/scenes/groups/test/groups/000009/groups/{:06d}'.format(i) )
image = group.get_feature( 'image' ).value
K     = group.get_feature( 'K'    ).value
dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )
H, W = image.shape[0], image.shape[1]

lm_mask = image.copy()
lm_lcs = image.copy()

count = len(group.examples_names)

COLORS = [
	(128, 128, 255),
	(128, 255, 255),
	(128, 255, 128),
	(255, 255, 128),
	(255, 128,   0),
	(192, 128, 255),
	(225,   0,   0),
	(  0, 255,   0),
	(0,     0, 255)
	]

for j in range(count):

	example = group.get_example( j, name_format='{}{:06d}', prefix='' )
	mesh    = example.get_feature( 'mesh' ).value
	R       = example.get_feature( 'R'    ).value
	t       = example.get_feature( 't'    ).value
	mask    = example.get_feature( 'visible_mask' ).value
	r       = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )

	# c = int( float(j) / float(count) * float(22) )
	# color = pydataset.data.ColorData.PREDEFINED_22[ c ]
	color = COLORS[ j ]

	# BB
	# bb    = mesh.get_bounding_box()
	# bb, _ = cv2.projectPoints( bb, r, t, K, dist_coeffs )
	# bb    = numpy.reshape( bb, [-1,2] )
	# image = pydataset.cv2_drawing.draw_bounding_box( image, bb,
	# 	edges_color      = color.to_tuple(False),
	# 	edges_thickness  = 1,
	# 	points_color     = color.to_tuple(False),
	# 	points_radius    = 1,
	# 	points_thickness = 1
	# 	)

	# MESH CLOUD
	# points, _ = cv2.projectPoints( mesh.points, r, t, K, dist_coeffs )
	# points    = numpy.reshape( points, [-1,2] )
	# image     = pydataset.cv2_drawing.draw_points( image, points, color=color.to_tuple(False), radius=1, thickness=1 )

	# MASK
	mask   = numpy.reshape( mask, [H, W, 1] )
	mask   = numpy.tile( mask, [1, 1, 3] )
	# colors = numpy.reshape( color.to_array(False), [1, 1, 3] )
	colors = numpy.reshape( numpy.asarray(color, dtype=numpy.uint8), [1, 1, 3] )
	colors = numpy.tile( colors, [H, W, 1] )
	lm_mask = numpy.where( mask, colors, lm_mask )

	# LCS
	lm_lcs = cv2.drawFrameAxes( lm_lcs, K, dist_coeffs, r, t, 0.1, 2 )


cv2.imshow( 'lm_mask', lm_mask )
cv2.imshow( 'lm_lcs', lm_lcs )

if cv2.waitKey(0) == ord('q'):
	exit()

cv2.imwrite( 'lm.png', image )
cv2.imwrite( 'lm_mask.png', lm_mask )
cv2.imwrite( 'lm_lcs.png', lm_lcs )
