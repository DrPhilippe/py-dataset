# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL           = 'file://D:/Datasets/bop_lm'
OUTPUT_DIRECTORY_PATH = 'stats/bop_lm'
NUMBER_OF_THREADS     = 24
URL_SEARCH_PATTERN    = 'groups/scenes/groups/test/groups/*/groups/*/examples/000000'

# ##################################################
# ###                  ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL of the LINEMOD dataset.', default=DATASET_URL )
parser.add_argument( '--url_search_pattern', type=str, help='URL seatch pattern used to find object instances.', default=URL_SEARCH_PATTERN )
parser.add_argument( '--output_directory_path', type=pytools.path.DirectoryPath, help='Directory where to save the statistics.', default=OUTPUT_DIRECTORY_PATH )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to agregate statistics.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP-LM STATS' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:           {}', arguments.dataset_url )
pt.log( '    url_search_pattern:    {}', arguments.url_search_pattern )
pt.log( '    output_directory_path: {}', arguments.output_directory_path )
pt.log( '    number_of_threads:     {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###                DIRECTORIES                 ###
# ##################################################

# Output directory tree
locs_dir = arguments.output_directory_path + pytools.path.DirectoryPath( 'locations' )
povs_dir = arguments.output_directory_path + pytools.path.DirectoryPath( 'points_of_views' )

# Echo arguments
pt.log( 'Directories:' )
pt.log( '    locations:       {}', locs_dir )
pt.log( '    points_of_views: {}', povs_dir )
pt.end_line()
pt.flush()

# ##################################################
# ###             COMPUTE STATISTICS             ###
# ##################################################

sequence = pydataset.manipulators.ManipulationSequence.create(
	# Agregate Test Point Of Views
	pydataset.manipulators.pose.PointOfViewAgregator.create(
		    category_index_feature_name = 'category_index',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = povs_dir,
		                filename_format = 'points_of_views_{}.csv',
		                           name = 'points_of_views_agregator'
		),
	# On-Screen Object Locations
	pydataset.manipulators.pose.ObjectLocationAgregator.create(
		     camera_matrix_feature_name = 'K',
		    category_index_feature_name = 'category_index',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = locs_dir,
		                filename_format = 'locations_{}.csv',
		                           name = 'objects_locations_agregator'
		),
	# Visibility
	pydataset.manipulators.pose.VisibilityAgregator.create(
		  category_feature_name = 'category_index',
		visibility_feature_name = 'visibility',
		      mask_feature_name = 'mask',
		        output_filepath = arguments.output_directory_path + pytools.path.FilePath( 'visibility.csv' ),
		),
	# Pose Error
	pydataset.manipulators.pose.PoseErrorAgregator.create(
		          category_feature_name = 'category_index',
		             depth_feature_name = 'depth',
		       depth_scale_feature_name = 'depth_scale',
		      render_depth_feature_name = 'render_depth',
		render_depth_scale_feature_name = 'render_depth_scale',
		              mask_feature_name = 'mask',#'visible_mask',
		          camera_k_feature_name = 'K',
		              outlier_threshold = 50.0,
		                output_filepath = arguments.output_directory_path + pytools.path.FilePath( 'pose_error.csv' ),
		                           name = 'pose_error_agregator',
	),
	# Sequence manipulation settings
	url_search_pattern = URL_SEARCH_PATTERN,
	              name = 'bop_lm_stats'
	)

pt.log( 'Agregating:' )
pt.flush()
pydataset.manipulators.apply_async(
	              manipulator = sequence,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pt,
	     wait_for_application = True
	)