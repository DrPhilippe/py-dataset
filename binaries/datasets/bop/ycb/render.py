# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pylinemod
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL                  = 'file://D:/Datasets/bop_ycb_video'
MODELS_URL_SEARCH_PATTERN    = 'groups/objects/examples/*'
IMAGES_URL_SEARCH_PATTERN    = 'groups/scenes/groups/{}/groups/{}/groups/*'
INSTANCES_URL_SEARCH_PATTERN = 'examples/*'

# ##################################################
# ###                 ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url', type=str, help='URL of the BOP YCB-V dataset.', default=DATASET_URL )
parser.add_argument( '--split', type=str, help='Split to render', default='*' )
parser.add_argument( '--scene', type=str, help='Scene to render', default='*' )
arguments = parser.parse_args()

IMAGES_URL_SEARCH_PATTERN = IMAGES_URL_SEARCH_PATTERN.format(
	arguments.split,
	'*' if arguments.scene=='*' else '{:06d}'.format( int(arguments.scene) )
	)

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP YCB-V RENDER' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    url_search_pattern: {}', IMAGES_URL_SEARCH_PATTERN )
pt.end_line()
pt.flush()

# ##################################################
# ###                   RENDER                   ###
# ##################################################

# renderer = pylinemod.Renderer.create(
# 	               progress_tracker = pytools.tasks.console_progress_logger( 'Rendering Color Images' ),
# 	                         logger = pytools.tasks.file_logger( 'logs/bop_ycb_v_render_color.log' ),
# 	                    dataset_url = arguments.dataset_url,
# 	     objects_url_search_pattern = MODELS_URL_SEARCH_PATTERN,
# 	      images_url_search_pattern = IMAGES_URL_SEARCH_PATTERN,
# 	   instances_url_search_pattern = INSTANCES_URL_SEARCH_PATTERN,
# 	              mesh_feature_name = 'mesh_eval',
# 	     category_name_feature_name = 'category_index',
# 	     camera_matrix_feature_name = 'K',
# 	   rotation_matrix_feature_name = 'R',
# 	translation_vector_feature_name = 't',
# 	            render_feature_name = 'render_color',
# 	                         shader = 'color',
# 	                 is_dry_running = False
# 	)
# renderer.execute()

renderer = pylinemod.Renderer.create(
	               progress_tracker = pytools.tasks.console_progress_logger( 'Rendering Depth Maps' ),
	                         logger = pytools.tasks.file_logger( 'logs/bop_ycb_v_render_depth.log' ),
	                    dataset_url = arguments.dataset_url,
	     objects_url_search_pattern = MODELS_URL_SEARCH_PATTERN,
	      images_url_search_pattern = IMAGES_URL_SEARCH_PATTERN,
	   instances_url_search_pattern = INSTANCES_URL_SEARCH_PATTERN,
	              mesh_feature_name = 'mesh_eval',
	     category_name_feature_name = 'category_index',
	     camera_matrix_feature_name = 'K',
	   rotation_matrix_feature_name = 'R',
	translation_vector_feature_name = 't',
	            render_feature_name = 'render_depth',
	                         shader = 'depth',
	                 is_dry_running = False
	)
renderer.execute()