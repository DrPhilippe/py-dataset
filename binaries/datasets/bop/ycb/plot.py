
import cv2
import numpy
import pydataset

# for i in range( 1213 ):
i = 5
group = pydataset.dataset.get( 'file://D:/Datasets/bop-ycb-video/groups/scenes/groups/test/groups/000059/groups/{:06d}'.format(i) )
image = group.get_feature( 'image' ).value
K     = group.get_feature( 'K'    ).value
dist_coeffs = numpy.zeros( [5], dtype=numpy.float32 )
H, W = image.shape[0], image.shape[1]

ycb_mask = image.copy()
ycb_lcs = image.copy()

count = len(group.examples_names)

COLORS = [
	(128, 128, 255),
	(128, 255, 255),
	(128, 255, 128),
	(255, 255, 128),
	(255, 128,   0),
	(192, 128, 255),
	(225,   0,   0),
	(  0, 255,   0),
	(0,     0, 255)
	]

for j in range(count):

	example = group.get_example( j, name_format='{}{:06d}', prefix='' )
	mesh    = example.get_feature( 'mesh' ).value
	R       = example.get_feature( 'R'    ).value
	t       = example.get_feature( 't'    ).value
	mask    = example.get_feature( 'visible_mask' ).value
	r       = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )
	color   = COLORS[ j ]

	# MASK
	mask     = numpy.reshape( mask, [H, W, 1] )
	mask     = numpy.tile( mask, [1, 1, 3] )
	colors   = numpy.reshape( numpy.asarray(color, dtype=numpy.uint8), [1, 1, 3] )
	colors   = numpy.tile( colors, [H, W, 1] )
	ycb_mask = numpy.where( mask, colors, ycb_mask )

	# LCS
	ycb_lcs = cv2.drawFrameAxes( ycb_lcs, K, dist_coeffs, r, t, 0.1, 2 )

cv2.imshow( 'ycb_mask', ycb_mask )
cv2.imshow( 'ycb_lcs', ycb_lcs )

if cv2.waitKey(0) == ord('q'):
	exit()

cv2.imwrite( 'ycb.png', image )
cv2.imwrite( 'ycb_mask.png', ycb_mask )
cv2.imwrite( 'ycb_lcs.png', ycb_lcs )
