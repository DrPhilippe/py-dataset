# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/bop_ycb_video'
NUMBER_OF_THREADS = 24

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL where to create the imported BOP-LM dataset.', default=DATASET_URL )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to import the dataset.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP-YCB-VIDEO PREPARE' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    number_of_threads:  {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###          COMPUTE IMAGE-WISE LABELS         ###
# ##################################################

mapper = pydataset.manipulators.ManipulationSequence.create(
	# Image wize complete foreground mask
	pydataset.manipulators.image.FullMaskCreator.create(
		instances_url_search_pattern = 'examples/*',
				   mask_feature_name = 'visible_mask',
		                        name = 'bop_ycb.full_mask'
		),
	# Yolo V1 labels
	pydataset.manipulators.yolo_v1.Labeler.create(
		                number_of_cells = 5,
		              number_of_classes = 21,
		   instances_url_search_pattern = 'examples/*',
		             image_feature_name = 'image',
		bounding_rectangle_feature_name = 'bounding_rectangle',
		  one_hot_category_feature_name = 'one_hot_category',
		              yolo_feature_name = 'yolo_v1',
		                      normalize = True,
		                           name = 'bop_ycb.yolo_v1'
		),
	# BB8 V1 labels
	pydataset.manipulators.bb8.MeshToBB8V1.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		    category_index_feature_name = 'category_index',
		           number_of_categories = 21,
		               bb8_feature_name = 'bb8_v1',
		   instances_url_search_pattern = 'examples/*',
		                           name = 'bop_ycb.bb8_v1'
		),
	# Common settings
	url_search_pattern = 'groups/scenes/groups/*/groups/*/groups/*',
		          name = 'bop_ycb.image_wise'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Image-Wise Labels' ),
	     wait_for_application = True
	)

# ##################################################
# ###        COMPUTE INSTANCE-WISE LABELS        ###
# ##################################################

mapper = pydataset.manipulators.ManipulationSequence.create(
	# Agregate objects sizes
	pydataset.manipulators.SizeAgregator.create(
		bounding_rectangle_feature_name = 'bounding_rectangle',
			      category_feature_name = 'category_name',
			      output_directory_path = 'data/bop_ycb_video/sizes',
			            filename_format = 'sizes_{}.csv',
			                       name = 'bop_ycbvideo.size_agregator'
		),
	# PoseNet quaternion labels
	pydataset.manipulators.pose.Matrix2Quaternion.create(
		     input_matrix_feature_name = 'R',
		output_quaternion_feature_name = 'q',
		                          name = 'bop_ycb.quaternion'
		),
	# Bounding boxes for PnP computations
	pydataset.manipulators.pose.MeshToBoundingBox.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = '', # bounding boxes are computed in model space
		       translation_feature_name = '', # bounding boxes are computed in model space
		          camera_k_feature_name = '', # bounding boxes are computed in model space
		camera_dist_coeffs_feature_name = '', # bounding boxes are computed in model space
		      bounding_box_feature_name = 'bounding_box',
		                           name = 'bop_ycb.bounding_box'
		),
	# BB8 V1 labels
	pydataset.manipulators.bb8.MeshToBB8V1.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		    category_index_feature_name = 'category_index',
		           number_of_categories = 21,
		   instances_url_search_pattern = '',
		               bb8_feature_name = 'bb8_v1',
		                           name = 'bop_ycb.bb8_v1'
		),
	# BB8 V2 labels
	pydataset.manipulators.bb8.MeshToBB8V2.create(
		         mesh_data_feature_name = 'mesh',
		          rotation_feature_name = 'R', # BB8 labels are computed in image space
		       translation_feature_name = 't', # BB8 labels are computed in image space
		          camera_k_feature_name = 'K', # BB8 labels are computed in image space
		camera_dist_coeffs_feature_name = '',  # BB8 labels are computed in image space
		               bb8_feature_name = 'bb8_v2',
		                           name = 'bop_ycb.bb8_v2'
		),
	# Common settings
	url_search_pattern = 'groups/scenes/groups/*/groups/*/groups/*/examples/*',
	              name = 'bop_ycb.instance_wise'
	)
pydataset.manipulators.apply_async(
	              manipulator = mapper,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'Computing Instance-Wise Labels' ),
	     wait_for_application = True
	)
