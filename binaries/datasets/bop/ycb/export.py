# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pykeras
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL = 'file://D:/Datasets/bop_ycb_video'

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL where to import BOP-YCB-V dataset.', default=DATASET_URL )
arguments = parser.parse_args()

# # ##################################################
# # ###        EXPORT IMAGE-WISE TF-RECORDS        ###
# # ##################################################

# # Export training tf-records
# pt = pytools.tasks.console_progress_logger( 'Exporting Image-Wise Training TF-Records' )
# exporter = pykeras.records.RecordsExporter.create(
# 	            logger = pytools.tasks.file_logger( 'logs/bop_ycb.imagewise_training_tfrecords.log' ),
# 	  progress_tracker = pt,
# 	         directory = '',
# 	  records_filename = 'training_imagewise.tfrecords',
# 	blueprint_filename = 'training_imagewise.json',
# 	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/train_real/groups/*/groups/*',
# 	  compression_type = 'GZIP',
# 	 compression_level = 8,
# 	           shuffle = True,
# 	    features_names = [
# 		'image',
# 		'visible_mask',
# 		'K',
# 		'yolo_v1',
# 		'bb8_v1',
# 		'bb8_v1_ref',
# 		]
# 	)
# exporter.execute()

# # Export testing tf-records
# pt = pytools.tasks.console_progress_logger( 'Exporting Image-Wise Testing TF-Records' )
# exporter = pykeras.records.RecordsExporter.create(
# 	            logger = pytools.tasks.file_logger( 'logs/bop_ycb.imagewise_testing_tfrecords.log' ),
# 	  progress_tracker = pt,
# 	         directory = '',
# 	  records_filename = 'testing_imagewise.tfrecords',
# 	blueprint_filename = 'testing_imagewise.json',
# 	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/test/groups/*/groups/*',
# 	  compression_type = 'GZIP',
# 	 compression_level = 8,
# 	           shuffle = False,
# 	    features_names = [
# 		'image',
# 		'visible_mask',
# 		'K',
# 		'yolo_v1',
# 		'bb8_v1',
# 		'bb8_v1_ref',
# 		]
# 	)
# exporter.execute()

# ##################################################
# ###      EXPORT INSTANCE-WISE TF-RECORDS       ###
# ##################################################

def visibility_filter ( example ):
	visibility = example.get_feature( 'visibility' ).value
	return visibility < 0.4

# Export training tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Instance-Wise Training TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	         filter_fn = visibility_filter,
	            logger = pytools.tasks.file_logger( 'logs/bop_ycb.instancewise_training_tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'training_instancewise.tfrecords',
	blueprint_filename = 'training_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/train_real/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = True,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref'
		]
	)
exporter.execute()

# Export testing tf-records
pt = pytools.tasks.console_progress_logger( 'Exporting Instance-Wise Testing TF-Records' )
exporter = pykeras.records.RecordsExporter.create(
	         filter_fn = visibility_filter,
	            logger = pytools.tasks.file_logger( 'logs/instancewise_testing.tfrecords.log' ),
	  progress_tracker = pt,
	         directory = '',
	  records_filename = 'testing_instancewise.tfrecords',
	blueprint_filename = 'testing_instancewise.json',
	url_search_pattern = arguments.dataset_url+'/groups/scenes/groups/test/groups/*/groups/*/examples/*',
	  compression_type = 'GZIP',
	 compression_level = 8,
	           shuffle = False,
	    features_names = [
		'image',
		'visible_mask',
		'K',
		'one_hot_category',
		'bounding_rectangle',
		'bounding_box',
		'R',
		'q',
		't',
		'bb8_v1',
		'bb8_v1_ref'
		]
	)
exporter.execute()