# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

DATASET_URL              = 'file://D:/Datasets/bop_ycb_video'
OUTPUT_DIRECTORY_PATH    = 'stats/bop_ycb_video'
NUMBER_OF_THREADS        = 16
TRAIN_URL_SEARCH_PATTERN = 'groups/scenes/groups/train_real/groups/*/groups/*/examples/*'
TEST_URL_SEARCH_PATTERN  = 'groups/scenes/groups/test/groups/*/groups/*/examples/*'

# ##################################################
# ###                  ARGUMENT                  ###
# ##################################################

# Argument
parser = argparse.ArgumentParser()
parser.add_argument( '--dataset_url',  type=str, help='URL of the BOP YCV-VIDEO dataset.', default=DATASET_URL )
parser.add_argument( '--train_url_search_pattern', type=str, help='URL seatch pattern used to find training object instances.', default=TRAIN_URL_SEARCH_PATTERN )
parser.add_argument( '--test_url_search_pattern', type=str, help='URL seatch pattern used to find test object instances.', default=TEST_URL_SEARCH_PATTERN )
parser.add_argument( '--output_directory_path', type=pytools.path.DirectoryPath, help='Directory where to save the statistics.', default=OUTPUT_DIRECTORY_PATH )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to agregate statistics.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP-YCB-VIDEO STATS' )

# Echo arguments
pt.log( 'Arguments:' )
pt.log( '    dataset_url:              {}', arguments.dataset_url )
pt.log( '    train_url_search_pattern: {}', arguments.train_url_search_pattern )
pt.log( '    test_url_search_pattern:  {}', arguments.test_url_search_pattern )
pt.log( '    output_directory_path:    {}', arguments.output_directory_path )
pt.log( '    number_of_threads:        {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###                DIRECTORIES                 ###
# ##################################################

# Output directory tree
train_dir      = arguments.output_directory_path + pytools.path.DirectoryPath( 'train' )
train_locs_dir = train_dir + pytools.path.DirectoryPath( 'locations' )
train_povs_dir = train_dir + pytools.path.DirectoryPath( 'points_of_views' )
test_dir       = arguments.output_directory_path + pytools.path.DirectoryPath( 'test' )
test_locs_dir  = test_dir + pytools.path.DirectoryPath( 'locations' )
test_povs_dir  = test_dir + pytools.path.DirectoryPath( 'points_of_views' )

# Echo directories
pt.log( 'Directories:' )
pt.log( '    train:                 {}', train_dir )
pt.log( '    train/locations:       {}', train_locs_dir )
pt.log( '    train/points_of_views: {}', train_povs_dir )
pt.log( '    test:                  {}', test_dir )
pt.log( '    test/locations:        {}', test_locs_dir )
pt.log( '    test/points_of_views:  {}', test_povs_dir )
pt.end_line()
pt.flush()

# ##################################################
# ###             COMPUTE STATISTICS             ###
# ##################################################

# TRAINING
sequence = pydataset.manipulators.ManipulationSequence.create(
	# Agregate Test Point Of Views
	pydataset.manipulators.pose.PointOfViewAgregator.create(
		    category_index_feature_name = 'category_name',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = train_povs_dir,
		                filename_format = 'points_of_views_{}.csv',
		                           name = 'points_of_views_agregator'
		),
	# On-Screen Object Locations
	pydataset.manipulators.pose.ObjectLocationAgregator.create(
		     camera_matrix_feature_name = 'K',
		    category_index_feature_name = 'category_name',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = train_locs_dir,
		                filename_format = 'locations_{}.csv',
		                           name = 'objects_locations_agregator'
		),
	# Visibility
	pydataset.manipulators.pose.VisibilityAgregator.create(
		  category_feature_name = 'category_name',
		visibility_feature_name = 'visibility',
		      mask_feature_name = 'mask',
		        output_filepath = train_dir + pytools.path.FilePath( 'visibility.csv' ),
		),
	# Pose Error
	pydataset.manipulators.pose.PoseErrorAgregator.create(
		          category_feature_name = 'category_name',
		             depth_feature_name = 'depth',
		       depth_scale_feature_name = 'depth_scale',
		      render_depth_feature_name = 'render_depth',
		render_depth_scale_feature_name = 'render_depth_scale',
		              mask_feature_name = 'visible_mask',
		              outlier_threshold = 50.0,
		                output_filepath = train_dir + pytools.path.FilePath( 'pose_error.csv' ),
		           output_directorypath = '', #test_dir + pytools.path.DirectoryPath( 'pose_errors' ),
		         output_filename_format = '', #'{}_error_{:07.4f}.png',
		                           name = 'pose_error_agregator'
		),
	# Sequence manipulation settings
	url_search_pattern = arguments.train_url_search_pattern,
	              name = 'bop_ycb_v_train_stats'
	)
pydataset.manipulators.apply_async(
	              manipulator = sequence,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'BOP-YCB-V TRAIN STATS' ),
	     wait_for_application = True
	)

# TESTING
sequence = pydataset.manipulators.ManipulationSequence.create(
	# Agregate Test Point Of Views
	pydataset.manipulators.pose.PointOfViewAgregator.create(
		    category_index_feature_name = 'category_name',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = test_povs_dir,
		                filename_format = 'points_of_views_{}.csv',
		                           name = 'points_of_views_agregator'
		),
	# On-Screen Object Locations
	pydataset.manipulators.pose.ObjectLocationAgregator.create(
		     camera_matrix_feature_name = 'K',
		    category_index_feature_name = 'category_name',
		   rotation_matrix_feature_name = 'R',
		translation_vector_feature_name = 't',
		          output_directory_path = test_locs_dir,
		                filename_format = 'locations_{}.csv',
		                           name = 'objects_locations_agregator'
		),
	# Visibility
	pydataset.manipulators.pose.VisibilityAgregator.create(
		  category_feature_name = 'category_name',
		visibility_feature_name = 'visibility',
		      mask_feature_name = 'mask',
		        output_filepath = test_dir + pytools.path.FilePath( 'visibility.csv' ),
		),
	# Pose Error
	pydataset.manipulators.pose.PoseErrorAgregator.create(
		          category_feature_name = 'category_name',
		             depth_feature_name = 'depth',
		       depth_scale_feature_name = 'depth_scale',
		      render_depth_feature_name = 'render_depth',
		render_depth_scale_feature_name = 'render_depth_scale',
		              mask_feature_name = 'visible_mask',
		              outlier_threshold = 50.0,
		                output_filepath = test_dir + pytools.path.FilePath( 'pose_error.csv' ),
		           output_directorypath = '', #test_dir + pytools.path.DirectoryPath( 'pose_errors' ),
		         output_filename_format = '', #'{}_error_{:07.4f}.png',
		                           name = 'pose_error_agregator'
		),
	# Sequence manipulation settings
	url_search_pattern = arguments.test_url_search_pattern,
	              name = 'bop_ycb_v_test_stats'
	)
pydataset.manipulators.apply_async(
	              manipulator = sequence,
	url_search_pattern_prefix = arguments.dataset_url,
	        number_of_threads = arguments.number_of_threads,
	         progress_tracker = pytools.tasks.console_progress_logger( 'BOP-YCB-V TEST STATS' ),
	     wait_for_application = True
	)