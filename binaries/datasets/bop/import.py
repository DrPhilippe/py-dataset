# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse

# INTERNALS
import pybop
import pytools

# ##################################################
# ###                  DEFAULTS                  ###
# ##################################################

MODE = 'create'
NUMBER_OF_THREADS = 24

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

# Arguments
parser = argparse.ArgumentParser()
parser.add_argument( 'dataset_path', type=str, help='Path of the directory containg the BOP dataset to import.' )
parser.add_argument( 'dataset_url',  type=str, help='URL where to import the BOP dataset.' )
parser.add_argument( '--mode',  type=str, choices=['create', 'combine'], default=MODE, help='Importation mode.' )
parser.add_argument( '--ignore_fine_models', action='store_true', help='Weither or not to import fine models (heavy models).', default=DATASET_URL )
parser.add_argument( '--number_of_threads', type=int, help='Number of threads used to prepare the dataset.', default=NUMBER_OF_THREADS )
arguments = parser.parse_args()

# ##################################################
# ###                  LOGGING                   ###
# ##################################################

# Create progress tracker / logger
pt = pytools.tasks.console_progress_logger( 'BOP IMPORT' )

# Echo arguments
pt.end_line()
pt.log( 'Arguments:' )
pt.log( '    dataset_path:       {}', arguments.dataset_path )
pt.log( '    dataset_url:        {}', arguments.dataset_url )
pt.log( '    mode:               {}', arguments.mode )
pt.log( '    ignore_fine_models: {}', arguments.ignore_fine_models )
pt.log( '    number_of_threads:  {}', arguments.number_of_threads )
pt.end_line()
pt.flush()

# ##################################################
# ###             IMPORT THE DATASET             ###
# ##################################################

dataset_importer = pybop.DatasetImporter.create(
	dataset_directory_path = arguments.dataset_path,
	         dst_group_url = arguments.dataset_url,
	                  mode = arguments.mode,
	    ignore_fine_models = True,
	                  name = 'bop_importer',
	      progress_tracker = pt,
	                logger = pytools.tasks.file_logger( 'logs/bop_importer.log' )
	)
dataset_importer.execute()