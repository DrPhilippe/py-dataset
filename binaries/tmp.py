import cv2
import numpy
import pykeras
import pytools

testing = pykeras.inputs_v2.TFRecordsDataset.open(
	        directory_path = 'D:\\Datasets\\bop_ycb_video',
	    tf_record_filename = 'training.tfrecords',
	    blueprint_filename = 'training.json',
	                  name = 'training',
	initial_features_names = [
	    'image',
	    'visible_mask',
	    'bounding_rectangle',
	    'one_hot_category'
	    ],
	                repeat = False,
	               shuffle = False,
	   shuffle_buffer_size = 0,
	            batch_size = 0,
	   drop_batch_reminder = False,
	         prefetch_size = 0
	)
testing.add_mapper(
	pykeras.inputs_v2.mappers.BoundingRectangleCropper,
	          images_features_names = [ 'image', 'visible_mask' ],
	bounding_rectangle_feature_name = 'bounding_rectangle',
	                      crop_size = (0, 0),
	                       pad_size = (16, 16),
	        affected_features_names = [ 'bounding_rectangle' ]
	)
testing.add_mapper(
	pykeras.inputs_v2.mappers.ImagesResizer,
	  images_features_names = [ 'image', 'visible_mask' ],
	                   size = (224, 224),
	                 method = 'bilinear',
	affected_features_names = [ 'bounding_rectangle' ]
	)
		

logger = pytools.tasks.console_logger()
for features in testing.use(logger):

	image = features[ 'image' ].numpy()
	mask  = features[ 'visible_mask' ].numpy().astype(numpy.uint8) * 255
	br    = features[ 'bounding_rectangle' ].numpy().astype(int)
	# print( image.shape, mask.shape )

	image = cv2.rectangle( image, (br[0,0], br[0,1]), (br[1,0], br[1,1]), (0,0,255), 2 )
	cv2.imshow( 'image', image )
	cv2.imshow( 'mask', mask )
	if cv2.waitKey( 10 ) == ord('q'):
		exit()
