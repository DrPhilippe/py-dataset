# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtCore
import PyQt5.QtWidgets

# INTERNALS
import pydataset.dataset
import pydataset.hierarchy
import pydataset.previews
import pydataset.widgets
import pytools.assertions
import pyui.inspector

# LOCALS
from .menu_bar import MenuBar

# ##################################################
# ###            CLASS MAIN-WINDOW               ###
# ##################################################

class MainWindow ( PyQt5.QtWidgets.QMainWindow ):

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, title='PyViewer' ):
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is( title, str )

		super( MainWindow, self ).__init__()

		# Set window properties
		self.setWindowTitle( title )
		self.setup()
		self.setWindowState( self.windowState() ^ PyQt5.QtCore.Qt.WindowMaximized )

	# def __init__ ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def setup ( self ):
		"""
		Creates the components of this main window.

		Arguments:
			self (`pyviewer.MainWindow`): The main window.
		"""
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		menu_bar = MenuBar( self )
		menu_bar.dataset_menu.dataset_opened.connect( self.on_dataset_opened )
		menu_bar.dataset_menu.dataset_closed.connect( self.on_dataset_closed )
		menu_bar.preview_menu.config_opened.connect( self.on_preview_config_opened_or_changed )
		menu_bar.preview_menu.config_changed.connect( self.on_preview_config_opened_or_changed )
		self.setMenuBar( menu_bar )

		# Hierarchy dock
		self._hierarchy_dock = pydataset.hierarchy.HierarchyDock(
			datasets = [],
			   title = 'Navigate',
			  parent = self
			)
		self._hierarchy_dock.item_selected.connect( self.on_dataset_item_selected )
		self.addDockWidget( PyQt5.QtCore.Qt.LeftDockWidgetArea, self._hierarchy_dock )

		# Inspector dock
		self._inspector_dock = pyui.inspector.InspectorDock(
			      value = None,
			is_editable = False,
			      title = 'Inspect',
			     parent = self
			)
		self.addDockWidget( PyQt5.QtCore.Qt.RightDockWidgetArea, self._inspector_dock )

		# Preview Dock
		self._preview_dock = pydataset.previews.PreviewDock(
			       example = None,
			preview_config = None,
			         title = 'Preview',
			        parent = self
			)
		self.setCentralWidget( self._preview_dock )

	# def setup ( self )

	# ##################################################
	# ###                   SLOTS                    ###
	# ##################################################

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( pydataset.dataset.Dataset )
	def on_dataset_opened ( self, dataset ):
		"""
		Slot invoked when a dataset is created or opened through the dataset menu.

		Arguments:
			self          (`pyviewer.MainWindow`): The main window.
			dataset (`pydataset.dataset.Dataset`): The dataset.
		"""
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Set the datasets in the docks
		self._hierarchy_dock.add_dataset( dataset )
		self._preview_dock.example = dataset
		self._inspector_dock.value = dataset.data if dataset else None
		
	# def on_dataset_opened ( self, dataset )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot()
	def on_dataset_closed ( self ):
		"""
		Slot invoked when a dataset is closed through the dataset menu.

		Arguments:
			self (`pyviewer.MainWindow`): The main window.
		"""
		assert pytools.assertions.type_is_instance_of( self, MainWindow )

		# Remove the dataset
		dataset = self._hierarchy_dock.current
		self._hierarchy_dock.remove_dataset( dataset )

		# Clear the UI
		self._inspector_dock.value = None

		# Disable the menu bar / dataset menu / close action
		self.menuBar().dataset_menu.close_action.setEnabled( False )

	# def on_dataset_closed ( self )

	# --------------------------------------------------

	@PyQt5.QtCore.pyqtSlot( pydataset.dataset.Item )
	def on_dataset_item_selected ( self, item ):
		"""
		Slot invoked when an item of the dataset hierarchy is selected.

		Arguments:
			self    (`pyviewer.MainWindow`): The main window.
			item (`pydataset.dataset.Item`): The selected item.
		"""
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is_instance_of( item, (type(None), pydataset.dataset.Item) )

		# Disable the menu bar / dataset menu / close action
		self.menuBar().dataset_menu.close_action.setEnabled( False )

		# If nothing is selected or a sub-groups, sub-features, sub-examples
		if item is None:

			# Clear the preview
			self._preview_dock.example = None

			# Clear the inspector
			self._inspector_dock.value = None

		# if something is selected
		else:
			# List of data display in the inspector
			data = [ item.data ]

			# If its an example, display it in the preview and inspect its features
			if isinstance( item, pydataset.dataset.Example ):

				# Display it in the preview
				self._preview_dock.example = item

				# Also display its features in the inspector
				for feature_name, feature in item.get_features().items():
					data.append( feature.data )

				# If its a dataset, enable the option to close it (dataset are special example)
				if isinstance( item, pydataset.dataset.Dataset ):

					# Enable the option to close it
					self.menuBar().dataset_menu.close_action.setEnabled( True )
			
			# if isinstance( item, pydataset.dataset.Example )

			self._inspector_dock.value = data

		# if item is None:

	# def on_dataset_closed ( self, item )

	# --------------------------------------------------

	def on_preview_config_opened_or_changed ( self, preview_config ):
		"""
		Slot invoked when a preview config is create, opened or modified through the preview menu.

		Arguments:
			self                        (`pyviewer.MainWindow`): The main window.
			preview_config (`pydataset.previews.PreviewConfig`): The preview config.
		"""
		assert pytools.assertions.type_is_instance_of( self, MainWindow )
		assert pytools.assertions.type_is_instance_of( preview_config, pydataset.previews.PreviewConfig )

		self._preview_dock.preview_config = preview_config

	# def on_preview_config_opened_or_changed ( self, preview_config )

# class MainWindow ( PyQt5.QtWidgets.QMainWindow )