# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pyui.widgets
import pydataset.hierarchy
import pydataset.previews

# LOCALS

# ##################################################
# ###             CLASS MENU-BAR                 ###
# ##################################################

class MenuBar ( pyui.widgets.MenuBar ):
	
	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, parent=None ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )
		assert pytools.assertions.type_is_instance_of( parent, (type(None), PyQt5.QtWidgets.QWidget) )

		# Init the parent
		super( MenuBar, self ).__init__( parent )

	# def __init__ ( self, parent )

	# ##################################################
	# ###                  GETTERS                   ###
	# ##################################################
		
	# --------------------------------------------------

	@property
	def dataset_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )

		return self._dataset_menu
	
	# def get_dataset_menu ( self )
	
	# --------------------------------------------------

	@property
	def preview_menu ( self ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )

		return self._preview_menu
	
	# def preview_menu ( self )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		assert pytools.assertions.type_is_instance_of( self, MenuBar )

		# Setup the parent
		super( MenuBar, self ).setup()

		# Create the Dataset menu
		self._dataset_menu = pydataset.hierarchy.HierarchyMenu( parent=self )
		self.addMenu( self._dataset_menu )

		# Create the Preview menu
		self._preview_menu = pydataset.previews.PreviewMenu( parent=self )
		self.addMenu( self._preview_menu )

	# def __init__ ( self )

# class MenuBar ( pyui.widgets.MenuBar )