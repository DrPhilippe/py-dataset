# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import PyQt5.QtWidgets

# INTERNALS
import pytools.assertions
import pytools.path
import pyui.widgets

# LOCALS
from .main_window import MainWindow

# ##################################################
# ###             CLASS APPLICATION              ###
# ##################################################

class Application ( pyui.widgets.Application ):
	"""
	Viewer application displays the examples of a dataset.
	"""
	
	# ##################################################
	# ###                CLASS FIELDS                ###
	# ##################################################

	# --------------------------------------------------

	"""
	Default assets directory (`pytools.path.DirectoryPath`).
	"""
	default_assets_directory_path = pytools.path.FilePath( __file__ ).parent().parent() + pytools.path.DirectoryPath( 'assets' )

	# ##################################################
	# ###               CONSTRUCTOR                  ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, argv ):
		"""
		Initializes a new instance of the `viewer.widgets.Application` class.

		Arguments:
			self (`viewer.widgets.Application`): Instance to initialize.
			argv              (`list` of `str`): The application command-line arguments.
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )
		assert pytools.assertions.type_is( argv, list )
		assert pytools.assertions.list_items_type_is( argv, str )

		# Initialize the application
		super( Application, self ).__init__( argv, 'ITECA', 'PyViewer', Application.default_assets_directory_path )

	# def __init__ ( self, argv )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def setup ( self ):
		"""
		Creates the componemets of this application.

		Arguments:
			self (`viewer.widgets.Application`): Application to setup.
		"""
		assert pytools.assertions.type_is_instance_of( self, Application )
		
		# Setup the base application
		super( Application, self ).setup()

		# Create the main window
		self.main_window = MainWindow()
		
	# def setup ( self )

# class Application ( pyui.widgets.Application )