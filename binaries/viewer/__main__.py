# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import sys

# LOCALS
from widgets import Application

# ##################################################
# ###                  MAIN                      ###
# ##################################################

if __name__ == '__main__':
	app = Application( sys.argv )
	app.exec()