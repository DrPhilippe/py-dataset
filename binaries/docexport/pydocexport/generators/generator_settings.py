# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# ##################################################
# ###          CLASS GENERATOR-SETTINGS          ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'GeneratorSettings',
	   namespace = 'docexport',
	fields_names = [
		'name'
		]
	)
class GeneratorSettings ( pytools.serialization.Serializable ):
	"""
	Base class for custom documentation generator settings.

	Extend the `docexport.GeneratorSettings` classto define custom documentation generator settings
	to allow for customization of the output of the associated custom generator.
	For example styling settings, the filepath of a logo and more could be defined.
	"""
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:GeneratorSettings, name:str='Generator' ):
		"""
		Initialize a new instance of the `GeneratorSettings` class.

		Arguments:
			self (`docexport.GeneratorSettings`): Instance to initialize.
			name                         (`str`): Name of the generator.
		"""
		assert pytools.assertions.type_is_instance_of( self, GeneratorSettings )
		assert pytools.assertions.type_is( name, str )

		super( GeneratorSettings, self ).__init__()

		self.name = name

	# def __init__ ( self, name )

	# --------------------------------------------------

	@property
	def name ( self:GeneratorSettings ) -> str:
		"""
		The nane of the generator (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, GeneratorSettings )

		return self._name

	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self:GeneratorSettings, value:str ):
		assert pytools.assertions.type_is_instance_of( self, GeneratorSettings )
		assert pytools.assertions.type_is( value, str )

		self._name = value
		
	# def name ( self, value )

# class GeneratorSettings( pytools.serialization.Serializable )