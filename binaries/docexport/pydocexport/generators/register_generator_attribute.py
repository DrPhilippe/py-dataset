# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.factory

# ##################################################
# ###     CLASS REGISTER-GENERATOR-ATTRIBUTE     ###
# ##################################################

class RegisterGeneratorAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Attribute used on the class declaration of custom documentation generator to register them in the `ClassFactory`.

	Generator are alwayse registered in the group named `'Generators'`.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self:RegisterGeneratorAttribute, name:str ):
		"""
		Initalizes a new instance of the `RegisterGeneratorAttribute` class.
		
		The name given to the generator using this attribute to a custom documentationgenerator
		is the same name that can be used with the command line argument `'--generator'` to
		specify which generator to use when running the `PydocExporter` programm.

		Arguments:
			self (`docexport.generators.RegisterGeneratorAttribute`): The attribute instance to initialize.
			name                                             (`str`): Name of the generator.
		"""
		assert pytools.assertions.type_is( name, str )
		
		super( RegisterGeneratorAttribute, self ).__init__(
			   group = 'generators',
			typename = name.lower()
			)

	# def __init__ ( self, name )

# class RegisterGeneratorAttribute ( pytools.factory.RegisterAttribute )