# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .generator_settings import GeneratorSettings

# ##################################################
# ###      CLASS GITLAB-GENERATOR-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'GeneratorSettings',
	   namespace = 'docexport.generator.GitlabGenrator',
	fields_names = [
		]
	)
class GitlabGeneratorSettings ( GeneratorSettings ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:GitlabGeneratorSettings, name:str='Gitlab Markdown Generator' ):
		assert pytools.assertions.type_is_instance_of( self, GitlabGeneratorSettings )
		assert pytools.assertions.type_is_instance_of( name, str )

		super( GitlabGeneratorSettings, self ).__init__( name )

	# def __init__ ( self, name )

# class GitlabGeneratorSettings ( Generator.GeneratorSettings )