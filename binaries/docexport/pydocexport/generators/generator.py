# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .. import data
from .generator_settings import GeneratorSettings

# ##################################################
# ###             CLASS A-GENERATOR              ###
# ##################################################

class Generator:
	"""
	Base class for custom documentation generator.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:Generator, settings:GeneratorSettings ):
		"""
		Initializes a new instance of the `docexport.Generator` with the given settings.

		Arguments:
			self             (`docexport.generators.Generator`): The generator instance to initialize.
			settings (`docexport.generators.GeneratorSettings`): Settings of the generator.
		"""
		assert pytools.assertions.type_is_instance_of( self, Generator )
		assert pytools.assertions.type_is_instance_of( settings, GeneratorSettings )

		self._settings = settings

	# def __init__ ( self, settings )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
		
	# --------------------------------------------------

	@property
	def settings ( self:Generator ) -> GeneratorSettings:
		"""
		Settings of this generator (`docexport.GeneratorSettings`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Generator )

		return self._settings

	# def settings ( self )

	# --------------------------------------------------

	@settings.setter
	def settings ( self:Generator, value:GeneratorSettings ):
		assert pytools.assertions.type_is_instance_of( self, Generator )
		assert pytools.assertions.type_is_instance_of( value, GeneratorSettings )

		self._settings = value
		
	# def settings ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def generate (
		self:GeneratorSettings,
		package_data:data.PackageData,
		output_directory_path:pytools.path.DirectoryPath,
		is_root:bool=False,
		progress_tracker:typing.Optional[pytools.task.ProgressTracker]=None
		):
		"""
		Generates the documentation of a package in the given output directory.

		Arguments:
			self                              (`docexport.Generator`): The documentation generator.
			package_data                    (`docexport.PackageData`): The package data.
			output_directory_path      (`pytools.path.DirectoryPath`): Path of the directory where to generate the documentation.
			is_root                                          (`bool`): If `True` this bool indicates that the package is the root package, otherwise, the package is a subpackage.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progression tracker used to display progression.
		"""
		assert pytools.assertions.type_is_instance_of( self, Generator )
		assert pytools.assertions.type_is_instance_of( package_data, docexport.data.PackageData )
		assert pytools.assertions.type_is_instance_of( output_directory_path, pytools.path.DirectoryPath )
		assert pytools.assertions.type_is( is_root, bool )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), pytools.tasks.ProgressTracker) )
		
		raise NotImplementedError()

	# def generate ( self, package_data, output_directory_path, is_root, progress_tracker )

# class Generator