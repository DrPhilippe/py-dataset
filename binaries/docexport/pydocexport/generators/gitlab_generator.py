# ##################################################
# ###                INFORMATIONS                ###
# ##################################################

"""
The `GitlabGenerator` generates documentation in Markdown format for Gitlab projects.

Authors:
	Philippe Perez de San Roman (philippe.perezdesanroman@iteca.eu)

References:
	https://docs.gitlab.com/ee/user/markdown.html
	https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/
"""

# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .generator                    import Generator
from .gitlab_generator_settings    import GitlabGeneratorSettings
from .register_generator_attribute import RegisterGeneratorAttribute

# ##################################################
# ###           CLASS GITLAB-GENERATOR           ###
# ##################################################

@RegisterGeneratorAttribute( 'Gitlab' )
class GitlabGenerator ( Generator ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, settings=GitlabGeneratorSettings() ):
		assert pytools.assertions.type_is_instance_of( self, GitlabGenerator )
		assert pytools.assertions.type_is_instance_of( settings, GitlabGeneratorSettings )

		super( GitlabGenerator, self ).__init__( settings )

	# def __init__ ( self, settings )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

# 	def GeneratePackageDocumentation ( self, package_data, output_directory_path, is_root=False, progress_tracker=None ):
# 		Assert.TypeIsInstanceOf( 'self', self, GitlabGenerator )
# 		Assert.TypeIs( 'package_data', package_data, Metadata.PackageData )
# 		Assert.TypeIs( 'output_directory_path', output_directory_path, FileSystem.DirectoryPath )
# 		Assert.TypeIsInstanceOf( 'progress_tracker', progress_tracker, (type(None), Progression.ATracker) )
		
# 		# Display the output directory
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'output directory:' )
# 			progress_tracker.Log( '    path: {}', output_directory_path  )
# 			progress_tracker.Log( '' )

# 		# Create the folder if it doesn't exist
# 		if ( output_directory_path.IsNotADirectory() ):
# 			output_directory_path.Create()

# 		# Compute number of sub-packages and modules
# 		sub_packages_count = len(package_data.packages)
# 		sub_modules_count  = len(package_data.modules)

# 		# Display package infos
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'package:' )
# 			progress_tracker.Log( '    name:         {}', package_data.name  )
# 			progress_tracker.Log( '    sub-packages: {}', sub_packages_count )
# 			progress_tracker.Log( '    sub-modules:  {}', sub_modules_count  )
# 			progress_tracker.Log( '' )

# 		# If the package as a specific namespace, create a subfolder for it
# 		if ( not is_root and package_data.namespace ):
# 			package_out_path = output_directory_path + FileSystem.DirectoryPath( package_data.name )
# 		else:
# 			package_out_path = output_directory_path

# 		# Display the package output directory
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'package output directory:' )
# 			progress_tracker.Log( '    path: {}', package_out_path  )
# 			progress_tracker.Log( '' )

# 		# Create the folder if it doesn't exist
# 		if ( package_out_path.IsNotADirectory() ):
# 			package_out_path.Create()

# 		self._GeneratePackageHomeDocumentation( package_data, package_out_path )

# 		# Display the current task
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'exporting sub-packages documentation' )

# 		# Iterate over the sub-packages
# 		for sub_package_index in range(sub_packages_count):

# 			sub_package_data = package_data.packages[ sub_package_index ]

# 			self.GeneratePackageDocumentation(
# 				         package_data = sub_package_data,
# 				output_directory_path = package_out_path,
# 				              is_root = False
# 				)

# 			if ( progress_tracker ):
# 				progress_tracker( float(sub_package_index)/float(sub_packages_count)/2.0 )

# 		# for sub_package_index in range(sub_packages_count)

# 		# Display progression
# 		if ( progress_tracker ):
# 			progress_tracker( 0.5 )
# 			progress_tracker.Log( 'done!' )
# 			progress_tracker.Log( '' )
# 			progress_tracker.Log( 'exporting sub-modules documentation' )

# 		# Generate each sub-modules
# 		for sub_module_index in range(sub_modules_count):

# 			# Get the sub-module data
# 			sub_module_data = package_data.modules[ sub_module_index ]

# 			# Export its documentation
# 			self.GenerateModuleDocumentation(
# 				          module_data = sub_module_data,
# 				output_directory_path = package_out_path
# 				)

# 			if ( progress_tracker ):
# 				progress_tracker( 0.5 + float(sub_module_index)/float(sub_modules_count)/2.0 )

# 		# for sub_module_index in range(sub_modules_count)

# 		# Display progression
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'done!' )
# 			progress_tracker.Log( '' )
# 			progress_tracker( 1.0 )

# 	# def GeneratePackageDocumentation ( self, package_data, output_directory_path )
	
# 	# --------------------------------------------------

# 	def _GeneratePackageHomeDocumentation ( self, package_data, output_directory_path ):

# 		output_filepath = output_directory_path + FileSystem.FilePath( 'Home.md' )
# 		if ( output_filepath.IsNotAFile() ):
# 			output_filepath.Create()

# 		with output_filepath.Open( 'w' ) as file:

# 			# Write the module title
# 			file.write( '## Package __' )
# 			file.write( GitlabGenerator.PrettyName(package_data.name) )
# 			file.write( '__\n' )
# 			file.write( '\n' )

# 			# Write the module import
# 			file.write( 'import ' )
# 			file.write( package_data.namespace )
# 			file.write( '\n' )

# 			# Line separator
# 			file.write( '\n' )
# 			file.write( '----\n' )
# 			file.write( '\n' )

# 			# Write the documentation
# 			file.write( GitlabGenerator.PrettyDoc( package_data.documentation ) )
# 			file.write( '\n' )
# 			file.write( '\n' )

# 			# Write the list of sub package
# 			if ( len(package_data.packages) > 0 ):

# 				# Line separator
# 				file.write( '----\n' )
# 				file.write( '\n' )

# 				# Write the functions
# 				file.write( '### Packages:\n' )
# 				file.write( '\n' )

# 				for sub_package_data in package_data.packages:

# 					file.write( '- ' )
# 					file.write( GitlabGenerator.PrettyName(sub_package_data.name) )
# 					file.write( '\n' )

# 				# for sub_package_data in package_data.packages
				
# 				file.write( '\n' )
			
# 			# if ( len(package_data.packages) > 0 )

# 			# Write the list of sub modules
# 			if ( len(package_data.modules) > 0 ):

# 				# Line separator
# 				file.write( '----\n' )
# 				file.write( '\n' )

# 				# Write the functions
# 				file.write( '### Modules:\n' )
# 				file.write( '\n' )

# 				for sub_module_data in package_data.modules:

# 					file.write( '- ' )
# 					file.write( GitlabGenerator.PrettyName(sub_module_data.name) )
# 					file.write( '\n' )

# 				# for sub_module_data in package_data.modules

# 				file.write( '\n' )
			
# 			# if ( len(package_data.modules) > 0 )

# 		# with output_filepath.Open( 'w' ) as file

# 	# def _GeneratePackageHomeDocumentation ( self, package_data, output_directory_path )

# 	# --------------------------------------------------
	
# 	def GenerateModuleDocumentation ( self, module_data, output_directory_path, progress_tracker=None ):
# 		Assert.TypeIsInstanceOf( 'self', self, GitlabGenerator )
# 		Assert.TypeIs( module_data, module_data, Metadata.ModuleData )
# 		Assert.TypeIs( output_directory_path, output_directory_path, FileSystem.DirectoryPath )
		
# 		# Display the output directory
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'output directory:' )
# 			progress_tracker.Log( '    path: {}', output_directory_path  )
# 			progress_tracker.Log( '' )

# 		# Create the folder if it doesn't exist
# 		if ( output_directory_path.IsNotADirectory() ):
# 			output_directory_path.Create()

# 		# Compute number of sub-packages and modules
# 		function_count = len(module_data.functions)
# 		class_count    = len(module_data.classes)

# 		# Display package infos
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'module:' )
# 			progress_tracker.Log( '    name:      {}', module_data.name  )
# 			progress_tracker.Log( '    functions: {}', function_count )
# 			progress_tracker.Log( '    classes:   {}', class_count  )
# 			progress_tracker.Log( '' )

# 		# If the package as a specific namespace, create a subfolder for it
# 		if ( module_data.namespace ):
# 			module_out_path = output_directory_path + FileSystem.DirectoryPath( module_data.name )
# 		else:
# 			module_out_path = output_directory_path

# 		# Display the package output directory
# 		if ( progress_tracker ):
# 			progress_tracker.Log( 'module output directory:' )
# 			progress_tracker.Log( '    path: {}', module_out_path  )
# 			progress_tracker.Log( '' )

# 		# Create the folder if it doesn't exist
# 		if ( module_out_path.IsNotADirectory() ):
# 			module_out_path.Create()

# 		# Generate a gome doc file for the module
# 		self._GenerateModuleHomeDocumentation( module_data, module_out_path )

# 		# Generate a doc file for the fucntions
# 		if ( len(module_data.functions) > 0 ):
		
# 			self._GenerateModuleFunctionsDocumentation( module_data, module_out_path )
		
# 		# if ( len(module_data.functions) > 0 )

# 		# Generate a doc file for each class in the module
# 		if ( len(module_data.classes) > 0 ):

# 			for class_data in module_data.classes:
			
# 				self._GenerateClassDocumentation( class_data, module_out_path )
			
# 			# for class_data in module_data.classes

# 		# if ( len(module_data.classes) > 0 )

# 	# def GenerateModuleDocumentation ( self, module_data, output_directory_path )

# 	# --------------------------------------------------

# 	def _GenerateModuleHomeDocumentation ( self, module_data, output_directory_path ):

# 		output_filepath = output_directory_path + FileSystem.FilePath( 'Home.md' )
# 		if ( output_filepath.IsNotAFile() ):
# 			output_filepath.Create()

# 		with output_filepath.Open( 'w' ) as file:

# 			# Write the module title
# 			file.write( '## Module __' )
# 			file.write( GitlabGenerator.PrettyName(module_data.name) )
# 			file.write( '__\n' )
# 			file.write( '\n' )

# 			# Write the module import
# 			file.write( 'import ' )
# 			file.write( GitlabGenerator.PrettyName(module_data.namespace) )
# 			file.write( '\n' )

# 			# Line separator
# 			file.write( '\n' )
# 			file.write( '----\n' )
# 			file.write( '\n' )

# 			# Write the documentation
# 			file.write( GitlabGenerator.PrettyDoc( module_data.documentation ) )
# 			file.write( '\n' )
# 			file.write( '\n' )

# 			if ( len(module_data.functions) > 0 ):

# 				# Line separator
# 				file.write( '----\n' )
# 				file.write( '\n' )

# 				# Write the functions
# 				file.write( '### Functions:\n' )
# 				file.write( '\n' )

# 				for function_data in module_data.functions:

# 					file.write( '- ' )
# 					self._GenerateFunctionSignature( function_data, file, False, False, True )
# 					file.write( '\n' )

# 				# for function_data in module_data.functions
# 				file.write( '\n' )
			
# 			# if ( len(module_data.functions) > 0 )
			
# 			if ( len(module_data.classes) > 0 ):

# 				# Line separator
# 				file.write( '----\n' )
# 				file.write( '\n' )

# 				# Write the functions
# 				file.write( '### Classes:\n' )
# 				file.write( '\n' )
			
# 				for class_data in module_data.classes:

# 					file.write( '- ' )
# 					file.write( GitlabGenerator.PrettyName(class_data.name) )
# 					file.write( '\n' )

# 				# for class_data in module_data.classes

# 				file.write( '\n' )

# 			# if ( len(module_data.classe) > 0 )

# 		# with output_filepath.Open( 'w' ) as file

# 	# def _GenerateModuleHomeDocumentation ( self, module_data, output_directory_path )
	
# 	# --------------------------------------------------

# 	def _GenerateModuleFunctionsDocumentation ( self, module_data, output_directory_path ):

# 		output_filepath = output_directory_path + FileSystem.FilePath( 'Functions.md' )
# 		if ( output_filepath.IsNotAFile() ):
# 			output_filepath.Create()

# 		with output_filepath.Open( 'w' ) as file:

# 			# Write the module title
# 			file.write( '## Functions of module __' )
# 			file.write( GitlabGenerator.PrettyName(module_data.name) )
# 			file.write( '__\n' )
# 			file.write( '\n' )

# 			# Write the module import
# 			file.write( 'import ' )
# 			file.write( GitlabGenerator.PrettyName(module_data.namespace) )
# 			file.write( '\n' )
# 			file.write( '\n' )

# 			# Write the functions
# 			if ( len(module_data.functions) > 0 ):

# 				for function_data in module_data.functions:

# 					# Line separator
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Function heading
# 					file.write( '### ' )
# 					file.write( GitlabGenerator.PrettyName(module_data.namespace) )
# 					file.write( '.' )
# 					self._GenerateFunctionSignature( function_data, file, False, False, True )
# 					file.write( '\n' )
# 					file.write( '\n' )

# 					# Function python header
# 					file.write( '```python\n' )
# 					file.write( 'def ')
# 					self._GenerateFunctionSignature( function_data, file, True, True, False )
# 					file.write( '\n' )
# 					file.write( '```\n' )
# 					file.write( '\n' )

# 					# Function documentation
# 					file.write( GitlabGenerator.PrettyDoc(function_data.documentation) )
# 					file.write( '\n' )
# 					file.write( '\n' )

# 				# for function_data in module_data.functions
			
# 			# if ( len(module_data.functions) > 0 )
			
# 		# with output_filepath.Open( 'w' ) as file

# 	# def _GenerateModuleFunctionsDocumentation ( self, module_data, output_directory_path )

# 	# --------------------------------------------------

# 	def _GenerateFunctionSignature ( self, function_data, file, display_default=True, display_annotations=True, escape_underscore=False ):

# 		# Get info
# 		parameters_count = len(function_data.parameters)

# 		# Function name
# 		if ( escape_underscore ):
# 			file.write( GitlabGenerator.PrettyName(function_data.name) )
# 		else:
# 			file.write( function_data.name )

# 		# Begin parameters list
# 		file.write( ' (' )	

# 		# Add a space ?
# 		if ( parameters_count > 0 ):
# 			file.write( ' ' )

# 		for parameter_index in range(parameters_count):

# 			# Get the parameter
# 			parameter_data = function_data.parameters[ parameter_index ]

# 			# Write the identifier
# 			file.write( parameter_data.name )

# 			# Write the annotation ?
# 			if ( display_annotations and parameter_data.hasAnnotation ):

# 				file.write( ':' )
				
# 				# Add simple quotes around str values
# 				if ( type(parameter_data.annotation) == str ):
# 					file.write( '\'' )
# 					file.write( parameter_data.annotation )
# 					file.write( '\'' )

# 				# Otherwise just write the value
# 				else:
# 					file.write( str(parameter_data.annotation) )

# 			# Write the default value ?
# 			if ( display_default and parameter_data.hasDefault ):

# 				file.write( '=' )

# 				# Add simple quotes around str values
# 				if ( type(parameter_data.default) == str ):
# 					file.write( '\'' )
# 					file.write( parameter_data.default )
# 					file.write( '\'' )

# 				# Otherwise just write the value
# 				else:
# 					file.write( str(parameter_data.default) )

# 			# Write comma ?
# 			if ( parameter_index < parameters_count - 1 ):

# 				file.write( ', ' )

# 		# for parameter_index in range(parameters_count)

# 		# Add a space ?
# 		if ( parameters_count > 0 ):
# 			file.write( ' ' )

# 		# End parameters list
# 		file.write( ')' )

# 	# def _GenerateFunctionSignature ( self, function_data, file )

# 	# --------------------------------------------------

# 	def _GenerateClassDocumentation( self, class_data, module_out_path ):

# 		# path of the class doc file
# 		filepath = module_out_path + FileSystem.FilePath.Format( '{}.md', class_data.name )

# 		# Create it if needed
# 		if ( filepath.IsNotAFile() ):
# 			filepath.Create()

# 		# Open it
# 		with filepath.Open( 'w' ) as file:

# 			# Write the class heading
# 			file.write( '## Class ' )
# 			file.write( GitlabGenerator.PrettyName(class_data.name) )
# 			file.write( '\n' )
# 			file.write( '\n' )

# 			# Write the documentation string
# 			file.write( GitlabGenerator.PrettyDoc(class_data.documentation) )
# 			file.write( '\n' )
# 			file.write( '\n' )

# 			# Generate the doc for the constructor ?
# 			if ( class_data.constructor ):

# 				# Write section heading
# 				file.write( '### Constructor\n' )
# 				file.write( '\n' )

# 				# Write line
# 				file.write( '----\n' )
# 				file.write( '\n' )

# 				# Write function doc
# 				self._GenerateClassFunctionDocumentation( class_data.name, class_data.constructor, file )
# 				file.write( '\n' )

# 			# if ( class_data.constructor )
			
# 			# Generate the doc for the constructor ?
# 			if ( class_data.properties ):

# 				# Write section heading
# 				file.write( '### Properties\n' )
# 				file.write( '\n' )

# 				for property_data in class_data.properties:

# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassPropertyDocumentation( class_data.name, property_data, file )
# 					file.write( '\n' )

# 				# for property_data in class_data.properties

# 			# if ( class_data.properties )

# 			# Generate the doc for the methods ?
# 			if ( class_data.methods ):

# 				# Write section heading
# 				file.write( '### Methods\n' )
# 				file.write( '\n' )

# 				for method in class_data.methods:
# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassFunctionDocumentation( class_data.name, method, file )
# 					file.write( '\n' )

# 				# for method in class_data.methods

# 			# if ( class_data.methods )

# 			# Generate the doc for the class methods ?
# 			if ( class_data.classMethods ):

# 				# Write section heading
# 				file.write( '### Class Methods\n' )
# 				file.write( '\n' )

# 				for method in class_data.classMethods:
# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassFunctionDocumentation( class_data.name, method, file )
# 					file.write( '\n' )

# 				# for method in class_data.methods

# 			# if ( class_data.classMethods )
			
# 			# Generate the doc for the static methods ?
# 			if ( class_data.staticMethods ):

# 				# Write section heading
# 				file.write( '### Static Methods\n' )
# 				file.write( '\n' )

# 				for method in class_data.staticMethods:
# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassFunctionDocumentation( class_data.name, method, file )
# 					file.write( '\n' )

# 				# for method in class_data.staticMethods

# 			# if ( class_data.staticMethods )
			
# 			# Generate the doc for the static fields ?
# 			if ( class_data.staticFields ):

# 				# Write section heading
# 				file.write( '### Static Fields\n' )
# 				file.write( '\n' )

# 				for field_data in class_data.staticFields:

# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassFieldsDocumentation( class_data.name, field_data, file )
# 					file.write( '\n' )

# 				# for field_data in class_data.staticFields

# 			# if ( class_data.properties )

# 			# Generate the doc for the operators ?
# 			if ( class_data.operators ):

# 				# Write section heading
# 				file.write( '### Operators\n' )
# 				file.write( '\n' )

# 				for operator in class_data.operators:
# 					# Write line
# 					file.write( '----\n' )
# 					file.write( '\n' )

# 					# Write function doc
# 					self._GenerateClassFunctionDocumentation( class_data.name, operator, file )
# 					file.write( '\n' )

# 				# for operator in class_data.operators

# 			# if ( class_data.operators )

# 		# with filepath.Open( 'w' ) as file

# 	# def _GenerateClassDocumentation( self, class_data, module_out_path )

# 	# --------------------------------------------------

# 	def _GenerateClassFunctionDocumentation ( self, class_name, function_data, file ):

# 		# Write heading
# 		file.write( '#### ' )
# 		file.write( GitlabGenerator.PrettyName(class_name) )
# 		file.write( '.' )
# 		self._GenerateFunctionSignature( function_data, file, False, False, True )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 		# Begin python code block
# 		file.write( '```python\n' )
# 		file.write( 'def ' )
# 		self._GenerateFunctionSignature( function_data, file, True, True, False )
# 		file.write( '\n' )
# 		file.write( '```\n' )
# 		file.write( '\n' )

# 		# Write doc
# 		file.write( GitlabGenerator.PrettyDoc(function_data.documentation) )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 	# def _GenerateFunctionDocumentation ( self, function_data, file )

# 	# --------------------------------------------------

# 	def _GenerateClassPropertyDocumentation( self, class_name, property_data, file ):
		
# 		# Write heading
# 		file.write( '#### ' )
# 		file.write( GitlabGenerator.PrettyName(class_name) )
# 		file.write( '.' )
# 		file.write( GitlabGenerator.PrettyName(property_data.name) )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 		# Begin python code block
# 		file.write( '```python\n' )
# 		file.write( class_name )
# 		file.write( '.' )
# 		file.write( property_data.name )
# 		file.write( '\n' )
# 		file.write( '```\n' )
# 		file.write( '\n' )

# 		# Write doc
# 		file.write( GitlabGenerator.PrettyDoc( property_data.documentation ) )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 	# def _GenerateClassPropertyDocumentation( self, class_name, property_data, file )

# 	# --------------------------------------------------

# 	def _GenerateClassFieldsDocumentation( self, class_name, field_data, file ):
		
# 		# Write heading
# 		file.write( '#### ' )
# 		file.write( GitlabGenerator.PrettyName(class_name) )
# 		file.write( '.' )
# 		file.write( GitlabGenerator.PrettyName(field_data.name) )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 		# Begin python code block
# 		file.write( '```python\n' )
# 		file.write( class_name )
# 		file.write( '.' )
# 		file.write( field_data.name )
# 		file.write( '\n' )
# 		file.write( '```\n' )
# 		file.write( '\n' )

# 		# Write doc
# 		file.write( GitlabGenerator.PrettyDoc( field_data.documentation ) )
# 		file.write( '\n' )
# 		file.write( '\n' )

# 	# def _GenerateClassPropertyDocumentation( self, class_name, field_data, file )

# 	# --------------------------------------------------

# 	@staticmethod
# 	def PrettyDoc ( text ):

# 		# split the line
# 		lines = text.splitlines()
		
# 		# Lines can be added so we save lines in a new list
# 		results = []

# 		# Go trough each line
# 		in_list = False
# 		for line in lines:

# 			# If it a heading
# 			if ( line.endswith( ':' ) ):

# 				# We are in a heading
# 				in_list = True

# 				# Add the heading and empty lines before and after
# 				results.append( '' )
# 				results.append( '##### ' + line )
# 				results.append( '' )

# 			# This line is empty ?
# 			elif ( not line ):

# 				# It ends the previous section
# 				in_list = False

# 				# Save the empty line
# 				results.append( '' )

# 			# If we are in a list
# 			elif ( in_list ):
				
# 				# add a minus before the line
# 				results.append( '- ' + line )

# 			# Otherwise
# 			else:
# 				# Add the line as is
# 				results.append( line )

# 		# while ( index < count )

# 		return '\n'.join( results )

# 	# def PrettyDoc ( text )

	# --------------------------------------------------

	@staticmethod
	def PrettyName ( name ):

		name = name.replace( '__', '\\_\\_' )
		return name

	# def PrettyName ( name )

# class GitlabGenerator ( Generator )