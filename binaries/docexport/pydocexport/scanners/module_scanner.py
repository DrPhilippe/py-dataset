# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import importlib.util
import inspect
import sys

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .. import data
from .module_scanner_settings import ModuleScannerSettings
from .scanner                 import Scanner

# ##################################################
# ###            CLASS MODULE-SCANNER            ###
# ##################################################

class ModuleScanner ( Scanner ):
	"""
	The `docexport.scanners.ModuleScanner` is used to scan a python module.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:ModuleScanner, settings:ModuleScannerSettings, **kwargs ):
		"""
		Initializes a new instance of the `ModuleScanner` class.

		Arguments:
			self             (`docexport.scanners.ModuleScanner`): Instance to initialize.
			settings (`docexport.scanners.ModuleScannerSettings`): Settings to use.
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleScanner )
		assert pytools.assertions.type_is_instance_of( settings, ModuleScannerSettings )

		super( ModuleScanner, self ).__init__( settings, **kwargs )

		self._module_data = None

	# def __init__ ( self, settings, **kwargs )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	@property
	def module_data ( self:ModuleScanner ) -> typing.Optional[data.ModuleData]:
		"""
		Data of the scanned module, or `None`
		"""
		return self._module_data
	
	# def module_data ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self:ModuleScanner ):
		"""
		Runs this module scanner.

		Arguments:
			self (`docexport.scanners.ModuleScanner`): Module scanner to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleScanner )

		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# log settings
		if self.logger:
			self.logger.log( '##################################################' )
			self.logger.log( 'MODULE SCANNER' )
			self.logger.end_line()
			self.logger.log( 'Settings:' )
			self.logger.log( '    module_path: {}', self.settings.module_path )
			self.logger.log( '    namespace: {}', self.settings.namespace )
			self.logger.log( '    ignores: {}', self.settings.ignores )
			self.logger.end_line()
			self.logger.flush()

		# Fetch module path
		module_path = self.settings.module_path

		# Add the module directory to the path
		# csys.path.append( str(module_path.parent()) )

		# Check the .py file exists
		if module_path.is_not_a_file():
			raise IOError( 'The file {} does not exist'.format(module_path) )

		# Get module name and compose module namespace
		module_name = module_path.filename()		
		if self.settings.namespace:
			module_namespace = self.settings.namespace + '.' + module_name
		else:
			module_namespace = module_name
		
		# Log the module infos
		if self.logger:
			self.logger.log( 'Module Infos:' )
			self.logger.log( '    path: {}', module_path )
			self.logger.log( '    name: {}', module_name )
			self.logger.log( '    namespace: {}', module_namespace )
			self.logger.end_line()
			self.logger.flush()

		# Get the module import specifications
		# print( 'spec_from_file_location(', module_namespace, ',', str(module_path), ')' )
		module_spec = importlib.util.spec_from_file_location( module_namespace, str(module_path) )

		# Check the module import specifications
		if module_spec is None:
			if self.logger:
				self.logger.log( 'Module Specs: ERROR' )
				self.logger.flush()
			
			raise RuntimeError( 'Could not create module specification for module naned {} from file {}'.format(
					module_name,
					module_path
				))

		if self.logger:
			self.logger.log( 'Module Specs: OK' )
			self.logger.flush()

		# Get the module
		module = importlib.util.module_from_spec( module_spec )

		# Check the module
		if module is None:
			if self.logger:
				self.logger.log( 'Get Module: ERROR' )
				self.logger.flush()

			raise RuntimeError( 'Could not get module from module specifications' )

		if self.logger:
			self.logger.log( 'Get Module: OK' )
			self.logger.flush()

		# Compile/import the module
		module_spec.loader.exec_module( module )
		
		if self.logger:
			self.logger.log( 'Module importation: OK' )
			self.logger.flush()

		self._module_data = data.ModuleData(
			         name = module_name,
			    namespace = module_namespace,
			documentation = inspect.cleandoc(module.__doc__) if module.__doc__ else '',
			    functions = [],
			      classes = []
			)

		# Scan the contents of the module
		module_keys       = list(module.__dict__.keys())
		module_keys_count = len(module_keys)

		# Create sub trackers
		sub_trackers = [None] * module_keys_count
		if self.progress_tracker:
			for module_key_index in range(module_keys_count):
				sub_trackers[ module_key_index ] = self.progress_tracker.create_child()

		# Scan the contents of the module
		for module_key_index in range( module_keys_count ):

			module_key   = module_keys[ module_key_index ]
			module_entry = module.__dict__[ module_key ]
			sub_tracker  = sub_trackers[ module_key_index ]
			
			if sub_tracker:
				sub_tracker.update(0.0)

			# If this entry shoud not be ignored
			if module_key not in self.settings.ignores:

				# This is a function
				if inspect.isfunction( module_entry ):
					pass
					# # create a function scanner
					# function_scanner = FunctionScanner.Create()

					# # Scan the function and its parameters
					# function_data = function_scanner.Scan(
					# 	function = module_entry
					# 	)

					# # Add the function data to the module data
					# self._module_data.functions.append( function_data )

				# This is a class
				elif inspect.isclass( module_entry ):
					pass
					# # Create a class scanner
					# class_scanner = ClassScanner.Create()
					
					# # Scan the class
					# class_data = class_scanner.Scan(
					# 	cls = module_entry
					# 	)

					# # Add the function data to the module data
					# self._module_data.classes.append( class_data )

				# if inspect.isfunction( module_entry )
			
			# if module_key not in self.settings.ignores:

			if sub_tracker:
				sub_tracker.update(1.0)

		# for module_key_index in range( module_keys_count )

		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

	# def Scan ( self, module_path, progress_tracker )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a module scanner.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`docexport.scanner.ModuleScannerSettings`: The settings.
		"""
		return ModuleScannerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class ModuleScanner ( Scaner )
