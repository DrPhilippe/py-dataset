# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from .scope_scanner_settings import ScopeScannerSettings

# ##################################################
# ###       CLASS PACKAGE-SCANNER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PackageScannerSettings',
	   namespace = 'docexport.scanners',
	fields_names = [
		'package_path'
		]
	)
class PackageScannerSettings ( ScopeScannerSettings ):
	"""
	Settings used to configure scanners.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self : PackageScannerSettings,
		package_path : typing.Union[pytools.path.DirectoryPath, str] = pytools.path.DirectoryPath(),
		namespace : str = '',
		ignores : list[str] = [ '__init__' ],
		**kwargs
		):
		"""
		Insitializes a new instance of the `PackageScannerSettings` class.

		Arguments:
			self (`docexport.scanners.PackageScannerSettings`): The instance to initialize.
			package_path        (`pytools.path.DirectoryPath`): Path of the directory containing the package to scan.
			namespace                                  (`str`): Namespace in which the package to scan is defined.
			ignores                          (`list` of `str`): Named of modules file to ignore in this package.
		Named Arguments:
			see `docexport.scanners.ScannerSettings`
			see `pytools.tasks.TaskSettings`
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageScannerSettings )
		assert pytools.assertions.type_is_instance_of( package_path, (str, pytools.path.DirectoryPath) )
		assert pytools.assertions.type_is( namespace, str )

		# Init parent class
		super( PackageScannerSettings, self ).__init__( namespace, ignores, **kwargs )

		# Set properties
		if isinstance( package_path, str ):
			self._package_path = pytools.path.DirectoryPath( package_path )
		else:
			self._package_path = package_path

	# def __init__ ( self, package_path, namespace, ignored_modules_names, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def package_path ( self:PackageScannerSettings ) -> pytools.path.DirectoryPath:
		"""
		Path of the directory containing the package to scan (`pytools.path.DirectoryPath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageScannerSettings )
		
		return self._package_path

	# def package_path ( self )

	# --------------------------------------------------

	@package_path.setter
	def package_path ( self:PackageScannerSettings, value:pytools.path.DirectoryPath ):
		assert pytools.assertions.type_is_instance_of( self, PackageScannerSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.DirectoryPath) )

		self._package_path = pytools.path.DirectoryPath(value) if isinstance(value, str) else value

	# def package_path ( self, value )

# class PackageScannerSettings( ScannerSettings )