# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import importlib.util
import inspect
import sys

# INTERNALS
import pytools.assertions
import pytools.path
import pytools.serialization

# LOCALS
from .. import data
from .module_scanner           import ModuleScanner
from .package_scanner_settings import PackageScannerSettings
from .scanner                  import Scanner

# ##################################################
# ###           CLASS PACKAGE-SCANNER            ###
# ##################################################

class PackageScanner ( Scanner ):
	"""
	Class used to scan python packages members.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:PackageScanner, settings:PackageScannerSettings, **kwargs ):
		"""
		Initializes a new instance of the `docexport.scanners.PackageScanner` class.

		Arguments:
			self             (`docexport.scanners.PackageScanner`): Instance to initialize.
			settings (`docexport.scanners.PackageScannerSettings`): Settings of the package scanner.

		Named Arguments:
			see `docexport.scanners.Scanner`.
			see `pytools.tasks.Task`.
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageScanner )
		assert pytools.assertions.type_is_instance_of( settings, PackageScannerSettings )

		super( PackageScanner, self ).__init__( settings, **kwargs )

		self._package_data = None

	# def __init__ ( self, settings, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	@property
	def package_data ( self:PackageScanner ) -> typing.Optional[data.PackageData]:
		"""
		Data of the scanned package, or `None`
		"""
		return self._package_data
	
	# def package_data ( self )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def run ( self:PackageScanner ):
		"""
		Run this scanner.

		Arguments:
			self (`docexport.scanners.PackageScanner`): Scanner to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageScanner )

		# reset progress
		if self.progress_tracker:
			self.progress_tracker.update( 0.0 )

		# log settings
		if self.logger:
			self.logger.log( '##################################################' )
			self.logger.log( 'PACKAGE SCANNER' )
			self.logger.end_line()
			self.logger.log( 'Settings:' )
			self.logger.log( '    package_path: \'{}\'', self.settings.package_path )
			self.logger.log( '    namespace:    \'{}\'', self.settings.namespace )
			self.logger.log( '    ignores:      \'{}\'', self.settings.ignores )
			self.logger.end_line()

		# Fetch the package path
		package_path = self.settings.package_path

		# Add the package parent directory to the path
		sys.path.append( str(package_path.parent()) )

		# Check that the package directory exists
		if package_path.is_not_a_directory():
			raise IOError( 'The directory {} does not exist'.format(package_path) )

		# Get the package
		package_name = package_path.name()
		
		# Decifer the package namespace
		if self.settings.namespace:
			package_namespace = self.settings.namespace + '.' + package_name
		else:
			package_namespace = package_name

		# Log the package path and name
		if self.logger:
			self.logger.log( 'Package:' )
			self.logger.log( '    path:      \'{}\'', package_path )
			self.logger.log( '    name:      \'{}\'', package_name )
			self.logger.log( '    namespace: \'{}\'', package_namespace )
			self.logger.end_line()

		# Create the path to the __init__.py file
		module_path  = package_path + pytools.path.FilePath( '__init__.py' )

		# Check the __init__.py file exists
		if module_path.is_not_a_file():
			raise IOError( 'The file {} does not exist'.format(module_path) )

		# Log the module path
		if self.logger:
			self.logger.log( 'package __init__.py module path: \'{}\'', module_path )

		# Get the package import specifications
		# print( 'spec_from_file_location(', package_namespace, ',', str(module_path), ')' )
		pakage_spec = importlib.util.spec_from_file_location( package_namespace, str(module_path) )

		# Check the package import specifications
		if pakage_spec is None:
			raise RuntimeError( 'Could not create package specification for package {} from file {}'.format(
				package_namespace,
				module_path
				))

		# Log the package spec status
		if self.logger:
			self.logger.log( 'package spec created' )

		# Get the package
		package = importlib.util.module_from_spec( pakage_spec )

		# Check the package
		if package is None:
			raise RuntimeError( 'Could not load package {} __init__.py module from file {}'.format(
				package_namespace,
				module_path
				))

		# Log the package status
		if self.logger:
			self.logger.log( 'package __init__.py module: OK')

		# Compile/import the package
		pakage_spec.loader.exec_module( package )
		
		# Log the package import status
		if self.logger:
			self.logger.log( 'package successfully imported' )
			self.logger.end_line()

		# Create the package info
		self._package_data = data.PackageData(
			         name = package_name,
			    namespace = package_namespace,
			documentation = inspect.cleandoc(package.__doc__) if package.__doc__ else '',
			 sub_packages = [],
			      modules = []
			)

		# Scan the contents of the package
		package_contents_paths = package_path.list_contents()
		package_contents_count = len(package_contents_paths)

		# Log the package import status
		if self.logger:
			self.logger.log( 'Scanning contents:' )

		# Create sub trackers
		sub_trackers = [None] * package_contents_count
		if self.progress_tracker:
			for package_content_index in range(package_contents_count):
				sub_trackers[ package_content_index ] = self.progress_tracker.create_child()

		# Scan the contents of the package
		for package_content_index in range(package_contents_count):

			package_content_path = package_contents_paths[ package_content_index ]
			sub_tracker          = sub_trackers[ package_content_index ]

			if sub_tracker:
				sub_tracker.update(0.0)

			# Is it a sub-pacakge ?
			if package_content_path.is_a_directory()\
			and package_content_path.name() not in self.settings.ignores:

				# Compose the path to ites init file
				package_content_init_path = package_content_path + pytools.path.FilePath( '__init__.py' )

				# if the init exists then its truely a package
				if package_content_init_path.is_a_file():

					log_filename = 'logs/docexport.{}.log'.format(
						str(package_content_path).replace( '/', '.' )
						)

					# Create the sub-package scanner
					subpackage_scanner = PackageScanner.create(
						progress_tracker = sub_tracker,
						          logger = pytools.tasks.file_logger( log_filename ),
						    package_path = package_content_path,
						       namespace = package_namespace,
						            name = 'sub-package-scanner'
						)

					# Run it
					subpackage_scanner.run()

					# Add the sub-package info to this package
					self._package_data.sub_packages.append( subpackage_scanner.package_data )

					if self.logger:
						self.logger.log( '    scanned package {}, see log at {}', package_content_path, log_filename )

				# if package_content_init_path.is_a_file()

			# Is it a python module file ?
			elif package_content_path.is_a_file()\
			 and package_content_path.extension() == '.py'\
			 and package_content_path.filename() not in self.settings.ignores:

				log_filename = 'logs/docexport.{}.log'.format(
					str(package_content_path).replace( '/', '.' )
					)

				# Create the sub-package scanner
				submodule_scanner = ModuleScanner.create(
					progress_tracker = sub_tracker,
					          logger = pytools.tasks.file_logger( log_filename ),
					     module_path = package_content_path,
					       namespace = package_namespace,
					            name = 'sub-module-scanner'
					)

				# Run it
				submodule_scanner.run()

				# Add the sub-module info to this package
				self._package_data.modules.append( submodule_scanner.module_data )

				if self.logger:
					self.logger.log( '    scanned module {}, see log at {}', package_content_path, log_filename )

			else:
				if self.logger:
					self.logger.log( '    ignored enty {}', package_content_path )


			# if package_content_path.is_a_directory()

			if sub_tracker:
				sub_tracker.update(1.0)

		# for package_content_index in range(package_contents_count)

		if self.progress_tracker:
			self.progress_tracker.update( 1.0 )

		# Log the package status
		if self.logger:
			self.logger.log( '    Done!' )
			self.logger.end_line()

	# def run ( self )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################
	
	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a package scanner.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`docexport.scanner.PackageScannerSettings`: The settings.
		"""
		return PackageScannerSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

# class PackageScanner ( Scanner )