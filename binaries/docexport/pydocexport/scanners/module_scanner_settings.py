# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# LOCALS
from .scope_scanner_settings import ScopeScannerSettings

# ##################################################
# ###       CLASS MODULE-SCANNER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ModuleScannerSettings',
	   namespace = 'docexport.scanners',
	fields_names = [
		'module_path'
		]
	)
class ModuleScannerSettings ( ScopeScannerSettings ):
	"""
	Settings used to configure a module scanners.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self : ModuleScannerSettings,
		module_path : typing.Union[pytools.path.FilePath, str] = pytools.path.FilePath(),
		namespace : str = '',
		ignores : list[str] = ['__doc__', '__name__', '__package__', '__loader__', '__spec__', '__file__', '__cached__', '__builtins__'],
		**kwargs
		):
		"""
		Insitializes a new instance of the `ModuleScannerSettings` class.

		Arguments:
			self (`docexport.scanners.ModuleScannerSettings`): Instance to initialize.
			module_path             (`pytools.path.FilePath`): Path of the module file to scan.
			namespace                                 (`str`): Namespace in which the module to scan is defined.
			ignores                         (`list` of `str`): Names of module items to ignore.
	
		Named Arguments:
			see `docexport.scanners.ScannerSettings`
			see `pytools.tasks.TaskSettings`
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleScannerSettings )
		assert pytools.assertions.type_is_instance_of( module_path, (str, pytools.path.FilePath) )
		assert pytools.assertions.type_is( namespace, str )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		if isinstance( module_path, str ):
			module_path = pytools.path.FilePath( module_path )

		super( ModuleScannerSettings, self ).__init__( namespace, ignores, **kwargs )

		self._module_path = module_path

	# def __init__ ( self, module_path, namespace, name )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def module_path ( self:ModuleScannerSettings ) -> pytools.path.FilePath:
		"""
		Path of the module file to scan (`pytools.path.FilePath`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleScannerSettings )
		
		return self._module_path

	# def module_path ( self )

	# --------------------------------------------------

	@module_path.setter
	def module_path ( self:ModuleScannerSettings, value:pytools.path.FilePath ):
		assert pytools.assertions.type_is_instance_of( self, ModuleScannerSettings )
		assert pytools.assertions.type_is_instance_of( value, (str, pytools.path.FilePath) )

		self._module_path = pytools.path.FilePath(value) if isinstance(value, str) else value

	# def module_path ( self, value )

# class ModuleScannerSettings( ScannerSettings )