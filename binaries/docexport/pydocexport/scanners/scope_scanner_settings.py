# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .scanner_settings import ScannerSettings

# ##################################################
# ###        CLASS SCOPE-SCANNER-SETTINGS        ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ScopeScannerSettings',
	   namespace = 'docexport.scanners',
	fields_names = [
		'namespace',
		'ignores'
		]
	)
class ScopeScannerSettings ( ScannerSettings ):
	"""
	Settings used to configure scope scanners.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:ScopeScannerSettings, namespace:str='', ignores:list[str]=[], **kwargs ):
		"""
		Insitializes a new instance of the `ScopeScannerSettings` class.

		Arguments:
			self (`docexport.scanners.ScopeScannerSettings`): The instance to initialize.
			namespace                                (`str`): Namespace in which the scope to scan is defined.
			ignores                        (`list` of `str`): Names of members inside the scope to ignore.
		
		Named Arguments:
			see `docexport.scanners.ScannerSettings`
			see `pytools.tasks.TaskSettings`
		"""
		assert pytools.assertions.type_is_instance_of( self, ScopeScannerSettings )
		assert pytools.assertions.type_is( namespace, str )

		super( ScopeScannerSettings, self ).__init__( **kwargs )

		self._namespace = namespace
		self._ignores   = copy.deepcopy( ignores )

	# def __init__ ( self, namespace )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def namespace ( self:ScopeScannerSettings ) -> str:
		"""
		Namespace in which the scope to scan is defined (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ScopeScannerSettings )
		
		return self._namespace

	# def namespace ( self )

	# --------------------------------------------------

	@namespace.setter
	def namespace ( self:ScopeScannerSettings, value:str ):
		assert pytools.assertions.type_is_instance_of( self, ScopeScannerSettings )
		assert pytools.assertions.type_is( value, str )

		self._namespace = value

	# def namespace ( self, value )
	
	# --------------------------------------------------

	@property
	def ignores ( self:ScopeScannerSettings ) -> list[str]:
		"""
		Names of members inside the scope to ignore (`list` of `str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ScopeScannerSettings )

		return self._ignores

	# def ignores ( self )

	# --------------------------------------------------

	@ignores.setter
	def ignores ( self:ScopeScannerSettings, value:list[str] ):
		assert pytools.assertions.type_is_instance_of( self, ScopeScannerSettings )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, str )

		self._ignores = copy.deepcopy( value )
		
	# def ignores ( self, value )

# class ScopeScannerSettings( ScannerSettings )