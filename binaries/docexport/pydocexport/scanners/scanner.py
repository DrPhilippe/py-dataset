# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.tasks

# LOCALS
from .scanner_settings import ScannerSettings

# ##################################################
# ###               CLASS SCANNER                ###
# ##################################################

class Scanner ( pytools.tasks.Task ):
	"""
	Base class for scanners.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:Scanner, settings:ScannerSettings, **kwargs ):
		"""
		Insitializes a new instance of the `docexport.scanners.Scanner` class.

		Arguments:
			self             (`docexport.scanners.Scanner`): Instance to initialize.
			settings (`docexport.scanners.ScannerSettings`): Settings of the scanner.

		Named Arguments:
			see `pytools.tasks.Task`.
		"""
		assert pytools.assertions.type_is_instance_of( self, Scanner )
		assert pytools.assertions.type_is_instance_of( settings, ScannerSettings )

		super( Scanner, self ).__init__( settings, **kwargs )

	# def __init__ ( self, settings, **kwargs )

# class Scanner ( pytools.tasks.Task )