# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###           CLASS SCANNER-SETTINGS           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ScannerSettings',
	   namespace = 'docexport.scanners',
	fields_names = []
	)
class ScannerSettings ( pytools.tasks.TaskSettings ):
	"""
	Base class for scanners settings.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:ScannerSettings, **kwargs ):
		"""
		Insitializes a new instance of the `docexport.scanners.Scanner` class.

		Arguments:
			self (`docexport.scanners.ScannerSettings`): Instance to initialize.

		Named Arguments:
			see `pytools.tasks.TaskSettings` for more details.
		"""
		assert pytools.assertions.type_is_instance_of( self, ScannerSettings )

		super( ScannerSettings, self ).__init__( **kwargs )

	# def __init__ ( self, **kwargs )

# class ScannerSettings ( pytools.tasks.TaskSettings )