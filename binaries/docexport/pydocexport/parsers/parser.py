# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
from __future__ import annotations
import copy
import typing

# INTERNAL
import pytools.assertions
import pytools.tasks

# LOCALS
from .. import data
from .parser_settings import ParserSettings

# ##################################################
# ###                CLASS PARSER                ###
# ##################################################

class Parser ( pytools.tasks.Task ):
	"""
	Base class for custom parsers.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:Parser, package_data:data.PackageData, settings:ParserSettings, **kwargs ):
		"""
		Initializes a new instance of the `docexport.parsers.Parser` class.

		Arguments:
			self             (`docexport.parsers.Parser`): Parser instance to initialize.
			package_data   (`docexport.data.PackageData`): Package to parse.
			settings (`docexport.parsers.ParserSettings`): Settings of the parser.

		Named Arguments:
			see `pytools.tasks.Task`
		"""
		assert pytools.assertions.type_is_instance_of( self, Parser )
		assert pytools.assertions.type_is_instance_of( package_data, data.PackageData )
		assert pytools.assertions.type_is_instance_of( settings, ParserSettings )

		super( Parser, self ).__init__( settings, **kwargs )

		self._package_data = copy.deepcopy( package_data )

	# def __init__ ( self, package_data, settings, **kwargs )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################
	
	# --------------------------------------------------

	@property
	def package_data ( self:Parser ) -> data.PackageData:
		"""
		Parsed package data (`docexport.data.PackageData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, Parser )

		return self._package_data
	
	# def package_data ( self:Parser )
	
	# --------------------------------------------------

	@package_data.setter
	def package_data ( self:Parser, value:data.PackageData ):
		assert pytools.assertions.type_is_instance_of( self, Parser )
		assert pytools.assertions.type_is_instance_of( value, data.PackageData )
		
		self._package_data = copy.deepcopy( value )
	
	# def package_data ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def parse_package ( self:Parser, package_data:data.PackageData, ) -> data.PackageData:
		"""
		Parse a python package using this parser.

		Arguments:
			self           (`doxexport.parsers.Parser`): The parser used for parsing.
			package_data (`docexport.data.PackageData`): The package parent of the package to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, Parser )
		assert pytools.assertions.type_is_instance_of( package_data, data.PackageData )
		
		raise NotImplementedError()

	# def parse_package ( self, package_data, progress_tracker )

	# --------------------------------------------------

	def run ( self:Parser ):
		"""
		Runs this parser.
		
		Arguments:
			self (`docexport.parsers.Parser`): Parser to run.
		"""
		assert pytools.assertions.type_is_instance_of( self, Parser )

		self._package_data = self.parse_package( self.settings.package_data )

	# def run ( self:Parser )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create (
		cls : type,
		package_data : data.PackageData,
		logger : typing.Optional[pytools.tasks.Logger] = None,
		progress_tracker : typing.Optional[pytools.tasks.ProgressTracker] = None,
		parent : typing.Optional[pytools.tasks.Task] = None,
		**kwargs
		):
		"""
		Creates a new parser instance of type `cls`.
		
		Arguments:
			cls                                              (`type`): The type of the parser.
			package_data               (`docexport.data.PackageData`): Data of the package to parse.
			logger                    (`None`/`pytools.tasks.Logger`): Logger used to log the activity of the parser.
			progress_tracker (`None`/`pytools.tasks.ProgressTracker`): Progress tracker used to report the progression of the parser.
			**kwargs                                         (`dict`): Arguments forwarded to the parser settings constructor.

		Returns:
			`pytools.tasks.Task`: The new task
		"""
		assert pytools.assertions.type_is( cls, type )
		assert pytools.assertions.type_is_instance_of( logger, (type(None), Logger) )
		assert pytools.assertions.type_is_instance_of( progress_tracker, (type(None), ProgressTracker) )

		# Create the settings for the task
		settings = cls.create_settings( **kwargs )

		# Create the task
		return cls( settings, logger, progress_tracker )

	# def create ( cls, logger, progress_tracker, **kwargs )

# class Parser ( pytools.tasks.Task )