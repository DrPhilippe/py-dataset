# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
from __future__ import annotations

# INTERNAL
import pytools.factory

# ##################################################
# ###      CLASS REGISTER-PARSER-ATTRIBUTE       ###
# ##################################################

class RegisterParserAttribute ( pytools.factory.RegisterClassAttribute ):
	"""
	Attribute used to register custom parsers.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------
	
	def __init__ ( self:RegisterParserAttribute, name:str ):
		"""
		Initializes a new instance of the `RegisterParserAttribute` class.
	
		Arguments:
			self (`docexport.parsers.RegisterParserAttribute`): The instance to initialize.
			name                                       (`str`): The name of the parser.
		"""
		assert pytools.assertions.type_is_instance_of( self, RegisterParserAttribute )
		assert pytools.assertions.type_is_instance_of( name, str )
		
		super( RegisterParserAttribute, self ).__init__(
			   group = 'parsers',
			typename = name.lower()
			)

	# def __init__ ( self, name )

# class RegisterParserAttribute ( pytools.factory.RegisterAttribute )