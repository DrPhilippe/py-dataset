# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
from __future__ import annotations

# INTERNAL
import pytools.assertions
import pytools.serialization
import pytools.tasks

# ##################################################
# ###           CLASS PARSER-SETTINGS            ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ParserSettings',
	   namespace = 'docexport.parsers',
	fields_names = []
	)
class ParserSettings ( pytools.tasks.TaskSettings ):
	"""
	The class `docexport.parsers.ParserSettings` is used to configure custom parsers.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:ParserSettings, name:str='parser' ):
		"""
		Intializes a new instance of the class `docexport.parsers.ParserSettings`.

		Arguments:
			self (`docexport.parsers.ParserSettings`): Instance to initialize.
			name                              (`str`): The name of the parser.
		"""
		assert pytools.assertions.type_is_instance_of( self, ParserSettings )
		assert pytools.assertions.type_is_instance_of( name, str )

		super( ParserSettings, self ).__init__( name )

	# def __init__ ( self, name )

# class ParserSettings ( pytools.serialization.Serializable )