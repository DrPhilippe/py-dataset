# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
from __future__ import annotations
import re

# INTERNAL
import pytools.assertions

# LOCALS
from .. import data
from .default_parser_settings   import DefaultParserSettings
from .parser                    import Parser
from .register_parser_attribute import RegisterParserAttribute

# ##################################################
# ###              CLASS A-PARSER                ###
# ##################################################

@RegisterParserAttribute( 'default-parser' )
class DefaultParser ( Parser ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self : DefaultParser,
		package_data : data.PackageData,
		settings : DefaultParserSettings = DefaultParserSettings(),
		**kwargs
		):
		"""
		Initializes a new instance of the `docexport.parsers.DefaultParser` class.

		Arguments:
			self             (`docexport.parsers.DefaultParser`): Parser instance to initialize.
			package_data          (`docexport.data.PackageData`): Package to parse.
			settings (`docexport.parsers.DefaultParserSettings`): Settings of the parser.

		Named Arguments:
			see `pytools.tasks.Task`
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( package_data, data.PackageData )
		assert pytools.assertions.type_is_instance_of( settings, DefaultParserSettings )

		super( DefaultParser, self ).__init__( package_data, settings, **kwargs )

	# def __init__ ( self, package_data, settings, **kwargs )
	
	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################
	
	# --------------------------------------------------

	def parse_package ( self:DefaultParser, package_data:data.PackageData, ) -> data.PackageData:
		"""
		Parse a python package data using this parser.

		Arguments:
			self    (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			package_data (`docexport.data.PackageData`): Package to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( package_data, data.PackageData )

		# Parse the documentation of the packge
		if package_data.documentation:
			
			# Remove indetnation
			package_data.documentation = DefaultParser.unindent_text( package_data.documentation )

		# if ( package_data.documentation )

		# Parse each sub module
		modules_count = len(package_data.modules)
		for module_index in range(modules_count):

			package_data.modules[ module_index ] = self.parse_module(
				package_data.modules[ module_index ]
				)
		
		# for module_index in range(modules_count)
		
		# Parse each module
		packages_count = len(package_data.packages)
		for package_index in range(packages_count):

			package_data.packages[ package_index ] = self.parse_package(
				package_data.packages[ package_index ]
				)
		
		# for package_index in range(packages_count)

		return package_data

	# def parse_package ( self, package_data )

	# --------------------------------------------------
	
	def parse_module ( self:DefaultParser, module_data:data.ModuleData ) -> data.ModuleData:
		"""
		Parse a python module data using this parser.

		Arguments:
			self  (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			module_data (`docexport.data.ModuleData`): Module data to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( module_data, data.ModuleData )

		# Parse the documentation of the module
		if module_data.documentation:
			
			# Remove indetnation
			module_data.documentation = DefaultParser.unindent_text( module_data.documentation )

		# if ( module_data.documentation )

		# Parse each class
		classes_count = len(module_data.classes)
		for class_index in range(classes_count):

			module_data.classes[ class_index ] = self.parse_class(
				module_data.classes[ class_index ]
				)
		
		# for class_index in range(classes_count)
		
		# Parse each function
		functions_count = len(module_data.functions)
		for function_index in range(functions_count):

			module_data.functions[ function_index ] = self.parse_function(
				module_data.functions[ function_index ]
				)
		
		# for function_index in range(functions_count)

		return module_data

	# def parse_module ( self, module_data )

	# --------------------------------------------------
	
	def parse_class ( self:DefaultParser, class_data:data.ClassData ) -> data.ClassData:
		"""
		Parse a python class data using this parser.

		Arguments:
			self (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			class_data  (`docexport.data.ClassData`): Class data to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( class_data, data.ClassData )
		
		# Parse the documentation of the module
		if class_data.documentation:

			# Remove indetnation
			class_data.documentation = DefaultParser.unindent_text( class_data.documentation )

		# if ( class_data.documentation )
		
		# Parse the constructor
		if ( class_data.constructor ):
			class_data.constructor = self.parse_function( class_data.constructor )

		# Parse each property
		properties_count = len(class_data.properties)
		for property_index in range(properties_count):

			class_data.properties[ property_index ] = self.parse_property(
				class_data.properties[ property_index ]
				)
		
		# for property_index in range(properties_count)
		
		# Parse each methods
		methods_count = len(class_data.methods)
		for method_index in range(methods_count):

			class_data.methods[ method_index ] = self.parse_function(
				function_data = class_data.methods[ method_index ]
				)
		
		# for method_index in range(methods_count)
		
		# Parse each class methods
		class_methods_count = len(class_data.classMethods)
		for class_method_index in range(class_methods_count):

			class_data.classMethods[ class_method_index ] = self.parse_function(
				function_data = class_data.classMethods[ class_method_index ]
				)
		
		# for class_method_index in range(class_methods_count)
		
		# Parse each static methods
		static_methods_count = len(class_data.staticMethods)
		for static_method_index in range(static_methods_count):

			class_data.staticMethods[ static_method_index ] = self.parse_function(
				function_data = class_data.staticMethods[ static_method_index ]
				)
		
		# for static_method_index in range(static_methods_count)
		
		# Parse each operators
		operators_count = len(class_data.operators)
		for operator_index in range(operators_count):

			class_data.operators[ operator_index ] = self.parse_function(
				function_data = class_data.operators[ operator_index ]
				)
		
		# for operator_index in range(operators_count)

		# Parse each static field
		static_fields_count = len(class_data.staticFields)
		for static_field_index in range(static_fields_count):

			class_data.staticFields[ static_field_index ] = self.parse_static_field(
				field_data = class_data.staticFields[ static_field_index ]
				)
		
		# for static_field_index in range(static_fields_count)

		return class_data

	# def ParseClass ( self, class_data )

	# --------------------------------------------------
	
	def parse_property ( self:DefaultParser, property_data:data.PropertyData ) -> data.PropertyData:
		"""
		Parse a python property data using this parser.

		Arguments:
			self      (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			property_data (`docexport.data.PropertyData`): Property data to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( property_data, data.PropertyData )
		
		# Parse the documentation of the module
		if property_data.documentation:

			# Remove indetnation
			property_data.documentation = DefaultParser.unindent_text( property_data.documentation )

		# if property_data.documentation

		return property_data
		
	# def parse_property ( self, property_data )
	
	# --------------------------------------------------
	
	def parse_static_field ( self:DefaultParser, field_data:data.StaticFieldData ) -> data.StaticFieldData:
		"""
		Parse a python property data using this parser.

		Arguments:
			self      (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			field_data (`docexport.data.StaticFieldData`): Static field data to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( field_data, data.StaticFieldData )
		
		# Parse the documentation of the module
		if field_data.documentation:

			# Remove indetnation
			field_data.documentation = DefaultParser.unindent_text( field_data.documentation )

		# if field_data.documentation

		return field_data
		
	# def parse_static_field ( self, field_data )

	# --------------------------------------------------
	
	def ParseFunction ( self:DefaultParser, function_data:data.FunctionData ) -> data.FunctionData:
		"""
		Parse a python property data using this parser.

		Arguments:
			self      (`doxexport.parsers.DefaultParser`): Parser used for parsing.
			function_data (`docexport.data.FunctionData`): Function data to parse.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParser )
		assert pytools.assertions.type_is_instance_of( function_data, data.FunctionData )
		
		# Parse the documentation of the module
		if function_data.documentation:

			# Remove indetnation
			function_data.documentation = DefaultParser.unindent_text( function_data.documentation )

		# if function_data.documentation

		return function_data
		
	# def ParseFunction ( self, function_data )

	# ##################################################
	# ###               CLASS-METHODS                ###
	# ##################################################

	# --------------------------------------------------

	@classmethod
	def create_settings ( cls, **kwargs ):
		"""
		Creates the settings for a default parser.
		
		Arguments:
			**kwargs (`dict`): Arguments forwarded to the settings constructor.

		Returns:
			`docexport.parsers.DefaultParserSettings`: The settings.
		"""
		return DefaultParserSettings( **kwargs )

	# def create_settings ( cls, **kwargs )

	# ##################################################
	# ###               STATIC-METHODS               ###
	# ##################################################
	
	# --------------------------------------------------

	@staticmethod
	def unindent_text ( text ):
			
		# Split each line and discard the new-line char
		lines = text.splitlines()
		
		# Trim each line
		for index in range(len(lines)):

			# Get the line
			line = lines[ index ]

			# Remove
			line = line.strip()

			# Remove duplicate spacings
			line = re.sub( r"\s\s+" , ' ',  line )

			# Set the line
			lines[ index ] = line

		return '\n'.join( lines )

	# def unindent_text ( text )

# class DefaultParser ( Parser )