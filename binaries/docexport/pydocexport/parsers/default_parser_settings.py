# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
from __future__ import annotations

# INTERNAL
import pytools.assertions
import pytools.serialization

# LOCALS
from .parser_settings import ParserSettings

# ##################################################
# ###        CLASS DEFAULT-PARSER-SETTINGS       ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'DefaultParserSettings',
	   namespace = 'docexport.parsers',
	fields_names = []
	)
class DefaultParserSettings ( ParserSettings ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name='default-parser' ):
		"""
		Intializes a new instance of the class `docexport.parsers.DefaultParserSettings`.

		Arguments:
			self (`docexport.parsers.DefaultParserSettings`): Instance to initialize.
			name                                     (`str`): The name of the parser.
		"""
		assert pytools.assertions.type_is_instance_of( self, DefaultParserSettings )
		assert pytools.assertions.type_is_instance_of( name, str )

		super( DefaultParserSettings, self ).__init__( name )

	# def __init__ ( self, name )

# class DefaultParserSettings ( ParserSettings )