# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .member_data import MemberData

# ##################################################
# ###           CLASS PARAMETER-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ParameterData',
	   namespace = 'docexport.data',
	fields_names = [
		'annotation',
		'has_annotation',
		'default_value',
		'has_default_value'
		]
	)
class ParameterData ( MemberData ):
	"""
	The class `docexport.data.ParameterData` is used to describe the parameters of a function or callable.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self:ParameterData,
		name:str='',
		documentation:str='',
		annotation=None,
		has_annotation:bool=False,
		default_value=None,
		has_default_value:bool=False
		):
		"""
		Initializes an instance of the class `docexport.data.ParameterData`.

		Arguments:
			name                 (`str`): The name of the parameter.
			documentation        (`str`): The documentation string of the parameter.
			annotation    (`typing.Any`): Anotation data of the parameter.
			has_annotation      (`bool`): If `True` the parameter is annotated in the code.
			default_value (`typing.Any`): Default value of the parameter.
			has_default_value   (`bool`): If `True` the parameter has a default_value value in the code.
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )
		assert pytools.assertions.type_in( has_annotation, bool )
		assert pytools.assertions.type_in( has_default_value, bool )

		super( ParameterData, self ).__init__( name, documentation )

		self._annotation        = annotation
		self._default_value     = default_value
		self._has_annotation    = has_annotation
		self._has_default_value = has_default_value

	# def __init__ ( self, name, documentation, annotation, has_annotation, default, has_default )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def annotation ( self:ParameterData ) -> typing.Any:
		"""
		Anotation data of the parameter (`typing.Any`).

		Annotation are serialized as `str`.
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		return self._annotation

	# def annotation ( self )

	# --------------------------------------------------

	@annotation.setter
	def annotation ( self:ParameterData, value:typing.Any ):
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		self._annotation = value
		
	# def annotation ( self, value )

	# --------------------------------------------------

	@property
	def default ( self:ParameterData ) -> typing.Any:
		"""
		The default value of this parameter (`typing.Any`).

		Default values are serialized as `str`.
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		return self._default

	# def default ( self )

	# --------------------------------------------------

	@default.setter
	def default ( self:ParameterData, value:typing.Any ):
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		self._default = value
		
	# def default ( self, value )
	
	# --------------------------------------------------

	@property
	def has_annotation ( self:ParameterData ) -> bool:
		"""
		Boolean indicating is this parameter is annotated (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		return self._has_annotation

	# def has_annotation ( self )

	# --------------------------------------------------

	@has_annotation.setter
	def has_annotation ( self:ParameterData, value:bool ):
		assert pytools.assertions.type_is_instance_of( self, ParameterData )
		assert pytools.assertions.type_is( value, bool )

		self._has_annotation = value
		
	# def has_annotation ( self, value )

	# --------------------------------------------------

	@property
	def has_default ( self:ParameterData ) -> bool:
		"""
		Boolean indicating is this parameter has a default value (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )

		return self._has_default

	# def has_default ( self )

	# --------------------------------------------------

	@has_default.setter
	def has_default ( self:ParameterData, value:bool ):
		assert pytools.assertions.type_is_instance_of( self, ParameterData )
		assert pytools.assertions.type_is( value, bool )

		self._has_default = value
		
	# def has_default ( self, value )

	# ##################################################
	# ###                  METHODS                   ###
	# ##################################################

	# --------------------------------------------------

	def serialize ( self:ParameterData, ignores:list[str]=[] ) -> typing.Dict[str, typing.Any]:
		"""
		Serializes the fields of this `Serializable` instance to a python `dict`.
		
		As `default` and `annotation` can be of any type, they might not be serializable.
		Thus only their text representation is serialized.

		Arguments:
			self (`docexport.data.ParameterData`): The instance to serialize.
			ignores             (`list` of `str`): Names of fields that should be ignored.

		Returns:
			`dict` of `str` to `typing.Any: The serialized data.
		"""
		assert pytools.assertions.type_is_instance_of( self, ParameterData )
		assert pytools.assertions.type_is( ignores, list )
		assert pytools.assertions.list_items_type_is( ignores, str )

		data = super( ParameterData, self ).serialize( ignores=['annotation', 'default'] )

		data[ 'annotation' ] = str( self.annotation )
		data[ 'default' ] = str( self.default )

		return data
		
	# def serialize ( self )

# class ParameterData ( MemberData )