# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .member_data import MemberData

# ##################################################
# ###              CLASS SCOPE-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ScopeData',
	   namespace = 'docexport.data',
	fields_names = [
		'namespace'
		]
	)
class ScopeData ( MemberData ):
	"""
	Base class for packages, modules and class that regroups sub-members in a namespace.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name:str='', documentation:str='', namespace:str='' ):
		"""
		Initializes a new instance of the `docexport.data.ScopeData` class.

		Arguments:
			self (`docexport.data.ScopeData`): Instance to initialize
			name                      (`str`): The name of the scope.
			documentation             (`str`): The documentation string of the scope.
			namespace                 (`str`): The namespace of the scope including the name of the scope.
		"""
		assert pytools.assertions.type_is_instance_of( self, ScopeData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )

		super( ScopeData, self ).__init__( name, documentation )
			 
		self._namespace = namespace

	# def __init__ ( self, name, documentation, namespace )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def namespace ( self:ScopeData ) -> str:
		"""
		The namespace of the scope including the name of the scope (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ScopeData )

		return self._namespace

	# def namespace ( self )

	# --------------------------------------------------

	@namespace.setter
	def namespace ( self:ScopeData, value:str ):
		assert pytools.assertions.type_is_instance_of( self, ScopeData )
		assert pytools.assertions.type_is( value, str )

		self._namespace = value
		
	# def namespace ( self, value )

# class ScopeData ( MemberData )