# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .member_data    import MemberData
from .parameter_data import ParameterData

# ##################################################
# ###            CLASS FUNCTION-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'FunctionData',
	   namespace = 'docexport.data',
	fields_names = [
		'annotation',
		'has_annotation',
		'parameters'
		]
	)
class FunctionData ( MemberData ):
	"""
	The `docexport.data.FunctionData` class describes a function.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self:FunctionData,
		name:str='',
		documentation:str='',
		annotation:typing.Any=None,
		has_annotation:bool=False,
		parameters:list[ParameterData]=[]
		):
		"""
		Initializes a new instance of the `FunctionData` class.

		A function is has a name, documentation string and a list of parameters.

		Arguments:
			self                  (`docexport.data.FunctionData`): Function instance to initialize.
			name                                          (`str`): Name of the function.
			documentation                                 (`str`): Documentation string of the function.
			annotation                             (`typing.Any`): Returned value annoation.
			has_annotation                               (`bool`): `True` if the function returned value is annotated in the code.
			parameters (`list` of `docexport.data.ParameterData`): Parameters of the function.
		"""
		assert pytools.assertions.type_is_instance_of( self, FunctionData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )
		assert pytools.assertions.type_is( has_annotation, bool )
		assert pytools.assertions.type_is( parameters, list )
		assert pytools.assertions.list_items_type_is( parameters, ParameterData )

		super( FunctionData, self ).__init__( name, documentation )
			
		self._annotation     = annotation
		self._has_annotation = has_annotation
		self._parameters     = [copy.deepcopy(p) for p in parameters]

	# def __init__ ( self, name, documentation, annotation, has_annotation, parameters )
	
	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def annotation ( self:FunctionData ) -> typing.Any:
		"""
		Annotation of the value returned by this function (`typing.Any`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FunctionData )

		return self._annotation

	# def annotation ( self:FunctionData )
	
	# --------------------------------------------------

	@annotation.setter
	def annotation ( self:FunctionData, value ):
		assert pytools.assertions.type_is_instance_of( self, FunctionData )

		self._annotation = value
		
	# def annotation ( self:FunctionData )
	
	# --------------------------------------------------

	@property
	def has_annotation ( self:FunctionData ) -> bool:
		"""
		`True` if the function returned value is annotated in the code (`bool`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FunctionData )

		return self._has_annotation

	# def has_annotation ( self:FunctionData )
	
	# --------------------------------------------------

	@has_annotation.setter
	def has_annotation ( self:FunctionData, value:bool ):
		assert pytools.assertions.type_is_instance_of( self, FunctionData )

		self._has_annotation = value
		
	# def has_annotation ( self:FunctionData )

	# --------------------------------------------------

	@property
	def parameters ( self:FunctionData ) -> list[FunctionData]:
		"""
		List of parameters data of the function (`list` of `docexport.data.ParameterData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, FunctionData )

		return self._parameters

	# def parameters ( self )

	# --------------------------------------------------

	@parameters.setter
	def parameters ( self:FunctionData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, FunctionData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, ParameterData )

		self._parameters = [copy.deepcopy(p) for p in value]
		
	# def parameters ( self, value )

# class FunctionData ( MemberData )