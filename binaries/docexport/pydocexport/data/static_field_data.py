# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .member_data import MemberData

# ##################################################
# ###          CLASS STATIC-FIELD-DATA           ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'StaticFieldData',
	   namespace = 'docexport.data',
	fields_names = []
	)
class StaticFieldData ( MemberData ):
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name:str='', documentation:str='' ):
		"""
		Initializes a new instance of the class `docexport.data.StaticFieldData`.

		Arguments:
			self (`docexport.data.StaticFieldData`): The `docexport.data.StaticFieldData` instance to initialize.
			name                            (`str`): The name of the static field.
			documentation                   (`str`): The documentation string of static field.
		"""
		assert pytools.assertions.type_is_instance_of( self, StaticFieldData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )

		super( StaticFieldData, self ).__init__( name, documentation )

	# def __init__ ( self, name, documentation )

# class StaticFieldData ( MemberData )