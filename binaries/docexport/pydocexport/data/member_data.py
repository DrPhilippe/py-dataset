# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# INTERNALS
from __future__ import annotations
import pytools.assertions
import pytools.serialization

# ##################################################
# ###             CLASS MEMBER-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'MemberData',
	   namespace = 'docexport.data',
	fields_names = [
		'name',
		'documentation'
		]
	)
class MemberData ( pytools.serialization.Serializable ):
	"""
	Base class for data types used to describe python packages, modules and their members.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self:MemberData, name:str='', documentation:str='' ):
		"""
		Initializes an instance of the class `docexport.data.MemberData` with the given `name` and `documentation`.

		Arguments:
			name          (`str`): The name of the member.
			documentation (`str`): The documentation string of the member.
		"""
		assert pytools.assertions.type_is_instance_of( self, MemberData )
		assert pytools.assertions.type_is( name, str )

		super( MemberData, self ).__init__()

		self._name          = name
		self._documentation = documentation

	# def __init__ ( self, name, documentation )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def name ( self:MemberData ) -> str:
		"""
		The name of this member (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MemberData )

		return self._name

	# def name ( self )

	# --------------------------------------------------

	@name.setter
	def name ( self:MemberData, value:str ):
		assert pytools.assertions.type_is_instance_of( self, MemberData )
		assert pytools.assertions.type_is( value, str )

		self._name = value
		
	# def name ( self, value )

	# --------------------------------------------------

	@property
	def documentation ( self:MemberData ) -> str:
		"""
		The documentation string of this member (`str`).
		"""
		assert pytools.assertions.type_is_instance_of( self, MemberData )

		return self._documentation

	# def documentation ( self )

	# --------------------------------------------------

	@documentation.setter
	def documentation ( self:MemberData, value:str ):
		assert pytools.assertions.type_is_instance_of( self, MemberData )
		assert pytools.assertions.type_is( value, str )

		self._documentation = value
		
	# def documentation ( self, value )

# class MemberData ( pytools.serialization.Serializable )