# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .member_data import MemberData

# ##################################################
# ###            CLASS PROPERTY-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PropertyData',
	   namespace = 'docexport.data',
	fields_names = []
	)
class PropertyData ( MemberData ):
	"""
	The class `docexport.data.PropertyData` is used to describe a class property.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ ( self, name:str='', documentation:str='' ):
		"""
		Initializes a new instance of the class `PropertyData`.

		Arguments:
			self (`docexport.data.PropertyData`): The `docexport.data.PropertyData` instance to initialize.
			name                         (`str`): The name of the property.
			documentation                (`str`): The documentation string of the property.
		"""
		assert pytools.assertions.type_is_instance_of( self, PropertyData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )

		super( PropertyData, self ).__init__( name, documentation )

	# def __init__ ( self, name, documentation )

# class PropertyData ( MemberData )