# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .class_data    import ClassData
from .function_data import FunctionData
from .scope_data    import ScopeData

# ##################################################
# ###             CLASS MODULE-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ModuleData',
	   namespace = 'docexport.data',
	fields_names = [
		'functions',
		'classes'
		]
	)
class ModuleData ( ScopeData ):
	"""
	The `docexport.data.ModuleData` class describes the contents of a module.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self:ModuleData,
		name:str='',
		documentation:str='',
		namespace:str='',
		functions:list[FunctionData]=[],
		classes:list[ClassData]=[]
		):
		"""
		Initailizes a new instance of the `docexport.data.ModuleData` class.

		A module has a name, documentation string and manespace like a scope.
		It can have functions and classes.

		Arguments:
			self                  (`docexport.data.ModuleData`): The module data to initialize.
			name                                        (`str`): The name of this module.
			documentation                               (`str`): The documentation string of this module.
			namespace                                   (`str`): The namespace of this module.
			functions (`list` of `docexport.data.FunctionData`): The fucntions of this module.
			classes      (`list` of `docexport.data.ClassData`): The classes of this module.
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )
		assert pytools.assertions.type_is( namespace, str )
		assert pytools.assertions.type_is( functions, list )
		assert pytools.assertions.list_items_type_is( functions, FunctionData )
		assert pytools.assertions.type_is( classes, list )
		assert pytools.assertions.list_items_type_is( classes, ClassData )

		super( ModuleData, self ).__init__( name, documentation, namespace )

		self.functions = [copy.deepcopy(f) for f in functions]
		self.classes   = [copy.deepcopy(c) for c in classes]

	# def __init__ ( self, name, namespace, documentation, functions, classes )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def functions ( self:ModuleData ) -> list[FunctionData]:
		"""
		The function of this module (`list` of `docexport.data.FunctionData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleData )

		return self._functions

	# def functions ( self )

	# --------------------------------------------------

	@functions.setter
	def functions ( self:ModuleData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ModuleData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, FunctionData )

		self._functions = [copy.deepcopy(f) for f in value]
		
	# def functions ( self, value )

	# --------------------------------------------------

	@property
	def classes ( self:ModuleData ) -> list[ClassData]:
		"""
		The classes of this module (`list` of `docexport.data.ClassData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ModuleData )

		return self._classes

	# def classes ( self )

	# --------------------------------------------------

	@classes.setter
	def classes ( self:ModuleData, value:list[ClassData] ):
		assert pytools.assertions.type_is_instance_of( self, ModuleData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, ClassData )

		self._classes = [copy.deepcopy(c) for c in value]
		
	# def classes ( self, value )

# class ModuleData ( ScopeData )