# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy
import typing

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .function_data     import FunctionData
from .property_data     import PropertyData
from .scope_data        import ScopeData
from .static_field_data import StaticFieldData

# ##################################################
# ###              CLASS CLASS-DATA              ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'ClassData',
	   namespace = 'docexport.data',
	fields_names = [
		'constructor',
		'properties',
		'methods',
		'operators',
		'class_methods',
		'static_fields',
		'static_methods',
		'sub_classes'
		]
	)
class ClassData ( ScopeData ):
	"""
	The class `ClassData` describes a class.
	"""

	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self:ClassData,
		name:str='',
		documentation:str='',
		namespace:str='',
		constructor:typing.Optional[FunctionData]=None,
		properties:list[PropertyData]=[],
		methods:list[FunctionData]=[],
		operators:list[FunctionData]=[],
		class_methods:list[FunctionData]=[],
		static_fields:list[StaticFieldData]=[],
		static_methods:list[FunctionData]=[],
		subclasses:list[ClassData]=[]
		):
		"""
		Initializes a new instance of the `docexport.data.ClassData` class.

		Arguments:
			self                          (`docexport.data.ClassData`): `docexport.data.ClassData` instance to initialize.
			name                                               (`str`): Name of the class.
			documentation                                      (`str`): Documentation of the class.
			namespace                                          (`str`): Namespace of the class.
			constructor         (`docexport.data.FunctionData`/`None`): Class constructor data.
			properties       (`list` of `docexport.data.PropertyData`): List of properties data of the class.
			methods          (`list` of `docexport.data.FunctionData`): List of methods data of the class.
			operators        (`list` of `docexport.data.FunctionData`): List of operators data of the class.
			class_methods    (`list` of `docexport.data.FunctionData`): List of class methods data of the class.
			static_fields (`list` of `docexport.data.StaticFieldData`): List of static fields data of the class.
			static_methods   (`list` of `docexport.data.FunctionData`): List of static methods data of the class.
			subclasses          (`list` of `docexport.data.ClassData`): List of sub-classes data of the class.
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )
		assert pytools.assertions.type_is( namespace, str )
		assert pytools.assertions.TypeIn( constructor, (type(None), FunctionData) )
		assert pytools.assertions.type_is( properties, list )
		assert pytools.assertions.list_items_type_is( properties, PropertyData )
		assert pytools.assertions.type_is( methods, list )
		assert pytools.assertions.list_items_type_is( methods, FunctionData )
		assert pytools.assertions.type_is( operators, list )
		assert pytools.assertions.list_items_type_is( operators, FunctionData )
		assert pytools.assertions.type_is( class_methods, list )
		assert pytools.assertions.list_items_type_is( class_methods, FunctionData )
		assert pytools.assertions.type_is( static_fields, list )
		assert pytools.assertions.list_items_type_is( static_fields, StaticFieldData )
		assert pytools.assertions.type_is( static_methods, list )
		assert pytools.assertions.list_items_type_is( static_methods, FunctionData )
		assert pytools.assertions.type_is( subclasses, list )
		assert pytools.assertions.list_items_type_is( subclasses, ClassData )

		# Init parent class (ScopeData)
		super( ClassData, self ).__init__( name, documentation, namespace )

		# Init fields/properties
		self._constructor    = copy.deepcopy(constructor)
		self._properties     = [copy.deepcopy(p) for p in properties]
		self._methods        = [copy.deepcopy(m) for m in methods]
		self._operators      = [copy.deepcopy(o) for o in operators]
		self._class_methods  = [copy.deepcopy(m) for m in class_methods]
		self._static_fields  = [copy.deepcopy(f) for f in static_fields]
		self._static_methods = [copy.deepcopy(m) for m in static_methods]
		self._sub_classes    = [copy.deepcopy(c) for c in subclasses]

	# def __init__ ( self, name, namespace, documentation, constructor, properties, methods, operators, class_methods, static_fields, static_methods, subclasses )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def constructor ( self:ClassData ) -> typing.Optional[FunctionData]:
		"""
		Data of the constructor of this class (`docexport.data.FunctionData`/`None`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._constructor

	# def constructor ( self )
	
	# --------------------------------------------------

	@constructor.setter
	def constructor ( self:ClassData, value:typing.Optional[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_in( value, (type(None), FunctionData) )
		
		self._constructor = value

	# def constructor ( self, value )

	# --------------------------------------------------

	@property
	def properties ( self:ClassData ) -> list[PropertyData]:
		"""
		List of properties data of this class (`list` of `docexport.data.PropertyData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._properties

	# def properties ( self )
	
	# --------------------------------------------------

	@properties.setter
	def properties ( self:ClassData, value:list[PropertyData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, PropertyData )
		
		self._properties = value

	# def properties ( self, value )

	# --------------------------------------------------

	@property
	def number_of_properties ( self:ClassData ) -> int:
		"""
		Number of properies of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )

		return len( self._properties ) 

	# def number_of_properties ( self:ClassData )
	
	# --------------------------------------------------

	@property
	def methods ( self:ClassData ) -> list[FunctionData]:
		"""
		List of methods data of this class (`list` of `docexport.data.FunctionData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._methods

	# def methods ( self )
	
	# --------------------------------------------------

	@methods.setter
	def methods ( self:ClassData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, FunctionData )
		
		self._methods = [copy.deepcopy(f) for f in value]

	# def methods ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_methods ( self:ClassData ) -> int:
		"""
		Number of methods of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._methods ) 

	# def number_of_methods ( self )

	# --------------------------------------------------

	@property
	def operators ( self:ClassData ) -> list[FunctionData]:
		"""
		List of operators data of this class (`list` of `docexport.data.FunctionData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._operators

	# def operators ( self )
	
	# --------------------------------------------------

	@operators.setter
	def operators ( self:ClassData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, FunctionData )
		
		self._operators = [copy.deepcopy(o) for o in value]

	# def operators ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_operators ( self:ClassData ) -> int:
		"""
		Number of operators of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._operators ) 

	# def number_of_operators ( self )

	# --------------------------------------------------

	@property
	def class_methods ( self:ClassData ) -> list[FunctionData]:
		"""
		List of class methods data of this class (`list` of `docexport.data.FunctionData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._class_methods

	# def class_methods ( self )
	
	# --------------------------------------------------

	@class_methods.setter
	def class_methods ( self:ClassData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, FunctionData )
		
		self._class_methods = [copy.deepcopy(m) for m in value]

	# def class_methods ( self, value )

	# --------------------------------------------------

	@property
	def number_of_class_methods ( self:ClassData ) -> int:
		"""
		Number of class methods of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._class_methods ) 

	# def number_of_class_methods ( self )

	# --------------------------------------------------

	@property
	def static_fields ( self:ClassData ) -> list[StaticFieldData]:
		"""
		The description of the static-fields of this class (`list` of `docexport.data.StaticFieldData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._static_fields

	# def static_fields ( self )
	
	# --------------------------------------------------

	@static_fields.setter
	def static_fields ( self:ClassData, value:list[StaticFieldData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, StaticFieldData )
		
		self._static_fields = [copy.deepcopy(f) for f in value]

	# def static_fields ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_static_fields ( self:ClassData ) -> int:
		"""
		Number of static fields of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._static_fields ) 

	# def number_of_static_fields ( self )

	# --------------------------------------------------

	@property
	def static_methods ( self:ClassData ) -> list[FunctionData]:
		"""
		The description of the static-methods of this class (`list` of `docexport.data.FunctionData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._static_methods

	# def static_methods ( self )
	
	# --------------------------------------------------

	@static_methods.setter
	def static_methods ( self:ClassData, value:list[FunctionData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, FunctionData )
		
		self._static_methods = [copy.deepcopy(m) for m in value]

	# def static_methods ( self, value )
	
	# --------------------------------------------------

	@property
	def number_of_static_methods ( self:ClassData ) -> int:
		"""
		Number of static methods of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._static_methods ) 

	# def number_of_static_methods ( self )

	# --------------------------------------------------

	@property
	def sub_classes ( self:ClassData ) -> list[ClassData]:
		"""
		List of sub-classes data of this class (`list` of `docexport.data.ClassData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return self._sub_classes

	# def sub_classes ( self )
	
	# --------------------------------------------------

	@sub_classes.setter
	def sub_classes ( self:ClassData, value:list[ClassData] ):
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, ClassData )
		
		self._sub_classes = [copy.deepcopy(c) for c in value]

	# def sub_classes ( self, value )

	# --------------------------------------------------

	@property
	def number_of_sub_classes ( self:ClassData ) -> int:
		"""
		Number of sub-classes of this class (`int`).
		"""
		assert pytools.assertions.type_is_instance_of( self, ClassData )
		
		return len( self._sub_classes ) 

	# def number_of_sub_classes ( self )

# class ClassData ( ScopeData )