# ##################################################
# ###                DEPENDENCES                 ###
# ##################################################

# EXTERNALS
from __future__ import annotations
import copy

# INTERNALS
import pytools.assertions
import pytools.serialization

# LOCALS
from .module_data import ModuleData
from .scope_data  import ScopeData

# ##################################################
# ###             CLASS PACKAGE-DATA             ###
# ##################################################

@pytools.serialization.RegisterSerializableAttribute(
	    typename = 'PackageData',
	   namespace = 'docexport.data',
	fields_names = [
		'sub_packages',
		'modules'
		]
	)
class PackageData ( ScopeData ):
	"""
	The `docexport.data.PackageData` class describes the contents of a package.
	"""
	
	# ##################################################
	# ###                CONSTRUCTOR                 ###
	# ##################################################

	# --------------------------------------------------

	def __init__ (
		self:PackageData,
		name:str='',
		documentation:str='',
		namespace:str='',
		sub_packages:list[PackageData]=[],
		modules:list[ModuleData]=[] ):
		"""
		Initailizes a new instance of the `docexport.data.PackageData` class.

		A package has a name, documentation string and manespace like a scope.
		And it cam have sub packages and sub modules.

		Arguments:
			self                   (`docexport.data.PackageData`): The package to initialize.
			name                                          (`str`): The name of this package.
			documentation                                 (`str`): The documentation string of this package.
			namespace                                     (`str`): The namespace of this package.
			sub_packages (`list` of `docexport.data.PackageData`): The sub-packages of this package.
			modules       (`list` of `docexport.data.ModuleData`): The modules of this package.
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageData )
		assert pytools.assertions.type_is( name, str )
		assert pytools.assertions.type_is( documentation, str )
		assert pytools.assertions.type_is( namespace, str )
		assert pytools.assertions.type_is( sub_packages, list )
		assert pytools.assertions.list_items_type_is( sub_packages, PackageData )
		assert pytools.assertions.type_is( modules, list )
		assert pytools.assertions.list_items_type_is( modules, ModuleData )

		super( PackageData, self ).__init__( name, documentation, namespace )

		self._sub_packages = [copy.deepcopy(p) for p in sub_packages]
		self._modules      = [copy.deepcopy(p) for m in modules]

	# def __init__ ( self, name, namespace, documentation, sub_packages, modules )

	# ##################################################
	# ###                 PROPERTIES                 ###
	# ##################################################

	# --------------------------------------------------

	@property
	def sub_packages ( self:PackageData ) -> list[PackageData]:
		"""
		The sub-packages of this package (`list` of `docexport.data.PackageData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageData )

		return self._sub_packages

	# def sub_packages ( self )

	# --------------------------------------------------

	@sub_packages.setter
	def sub_packages ( self:PackageData, value:list[PackageData] ):
		assert pytools.assertions.type_is_instance_of( self, PackageData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, PackageData )

		self._sub_packages = [copy.deepcopy(p) for p in value]
		
	# def sub_packages ( self, value )

	# --------------------------------------------------

	@property
	def modules ( self:PackageData ) -> list[ModuleData]:
		"""
		The sub-modules of this package (`list` of `docexport.data.ModuleData`).
		"""
		assert pytools.assertions.type_is_instance_of( self, PackageData )

		return self._modules

	# def modules ( self )

	# --------------------------------------------------

	@modules.setter
	def modules ( self:PackageData, value:list[ModuleData] ):
		assert pytools.assertions.type_is_instance_of( self, PackageData )
		assert pytools.assertions.type_is( value, list )
		assert pytools.assertions.list_items_type_is( value, ModuleData )

		self._modules = [copy.deepcopy(m) for m in value]
		
	# def modules ( self, value )

# class PackageData ( ScopeData )