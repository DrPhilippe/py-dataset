# ##################################################
# ###               INFORMATIONS                 ###
# ##################################################

"""
Enty point of the PydocExporter program.

This program export python docstrings in custom formats.

Usage:

	```console 
	python3 docexport package_path output_path [--parser parser_name] [--generator generator_name]
	```

	For more details run:

	```console 
	python3 docexport --help
	```

Example:

	```console 
	python3 docexport MyProject/Package MyProject/Wiki --generator gitlab
	```

Authors:
	Philippe Perez de San Roman (philippe.perezdesanroman@iteca.eu)

Version:
	2020.01.0f0
"""

# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNAL
import argparse

# INTERNAL
import pytools.factory
import pytools.path
import pytools.plugins
import pytools.tasks

# LOCALS
import pydocexport

# ##################################################
# ###                    MAIN                    ###
# ##################################################

if __name__ == '__main__':

	# Title
	print( 'DOC-EXPORT:\n' )

	# Environment
	main_filepath     = pytools.path.FilePath( __file__ )
	project_directory = main_filepath.parent()
	plugins_directory = project_directory + pytools.path.DirectoryPath( 'plugins' )
	
	# Import plugins
	pt = pytools.tasks.console_progress_logger( 'Loading plugins' )
	pytools.plugins.load_plugins( plugins_directory, 'plugins', pt )
	parsers_names    = pytools.factory.ClassFactory.get_typenames_of_group( 'parsers'    )
	generators_names = pytools.factory.ClassFactory.get_typenames_of_group( 'generators' )

	# Echo plugins
	print( 'Plugins:' )
	print( '       parsers:', parsers_names    )
	print( '    generators:', generators_names )
	print( '' )

	# Parse Arguments
	parser = argparse.ArgumentParser()
	parser.add_argument( 'package_path', type=pytools.path.DirectoryPath, help='Path of the python package' )
	parser.add_argument( 'output_path', type=pytools.path.DirectoryPath, help='Path of the directory where to write the documentation' )
	parser.add_argument( '-p', '--parser', default='default', type=str, choices=parsers_names, help='Name of the parser used to scan the python packages and modules' )
	parser.add_argument( '-g', '--generator', default='gitlab', type=str, choices=generators_names, help='Name of the genator used create the documentation' )
	arguments = parser.parse_args()

	# Echo Arguments
	print( 'Arguments:' )
	print( '     package path:', arguments.package_path )
	print( '     parser:      ', arguments.parser       )
	print( '     generator:   ', arguments.generator    )
	print( '     output path: ', arguments.output_path  )
	print( '' )

	# Scan the package
	progress_tracker = pytools.tasks.console_progress_logger( 'Scanning package' )
	package_scanner = pydocexport.scanners.PackageScanner.create(
		progress_tracker = progress_tracker,
		          logger = pytools.tasks.file_logger( 'logs/docexport.scanner.log' ),
		    package_path = arguments.package_path,
		       namespace = '',
		            name = 'scanner'
		)
	package_scanner.run()

	# Save the scanned package
	package_data_filepath = arguments.output_path + pytools.path.FilePath( 'package_data.json' )
	package_data_filepath.serialize_json( package_scanner.package_data )

	# --------------------------------------------------
	# Parse the doc

	# # Instantiate the parser
	# parser = Factory.ClassFactory.Instantiate(
	# 	group_name = 'Parsers',
	# 	  typename = arguments.parser.lower()
	# 	)

	# # Parse the doc of the package
	# package_data = parser.ParsePackage(
	# 	package_data = package_data
	# 	)

	# --------------------------------------------------
	# Generate the doc

	# # Instantiate the generator
	# generator = Factory.ClassFactory.Instantiate(
	# 	group_name = 'Generators',
	# 	  typename = arguments.generator.lower()
	# 	)

	# # Generate the doc of the package
	# progress_tracker = Progression.Tracker(
	# 	            text = 'Generating documentation',
	# 	           start = 0,
	# 	             end = 100,
	# 	    report_every = 1,
	# 	break_line_every = 10,
	# 	     indent_size = 4
	# 	)
	# generator.GeneratePackageDocumentation(
	# 	         package_data = package_data,
	# 	output_directory_path = FileSystem.DirectoryPath( arguments.output_path ),
	# 	              is_root = True,
	# 	     progress_tracker = progress_tracker
	# 	)

# if __name__ == '__main__'