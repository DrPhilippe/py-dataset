import pydataset
import numpy
import cv2


# Function to create a direction map
def create_map ( point, W, H ):
	dir_map = numpy.zeros( [H, W, 2] )
	for u in range( W ):
		for v in range( H ):
			coordinates     = numpy.asarray( [u, v], dtype=numpy.float32 )
			direction       = numpy.subtract( point, coordinates )
			direction       = numpy.divide( direction, numpy.linalg.norm(direction)+numpy.finfo(numpy.float32).eps )
			dir_map[ v, u ] = direction
	return dir_map

def display_as_colors ( dir_map ):
	H, W    = dir_map.shape[0], dir_map.shape[1]
	display = numpy.empty( [H, W], dtype=numpy.uint8 )
	for u in range(W):
		for v in range(H):
			du, dv = dir_map[ v, u ]
			angle  = numpy.arctan2( dv, du ) # angle between -PI and PI
			angle  = angle + numpy.pi # angle between 0 and 2PI
			angle  = numpy.mod( angle, numpy.pi*2. ) # angle between 0 and 2PI for sure
			angle  = numpy.divide( angle, numpy.pi*2 ) # angle between 0 and 1
			angle  = numpy.multiply( angle, 255. ).astype(numpy.uint8) # angle between 0 and 255
			display[ v, u ] = angle
	return cv2.applyColorMap( display, cv2.COLORMAP_HSV )

# def display_as_colors ( dir_map ):
# 	H, W    = dir_map.shape[0], dir_map.shape[1]
# 	display = numpy.empty( [H, W, 3], dtype=numpy.uint8 )
# 	display[ :, :, 0 ] = numpy.multiply( numpy.abs( dir_map[ :, :, 0 ] ), 255. ).astype( numpy.uint8 )
# 	display[ :, :, 2 ] = numpy.multiply( numpy.abs( dir_map[ :, :, 1 ] ), 255. ).astype( numpy.uint8 )
# 	return display


def display_points ( image, points, radius=1, color=(0, 0, 0), thickness=1 ):
	for i in range( points.shape[0] ):
		u, v = points[ i ]
		image = cv2.circle( image, (int(u),int(v)), radius, color, thickness)
	return image

DEG_TO_RAD = numpy.pi / 180.

example = pydataset.dataset.get( 'file://D:/Datasets/bop_lm/groups/scenes/groups/test/groups/000009/groups/000066/examples/000000' )
image   = example.get_feature( 'image' ).value
W, H    = image.shape[1], image.shape[0]
mask    = example.get_feature( 'visible_mask' ).value
# mesh    = example.get_feature( 'mesh_ply' ).value
# K       = example.get_feature( 'K' ).value
# R       = example.get_feature( 'R' ).value
# t       = example.get_feature( 't' ).value
# bb      = example.get_feature( 'bounding_box' ).value

# Prepare mask
mask = numpy.reshape( mask, [H, W, 1] )
mask = numpy.tile( mask, [1, 1, 3] ).astype( numpy.uint8 )

# Pick points
SAMPLES = 1
# indexes = numpy.arange( mesh.points.shape[0], dtype=numpy.int32 )
# indexes = numpy.random.choice( indexes, size=SAMPLES )
# points  = mesh.points[ indexes ]

# Project points
# r = pydataset.pose_utils.convert_rotation_to_eulers_if_needed( R )
# points, jacobian = cv2.projectPoints( points, r, t, K, numpy.zeros([5]) )
# points = numpy.reshape( points, [SAMPLES, 2] )

points = numpy.asarray( [[355, 200]], dtype=numpy.float32 )


# Create direction maps
dir_map = numpy.zeros( [H, W, SAMPLES, 2] )
for i in range( SAMPLES ):
	point = points[ i ]
	dir_map[ :, :, i, : ] = create_map( point, W, H )

dir_map_color = display_as_colors( dir_map[:, :, 0, :] )

k = numpy.ones( [5,5], dtype=numpy.uint8 )
mask = cv2.dilate( mask, k )
mask = cv2.erode( mask, k )

dir_map_color = numpy.where( mask, dir_map_color, (dir_map_color.astype(numpy.float32)*0.2).astype(numpy.uint8) )#numpy.zeros([H,W,3], numpy.uint8) )

# dir_map_color = display_points( dir_map_color, points, 1, (0,0,0), 2 )
# dir_map_color = numpy.multiply( dir_map_color, mask )

# C=128
# image = image[ 200-C//2:200+C//2, 355-C//2:355+C//2, : ]
# dir_map_color = dir_map_color[ 200-C//2:200+C//2, 355-C//2:355+C//2, : ]

# image = cv2.resize( image, (256, 256) )
# dir_map_color = cv2.resize( dir_map_color, (256, 256) )

cv2.imwrite( 'duck_66_color.png', image )
cv2.imwrite( 'duck_66_directions.png', dir_map_color )

cv2.imshow( 'image', image )
cv2.imshow( 'dir_map_color', dir_map_color )
cv2.imshow( 'mask', mask*255 )
cv2.waitKey( 0 )