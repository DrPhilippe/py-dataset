import cv2
import numpy
import pydataset


g     = pydataset.dataset.get( 'file://E:/Datasets/linemod/groups/raw/groups/all/groups/duck/groups/00066' )
image = g.get_feature( 'image' ).value
e     = g.get_example( '0' )
mask  = e.get_feature( 'mask' ).value

mask = numpy.reshape( mask, [480, 640, 1] )
mask = numpy.tile( mask, [1, 1, 3] ).astype( numpy.uint8 )*255

back = cv2.imread( 'room.jpg' )
display = numpy.where( mask, image, back )

cv2.imwrite( 'duck_66_color.png', image )
cv2.imwrite( 'duck_66_mask.png', mask )
cv2.imwrite( 'duck_66_disp.png', display )

cv2.imshow( 'duck 66', display )
cv2.waitKey(0)