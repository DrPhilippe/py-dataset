# ##################################################
# ###                DEPENDANCES                 ###
# ##################################################

# EXTERNALS
import argparse
import sys

# INTERNALS
import pydataset
import pytools

# ##################################################
# ###                 ARGUMENTS                  ###
# ##################################################

parser = argparse.ArgumentParser()
parser.add_argument( 'directory_path', type=str, help='Path of the directory in which to search for mesh to convert.' )
parser.add_argument( '--override', action='store_true', help='Override existing OBJ meshs.' )
arguments = parser.parse_args()

sys.stdout.write( 'PLY TO OBJ:\n' )
sys.stdout.write( '    directory_path: {}\n'.format(arguments.directory_path) )
sys.stdout.write( '          override: {}\n'.format(arguments.override) )
sys.stdout.write( '\n' )

# ##################################################
# ###         SEACH FOR MESH TO CONVERT          ###
# ##################################################

sys.stdout.write( 'SEARCHING:\n' )
directory_path = pytools.path.DirectoryPath( arguments.directory_path )
ply_files = []
for path in directory_path.list_contents():
	if path.is_a_file() and path.extension().lower() == '.ply':
		sys.stdout.write( '    Found: {}\n'.format(path) )
		ply_files.append( path )
sys.stdout.write( '    DONE\n' )
sys.stdout.write( '\n' )

if not ply_files:
	sys.stdout.write( 'NOTHING TO CONVERT\n' )
	exit()

# ##################################################
# ###                  CONVERT                   ###
# ##################################################

sys.stdout.write( 'CONVERTING:\n' )
for ply_file in ply_files:
	obj_file  = ply_file.parent() + pytools.path.FilePath.format( '{}.obj', ply_file.filename() )
	sys.stdout.write( '    {} -> {} '.format(ply_file, obj_file) )
	sys.stdout.flush()
	try:
		mesh_data = pydataset.render.MeshData.load_ply_v2( ply_file )
		mesh_data.save_obj( obj_file )
		sys.stdout.write( 'OK\n' )
	except Exception as e:
		sys.stdout.write( 'FAILED: {}\n'.format(e) )
print( '    DONE' )
print( '' )