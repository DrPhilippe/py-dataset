import serial
import serial.tools.list_ports

ports = serial.tools.list_ports.comports(include_links=False)
for port in ports:
	print( port.device )

arduino = serial.Serial( 'COM4', 9600, timeout=1.0 )
while True:

	data = arduino.readline()[ :-2 ] #the last bit gets rid of the new-line chars
	if data:
		print( data )

	value = input()
	if value:
		arduino.write( bytes(value, 'ascii') )

	data = arduino.readline()[ :-2 ] #the last bit gets rid of the new-line chars
	if data:
		print( data )