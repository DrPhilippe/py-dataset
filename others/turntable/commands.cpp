#include "commands.h"

Command::Command ( const char id, const float param )
: id( id ), param( param )
{}

Command::operator bool () const
{
	return this->id != '-';
}

Command Command::parse_from_serial ()
{
	// Create a default command
	Command cmd;

	// If serial received nothing
	if ( !Serial.available() )
	{
		// Return the default command
		return cmd;
	}

	// Read the command id
	char data = (char)Serial.read();    
	if ( data=='\n' || data=='\r' || data=='\0' )
	{
		return cmd;
	}
	cmd.id = toupper( data );

	// Read command param
	switch ( cmd.id )
	{
	case 'R':
	case 'S':
	case 'U':
	case 'V':
		if ( !Serial.available() )
		{
			Serial.print( "MISSING PARAMETER FOR COMMAND '" );
			Serial.print( cmd.id );
			Serial.println( "'" );
			// return Command();
		}
		else
		{
			cmd.param = Serial.parseFloat();
		}
		break;
	default:
		while ( Serial.available() )
		{
			Serial.read();
		}
		break;
	}
	
	return cmd;
}
