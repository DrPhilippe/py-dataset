#ifndef commands_h
#define commands_h

#include <Arduino.h>

class Command
{
public:
    Command ( const char id='-', const float param=0.0f );
    operator bool () const;
    static Command parse_from_serial ();

    char id;
    float param;
};

#endif
