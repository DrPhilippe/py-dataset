#ifndef turntable_h
#define turntable_h

#include <Arduino.h>
#include <Stepper.h>
#include <math.h>

#include "config.h"
#include "commands.h"

enum TurnTableStatus
{
	unknown = 0,
	ok      = 1,
	error   = -1
};

class TurnTableData
{
public:
	TurnTableData ();

	TurnTableStatus status;
	int steps;
	float angle;
	bool is_home;
	float unit_angle;
	int velocity;
	String message;
};

class TurnTable
{
public:
	TurnTable ();
	~TurnTable ();
	
	const bool is_home () const;
	void set_velocity ( int value );
	void set_unit_angle ( float value );

	void run_command ( const Command& cmd );
	void home ();
	void rotate ( const float angle );
	void step ( const int steps );
	void turn ();
	void update ();

	void print () const;
	
private:
	void _read_home_switch ();
	void _set_error ( const String message );
	void _set_ok ( const String message="" );
	void _turn_led_on ();
	void _turn_led_off ();
	String _status_to_string ();

	Stepper* stepper;
	TurnTableData data;
};

#endif