
#include "config.h"
#include "commands.h"
#include "turntable.h"

TurnTable table;

void setup()
{
	Serial.begin( 9600 );
	pinMode( PIN_BUT, INPUT );
	table.print();
}

int previous_button_state = LOW;
void loop ()
{
	// Parse and run command from the serial
	Command cmd = Command::parse_from_serial();
	if ( cmd )
	{
		table.run_command( cmd );
		table.print();
	}
	
	// Is the button pressed ?
	int current_button_state = digitalRead( PIN_BUT );
	if ( current_button_state == HIGH && current_button_state != previous_button_state )
	{
		cmd = Command( 'T' );
		table.run_command( cmd );
		table.print();
	}    
	previous_button_state = current_button_state;

	// Update the table
	table.update();

	// SLEEP
	delay( 50 );
}
