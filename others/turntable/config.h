// PIN LAYOUT
#define PIN_IN1 12
#define PIN_IN2 11
#define PIN_IN3 10
#define PIN_IN4 9
#define PIN_END 4
#define PIN_BUT 3
#define PIN_LED 2

// CONSTANTS
#define STEPS_PER_TURN 2038

#define MIN_ANGLE (360.0f/(float)STEPS_PER_TURN)
#define MAX_ANGLE 360
#define MIN_VELOCITY 1
#define MAX_VELOCITY 9