#include "turntable.h"

const float fmap ( const float x, const float in_min, const float in_max, const float out_min, const float out_max )
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

TurnTableData::TurnTableData ()
: status( TurnTableStatus::unknown )
, steps( 0 )
, angle( 0.0f )
, is_home( false )
, unit_angle( 1.0f )
, velocity( 4 )
, message( "" )
{}

TurnTable::TurnTable ()
: stepper( NULL )
, data()
{
	this->stepper = new Stepper( STEPS_PER_TURN, PIN_IN1, PIN_IN3, PIN_IN2, PIN_IN4 );
	this->stepper->setSpeed( this->data.velocity );
   
	pinMode( PIN_END, INPUT );
	pinMode( PIN_LED, OUTPUT );
	digitalWrite( PIN_LED, LOW );
	
	this->_read_home_switch();
	this->_set_ok( "Ready" );
}

TurnTable::~TurnTable()
{
	delete this->stepper;
}

const bool TurnTable::is_home () const
{
	return this->data.is_home;
}

void TurnTable::set_velocity ( const int value )
{
	this->data.velocity = value;
	this->stepper->setSpeed( value );
	this->_set_ok( String("Velocity set to ")+value );
}

void TurnTable::set_unit_angle ( const float value )
{
	this->data.unit_angle = value;
	this->_set_ok( String("Unit angle set to ")+value );
}

void TurnTable::run_command ( const Command& cmd )
{
	float angle  = cmd.param;
	int steps    = (int)cmd.param;
	int velocity = (int)cmd.param;

	switch ( cmd.id )
	{		
	case 'H':        
		this->home();
		break;
		
	case 'R':
		if ( angle < MIN_ANGLE )
		{
			this->_set_error( String("Angle ")+angle+" is too small, minimum is "+MIN_ANGLE+" degrees" );
			return;
		}
		if ( angle > 360.0f )
		{
			angle = fmod( angle, 360.0f );
		}
		this->rotate( angle );
		break;
		
	case 'S':
		if ( steps < 1 )
		{
			this->_set_error( "Cannot take less than one step" );
			return;
		}
		this->step( steps );
		break;
		
	case 'T':
		this->turn();
		break;
		
	case 'U':
		if ( angle < MIN_ANGLE )
		{
			this->_set_error( String("Unit angle ")+angle+" is too small, minimum is "+MIN_ANGLE+" degrees" );
			return;
		}
		if ( angle > 360.0f )
		{
			angle = fmod( angle, 360.0f );
		}
		this->set_unit_angle( angle );
		break;
		
	case 'V':
		if ( velocity < MIN_VELOCITY )
		{
			this->_set_error( String("Velocity ")+velocity+" is too small, minimum is 1" );
			return;
		}
		if ( velocity > MAX_VELOCITY )
		{
			this->_set_error( String("Velocity ")+velocity+" is too large, maximum is 9" );
			return;
		}
		this->set_velocity( velocity  );
		break;
		
	default:
		this->_set_error( String("Unsupported command: id=")+cmd.id+", param="+cmd.param );
		break;
	}
}

void TurnTable::home ()
{
	// Read the home switch
	this->_read_home_switch();

	// If already home, do not home
	if ( this->is_home() )
	{
		return;
	}
	
	// Turn ON the LED
	this->_turn_led_on();

	// Remember previous velocity
	int previous_velocity = this->data.velocity;

	// Set the highest velocity
	this->set_velocity( 9 );

	// Fast / Inacurate homing
	int count = 0;
	while ( !this->is_home() && count < 2*STEPS_PER_TURN )
	{
		// Move the steper by 10 steps
		this->stepper->step( 10 );
		
		// Read the home switch again
		this->_read_home_switch();
		
		// Count loops to stop if switch is not triggered
		count += 10;
	}

	// Check if homing failed
	if ( count > 2*STEPS_PER_TURN )
	{
		this->_set_error( "HOMING FAILED: TRY ADJUSTING THE SCREW" );
		return;
	}

	// Move the table back
	this->stepper->step( -STEPS_PER_TURN/8 );

	// Read the home switch again
	this->_read_home_switch();

	// Set the lowest velocity
	this->set_velocity( 1 );
	
	// Slow / Acurate homing
	count = 0;
	while ( !this->is_home() && count < STEPS_PER_TURN )
	{
		// Move the steper by 1 step
		this->stepper->step( 1 );

		// Read the home switch again
		this->_read_home_switch();

		// Count loops to stop if switch is not triggered
		count++;
	}

	// Check if homing failed
	if ( count > STEPS_PER_TURN )
	{
		this->_set_error( "HOMING FAILED: TRY ADJUSTING THE SCREW" );
		return;
	}

	// Set new position
	this->data.steps = 0;
	this->data.angle = 0.0f;

	// Reset velocity
	this->set_velocity( previous_velocity );

	// Set status
	this->_set_ok( "Home successfull" );

	// Turn OFF the LED
	this->_turn_led_off();
}

void TurnTable::rotate ( float angle )
{
	// Turn ON the LED
	this->_turn_led_on();

	// Compute the future position of this turn table
	float future_angle = fmod( this->data.angle + angle, 360.0f );
	int future_steps = (int)fmap( future_angle, 0.0f, 360.0f, 0.0, (float)STEPS_PER_TURN );   

	// Compute the number of step to go their
	int steps = future_steps - this->data.steps;
	if ( steps < 0 )
	{
		steps = steps + STEPS_PER_TURN;
	}

	// Move stepper
	stepper->step( steps );

	// Set the new position of this table
	this->data.steps = future_steps;
	this->data.angle = future_angle;
	  
	// Read home switch
	this->_read_home_switch();

	// Set status
	this->_set_ok( String("Rotatation by ")+angle+" degrees successfull" );

	// Turn OFF the LED
	this->_turn_led_off();
}

void TurnTable::step ( int steps )
{
	// Turn on LED
	this->_turn_led_on();

	// Move stepper
	stepper->step( steps );

	// Set new position
	this->data.steps = ( this->data.steps + steps ) % STEPS_PER_TURN;
	this->data.angle = fmap( (float)this->data.steps, 0.0f, (float)STEPS_PER_TURN, 0.0f, 360.0f );

	// Read home switch
	this->_read_home_switch();

	// Set status
	this->_set_ok( String("Rotation by ")+steps+" steps successfull" );

	// Turn OFF the LED
	this->_turn_led_off();
}

void TurnTable::turn ()
{
	if ( this->data.unit_angle > 0 )
	{
		this->rotate( this->data.unit_angle );

		// Set status
		this->_set_ok( String("Turn by ")+this->data.unit_angle+" degrees successfull" );
	}
}

void TurnTable::update ()
{
	this->_read_home_switch();
}


void TurnTable::print () const
{
	Serial.print( "{ " );

	// status
	Serial.print( "\"status\": \"" );
	Serial.print( this->_status_to_string() );
	Serial.print( "\", " );

	// steps
	Serial.print( "\"steps\": " );
	Serial.print( this->data.steps );
	Serial.print( ", " );
	
	// angle
	Serial.print( "\"angle\": " );
	Serial.print( this->data.angle );
	Serial.print( ", " );

	// is_home
	Serial.print( "\"is_home\": " );
	Serial.print( this->data.is_home ? "true" : "false" );
	Serial.print( ", " );

	// unit_angle
	Serial.print( "\"unit_angle\": " );
	Serial.print( this->data.unit_angle );
	Serial.print( ", " );

	// velocity
	Serial.print( "\"velocity\": " );
	Serial.print( this->data.velocity );
	Serial.print( ", " );

	// message
	Serial.print( "\"message\": \"" );
	Serial.print( this->data.message );
	Serial.print( "\"" );

	Serial.println( " }" );
}


void TurnTable::_read_home_switch ()
{
	this->data.is_home = (bool)digitalRead( PIN_END );
}

void TurnTable::_set_error ( const String message )
{
	this->data.status = TurnTableStatus::error;
	this->data.message = message;
}

void TurnTable::_set_ok ( const String message )
{
	this->data.status = TurnTableStatus::ok;
	this->data.message = message;
}

void TurnTable::_turn_led_on ()
{
	digitalWrite( PIN_LED, HIGH );
}

void TurnTable::_turn_led_off ()
{
	digitalWrite( PIN_LED, LOW );
}

String TurnTable::_status_to_string ()
{
	switch ( this->data.status )
	{
	default:
	case TurnTableStatus::unknown:
		return "unknown";
	case TurnTableStatus::error:
		return "error";
	case TurnTableStatus::ok:
		return "ok";
	}
}
