TURN TABLE COMMANDS:
----------------------------------------------------------------------------------------------------
ID	PARAM	NAME		DESCRIPTION
H	-		home		Home turn table.
R	<float>	rotate		Rotate the turn table by the given angle in degrees times the angle scale.
S	<int>	step		Rotate the turn table by the given number of steps.
T	-		turn		Rotate the turn table by 1 degree divided by the angle scale.
U	<float>	unit		Set the angle (degree) taken at each turn.
V	<int>	velocity	Set the velocity (speed) of the rotation movements.